
// Stylesheets
import "./main.scss";

// Javascript or Typescript
import "../components/**/*.ts";
import "../components/**/*.js";