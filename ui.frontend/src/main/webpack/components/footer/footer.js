$(document).ready(function () {
    /** Start:  Footer Copy Right year change dynamically **/
    var copyRightDate = new Date().getFullYear();
    $('.cmp-text__copyrightyear').text(copyRightDate);
    /** End:  Footer Copy Right year change dynamically **/
});
