$(document).ready(function () {
    //apply data attribute height to modal conent
    $('.cmp-modal').each(function () {
        if ($(this).attr("data-width")) {
            $(this).css('width', $(this).attr("data-width") + 'px');
        } else {
            $(this).addClass('modal-content-default-height');
        }
    });

    //checking outside click to close modal
    $(document).click(function (e) {
        if (e.target.className === 'cmp-md-overlay') {
            $('.cmp-modal').css('visibility', 'hidden');
            $('.cmp-modal').removeClass('md-show');
            $('.cmp-modal').siblings('.open-modal.is-active').removeClass('is-active');
            $('.open-modal .cmp-button').css('visibility', 'visible');
            // $('.open-modal .cmp-button').removeAttr("style");
        }
    });

    //checking button click to open modal
    $('.open-modal .cmp-button').on('click', function (event) {
        event.preventDefault();
        $(this).parent(".button.open-modal").siblings('.cmp-modal').css('visibility', 'visible');
        $(this).parent(".button.open-modal").siblings('.cmp-modal').addClass('md-show');
        $(this).parent(".button.open-modal").addClass('is-active');
        $(this).css('visibility', 'hidden');
    });

    //close button blick to close modal
    $('.close-modal').on('click', function () {
        $(this).closest('.cmp-modal').css('visibility', 'hidden');
        $(this).closest('.cmp-modal').removeClass('md-show');
        $(this).closest('.cmp-modal').siblings('.open-modal.is-active').removeClass('is-active');
        $(this).closest('.cmp-modal').siblings('.open-modal').find('.cmp-button').css('visibility', 'visible')
        // $('.open-modal .cmp-button').removeAttr("style");
    });

    //checking RTE link to open modal with ID & data attribute
    $('a').on('click', function (event) {
        if ($(this).attr('data-modal') && $(this).attr('data-modal') === 'open') {
            $('.cmp-modal#' + $(this).attr('data-modal-id')).css('visibility', 'visible');
            $('.cmp-modal#' + $(this).attr('data-modal-id')).addClass('md-show');
        }
    });

    //Scrollbar hidden funtionality 
    var timeout;
    $('.modal-content').addClass('scrollbar-hidden')
    $('.modal-content').on("scroll", function(){
        $('.modal-content').addClass('scrollbar-visible')
        $('.modal-content').removeClass('scrollbar-hidden')
        clearTimeout(timeout);
        timeout = setTimeout(function(){
            $('.modal-content').removeClass('scrollbar-visible')
            $('.modal-content').addClass('scrollbar-hidden')
        }, 400);
    });    
});