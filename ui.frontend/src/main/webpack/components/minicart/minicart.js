$(document).ready(function () {
    var localStorageData = JSON.parse(localStorage.getItem("mage-cache-storage"));
    //shopping cart JS
    if ($("body").hasClass("day")) {
        if (localStorageData && Object.entries(localStorageData).length && localStorageData.cart) {
            if(localStorageData.cart.summary_count > 0){
                $('.home-cart.icon-custom.icon-cart-day-filled-inactive').show();
                $('.home-cart.icon-custom.icon-cart-day-inactive').hide();
    
                $('.home-cart.icon-custom.icon-cart-night-filled-inactive').hide();
                $('.home-cart.icon-custom.icon-cart-night-inactive').hide();
            }else {
                $('.home-cart.icon-custom.icon-cart-day-filled-inactive').hide()
                $('.home-cart.icon-custom.icon-cart-day-inactive').show();
    
                $('.home-cart.icon-custom.icon-cart-night-filled-inactive').hide();
                $('.home-cart.icon-custom.icon-cart-night-inactive').hide();
            }
        } 
    } else {
        if (localStorageData && Object.entries(localStorageData).length && localStorageData.cart) {
            if(localStorageData.cart.summary_count > 0){
                $('.home-cart.icon-custom.icon-cart-night-filled-inactive').show();
                $('.home-cart.icon-custom.icon-cart-night-inactive').hide();
    
                $('.home-cart.icon-custom.icon-cart-day-filled-inactive').hide();
                $('.home-cart.icon-custom.icon-cart-day-inactive').hide();
            }else {
                $('.home-cart.icon-custom.icon-cart-night-filled-inactive').hide()
                $('.home-cart.icon-custom.icon-cart-night-inactive').show();
    
                $('.home-cart.icon-custom.icon-cart-day-filled-inactive').hide();
                $('.home-cart.icon-custom.icon-cart-day-inactive').hide();
            }
        } 
    }
})