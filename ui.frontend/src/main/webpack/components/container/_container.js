$(window).bind("load resize", function () {
    $('.container.responsivegrid.cmp-container__h-left').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });
    $('.container.responsivegrid.cmp-container__h-center').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });
    $('.container.responsivegrid.cmp-container__h-right').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });
    $('.container.responsivegrid.cmp-container__v-center').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });
    $('.container.responsivegrid.cmp-container__v-bottom').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });
    $('.container.responsivegrid.cmp-container__v-top').each(function () {
        $(this).children('.cmp-container').css('min-height', $(this).children('.cmp-container').children('div').height() + 'px');
    });

    $('.cmp-container__background-max-height').each(function (index, elem) {
        let img = $(this).css("background-image");
        img = img.match(/^url\("?(.+?)"?\)$/);
        if (img[1]) {
            img = img[1];
        }

        const image = new Image();
        image.src = img;

        let height;
        $(image).load(function () {
            height = image.height;
            if ($(elem).parent().height() < height) {
                $(elem).css("background-size", `auto ${$(elem).parent().height()}px `);
            } else {
                $(elem).css("background-size", `auto ${height}px `);
            }
        });
    });
})

//check page load time and add loading spinner
$(window).bind("load", function () {
    $('.loading-spinner').show();
    setTimeout(function () {
        $('.loading-spinner').hide();
    }, 1000);
});

$(document).ready(function () {
    $('.container.responsivegrid').each(function () {
        if ($(this).find('.cmp-container').attr('style') &&
            ($(this).find('.cmp-container').attr('style').includes('background') || $(this).find('.cmp-container').attr('style').includes('background-color'))) {
            var getStyle = $(this).find('.cmp-container').attr('style');
            $(this).attr('style', getStyle);
            $(this).find('.cmp-container').first().removeAttr('style');
        }
    });

    //copying header background container color to fixed header class.
    if ($('.header').parent().parent().parent().attr('style') && ($('.header').parent().parent().parent().attr('style').includes('background-color') || $('.header').parent().parent().parent().attr('style').includes('background'))) {
        var getStyle = $('.header').parent().parent().parent().attr('style');
        $('.header').attr('style', getStyle);
    }

    //add leadgen form to mobile container
    $('#leadgenDesktop').find(".leadgen").first().clone(true).appendTo('#mobileLeadGen');

    //Overlay 
    $('.cmp-container__z-index-1').each(function () { $(this).parent().css({ "position": "absolute", "z-index": "1", }); });
    $('.cmp-container__z-index-2').each(function () { $(this).parent().css({ "position": "absolute", "z-index": "2", }); });
    $('.cmp-container__z-index-3').each(function () { $(this).parent().css({ "position": "absolute", "z-index": "3", }); });
    $('.cmp-container__z-index-4').each(function () { $(this).parent().css({ "position": "absolute", "z-index": "4", }); });
    $('.cmp-container__z-index-5').each(function () { $(this).parent().css({ "position": "absolute", "z-index": "5", }); });

    /*** START: Loading container background image based on screen resolutions/breakpoints without downloading all images at once ***/
    $(window).bind("load resize", function () {
        var screenWidth = window.innerWidth;
        if (screenWidth < 768) {
            /** Load mobile image **/
            var imageBgs = $('[data-bgphoneimage]');
            for (var i = 0; i < imageBgs.length; i++) {
                if (imageBgs[i].length !== 0) {
                    imageBgs[i].style.backgroundImage = 'url(' + imageBgs[i].getAttribute('data-bgphoneimage') + ')';
                }
            }
        } else if (screenWidth >= 768 && screenWidth <= 1024) {
            /** Ipad/ Tablet **/
            var imageBgs = $('[data-bgtabletimage]');
            for (var i = 0; i < imageBgs.length; i++) {
                if (imageBgs[i].length !== 0) {
                    imageBgs[i].style.backgroundImage = 'url(' + imageBgs[i].getAttribute('data-bgtabletimage') + ')';
                }
            }
        } else {
            /** desktop image **/
            var imageBgs = $('[data-bgdesktopimage]');
            for (var i = 0; i < imageBgs.length; i++) {
                if (imageBgs[i].length !== 0) {
                    imageBgs[i].style.backgroundImage = 'url(' + imageBgs[i].getAttribute('data-bgdesktopimage') + ')';
                }
            }
        }
    })
    /*** END: Loading container background image based on screen resolutions/breakpoints without downloading all images at once ***/

});
