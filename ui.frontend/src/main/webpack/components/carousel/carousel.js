$(document).ready(function () {
    if ($("body").hasClass("day") && $(".header").hasClass("cmp-header__smoke") ) {
        $('.carousel').addClass('cmp-header__smoke');
    } else if ($("body").hasClass("day") && $(".header").hasClass("cmp-header__marshmallow") )  {
        $('.carousel').addClass('cmp-header__marshmallow');
    }

    if ($("body").hasClass("night") && $(".header").hasClass("cmp-header__coal") ) {
        $('.carousel').addClass('cmp-header__coal');
    } else if ($("body").hasClass("night") && $(".header").hasClass("cmp-header__charcoal") )  {
        $('.carousel').addClass('cmp-header__charcoal');
    }
});
