$(document).ready(function () {
    //setting 90 days cookies
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //if cookie exits hide banner
    if (getCookie('ccpa')) {
        $('.ccpa-banner').hide();
    }

    $('.ccpa-banner .button').click(function (event) {
        event.preventDefault();
        if (!getCookie('ccpa')) {
            var cookieName = 'ccpa'
            var timeToAdd = 1000 * 60 * 60 * 24 * 90;
            var date = new Date();
            var expiryTime = parseInt(date.getTime()) + timeToAdd;
            date.setTime(expiryTime);
            var utcTime = date.toUTCString();
            document.cookie = cookieName + "=yes; expires=" + utcTime + ";";
            $('.ccpa-banner').hide();
        }
    })
});