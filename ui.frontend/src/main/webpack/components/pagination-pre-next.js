$(document).ready(function(){
    var numOfCards = $(this).find('.pagination .page-item').length;
    if(numOfCards > 5){
        $('.move-prev').click(function() {
            event.preventDefault();
            $('.pagination').animate({
              scrollLeft: "-=56px"
            }, "fast");
          });
          
          $('.move-next').click(function() {
            event.preventDefault();
            $('.pagination').animate({
              scrollLeft: "+=56px"
            }, "fast");
        })
    }
    $(".catalogue-pagination .page-item").click(function(){
      if($(".catalogue-pagination .pagination .page-item:first-child").hasClass("active")){
        $(".catalogue-pagination .prev-wrap .page-item").hide();
      }else{
        $(".catalogue-pagination .prev-wrap .page-item").show();
      }
      if($(".catalogue-pagination .pagination .page-item:last-child").hasClass("active")){
        $(".catalogue-pagination .next-wrap .page-item").hide();
      }else{
        $(".catalogue-pagination .next-wrap .page-item").show();
      }
    });
    
});  