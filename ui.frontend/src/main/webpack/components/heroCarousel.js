$(document).ready(function() {
	var heroWcmMode = document.getElementById('heroWcmMode');
	if(heroWcmMode!=null){
		heroWcmMode = document.getElementById('heroWcmMode').value;
	}
	if(( heroWcmMode!=null || heroWcmMode!=undefined || heroWcmMode!='') && heroWcmMode=="true"){
        $('.carousel').attr('data-bs-interval','false');
        $('.progress').hide();
	}	
	else {
    $(window).on('focus', function () {
        $('.carousel').find('.indicator[aria-label="Slide1"]').click();
    });
    var attValue = $('.carousel').attr('data-bs-interval');

    $(window).resize(function(){
        var isMobile = window.matchMedia("only screen and (max-width: 991px)").matches;
        if (isMobile) {
            $('.carousel').attr('data-bs-interval','false');
        }else{
            $('.carousel').attr('data-bs-interval', attValue);
        }
    });
    $(function() {      
        var isMobile = window.matchMedia("only screen and (max-width: 991px)").matches;
        if (isMobile) {
            $('.carousel').attr('data-bs-interval','false');
        }
        });
    function setLink(elem){
        var label= elem.attr('data-label');
        var link= elem.attr('data-link');
        if(label && link){
            elem.closest('.carousel').find('.feature-links-url').attr('href',link).attr('data-cta',label);
            elem.closest('.carousel').find('.feature-links-label').text(label);
            elem.closest('.carousel').find('.feature-links').show();
        }else{
            elem.closest('.carousel').find('.feature-links').hide();
        }
    }

    setLink($(".carousel-indicators").find(".active"));
    $('.hero-carousel').each(function() {   
        var interval = parseInt($(this).find('.carousel').attr('data-bs-interval'));

        function progressAnimation(){
            $('.progress-bar').stop();
            $('.progress-bar').remove();
            $(".hero-carousel").find(".progress").append('<div class="progress-bar" role="progressbar" style="width: 0" aria-valuenow="25" aria-valuemin="50" aria-valuemax="100"></div>');
            
            if(interval == 0){
                $('.progress').remove();
            }

            setTimeout(function(){ 
                var element = $(".carousel-indicators").find(".active");
                setLink(element);
            }, 150);

            $(".hero-carousel").find(".progress-bar").animate(
                {width:"110%"},
                {duration: interval,
                    easing: "linear",
                complete: function(){
                    progressAnimation();
                }
            });       
        }
        
        progressAnimation();
        
        $(this).find('.indicator').click(function() {
            progressAnimation();
        })
    }) 
	}	
});