$(document).ready(function(){
    var stickyWcmModeVal = $('#stickyWcmMode').val();
    
    if(stickyWcmModeVal=="true"){
        $('.stickyEmail').css("position","relative").css("z-index","unset");
    }else{
        var windowHeight =  $(window).height();
        var divHeight = $('.sticky-email').height();

        $(window).resize(function(){
            windowHeight =  $(window).height();
            divHeight = $('.sticky-email').height();

            if ($('.contact-us-form').hasClass("show_contact")) {
                $(".stickyEmail").css({
                    bottom: (windowHeight - divHeight) + 'px'
                });
                $(".contact-us-form").show();
            } else {
                $(".stickyEmail").css({
                    bottom: '0'
                });
                setTimeout(function(){
                    $(".contact-us-form").hide();
                }, 1001);
            }
        });

        $(".sticky-email .icon-close").click(function(){
            if(!$(".sticky-right-section .email-section").hasClass("collapsed")){
                $(".sticky-right-section .email-section").toggleClass("collapsed");
                $(".sticky-left-section .icon-chevron-double-right").toggle();
                $(this).slideToggle();
            }

            if ($('.contact-us-form').hasClass("show_contact")) {
                $(".stickyEmail").animate({
                    bottom: '0'
                }, 1000);
                setTimeout(function(){
                    $(".contact-us-form").hide();
                }, 1001);
                $('.contact-us-form').removeClass("show_contact");
            }
        });

        $(".sticky-right-section .email-section").click(function(){
            $(this).toggleClass("collapsed");
            $(".contact-us-form").toggleClass("show_contact");
            $(".sticky-right-section .icon-close").slideToggle();
            $(".sticky-left-section .icon-chevron-double-right").toggle();

            if ($('.contact-us-form').hasClass("show_contact")) {
                $(".stickyEmail").animate({
                    bottom: (windowHeight - divHeight) + 'px'
                }, 1000);
                $(".contact-us-form").show();
            } else {
                $(".stickyEmail").animate({
                    bottom: '0'
                }, 1000);
                setTimeout(function(){
                    $(".contact-us-form").hide();
                }, 1001);
            }
        });
        $(".sticky-left-section .icon-chevron-double-right").click(function(){
            $(".sticky-email").hide();
            $(".contact-us-closed-section").show();
            $(".sticky-parent-container").toggleClass("no-background grey-background");
        });
        $(".contact-us-closed-section .icon-chevron-double-left").click(function(){
            $(".sticky-email").show();
            $(".contact-us-closed-section").hide();
            $(".sticky-parent-container").toggleClass("grey-background no-background");
        });
    }
});