$(document).ready(function(){
	var sections = $('.section')
  , nav = $('.sticky-anchor-items')
  , navHeight = nav.outerHeight();

  $('body').scroll(function() {
		var curPos = $(nav).scrollTop();
		sections.each(function() {
			var top = $(this).offset().top - navHeight,
				bottom = top + $(this).outerHeight();
			
			if (curPos >= top && curPos <= bottom) {
				nav.find('a').removeClass('active');
				sections.removeClass('active');
				$(this).addClass('active');
				nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
			}
		});
	});
});


$(document).ready(function () {
	var leftPaddle = document.getElementsByClassName('anchor-left-angle');
	var rightPaddle = document.getElementsByClassName('anchor-right-angle');

	// scroll to left
	$(".anchor-right-angle").on('click', function () {
		$('.sticky-anchor-items').animate({
			scrollLeft: "+=875px"
		}, "slow");
        $(leftPaddle).removeClass('hidden');
		$(rightPaddle).addClass('hidden');
	});

	// scroll to right
	$(".anchor-left-angle").on('click', function () {
		$('.sticky-anchor-items').animate({
			scrollLeft: '-=875px'
		}, "slow");
		$(rightPaddle).removeClass('hidden');
		$(leftPaddle).addClass('hidden');
	});
});