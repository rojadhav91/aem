$(document).ready(function () {
  if($('.focus_item_links').length>0){
    if($('.focus_item_links').prop('scrollWidth')>$('.focus_item_links').width()+10
    ||$('.focus_item_links').position().left+$('.focus_item_links').width()+10<$('.focus_item_links').find('li:last').position().left){
      //$('.focus_anc_pre').show();
      $('.focus_anc_next').show();
    }
  }

  $('.pre_button').click(function() {
      event.preventDefault();
      $('.focus_item_links').animate({
        scrollLeft: "-=100px"
      }, "slow",function() {
        if($('.focus_anc_pre').position().left<$('.focus_item_links').find('li:first').position().left)
          $('.focus_anc_pre').hide();
      });

      $('.focus_anc_next').show();
    });
    
    $('.next_button').click(function() {
      event.preventDefault();
      $('.focus_item_links').animate({
        scrollLeft: "+=100px"
      }, "slow",function() {
        if($('.focus_anc_next').position().left>$('.focus_item_links').find('li:last').position().left+$('.focus_item_links').find('li:last').width()+15)
          $('.focus_anc_next').hide();
      });

      $('.focus_anc_pre').show();
  });
});
