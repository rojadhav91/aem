$(document).ready(function () {
    //setDefaultOptionInDropDown();
    $("#btndd").on("click", function () {
        document.getElementById("myDropdown").classList.toggle("show");
        document.getElementById("spnCaret").classList.toggle("caret");
        
        if ($("#spnCaret").hasClass('caret'))
            $(".selectedItem").removeClass("leftBorderStyle");
        else
            $(".selectedItem").addClass("leftBorderStyle");

    })
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if($.find('.customer-account-paymentpreference').length !== 1) {
            if (!event.target.matches('.btnDropDown') && !event.target.matches('.btnDropDown span')
                && !event.target.matches('#spnCaret')) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                        document.getElementById("spnCaret").classList.add("caret");
                        $(".selectedItem").removeClass("leftBorderStyle");
                    }
                }
            }
        }
    }

    $(".ddOption").on("click", function () {
        var val = $(this).text();
        var abc = window.location.pathname;
        var urlSplit = abc.split("/site/");
        var subURL = urlSplit[0].split("/");
        var modifiedURL = "";
        for (var i = 0; i < subURL.length - 1; i++) {
            modifiedURL = modifiedURL + "/" + subURL[i];
        }
        modifiedURL = modifiedURL.substring(1);
        var previousLang = sessionStorage.getItem("languagecode");
        if (!previousLang) {
            if ($(this).attr("langcode") == "es") {
                previousLang = "en";
            } else {
                previousLang = "es";
            }
        }
        sessionStorage.setItem("languagecode", $(this).attr("langcode"));
        sessionStorage.setItem("languagedescription", val);
        window.location.href = window.location.href.replace("/" + previousLang + "/", "/" + $(this).attr("langcode") + "/");
        console.log(window.location.href);
    })

    var selectedLanguageCode = sessionStorage.getItem("languagecode");
    console.log(selectedLanguageCode);
    if (selectedLanguageCode) {
        document.getElementById("btndd").innerHTML = '<span class="selectedItem">' + sessionStorage.getItem("languagedescription") + '</span><span id="spnCaret" class="caret"></span>';
        $(".ddOption").each(function () {
            if ($(this).attr("langcode") == selectedLanguageCode) {
                $(this).css("display", "none");
            }
            else $(this).css("display", "block");
        });
    }
    else {
        var listItems = $("#myDropdown a");
        for (var li of listItems) {
            if ($(li).attr("isDefaultLanguage") == 'Y') {
                document.getElementById("btndd").innerHTML = '<span class="selectedItem">' + $(li).text() + '</span><span id="spnCaret" class="caret"></span>';
            }
        }
        $(".ddOption").each(function () {
            if ($(this).attr("isDefaultLanguage") == 'Y') {
                $(this).css("display", "none");
            }
            else $(this).css("display", "block");
        });

    }
});


