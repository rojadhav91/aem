$(document).ready(function(){
    var imgTagSrc = $('.molecule-animation .molecule-animation-gif img').attr("src");
    $('.molecule-animation .molecule-animation-gif img').attr("src","");
    $('body').scroll(function(){
        if($('.molecule-animation').length>0){
            if ($('.molecule-animation').offset().top <= 30) {
                if($('.molecule-animation .molecule-animation-gif img').attr("src") === ""){
                    $('.molecule-animation .molecule-animation-gif img').attr("src",imgTagSrc);
                    $('.molecule-animation').css("visibility", "visible");
                    $('.molecule-animation .CTA-highlight').fadeIn(3000);
                }
            } else {
                $('.molecule-animation .molecule-animation-gif img').attr("src","");
                $('.molecule-animation').css("visibility", "hidden");
                $('.molecule-animation .CTA-highlight').fadeOut(3000);
            }
        }
    });
});