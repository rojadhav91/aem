
$(document).ready(function () {
    var localStorageData = JSON.parse(localStorage.getItem("mage-cache-storage"));
    //my account pop up box
    $('#myaccount').on('click', function () {
        $(".my-account-popup").toggle();
        $('#myaccount').addClass('active');
    })
    $('.my-account-popup nav a').on('click', function () {
        $('.my-account-popup nav a').removeClass('active');
        $(this).addClass('active');
    })
    $(document).mouseup(function (e) {
        //console.log(e)
        if (e.target.id !== 'burger-menu-right' || 'account-menu') {
            $(".my-account-popup").hide();
        }
        $('#myaccount').removeClass('active');
        
    });

    //sign in & sign out button
    if (localStorageData && Object.entries(localStorageData).length && localStorageData.customer) {
        if(localStorageData.customer.fullname){
            $('.auth .signIn').hide();
            //$('.auth p').hide();
            $('.auth .signOut').show();
            $('#myaccount span').show();
            var username = "Hi, "+localStorageData.customer.fullname;
            $('.modal-heading').text(username);
        }else {
            $('.auth .signIn').show();
            $('.auth .signOut').hide();
            $('#myaccount span').hide();
        }
    }else {
        $('.auth .signIn').show();
        $('.auth .signOut').hide();
        $('#myaccount span').hide();
    }

    // Add active class to user menu <a>
    $('.my-account-popup nav li a').each(function () {
        const anchorPath = this.pathname;
        const windowPath = window.location.pathname;
        const aemPath = $(this).parents('ul').data('page-path') + '.html';

        if ((windowPath === anchorPath) || (aemPath === anchorPath)) {
            $(this).addClass('active');
        }
    });

    $('.auth .signOut').on('click', function(){
        localStorage.setItem('mage-cache-storage','{}');
    });
});