$(document).ready(function () {
    //add exp. fragment class dynamically to exp. fragment
    $(".floyout-submenu .cmp-list").addClass("experiment-fragment");

    function resetToMobileScreen() {
        $('.floyout-submenu').removeClass('showBlock');
        $('.container-header-fluid .parent-list').removeClass('showMobileBlock');

        $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
        $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").hide();
        $(".floyout-submenu").removeClass('showBlock');
        $(".cmp-navigation__item").removeClass('removeBorder');
        $('.cmp-navigation__item-link').removeClass('backButtonText');
        $('.aem-navigation').parent('li').siblings().removeAttr("style");
        $('.aem-navigation').parent('li').removeAttr("style");
        //add next & previous icon
        $(".parentBlock .cmp-navigation__group > .cmp-navigation__item > a + ul").prev("a")
            .after('<span class="iconarrow icon-arrow_forward-active children"></span>')
            .before('<span class="iconarrow icon-arrow_backward parent"></span>');
        $(".parentBlock  .iconarrow.icon-arrow_forward-active.children").show();
    };

    $(window).bind("load resize", function () {
        if ($(window).width() <= 1024) {
            $('.parent-list').addClass('parentBlock');
            resetToMobileScreen();
            $('#burger-menu').show();
            $('#close-menu').hide();

            if ($('.parent-list').hasClass('parentBlock')) {
                $('.navLink').removeClass("active");
                //add next & previous icon
                $(".parentBlock .cmp-navigation__group > .cmp-navigation__item > a + .floyout-submenu").prev("a")
                    .after('<span class="iconarrow icon-arrow_forward-active children"></span>')
                    .before('<span class="iconarrow icon-arrow_backward parent"></span>');
                $(".parentBlock  span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                //open side navigation on menu click
                $('.cmp-navigation__group li.cmp-navigation__item a.cmp-navigation__item-link').click(function () {
                    if (!$(this).next().hasClass('showblock')) {
                        $('.showblock').removeClass('showblock');
                    }
                    $('.cmp-navigation__item').removeClass('cmp-navigation__item--active');
                    $(this).parent().addClass('cmp-navigation__item--active');
                    $(this).next().addClass('showblock');
                    $(".floyout-submenu ul > li").show();
                    $(".floyout-submenu ul > li ul").hide();
                    $(".cmp-navigation__item").removeClass('removeBorder');
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                    $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").show();

                });


                $("span.children").on('click', function () {
                    $('.navLink').removeClass("active");
                    $('.parent-list > li').css('display', 'none');
                    $(this).parents('.aem-navigation').parent('li').css('display', 'block');
                    $(this).siblings('.floyout-submenu').addClass('showBlock');
                    $(this).siblings('.floyout-submenu').find('ul:first').show();
                    //set parent text
                    var parentText = $(this).prev("a").text();

                    $(this).parent().addClass("removeBorder");
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                    $(this).hide();
                    $(this).prev().addClass("backButtonText");
                    //set parent text
                    var parentText = $(this).prev("a").text();

                    //hide other li
                    $(this).parent().siblings().hide();

                    //show clicked as a back button
                    $(".backText").text(parentText).show();

                    //show uls under clicked parent
                    $(this).siblings('ul:first').show();


                    $(this).parents('.aem-navigation').parent('li').find("span.iconarrow.iconarrow.icon-arrow_backward.parent:first").show();
                    $(".floyout-submenu span.children").on('click', function () {
                        $(this).parents('.aem-navigation').parent('li').find('a:first').css('display', 'none');
                        $(".floyout-submenu  span.iconarrow.iconarrow.icon-arrow_backward.parent").show();
                        $(".floyout-submenu  span.iconarrow.iconarrow.icon-arrow_forward-active.children").hide();
                        $(this).parents('.aem-navigation').parent('li').find("span.iconarrow.iconarrow.icon-arrow_backward.parent:first").hide();
                    })
                    $(".floyout-submenu span.parent").on('click', function () {
                        $(this).parents('.aem-navigation').parent('li').find('a:first').css('display', 'inline-block');
                        $(this).parents('.aem-navigation').parent('li').find('span.iconarrow.iconarrow.icon-arrow_backward.parent:first').show();
                        $(this).parents('.aem-navigation').parent('li').find('span.iconarrow.iconarrow.icon-arrow_forward-active.children:first').hide();
                        $(".floyout-submenu  span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                        $(".floyout-submenu  span.iconarrow.iconarrow.icon-arrow_forward-active.children").show();
                    })

                    $(".aem-navigation").parent('li').find('span.iconarrow.iconarrow.icon-arrow_backward.parent:first').on('click', function () {
                        $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                        $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").hide();
                        $(".floyout-submenu").removeClass('showBlock');
                        $(".cmp-navigation__item").removeClass('removeBorder');
                        $(this).parents('.aem-navigation').parent('li').siblings().removeAttr("style");
                        $(this).parents('.aem-navigation').parent('li').removeAttr("style");
                        //add next & previous icon
                        $(".parentBlock .cmp-navigation__group > .cmp-navigation__item > a + ul").prev("a")
                            .after('<span class="iconarrow icon-arrow_forward-active children"></span>')
                            .before('<span class="iconarrow icon-arrow_backward parent"></span>');
                        $(".parentBlock  .iconarrow.icon-arrow_forward-active.children").show();
                    })


                });

                $("span.parent").on('click', function () {
                    if ($(this).hasClass('parent')) {
                        $("#text").html($(this).parent().parent().parent().find('a:first').text());
                    }
                    $(this).parent().removeClass("removeBorder");
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                    $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").show();
                    var parentUl = $(this).closest('ul');
                    var backText = $(this).closest('ul').parent('li').find('a:first').text();
                    if (backText == '') {
                        //show clicked as a back button
                        $(".backText").hide();
                    } else {
                        //show clicked as a back button
                        $(".backText").text(backText);
                    }
                    parentUl.children().show().find('ul').hide();
                });
            }

        } else {
            $('.parent-list').removeClass('parentBlock');
            resetToMobileScreen();
            $('#close-menu').hide();
            $('#burger-menu').hide();

            if (!$('.parent-list').hasClass('parentBlock')) {
                $(".cmp-navigation__group > .cmp-navigation__item").find("span").not('.cmp-list__item-title').remove();
                //flyout menu reset
                $(".floyout-submenu ul > li ul").hide();
                $(".floyout-submenu ul > li ul").hide();
                $(".floyout-submenu ul > li > a + ul").prev("a")
                    .after('<span class="test iconarrow icon-arrow_forward-active children"></span>')
                    .before('<span class="test iconarrow icon-arrow_backward parent"></span>');
                $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();

                //open side navigation on menu click
                $('.cmp-navigation__group li.cmp-navigation__item a.cmp-navigation__item-link').click(function () {
                    $('.navLink').removeClass("active");
                    if ($(this).next().hasClass('showblock')) {
                        $('.showblock').removeClass('showblock');
                        // $(this).parents().parent(".experiencefragment").next().removeAttr("style");
                    }else{
                         $(this).next().addClass('showblock');
                    }
                    // //Flyout-Menu Updates                    
                    // $(this).parents().parent(".experiencefragment").next().attr("style", "margin-left:"+$('.floyout-submenu.showblock').width() + "px !important");
                    
                    $('.cmp-navigation__item').removeClass('cmp-navigation__item--active');
                    $(this).parent().addClass('cmp-navigation__item--active');
                    // $(this).next().addClass('showblock');
                    $(".floyout-submenu ul > li").show();
                    $(".floyout-submenu ul > li ul").hide();
                    $(".cmp-navigation__item").removeClass('removeBorder');
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                    $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").show();

                });

                $("span.children").on('click', function () {
                    $(this).parent().addClass("removeBorder");
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").show();
                    $(this).hide();
                    $(this).prev().addClass("backButtonText");
                    //set parent text
                    var parentText = $(this).prev("a").text();

                    //hide other li
                    $(this).parent().siblings().hide();

                    //show clicked as a back button
                    $(".backText").text(parentText).show();

                    //show uls under clicked parent
                    $(this).siblings('ul:first').show();

                });

                $("span.parent").on('click', function () {
                    $(this).parent().removeClass("removeBorder");
                    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();
                    $("span.iconarrow.iconarrow.icon-arrow_forward-active.children").show();
                    var parentUl = $(this).closest('ul');
                    var backText = $(this).closest('ul').parent('li').find('a:first').text();
                    if (backText == '') {
                        //show clicked as a back button
                        $(".backText").hide();
                    } else {
                        //show clicked as a back button
                        $(".backText").text(backText);
                    }
                    parentUl.children().show().find('ul').hide();
                });
            }
        }
    });

    // mobile responsive hamburger menu and close button
    $('#burger-menu').click(function () {
        $('.container-header-fluid .parent-list').addClass('showMobileBlock');
        $('#close-menu').show();
        $('#burger-menu').hide();
    })
    $('#close-menu').click(function () {
        $('#close-menu').hide();
        $('#burger-menu').show();
        $('.parentBlock .input-box').hide();
        resetToMobileScreen();
    })
    $('.navLink').click(function () {
        $('.navLink').removeClass("active");
        $(this).addClass("active");
    });

    //flyout menu
    $(".floyout-submenu ul > li ul").hide();
    $(".floyout-submenu ul > li ul").hide();
    $(".floyout-submenu ul > li > a + ul").prev("a")
        .after('<span class="test iconarrow icon-arrow_forward-active children"></span>')
        .before('<span class="test iconarrow icon-arrow_backward parent"></span>');
    $("span.iconarrow.iconarrow.icon-arrow_backward.parent").hide();


    //close sidenavigation outside click
    $(window).click(function (event) {
        var $target = $(event.target);
        if ($(window).width() <= 1024 && !$target.closest('#burger-menu').length && !$target.closest('.showMobileBlock').length) {
            $('.parent-list').removeClass('showMobileBlock');
            $('#close-menu').hide();
            $('#burger-menu').show();
            $('.parentBlock .input-box').hide();
            resetToMobileScreen();
        }
    });

    // Add active class to active top-nav link
    $('.header .container-header-fluid .parent-list .navLink a').each(function () {
        const anchorPath = this.pathname;
        const windowPath = window.location.pathname;
        const aemPath = $(this).parents('ul.parent-list').data('page-path') + '.html';

        if ((windowPath === anchorPath) || (aemPath === anchorPath)) {
            $(this).parent('li').addClass('active');
        }
    });
});
