$(document).ready(function(){
    if($(".footer-country-links").length){
        var pagePath = $(".footer-country-links").attr("data-page-path");
        var pageInfo = {
            currentPagePath : pagePath
        };
        $.ajax({
            url : "/bin/kemin-redesign/regionSelector.json",
            data: pageInfo,
            method : 'GET'
        }).done(function(response) {
            var currentRegionCode = response.currentRegionLang.selectedRegion;
            var htmlString="<ul>";
            $.each(response.regionList, function(index, value) {
                if(value.regionName == currentRegionCode){
                    htmlString+='<li class="current-region">'+value.regionTitle+'</li>';  
                }else{
                    htmlString+='<li>'+value.regionTitle+'</li>';  
                }   
            });
            htmlString+="</ul>";
            $(".footer-country-links").html(htmlString);
        });
    }
});