$(function() {
  function removeLastSeparator(){
    var lastElement = false;
    $(".footer-multi-links ul > li").each(function() {
      $(this).css("border-right","1px solid rgba(255, 255, 255, .2)");
        if (lastElement && lastElement.offset().top != $(this).offset().top) {
            lastElement.css("border-right","0px");
        }
        lastElement = $(this);
    }).last().css("border-right","0px");
  }

  removeLastSeparator();

  $(window).on('resize', function(){
    removeLastSeparator();
  });
});