function ruContactUsEmailClickedTracking() {
    $('a').click(function(){
        var e = $(this);
        var email = e.attr('href') && e.attr('href').startsWith("mailto:") ? e.attr('href') : null;
        if (email) {
            if (typeof digitalData !== "undefined") {
                digitalData.event = digitalData.event || [];
                digitalData.event.push({
                    eventInfo:{
                        eventName: email.replace('mailto:', ''),
                        eventAction: 'email-click',
                        type: 'contact'
                    },
                    category:{
                        primaryCategory: 'contact'
                    },
                    attributes:{}
                });
            }
            if (typeof _satellite !== "undefined") {
                _satellite.track('generic-event');
            }
        }
    });
}