$('a[title=faq-chatnow]').click(function () {
   $("#asapp-chat-sdk-badge").trigger("click");
   return false;
});

$('.cmp-color-link').hover(function () {
   var className = null;
   if ($(this).children('span').length) {
      className = $(this).children('span').attr('class');
   } else {
      className = $(this).parent('span').attr('class');
   }
   if (className) {
      $(this).closest('a').removeClass(className);
      $(this).closest('a').addClass(className);
   }
});
$(".cmp-chat-link").on("click", function () {
   ASAPP('show');
});


$(document).ready(function () {
   $('.text').each(function () {
      if ($(this).find('.cmp-text__no-hover').length > 0) {
         $(this).addClass('cmp-text__no-hover');
         $(this).find('span').removeClass('cmp-text__no-hover');
      }
   })

   //checkmark JS
   $('.text .cmp-text__checkmark').each(function () {
      $(this).closest("li").addClass('cmp-text__checkmark');

      // adding colors to checkmark icons
      if ($(this).closest("li").find('.cmp-text__coal').length == 1) {
         $(this).closest("li").addClass('cmp-text__coal')
      }
      if ($(this).closest("li").find('.cmp-text__charcoal').length == 1) {
         $(this).closest("li").addClass('cmp-text__charcoal')
      }
      if ($(this).closest("li").find('.cmp-text__cinder').length == 1) {
         $(this).closest("li").addClass('cmp-text__cinder')
      }
      if ($(this).closest("li").find('.cmp-text__ash').length == 1) {
         $(this).closest("li").addClass('cmp-text__ash')
      }
      if ($(this).closest("li").find('.cmp-text__smoke').length == 1) {
         $(this).closest("li").addClass('cmp-text__smoke')
      }
      if ($(this).closest("li").find('.cmp-text__marshmallow').length == 1) {
         $(this).closest("li").addClass('cmp-text__marshmallow')
      }
      if ($(this).closest("li").find('.cmp-text__green').length == 1) {
         $(this).closest("li").addClass('cmp-text__green')
      }
      if ($(this).closest("li").find('.cmp-text__boost-orange-1').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-orange-1')
      }
      if ($(this).closest("li").find('.cmp-text__boost-orange-2').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-orange-2')
      }
      if ($(this).closest("li").find('.cmp-text__boost-orange-3').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-orange-3')
      }
      if ($(this).closest("li").find('.cmp-text__boost-dark-blue').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-dark-blue')
      }
      if ($(this).closest("li").find('.cmp-text__boost-mid-blue').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-mid-blue')
      }
      if ($(this).closest("li").find('.cmp-text__boost-accent-blue').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-accent-blue')
      }
      if ($(this).closest("li").find('.cmp-text__boost-accent-green').length == 1) {
         $(this).closest("li").addClass('cmp-text__boost-accent-green')
      }
      if ($(this).closest("li").find('.cmp-text__mineral-green').length == 1) {
         $(this).closest("li").addClass('cmp-text__mineral-green')
      }
      if ($(this).closest("li").find('.cmp-text__russian-black').length == 1) {
         $(this).closest("li").addClass('cmp-text__russian-black')
      }

      //checkmark font-size
      if ($(this).closest("li").find('.cmp-text__text-256').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-256')
      }
      if ($(this).closest("li").find('.cmp-text__text-192').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-192')
      }
      if ($(this).closest("li").find('.cmp-text__text-128').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-128')
      }
      if ($(this).closest("li").find('.cmp-text__text-96').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-96')
      }
      if ($(this).closest("li").find('.cmp-text__text-62').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-62')
      }
      if ($(this).closest("li").find('.cmp-text__text-52').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-52')
      }
      if ($(this).closest("li").find('.cmp-text__text-44').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-44')
      }
      if ($(this).closest("li").find('.cmp-text__text-36').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-36')
      }
      if ($(this).closest("li").find('.cmp-text__text-32').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-32')
      }
      if ($(this).closest("li").find('.cmp-text__text-28').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-28')
      }
      if ($(this).closest("li").find('.cmp-text__text-24').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-24')
      }
      if ($(this).closest("li").find('.cmp-text__text-20').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-20')
      }
      if ($(this).closest("li").find('.cmp-text__text-18').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-18')
      }
      if ($(this).closest("li").find('.cmp-text__text-16').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-16')
      }
      if ($(this).closest("li").find('.cmp-text__text-14').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-14')
      }
      if ($(this).closest("li").find('.cmp-text__text-12').length == 1) {
         $(this).closest("li").addClass('cmp-text__text-12')
      }
      $(this).removeClass('cmp-text__checkmark')
   })

   //circle styles JS
   $('.text .cmp-text-circle-ghost').each(function () {
      if ($(this).closest("p").find('.cmp-text__green').length == 1) {
         $(this).closest("p").addClass('cmp-text__green')
      }
      if ($(this).closest("p").find('.cmp-text__coal').length == 1) {
         $(this).closest("p").addClass('cmp-text__coal')
      }
      if ($(this).closest("p").find('.cmp-text__charcoal').length == 1) {
         $(this).closest("p").addClass('cmp-text__charcoal')
      }
      if ($(this).closest("p").find('.cmp-text__cinder').length == 1) {
         $(this).closest("p").addClass('cmp-text__cinder')
      }
      if ($(this).closest("p").find('.cmp-text__ash').length == 1) {
         $(this).closest("p").addClass('cmp-text__ash')
      }
      if ($(this).closest("p").find('.cmp-text__smoke').length == 1) {
         $(this).closest("p").addClass('cmp-text__smoke')
      }
      if ($(this).closest("p").find('.cmp-text__marshmallow').length == 1) {
         $(this).closest("p").addClass('cmp-text__marshmallow')
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-1').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-1')
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-2').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-2')
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-3').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-3')
      }
      if ($(this).closest("p").find('.cmp-text__boost-dark-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-dark-blue')
      }
      if ($(this).closest("p").find('.cmp-text__boost-mid-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-mid-blue')
      }
      if ($(this).closest("p").find('.cmp-text__boost-accent-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-accent-blue')
      }
      if ($(this).closest("p").find('.cmp-text__boost-accent-green').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-accent-green')
      }
      // $(this).removeClass('cmp-text-circle-ghost');      
      // $(this).closest("p").addClass('cmp-text-circle-ghost');
   })
   $('.text .cmp-text-circle-filled').each(function () {
      if ($(this).closest("p").find('.cmp-text__green').length == 1) {
         $(this).closest("p").addClass('cmp-text__green');
      }
      if ($(this).closest("p").find('.cmp-text__coal').length == 1) {
         $(this).closest("p").addClass('cmp-text__coal');
      }
      if ($(this).closest("p").find('.cmp-text__charcoal').length == 1) {
         $(this).closest("p").addClass('cmp-text__charcoal');
      }
      if ($(this).closest("p").find('.cmp-text__cinder').length == 1) {
         $(this).closest("p").addClass('cmp-text__cinder');
      }
      if ($(this).closest("p").find('.cmp-text__ash').length == 1) {
         $(this).closest("p").addClass('cmp-text__ash');
      }
      if ($(this).closest("p").find('.cmp-text__smoke').length == 1) {
         $(this).closest("p").addClass('cmp-text__smoke');
      }
      if ($(this).closest("p").find('.cmp-text__marshmallow').length == 1) {
         $(this).closest("p").addClass('cmp-text__marshmallow');
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-1').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-1')
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-2').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-2');
      }
      if ($(this).closest("p").find('.cmp-text__boost-orange-3').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-orange-3');
      }
      if ($(this).closest("p").find('.cmp-text__boost-dark-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-dark-blue');
      }
      if ($(this).closest("p").find('.cmp-text__boost-mid-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-mid-blue');
      }
      if ($(this).closest("p").find('.cmp-text__boost-accent-blue').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-accent-blue');
      }
      if ($(this).closest("p").find('.cmp-text__boost-accent-green').length == 1) {
         $(this).closest("p").addClass('cmp-text__boost-accent-green');
      }
      // $(this).removeClass('cmp-text-circle-filled');      
      // $(this).closest("p").addClass('cmp-text-circle-filled');
   });
   // cmp-text_copyrightyear these class bind year it's use in footer 
   const currentYear= new Date().getFullYear(); 
   $(".cmp-text_copyrightyear").text("@"+ currentYear);

});