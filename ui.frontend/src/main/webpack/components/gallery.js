$(document).ready(function () {
	var slideIndex = 2;
	var slideIndexMobile = 1;
	var objArr = [];
	var count = 0;
	var galleryMap = new Map();
	var galleryMapMobile = new Map();
	var windowsize = $(window).width();
	$(".image-gallery-component").each(function () {
		if ($(this).is(':visible')) {
			var id = $(this).attr("id", "gallery-" + count);
			galleryMap.set("gallery-" + count, slideIndex);
			galleryMapMobile.set("gallery-" + count, slideIndexMobile);
			objArr.push(id);
			showSlides(slideIndex, this);
			showSlidesMobile(slideIndexMobile, this)
			count++;
		}
	});

	function showSlides(n, obj) {
		var parent = $(obj).attr('id');
		var index = galleryMap.get(parent);
		var slides = $(obj).find(".mySlides");
		var dots = $(obj).find(".thumbnail");
		var captionText = $(obj).find(".caption-text");
		var i;
		if (n > slides.length) {
			index = 1;
			galleryMap.set(obj.attr('id'), index);
		}
		if (n < 1) {
			index = slides.length;
			galleryMap.set(obj.attr('id'), index);
		}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].style.opacity = "0.8";
			dots[i].style.filter = "brightness(30%)";
		}
		dots[index - 1].style.opacity = "1";
		dots[index - 1].style.filter = "brightness(100%)";
		slides[index - 1].style.display = "block";
		captionText.innerHTML = dots[index - 1].alt;

	}

	function showSlidesMobile(n, obj) {
		var parent = $(obj).attr('id');
		var index = galleryMapMobile.get(parent);
		var slidesMobile = $(obj).find(".mySlides-mobile");
		var dotsMobile = $(obj).find(".thumbnail-mobile");
		var captionTextMobile = $(obj).find(".caption-text-mobile");
		var i;
		if (n > slidesMobile.length) {
			index = 1;
			galleryMapMobile.set(obj.attr('id'), index);
		}
		if (n < 1) {
			index = slidesMobile.length;
			galleryMapMobile.set(obj.attr('id'), index);
		}
		for (i = 0; i < slidesMobile.length; i++) {
			slidesMobile[i].style.display = "none";
		}
		for (i = 0; i < dotsMobile.length; i++) {
			dotsMobile[i].style.opacity = "0.8";
			dotsMobile[i].style.filter = "brightness(30%)";
		}
		dotsMobile[index - 1].style.opacity = "1";
		dotsMobile[index - 1].style.filter = "brightness(100%)";
		slidesMobile[index - 1].style.display = "block";
		captionTextMobile.innerHTML = dotsMobile[index - 1].alt;
	}

	$(".chevron-right").click(function () {
		var currentObj = $(this).closest('.image-gallery-component').attr('id');
		var parent = $("#" + currentObj);
		var thumbs = parent.find(".thumbnail-column");
		var $first = thumbs.first();
		var last = thumbs.last();
		var newclass = parent.find(".thumbnail").attr("id");
		var outerWidth = parent.find(".tiles").outerWidth();
		var outerWidthThumbnail = parent.find(".thumbnail").outerWidth();
		var thumbnailWidth = outerWidthThumbnail + 20;
		var scrollProperty = 0;
		var scrollWidth = parent.find(".thumbnail-column").outerWidth();
		// console.log("child outer wi"+scrollWidth);
		let count = parent.find(".thumbnail-column").length;
		outerWidth = outerWidth * count;

		if (last.children('.thumbnail').css("opacity") == 1) {
			parent.find(".tiles").animate({
				scrollLeft: "-=" + outerWidth + "px"
			}, "slow");
		} else {
			parent.find(".tiles").animate({
				scrollLeft: "+=" + scrollWidth + "px"
			}, "slow");
		}
		plusSlides(1, parent);
	});

	$(".icons-chevron-right-mobile").click(function () {
		var currentObj = $(this).closest('.image-gallery-component').attr('id');
		var parent = $("#" + currentObj);
		var thumbs = parent.find(".thumbnail-column-mobile");
		var $first = thumbs.first();
		var last = thumbs.last();
		var newclass = parent.find(".thumbnail-mobile").attr("id");
		var outerWidth = parent.find(".tiles-mobile").outerWidth();
		let scrollWidth = parent.find(".thumbnail-column-mobile").outerWidth();
		let scrollProperty = 0;
		let count = parent.find(".thumbnail-column-mobile").length;
		outerWidth = outerWidth * count;
		// console.log();


		//var outerWidth = parent.find(".tiles-mobile").outerWidth();
		if (last.children('.thumbnail-mobile').css("opacity") == 1) {
			parent.find(".tiles-mobile").animate({
				scrollLeft: "-=" + outerWidth + "px"
			}, "slow");
		} else {
			parent.find(".tiles-mobile").animate({
				scrollLeft: "+=" + scrollWidth + "px"
			}, "slow");
		}
		plusSlidesMobile(1, parent);
	});


	$(".chevron-left").click(function () {
		var currentObj = $(this).closest('.image-gallery-component').attr('id');
		var parent = $("#" + currentObj);
		var thumbs = parent.find(".thumbnail-column");
		var first = thumbs.first();
		var outerWidth = parent.find(".tiles").outerWidth();
		var outerWidthThumbnail = parent.find(".thumbnail").outerWidth();
		var thumbnailWidth = outerWidthThumbnail + 20;
		let scrollWidth = parent.find(".thumbnail-column").outerWidth();
		let scrollProperty = 0;
		let count = parent.find(".thumbnail-column").length;
		outerWidth = outerWidth * count;
        console.log("outer width of thumbnail "+scrollWidth);

		if (first.children('.thumbnail').css("opacity") == 1) {
			parent.find(".tiles").animate({
				scrollLeft: "+=" + outerWidth + "px"
			}, "slow");
		} else {
			parent.find(".tiles").animate({
				scrollLeft: "-=" + scrollWidth + "px"
			}, "slow");
		}
		plusSlides(-1, parent);
	});

	$(".icons-chevron-left-mobile").click(function () {
		var currentObj = $(this).closest('.image-gallery-component').attr('id');
		var parent = $("#" + currentObj);
		var thumbs = parent.find(".thumbnail-column-mobile");
		var first = thumbs.first();
		var outerWidth = parent.find(".tiles-mobile").outerWidth();
		var outerWidthThumbnail = parent.find(".thumbnail-mobile").outerWidth();
		var thumbnailWidth = outerWidthThumbnail + 20;
		let scrollWidth = parent.find(".thumbnail-column-mobile").outerWidth();
		let scrollProperty = 0;
		let count = parent.find(".thumbnail-column-mobile").length;
		outerWidth = outerWidth * count;


		if (first.children('.thumbnail-mobile').css("opacity") == 1) {
			parent.find(".tiles-mobile").animate({
				scrollLeft: "+=" + outerWidth + "px"
			}, "slow");
		} else {
			parent.find(".tiles-mobile").animate({
				scrollLeft: "-=" + scrollWidth + "px"
			}, "slow");
		}
		plusSlidesMobile(-1, parent);
	});

	function plusSlides(n, obj) {
		var index = galleryMap.get(obj.attr('id'));
		galleryMap.set(obj.attr('id'), index += n);
		showSlides(galleryMap.get(obj.attr('id')), obj);
	}

	function plusSlidesMobile(n, obj) {
		var index = galleryMapMobile.get(obj.attr('id'));
		galleryMapMobile.set(obj.attr('id'), index += n);
		showSlidesMobile(galleryMapMobile.get(obj.attr('id')), obj);
	}

	$(".thumbnail ").click(function () {

		var currentObj = $(this).closest('.image-gallery-component').attr('id');
		var parent = $("#" + currentObj);
		var thumbs = parent.find(".thumbnail-column");
		var newclass = $(this).attr("id");
		var index = galleryMap.get(parent.attr('id'));
		newclass++;
		index = newclass;
		galleryMap.set(parent.attr('id'), index);
		showSlides(galleryMap.get(parent.attr('id')), parent);
	});

	$(".thumbnail-mobile ").click(function () {
		var currentObj = this.closest('.image-gallery-component');
		var slides = $(currentObj).find(".mySlides-mobile");
		var dots = $(currentObj).find(".thumbnail-mobile");
		var newclass = $(this).attr("id");
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].style.opacity = "0.8";
			dots[i].style.filter = "brightness(30%)";
		}
		slides[newclass].style.display = "block";
		$(this).css("opacity", "1").css("filter", "brightness(100%)");
	});

});