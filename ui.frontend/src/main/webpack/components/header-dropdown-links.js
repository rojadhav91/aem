$(document).ready(function(){
    $(".header-link").click(function(){
        $(".navbar-collapse").removeClass("open_menu");
    });
});

function megaMenu(leftOffset, _this){
    $(".search_box").hide();
    $(".lg-search").removeClass("lg-hover-search");
    $(".icon-search").removeClass("text-white");
    if(!_this.hasClass("lg-hover-link")){
        $(".header-link").find(".header_mega_menu").hide();
        $(".header-link").removeClass("lg-hover-link text-white downarrow_hide");
    }
    
    _this.find(".mega_menu, .services_mega_position").css({
        'width' : $(window).width(),
        'left' : leftOffset
    });
    
    _this.find(".header_mega_menu").toggle();
    _this.toggleClass("lg-hover-link text-white downarrow_hide");
}

function mobileDesktopMenu() {
    if($(window).width() > 991){        
        $(".header-link").hover(function(){
            if(! $(".header-link").hasClass("search-box-open")){
                var leftOffset = '-' + ($(this).offset().left + 18) + 'px';
                var _this = $(this);
                megaMenu(leftOffset, _this); 
            }
        });
    }else {
        $(".header-link").click(function(){
            var leftOffset = '-' + ($(this).offset().left + 10) + 'px';
            var _this = $(this);
            megaMenu(leftOffset, _this);
            $(".navbar-collapse").hide();
            $(".navbar-toggler").removeClass("xs-hover-menu");
            $(".icon-menu").removeClass("icon-cross text-white");
        });
    }
}

$(document).ready(function(){
    mobileDesktopMenu();
    $(window).resize(function(){
        mobileDesktopMenu();
    });
});