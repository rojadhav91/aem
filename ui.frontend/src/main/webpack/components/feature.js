$(document).ready(function(){
    $('.feature-container').each(function(){
        var numOfCards = $(this).find('.feature-card').length;
        if(numOfCards  > 3){
            $(this).removeClass("feature-flex");
        }else{
            $(this).addClass("feature-flex");
        }
    })
    $('.col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8').find('.feature-container').removeClass("feature-flex");
});