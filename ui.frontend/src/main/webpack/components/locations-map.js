/** Hide a DOM element. */
function hideElement(el) {
    el.style.display = 'none';
}

/** Show a DOM element that has been hidden. */
function showElement(el) {
    el.style.display = 'block';
}

/** Initialize Place Details service and UI for the locator. */
function initializeDetails(locator) {
    const panelDetailsEl = document.getElementById('locations-panel-details');
    // const detailsService = new google.maps.places.PlacesService(this.map);
    var checkTemplateDetail = document.getElementById('locator-details-tmpl');
    if (checkTemplateDetail) {
        const detailsTemplate = Handlebars.compile(document.getElementById('locator-details-tmpl').innerHTML);
    }

    function hideDetails() {
        showElement(this.panelListEl);
        hideElement(panelDetailsEl);
    };

    function renderDetails(context) {
        panelDetailsEl.innerHTML = detailsTemplate(context);
        panelDetailsEl.querySelector('.back-button')
            .addEventListener('click', hideDetails);
    };


    this.showDetails = function (locationIndex) {
        const location = this.locations[locationIndex];
        const context = { location };

        // Helper function to create a fixed-size array.
        function initArray(arraySize) {
            const array = [];
            while (array.length < arraySize) {
                array.push(0);
            }
            return array;
        };

        if (location.placeId) {
            const request = {
                placeId: location.placeId,
                fields: [
                    'formatted_phone_number', 'website', 'opening_hours', 'url',
                    'utc_offset_minutes', 'price_level', 'rating', 'user_ratings_total'
                ]
            };
            // detailsService.getDetails(request, function (place, status) {
            //     if (status == google.maps.places.PlacesServiceStatus.OK) {
            //         if (place.opening_hours) {
            //             const daysHours =
            //                 place.opening_hours.weekday_text.map(e => e.split(/\:\s+/))
            //                     .map(e => ({ 'days': e[0].substr(0, 3), 'hours': e[1] }));

            //             for (let i = 1; i < daysHours.length; i++) {
            //                 if (daysHours[i - 1].hours === daysHours[i].hours) {
            //                     if (daysHours[i - 1].days.indexOf('-') !== -1) {
            //                         daysHours[i - 1].days =
            //                             daysHours[i - 1].days.replace(/\w+$/, daysHours[i].days);
            //                     } else {
            //                         daysHours[i - 1].days += ' - ' + daysHours[i].days;
            //                     }
            //                     daysHours.splice(i--, 1);
            //                 }
            //             }
            //             place.openingHoursSummary = daysHours;
            //         }
            //         if (place.rating) {
            //             const starsOutOfTen = Math.round(2 * place.rating);
            //             const fullStars = Math.floor(starsOutOfTen / 2);
            //             const halfStars = fullStars !== starsOutOfTen / 2 ? 1 : 0;
            //             const emptyStars = 5 - fullStars - halfStars;

            //             // Express stars as arrays to make iterating in Handlebars easy.
            //             place.fullStarIcons = initArray(fullStars);
            //             place.halfStarIcons = initArray(halfStars);
            //             place.emptyStarIcons = initArray(emptyStars);
            //         }
            //         if (place.price_level) {
            //             place.dollarSigns = initArray(place.price_level);
            //         }
            //         if (place.website) {
            //             const url = new URL(place.website);
            //             place.websiteDomain = url.hostname;
            //         }

            //         context.place = place;
            //         renderDetails(context);
            //     }
            // });
        }
        renderDetails(context);
        hideElement(locator.panelListEl);
        showElement(panelDetailsEl);
    };
}


function initMap(jsonLocations) {

    const CONFIGURATION = {
        "locations":jsonLocations,
        /* [
            { "title": "Kemin Industries Inc","image": "https://picsum.photos/200", "address1": "1900 Scott Ave", "address2": "Des Moines, IA 50317, USA", "coords": { "lat": 41.583704217932784, "lng": -93.5839396067459 }, "placeId": "ChIJ5VVhkICX7ocR4_25qM9JaQI", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "K-3 \u0026 K2A", "address2": "Sipcot Industrial Estate, Anthoni Pillai Nagar, Gummidipoondi, Tamil Nadu 601201, India", "coords": { "lat": 13.416143665772774, "lng": 80.09867906441801 }, "placeId": "ChIJ_W34jN-BTToRsu1mPbsnZ9k", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "12 Senoko Dr", "address2": "Singapore 758200", "coords": { "lat": 1.4575399714649706, "lng": 103.7974205165329 }, "placeId": "ChIJ1zPb4z8T2jER5h22iAnm_JQ", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Via Della Tecnica", "address2": "11, 37040 Veronella VR, Italy", "coords": { "lat": 45.331522381344655, "lng": 11.3362750072708 }, "placeId": "ChIJLy3bq7oVf0cR4CSRWsdrYks", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Food Technologies - Kemin Nutrisurance", "image": "https://picsum.photos/200", "address1": "Atealaan 4i", "address2": "2200 Herentals, Belgium", "coords": { "lat": 51.155886462220614, "lng": 4.8037598932540915 }, "placeId": "ChIJUcuj0mBRwUcRS41V_Ep_4so", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "BR-282 KM 475 - Colina", "address2": "VargeÃ£o - SC, 89690-000, Brazil", "coords": { "lat": -26.880418144847756, "lng": -52.16028956071319 }, "placeId": "ChIJEy6bGDbz5JQRqg2qRDRlVTk","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin AmÃ©rica do Sul", "image": "https://picsum.photos/200", "address1": "Rua Krebsfer", "address2": "736 - Macuco, Valinhos - SP, 13279-450, Brazil", "coords": { "lat": -22.99771726748451, "lng": -47.0449410067459 }, "placeId": "ChIJT3UMC4tMz5QR_a548ZBw0m0","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Central Europe s.r.o. / Agrifoods", "image": "https://picsum.photos/200", "address1": "Vrazska 1418 15300", "address2": "5 Praha-Praha 16, Czechia", "coords": { "lat": 49.98595217054258, "lng": 14.364716406745902 }, "placeId": "ChIJz-AnIc6XC0cRQk-PEzuQS3E","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "875 Rue Saint-BenoÃ®t", "address2": "QuÃ©bec, QC J3H 0L6, Canada", "coords": { "lat": 46.85328806199383, "lng": -71.51215076441801 }, "placeId": "ChIJ0TmZYMyfuEwRQI1F-wG_15o" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "No. C3", "address2": "1st St, Ambattur Industrial Estate, Chennai, Tamil Nadu 600058, India", "coords": { "lat": 13.09692230860934, "lng": 80.17112563558197 }, "placeId": "ChIJp91g8D5jUjoRLDeD3ViunV8","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "2 Rue Crucy", "address2": "44000 Nantes, France", "coords": { "lat": 47.214856491937724, "lng": -1.5466361644180204 }, "placeId": "ChIJH4Y-NWTvBUgRlcVnxO6delI","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Deutschland GmbH", "image": "https://picsum.photos/200", "address1": "SpeditionstraÃŸe 21", "address2": "40221 DÃ¼sseldorf, Germany", "coords": { "lat": 51.21670034077025, "lng": 6.75300543558198 }, "placeId": "ChIJM8d7FQvKuEcR_zWj-T_BC0s","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Hellas SA", "image": "https://picsum.photos/200", "address1": "19", "address2": "Ipsilantou Str, Pallini 153 51, Greece", "coords": { "lat": 38.001837623054826, "lng": 23.881880135581948 }, "placeId": "ChIJTX1MygGboRQRmjz_HDHxFs0","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Budapest", "address2": "FehÃ©rvÃ¡ri Ãºt 50-52, 1117 Hungary", "coords": { "lat": 47.469303210888455, "lng": 19.04512643558199 }, "placeId": "ChIJo1vzFejeQUcRI2SK_sV9foU","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Nutrisurance Europe Srl", "image": "https://picsum.photos/200", "address1": "Via Della Tecnica", "address2": "11, 37040 Veronella VR, Italy", "coords": { "lat": 45.33152839250726, "lng": 11.336263506745906 }, "placeId": "ChIJS_GOG1oVf0cR2GCJeDnw1Jk","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "6th Floor Zone C", "address2": "Graha MIR, Jl. Pemuda No.9, RT.1/RW.3, Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13220, Indonesia", "coords": { "lat": -6.192577334418858, "lng": 106.88052826441803 }, "placeId": "ChIJh89Fu-X1aS4RIZm7o92NxeM","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Via Giacomo Leopardi", "address2": "2/C, 42025 Cavriago RE, Italy", "coords": { "lat": 44.69666986234323, "lng": 10.511377066270441 }, "placeId": "ChIJb5JxioMagEcRUO9ClOEch4s","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Adam Plaza Khalda Wasfi Al Tal St Building No.293 4th floor", "address2": "Office # 403 \u0026404 Amman, Amman 11185, Jordan", "coords": { "lat": 31.99683282685983, "lng": 35.845359306745934 }, "placeId": "ChIJMX8lLaihHBURrYSGaRZj3TI","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Tancheonsang-ro 164", "address2": "719 Sigma 2 C, êµ¬ë¯¸ë™ ë¶„ë‹¹êµ¬ ì„±ë‚¨ì‹œ ê²½ê¸°ë„ South Korea", "coords": { "lat": 37.34394438604765, "lng": 127.10572726441804 }, "placeId": "ChIJv1wS4_JZezURMbjSThT5gUY","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "23", "address2": "Jalan USJ 4/6a, Usj 2, 47600 Subang Jaya, Selangor, Malaysia", "coords": { "lat": 3.051719944850997, "lng": 101.57463063558195 }, "placeId": "ChIJl1xXa-dNzDERuMeYjd_U9Zw","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "177 Co Rd 37", "address2": "Sarcoxie, MO 64862, USA", "coords": { "lat": 37.051996063967046, "lng": -94.12733612209014 }, "placeId": "ChIJlRCy_2CXyIcRrdc6A7lZWlk" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Ulitsa Letnikovskaya House 10", "address2": "Building 4, ÐœÐ¾ÑÐºÐ²Ð°, Russia, 115114", "coords": { "lat": 55.72599513496705, "lng": 37.644366735581954 }, "placeId": "ChIJFULeBrdLtUYREQlWLsgJ8D8","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Nutrisurance", "image": "https://picsum.photos/200", "address1": "519 N 3rd St", "address2": "Verona, MO 65769, USA", "coords": { "lat": 36.968400425859556, "lng": -93.79636846256561 }, "placeId": "ChIJtzzm4G-0yIcRSZB7EG8y3hU" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "8 Marsden Bay Drive", "address2": "RuakÄkÄ 0118, New Zealand", "coords": { "lat": -35.84605046520522, "lng": 174.47813776256564 }, "placeId": "ChIJI6eMUfacDG0RseFabbAwFBs" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Unit 1908", "address2": "Medical Plaza Ortigas, No. 25 San Miguel Ave, Ortigas Center, Pasig, Metro Manila, Philippines", "coords": { "lat": 14.580384108798432, "lng": 121.059403664418 }, "placeId": "ChIJLydNM-fHlzMROSCPrifS674","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Poland Sp. z o.o.", "image": "https://picsum.photos/200", "address1": "PuÅ‚awska 303", "address2": "02-785 Warszawa, Poland", "coords": { "lat": 52.15609970758678, "lng": 21.018918635581965 }, "placeId": "ChIJHwFr0_IyGUcRrzwv_G2Dp0k","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Campo Grande 35", "address2": "1700-087 Lisboa, Portugal", "coords": { "lat": 38.750144638847274, "lng": -9.150391593254094 }, "placeId": "ChIJvUYnf6UzGQ0RHC65Oub6QoY","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Str. Doamna Ghica nr. 32 B Bloc T 3", "address2": "Scara 1, etaj 7 Ap. 712, BucureÈ™ti 022838, Romania", "coords": { "lat": 44.45541751547133, "lng": 26.14277863558197 }, "placeId": "ChIJhXSS1lj_sUARdlewOefmRO0","details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "21 Industrial Estate", "address2": "5 Purlin St N, Olifantsfontein, 1666, Sub-Saharan Africa", "coords": { "lat": -25.964143461225397, "lng": 28.251646864418014 }, "placeId": "ChIJq8Ye7HVplR4RUNYKaZ2vtwE", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati"},
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Rambla Nova", "address2": "114, 3-2, 43001 Tarragona, Spain", "coords": { "lat": 41.11805499439422, "lng": 1.2470912644180387 }, "placeId": "ChIJDwNEL-f9oxIRV98rW1TLkvI" , "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati"},
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "No. 2", "address2": "Da Zern St, Yuanlin City, Changhua County, Taiwan 51050", "coords": { "lat": 23.959615974892163, "lng": 120.585462035582 }, "placeId": "ChIJU8IFY383aTQRLld5BlTO8OY", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "Ratchadapisek Road 444", "address2": "Olympia Thai Tower, 8th floor, Khwaeng Samsen Nok, Khet Huai Khwang, Krung Thep Maha Nakhon 10320, Thailand", "coords": { "lat": 13.800423657366775, "lng": 100.5751779067459 }, "placeId": "ChIJDRqHscqd4jARYiHdhGqyv3I", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "AtatÃ¼rk Mah.Sedef Cad.No:2 AtaÅŸehir Residence B-Blok", "address2": "AtaÅŸehir AtatÃ¼rk, D:1, 34758 AtaÅŸehir/Ä°stanbul, Turkey", "coords": { "lat": 40.992022818671465, "lng": 29.115446464418046 }, "placeId": "ChIJLYGMBWTPyhQR6k6EwKWbXDM", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Industries", "image": "https://picsum.photos/200", "address1": "34 Botanic Rd", "address2": "Southport PR9 7NG, UK", "coords": { "lat": 53.65834579737635, "lng": -2.964708908598337 }, "placeId": "ChIJ7QyiXEc_e0gRA6C2dVSnPAU", "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati" },
            { "title": "Kemin Agrifoods Europa GB", "image": "https://picsum.photos/200", "address1": "Tudor House", "address2": "Hampton Rd, Southport PR8 6QD, UK", "coords": { "lat": 53.638903176371414, "lng": -2.9968360355819668 }, "placeId": "ChIJ6x7Y48E-e0gRwwscbuQjpbs" , "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati"},
            { "title": "CÃ´ng Ty Tnhh Kemin Industries (Viá»‡t Nam)", "image": "https://picsum.photos/200", "address1": "1st Floor HB Tower 669-671", "address2": "2 Äiá»‡n BiÃªn Phá»§, St, BÃ¬nh Tháº¡nh, ThÃ nh phá»‘ Há»“ ChÃ­ Minh, Vietnam", "coords": { "lat": 10.799154737963356, "lng": 106.72206483558197 }, "placeId": "ChIJDcw-8rYodTERFFh1cQS1kEU" , "details": "Lorem ipsum dolor sit ame Aenean massa. Cum sociis natoque penati"}
        ]*/
         "mapOptions": {"center":{"lat":38.0,"lng":-100.0},"fullscreenControl":true,"mapTypeControl":false,"streetViewControl":false,"zoom":4, mapId: "b180cff60f8f0e00", "zoomControl":true,"maxZoom":17},
         "mapsApiKey": "AIzaSyCuWbxjvTD_bV865l8QvB8ye-27aHtQFFU"
    };




    this.locations = CONFIGURATION.locations || [];
    this.capabilities = CONFIGURATION.capabilities || {};

    const mapEl = document.getElementById('map');
    const panelEl = document.getElementById('locations-panel');
    this.panelListEl = document.getElementById('locations-panel-list');
    //const sectionNameEl = document.getElementById('location-results-section-name');
    const resultsContainerEl = document.getElementById('location-results-list');

    const itemsTemplate = Handlebars.compile(
        document.getElementById('locator-result-items-tmpl').innerHTML);

    this.selectedLocationIdx = null;
    this.userCountry = null;

    // Initialize the map
    this.map = new google.maps.Map(mapEl, CONFIGURATION.mapOptions);


    // Render the results list
    function getResultIndex(elem) {
        return parseInt(elem.getAttribute('data-location-index'));
    };


    // Store selection.
    function selectResultItem(locationIdx, panToMarker, scrollToResult) {

        this.selectedLocationIdx = locationIdx;
        for (const locationElem of resultsContainerEl.children) {
            locationElem.classList.remove('selected');
            if (getResultIndex(locationElem) === this.selectedLocationIdx) {
                locationElem.classList.add('selected');
                if (scrollToResult) {

                    panelEl.scrollTop = locationElem.offsetTop;
                    //  $( "li.selected" ).scrollTop( locationElem.offsetTop );
                    //$('#location-results-list').scrollTop($('#location-results-list li:nth-child(4)').position().top);
                }
            }
        }
        if (panToMarker && (locationIdx != null)) {
            this.map.panTo(this.locations[locationIdx].coords);
            map.setZoom(10);
        }
    };


    // Create a marker for each location.
    var infoWindow = new google.maps.InfoWindow();

    const markers = this.locations.map(function (location, index) {
        var title = location.title;
        var image = location.image;
        var address1 = location.address1;
        var address2 = location.address2;
        var lat = location.coords.lat;
        var lng = location.coords.lng;
        var windowContent = '<div class="marker-contianer"><div class="title">' + title + '</div><div class="info-image"><img src="'+image+'"/></div><div class="address-one">' + address1 + '</div><div class="address-two">' + address2 + '</div><div class="lat-Lng"><span class="icon-location">' +lat+ ' , ' +lng+ '</span></div>';

        const marker = new google.maps.Marker({
            position: location.coords,
            map: this.map,
            title: location.title,
        });

        //Attach click event to the marker.
        (function (marker) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent(windowContent);
                infoWindow.open(map, marker);
                map.setZoom(10);
                const panPosition = { lat: (location.coords.lat + 5), lng: location.coords.lng };
                map.panTo(panPosition);
                selectResultItem(index, false, true);
            });
        })(marker);

        return marker;
    });

    google.maps.event.addListener(map, "click", function (e) {
        infoWindow.close();
    });

    // Fit map to marker bounds.
    this.updateBounds = function () {
        const bounds = new google.maps.LatLngBounds();
        for (let i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        this.map.fitBounds(bounds);
    };
    if (this.locations.length) {
        this.updateBounds();
    }

    this.renderResultsList = function () {
        const locations = this.locations.slice();
        for (let i = 0; i < locations.length; i++) {
            locations[i].index = i;
        }
        //sectionNameEl.textContent = `Regional Headquarters (${locations.length})`;
        const resultItemContext = { locations: locations };
        resultsContainerEl.innerHTML = itemsTemplate(resultItemContext);
        for (const item of resultsContainerEl.children) {
            const resultIndex = getResultIndex(item);
            if (resultIndex === this.selectedLocationIdx) {
                item.classList.add('selected');
            }

            function resultSelectionHandler() {
                selectResultItem(resultIndex, true, false);
            };

            // Clicking anywhere on the item selects this location.
            // Additionally, create a button element to make this behavior
            // accessible under tab navigation.
            item.addEventListener('click', resultSelectionHandler);
            item.querySelector('.select-location')
                .addEventListener('click', function (e) {
                    resultSelectionHandler();
                    e.stopPropagation();
                });

            item.querySelector('.details-button')
                .addEventListener('click', function () {
                    //   this.showDetails(resultIndex);
                });
        }
    };

    // Optional capability initialization 
    initializeDetails(this);

    // Initial render of results 
    this.renderResultsList();


    $(document).ready(function(){
        $(".details-button").click(function(){
            $(this).next(".details").toggle();
          });
    });
}

var creatorData=[

          {"title":"Kemin Industries Inc Headquarters","address1":"1900 Scott Ave","address2":"Des Moines, IA 50317, USA","coords":{"lat":41.583704217932784,"lng":-93.58396106441802},"placeId":"ChIJ5VVhkICX7ocR4_25qM9JaQI"},
          {"title":"USA (Des Moines, Iowa) - Kemin AgriFoods","address1":"Buildind","address2":"2100 Maury St #2, Des Moines, IA 50317, USA","coords":{"lat":41.58123441108445,"lng":-93.58023026441802},"placeId":"ChIJRzJNwICX7ocRlnHYhhBd2vY"},
          {"title":"USA (Des Moines, Iowa) - Kemin Health","address1":"2111 E 17th St","address2":"Des Moines, IA 50316, USA","coords":{"lat":41.61357065500562,"lng":-93.59052767790985},"placeId":"ChIJc9QIaeWZ7ocRtJS8mF5ApPg"},
          {"title":"USA (Des Moines, Iowa) - Kemin Foods #2 Warehouse","address1":"205 E 18th St","address2":"Des Moines, IA 50316, USA","coords":{"lat":41.58940239695995,"lng":-93.58587499325407},"placeId":"ChIJ64WADtSZ7ocR2LnlyeQXh_E"},
          {"title":"USA (Verona, Missouri) - Kemin Nutrisurance","address1":"519 N 3rd St","address2":"Verona, MO 65769, USA","coords":{"lat":36.96840096160887,"lng":-93.79637919140167},"placeId":"ChIJtzzm4G-0yIcRSZB7EG8y3hU"},
          {"title":"USA (Sarcoxie, Missouri) - Kemin Nutrisurance","address1":"177 Co Rd 37","address2":"Sarcoxie, MO 64862, USA","coords":{"lat":37.05199833836888,"lng":-94.12735046441802},"placeId":"ChIJV2Q3oWGXyIcR1U73aGn6bFk"},
          {"title":"Canada","address1":"875 Chem. Benoît","address2":"Mont-Saint-Hilaire, QC J3G 4S6, Canada","coords":{"lat":45.587245467759864,"lng":-73.14765943558196},"placeId":"ChIJvzDZfL5UyEwRk2C1OV0KJuw"},
          {"title":"USA (California)","address1":"30133 Miller Rd","address2":"Valley Center, CA 92082, USA","coords":{"lat":33.25510184445606,"lng":-117.02291649325409},"placeId":"ChIJt4ZUV3qO24ARGkA2VR52YNw"},
          {"title":"Argentina","address1":"10th Floor","address2":"Paraná 783, C1017 AAO, Buenos Aires, Argentina","coords":{"lat":-34.599647385901584,"lng":-58.388052935581975},"placeId":"ChIJF9NQlf41o5URMz2eYXxT8Pc"},
          {"title":"Brazil (Vargeão)","address1":"BR-282 KM 475 - Colina","address2":"Vargeão - SC, 89690-000, Brazil","coords":{"lat":-26.88040349138469,"lng":-52.16031101838531},"placeId":"ChIJEy6bGDbz5JQRqg2qRDRlVTk"},
          {"title":"Brazil (Valinhos)","address1":"Rua Krebsfer","address2":"736 - Macuco, Valinhos - SP, 13279-450, Brazil","coords":{"lat":-22.99769319445772,"lng":-47.0449410067459},"placeId":"ChIJT3UMC4tMz5QR_a548ZBw0m0"},
          {"title":"Belgium","address1":"Toekomstlaan 42","address2":"2200 Herentals, Belgium","coords":{"lat":51.165678397012115,"lng":4.795413593254083},"placeId":"ChIJ0QJfWt9TwUcRPpwsM8d8C3w"},
          {"title":"Belgium - Kemin Food Technologies - Kemin Nutrisurance","address1":"Atealaan 4i","address2":"2200 Herentals, Belgium","coords":{"lat":51.155799193276536,"lng":4.803716977909853},"placeId":"ChIJUcuj0mBRwUcRS41V_Ep_4so"},
          {"title":"Germany","address1":"Speditionstraße 21","address2":"40221 Düsseldorf, Germany","coords":{"lat":51.216725331849865,"lng":6.75300543558198},"placeId":"ChIJM8d7FQvKuEcR_zWj-T_BC0s"},
          {"title":"France","address1":"2 Rue Crucy","address2":"44000 Nantes, France","coords":{"lat":47.21487220576453,"lng":-1.5466147067459013},"placeId":"ChIJH4Y-NWTvBUgRlcVnxO6delI"},
          {"title":"Czechia","address1":"Vrazska 1418 15300","address2":"5 Praha-Praha 16, Czechia","coords":{"lat":49.98598687799319,"lng":14.364737864418021},"placeId":"ChIJz-AnIc6XC0cRQk-PEzuQS3E"},
          {"title":"Denmark","address1":"Gelsåvej 26","address2":"6500 Vojens, Denmark","coords":{"lat":55.198010685098346,"lng":9.277652335581976},"placeId":"ChIJpWJNaJZVS0YR2GdWOC882BE"},
          {"title":"Greece","address1":"19","address2":"Ipsilantou Str, Pallini 153 51, Greece","coords":{"lat":38.00181965782168,"lng":23.881901593254067},"placeId":"ChIJTX1MygGboRQRmjz_HDHxFs0"},
          {"title":"Hungary","address1":"Budapest","address2":"Fehérvári út 50-52, 1117 Hungary","coords":{"lat":47.469331314448134,"lng":19.04513716441805},"placeId":"ChIJo1vzFejeQUcRI2SK_sV9foU"},
          {"title":"Italy (Veronella)","address1":"Via Della Tecnica","address2":"11, 37040 Veronella VR, Italy","coords":{"lat":45.33153581626032,"lng":11.33626427843474},"placeId":"ChIJLy3bq7oVf0cR4CSRWsdrYks"},
          {"title":"Italy (Cavriago)","address1":"Via Giacomo Leopardi","address2":"2/C, 42025 Cavriago RE, Italy","coords":{"lat":44.696678680471216,"lng":10.511377066270441},"placeId":"ChIJb5JxioMagEcRUO9ClOEch4s"},
          {"title":"Russia (Lipetsk)","address1":"территория ОЭЗ ППТ «Липецк»","address2":"здание 18, Kazinka, Lipetskaya oblast\u0027, Russia, 399071","coords":{"lat":52.514146470514206,"lng":39.79934159325406},"placeId":"ChIJYSgRn7c4OkERIMGYoEeKPGs"},
          {"title":"Russia","address1":"Ulitsa Letnikovskaya House 10","address2":"Building 4, Москва, Russia, 115114","coords":{"lat":55.726001176923376,"lng":37.644377464418014},"placeId":"ChIJFULeBrdLtUYREQlWLsgJ8D8"},
          {"title":"Poland","address1":"Puławska 303","address2":"02-785 Warszawa, Poland","coords":{"lat":52.15609209683316,"lng":21.018897177909846},"placeId":"ChIJHwFr0_IyGUcRrzwv_G2Dp0k"},
          {"title":"Romania","address1":"Str. Doamna Ghica nr. 32 B Bloc T 3","address2":"Scara 1, etaj 7 Ap. 712, București 022838, Romania","coords":{"lat":44.45542613094138,"lng":26.14280009325409},"placeId":"ChIJhXSS1lj_sUARdlewOefmRO0"},
          {"title":"Spain","address1":"Rambla Nova","address2":"114, 3-2, 43001 Tarragona, Spain","coords":{"lat":41.11806636059897,"lng":1.2471019932540983},"placeId":"ChIJDwNEL-f9oxIRV98rW1TLkvI"},
          {"title":"Turkey","address1":"Atatürk Mah.Sedef Cad.No:2 Ataşehir Residence B-Blok","address2":"Ataşehir Atatürk, D:1, 34758 Ataşehir/İstanbul, Turkey","coords":{"lat":40.99201446746894,"lng":29.115425006745927},"placeId":"ChIJLYGMBWTPyhQR6k6EwKWbXDM"},
          {"title":"United Kingdom","address1":"34 Botanic Rd","address2":"Southport PR9 7NG, UK","coords":{"lat":53.65834559869209,"lng":-2.9646445355819795},"placeId":"ChIJ7QyiXEc_e0gRA6C2dVSnPAU"},
          {"title":"United Kingdom - Kemin Agrifoods","address1":"Tudor House","address2":"Hampton Rd, Southport PR8 6QD, UK","coords":{"lat":53.63889025593546,"lng":-2.9968467644180263},"placeId":"ChIJ6x7Y48E-e0gRwwscbuQjpbs"},
          {"title":"India","address1":"K-3 & K2A","address2":"Sipcot Industrial Estate, Anthoni Pillai Nagar, Gummidipoondi, Tamil Nadu 601201, India","coords":{"lat":13.416124098174585,"lng":80.09867906441801},"placeId":"ChIJ_W34jN-BTToRsu1mPbsnZ9k"},
          {"title":"Indonesia","address1":"6th Floor Zone C","address2":"Graha MIR, Jl. Pemuda No.9, RT.1/RW.3, Rawamangun, Kec. Pulo Gadung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13220, Indonesia","coords":{"lat":-6.192537002724797,"lng":106.88051753558197},"placeId":"ChIJh89Fu-X1aS4RIZm7o92NxeM"},
          {"title":"Japan","address1":"Japan","address2":"〒102-0076 Tokyo, Chiyoda City, Gobanchō, 12−番地 五番町Kビル 4階","coords":{"lat":35.68957125540933,"lng":139.73376323558196},"placeId":"ChIJAQCsT2CMGGARo1hP6BzEu6I"},
          {"title":"South Korea","address1":"Tancheonsang-ro 164","address2":"719 Sigma 2 C, 구미동 분당구 성남시 경기도 South Korea","coords":{"lat":37.34395344865734,"lng":127.10571653558198},"placeId":"ChIJv1wS4_JZezURMbjSThT5gUY"},
          {"title":"Malaysia","address1":"23","address2":"Jalan USJ 4/6a, Usj 2, 47600 Subang Jaya, Selangor, Malaysia","coords":{"lat":3.0516991872094477,"lng":101.57463063558195},"placeId":"ChIJl1xXa-dNzDERuMeYjd_U9Zw"},
          {"title":"Philippines","address1":"Unit 1908 Medcal Plaza","address2":"San Miguel Avenue, Ortigas Center, Barangay San Antonio, San Antonio, Pasig, 1605 Metro Manila, Philippines","coords":{"lat":14.58334627191013,"lng":121.06027223558196},"placeId":"ChIJyxjpJhTIlzMRKUFN5xZIqZM"},
          {"title":"Singapore","address1":"12 Senoko Dr","address2":"Singapore 758200","coords":{"lat":1.4575262295913542,"lng":103.7974205165329},"placeId":"ChIJ1zPb4z8T2jER5h22iAnm_JQ"},
          {"title":"Taiwan","address1":"No. 2","address2":"Da Zern St, Yuanlin City, Changhua County, Taiwan 51050","coords":{"lat":23.95959943004579,"lng":120.58547276441806},"placeId":"ChIJU8IFY383aTQRLld5BlTO8OY"},
          {"title":"Vietnam","address1":"1st Floor HB Tower 669-671","address2":"2 Điện Biên Phủ, St, Bình Thạnh, Thành phố Hồ Chí Minh, Vietnam","coords":{"lat":10.799159348700798,"lng":106.72212920859833},"placeId":"ChIJDcw-8rYodTERFFh1cQS1kEU"},
          {"title":"Jordan","address1":"Adam Plaza Khalda Wasfi Al Tal St Building No.293 4th floor","address2":"Office # 403 & 404 Amman, Amman 11185, Jordan","coords":{"lat":31.996769987675805,"lng":35.84538076441805},"placeId":"ChIJMX8lLaihHBURrYSGaRZj3TI"},
          {"title":"New Zealand","address1":"8 Marsden Bay Drive","address2":"Ruakākā 0118, New Zealand","coords":{"lat":-35.84610699388965,"lng":174.47819140674594},"placeId":"ChIJI6eMUfacDG0RseFabbAwFBs"},
          {"title":"South Africa","address1":"21 Industrial Estate","address2":"6 Purlin St N, Olifantsfontein, 1666, South Africa","coords":{"lat":-25.96392667845014,"lng":28.250527877909843},"placeId":"ChIJq8Ye7HVplR4RUNYKaZ2vtwE"},
          {"title":"Australia","address1":"694 Pacific Hwy Suites 6-7","address2":"Killara NSW 2071, Australia","coords":{"lat":-33.76309590498486,"lng":151.15498364907378},"placeId":"ChIJ2Y-E7KupEmsRrd8MIDgHe7w"}

]

var checkMapPresent = document.getElementById('map-container');


$(document).ready(function () {

 var pathResourceLoc=document.getElementById('locatorResourcePath');
    if(pathResourceLoc){
        var path= pathResourceLoc.value;
        var url = path+'.locations.json';
        $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
              if (checkMapPresent) {
                  initMap(data);
              }
            },
        error: function(data) {
                    var fallBack=[{ "title": "Not Found","image": "", "address1":"", "address2": "", "coords": { "lat":0, "lng":0 }, "placeId": "ChIJ5VVhkICX7ocR4_25qM9JaQI", "details": "" }]
                    initMap(fallBack);
                }
        });

       }

});




