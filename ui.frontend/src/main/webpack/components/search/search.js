$(document).ready(function () {
    $('.input-box .close').css({ visibility: 'hidden' });                 
    $('.parentBlock .input-box').hide();        
    $('.search-icon-placeholder').hide('');
    $('#searchBox').click(function () {
        if ($(window).width() <= 1024) {
            $('.left-navigation-extra .input-box').hide();
            $('.parentBlock .input-box').show();

        }else{            
            $('.left-navigation-extra .input-box').show();            
            $('.parentBlock .input-box').hide();
        }
        $('.search-icon-placeholder').show('');
        $('.input-box .close').css({ visibility: 'hidden' });
        arr = this.className.split(" ");
        if (arr.indexOf("active") == -1) {
            this.className += " active";    
        }        
        this.getElementsByTagName("INPUT")[0].focus();
    });

    //close searchbox
    $(window).click(function (event) {
        var $target = $(event.target);
        if (!$target.closest('#searchBox').length) {
            $('#searchBox').removeClass('active');
            $('#searchBox input').removeClass('active');
            $('#searchBox input').removeClass('error');
            $('#searchBox input').removeClass('completed');
            $('#searchBox input').val('');
            $('.search-icon-placeholder').hide('');
        }
    });

    $('#searchBox input').change(function () {
        $(this).removeClass('active');
        $(this).addClass('completed');
    })
    $('#searchBox input').keyup(function () {
        $('.input-box .close').css({ visibility: 'hidden' });
        $(this).addClass('active');
        $(this).removeClass('completed');
        var text = $(this).val();
        if (text.length > 0) {
            $('.input-box .close').css({ visibility: 'visible' });
        }
    })
    $('#searchBox input').keypress(function (e) {
        var key = e.which;
        if (key == 13) {
            var parameter = $(this).val();
            var url = $('.left-navigation-extra #searchBox').attr("data-link")+"?q=" + parameter;
            window.open(url, '_blank');
        }
    });
    $('#searchBox .close').click(function (e) {
        $('#searchBox input').val('');
        $('.input-box .close').css({ visibility: 'hidden' });
    });

});