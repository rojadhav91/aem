function binCTAAttributes(linkElements) {

    var dataNavStr = undefined, dataAtStr = undefined, dataCTAStr = undefined;

    function contains(s1, s2) {
        if (s1 && s2) {
            var arr = s1.split(s2);
            return arr.length > 1;
        }
        return false;
    }
    function getExt(s) {
        var arr = s.split('.'), ext = arr[arr.length-1];
        return ext;
    }

    function containFile(s) {
        var ext = getExt(s);
        if (ext) {
            var exts = ['pdf', 'docx', 'xlsx', 'doc', 'xls', 'exe'];
            for (var i in exts) {
                if (exts.hasOwnProperty(i) && exts[i] === s) {
                    return true;
                }
            }
        }
        return false;
    }

    function getFilename(s) {
        var arr = s.split('/'), filename = arr[arr.length-1];
        return filename;
    }

    function isButton(s) {
        if (s) {
            var arr = s.split("btn");
            if (arr.length > 1) {return true;}
            arr = s.split("button");
            return arr.length > 1;
        }
        return false;
    }

    function isValidLink(s) {
        if (s) {
            var arr = s.split("javascript:");
            if (arr.length > 1) {return false;}
            arr = s.split("#");
            return arr.length === 1;
        }
        return false;
    }

    function dataNav(e) {
        var c = e.attr('class');
        if (contains(c, 'footer')) {
            dataNavStr = 'Footer';
        }
        else if (contains(c, 'header')) {
            dataNavStr = 'Header';
        }
        else if (contains(c, 'carousel') ||
            contains(c, 'banner') ||
            contains(c, 'accordion') ||
            contains(c, 'tile')) {
            dataNavStr = 'Banner';
        }
        if (typeof e[0] !== "undefined" && typeof e[0].tagName !== "undefined") {
            if (e[0].tagName.toUpperCase() !== 'BODY') {
                dataNav(e.parent());
            }
        }
    }
    function dataAt(e) {
        var c = e.attr('class');
        if (contains(c, 'fdn-text-cta')) {
            if (e.find('h1').length) {
                dataAtStr = e.find('h1').text();
            }
            else if (e.find('h2').length) {
                dataAtStr = e.find('h2').text();
            }
            else if (e.find('h3').length) {
                dataAtStr = e.find('h3').text();
            }
            else if (e.find('h4').length) {
                dataAtStr = e.find('h4').text();
            }
        }
        if (typeof e[0] !== "undefined" && typeof e[0].tagName !== "undefined") {
            if (e[0].tagName.toUpperCase() !== 'BODY') {
                dataNav(e.parent());
            }
        }
    }

    linkElements.each(function(){
        var $t = $(this), s = $t.attr('href');
        if (isValidLink(s)) {
            dataNav($t);
            dataAt($t);
            if (!$t.attr('data-cta')) {
                if (s && containFile(s)) {
                    $t.attr('data-cta', getFilename(s));
                    $t.attr('data-at', getExt(s));
                    $t.attr('data-nav', dataNavStr||'Body');
                    $t.attr('data-cta-type', isButton($t.attr('class')) ? 'button' : ($t.find('img').length?'image':'link'));
                }
                else {
                    $t.attr('data-cta', dataCTAStr||$t.text());
                    $t.attr('data-at', dataAtStr||$t.text());
                    $t.attr('data-nav', dataNavStr||'Body');
                    $t.attr('data-cta-type', isButton($t.attr('class')) ? 'button' : ($t.find('img').length?'image':'link'));
                }
            }
            else if ($t.attr('data-nav') !== 'Header' && $t.attr('data-nav') !== 'Hovernav') {
                if (!$t.attr('data-cta') && dataCTAStr) {
                    $t.attr('data-cta', dataCTAStr);
                }
                $t.attr('data-at', dataAtStr||$t.attr('data-at'));
                $t.attr('data-nav', dataNavStr||'Body');
                if ($t.find('img').length) {
                    $t.attr('data-cta-type', 'image');
                }
            }
            dataNavStr = undefined;
            dataAtStr = undefined;
        }
    });

    $('.fdn-rte a').each(function(){
        var $t = $(this);
        if ($t.next('b').length) {
            var s = $t.next().text();
            if (!$t.attr('data-cta')) {
                $t.attr('data-cta', s);
            }
            $t.attr('data-at', s);
            if ($t.find('img').length && !$t.attr('data-cta-type')) {
                $t.attr('data-cta-type', 'image');
            }
        }
    });

    $('.fdn-text-cta .text-center a').each(function(){
        var $t = $(this), $p = $t.parent().parent();
        var $t2 = $p.find('.fdn-rte a');
        if ($t2.next('b').length) {
            var s = $t2.next().text();
            $t.attr('data-at', s);
        }
        else if ($p.find('h1').length) {
            $t.attr('data-at', $p.find('h1').text());
        }
    });
}

$(document).ready(function(){
    binCTAAttributes(jQuery('a'));
});