$(document).ready(function () {
    $('.boost .header').parents('.experiencefragment').css('margin-top', '4rem')
    if (document.querySelector('body.day')) {
        $("#nightImage").hide();
        $("#dayImage").show();
    }
    if (document.querySelector('body.night')) {
        $("#nightImage").show();
        $("#dayImage").hide();
    }
    //$('.genesis.day .header').css({position:'relative'});
   // $('.genesis.night .header').css({position:'relative'});
});