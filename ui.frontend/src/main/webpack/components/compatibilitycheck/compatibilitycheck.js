// UI compability code start here
//var myJson = {
//    "devicesCompability": [{
//        "name": "Apple",
//        "id": "type1",
//        "modelFamily": [{
//            "name": "Iphone 11",
//            "id": "Manufacturer1",
//            "devices": [{
//                "name": "Iphone 11 Mini",
//                "id": "devices_1",
//            },
//            {
//                "name": "Iphone 11 pro",
//                "id": "devices_2",
//            },
//            {
//                "name": "Iphone 11 pro max",
//                "id": "devices_3",
//            }
//            ]
//        },
//        {
//            "name": "Iphone 12",
//            "id": "Manufacturer2",
//            "devices": [{
//                "name": "Iphone 12 Mini",
//                "id": "devices_1",
//            },
//            {
//                "name": "Iphone 12 pro",
//                "id": "devices_2",
//            },
//            {
//                "name": "Iphone 12 pro max",
//                "id": "devices_3",
//            }
//            ]
//        },
//        ]
//    },
//    {
//        "name": "Android",
//        "id": "type2",
//        "modelFamily": [{
//            "name": "Samsung Galaxy",
//            "id": "Manufacturer3",
//            "devices": [{
//                "name": "Samsung Galaxy Spica",
//                "id": "devices_1",
//            },
//            {
//                "name": "Samsung Galaxy U",
//                "id": "devices_2",
//            },
//            {
//                "name": "Samsung Galaxy Neo",
//                "id": "devices_2",
//            }
//            ]
//        },
//        {
//            "name": "Samsung Retina",
//            "id": "Manufacturer4",
//            "devices": [{
//                "name": "Samsung Retina 3",
//                "id": "divices3",
//            },
//            {
//                "name": "Samsung Retina 4",
//                "id": "divices4",
//            }
//            ]
//        }
//        ]
//    }
//    ]
//}
const myJson = window.deviceCompatibility || {};

function getDevices() {
    var devices = [];
    for (var i = 0; i < myJson.deviceCompatibility.length; i++) {
        var deviceCompatibility = myJson.deviceCompatibility[i];
        for (var j = 0; j < deviceCompatibility.modelFamily.length; j++) {
            var modelFamily = deviceCompatibility.modelFamily[j];
            for (var k = 0; k < modelFamily.devices.length; k++) {
                var device = modelFamily.devices[k];
                devices.push({
                    brandId: deviceCompatibility.id,
                    familyId: modelFamily.id,
                    familyName: modelFamily.name,
                    diviceId: device.id,
                    deviceName: device.name
                });
            }
        }
    }
    return devices;
}

$.each(myJson.deviceCompatibility, function (index, value) {
    $("#brand2").append('<li data-val="' + value.id + '"> <div class="dp-val1">' + value.name +
        '</li></div>');
    $('#modelFamilyPlaceholder').parents('.wrapper').addClass('disabled');
    $('#modelPlaceholder').parents('.wrapper').addClass('disabled');
});



// custom dropdown 
$(".selected").click(function () {
    $(this).parent().next(".custom-dropdown-list").toggleClass("open");
    if ($(this).parent().next(".custom-dropdown-list").hasClass('open')) {
        $(this).parent().addClass('active')
    } else {
        $(this).parent().removeClass('active')
    }
    $(".bottom-wrapper").remove();
    $('<div class="bottom-wrapper"></div').insertAfter('.custom-dropdown-list');
    $(this).parent().parent().toggleClass('active');
});

$('#brand2').on('click', 'li', function (event) {
    event.preventDefault();
    $('#devicetxt').css("display", "none");
    $('#modelFamilyPlaceholder').html('<div class="dp-val1">Please select Model</div>');
    $('#modelPlaceholder').html('<div class="dp-val1">Please select Device</div>');
    $('#modelFamily2').html('<li data-val="0"> <div class="dp-val1">Please select Model</div> </li>');
    $('#model2').html('<li data-val="0"> <div class="dp-val1">Please select Device</div> </li>');
    var dataVal = $(this).attr("data-val");
    var dpText1 = $(this).children('.dp-val1').text();
    $(this).parents('.custom-dropdown-list').removeClass('active');
    $(this).parents('.custom-dropdown-list').find('li').removeClass('active');
    $(this).addClass('active');
    if (dataVal == 0) {
        $(this).parent().prev().removeClass('active');
        $(this).removeClass('active');
        $('#modelFamilyPlaceholder').parents('.wrapper').addClass('disabled');
        $('#modelPlaceholder').parents('.wrapper').addClass('disabled');
    } else {
        // $(this).parent().prev().addClass('active');
        $('#modelFamilyPlaceholder').parents('.wrapper').removeClass('disabled');
        $('#modelPlaceholder').parents('.wrapper').addClass('disabled');
    }
    $(this).parent().prev().find('.selected').attr("data-val", dataVal);
    $(this).parent().prev().find('.selected').children('.dp-val1').text(dpText1);
    // $(this).parent().removeClass("open");
    $('.custom-dropdown-list').removeClass("open");
    $(".custom-dropdown").removeClass('active');

    //populate model list
    for (var i = 0; i < myJson.deviceCompatibility.length; i++) {
        if (myJson.deviceCompatibility[i].id == dataVal) {
            $.each(myJson.deviceCompatibility[i].modelFamily, function (index, value) {
                $("#modelFamily2").append('<li data-val="' + value.id + '"> <div class="dp-val1">' + value.name + '</li></div>');
            });
        }
    }
});

$('#modelFamily2').on('click', 'li', function (event) {
    event.preventDefault();
    $('#devicetxt').css("display", "none");
    var dataVal = $(this).attr("data-val");
    var brandId = $('#brand2').parents('.input-container').find('.selected').attr("data-val");
    var dpText1 = $(this).children('.dp-val1').text();
    if (dataVal == 0) {
        $(this).parent().prev().removeClass('active');
        $(this).removeClass('active');
        $('#modelPlaceholder').parents('.wrapper').addClass('disabled');
    } else {
        $('#modelPlaceholder').parents('.wrapper').removeClass('disabled');
    }
    $(this).parent().prev().find('.selected').attr("data-val", dataVal);
    $(this).parent().prev().find('.selected').children('.dp-val1').text(dpText1);
    $('.custom-dropdown-list').removeClass("open");
    $(".custom-dropdown").removeClass('active');

    $(this).parents('.custom-dropdown-list').removeClass('active');
    $(this).parents('.custom-dropdown-list').find('li').removeClass('active');
    $(this).addClass('active');
    //reset devices list based on model family selections            
    $('#modelPlaceholder').html('<div class="dp-val1">Please select Device</div>');
    $('#model2').html('<li data-val="0"> <div class="dp-val1">Please select Device</div> </li>');

    // populate model list
    var devices = getDevices();
    var familyId = dataVal;
    $.each(devices, function (index, value) {
        if (value.familyId == familyId && value.brandId == brandId) {
            $("#model2").append('<li data-val="' + value.diviceId + '"> <div class="dp-val1">' +
                value.deviceName + '</li></div>');
        }
    });
});


$('#model2').on('click', 'li', function (event) {
    event.preventDefault();
    var dataVal = $(this).attr("data-val");
    var dpText1 = $(this).children('.dp-val1').text();
    $('#devicetxt').css("display", "none");
    if (dataVal == 0) {
        $(this).parent().prev().removeClass('active');
        $(this).removeClass('active');
    } else {
        $(this).parent().prev().addClass('active');
        $('#devicetxt .sucesstxt').text($(this).parents('form').data('success-msg') || "Yeah! your device is compatible");
        $('#devicetxt').css("display", "block");
    }
    $(this).parent().prev().find('.selected').attr("data-val", dataVal);
    $(this).parent().prev().find('.selected').children('.dp-val1').text(dpText1);
    // $(this).parent().removeClass("open");
    $('.custom-dropdown-list').removeClass("open");
    $(".custom-dropdown").removeClass('active');

    $(this).parents('.custom-dropdown-list').removeClass('active');
    $(this).parents('.custom-dropdown-list').find('li').removeClass('active');
    $(this).addClass('active');
});



$('html').on('click', function (e) {
    if ($(e.target).closest('.custom-dropdown').length || $(e.target).hasClass('custom-dropdown'))
        return
    $(".custom-dropdown-list").removeClass("open");
    $(".custom-dropdown").removeClass('active');
    $(".input-container").removeClass('active');
});