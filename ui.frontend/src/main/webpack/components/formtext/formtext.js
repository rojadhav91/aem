$(document).ready(function () {
    if ($('.cmp-form-img__left').contents('svg').length == 0) {
        $('.cmp-form-img__left').siblings('.cmp-form-text__text').css({ 'padding-left': '16px' });
        $('.cmp-form-img__left').siblings('.cmp-form-text__textarea').css({ 'padding-left': '16px' });
    }
    if ($('.cmp-form-img__right').contents('svg').length == 0) {
        $('.cmp-form-img__right').siblings('.cmp-form-text__text').css({ 'padding-right': '16px' });
    }

    $(".cmp-form-text__text").focus(function () {
        $(this).siblings('.cmp-form-img__right').css('top', '39px');
        $(this).siblings('.cmp-form-img__left').css('top', '39px');
    });

    $(".cmp-form-text__text").focusout(function () {
        $(this).siblings('.cmp-form-img__right').css('top', '37.5px');
        $(this).siblings('.cmp-form-img__left').css('top', '37.5px');
    });

    if ($('.cmp-form-text__text:active')) {
        $(this).siblings('.cmp-form-img__right').css('top', '38px');
        $(this).siblings('.cmp-form-img__left').css('top', '38px');
    }

})