$(document).ready(function () {
  var timelinePresent = document.getElementById("kemin-timeline");
  if(timelinePresent){
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      responsive : {
        0 : {
           items : 1,
        },
        480 : {
           items:1,
        },
        768 : {
            items:2,
        },
        992 :{
            items:3,
        },
        1200 :{
          items:4,
      },
    },
      loop: false,
      margin: 30,
      autoplay: false,
     // autoplayTimeout: 1000,
      autoplayHoverPause: false,
      stagePadding: 50,
     rewind: false,
      nav: true,
      dots:false,
      navText: ["<div class='nav-button owl-prev'></div>", "<div class='nav-button owl-next'></div>"],
    });

    $(".kemin-timeline-slider").each(function( idx ) {
      var badgeNum = $(this).find(".badge").length;
      var r=63, g=193, b=203;
      var lr=165/badgeNum, lg=50/badgeNum, lb=40/badgeNum;
      $(this).find(".badge").each(function( index ) {
        $(this).css("background-color","rgb("+(r+index*lr)+","+(g+index*lg)+","+(b+index*lb)+")");
      });
    });

    $(".kemin-timeline-slider .timeline-group").each(function( index ) {
      var leftPad = parseInt($(this).closest('.badge').css('padding-left').replace('px',''));
      var rightMar = parseInt($(this).closest('.owl-item').css('margin-right').replace('px',''));
      var displayWidth = $(this).closest('.owl-item').width()+rightMar-leftPad;
      if($(this).prop('scrollWidth') > displayWidth){
        $(this).addClass("timeline-group-moreline");
      }
    });
    $(".kemin-timeline-slider .timeline-desc").each(function( index ) {
      if($(this).prop('scrollHeight') > $(this).height()){
        $(this).addClass("timeline-desc-moreline");
      }
    });

    $(window).on('resize', function(){
      $(".kemin-timeline-slider .timeline-group").each(function( index ) {
        var leftPad = parseInt($(this).closest('.badge').css('padding-left').replace('px',''));
        var rightMar = parseInt($(this).closest('.owl-item').css('margin-right').replace('px',''));
        var displayWidth = $(this).closest('.owl-item').width()+rightMar-leftPad;
        if($(this).prop('scrollWidth') > displayWidth){
          $(this).addClass("timeline-group-moreline");
        }
      });
      $(".kemin-timeline-slider .timeline-desc").each(function( index ) {
        if($(this).prop('scrollHeight') > $(this).height()){
          $(this).addClass("timeline-desc-moreline");
        }else{
          //$(this).removeClass("timeline-desc-moreline");
        }
      });
    });
  }
   
  //  $('.owl-item.active').last().addClass( "bcg-opacity" );

  //   $('.owl-next').click(function() {
  //     $(".owl-item.active").removeClass("bcg-opacity");
  //     $('.owl-item.active').last().addClass( "bcg-opacity" );
  // });
 
  // $('.owl-prev').click(function() {
  //   $(".owl-item.active").removeClass("bcg-opacity");
  //   $('.owl-item.active').last().addClass( "bcg-opacity" );
  // });

  });