$(document).ready(function () {
    $(document).bind('keydown', function (e) {
        if (e.keyCode === 9) {
            $(".cmp-button").focus(function () {
                $(this).addClass('keyboardFocus');
            });
            $(".cmp-button").focusout(function () {
                $(".cmp-button").removeClass('keyboardFocus');
            });
        }
    });
    $('.cmp-button').bind('mouseenter', function (e) {
        $(".cmp-button").removeClass('keyboardFocus');
    });
    $('.cmp-button').click(function () {
        $(".cmp-button").removeClass('keyboardFocus');
    });

    //sticky button for phone
    $(window).bind("load resize", function () {
        if ($(window).width() <= 767) {

            //boost sticky button
            $('#stickybutton.phoneBg').addClass('stickybuttonClass');

            //genesis sticky button
            $('#genesis-sticy-button.phoneBg').parent().addClass('genesis-sticky');
            var buttonHeight = $('#genesis-sticy-button').parent().outerHeight(true);
            $('.genesis .footer').css({ "margin-bottom": buttonHeight + "px" });
            $('#genesis-sticy-button.phoneBg').parent().css({
                "height": buttonHeight + "px",
                "margin-bottom": "0px",
                "background-color": "var(--color-brand-black)",
                "display": "flex",
                "justify-content": "center",
                "align-items": "center",
                "flex-direction": "column",
                "width": "100%",
                "z-index": "99999"
            });


            $('#stickybuttonClass').css({ "bottom": "77px" });
            $(window).scroll(function () {
                var specialHeight = $('#specialRte').height() ? ($('#specialRte').height() + 32) : 0;
                var footerHeight = $('.footer').parent().parent().height() ? $('.footer').parent().parent().height() : 0;
                if ($(window).scrollTop() + $(window).height() > $(document).height() - (specialHeight ? specialHeight : footerHeight)) {
                    var getStyle = $('.stickybuttonClass').attr('style');
                    $('.stickybuttonClass').css({
                        "bottom": (footerHeight + specialHeight + 45) + 'px',
                        "width": (getStyle.split(":")[1] ? getStyle.split(":")[1] : 1) * 1 + 'rem'
                    });
                } else {
                    // $('.phoneBg').removeAttr('style');
                    $('#stickybutton.phoneBg').css({ "bottom": "auto" });
                }
            });
        } else {
            //boost sticky button
            $('#stickybutton.phoneBg').removeClass('stickybuttonClass');

            //genesis sticky button
            $('#genesis-sticy-button.phoneBg').parent().removeClass('genesis-sticky');
            $('.genesis .footer').removeAttr("style")
            $('#genesis-sticy-button.phoneBg').parent().removeAttr("style")
        }
    })


    $('.disable-after-click').on('click', function () {
        $(this).addClass('disable-after-click-active');
    });

    $('.button').each(function () {
        if ($(this).find(".cmp-button__text").length === 0) {
            $(this).addClass('cmp-button__no-text');
        }
    })

    $('.button.cmp-button__tertiary').each(function () {
        if ($(this).find('.cmp-button__text').length > 0 && ($(this).find('.cmp-button__left').length > 0 && $(this).find('.cmp-button__left').text().trim().length == 0 || $(this).find('.cmp-button__right').length > 0 && $(this).find('.cmp-button__right').text().trim().length == 0)) {
            $(this).addClass("cmp-button__only-text")
        }
    })


    /*** START: Button Loading Behaviour based on Breakpoints ***/
    $(window).bind("load resize", function () {
        var screenWidth = window.innerWidth;
        if (screenWidth < 768) {
            /** Load mobile image **/
            var buttonBg = $('[data-cmp-button-phonebg]');
            for (var i = 0; i < buttonBg.length; i++) {
                if (buttonBg[i].length !== 0) {
                    buttonBg[i].style.width = 'calc(0.0625 * ' + buttonBg[i].getAttribute('data-cmp-button-phonebg') + 'rem)';
                }
            }
        } else if (screenWidth >= 768 && screenWidth <= 1024) {
            /** Ipad/ Tablet **/
            var buttonBg = $('[data-cmp-button-tabletbg]');
            for (var i = 0; i < buttonBg.length; i++) {
                if (buttonBg[i].length !== 0) {
                    buttonBg[i].style.width = 'calc(0.0625 * ' + buttonBg[i].getAttribute('data-cmp-button-tabletbg') + 'rem)';
                }
            }
        } else {
            /** desktop image **/
            var buttonBg = $('[data-cmp-button-desktopbg]');
            for (var i = 0; i < buttonBg.length; i++) {
                if (buttonBg[i].length !== 0) {
                    buttonBg[i].style.width = 'calc(0.0625 * ' + buttonBg[i].getAttribute('data-cmp-button-desktopbg') + 'rem)';
                }
            }
        }
    })
    /*** END: Button Loading Behaviour based on Breakpoints ***/
});