$(document).ready(function(){
     if($(".banner-container").length){
        var screenWidth = $("body").prop("clientWidth");
        $(".banner").each(function(){
            var bannerWidth = $(this).width();
            if(bannerWidth < screenWidth){
                $(this).find(".banner-container").css("padding","36px");
            }else{
                $(this).find(".banner-container").css("padding","85px")
            }
        });
     }
});