$(document).ready(function(){
    var storeClicked = [];
    var storeClickedGet = [];

    var storeClickedCell = [];
    var storeClickedSingleCell = [];

    var allnavigationCell = $(".accordion-header .navigation-table-cell");
    var plusMinusButton = $(".navigation-nav-body .accordion-button");
    var singleURL = $(".navigation-single-url");                 
    for (var ia = 0; ia < plusMinusButton.length; ia++) {
        $(plusMinusButton[ia]).attr("id","accrodionBTN"+ia);
    } 

    for (var ib = 0; ib < allnavigationCell.length; ib++) {
        $(allnavigationCell[ib]).attr("id","cellnavigation"+ib);
    }

    for (var ic = 0; ic < singleURL.length; ic++) {
        $(singleURL[ic]).attr("id","singleURL"+ic);
    }

    $(".navigation-single-url").click(function(){
        $(".navigation-single-url, .accordion-header > div, .accordion-header").removeClass("accordionOpen");
        $(this).addClass("accordionOpen");
        if($(this).hasClass("accordionOpen")){
            storeClicked.push($(this).attr("id"));
            storeClickedSingleCell.push($(this).attr("id"));

            for (var id = 0; id < storeClickedCell.length; id++) {
                var storeIndexxx = storeClicked.indexOf(storeClickedCell[id]);
                storeClicked.splice(storeIndexxx, 1);
            }
        } 
        sessionStorage.setItem("clicksStore", storeClicked.toString());
    });

    $(".accordion-header .navigation-table-cell").click(function(){
        $(".navigation-single-url, .accordion-header > div, .accordion-header").removeClass("accordionOpen");
        $(this).parent().parent().addClass("accordionOpen");
        if($(this).parent().parent().hasClass("accordionOpen")){
            storeClicked.push($(this).attr("id"));
            storeClickedCell.push($(this).attr("id"));

            for (var ig = 0; ig < storeClickedSingleCell.length; ig++) {
                var storeIndexx = storeClicked.indexOf(storeClickedSingleCell[ig]);
                storeClicked.splice(storeIndexx, 1);
            }
        }
        sessionStorage.setItem("clicksStore", storeClicked.toString());
    });

    $(".navigation-nav-body .accordion-button").click(function(){
        $(this).toggleClass("accordionOpenbtn");
        if($(this).hasClass("accordionOpenbtn")){
            storeClicked.push($(this).attr("id"));
        } else {
            var storeIndex = storeClicked.indexOf($(this).attr("id"));
            storeClicked.splice(storeIndex, 1);
        }
        sessionStorage.setItem("clicksStore", storeClicked.toString());
    });
    
    storeClickedGet = sessionStorage.getItem("clicksStore").split(',');
    if(storeClickedGet.length !== 0) {
        for (var ii = 0; ii < storeClickedGet.length; ii++) {
            $("#"+storeClickedGet[ii]).click();
        }
    }
});