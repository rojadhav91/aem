$(document).ready(function(){
    $(".headerSearchBox .btn-dark").click(function(){
        $(".search_box").hide();
        $(".lg-search").removeClass("lg-hover-search");
        $(".icon-search").removeClass("text-white");
    });

    $(".navbar-toggler").click(function(){
        $(".navbar-collapse").fadeToggle("slow");
        $("#loseFocus").blur();
        $(".header_mega_menu").hide();
        $(".header-link").removeClass("lg-hover-link text-white downarrow_hide");
        $(".icon-menu").toggleClass("icon-cross text-white");
        $(".navbar-collapse").toggleClass("open_menu");
        $(".navbar-collapse").removeClass("close-menu");
        $(".navbar-toggler").toggleClass("xs-hover-menu");
        $(".lg-search").removeClass("lg-hover-search");
        $(".icon-search").removeClass("text-white");
        $(".search_box").hide();
        if($(window).width() < 768){
            $(this).next(".navbar-collapse").css({"width": $(window).width()});
        }
    });

    $(".lg-search").click(function(){
        $(".search_box").fadeToggle(200);
        $(".header_mega_menu").hide();
        $(".navbar-collapse").toggleClass("close-menu");
        $(".navbar-collapse").removeClass("open_menu");
        $(".navbar-toggler").removeClass("xs-hover-menu");
        $(".icon-menu").removeClass("icon-cross text-white");
        $(".header-link").removeClass("lg-hover-link text-white downarrow_hide");
    });

    $(".lg-search").click(function(){
        $(".lg-search").toggleClass("lg-hover-search");
        $(".icon-search").toggleClass("text-white");
        $(".header-link").toggleClass("search-box-open");
    });
});