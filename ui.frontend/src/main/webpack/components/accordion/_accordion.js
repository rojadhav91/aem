$(document).ready(function () {
    function hideIconViewAccordion() {
        if ($('.cmp-accordion.iconview').parent().find(".tabss").length == 0) {
            $('.cmp-accordion.iconview').parent().prepend("<div class='tabss'><div class='accordion__tabs-content'></div>");

            var html = [];
            $('.accordion').each(function (index, element) {
                if ($(element).children('.cmp-accordion.iconview').text().length > 0) {
                    var html = [];
                    $(element).find('.cmp-accordion.iconview .cmp-accordion__item').each(function () {
                        html.push('<div class="tabss__item" data-tab-id="' + $(this).attr('id') + '"><img src="' + $(this).find('.cmp-accordion__button img').attr('src') + '" alt="' + $(this).find('.cmp-accordion__button img').attr('alt') + '" ><p>' + $(this).find('.cmp-accordion__button p').text() + '</p></div>');
                    })
                    $(element).find('.cmp-accordion.iconview').closest('.accordion').find('.tabss .accordion__tabs-content').append(html);
                }
            })
        }
    }

    hideIconViewAccordion()
    $('.tabss .cmp-accordion__item').on("click", function () {
        $('.cmp-accordion__item').removeClass('active');
        $(this).addClass('active');
        $(this).closest('.tabss').siblings().children().removeClass('iconViewTabSelectd');
        $(this).closest('.tabss').siblings().find('#' + $(this).attr('id')).addClass('iconViewTabSelectd');
    });
    $('.tabss .tabss__item').on("click", function () {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $(this).closest('.tabss').siblings().children().removeClass('iconViewTabSelectd');
        $(this).closest('.tabss').siblings().find('#' + $(this).data('tab-id')).addClass('iconViewTabSelectd');
    });
})