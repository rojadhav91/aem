$(document).ready(function () {
    function getSubstringIndex(str, substring, n) {
      var times = 0, index = null;
      while (times < n && index !== -1) {
          index = str.indexOf(substring, index + 1);
          times++;
      }
      return index;
    }
    function mapPathToCountryEnglish(path){
      var country="";
      switch(path) {
        case '/na/en-us':
        case '/na/es-us':
          country = 'United States';
          break;
        case '/na/en-ca':
        case '/na/fr-ca':
          country = 'Canada';
          break;
        case '/na/es-mx':
          country = 'Mexico';
          break;
        case '/ru/ru':
          country = 'Russia';
          break;
        case '/in/en':
          country = 'India';
          break;
        case '/af/en':
          country = 'Sub-Saharan Africa';
          break;
        case '/eu/en':
          country = 'EMEA';
          break;
        case '/ap/en':
        case '/ap/ja':
          country = 'Asia Pacific';
          break;
        case '/sa/pt':
        case '/sa/es':
          country = 'South America';
          break;
        default:
          country = 'United States';
      }
      return country;
    }
    function mapPathToCountry(path){
      var country="";
      switch(path) {
        case '/na/en-us':
          country = 'United States';
          break;
        case '/na/es-us':
          country = 'Estados Unidos';
          break;
        case '/na/en-ca':
          country = 'Canada';
          break;
        case '/na/fr-ca':
          country = 'Canada';
          break;
        case '/na/es-mx':
          country = 'México';
          break;
        case '/ru/ru':
          country = 'Россия';
          break;
        case '/in/en':
          country = 'India';
          break;
        case '/af/en':
          country = 'Sub-Saharan Africa';
          break;
        case '/eu/en':
          country = 'EMEA';
          break;
        case '/ap/en':
          country = 'Asia Pacific';
          break;
        case '/ap/ja':
          country = 'アジア太平洋地域';
          break;
        case '/sa/pt':
          country = 'América do Sul';
          break;
        case '/sa/es':
          country = 'Sudamerica';
          break;
        default:
          country = 'United States';
      }
      return country;
    }
    function mapPathToExtraPath(path){
      var extraPath="";
      switch(path) {
        case '/na/en-us':
          extraPath = '/na/es-us';
          break;
        case '/na/es-us':
          extraPath = '/na/en-us';
          break;
        case '/na/en-ca':
          extraPath = '/na/fr-ca';
          break;
        case '/na/fr-ca':
          extraPath = '/na/en-ca';
          break;
        case '/ap/en':
          extraPath = '/ap/ja';
          break;
        case '/ap/ja':
          extraPath = '/ap/en';
          break;
        case '/sa/pt':
          extraPath = '/sa/es';
          break;
        case '/sa/es':
          extraPath = '/sa/pt';
          break;
      }
      return extraPath;
    }
    function mapPathToExtraLang(path){
      var lang="";
      switch(path) {
        case '/na/en-us':
        case '/sa/pt':
          lang = 'Español';
          break;
        case '/na/en-ca':
          lang = 'Français';
          break;
        case '/na/es-us':
        case '/na/fr-ca':
        case '/ap/ja':
          lang = 'English';
          break;
        case '/sa/es':
          lang = 'Português';
          break;
        case '/ap/en':
          lang = '日本語';
          break;
      }
      return lang;
    }
    function mapPathToLang(path){
      var lang="";
      switch(path) {
        case '/na/en-us':
        case '/na/en-ca':
        case '/in/en':
        case '/eu/en':
        case '/af/en':
        case '/ap/en':
          lang = 'English';
          break;
        case '/na/fr-ca':
          lang = 'Français';
          break;
        case '/na/es-mx':
        case '/na/es-us':
        case '/sa/es':
          lang = 'Español';
          break;
        case '/ru/ru':
          lang = 'русский';
          break;
        case '/ap/ja':
          lang = '日本語';
          break;
        case '/sa/pt':
          lang = 'Português';
          break;
        default:
          lang = 'English';
      }
      return lang;
    }

    function mapPathToRegion(path){
      var region="";
      switch(path) {
        case '/na/en-us':
          region = 'United States';
          break;
        case '/na/es-us':
          region = 'Estados Unidos';
          break;
        case '/na/en-ca':
          region = 'Canada';
          break;
        case '/na/fr-ca':
          region = 'Canada';
          break;
        case '/ap/ja':
          region = 'アジア太平洋地域';
          break;
        case '/ap/en':
          region = 'Asia Pacific';
          break;
        case '/sa/pt':
          region = 'América do Sul';
          break;
        case '/sa/es':
          region = 'Sudamerica';
          break;
      }
      return region;
    }
    function renderImagesFromLocation(getCountryLocation, $container) {

        $container.find('[data-location-link]').each(function(index){
          $(this).css('text-decoration','underline');
        });
        if (getCountryLocation == "United States" || getCountryLocation == "Canada" || getCountryLocation == "Mexico" ) {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/northamerica-desktop.svg">');
            $( '.country-north-noactive,.country-south-active,.country-emea-active,.country-africa-active,.country-russia-active,.country-india-active,.country-china-active,.country-asia-active' ).css( "display", "none" );
            $container.find('[data-location-link="North America"]').css('text-decoration','none');     
            if(getCountryLocation == "United States"){
              $('[data-location-link="United States"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
            }else if(getCountryLocation == "Canada"){
              $('[data-location-link="Canada"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
            }else if(getCountryLocation == "Mexico"){
              $('[data-location-link="Mexico"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
            }
        } else if (getCountryLocation == "South America") {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/south-america-desktop.svg">');
            $( '.country-south-noactive,.country-north-active,.country-emea-active,.country-africa-active,.country-russia-active,.country-india-active,.country-china-active,.country-asia-active' ).css( "display", "none" );
            $('[data-location-link="South America"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        } else if (getCountryLocation == "EMEA") {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/emea-desktop.svg">');
            $( '.country-emea-noactive,.country-north-active,.country-south-active,.country-africa-active,.country-russia-active,.country-india-active,.country-china-active,.country-asia-active' ).css( "display", "none" );
            $('[data-location-link="EMEA"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        } else if (getCountryLocation == "Russia") {
          $('[data-location-link="Russia"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/russia-desktop.svg">');
            $( '.country-russia-noactive,.country-north-active,.country-south-active,.country-africa-active,.country-emea-active,.country-india-active,.country-china-active,.country-asia-active' ).css( "display", "none" );
        } else if (getCountryLocation == "India") {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/india-desktop.svg">');
            $( '.country-india-noactive,.country-north-active,.country-south-active,.country-africa-active,.country-emea-active,.country-russia-active,.country-china-active,.country-asia-active' ).css( "display", "none" );
            $('[data-location-link="India"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        } else if (getCountryLocation == "China") {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/china-desktop.svg">');
            $( '.country-china-noactive,.country-north-active,.country-south-active,.country-africa-active,.country-emea-active,.country-russia-active,.country-india-active,.country-asia-active' ).css( "display", "none" );
            $('[data-location-link="China"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        } else if (getCountryLocation == "Asia Pacific") {
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/asiapacific-desktop.svg">');
            $( '.country-asia-noactive,.country-north-active,.country-south-active,.country-africa-active,.country-emea-active,.country-russia-active,.country-india-active,.country-china-active' ).css( "display", "none" );
            $('[data-location-link="Asia Pacific"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        } else if(getCountryLocation == "Sub-Saharan Africa"){
            $container.find('.img-map').html('<img src="/content/dam/kemin-redesign/south-africa-desktop.svg">');
            $( '.country-africa-noactive,.country-north-active,.country-south-active,.country-asia-active,.country-emea-active,.country-russia-active,.country-india-active,.country-china-active' ).css( "display", "none" );
            $('[data-location-link="Sub-Saharan Africa"]').css({'color':'#3FC1CB', 'text-decoration': 'none'});
        }
    }

    var path = window.location.pathname;
    if (!path.includes('/content/kemin-redesign')) {
      path = path.substring(0, getSubstringIndex(path, '/', 2));
    } else {
      path = path.substring(23, getSubstringIndex(path, '/', 4));
    }
    var country = mapPathToCountry(path);
    var countryEnglish = mapPathToCountryEnglish(path);
    var lang = mapPathToLang(path);
    console.log("path:"+path+" lang:"+lang+" country:"+country);
    $('#kemin-user-location,#kemin-user-location-footer').attr('data-location', countryEnglish).text(country);
    $('.lang_heading a').text(lang);
    
    renderImagesFromLocation($('#kemin-user-location').attr('data-location'), $('#kemin-user-location').closest('.location-container'));
    renderImagesFromLocation($('#kemin-user-location-footer').attr('data-location'), $('#kemin-user-location-footer').closest('.location-container'));
    $(".header").find(".img-map img").attr("usemap","#countryHeader");
    $(".footer").find(".img-map img").attr("usemap","#countryFooter");
    
    //click on header link
    $('#kemin-user-location,#icon-button-lang').click(function (event) {
        event.preventDefault();

        var $locContainer = $(this).closest('.location-container');
        var checkIfMapOpened = $locContainer.find("div.map_dropdown-none");
        if (checkIfMapOpened.length == 1) {
          $locContainer.find(".map_heading").first().addClass("map_heading_color");
          $locContainer.find(".icon-button-lang").removeClass("icon-chevron-down").addClass("icon-chevron-up");
          $locContainer.find(".map-dropdown").removeClass("map_dropdown-none").addClass("map_dropdown-block");
          $('map').imageMapResize();
        } else {
          $locContainer.find(".map_heading").first().removeClass("map_heading_color");
          $locContainer.find(".icon-button-lang").removeClass("icon-chevron-up").addClass("icon-chevron-down");
          $locContainer.find(".map-dropdown").removeClass("map_dropdown-block").addClass("map_dropdown-none");
        }
    });

    $('#kemin-user-location-footer,#icon-button-lang-footer').click(function (event) {
        event.preventDefault();

        var $locContainer = $(this).closest('.location-container');
        var checkIfMapOpened = $locContainer.find("div.map_dropdown-none");
        if (checkIfMapOpened.length == 1) {
          $locContainer.find(".map_heading").first().addClass("map_heading_color");
          $locContainer.find(".icon-button-lang").removeClass("icon-chevron-up").addClass("icon-chevron-down");
          $locContainer.find(".map-dropdown").removeClass("map_dropdown-none").addClass("map_dropdown-block");
          $('map').imageMapResize();
        } else {
          $locContainer.find(".map_heading").first().removeClass("map_heading_color");
          $locContainer.find(".icon-button-lang").removeClass("icon-chevron-down").addClass("icon-chevron-up");
          $locContainer.find(".map-dropdown").removeClass("map_dropdown-block").addClass("map_dropdown-none");
        }
    });

    //link clicks on map
    $('.mobile-map-click-NA').click(function () {
      $('.mobile-map-click-NA').next().find("button.accordion-button").click();
   });

    var modalTitle=$('.modal-header-title').text();
    var modalLanguageTitle=$('.modal-language-title').text();
    
    $('ul.country-location-links li,.location-map-title, .mobile-map-click').click(function(e) { 
      e.preventDefault();      
      var linkClickLocation = $(this).attr("data-location-link");
      var targetLocation = $(this).text();
      if($(this).hasClass('mobile-map-click')){
        targetLocation = $(this).parent().find('.location-map-title').text();
        console.log("targetLocation:"+targetLocation);
      }
      
      if(linkClickLocation !== "North America"){
          $('#location-model-header').modal('show');
      };

      $('.modal-header-title').text(modalTitle.replace('{}',targetLocation));

      if(linkClickLocation == "United States"){
        
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="United States" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/na/en-us/home">English</a><a data-cta="Spanish" data-at="United States" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/na/es-us/home">Español</a>');
      }
      if(linkClickLocation == "Canada"){
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="Canada" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/na/en-ca/home">English</a><a data-cta="French" data-at="Canada" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/na/fr-ca/home">Français</a>');
      }
      if(linkClickLocation == "Mexico"){
        $('#locations-modal-footerbutton').html('<a data-cta="Spanish" data-at="Mexico" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/na/es-mx/home">Español</a>');
      }
      if(linkClickLocation == "Russia"){
        $('#locations-modal-footerbutton').html(' <a data-cta="Russian" data-at="Russia" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/ru/ru/home">русский</a>');
      }

      if(linkClickLocation == "India"){
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="India" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/in/en/home">English</a>');
      }
      
      if(linkClickLocation == "China"){
        $('#locations-modal-footerbutton').html(' <a data-cta="Chinese" data-at="China" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="https://www.kemin.cn/">中文</a>');
      }
      if(linkClickLocation == "Sub-Saharan Africa"){
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="Sub-Saharan Africa" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/af/en/home">English</a>');
      }
      if(linkClickLocation == "EMEA"){
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="EMEA" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/eu/en/home">English</a>');
      }
      if(linkClickLocation == "Asia Pacific"){
        $('#locations-modal-footerbutton').html(' <a data-cta="English" data-at="Asia Pacific" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/ap/en/home">English</a><a data-cta="Japanese" data-at="Asia Pacific" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/ap/ja/home">日本語</a>');
      }
      if(linkClickLocation == "South America"){
        $('#locations-modal-footerbutton').html(' <a data-cta="Portuguese" data-at="South America" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/sa/pt/home">Português</a><a data-cta="Spanish" data-at="South America" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="/sa/es/home">Español</a>');
      }
    });

    $('.lang_heading a, .lang_heading span').click(function(e) { 
      e.preventDefault();      
      var extraPath = mapPathToExtraPath(path);
      if(extraPath!=""){
        var extraLang = mapPathToExtraLang(path);
        var region = mapPathToRegion(path);
        $('.modal-header-title').text(modalLanguageTitle);
        $('#locations-modal-footerbutton').html(' <a data-cta="'+lang+'" data-at="'+region+'" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="'+path+'/home">'+lang+'</a><a data-cta="'+extraLang+'" data-at="'+region+'" data-nav="region map" data-cta-type="link" type="button" target="_blank" class="btn btn-primary" href="'+extraPath+'/home">'+extraLang+'</a>');

        $('#location-model-header').modal('show');
      }
    });

    $(document).on('click','#locations-modal-footerbutton a', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var gotoPath=$(this).attr('href');

      if(gotoPath.indexOf("kemin.cn")<0){
        if(window.location.hostname.indexOf('kemin')<0)
          gotoPath=window.location.protocol+"//"+window.location.hostname+":"+window.location.port+"/content/kemin-redesign"+gotoPath+".html";
        else
          gotoPath=window.location.protocol+"//"+window.location.hostname+gotoPath;
      }
      window.open(gotoPath, '_blank');
      
    });

    var extraLangPath = mapPathToExtraPath(path);
    if(extraLangPath==''){
      $('.lang_heading .icon-chevron-down,.lang_heading .icon-chevron-up').hide();
    }
});