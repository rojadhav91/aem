$(document).ready(function () {

    function fromSessionStorage(key) {
        let value = sessionStorage.getItem(key);
        if (!value) {
            console.log(key, "not found in session storage");
            value = '';
        }
        return value;
    }

    function inputField(key) {
        const value = fromSessionStorage(key);
        return `<input type="hidden" name="${key}" value="${value}">`;
    }

    $('.cmp-genesis_checkout .genesis-checkout-button').on('click', function (event) {
        event.preventDefault();
        $('.loading-spinner').show();
        const magentoControllerUrl = $(this).parents('.cmp-genesis_checkout').data('controller-url');
        //const redirectUrl = $(this).children('a').attr('href');
        const $form = $(this).parents('form[name=qualificationsurvey]');
        if (!$form.length){
            console.warn("Genesis checkout: couldn't find check qualifications survey form!");
            return;
        }

        const formFields = ['zip', 'email', 'address', 'address2', 'city', 'state'];
        for (const key of formFields) {
            console.debug('Adding', key, "input field to the form.");
            $form.append(inputField(key));
        }
        //$form.append(`<input type="hidden" name="redirect" value=${redirectUrl}>`)

        $form.attr('method', "post");
        $form.attr('action', magentoControllerUrl);
        console.debug("submitting genesis checkout form to", magentoControllerUrl);
        $form.submit();
    });
});