(function($) {
    $(document).ready(function(){
        $(".playbtnblack").click(function(){
            var _this = $(this);
            
            _this.parents(".video_wrapper").find(".video_assets").hide();
            _this.parents(".video_wrapper").find(".play_stop_btn").hide();

            //Dam video play
            if(_this.parents(".video_wrapper").find(".dam-video")[0] != undefined){
                var vid = _this.parents(".video_wrapper").find(".dam-video")[0]; 
                vid.play();
            }else if(_this.parents(".video_wrapper").find(".youtube").find("iframe")[0] != undefined){
                _this.parents(".video_wrapper").find(".youtube").find("iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
            }else if(_this.parents(".video_wrapper").find(".wistia_responsive_padding")[0] != undefined){

                window._wq = window._wq || [];

                _wq.push({
                    id: "_all",
                    onReady: function(wistiaEmbed) {

                        var wistiaHash = _this.parents(".video_wrapper").find(".wistia_embed").attr("id", "wistiaGenID_" + wistiaEmbed.hashedId());

                        // grab Wista API
                        wistiaEmbed = Wistia.api("wistiaGenID_");

                        // when you click the custom play button
                        wistiaEmbed.play();

                    }
                });
                
            } 
            
        });

    });
})(jQuery);