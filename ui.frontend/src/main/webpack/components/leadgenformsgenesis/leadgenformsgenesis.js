//$(document).ready(function () {
//		function submitClick(event) {
//		//alert("submit button clicked..!");
//		event.preventDefault();
//		var forms = document.getElementsByClassName("endpoint");
//		var actionurl = $(forms).data("path");
//		var emailId = $("#txtEmail").val();
//		var zipCode = $("#txtZipCode").val();
//		var href = $("#btnSubmit").attr("href");
//		$("#leadgenform").attr("action", href);
//		$.ajax({
//			url: actionurl,
//			method: 'POST',
//			dataType: 'json',
//			data: {
//				email: emailId,
//				zip: zipCode,
//				site: "boost",
//			}
//		}).done(function(response) {
//            console.log(response.status);
//           // var apiResponse = response.email;
//            if(response.status == "success"){
//               //brite verify + responsys call successful
//                console.log("email vaildated successfully");
//                $("#leadgenform").submit();
//            }else if(response.status == "failure" && response.api == "briteVerify"){
//                console.log("invalid email id entered");
//                window.adobeDataLayer = window.adobeDataLayer || [];
//                                      window.adobeDataLayer.push({
//                                      event: 'form_Error',
//                                      form: {
//                                           formDetails: {
//                                                formError: {
//                                                    value: 1
//                                                },
//                                                formName: 'email/zip form',
//                                                formErrorName: 'Invalid Email Address',
//                                           }
//                                      }
//                                });
//            }
//             else{
//               console.log("API call failed");
//             }
//
//		}).fail(function(jqXHR, textStatus){
//                alert("API call failed");
//		});
//	}
//
//	function verifyEmailPattern() {
//		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//		if (re.test($("#txtEmail").val())) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	function verifyZipPattern() {
//		var zipcode = $("#txtZipCode").val();
//		if (!isNaN(zipcode) && zipcode.length == 5) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	function disableSubmitButton(val) {
//		if (val) {
//			$("#btnSubmit").parent().addClass("cmp-button__disabled");
//		} else {
//			$("#btnSubmit").parent().removeClass("cmp-button__disabled");
//		}
//	}
//
//	function ValidateEmail() {
//		if (verifyEmailPattern()) {
//			if (verifyZipPattern()) {
//				disableSubmitButton(false);
//			}
//			$("#txtEmail")
//				.closest(".input-container")
//				.find(".span-element")
//				.css("color", "#171725");
//		} else {
//			disableSubmitButton(true);
//			$("#txtEmail")
//				.closest(".input-container")
//				.css("border-bottom-color", "#384AFF");
//		}
//	}
//
//	function ValidateZip() {
//		if (verifyZipPattern()) {
//			if (verifyEmailPattern()) {
//				disableSubmitButton(false);
//			}
//			$("#txtZipCode")
//				.closest(".input-container")
//				.find(".span-element")
//				.css("color", "#171725");
//
//		} else {
//			disableSubmitButton(true);
//			$("#txtZipCode")
//				.closest(".input-container")
//				.css("border-bottom-color", "#384AFF");
//		}
//	}
//
//	function ValidateZipAfterFocusOut() {
//		if (verifyZipPattern()){
//		  $("#txtZipCode")
//			.closest(".input-container")
//			.css("border-bottom-color", "#171725");
//
//			$("#errorZipcode").hide();
//        }
//		else{
//		  $("#txtZipCode")
//			.closest(".input-container")
//			.css("border-bottom-color", "#931621");
//
//          $("#errorZipcode").show();
//
//            window.adobeDataLayer = window.adobeDataLayer || [];
//                window.adobeDataLayer.push({
//                event: 'form_error',
//                form: {
//                    formDetails: {
//                        formError: {
//                            value: 1
//                        },
//                        formName: 'email/zip form',
//                        formErrorName: 'Enter Valid Zip Code',
//                    }
//                }
//            });
//        }
//	  }
//
//	  function ValidateEmailAfterFocusOut() {
//		if (verifyEmailPattern()){
//		  $("#txtEmail")
//			.closest(".input-container")
//			.css("border-bottom-color", "#171725");
//
//			$("#errorEmail").hide();
//        }
//		else{
//		  $("#txtEmail")
//			.closest(".input-container")
//			.css("border-bottom-color", "#931621");
//
//            $("#errorEmail").show();
//
//            window.adobeDataLayer = window.adobeDataLayer || [];
//                window.adobeDataLayer.push({
//                event: 'form_Error',
//                form: {
//                    formDetails: {
//                        formError: {
//                            value: 1
//                        },
//                        formName: 'email/zip form',
//                        formErrorName: 'Invalid Email Address',
//                    }
//                }
//            });
//
//		}
//	}
//
//	function acceptOnlyNumbers(event) {
//		return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key));
//	}
//
//	$("#btnSubmit").parent().addClass("cmp-button__disabled");
//	$("#txtEmail").on("keyup", ValidateEmail);
//	$("#txtZipCode").on("keyup", ValidateZip);
//	$("#btnSubmit").on("click", submitClick);
//	$("#txtZipCode").on("focusout", ValidateZipAfterFocusOut);
//	$("#txtEmail").on("focusout", ValidateEmailAfterFocusOut);
//	$("#txtZipCode").on("keydown", acceptOnlyNumbers);
//});