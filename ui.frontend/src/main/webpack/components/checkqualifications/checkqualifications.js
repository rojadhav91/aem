let checkoutForm;
$(document).ready(function () {

    function fromSessionStorage(key) {
        let value = sessionStorage.getItem(key);
        if (!value) {
            value = '';
        }
        return value;
    }

    function inputField(key) {
        const value = fromSessionStorage(key);
        return `<input type="hidden" name="${key}" value="${value}">`;
    }

    function callToCheckout(){

        $('.loading-spinner').show();
        const magentoControllerUrl = $('#genesis-checkout-button').attr('href');
        const $form = $(checkoutForm);
        if (!$form.length){
            return;
        }

        const formFields = ['zip', 'email', 'address', 'address2', 'city', 'state'];
        for (const key of formFields) {
            $form.append(inputField(key));
        }

        $form.attr('method', "post");
        $form.attr('action', magentoControllerUrl);
        $form.submit();
    }

        $('#genesis-checkout-button.desktopBg').on('click', function (event) {
            checkoutForm = $(this).parents('form[name=qualificationsurvey]');
            event.preventDefault();
            callToCheckout();
        });

        $('#genesis-checkout-button.tabletBg').on('click', function (event) {
            checkoutForm = $(this).parents('form[name=qualificationsurvey]');
            event.preventDefault();
            callToCheckout();
        });

        $('#genesis-checkout-button.phoneBg').on('click', function (event) {
            checkoutForm = $(this).parents('form[name=qualificationsurvey]');
            event.preventDefault();
            callToCheckout();
        });
});