$(document).ready(function(){
    $(".Hubspots-toggle").click(function(){
      $(".Hubspots-form").slideToggle(300);
      $(".hs-msg-heading .icon-chevron-down").toggleClass("icon-chevron-up");
    });

    $(".submitContactUs").click(function(event){
        var inputElements = $(".contactus input[required]");
        var selectElements = $(".contactus select[required]");
        
        for (var i = 0; i < inputElements.length; i++) {
          if($(inputElements[i]).val() === ""){
            $(inputElements[i]).next(".error").text("Please complete this required field.");
            $(inputElements[i]).addClass("invalid");
          }else {
            $(inputElements[i]).next(".error").text("");
            $(inputElements[i]).removeClass("invalid");
          }
        }

        for (var s = 0; s < selectElements.length; s++) {
          if($(selectElements[s]).val() === ""){
            $(selectElements[s]).next(".error").text("Please select an option from the dropdown menu.");
            $(selectElements[s]).addClass("invalid");
          }else {
            $(selectElements[s]).next(".error").text("");
            $(selectElements[s]).removeClass("invalid");
          }
        }
    });
});