"use strict";

var TagManager = Packages.com.day.cq.tagging.TagManager;

use(function() {

    var removeFromPath = ["/content/kemin-redesign/", "/home/", ".html"];
    var subRegionPage = currentPage.getAbsoluteParent(3);
    var pathSegments = [], productPathSegments = [];
    var tagManager = resolver.adaptTo(TagManager);

    var digitalData = {
        page: {
            pageInfo: {
                pageName: "",
                URL: ""
            },
            category: {
                pageType: "",
                primaryCategory: "",
                subCategory1: "",
                subCategory2: "",
                subCategory3: "",
                businessUnit: ""
            },
            attributes: {
                region: "",
                country: "",
                org: "",
                language: ""

            }
        },
        product: {
            productInfo: {
                productName: "",
                productID: "",
                productCategory: "",
                application: "",
                itemClass: ""
            },
            productSearchInfo: {
                searchTerm: "",
                numOfResults: ""
            }
        },
        search: {
            searchInfo: {
                searchTerm: "",
                numOfResults: "",
                searchResultClick: ""
            }
        },
        form: {
            formInfo: {
                formName: "",
                formID: "",
                formStatus: ""
            }
        },
        video: {
            videoInfo: {
                videoName: "",
                videoID: "",
                videoStart: "",
                videoMilestone: ""
            }
        },
        click: {
            clickInfo: {
                linkName: "",
                buttonName: "",
                linkHeader: "",
                linkNavigation: ""
            },
            bannerInfo: {
                bannerTitle: ""
            },
            downloadInfo: {
                downloadFileName: "",
                downloadFileURL: "",
                downloadFileType: "",
                downloadAsset: ""
            }
        },
        vanity: ''
    };

    function contains(haystack, needle) {
        var arr = haystack.split(needle);
        return arr.length > 1;
    }
    function setPathSegments() {
        pathSegments = String(request.getRequestURL().toString())
            .replace(removeFromPath[0], "/")
            .replace(removeFromPath[1], "/")
            .replace(removeFromPath[2], "")
            .replace(/(http|https):\/\/(.[^\/]*)\//, "\/")
            .replace(/\/(.[^\/]*)\/(.[^\/]*)\//, "")
            .replace(/(\/+)/, "/")
            .split("/");
    }
    function setProductPathSegments() {
        var path = String(request.getRequestURL().toString());
        if (contains(path, '/products/')) {
            productPathSegments = path
                .replace(removeFromPath[0], "/")
                .replace(removeFromPath[1], "/")
                .replace(removeFromPath[2], "")
                .replace(/(http|https):\/\/(.[^\/]*)\//, "\/")
                .replace(/\/(.[^\/]*)\/(.[^\/]*)\/products\//, "")
                .replace(/(\/+)/, "/")
                .split("/");
        }
    }

    function getAttributeRegion(){
        var regionURL = currentPage.getAbsoluteParent(2);
        var region = '';
        if(regionURL){
            region = String(regionURL.getName());
        }
        return region;

    }

    function getAttributeSubRegion(){
        var subRegion = '';
        if(subRegionPage){
            var subRegionTitle = String(subRegionPage.getName());
            var subRegionArray = subRegionTitle.split('-');
            if(subRegionArray.length > 1){
                subRegion = subRegionArray[1];
            }
        }
        return subRegion;
    }

    function getAttributeLanguage(){
        var pageLanguage = '';
        if(subRegionPage){
            pageLanguage = String(subRegionPage.getName());
            var languageArray = pageLanguage.split('-');
            if(languageArray.length > 1){
                pageLanguage = languageArray[0];
            }
        }
        return pageLanguage;
    }

    function loopAndParse(valueArr) {
        var section = [];
        if(valueArr) {
            for(var i =0;i<valueArr.length;i++) {
                var splitStr = String(valueArr[i]);
                if(splitStr && section.indexOf(splitStr) === -1) {
                    section[section.length] = splitStr;
                }
            }
            return section.toString();
        }
        return "";
    }

    function getPageProperty(propName, defaultVal){
        return String(currentPage.getProperties().get(propName, defaultVal||"")).replace(/<sup>.+<\/sup>/g,"");
    }

    function getPageType() {
        var pageType = "";//getPageProperty("pageType", "");
        var pageTypeArray = getPageProperty("cq:template").split("/");
        if(pageTypeArray.length > 0){
            pageType = String(pageTypeArray[pageTypeArray.length - 1]);
        }
        return pageType;
    }
    function getBusinessUnit() {
        var bizUnit = getPageProperty("businessUnit", "");
        if (bizUnit) {
            var tag = tagManager.resolve(bizUnit);
            if (tag != null) {
                bizUnit = String(tag.title);
            }
            else {
                var arr = bizUnit.split("/");
                bizUnit = String(arr[arr.length-1]);
            }
        }
        return bizUnit;
    }
    function getPrimaryCategory() {return String(pathSegments[0]||"NA");}
    function getSubCategory1() {return String(pathSegments[1]||"NA");}
    function getSubCategory2() {return String(pathSegments[2]||"NA");}
    function getSubCategory3() {return String(pathSegments[3]||"NA");}
    function getAttributeOrg() {return "Kemin";}
    function getProductName() {return String(productPathSegments[0]||"");}
    function getProductId() {return getPageProperty("prodNumber");}
    function getProductCategory() {return getPageProperty("prodCatalogName");}
    function getProductApplication() {
        var tag;
        var productApplication = currentPage.getProperties().get('productApplication');
        if (typeof productApplication === "string") {
            tag = tagManager.resolve(productApplication);
            return String(tag != null ? tag.title : "");
        }
        else {
            var tags = [];
            for (var i in productApplication) {
                if (productApplication.hasOwnProperty(i)) {
                    tag = tagManager.resolve(productApplication[i]);
                    if (tag != null) {
                        tags.push(String(tag.title));
                    }
                }
            }
            return String(tags.join(', '));
        }
    }
    function getProductItemClass() {
        var itemClass = getPageProperty("itemClass", "");
        if (itemClass) {
            var tag = tagManager.resolve(itemClass);
            if (tag != null) {
                itemClass = String(tag.title);
            }
        }
        return itemClass;
    }
    function getSearchTerm() {
        var param = String(request.getParameter('query'));
        return param === 'null' ? '' : param;
    }

    function setSearchInfo() {
        digitalData.search.searchInfo.searchTerm = getSearchTerm();
    }

    function setProductInfo() {
        digitalData.product.productInfo.productName = getProductName();
        digitalData.product.productInfo.productID = getProductId();
        digitalData.product.productInfo.productCategory = getProductCategory();
        digitalData.product.productInfo.application = getProductApplication();
        digitalData.product.productInfo.itemClass = getProductItemClass();
    }

    function setAttributes() {
        digitalData.page.attributes.region = getAttributeRegion();
        digitalData.page.attributes.country = getAttributeSubRegion();
        digitalData.page.attributes.language = getAttributeLanguage();
        digitalData.page.attributes.org = getAttributeOrg();
    }

    function setPageCategory() {
        digitalData.page.category.pageType = getPageType();
        digitalData.page.category.businessUnit = getBusinessUnit();
        digitalData.page.category.primaryCategory = getPrimaryCategory();
        digitalData.page.category.subCategory1 = getSubCategory1();
        digitalData.page.category.subCategory2 = getSubCategory2();
        digitalData.page.category.subCategory3 = getSubCategory3();
    }

    function setPageInfo() {
        digitalData.page.pageInfo.pageName = getPageProperty("jcr:title");
        digitalData.page.pageInfo.URL = String(request.getRequestURL().toString());
    }

    function setVanity() {
        var vanityArr = currentPage.getProperties().get("sling:vanityPath");
        if(vanityArr && vanityArr.length > 0) {
            digitalData.vanity = loopAndParse(vanityArr);
        }
        else {
            digitalData.vanity = String(currentPage.getVanityUrl() != null ? currentPage.getVanityUrl() : "");
        }
    }

    function init() {
        setPathSegments();
        setProductPathSegments();
        setPageInfo();
        setPageCategory();
        setAttributes();
        setProductInfo();
        setSearchInfo();
        setVanity();
    }

    init();

    return {
        init: JSON.stringify(digitalData, null, 2)
    };
});
