$(document).ready(function() {
    var strGet = $('div #root').attr('rootPath');
    var pathUrl = $('div #showMore').attr('fde');
    var jsonDATA;
    $('form.contactForm').submit(function() {
        event.preventDefault();
        var str = $(this).serialize();
        $.ajax({
            processData: false,
            contentType: false,
            async: true,
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: str, //passing values to servlet
            success: function(responseData, textStatus, jqXHR) {
                $("#contactUsForm").hide();
                $("#sendmessage").addClass("show");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#contactUsForm").hide();
                $("#errormessage").addClass("show");
            }
        });

    });

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: pathUrl,
        data: {
            rootPath: strGet
        },
        success: function(responseData, textStatus, jqXHR) {
            jsonDATA = responseData;
            var i = 1;
            $.each(responseData, function(i, item) {
                if (i < 3) {
                    feedList(item);
                    i++;
                }
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });

    $("#btn").click(function() {

        $("#showMore").html("");
        $.each(jsonDATA, function(i, item) {
            feedList(item);
        });

    });

    function feedList(item) {
        const formattedDate = dateFormat(item);
        $('#showMore').append(
            "<div class='col-md-4'>" +
            "<img src=" + item.heroImagePath + " alt='' />" +
            "<h4><a href=" + item.guideDetailLink + ">" + item.guideTitle + "</a></h4>" +
            "<span id='date'>" + formattedDate + "</span>" +
            "</div>")

    }

    function dateFormat(item) {
        const date = new Date(item.guidepublishDate)
            .toLocaleDateString({}, {
                timeZone: "UTC",
                month: "long",
                day: "2-digit",
                year: "numeric"
            })
        return date;
    }
});