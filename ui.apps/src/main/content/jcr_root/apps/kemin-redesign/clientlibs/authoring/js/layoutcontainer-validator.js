(function ($, window) {
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "[data-validation=top-padding]",
        validate: function (element) {
            var el = $(element);
			el.val(el.val().replace(/^[ ]+|[ ]+$/g,''));
            if (!/^[1-9 ]\d*$/.test(el.val()) && el.val()!="")
                return "Please enter a positive number for top padding.";

        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "[data-validation=right-padding]",
        validate: function (element) {
            var el = $(element);
            el.val(el.val().replace(/^[ ]+|[ ]+$/g,''));
            if (!/^[1-9 ]\d*$/.test(el.val()) && el.val()!="")
                return "Please enter a positive number for right padding.";
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "[data-validation=bottom-padding]",
        validate: function (element) {
            var el = $(element);
            el.val(el.val().replace(/^[ ]+|[ ]+$/g,''));
            if (!/^[1-9 ]\d*$/.test(el.val()) && el.val()!="")
                return "Please enter a positive number for bottom padding.";
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "[data-validation=left-padding]",
        validate: function (element) {
            var el = $(element);
            el.val(el.val().replace(/^[ ]+|[ ]+$/g,''));
            if (!/^[1-9 ]\d*$/.test(el.val()) && el.val()!="")
                return "Please enter a positive number for left padding.";
        }
    });
})($, window);
