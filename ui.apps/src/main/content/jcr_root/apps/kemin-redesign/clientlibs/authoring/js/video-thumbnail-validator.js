(function($, window) {
    $(document).on('dialog-ready', function(e) {

        Coral.commons.ready($(this), function() {
			var url = $(".cmp-video-thumbnail-field coral-taglist coral-tag input").val();;
        	enableDisableField(url);
        })

        $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
            selector: ".cmp-video-thumbnail-field",
            validate: function(element) {
                var ctaUrl = element.value;
                enableDisableField(ctaUrl);
            }
        });

        function enableDisableField(ctaUrl){
            var enableField = (ctaUrl !== '') && (ctaUrl != null) && (ctaUrl.startsWith("/content/dam"));
            $('.cmp-video-play-btn-color-field').attr('disabled', !enableField);
        }

    })

})
($, window);