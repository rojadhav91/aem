(function (document, $, ns, window) {
    "use strict";

    var msgs=['','',''];
    $(document).on('dialog-ready', function(e) {
        $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
            selector: ".carouselImage",
            validate: function(element) {
                var imageUrl = $(element).find('input[data-cq-fileupload-parameter="filereference"]').val();
                console.log('imageUrl:'+imageUrl);
                var elName = $(element).attr('data-id');
                var index = parseInt(elName[elName.length -1]);
                const img = new Image();
                img.onload = function() {
                    console.log(this.width + 'x' + this.height);
                    var valid=true;
                    if(this.width+10 < 1920 || this.width-10>1920) //not around 1920 px width
                        valid=false;
                    else if(this.height+10 < 650 || this.height-10>650) //not around 650 px height
                    	valid=false;
                    if(!valid){
                        msgs[index-1]= "Image "+ index +" must be 1920 px * 650 px in desktop";
                    }else{
                        msgs[index-1]='';
                    }
                    console.log('msgs:'+msgs);
                }
                img.src = imageUrl;
            }
        });
    });

    $(document).on("click", ".cq-dialog-submit", function (e) {
        e.stopPropagation();
        e.preventDefault();

        var valid = true;

		msgs.forEach(function(msg) {
            if(msg!=''){
                valid = false;
                ns.ui.helpers.prompt({
                  title: Granite.I18n.get("Invalid Input"),
                  message: msg,
                  actions: [{
                    id: "CANCEL",
                    text: "CANCEL",
                    className: "coral-Button"
                  }],
                  callback: function (actionId) {
                    if (actionId === "CANCEL") {
                    }
                  }
                  });
                return false;
            }
        });
        /* Select the form fields, will be project specific. */
        var $formFields = $('.cq-dialog .coral-Form-field');
        
        $formFields.each(function(){
            if (!$(this).checkValidity()) {
                valid = false;

                return false;
            }
        });
        if(valid)
            $(this).closest("form.foundation-form").submit();
        else
            return false;
    });

})(document, Granite.$, Granite.author, window);