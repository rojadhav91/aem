;(function ($, ns, channel, window, undefined) {

    ns.actions.publishApproval = function () {
        window.open("/etc/designs/kemin-redesign/tools/publishapproval.html?page_path="+ns.ContentFrame.location.substring(0, ns.ContentFrame.location.indexOf(".")));
    };

    channel.on("click", ".workflow-publishapproval", function () {
        ns.actions.publishApproval();
    });

    channel.on("click", "#pageinfo-trigger", function () {
        if(ns.ContentFrame.location.indexOf('/kemin-redesign/')<=0)
        {
            setTimeout(function(){ 
                $('.workflow-publishapproval').hide();
            }, 100);
            setInterval(function(){ 
                if($('.workflow-publishapproval').is(':visible')){
                	$('.workflow-publishapproval').hide();
                }
            }, 500);
        }else if($('.js-editor-WorkflowStart-activator').length<=0){
            setTimeout(function(){ 
                $('.workflow-publishapproval').hide();
            }, 100);
            setInterval(function(){ 
                if($('.workflow-publishapproval').is(':visible')){
                	$('.workflow-publishapproval').hide();
                }
            }, 500);
        }else{
            setTimeout(function(){ 
                $('.workflow-publishapproval').insertAfter($('.js-editor-WorkflowStart-activator'));
            }, 100);

            setInterval(function(){ 
                if(!$('.workflow-publishapproval').prev().hasClass('js-editor-WorkflowStart-activator')){
                	$('.workflow-publishapproval').insertAfter($('.js-editor-WorkflowStart-activator'));
                }
            }, 500);
        }
    });

}(jQuery, Granite.author, jQuery(document), this));