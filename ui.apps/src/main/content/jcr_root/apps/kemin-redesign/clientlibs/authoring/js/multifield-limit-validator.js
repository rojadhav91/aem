(function($, window) {
    function showHideAddButtonOfElement(el,maxPanels,totalPanels){
        if(totalPanels >= maxPanels){
            $(el).children("button").hide();
        }else{
            $(el).children("button").show();
        }
    }
    function showHideAddButtons(){
        $("coral-multifield[data-max-item]").each(function(i,el){
            var max = $(el).data("max-item");
            var total = $(el).children('coral-multifield-item').length;
            showHideAddButtonOfElement(el,max,total);
        });
    }
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "coral-multifield",
        validate: function(el) {
            var totalPanels = $(el).children('coral-multifield-item').length;
            if ($(el).data("min-item")){
                var min = $(el).data("min-item");
                if(totalPanels < min) {
                    return "Minimum numbers of items required are: " + min;
                }
            }
            if ($(el).data("max-item")){
                var max = $(el).data("max-item");
                showHideAddButtonOfElement(el,max,totalPanels);
                if(totalPanels > max) {
                    return "Maximum numbers of items allowed are: " + max;
                }
            }
    }});
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator",
    		{
    		   selector: "[data-validation=kemin-multifield]",
    		   validate: function(element)
    			{
                    var el = $(element);
                    let min=el.data("min-items");
                    let items=el.children("coral-multifield-item").length;
                    console.log("{} :{} :{}", min,items);

                     if (items < min)
    				{
    				return "Min allowed items is " + min
                    }


                }
            });
    $(window).on("dialog-ready",function(){
        showHideAddButtons();
    });
    $(document).ready(function(){
        showHideAddButtons();
    });
})($, window);