$(document).ready(function () {
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    var pagePath = getUrlParameter('page_path');
    console.log('pagePath:'+pagePath);
    $('#page_path').val(pagePath);
    $('#backLink').attr('href','/editor.html'+pagePath+'.html');
    $('#btnSubmitForm').click( function(e) {
        e.preventDefault();    
        var dateSelected=$('coral-datepicker').find('input[type=hidden]').val();
        if(dateSelected==null || dateSelected==''){
            alert("Due date is required!");
        	return;
        }
        $.ajax({
            url: '/bin/kemin-redesign/publishApproval',
            type: 'post',
            dataType: 'json',
            data: $('form#publishapproval-form').serialize(),
            success: function(data) {
                alert(data.status+":"+data.message);
            }
        });
    });
});