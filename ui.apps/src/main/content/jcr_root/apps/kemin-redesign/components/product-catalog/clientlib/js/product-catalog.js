document.addEventListener("DOMContentLoaded", function (event) {
    let resultsPerPage = parseInt($('.result-per-catalogue .active')[0].textContent);
    renderPageNavigation(productListJSON, resultsPerPage); //renders the page numbers for pagination at bottom.
    divideProductCardsInPages(productListJSON, resultsPerPage); //builds a pageToProductMap map.
    renderProductsByPageNumber(1); //by default first page will be shown everytime when a tag is clicked.
    $('.product_result h2').text(getProductCardsRange(1));
});

function showTier1TaggedPages(tag, parentCheckbox) {
    chkName = tag.name;
    var isChecked = $('input[name=' + chkName + ']:checked').length;
    if (parentCheckbox != undefined && parentCheckbox == true) {
        if (isChecked == 1) {
            $('.' + chkName + ' :checkbox:enabled').prop('checked', true);
        } else {
            $('.' + chkName + ' :checkbox:enabled').prop('checked', false);
        }
    }
    /*else if(parentCheckbox!=undefined && parentCheckbox==false){
    var parentCheckBoxElement = $("#"+tag.id).parent().parent()[0].className.replace(' ', '.');
    var siblingCount = $('.'+parentCheckBoxElement+ '  input').length;
    var checkedSiblingCount = $('.'+parentCheckBoxElement+ '  input:checked').length;
    var parantCheckBoxId = tag.id.replace('-'+chkName,'');
    if(checkedSiblingCount == 0){
    $('input[name='+parantCheckBoxId).prop('indeterminate', false);
    $('input[name='+parantCheckBoxId).prop('checked', false);
    }else if(checkedSiblingCount < siblingCount){
    $('input[name='+parantCheckBoxId).prop('indeterminate', true);
    }else if(checkedSiblingCount == siblingCount){
    $('input[name='+parantCheckBoxId).prop('indeterminate', false);
    $('input[name='+parantCheckBoxId).prop('checked', true);
    }
    }*/

    var checkedBoxNameList = [];
    $('.inner-container-accordian input:checked').each(function () {
        checkedBoxNameList.push($(this).attr('name'));
    });
    pageToProductMap = new Map();
    let resultsPerPage = parseInt($('#product_result_dropdown option:selected').val());
    if (checkedBoxNameList.length == 0) {
        userProductListJSON = [];
        renderPageNavigation(productListJSON, resultsPerPage); //renders the page numbers for pagination at bottom.
        divideProductCardsInPages(productListJSON, resultsPerPage); //builds a pageToProductMap map.
        renderProductsByPageNumber(1); //by default first page will be shown everytime when a tag is clicked.
    } else if (checkedBoxNameList.length > 0) {
        userProductListJSON = buildUserProductListJSON(checkedBoxNameList); //builds a user selected product list - userProductListJSON
        renderPageNavigation(userProductListJSON, resultsPerPage); //renders the page numbers for pagination at bottom.
        divideProductCardsInPages(userProductListJSON, resultsPerPage); //builds a pageToProductMap map.
        renderProductsByPageNumber(1); //by default first page will be shown everytime when a tag is clicked.
    }
    $('.product_result h2').text(getProductCardsRange(1));
}

function renderPagination(resultsPerPage) {
    productCount = parseInt(resultsPerPage.textContent);
    $('.result-per-catalogue .active')[0].setAttribute('class', 'page-item');
    $('#'+resultsPerPage.id).addClass("active");
    $('html, body').animate({scrollTop: $(".pdptitle").offset().top}, 1000);
    let productList = userProductListJSON.length != 0 ? userProductListJSON : productListJSON;
    renderPageNavigation(productList, productCount); //renders the page numbers for pagination at bottom.
    divideProductCardsInPages(productList, productCount); //builds a pageToProductMap map.
    renderProductsByPageNumber(1); //by default first page will be shown everytime when a tag is clicked.
    $('.product_result h2').text(getProductCardsRange(1));
}

function gotoPage(page) {
    let pageNo;
    if (page == 'next') {
        $('.pagination').animate({ scrollLeft: "+=56px" }, "fast");
        let activePageNo = $('.catalogue-pagination .user-generated').length;
        let isLastPageActive = $('.catalogue-pagination #page-item-' + activePageNo).attr("class").includes('active');
        if (!isLastPageActive)
            pageNo = parseInt($('.catalogue-pagination .pagination').find(".active")[0].textContent) + 1;
        else
            pageNo = null;
    } else if (page == 'prev') {
        $('.pagination').animate({ scrollLeft: "-=56px" }, "fast");
        let isLastPageActive = $('.catalogue-pagination #page-item-1').attr("class").includes('active');
        if (!isLastPageActive)
            pageNo = parseInt($('.catalogue-pagination .pagination').find(".active")[0].textContent) - 1;
        else
            pageNo = null;
    } else {
        pageNo = parseInt(page.textContent);
    }

    if (pageNo !== null) {
        $(".productCatalogue-container .product-row .productCard").css('display', 'none');
        renderProductsByPageNumber(pageNo);
        $(".catalogue-pagination .pagination .page-item").removeClass('active');
        $(".catalogue-pagination .pagination #page-item-" + pageNo).addClass('active');
        $('.product_result h2').text(getProductCardsRange(pageNo));

        if (pageNo == 1) {
            $('.catalogue-pagination .move-prev').css('display','none');//addClass('disable');
            $('.catalogue-pagination .move-next').css('display','block');//removeClass('disable');
        } else if (pageNo == $('.catalogue-pagination .user-generated').length) {
            $('.catalogue-pagination .move-next').css('display','none');//addClass('disable');
            $('.catalogue-pagination .move-prev').css('display','block');//removeClass('disable');
        } else {
            $('.catalogue-pagination .move-prev').css('display','block');//removeClass('disable');
            $('.catalogue-pagination .move-next').css('display','block');//removeClass('disable');
        }
        $('html, body').animate({scrollTop: $(".pdptitle").offset().top}, 1000);
    }

}
function pageNumberGrouping(noOfPages) {
    let groupCount = (noOfPages - 5) + 1
    mapOfGroup = new Map();
    for (var i = 1; i <= groupCount; i++) {
        let group = [];
        for (var j = i, k = 1; k <= 5; k++, j++) {
            group.push(j);
        }
        mapOfGroup.set(i, group);
    }
}

function renderPageNavigation(productListJSON, perPageProduct) {
    let productCount = productListJSON.length;
    let noOfPages = Math.ceil(productCount / perPageProduct);
    //render the page numbers based on the noOfPages
    $('.catalogue-pagination .pagination .user-generated').remove();
    $('.catalogue-pagination .next-wrap').remove();
    for (var i = 2; i <= noOfPages; i++) {
        listItem = `<li class="page-item user-generated" id="page-item-` + i + `" style="cursor: pointer;" onclick="gotoPage(this);">` + i + `</li>`;
        $('.catalogue-pagination .pagination').append(listItem);
    }
    $('.catalogue-pagination').append(`</ul><ul class="next-wrap"><li class="page-item user-generated move-next" style="cursor: pointer;" onclick="gotoPage('next');"><span class="icon-chevron-right"></span></li> </ul>`);

    if (noOfPages == 1) {
        $('.catalogue-pagination .move-next').addClass('disable');
    }
    //create a new class "icon-chevron-LR" for the button  '...' and show it when number of pages are greater than 5.
    if (noOfPages > 5) {
        $('.icon-chevron-LR').css('display', 'block');
    } else {
        $('.icon-chevron-LR').css('display', 'none');
    }
}

function divideProductCardsInPages(productListJSON, productCount) {
    //create a temporary Map of page to products, to serve product data based on page number.
    let counter = 1,
    pageNo = 1;
    let tempProductArray = [];
    for (let product of productListJSON) {
        tempProductArray.push(product)
        if (counter % productCount == 0) {
            pageToProductMap.set(pageNo, tempProductArray);
            tempProductArray = [];
            pageNo += 1;
        }
        counter += 1
    }
    if (tempProductArray.length > 0)
        pageToProductMap.set(pageNo, tempProductArray);

}

function removeProductCards() {
    //first remove all the existing productcards from container.
    $(".productCatalogue-container .product-row .productCard").remove();
}
function undefinedCheck(object) {
    return object != undefined || object != null ? object : '';
}

function renderProductsByPageNumber(pageNo) {
    //on Page Load, iterate over the complete list of products mapped to the pageNo and render it inside the productCatalogue-Container.
    let productList = pageToProductMap.get(pageNo);
    removeProductCards();
    let i = 1;
    for (let productItem of productList) {
        let product = JSON.parse(productItem);
        let altImageName = getFileName(product.productLogoImage);
        let productCard = `<div class="col-lg-6 col-md-6 ` + undefinedCheck(product.tagClass) + ` productCard productCard-` + i + `" id="productCard-` + undefinedCheck(product.productNumber) + `">
				<a href="` + undefinedCheck(product.productURL) + `.html">
                	<div class="featureCatalogue">
                        <div class="feature-card">
                            <!-- card body-->
                            <img src="` + undefinedCheck(product.productLogoImage) + `" alt="` + altImageName + `">
                            <div class="card-text">
                                 <p class="product-title">` + undefinedCheck(product.productCatalogName) + `</p>
                                 <p class="product-card">` + undefinedCheck(product.productDescription) + `</<p>
                             </div>
                         </div>
                     </div>
                </a>
			</div>`;
        i += 1;
        $(".productCatalogue-container .product-row").append(productCard);
    }
    $(".catalogue-pagination .pagination #page-item-" + pageNo).addClass('active');
}

function getFileName(filePath) {
    return undefinedCheck(filePath) != '' ? filePath.split('\\').pop().split('/').pop() : '';
}
function buildUserProductListJSON(checkedBoxNameList) {
    let userProductList = [];
    let uniqueProductMap = new Map();
    for (let checkItem of checkedBoxNameList) {
        let productList = productsByTagListJson[checkItem];
        for (var prod of productList) {
            prodJSONObj = JSON.parse(prod);
            uniqueProductMap.set(prodJSONObj.productNumber, prod);
        }
    }
    uniqueProductMap.forEach((value) => {
        userProductList.push(value);
    });
    return userProductList;
}

function getProductCardsRange(pageNo) {
    let totalProdCount = userProductListJSON.length != 0 ? userProductListJSON.length : productListJSON.length;
    let resultsPerPage = parseInt($('.result-per-catalogue .active')[0].textContent);

    let endRange = resultsPerPage * pageNo,
    startRange;
    if (endRange > totalProdCount) {
        startRange = endRange - (resultsPerPage - 1);
        endRange = totalProdCount;
    } else {
        startRange = endRange - (resultsPerPage - 1);
    }

    return "Showing " + startRange + " - " + endRange + " of " + totalProdCount + " results";
}
