"use strict";
use(function () {
  var CONST= {
   PROP_Checkbox:"jcr:checkbox",
   PROP_Value: "value"
}
 var checkbox = {};
checkbox.value:properties.get(CONST.PROP_CHECKBOX)
||pageProperties.get(CONST.PROP_CHECKBOX)
||currentPage.name;

checkbox.value=currentStyle.get(CONST.PROP_Value) || "false";

return checkbox;
});
