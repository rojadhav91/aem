isDesktop = false;
elements = [];
document.addEventListener("DOMContentLoaded", function (event) {
    elements = $(".layout-container-kemin");
    if (window.innerWidth >= 992) {
        isDesktop = true;
    } else if (window.innerWidth <= 991) {
        isDesktop = false;
        updatePadding();
    }
    $(window).resize(function () {
        if (window.innerWidth >= 992 && !isDesktop) {
            isDesktop = true;
            updatePadding();

        } else if (window.innerWidth <= 991 && isDesktop) {
            isDesktop = false;
            updatePadding();
        }
    });
});

function updatePadding() {
    for (var el of elements) {
        console.log("padding is " + el.style.padding);
        let currentContainerPadding = el.style.padding;
        let padding = "";
        paddingArray = currentContainerPadding.split(" ");
        for (let i = 0; i < paddingArray.length; i++) {
            let item = parseFloat(paddingArray[i].replace("px", ""));
            if (item != 0) {
                if (!isDesktop)
                    padding += (item / 2) + "px ";
                else if (isDesktop)
                    padding += (item * 2) + "px ";
            } else {
                padding += "0px ";
            }
        }
        el.style.padding = padding;
    }
}