(function($, Coral)
  {
      "use strict";
      console.log("------Clientlibs Loaded----");
      var registry= $(window).adaptTo("foundation-registry").register("foundation.validation.validator",
		{
		   selector: "[data-validation=geeks-multifield]",
		   validate: function(element)
			{
                var el = $(element);
                let max=el.data("max-items");
                let min=el.data("min-items");
                let items=el.children("coral-multifield-item").length;
                console.log("{} :{} :{}", max,min,items);
                 if (items > max)
				{
				return "Max allowed items is " + max
                }
                 if (items < min)
				{
				return "Min allowed items is " + min
                }


            }
        });
  })
(jQuery, Coral);