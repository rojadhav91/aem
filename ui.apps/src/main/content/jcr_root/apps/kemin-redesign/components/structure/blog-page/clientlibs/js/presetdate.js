(function(window, document, $, Granite) {
    "use strict";

    $(document).on("foundation-contentloaded", function(e) {
        //alert("test");
        var datepicker=$("coral-datepicker[data-id='posted']");
        if(datepicker.length>0){
            $(datepicker).closest(".coral-FixedColumn-column").css("min-height", "400px");
            setTimeout(function(){ 
                if(datepicker.val()==''){
                    var d = new Date().toISOString();
                    datepicker.val(d);
                }
            }, 500);
        }
    });

})(window, document, Granite.$, Granite);
