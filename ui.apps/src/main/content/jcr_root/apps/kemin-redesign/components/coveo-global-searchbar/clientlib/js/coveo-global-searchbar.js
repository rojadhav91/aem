console.log("initializing coveo global searchbar interface");

document.addEventListener('DOMContentLoaded', function () {
  // The Coveo JavaScript Search UI comes with a pre-configured public search endpoint
  // (a public index against which to execute queries for demo purposes).

  var language = $('#coveoGlobalSearchBarLanguage').val();
  var locale = $('#coveoGlobalSearchBarLocale').val();
  var region = $('#coveoGlobalSearchBarRegion').val();
  var searchResultsPath = $('#coveoGlobalSearchBarUrl').val();  
  var showPopularQueries = $('#coveoGlobalSearchBarUrlPopularQueries').val();
  var region = $('#coveoGlobalSearchBarRegion').val();

  const renewToken = () => {
    // Granted that the call returns the search token in text/plain.
    return fetch("/bin/coveo/token")
      .then((response) => response.json())
      .then((data) => {
        console.log("getting new token");
        return data.token;
      }).catch((error) => {
        console.error('Error:', error);
      });
  }

  const renderCommonQueries = (suggestions) => {
    $('#coveoGlobalSearchBarDivTopQueries').empty();
    $('#coveoGlobalSearchBarDivTopQueriesMobile').empty();
    //this is added since there is different html for mobile
     $('#coveoGlobalSearchBarDivTopQueriesMobile').append($("<ul></ul>"));

    if(suggestions.completions) {
      suggestions.completions.forEach((suggestion,i) => {
        if((i%3)==0) {
          $('#coveoGlobalSearchBarDivTopQueries').append($("<div class='row'></div>"));
        }
        var row = $('#coveoGlobalSearchBarDivTopQueries').children('.row').last()
        row.append(`<div class="col"><ul><li><a data-cta="${suggestion.expression}" data-at="search suggestion" data-nav="Search" data-cta-type="link" href="${searchResultsPath}#q=${suggestion.expression}">${suggestion.expression}</a></li></ul></div>`);
       //this is added since there is different html for mobile
      var rowMobile = $('#coveoGlobalSearchBarDivTopQueriesMobile').children('ul').last()
      rowMobile.append(`<li><a data-cta="${suggestion.expression}" data-at="search suggestion" data-nav="Search" data-cta-type="link" href="${searchResultsPath}#q=${suggestion.expression}">${suggestion.expression}</a></li>`);
  });
    }
  }

  var globalSearchBoxDiv = document.getElementById('coveoGlobalSearchBarDiv');
  var globalSearchBoxDivMobile = document.getElementById('coveoGlobalSearchBarDivMobile');
  var coveoSearchDiv = document.getElementById('coveoSearchDiv');

    //if the global search box exist on the same page as the main search widget, 
    // skip initialization since it will be handled as an external component to the main search interface
  if(!coveoSearchDiv){
    // Use Coveo events to set locale
    Coveo.$$(globalSearchBoxDiv).on('buildingQuerySuggest', function (e, args) {
      args.payload.locale = locale;
    });
    // this is added since there is different html for mobile
    Coveo.$$(globalSearchBoxDivMobile).on('buildingQuerySuggest', function (e, args) {
      args.payload.locale = locale;
    });
    Coveo.$$(globalSearchBoxDiv).on('buildingQuery', function (e, args) {
      args.queryBuilder.locale = locale;
    });
     // this is added since there is different html for mobile
      Coveo.$$(globalSearchBoxDivMobile).on('buildingQuery', function (e, args) {
      args.queryBuilder.locale = locale;
    });
    // Use Coveo events to set locale
    Coveo.$$(globalSearchBoxDiv).on('changeAnalyticsCustomData', function (e, args) {
      args.language = language;
      args.metaObject.context_region = region;
      args.metaObject.context_locale = locale;
    });
    // this is added since there is different html for mobile
    Coveo.$$(globalSearchBoxDivMobile).on('changeAnalyticsCustomData', function (e, args) {
      args.language = language;
      args.metaObject.context_region = region;
      args.metaObject.context_locale = locale;
    });
    // Find top suggestionts
    if(showPopularQueries == 'yes') {
      Coveo.$$(globalSearchBoxDiv).on('afterComponentsInitialization', function(e, args) {
        Coveo.SearchEndpoint.endpoints.default.getQuerySuggest({'enableWordCompletion':true,count:6,q:'',locale:locale}).done(renderCommonQueries);
      });
     //this is added since there is different html for mobile
         Coveo.$$(globalSearchBoxDivMobile).on('afterComponentsInitialization', function(e, args) {
        Coveo.SearchEndpoint.endpoints.default.getQuerySuggest({'enableWordCompletion':true,count:6,q:'',locale:locale}).done(renderCommonQueries);
      });
    }


    renewToken().then(token => {
      Coveo.SearchEndpoint.configureCloudV2Endpoint(
        undefined,
        token,
        undefined,
        {
          renewAccessToken: renewToken
        }
      );

      Coveo.initSearchbox(globalSearchBoxDiv, searchResultsPath);
      Coveo.initSearchbox(globalSearchBoxDivMobile, searchResultsPath);
     
    }).catch((error) => {
      console.error('Error:', error);
    });
  } else {
    if(showPopularQueries == 'yes') {
      // Find top suggestions when search component is on page.
      Coveo.$$(coveoSearchDiv).on('afterComponentsInitialization', function(e, args) {
        Coveo.SearchEndpoint.endpoints.default.getQuerySuggest({
            enableWordCompletion:true,
            count:6,
            q:'',
            locale:locale,
            context:{locale:locale, region: region}
          }).done(renderCommonQueries);
      });
    }
  
  }

  Coveo.globalSearchbtn = function(){
    //send enter press to support button
    $(document.getElementById('coveoGlobalSearchBarSearchBox')
          .querySelector('input'))
          .trigger($.Event("keydown", {which:13}));
  }
});
