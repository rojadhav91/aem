
window.onload = (event) => {
    let resultsPerPage = parseInt($('#blog_product_result_dropdown option:selected').val());
    for (var i = 0; i < blogsByPage.length; i++) {
        let product = JSON.parse(blogsByPage[i]);
        JSONArrayOfBlogs.push(product);
    }
    renderBlogPageNavigation(JSONArrayOfBlogs, globalPageCount);
    divideBlogCardsInPages(JSONArrayOfBlogs, globalPageCount);
    renderBlogsByPageNumber(1);
}
function showBlogPagesMobile(tag) {
    var checkedBoxNameList = [];
    chkName = tag.name;
    $('.inner-container-accordian input:checked').each(function () {
        checkedBoxNameList.push($(this).attr('id'));
    });
    getAllParentPageTitle(checkedBoxNameList);
}
function showBlogPages(tag) {
    var checkedBoxNameList = [];
    chkName = tag.name;
    $('.inner-container-accordian input:checked').each(function () {
        checkedBoxNameList.push($(this).attr('id'));
    });
    getAllParentPageTitle(checkedBoxNameList);
}
function getAllParentPageTitle(checkedBoxNameList) {
    if (checkedBoxNameList.length != 0) {
        var blogsJOSNArray = [];
        for (var j = 0; j < checkedBoxNameList.length; j++) {
            for (var i = 0; i < blogsByPage.length; i++) {
                var obj = blogsByPage[i];
                let blogs_sorted = JSON.parse(obj);
                if (blogs_sorted.parentPageTitle == checkedBoxNameList[j]) {
                    blogsJOSNArray.push(blogs_sorted);
                }
            }
        }
        JSONArrayOfBlogs = blogsJOSNArray;
        renderBlogPageNavigation(JSONArrayOfBlogs, globalPageCount);
        divideBlogCardsInPages(JSONArrayOfBlogs, globalPageCount);
        renderBlogsByPageNumber(1);
    } else {
        for (var i = 0; i < blogsByPage.length; i++) {
            let product = JSON.parse(blogsByPage[i]);
            JSONArrayOfBlogs.push(product);
        }
        renderBlogPageNavigation(JSONArrayOfBlogs, globalPageCount);
        divideBlogCardsInPages(JSONArrayOfBlogs, globalPageCount);
        renderBlogsByPageNumber(1);
    }
}
function renderBlogPagination(resultsPerPage) {

    productCount = parseInt(resultsPerPage.value);
     $(".product_result_dropdwn .pagination .page-item").removeClass('active');
            $(".product_result_dropdwn .pagination #page-item-" + productCount).addClass('active');
    globalPageCount = productCount;
    removeBlogCards();
    divideBlogCardsInPages(JSONArrayOfBlogs, globalPageCount);
    renderBlogPageNavigation(JSONArrayOfBlogs, globalPageCount);
    renderBlogsByPageNumber(1);
}
function renderBlogPageNavigation(blogListJSON, perPageProduct) {
    let productCount = blogListJSON.length;
    let noOfPages = Math.ceil(productCount / perPageProduct);
    //render the page numbers based on the noOfPages
    $('.catalogue-pagination .pagination .user-generated').remove();
    $('.catalogue-pagination .next-wrap').remove();
    for (var i = 2; i <= noOfPages; i++) {
        listItem = `<li class="page-item user-generated" id="page-item-` + i + `" style="cursor: pointer;" onclick="gotoPage(this);">` + i + `</li>`;
        $('.catalogue-pagination .pagination').append(listItem);
    }
    $('.catalogue-pagination').append(`</ul><ul class="next-wrap"><li class="page-item user-generated move-next" style="cursor: pointer;" onclick="gotoPage('next');"><span class="icon-chevron-right"></span></li> </ul>`);

    if (noOfPages == 1) {
        $('.catalogue-pagination .move-next').addClass('disable');
    }
    //create a new class "icon-chevron-LR" for the button  '...' and show it when number of pages are greater than 5.
    if (noOfPages > 5) {
        $('.icon-chevron-LR').css('display', 'block');
    } else {
        $('.icon-chevron-LR').css('display', 'none');
    }
}
//blog map created here
function divideBlogCardsInPages(blogListJSON, blogCount) {
    //create a temporary Map of page to products, to serve product data based on page number.
    let counter = 1,
    pageNo = 1;
    let tempProductArray = [];
    for (let product of blogListJSON) {
        tempProductArray.push(product)
        if (counter % blogCount == 0) {
            pageToBlockMap.set(pageNo, tempProductArray);
            tempProductArray = [];
            pageNo += 1;
        }
        counter += 1;
    }
    if (tempProductArray.length > 0) {
        pageToBlockMap.set(pageNo, tempProductArray);
    }
}
function removeBlogCards() {
    $(".blogsRow").empty();
}
function renderBlogsByPageNumber(pageNo) {
    let blogList = pageToBlockMap.get(pageNo);
    removeBlogCards();
    let i = 1;
    for (let blogItem of blogList) {
    let url= blogItem.path.concat(".html");

        var myString = '<div class="col-lg-4 col-md-6" >' +
            '<div class="featureCatalogue">' +
            ' <div class="feature-card">' +
            '<a href=' + url + '>'+
            '<img class="blog-image" src=' + blogItem.thumbnailImage + ' alt=""></a>' +
            '<div class="card-text">' +
            '<p class="card-title">' + blogItem.title + '</p>' +
            '<div class="more-text"><a href=' + url + '>' +
            '<span class="icon-chevron-right"></span></a></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        i += 1;
        $(".blogsRow").append(myString);
    }
}
function gotoPage(page) {
    let pageNo;
    if (page == 'next') {
        $('.pagination').animate({ scrollLeft: "+=56px" }, "fast");
        $('.catalogue-pagination .prev-wrap .move-prev').css('display','block');
        let activePageNo = $('.catalogue-pagination .user-generated').length;
        let isLastPageActive = $('.catalogue-pagination #page-item-' + activePageNo).attr("class").includes('active');
        if (!isLastPageActive)
            pageNo = parseInt($('.catalogue-pagination .pagination').find(".active")[0].textContent) + 1;
        else
            pageNo = null;
    } else if (page == 'prev') {
        $('.pagination').animate({ scrollLeft: "-=56px" }, "fast");
       // $('.catalogue-pagination .prev-wrap .move-prev').css('display','block');
        let isLastPageActive = $('.catalogue-pagination #page-item-1').attr("class").includes('active');
        if (!isLastPageActive)
            pageNo = parseInt($('.catalogue-pagination .pagination').find(".active")[0].textContent) - 1;
        else
            pageNo = null;
    } else {
        pageNo = parseInt(page.textContent);
    }
    if (pageNo !== null) {
        renderBlogsByPageNumber(pageNo);
        $(".catalogue-pagination .pagination .page-item").removeClass('active');
        $(".catalogue-pagination .pagination #page-item-" + pageNo).addClass('active');
        $('.product_result h2').text(getProductCardsRange(pageNo));
        if (pageNo == 1) {
            $('.catalogue-pagination .move-prev').addClass('disable');
            $('.catalogue-pagination .move-next').removeClass('disable');
        } else if (pageNo == $('.catalogue-pagination .user-generated').length) {
            $('.catalogue-pagination .move-next').addClass('disable');
           $('.catalogue-pagination .move-prev').removeClass('disable');
            $('.catalogue-pagination .prev-wrap .move-prev').css('display','block');
        } else {
            $('.catalogue-pagination .move-prev').removeClass('disable');
            $('.catalogue-pagination .move-next').removeClass('disable');
        }
    }
}