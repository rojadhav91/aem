(function ($, Coral) 
{ 
  $(window).adaptTo("foundation-registry").register("foundation.validation.validator", { 
    selector: "[data-foundation-validation=phoneValidate]", 
	 validate: function(el) { 
     var regex_pattern = /^\(\d{3}\) \d{3}-\d{4}$/;
            var error_message = "Please insert Phone no in (000) 000-0000 format  only";
         var phoneLength = el.value.length;
            var result = el.value.match(regex_pattern);

     if (result === null) {

                return error_message;
            }
    } 
  }); 
}) 
($, Coral); 