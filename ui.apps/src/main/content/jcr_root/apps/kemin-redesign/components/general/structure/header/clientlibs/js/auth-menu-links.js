document.addEventListener('DOMContentLoaded', function () {

       $(".auth-links-menu").find('li').removeClass('displayToNotLoggedInUsersOnly');
       $(".auth-links-menu").find('li').removeClass('hideToNotLoggedInUsersOnly');


       $("li").closest(".auth-showhide-links").each(function(i, element) {
                 var displayLinkProp = $(this).data("display-link-prop");

                 if(displayLinkProp == "alwaysShow"){
                    $(".auth-links-menu").find('li').show();
                 }else if(displayLinkProp == "showToLoggedInUsersOnly"){
                        var customerData = getCustomerData();
                        if(typeof customerData != 'undefined'){
                        var isAuthenticated = customerData.authenticated;

                     if((typeof isAuthenticated != 'undefined' && isAuthenticated === true)){
                        $(this).addClass('displayToNotLoggedInUsersOnly');
                     }
                  }else{
                     $(this).addClass('hideToNotLoggedInUsersOnly');
                  }

                 }else if(displayLinkProp == "showToNotLoggedInUsersOnly"){
                        var customerData = getCustomerData();
                        if(typeof customerData != 'undefined'){
                        var isAuthenticated = customerData.authenticated;
                          if((typeof isAuthenticated === 'undefined' || isAuthenticated === false)){
                            $(this).addClass('displayToNotLoggedInUsersOnly');
                        }else{
                            $(this).addClass('hideToNotLoggedInUsersOnly');
                        }

                 }else{
                         $(this).addClass('displayToNotLoggedInUsersOnly');
                 }
               }

       });

       function getCustomerData(){
           var customerData;
           var storeKey = localStorage.getItem("mage-cache-storage");
            if(storeKey !== null){
            var parsedStoreKey = JSON.parse(storeKey);
            customerData = parsedStoreKey.customer;
            }
            return customerData;
        }

});