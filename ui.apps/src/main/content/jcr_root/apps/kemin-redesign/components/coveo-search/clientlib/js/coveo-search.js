console.log("initializing coveo interface");

document.addEventListener('DOMContentLoaded', function () {
  // The Coveo JavaScript Search UI comes with a pre-configured public search endpoint
  // (a public index against which to execute queries for demo purposes).

  var language = $('#coveoSearchLanguage').val();
  var locale = $('#coveoSearchLocale').val();
  var region = $('#coveoSearchRegion').val();
  var showRelatedQueries = $('#coveoSearchRelatedQueries').val();
    
  const renewToken = () => {
    // Granted that the call returns the search token in text/plain.
    return fetch("/bin/coveo/token")
      .then((response) => response.json())
      .then((data) => {
        console.log("getting new token");
        return data.token;
      }).catch((error) => {
        console.error('Error:', error);
      });
  }

  const renderSuggestions = (suggestions) => {
    $('#coveoSearchDivSuggestions').empty();
    $('#coveoSearchDivSuggestionsHeader').hide();    
    if(suggestions.completions && suggestions.completions.length > 0) {
      $('#coveoSearchDivSuggestionsHeader').show();
      suggestions.completions.forEach((suggestion, i) => {
        if((i%3)==0) {
          $('#coveoSearchDivSuggestions').append($("<div class='row'></div>"));
        }
        var row = $('#coveoSearchDivSuggestions').children('.row').last()
        row.append(`<div class="col"><ul><li><a data-cta="${suggestion.expression}" data-at="search suggestion" data-nav="Search" data-cta-type="link" href="#q=${suggestion.expression}">${suggestion.expression}</a></li></ul></div>`);      });
    }
  }

  var coveoSearchDiv = document.getElementById('coveoSearchDiv');
  // Use Coveo events to set locale
  Coveo.$$(coveoSearchDiv).on('buildingQuery', function (e, args) {
    args.queryBuilder.locale = locale;
    args.queryBuilder.summaryLength = 200;
  });
  // Use Coveo events to set locale
  Coveo.$$(coveoSearchDiv).on('buildingQuerySuggest', function (e, args) {
    args.payload.locale = locale;
  });
  // Use Coveo events to set locale
  Coveo.$$(coveoSearchDiv).on('changeAnalyticsCustomData', function (e, args) {
    args.language = language;
    args.metaObject.context_region = region;
    args.metaObject.context_locale = locale;
  });
  Coveo.$$(coveoSearchDiv).on("afterComponentsInitialization", function() {
    Coveo.$$(coveoSearchDiv).on("populateBreadcrumb", function(e, args) {
      //need to remove the duplicate breadcrumbs caused by having two facets on this interface
      args.breadcrumbs = args.breadcrumbs.slice(0,Math.ceil(args.breadcrumbs.length/2));
    });
  });
  // Find top suggestions
  if(showRelatedQueries == "yes") {    
    Coveo.$$(coveoSearchDiv).on('querySuccess', function(e, args) {
      $('#coveoSearchDivSuggestions').empty();
      $('#coveoSearchDivSuggestionsHeader').hide();    

      if(args.query.q && args.query.q != "") {
        if (window.digitalData) {
          window.digitalData.search.searchInfo.numOfResults = args.results.totalCount;
          window.digitalData.search.searchInfo.searchTerm = args.query.q;
        }
        Coveo.SearchEndpoint.endpoints.default.getQuerySuggest({
            enableWordCompletion:true,
            count:6, 
            q:args.query.q, 
            locale:locale,
            context: {
              locale:locale,
              region:region
            }
          }).done(renderSuggestions);
      }
    });
    Coveo.$$(coveoSearchDiv).on('noResults', function(e, args) {
      $('#coveoSearchDivSuggestions').empty();
      $('#coveoSearchDivSuggestionsHeader').hide();    
    });
  }else{
    Coveo.$$(coveoSearchDiv).on('querySuccess', function(e, args) {
      if(args.query.q && args.query.q != "") {
        if (window.digitalData) {
          window.digitalData.search.searchInfo.numOfResults = args.results.totalCount;
          window.digitalData.search.searchInfo.searchTerm = args.query.q;
          window.digitalData.search.searchInfo.selectedFilters = "";
          for(var i=0;i<args.query.facets.length;i++){
            var oneFacet = args.query.facets[i];
            for(var j=0;j<oneFacet.currentValues.length;j++){
              var oneCurrentValues = oneFacet.currentValues[j];
              if("selected"==oneCurrentValues.state 
                && window.digitalData.search.searchInfo.selectedFilters.indexOf(oneCurrentValues.value)<0){
                window.digitalData.search.searchInfo.selectedFilters+=oneCurrentValues.value+",";
              }
            }
          }
          window.digitalData.search.searchInfo.pageNumber = Coveo.$$(document.getElementsByClassName('CoveoPager')).el[0].CoveoPager.currentPage;
        }
      }

      $(document).on('click','.CoveoResultLink', function (e) {
        window.digitalData.search.searchInfo.searchResultClick = $(this).html();
      });
    });
  }
  
  
  //if the the global search box exist on the page, we must init it as an external component
  var globalSearchBoxDiv = document.getElementById('coveoGlobalSearchBarSearchBox');
 var globalSearchBoxDivMobile = document.getElementById('coveoGlobalSearchBarSearchBoxMobile');
  var externalSearchComponents = [];
var externalSearchComponentsMobile = [];
  if(globalSearchBoxDiv) {
    externalSearchComponents.push(globalSearchBoxDiv);
  }
// this is added since there is different html for mobile 
 if(globalSearchBoxDivMobile) {
    externalSearchComponents.push(globalSearchBoxDivMobile);
  }
  renewToken().then(token => {
    Coveo.SearchEndpoint.configureCloudV2Endpoint(
      undefined,
      token,
      undefined,
      {
        renewAccessToken: renewToken
      }
    );
    Coveo.init(coveoSearchDiv, { 
      externalComponents: externalSearchComponents
    });

  }).catch((error) => {
    console.error('Error:', error);
  });
  
  Coveo.searchbtn = function(){
	  var rootElement = document.getElementById('coveoSearchDiv');
    Coveo.executeQuery(rootElement);
  }
  checkPaginationCreated();
});

function checkPaginationCreated() {
    if (document.getElementsByClassName("coveo-pager-list-item").length > 0) {
        clearTimeout();
        $(".CoveoPager .coveo-pager-list-item, .coveo-results-per-page-list-item-text").click(function () {
            $('html, body').animate({
                scrollTop: $(".coveo-results-header").offset().top
            }, 1000);
            setTimeout(checkPaginationCreated, 1000);
        });

    } else {
        setTimeout(checkPaginationCreated, 1000);
    }
}
