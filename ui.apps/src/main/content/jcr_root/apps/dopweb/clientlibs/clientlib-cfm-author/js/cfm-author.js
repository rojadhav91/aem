(function ($) {
    'use strict';

    let CFM;

    function loadMarket() {
        if (window.Dam !== undefined) {
            CFM = window.Dam.CFM;
            getMarket();
        }
    }

    function cleanDynamicFields() {
        const textAreaSelect = $('#aem-cfm-editor-elements-form textarea[name="zipcodes"]');
        const coralTextAreaSelect = textAreaSelect.next().find('span[role="presentation"]');
        const marketNameSelect = $('#aem-cfm-editor-elements-form input[name="marketName"]');
        textAreaSelect.val('');
        textAreaSelect.text('');
        textAreaSelect.html('');
        coralTextAreaSelect.val('');
        coralTextAreaSelect.text('');
        coralTextAreaSelect.html('');
        marketNameSelect.val('');
    }

    $(window).load(function() {
        loadMarket();
        const textAreaSelect = $('#aem-cfm-editor-elements-form textarea[name="zipcodes"]');
        const coralTagListSelect = $('#aem-cfm-editor-elements-form coral-taglist[name="marketId"]');
        textAreaSelect.attr('disabled', true);
        textAreaSelect.next().attr('disabled', true);
        coralTagListSelect.on('DOMNodeInserted', loadMarket);
        coralTagListSelect.on('DOMNodeRemoved', cleanDynamicFields);
        $('#aem-cfm-editor-elements-form input[name="marketName"]').attr('disabled', true);
    });

    function getMarket() {
        if (CFM.EditSession !== undefined) {
            const marketTag = $('#aem-cfm-editor-elements-form coral-taglist[name="marketId"] coral-tag').val();

            if (marketTag !== undefined) {
                const marketZipCodesUrl = "/content/cq:tags/" + marketTag.replace(":", "/") + "/zip-codes.1.json";
                const marketTagUrl = "/content/cq:tags/" + marketTag.replace(":", "/") + ".json";
                $.ajax(marketZipCodesUrl).done(loadZipCodesJsonField);
                $.ajax(marketTagUrl).done(data => $('#aem-cfm-editor-elements-form input[name="marketName"]').val(data['jcr:title']));
            } else {
                cleanDynamicFields();
            }
        }
    }

    function loadZipCodesJsonField(data) {
        const regex = /^\d+$/;
        const zipCodesArray = [];

        for (const key in data) {
            if (regex.test(key) && data[key]['cq:lastReplicationAction'] === 'Activate') {
                zipCodesArray.push(key);
            }
        }

        const textAreaSelect = $('#aem-cfm-editor-elements-form textarea[name="zipcodes"]');
        const coralTextAreaSelect = textAreaSelect.next().find('span[role="presentation"]');
        const htmlZipCodes = '[<span class="cm-number">' + zipCodesArray.join('</span>,<span class="cm-number">') + '</span>]';
        const stringZipCodes = '[' + zipCodesArray.join(',') + ']';
        textAreaSelect.val(stringZipCodes);
        textAreaSelect.text(stringZipCodes);
        textAreaSelect.html(stringZipCodes);
        coralTextAreaSelect.val(stringZipCodes);
        coralTextAreaSelect.text(stringZipCodes);
        coralTextAreaSelect.html(htmlZipCodes);
    }

}(jQuery));