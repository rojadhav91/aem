(function ($, window) {
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
            selector: "coral-checkbox[name='./isDesktopBGImg']",
            validate: function (el) {
				var isDesktopBGImgRequired = el.checked;
				var desktopBGImgParent = el.closest('.coral-Form-fieldwrapper').closest('.coral-FixedColumn-column');
				var desktopBGImgPath = desktopBGImgParent.querySelector('.cq-FileUpload[name="./imgDesktop/file"]');
                var isDesktopImgAuthored = desktopBGImgPath.querySelector('input[name="./imgDesktop/backgroundImageReference"]').value.length;
                if(isDesktopBGImgRequired){
                    if(!isDesktopImgAuthored){
                        return "Desktop Image Required";
                    }
                }

            }
        });
    }
)($, window);