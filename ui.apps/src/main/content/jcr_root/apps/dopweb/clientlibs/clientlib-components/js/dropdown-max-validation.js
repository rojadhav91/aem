(function ($, window) {
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
            selector: "[data-foundation-validation^='dropdown-max']",
            validate: function (el) {
                var validationName = el.getAttribute("data-validation")
                var max = validationName.replace("dropdown-max-", "");
                max = parseInt(max);
                if (el.selectedItems.length > max) {
                    return "Max allowed items to be selected " + max;
                }
            }
        });
    }
)($, window);