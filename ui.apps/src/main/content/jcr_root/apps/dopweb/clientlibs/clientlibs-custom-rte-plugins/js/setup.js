
var ACT = ACT || {};
ACT.rte = ACT.rte || {
    GROUP: "act",

    // Commands
    COMMAND: {
   		STYLES: "actstyles"
	},

    DEBUG: {
        ALL: false,
        STYLESELECTOR: false,
        TOOLKITIMPL: false,
        TOOLBARBUILDER: false,
        STYLECOMMAND: false,
        FONTCOLORCORE: false,
        FONTSTYLE: false,
        FONTCOLORBOOST: false,
		FONTCOLORGENESIS: false
    },

    // Features
    FEATURE: {
        FONT_COLOR_CORE: {
            NAME: "fontcolorcore",
            ID: "#fontcolorcore",
            POPID: "fontcolorcore:getStyles:styles-pulldown",
            ICON: "coral-Icon coral-Icon--coreColorStyle"
        },
        FONT_STYLE: {
            NAME: "fontstyles",
            ID: "#fontstyles",
            POPID: "fontstyles:getStyles:styles-pulldown",
            ICON: "coral-Icon coral-Icon--textStyle"
        },
        FONT_COLOR_BOOST: {
            NAME: "fontcolorboost",
            ID: "#fontcolorboost",
            POPID: "fontcolorboost:getStyles:styles-pulldown",
            ICON: "coral-Icon coral-Icon--boostColorStyle"
        },
        FONT_COLOR_GENESIS: {
            NAME: "fontcolorgenesis",
            ID: "#fontcolorgenesis",
            POPID: "fontcolorgenesis:getStyles:styles-pulldown",
            ICON: "coral-Icon coral-Icon--genesisColorStyle"
        }
    },

    STYLE_TAG: "span",

    STYLEABLE_OBJECTS: [
    	"img"
	]
};
ACT.rte.commands = {};
ACT.rte.plugins = {};
ACT.rte.ui = {};