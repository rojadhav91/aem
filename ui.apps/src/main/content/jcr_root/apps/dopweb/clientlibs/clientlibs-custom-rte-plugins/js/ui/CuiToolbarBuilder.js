ACT.rte.ui.CuiToolbarBuilder = new Class({
    
    toString: "ACTCuiToolbarBuilder",
    
    extend: CUI.rte.ui.cui.CuiToolbarBuilder,

    _getUISettings: function (options) {
        var uiSettings = this.superClass._getUISettings(options);

        if(ACT.rte.DEBUG.TOOLBARBUILDER || ACT.rte.DEBUG.ALL) {
        	console.log("ACTCuiToolbarBuilder - _getUISettings()");
        }

        //inline toolbar
       // var items = uiSettings["inline"]["popovers"]["format"].items;


	//add plugins to inline toolbar
        var toolbar = uiSettings["dialogFullScreen"]["toolbar"];
		var popovers = uiSettings["dialogFullScreen"]["popovers"];

        // Font Color - Core
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_COLOR_CORE.ID) == -1) {
        	toolbar.splice(toolbar.length, 0, ACT.rte.FEATURE.FONT_COLOR_CORE.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_COLOR_CORE.NAME)) {
            popovers.fontcolorcore = {
            	"ref": ACT.rte.FEATURE.FONT_COLOR_CORE.NAME,
                "items": ACT.rte.FEATURE.FONT_COLOR_CORE.POPID
            };
        }
        //-- This would search coral icon has added if not added registered it
        if (!this._getIconForCommand(ACT.rte.FEATURE.FONT_COLOR_CORE.ID)) {
        	this.registerIcon(ACT.rte.FEATURE.FONT_COLOR_CORE.ID, ACT.rte.FEATURE.FONT_COLOR_CORE.ICON);
        }

        // Font Color - Boost
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_COLOR_BOOST.ID) == -1) {
        	toolbar.splice(toolbar.length, 0, ACT.rte.FEATURE.FONT_COLOR_BOOST.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_COLOR_BOOST.NAME)) {
            popovers.fontcolorboost = {
            	"ref": ACT.rte.FEATURE.FONT_COLOR_BOOST.NAME,
                "items": ACT.rte.FEATURE.FONT_COLOR_BOOST.POPID
            };
        }
        //-- This would search coral icon has added if not added registered it
        if (!this._getIconForCommand(ACT.rte.FEATURE.FONT_COLOR_BOOST.ID)) {
        	this.registerIcon(ACT.rte.FEATURE.FONT_COLOR_BOOST.ID, ACT.rte.FEATURE.FONT_COLOR_BOOST.ICON);
        }

        // Font Color - Genesis
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_COLOR_GENESIS.ID) == -1) {
        	toolbar.splice(toolbar.length, 0, ACT.rte.FEATURE.FONT_COLOR_GENESIS.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_COLOR_GENESIS.NAME)) {
            popovers.fontcolorgenesis = {
            	"ref": ACT.rte.FEATURE.FONT_COLOR_GENESIS.NAME,
                "items": ACT.rte.FEATURE.FONT_COLOR_GENESIS.POPID
            };
        }
        //-- This would search coral icon has added if not added registered it
        if (!this._getIconForCommand(ACT.rte.FEATURE.FONT_COLOR_GENESIS.ID)) {
        	this.registerIcon(ACT.rte.FEATURE.FONT_COLOR_GENESIS.ID, ACT.rte.FEATURE.FONT_COLOR_GENESIS.ICON);
        }

        /*
        if (!this._getClassesForCommand(ACT.rte.FEATURE.FONT_COLOR.ID)) {
        	this.registerAdditionalClasses(ACT.rte.FEATURE.FONT_COLOR.ID, ACT.rte.FEATURE.FONT_COLOR.ICON);
        }*/

        // Font Styles
        if (toolbar.indexOf(ACT.rte.FEATURE.FONT_STYLE.ID) == -1) {
        	toolbar.splice(toolbar.length, 0, ACT.rte.FEATURE.FONT_STYLE.ID);
        }
		if (!popovers.hasOwnProperty(ACT.rte.FEATURE.FONT_STYLE.NAME)) {
            popovers.fontstyles = {
            	"ref": ACT.rte.FEATURE.FONT_STYLE.NAME,
                "items": ACT.rte.FEATURE.FONT_STYLE.POPID
            };
        }

        //-- This would search coral icon has added if not added registered it
        if (!this._getIconForCommand(ACT.rte.FEATURE.FONT_STYLE.ID)) {
        	this.registerIcon(ACT.rte.FEATURE.FONT_STYLE.ID, ACT.rte.FEATURE.FONT_STYLE.ICON);
        }


        return uiSettings;
    },

    // Styles dropdown builder
    createACTStyleSelector: function(id, plugin, tooltip, styles) {
        if(ACT.rte.DEBUG.TOOLBARBUILDER || ACT.rte.DEBUG.ALL) {
        	console.log("ACTCuiToolbarBuilder - createACTStyleSelector()");
        }
        return new ACT.rte.ui.StyleSelectorImpl(id, plugin, false, tooltip, false,
                                                    undefined, styles);
    }
});
