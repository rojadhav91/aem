(function($, document){
    $(document).on('cq-layer-activated', function(event){
        if(event.layer === "Edit"){
           // only modal pointer-events should be active when modal-button is-active
           if ($('#ContentFrame').contents().find('html body .modal .open-modal.is-active').length) {
               $('[data-type=Editable]').each(function(idx,item){
                   const path = item.getAttribute('data-path');
                   if((new RegExp('.*(/root).*(/modal)(.*)(/container).*').test(path))){
                       item.style.pointerEvents = "auto";
                       item.style.visibility = "visible";
                   } else {
                       item.style.pointerEvents = "none";
                       item.style.visibility = "hidden";
                   }
               });
           } else {
                $('[data-type=Editable]').css('pointer-events', 'auto');
           }
        }

    });
})($, document);