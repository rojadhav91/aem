let submitClick, addErrorToDataLayer;

$(document).ready(function () {
    window.isFormStart = false;
    //hide all errorcode & successcode
    function resetErrSuccssCode() {
        $('.leadgengenesis').find(`[data-successcode='200']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='invalid-zip']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='invalid-email']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='invalid-address']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='email-already-exist']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='email-not-whitelisted']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='signup-error']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='service-availability-error']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='other-error']`).hide();
        $('.leadgengenesis').find(`[data-errorcode='hcaptcha-error']`).hide();
        $('.errormsg').hide();
    }

    //hide all leadgen for success/error forms
    resetErrSuccssCode();

    addErrorToDataLayer = function(msg, siteId) {
        var formNameVar = "email/zip form";
        if (siteId === "genesis") {
            formNameVar = "Address Form";
        }
        window.adobeDataLayer = window.adobeDataLayer || [];
        window.adobeDataLayer.push({
            event: "form_error",
            form: {
                formDetails: {
                    formError: {
                        value: 1,
                    },
                    formName: formNameVar,
                    formErrorName: msg,
                },
            },
        });
    }

    function showError(errorType) {
        if (errorType === "xhr") {
            addErrorToDataLayer("ApiProxyServlet call failed.");
        } else if (errorType === "briteVerify") {
            addErrorToDataLayer("invalid email id entered");
        } else if (errorType === "responsys") {
            addErrorToDataLayer("responsys error");
        } else if (errorType === "omnisend ") {
            addErrorToDataLayer("omnisend error");
        } else if (errorType === "invaild response") {
            addErrorToDataLayer("invalid response from server");
        }
    }

    //go back button click
    function goBackClick() {
        //Hide Qualification check form by default
        // $('#leadgenQualificationform').hide();
        $('.leadgenform').trigger("reset");

        $('#leadgenform').show();

        //Hide First Zip/Email form
        $('.leadgenform').show();
        $('.leadgenform .errormsg').hide();
        $('.leadgenform').trigger("reset");
        $('#dvZipContainer').css('display', 'block');
        $('#dvEmailContainer').css('display', 'block');
    }

    var rowNum = 0;
    submitClick = function() {
        //Analytics | Form Tracking
        window.adobeDataLayer = window.adobeDataLayer || [];
        var formNameVar = $(this).closest("form").attr('name');
        window.adobeDataLayer.push({
            event: "form_complete",
            form: {
                formDetails: {
                    formComplete: {
                        value: 1,
                    },
                    formName: formNameVar,
                },
            },
        });

        var $form = $(this).closest("form");
        //Lead Gen Form
        var formIdIndex = rowNum++;
        var actionurl = $form.siblings(".endpoint").data("path");
        var siteId = $form.siblings(".endpoint").data("site");
        var emailId = $form.find(".txtEmail").val();
        emailId = emailId && emailId.length > 0 ? emailId : sessionStorage.getItem("email");
        var zipCode = $form.find(".txtZipCode").val();
        zipCode = zipCode && zipCode.length > 0 ? zipCode : sessionStorage.getItem("zip");
        var phoneNumber = $form.find(".phoneNumber").val();
        var href = $form.find(".button > a").attr("href");
        var isBackendValidationEnabled = $form.data("bevalidation");
        var isEmailWhitelistEnabled = $form.data("emailwhitelisting");
        var isEmailValidationEnabled = $form.data("emailvalidation");
        var isZipValidationEnabled = $form.data("zipvalidation");
        var isSignupEnabled = $form.data("signup");
        var isAddressValidationEnabled = $form.data("addressvalidation");
        var isServiceAvailabilityEnabled = $form.data("service-availability-validation");
        var address = $form.find(".txtAddress").val();
        var product = $form.find("#product").val();
        var magentoFormId = $form.data("form-id");
        var redirectPath = "";
        if (typeof href !== 'undefined') {
            if (href.includes("https://my.boostmobile.com/shop/")) {
                redirectPath = href.replace("https://my.boostmobile.com/shop/", "");
            }
            else {
                redirectPath = href;
            }
        }
        $form.attr("redirect", redirectPath);
        var actionhref = $form.attr("action");
        $form.attr("id", "leadgenform_" + formIdIndex);
        var formId = $form.attr("id", "leadgenform_" + formIdIndex);
        var consentCheckboxValue = $form.find(".consentCheckbox").is(":checked");
        $('.loading-spinner').show();

        var requestData = {
            product: product && product.length > 0 ? product : undefined,
            site: siteId,
            phone: phoneNumber,
            consent: consentCheckboxValue,
            beValidation: isBackendValidationEnabled,
            emailWhitelist: isEmailWhitelistEnabled,
            signupEnabled: isSignupEnabled,
            emailValidation: isEmailValidationEnabled,
            zipValidation: isZipValidationEnabled,
            serviceAvailability: isServiceAvailabilityEnabled
        };

        if (emailId && emailId.length > 0 && emailId !== "null") {
            requestData.email = emailId;
        }
        if (zipCode && zipCode.length > 0) {
            requestData.zip = zipCode;
        }
        if (address && address.length > 0) {
            requestData.address = address;
        }

        $.ajax({
            url: actionurl,
            method: "POST",
            dataType: "json",
            data: requestData,
        })
            .done(function (response) {

                if (response && response.status) {
                    if (response.status == "success") {
                        //brite verify + responsys call successful

                        if (siteId === "boost") {
                            document.cookie = 'leadGenSuccess = true; path=/'
                            if (typeof magentoFormId != 'undefined') {
                                // Uncomment below line when integration done with Magento
                                // as of now showing console log to validate this case
                                // $formId.closest("form").submit();
                                console.log("Magento form submit call");
                                $('.loading-spinner').hide();
                            } else {
                                setTimeout(() => {
                                    $form.submit(); // replace with form reference
                                    $('.loading-spinner').hide();
                                }, 500);
                            }
                        } else if (siteId === "genesis") {
                            // Store validated email and zipcode in session storage
                            if (zipCode && zipCode.length > 0) {
                                sessionStorage.setItem("zip", zipCode);
                            }

                            if (emailId && emailId.length > 0) {
                                sessionStorage.setItem("email", emailId);
                            }

                            //show success leadgen form if it exists
                            var successDiv = $form.parent().parent().find('[data-successcode="200"]');
                            if (successDiv.length > 0) {
                                $form.parent().hide();
                                successDiv.first().show();
                                $('.loading-spinner').hide();
                            }
                            else {
                                // redirect to button url if it exists
                                if (redirectPath && redirectPath.length > 0) {
                                    window.location.href = redirectPath;
                                }
                            }


                            // $('#successmsg').html(Granite.I18n.get("Congrats, We have coverage in {0}, please confirm your full address.", sessionStorage.getItem('zip')));
                        }
                    } else {
                        document.cookie = 'leadGenSuccess = false; path=/'
                        if (siteId === "genesis") {
                            $('.leadgenform').trigger("reset");
                            $('.leadgenform').hide();
                            addErrorToDataLayer(response.errorMsg);
                            $('.loading-spinner').hide();
                        } else {
                            $form.children(".errormsg").show();
                            showError(response.api);
                            $('.loading-spinner').hide();
                        }
                    }
                } else {
                    $form.children(".errormsg").show();
                    showError("invaild response");
                    $('.loading-spinner').hide();
                }
            })
            .fail(function (jqXHR, textStatus) {
                document.cookie = 'leadGenSuccess = false; path=/'
                var errStatusCodeObj = ["invalid-zip", "invalid-email", "invalid-address","email-already-exist", "email-not-whitelisted", "signup-error", "service-availability-error", "other-error"];

                $('.loading-spinner').hide();

                if (siteId === "genesis") {
                    // Store validated email and zipcode in session storage
                    sessionStorage.setItem("zip", zipCode);
                    sessionStorage.setItem("email", emailId);
                    var statusCodeIndex = "";
                    var errorCode = jqXHR.responseJSON.errorCode;
                    if (errorCode && errorCode.length > 0) {
                        statusCodeIndex = parseInt(errStatusCodeObj.indexOf((errorCode).toString()));
                    } else {
                        statusCodeIndex = parseInt(errStatusCodeObj.indexOf(('other-error').toString()));
                    }

                    $form.parent().hide();
                    $form.parent().siblings(`[data-errorcode="${errStatusCodeObj[statusCodeIndex]}"]`).show();
                    $('.leadgenform').trigger("reset");
                    addErrorToDataLayer(jqXHR.responseJSON.errorMsg);
                    $('.loading-spinner').hide();
                } else {
                    showError("xhr");
                    $('.loading-spinner').hide();
                }
            });
    }

    function verifyEmailPattern() {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test($(".txtEmail").val())) {
            return true;
        } else {
            return false;
        }
    }

    function verifyZipPattern() {
        var zipcode = $(".txtZipCode").val();
        if (!isNaN(zipcode) && zipcode.length == 5) {
            return true;
        } else {
            return false;
        }
    }

    function disableSubmitButton(val) {
        if (val) {
            $("#btnSubmit").parent().addClass("cmp-button__disabled");
        } else {
            $("#btnSubmit").parent().removeClass("cmp-button__disabled");
        }
    }


    function ValidateZipCheck($form) {
        if ($form.find(".txtZipCode").val() && $form.find(".txtZipCode").val().length == 5) {
            return true;
        } else {
            return false;
        }
    }
    function ValidateEmailCheck($form) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test($form.find(".txtEmail").val())) {
            return true;
        } else {
            return false;
        }
    }
    function ValidateAddressCheck($form) {
        if ($form.find(".txtAddress").val() && $form.find(".txtAddress").val().length > 0) {
            var isAddressValidationChecked = $form.attr("data-addressvalidation");
            // console.log(isAddressValidationChecked);
            if (isAddressValidationChecked && isAddressValidationChecked.toLowerCase() === "true") {
                if (window.zipcodes && window.zipcodes.length > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    function ValidatePhoneCheck($form) {
        var phoneRegex = /^\d{10}$/g;
        var phone = $form.find(".phoneNumber").val();
        if ($form.find(".phoneNumber").val() && phoneRegex.test(phone)) {
            return true;
        } else {
            return false;
        }
    }

    function ValidateEmail() {
        var $form = $(this).closest("form");
        var zipcode = $form.find(".txtZipCode").val();
        var siteId = $form.siblings(".endpoint").data("site");

        var checked = 0;
        if ($form.find(".txtEmail").length > 0 && !ValidateEmailCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtZipCode").length > 0 && !ValidateZipCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtAddress").length > 0 && !ValidateAddressCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".phoneNumber").length > 0 && !ValidatePhoneCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }

        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("typing");
        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("default");
        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("error");

        if (checked) {
            $form.find(".button").addClass("cmp-button__disabled");
            $form.find(".txtEmail").closest(".input-container").addClass("typing");
            $form.find("#errorEmail").show();
        } else {
            $form.find(".button").removeClass("cmp-button__disabled");
            $form.find(".txtEmail").closest(".input-container").addClass("typing");
            $form.find("#errorEmail").hide();
        }
    }

    function ValidateZip() {
        var $form = $(this).closest("form");
        var siteId = $form.siblings(".endpoint").data("site");
        var zipcode = $(this).closest("form").find(".txtZipCode").val();

        var checked = 0;
        if ($form.find(".txtEmail").length > 0 && !ValidateEmailCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtZipCode").length > 0 && !ValidateZipCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtAddress").length > 0 && !ValidateAddressCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".phoneNumber").length > 0 && !ValidatePhoneCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }

        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("typing");
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("default");
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("error");

        if (checked) {
            $form.find(".button").addClass("cmp-button__disabled");
            $form.find(".txtZipCode").closest(".input-container").addClass("typing");
        } else {
            $form.find(".button").removeClass("cmp-button__disabled");
            $form.find(".txtZipCode").closest(".input-container").addClass("typing");
            $form.find("#errorZipcode").hide();
        }
    }

    function ValidateZipAfterFocusOut() {
        //Analytics | Form Tracking
        if (!window.isFormStart) {
            window.adobeDataLayer = window.adobeDataLayer || [];
            var formNameVar = $(this).closest("form").attr('name');
            window.adobeDataLayer.push({
                event: "form_start",
                form: {
                    formDetails: {
                        formStart: {
                            value: 1,
                        },
                        formName: formNameVar,
                    },
                },
            });
            window.isFormStart = true;
        }

        var zipcode = $(this).closest("form").find(".txtZipCode").val();
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("default");
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("typing");
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("error");
        $(this).closest("form").find(".txtZipCode").closest(".input-container").removeClass("success");
        if (!isNaN(zipcode) && zipcode.length == 5) {
            $(this).closest("form").find(".txtZipCode").closest(".input-container").addClass("success");
            $(this).closest("form").find("#errorZipcode").hide();
        } else {
            $(this).closest("form").find(".txtZipCode").closest(".input-container").addClass("error");
            $(this).closest("form").find("#errorZipcode").show();
            addErrorToDataLayer("Invalid Zip Code");
        }
    }

    function ValidatePhoneAfterFocusOut() {
        //Analytics | Form Tracking
        if (!window.isFormStart) {
            window.adobeDataLayer = window.adobeDataLayer || [];
            var formNameVar = $(this).closest("form").attr('name');
            window.adobeDataLayer.push({
                event: "form_start",
                form: {
                    formDetails: {
                        formStart: {
                            value: 1,
                        },
                        formName: formNameVar,
                    },
                },
            });
            window.isFormStart = true;
        }

        $(this).closest("form").find(".phoneNumber").closest(".input-container").removeClass("default");
        $(this).closest("form").find(".phoneNumber").closest(".input-container").removeClass("error");
        $(this).closest("form").find(".phoneNumber").closest(".input-container").removeClass("typing");
        var phoneRegex = /^\d{10}$/g;
        if (phoneRegex.test($(this).val())) {
            $(this).closest("form").find(".phoneNumber").closest(".input-container").addClass("default");
            $(this).closest("form").find("#errorEmail").hide();
        } else {

            $(this).parent(".span-element").siblings(".errorPhoneNumber").show();
            $(this).closest("form").find(".button").addClass("cmp-button__disabled");
            $(this).closest("form").find(".phoneNumber").closest(".input-container").addClass("error");
            addErrorToDataLayer("Invalid Email Address");
        }
    }
    function ValidateEmailAfterFocusOut() {
        //Analytics | Form Tracking
        if (!window.isFormStart) {
            window.adobeDataLayer = window.adobeDataLayer || [];
            var formNameVar = $(this).closest("form").attr('name');
            window.adobeDataLayer.push({
                event: "form_start",
                form: {
                    formDetails: {
                        formStart: {
                            value: 1,
                        },
                        formName: formNameVar,
                    },
                },
            });
            window.isFormStart = true;
        }

        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("default");
        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("error");
        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("typing");
        $(this).closest("form").find(".txtEmail").closest(".input-container").removeClass("success");
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test($(this).closest("form").find(".txtEmail").val())) {
            $(this).closest("form").find(".txtEmail").closest(".input-container").addClass("success");
            $(this).closest("form").find("#errorEmail").hide();
        } else {
            $(this).closest("form").find(".txtEmail").closest(".input-container").addClass("error");
            $(this).closest("form").find("#errorEmail").show();
            addErrorToDataLayer("Invalid Email Address");
        }
    }

    function acceptOnlyNumbers(event) {
        return event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 46
            ? true
            : !isNaN(Number(event.key));
    }

    function validatePhone() {
        var $form = $(this).closest("form");
        $form.find(".phoneNumber").closest(".input-container").removeClass("default");
        $form.find(".phoneNumber").closest(".input-container").removeClass("error");
        $form.find(".phoneNumber").closest(".input-container").removeClass("typing");

        var checked = 0;
        if ($form.find(".txtEmail").length > 0 && !ValidateEmailCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtZipCode").length > 0 && !ValidateZipCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtAddress").length > 0 && !ValidateAddressCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".phoneNumber").length > 0 && !ValidatePhoneCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }

        if (checked) {
            $form.find(".button").addClass("cmp-button__disabled");
            $form.find(".txtEmail").closest(".input-container").addClass("typing");
            $form.find("#errorPhoneNumber").show();
        } else {
            $form.find(".button").removeClass("cmp-button__disabled");
            $form.find(".txtEmail").closest(".input-container").addClass("typing");
            $form.find("#errorPhoneNumber").hide();
        }
    }

    function lookupAddress() {
        var isCheckout = true;
        var selected = '';
        var partialAddress = '13';
        //var configData = (isCheckout) ? window.checkoutConfig.smartystreets : window.autocompleteConfig.smartystreets;
        var lookupUrl = "https://us-autocomplete-pro.api.smartystreets.com/lookup?";
        $.ajax({
            url: lookupUrl,
            data: { "auth-id": "113676394146972187", "search": partialAddress, "selected": (selected ? selected : "") },
            dataType: "jsonp",
            success: function (data) {
                return data.suggestions;
                // if (data.suggestions) { showResults(data.suggestions); }
                // else { jqueryResponse([{ label: $t('No results found.'), value: -1 }]); }
            },
            error: function (error) {
                //handleError(error);
            }
        });
    }


    function bindAutoSuggestionData(event) {
        // if (event.keyCode === 13) {
        //   return false;
        // }
        $form = $(this).closest("form");
        $form.find("#myInputautocomplete-list").css("display", "block");
        $form.find("#myInputautocomplete-list").html('');
        var selected = '';
        var partialAddress = $form.find(".txtAddress").val();
        var lookupUrl = "https://us-autocomplete-pro.api.smartystreets.com/lookup?";
        var apiKey = $("#leadgenform").attr("data-auth-id");

        var checked = 0;
        if ($form.find(".txtEmail").length > 0 && !ValidateEmailCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtZipCode").length > 0 && !ValidateZipCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".txtAddress").length > 0 && !ValidateAddressCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }
        if ($form.find(".phoneNumber").length > 0 && !ValidatePhoneCheck($form)) {
            $form.find(".button").addClass("cmp-button__disabled");
            checked = 1;
        }

        if (checked) {
            $form.find(".button").addClass("cmp-button__disabled");
            $form.find(".txtAddress").closest(".input-container").addClass("typing");
            // $form.find("#errorPhoneNumber").show();
        } else {
            $form.find(".button").removeClass("cmp-button__disabled");
            $form.find(".txtAddress").closest(".input-container").addClass("typing");
            $form.find("#errorAddress").hide();
        }

        if (partialAddress && partialAddress != '') {
            $form.find("#errorAddress").css("display", "none");
            $.ajax({
                url: lookupUrl,
                data: { "auth-id": apiKey, "search": partialAddress, "selected": (selected ? selected : "") },
                dataType: "jsonp",
                success: function (data) {
                    var innerHtmlContent = '';
                    for (var i = 0; i < data.suggestions.length; i++) {
                        innerHtmlContent += '<div class="dvOption"><span class="streetLine" street_line="' + data.suggestions[i].street_line + '" secondary="' + data.suggestions[i].secondary + '" city="' + data.suggestions[i].city + '" state="' + data.suggestions[i].state + '" zip="' + data.suggestions[i].zipcode + '">' + data.suggestions[i].street_line + '</span><br/><span> ' + data.suggestions[i].city + ', ' + data.suggestions[i].state + ' ' + data.suggestions[i].zipcode + '</span></div>';
                    }
                    $form.find("#myInputautocomplete-list").html(innerHtmlContent);
                }
            });
        }
        else {
            var innerHtmlContent = '';
            $form.find("#myInputautocomplete-list").html(innerHtmlContent);
            $form.find("#errorAddress").css("display", "block");
            $form.find(".button").addClass("cmp-button__disabled");
        }
    }

    function ValidateAddressAfterFocusOut() {
        console.log("test");
        var partialAddress = $("#txtAddress").val();
        console.log(partialAddress);
        sessionStorage.setItem("fullAddress", partialAddress);

        $(this).closest("form").find(".txtAddress").closest(".addressIcon").removeClass("default");
        $(this).closest("form").find(".txtAddress").closest(".addressIcon").removeClass("typing");
        $(this).closest("form").find(".txtAddress").closest(".addressIcon").removeClass("error");
        $(this).closest("form").find(".txtAddress").closest(".addressIcon").removeClass("success");
        var isAddressValidationChecked = $(this).closest("form").attr("data-addressvalidation");
        if (isAddressValidationChecked && isAddressValidationChecked.toLowerCase() === "true") {
            if (partialAddress && partialAddress != '') {
                // $(this).closest("form").find(".txtAddress").closest(".input-container").addClass("default");
                $("#errorAddress").css("display", "none");
                if (partialAddress.length > 0) {
                    var lastFiveCharAddress = partialAddress.slice(-5);
                    if (window.zipcodes && window.zipcodes.length > 0) {
                        if (window.zipcodes.indexOf(lastFiveCharAddress) != -1) {
                            $(this)
                                .closest("form")
                                .find(".button")
                                .removeClass("cmp-button__disabled");
                            $(this).closest("form").find(".txtAddress").closest(".addressIcon").addClass("success");
                            var city = sessionStorage.getItem("city");
                            if (city == undefined || city == null) {
                                sessionStorage.setItem("zip", lastFiveCharAddress);
                                sessionStorage.setItem("address", partialAddress.substring(0, partialAddress.length - 5));
                                sessionStorage.setItem("address", sessionStorage.getItem("fullAddress").split(",")[0])
                                sessionStorage.setItem("address2", '')
                                sessionStorage.setItem("city", sessionStorage.getItem("fullAddress").split(",")[1])
                                sessionStorage.setItem("state", sessionStorage.getItem("fullAddress").split(",")[2])
                            }
                        }
                        else {
                            $("#errorAddress").css("display", "block");
                            $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                            $(this)
                                .closest("form")
                                .find(".button")
                                .addClass("cmp-button__disabled");
                            $(this).closest("form").find(".txtAddress").closest(".addressIcon").addClass("error");
                        }
                    }
                }
                else {
                    $("#errorAddress").css("display", "block");
                    $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                    $(this)
                        .closest("form")
                        .find(".button")
                        .addClass("cmp-button__disabled");
                    $(this).closest("form").find(".txtAddress").closest(".addressIcon").addClass("error");
                }
            } else {
                $(this).closest("form").find(".txtAddress").closest(".addressIcon").addClass("error");
                $("#errorAddress").css("display", "block");
                $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                var genesisSiteId = $(this).closest("form").data("site");
                addErrorToDataLayer("Enter Valid Address", genesisSiteId);
                $(this)
                    .closest("form")
                    .find(".button")
                    .addClass("cmp-button__disabled")
            }
        }
        else {
            if (partialAddress && partialAddress != '') {
                if (partialAddress.length > 0) {
                    var lastFiveCharAddress = partialAddress.slice(-5);
                    sessionStorage.setItem("zip", lastFiveCharAddress);
                    sessionStorage.setItem("address", partialAddress.substring(0, partialAddress.length - 5));
                    sessionStorage.setItem("address", sessionStorage.getItem("fullAddress").split(",")[0])
                    sessionStorage.setItem("address2", '')
                    sessionStorage.setItem("city", sessionStorage.getItem("fullAddress").split(",")[1])
                    sessionStorage.setItem("state", sessionStorage.getItem("fullAddress").split(",")[2])
                    $("#errorAddress").css("display", "none");
                    $(this)
                        .closest("form")
                        .find(".button")
                        .removeClass("cmp-button__disabled");
                }
                else {
                    $("#errorAddress").css("display", "block");
                    $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                    $(this)
                        .closest("form")
                        .find(".button")
                        .addClass("cmp-button__disabled")
                }
            }
            else {
                $("#errorAddress").css("display", "block");
                $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                $(this)
                    .closest("form")
                    .find(".button")
                    .addClass("cmp-button__disabled")
            }
        }
    }

    function bindValueInTextBox() {
        var $form = $(this).closest("form");
        var zip = $(this).closest('div').find('span').attr('zip');
        var isAddressValidationChecked = $(this).closest("form").attr("data-addressvalidation");
        $("#errorAddress").css("display", "none");
        if (isAddressValidationChecked && isAddressValidationChecked.toLowerCase() === "true") {
            if (window.zipcodes && window.zipcodes.length > 0) {
                if (window.zipcodes.indexOf(zip) != -1) {
                    $form.find(".button").removeClass("cmp-button__disabled");
                    $(this).closest("form").find(".txtAddress").closest(".addressIcon").addClass("success");
                }
                else {
                    $("#errorAddress").css("display", "block");
                    $("#errorAddress").html(Granite.I18n.get("Enter Valid Address."));
                    $form.find(".button").addClass("cmp-button__disabled");
                    $form.find(".txtAddress").closest(".addressIcon").addClass("error");
                }
            }
        }
        var streetLine = $(this).closest('div').find('span').attr('street_line');
        var city = $(this).closest('div').find('span').attr('city');
        var state = $(this).closest('div').find('span').attr('state');
        sessionStorage.setItem("address", streetLine);
        sessionStorage.setItem("address2", $(this).closest('div').find('span').attr('secondary'));
        sessionStorage.setItem("city", city);
        sessionStorage.setItem("state", state);
        sessionStorage.setItem("zip", zip);
        $form.find(".txtAddress").val(streetLine + ', ' + city + ', ' + state + ', ' + zip);
        $form.find("#myInputautocomplete-list").css("display", "none");
    }

    function fromSessionStorage(key) {
        let value = sessionStorage.getItem(key);
        if (!value) {
            value = '';
        }
        return value;
    }

    function getAddressExceptZipFromSessionStorage() {
        const addressFields = ['address', 'address2', 'city', 'state'];
        return addressFields.map(field => fromSessionStorage(field)).filter(field => field != '').join(', ');
    }

    function storeAdressToOmnisend(event) {
        event.preventDefault();
        var $form = $(this).closest("form");
        var formIdIndex = rowNum++;
        var actionurl = $form.siblings(".endpoint").data("path");
        var siteId = $form.siblings(".endpoint").data("site");
        var emailId = sessionStorage.getItem("email");
        var address = getAddressExceptZipFromSessionStorage();
        var zipCode = sessionStorage.getItem("zip");
        var href = $form.find(".button > a").attr("href");

        var actionhref = $form.attr("action");
        $form.attr("id", "leadgenform_" + formIdIndex);
        var formId = $form.attr("id", "leadgenform_" + formIdIndex);

        $('.loading-spinner').show();
        $.ajax({
            url: actionurl,
            method: "POST",
            dataType: "json",
            data: {
                email: emailId,
                zip: zipCode,
                address: address,
                site: siteId,
            },
        })
            .done(function (response) {
                if (response && response.status) {
                    if (response.status == "success") {
                        window.location.href = href;
                        $('.loading-spinner').hide();
                    }
                }
            })
            .fail(function (jqXHR, textStatus) {
                $form.children(".errormsg").show();
                showError("xhr");
                $('.loading-spinner').hide();
            });
    }

    function checkFormButtonDisabled() {
        $(".leadgenform").each(function () {
            $form = $(this).closest("form");
            var emailId = $form.find(".txtEmail").length;
            var zipCode = $form.find(".txtZipCode").length;
            var phoneNumber = $form.find(".phoneNumber").length;
            var address = $form.find(".txtAddress").length;
            if (emailId || zipCode || phoneNumber || address) {
                $(this).find(".button").addClass("cmp-button__disabled");
            } else {
                $(this).find(".button").removeClass("cmp-button__disabled");
            }
        })
    }

    checkFormButtonDisabled();

    $(".txtEmail").on("keypress keydown keyup", ValidateEmail);
    $(".txtZipCode").on("keypress keydown keyup", ValidateZip);
    //hcaptcha binding
    if ($('.leadgenform[data-validate-hcaptcha]').data('validate-hcaptcha')) {
        $('.leadgenform[data-validate-hcaptcha]').children('.button').find('a.h-captcha').click(function(event){
            event.preventDefault();
            hcaptcha.execute();
        });
    } else {
        $(".leadgenform .button .cmp-button").on("click", function(event) {
            event.preventDefault();
            submitClick();
        });
    }
    $("#leadgenQualificationform .button").on("click", goBackClick);
    $(".txtZipCode").on("focusout", ValidateZipAfterFocusOut);
    $(".txtEmail").on("focusout", ValidateEmailAfterFocusOut);
    $(".txtZipCode").on("keydown", acceptOnlyNumbers);
    $(".phoneNumber").on("keypress keydown keyup", validatePhone);
    $(".phoneNumber").on("focusout", ValidatePhoneAfterFocusOut);
    $(".txtAddress").on("keyup", bindAutoSuggestionData);
    $(document).on("click", ".dvOption", bindValueInTextBox);
    $("#leadgengenesisform2 .address-form-button").on("click", storeAdressToOmnisend);
    $(".txtAddress").on("focusout", ValidateAddressAfterFocusOut);
    $(document).click(function (e) {
        e.stopPropagation();
        var container = $(".txtAddress");
        //check if the clicked area is dropDown or not
        if (container.has(e.target).length === 0) {
            $("form #myInputautocomplete-list").css("display", "none");
        }
    })
    $('.root').after("<div class='loading-spinner'><div class='loading-container animation-2'> <div class='shape shape1'></div> <div class='shape shape2'></div> <div class='shape shape3'></div> <div class='shape shape4'></div> </div></div>");
});

function onSubmit(token) {
    var HCaptchaActionUrl = $('.leadgenform[data-validate-hcaptcha]').siblings(".endpoint").data("path");
    HCaptchaActionUrl = HCaptchaActionUrl.replace(".leadgen.api", ".siteverify.api");
    var hCaptchaRequestData = {
        hCaptchaResponse: token
    };
    $.ajax({
        url: HCaptchaActionUrl,
        method: "POST",
        dataType: "json",
        data: hCaptchaRequestData,
    }).done(function (response) {
        if (response.success) {
            submitClick();
        } else {
            $form.parent().hide();
            $form.parent().siblings(`[data-errorcode="hcaptcha-error"]`).show();
            $('.leadgenform').trigger("reset");
            addErrorToDataLayer("HCaptcha Error");
            $('.loading-spinner').hide();
        }
    }).fail(function () {
        $form.parent().hide();
        $form.parent().siblings(`[data-errorcode="hcaptcha-error"]`).show();
        $('.leadgenform').trigger("reset");
        addErrorToDataLayer("HCaptcha Error");
        $('.loading-spinner').hide();
    });
}