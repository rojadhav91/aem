<%@include file="/libs/foundation/global.jsp" %>
<%@taglib prefix="sling2" uri="http://sling.apache.org/taglibs/sling" %>
<sling2:adaptTo var="cell" adaptable="${slingRequest}" adaptTo="com.dish.dopweb.core.models.CustomReportCellValue" />
<td is="coral-table-cell" value="${sling2:encode(val,'HTML_ATTR')}">
	<c:choose>
		<c:when test="${cell.array}">
			<ul>
				<c:forEach var="val" items="${cell.value}">
					<li>
						${val}
					</li>
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			${cell.value}
		</c:otherwise>
	</c:choose>
</td>