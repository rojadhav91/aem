(function ($, document, ns) {
       $(document).on("dialog-ready", function() {
       var dropDownOptions = $("coral-select[name='./accordionOptions']").closest(".coral3-Select");
       var selectedOptionBefore;
           $("coral-select[name='./accordionOptions']").closest(".coral3-Select").children('coral-select-item').each(function(i, element) {
            if($(this).attr('selected') === 'selected'){
               selectedOptionBefore = $(this).val();
            }
       });
       iconViewShowHide(selectedOptionBefore);
       $(dropDownOptions).on("change",function (event) {
            var selectedOptionAfter = $("input[name='./accordionOptions']").val();
            iconViewShowHide(selectedOptionAfter);
   			});
       });

       function iconViewShowHide(selectedOptionAfter){
           if(selectedOptionAfter == "iconview"){
                        $(".hidePropertiesClass").closest('.coral-Form-fieldwrapper').hide();
                        $(".coral-Form--vertical .coral-Form-field.iconSelectionClass.coral3-Multifield").css("display", "block");
                        $(".coral-Form--vertical .coral-Form-field.iconSelectionClass.coral3-Multifield").closest('.coral-Form-fieldwrapper').show();
                  }else {
                        $(".hidePropertiesClass").closest('.coral-Form-fieldwrapper').show();
                        $(".coral-Form--vertical .coral-Form-field.iconSelectionClass.coral3-Multifield").css("display", "none");
                        $(".coral-Form--vertical .coral-Form-field.iconSelectionClass.coral3-Multifield").closest('.coral-Form-fieldwrapper').hide();
                   }
       }
})(Granite.$, document, Granite.author);