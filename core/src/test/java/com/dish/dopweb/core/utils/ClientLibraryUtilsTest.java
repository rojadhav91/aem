package com.dish.dopweb.core.utils;

import com.adobe.cq.wcm.core.components.models.ClientLibraries;
import com.adobe.granite.ui.clientlibs.LibraryType;
import com.day.cq.commons.Externalizer;
import com.google.common.collect.ImmutableMap;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class ClientLibraryUtilsTest {

  private final AemContext context = new AemContext();

  private String[] categories;

  @Mock ClientLibraries clientLibraries;

  @BeforeEach
  void setup() {
    final MockSlingHttpServletRequest request = context.request();
    categories = new String[] {"category-one"};

    context.registerAdapter(
        ResourceResolver.class,
        Externalizer.class,
        MockExternalizerFactory.getExternalizerService());
    context.registerAdapter(SlingHttpServletRequest.class, ClientLibraries.class, clientLibraries);

    // mock client library calls
    when(clientLibraries.getCssIncludes())
        .thenReturn(
            "<link href=\"/etc.clientlibs/css/path.css\" rel=\"stylesheet\" type=\"text/css\" />");
    when(clientLibraries.getJsIncludes())
        .thenReturn("<script src=\"/etc.clientlibs/js/path.js\"></script>");
  }

  @Test
  void emptyConstructor()
      throws InstantiationException, IllegalAccessException, NoSuchMethodException {
    final Constructor<ClientLibraryUtils> constructor =
        ClientLibraryUtils.class.getDeclaredConstructor();

    assertTrue(Modifier.isPrivate(constructor.getModifiers()));

    constructor.setAccessible(true);
    Throwable currentException = null;
    try {
      constructor.newInstance();
    } catch (InvocationTargetException e) {
      currentException = e;
    }
    assertEquals("Utility class", currentException.getCause().getMessage());
  }

  @Test
  void clientLibraryPaths() {
    final List<String> clientLibraryPathsCss =
        ClientLibraryUtils.clientLibraryPaths(categories, LibraryType.CSS, context.request());
    assertEquals(1, clientLibraryPathsCss.size());
    assertEquals("/etc.clientlibs/css/path.css", clientLibraryPathsCss.get(0));

    final List<String> clientLibraryPathsJs =
        ClientLibraryUtils.clientLibraryPaths(categories, LibraryType.JS, context.request());
    assertEquals(1, clientLibraryPathsJs.size());
    assertEquals("/etc.clientlibs/js/path.js", clientLibraryPathsJs.get(0));
  }

  @Test
  void clientLibraryPathsAbsolutePaths() {
    context.request().setParameterMap(ImmutableMap.of("getAbsolutePaths", "yes"));

    final List<String> clientLibraryPathsCss =
        ClientLibraryUtils.clientLibraryPaths(categories, LibraryType.CSS, context.request());
    assertEquals(1, clientLibraryPathsCss.size());
    assertEquals("https://publish.org/etc.clientlibs/css/path.css", clientLibraryPathsCss.get(0));

    final List<String> clientLibraryPathsJs =
        ClientLibraryUtils.clientLibraryPaths(categories, LibraryType.JS, context.request());
    assertEquals(1, clientLibraryPathsJs.size());
    assertEquals("https://publish.org/etc.clientlibs/js/path.js", clientLibraryPathsJs.get(0));
  }
}
