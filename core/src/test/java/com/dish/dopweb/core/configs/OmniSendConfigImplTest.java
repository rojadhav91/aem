package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.OmniSendConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OmniSendConfigImplTest {

    private final String OMNISEND_KEY = "omni";
    private final String OMNISEND_ENDPOINT = "www.omnisend.com/dummy";

    OmniSendConfigImpl omniSendConfigImpl = new OmniSendConfigImpl();

    @Mock
    OmniSendConfigImpl.Config omniSendConfig;

    @Test
    public void testKey(){
        when(omniSendConfig.omniSendKey()).thenReturn(OMNISEND_KEY);
        omniSendConfigImpl.activate(omniSendConfig);
        Assertions.assertEquals(OMNISEND_KEY,omniSendConfigImpl.getOmniSendKey());
    }
    @Test
    public void testEndpoint(){
        when(omniSendConfig.omniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
        omniSendConfigImpl.activate(omniSendConfig);
        Assertions.assertEquals(OMNISEND_ENDPOINT,omniSendConfigImpl.getOmniSendEndpoint());
    }
}
