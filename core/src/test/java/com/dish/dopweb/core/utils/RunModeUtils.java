package com.dish.dopweb.core.utils;

import com.adobe.acs.commons.util.ModeUtil;
import io.wcm.testing.mock.aem.junit5.AemContext;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.cm.ConfigurationException;

import java.util.*;
import java.util.stream.Stream;

public class RunModeUtils {
  private static void doWithRunMode(AemContext context, String runmode, Runnable r)
      throws ConfigurationException {
    Set<String> originalRunModes = setRunMode(context, runmode);
    r.run();
    setRunMode(context, originalRunModes.toArray(String[]::new));
  }

  public static void doInAuthorInstance(AemContext context, Runnable r)
      throws ConfigurationException {
    doWithRunMode(context, "author", r);
  }

  public static void doInPublishInstance(AemContext context, Runnable r)
      throws ConfigurationException {
    doWithRunMode(context, "publish", r);
  }

  private static Set<String> setRunMode(AemContext context, String... runmodesToOverride)
      throws ConfigurationException {
    SlingSettingsService slingSettingsService = context.getService(SlingSettingsService.class);
    Set<String> originalRunmodes =
        Optional.ofNullable(slingSettingsService)
            .map(SlingSettingsService::getRunModes)
            .orElseGet(Collections::emptySet);

    String[] runmodes =
        Stream.concat(
                Arrays.stream(runmodesToOverride),
                originalRunmodes.stream().filter("author"::equals).filter("publish"::equals))
            .toArray(String[]::new);
    context.runMode(runmodes);
    ModeUtil.configure(Objects.requireNonNull(slingSettingsService));
    return originalRunmodes;
  }
}
