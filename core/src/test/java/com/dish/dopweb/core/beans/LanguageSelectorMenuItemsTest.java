package com.dish.dopweb.core.beans;

import com.dish.dopweb.core.models.LanguageSelector;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class LanguageSelectorMenuItemsTest {

    private LanguageSelectorMenuItems languageSelectorMenuItems;
    private final AemContext context = new AemContext();

    @Mock
    private ModelFactory modelFactory;

    @BeforeEach
    public void setup() throws Exception {
        context.addModelsForClasses(LanguageSelectorMenuItems.class);
        Resource resource = context.load().json("/com/dish/dopweb/core/models/languageselector.json",
                "/dopweb/components/structure/languageselector");
        languageSelectorMenuItems =
                context.resourceResolver()
                        .getResource("/dopweb/components/structure/languageselector/languageSelectorMenuItems/item0")
                        .adaptTo(LanguageSelectorMenuItems.class);
    }

    @Test
    void testLanguageTitle(){
        assertEquals("en",languageSelectorMenuItems.getLanguageTitle());
    }

    @Test
    void testLanguageIsoCode(){
        assertEquals("ENGLISH",languageSelectorMenuItems.getLanguageIsoCode());
    }

    @Test
    void testLanguageDefault(){
        assertTrue(languageSelectorMenuItems.getIsDefaultLanguage());
    }



}
