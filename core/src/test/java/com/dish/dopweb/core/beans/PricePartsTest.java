package com.dish.dopweb.core.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PricePartsTest {

  PriceParts price;

  @BeforeEach
  void setup() {}

  @Test
  void getPrice() {
    price = new PriceParts("10.01");
    assertEquals("10.01", price.getPrice());
  }

  @Test
  void getIntegral() {
    price = new PriceParts("10.01");
    assertEquals("10", price.getIntegral());
  }

  @Test
  void getFractional() {
    price = new PriceParts("10.01");
    assertEquals("01", price.getFractional());
  }
}
