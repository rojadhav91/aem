package com.dish.dopweb.core.listeners;



import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
@Component(
        service = ResourceChangeListener.class,
        property = {
                ResourceChangeListener.PATHS + "=" + "/content/genesis",
                ResourceChangeListener.CHANGES + "=" + "ADDED",
                ResourceChangeListener.CHANGES + "=" + "CHANGED",
                ResourceChangeListener.CHANGES + "=" + "REMOVED"
        }
)
public class DynamicLeadgenListener implements ResourceChangeListener {
private static final Logger logger= LoggerFactory.getLogger(DynamicLeadgenListener.class);
    @Override
    public void onChange(List<ResourceChange> list) {
        list.forEach(addedResource -> {
            logger.info("resource change path {}",addedResource.getPath());
        });
    }
}
