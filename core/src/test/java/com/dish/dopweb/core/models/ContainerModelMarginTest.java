package com.dish.dopweb.core.models;

import com.dish.dopweb.core.utils.ContainerUtil;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(AemContextExtension.class)
class ContainerModelMarginTest {
  private final AemContext context = new AemContext();
  ContainerModelMargin containerModel;
  private static final String CMP_CONTAINER_MARGIN = "cmp-container__margin";
  private static final String DESKTOP_VALUES = "margindesktopvalues";
  private static final String TABLET_VALUES = "margintabletvalues";
  private static final String MOBILE_VALUES = "marginmobilevalues";
  private static final String DESKTOP_DIRECTION = "desktopdirectionMargin";
  private static final String TABLET_DIRECTION = "tabletdirectionMargin";
  private static final String MOBILE_DIRECTION = "mobiledirectionMargin";

  @BeforeEach
  void setUp() {
    context.addModelsForClasses(ContainerModel.class);
    context.load().json("/com/dish/dopweb/core/models/containermargin.json", "/rootcontainer");
  }

  @Test
  void getDesktopPaddingMenuItems() throws NoSuchFieldException {
    final Resource resource = context.currentResource("/rootcontainer/container/desktopMarginMenuItems");
    containerModel = context.request().adaptTo(ContainerModelMargin.class);
    PrivateAccessor.setField(containerModel, "desktopMarginMenuItems", resource);
    assertAll(
            () -> assertEquals(1, containerModel.getDesktopMarginMenuItems().size()),
            () -> assertEquals("cmp-container__margin-bottom-1", containerModel.getDesktopMarginMenuItems().get(0))
    );
  }

  @Test
  void getTabletPaddingMenuItems() throws NoSuchFieldException {
    final Resource resource = context.currentResource("/rootcontainer/container/tabletMarginMenuItems");
    containerModel = context.request().adaptTo(ContainerModelMargin.class);
    PrivateAccessor.setField(containerModel, "tabletMarginMenuItems", resource);
    assertAll(
            () -> assertEquals(1, containerModel.getTabletMarginMenuItems().size()),
            () -> assertEquals("cmp-container__margin-bottom-2", containerModel.getTabletMarginMenuItems().get(0))
    );
  }

  @Test
  void getMobilePaddingMenuItems() throws NoSuchFieldException {
    final Resource resource = context.currentResource("/rootcontainer/container/mobileMarginMenuItems");
    containerModel = context.request().adaptTo(ContainerModelMargin.class);
    PrivateAccessor.setField(containerModel, "mobileMarginMenuItems", resource);
    assertAll(
            () -> assertEquals(1, containerModel.getMobileMarginMenuItems().size()),
            () -> assertEquals("cmp-container__margin-bottom-4", containerModel.getMobileMarginMenuItems().get(0))
    );
  }
}