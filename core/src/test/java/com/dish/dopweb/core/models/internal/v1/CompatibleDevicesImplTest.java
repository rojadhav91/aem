package com.dish.dopweb.core.models.internal.v1;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import lombok.NonNull;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class CompatibleDevicesImplTest {

    @Mock ResourceResolverFactory resourceResolverFactory;
    @Mock ResourceResolver resolver;
    @Mock Resource resource;

    @InjectMocks CompatibleDevicesImpl devices;

    AemContext aemContext = new AemContext();

    @Test
    void getDevices() throws LoginException, NoSuchFieldException
    {
        Resource resource =
                aemContext
                        .load()
                        .json(
                                "/com/dish/dopweb/core/models/internal.v1/deviceCompatibilityTags.json",
                                "/content/cq:tags/dop-web/devices-compatibility");

        devices =
                aemContext
                        .resourceResolver()
                        .getResource("/content/cq:tags/dop-web/devices-compatibility")
                        .adaptTo(CompatibleDevicesImpl.class);

        devices.initModel();

        String output =
                "{\"deviceCompatibility\":[{\"modelFamily\":[{\"id\":\"3-series\",\"name\":\"3-series\",\"devices\":[{\"id\":\"3v-5032w\",\"name\":\"3V (5032W)\"}]},{\"id\":\"7-series\",\"name\":\"7-series\",\"devices\":[{\"id\":\"7-6062w\",\"name\":\"7 (6062W)\"}]}],\"id\":\"alcatel\",\"name\":\"Alcatel\"},{\"modelFamily\":[{\"id\":\"iphone\",\"name\":\"iPhone\",\"devices\":[{\"id\":\"iphone-6-and-up\",\"name\":\"iPhone 6 \\u0026 Up\"}]}],\"id\":\"apple\",\"name\":\"Apple\"}]}";
        assertEquals(output, devices.getDevices());
    }
}
