package com.dish.dopweb.core.utils;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
public class ResourceResolverUtilTest {

    @Mock
    ResourceResolverFactory resourceResolverFactory;
    @Mock
    Map<String, Object> params;
    @Mock
    ResourceResolver resourceResolver;


    @Test
    void emptyConstructor() throws InstantiationException, IllegalAccessException, NoSuchMethodException {
        final Constructor<ResourceResolverUtil> constructor = ResourceResolverUtil.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        Throwable currentException = null;
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            currentException = e;
        }
        assertEquals("Utility class",currentException.getCause().getMessage());
    }

    @Test
    void getServiceResourceResolverMapParams() throws LoginException {
        when(resourceResolverFactory.getServiceResourceResolver(params)).thenReturn(resourceResolver);
        assertNotNull(ResourceResolverUtil.getServiceResourceResolver(resourceResolverFactory, params));
    }

    @Test
    void getServiceResourceResolverMapParamsException() throws LoginException {
        when(resourceResolverFactory.getServiceResourceResolver(params)).thenThrow(LoginException.class);
        assertNull(ResourceResolverUtil.getServiceResourceResolver(resourceResolverFactory, params));
    }

    @Test
    void getServiceResourceResolverString() throws LoginException {
        when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenThrow(LoginException.class);
        assertNull(ResourceResolverUtil.getServiceResourceResolver(resourceResolverFactory, "params"));
    }

}
