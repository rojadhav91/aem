package com.dish.dopweb.core.models;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(AemContextExtension.class)
class ButtonModelMarginTest {
    private final AemContext context = new AemContext();
    ButtonModelMargin buttonModelMargin;
    private static final String CMP_CONTAINER_MARGIN = "cmp-button__bm-";
    private String desktopButtonMarginMenuItems;
    private String tabletButtonMarginMenuItems;
    private String mobileButtonMarginMenuItems;

    @BeforeEach
    void setUp() {
        context.addModelsForClasses(ButtonModelMargin.class);
        context.load().json("/com/dish/dopweb/core/models/buttonMargin.json", "/margin");
    }

    @Test
    void getDesktopButtonMarginMenuItems() throws NullPointerException{
        final Resource resource = context.currentResource("/margin/button");
        buttonModelMargin =resource.adaptTo(ButtonModelMargin.class);
        assertEquals(CMP_CONTAINER_MARGIN+8,buttonModelMargin.getDesktopButtonMarginMenuItems());
    }

    @Test
    void getTabletButtonMarginMenuItems() throws NullPointerException{
        final Resource resource = context.currentResource("/margin/button");
        buttonModelMargin = resource.adaptTo(ButtonModelMargin.class);
        assertEquals(CMP_CONTAINER_MARGIN+2,buttonModelMargin.getTabletButtonMarginMenuItems());
    }

    @Test
    void getMobileButtonMarginMenuItems() throws NullPointerException{
        final Resource resource = context.currentResource("/margin/button");
        buttonModelMargin = resource.adaptTo(ButtonModelMargin.class);
        assertEquals(CMP_CONTAINER_MARGIN+4,buttonModelMargin.getMobileButtonMarginMenuItems());
    }
}
