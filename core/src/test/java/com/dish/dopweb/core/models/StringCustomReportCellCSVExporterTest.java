package com.dish.dopweb.core.models;

import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class StringCustomReportCellCSVExporterTest {
    private static final String[] ARRAY_VALUE = new String[] { "/val1/", "val2" };

    @Mock
    Resource mockResource;

    @InjectMocks
    StringCustomReportCellCSVExporter stringCustomReportCellCSVExporter;

    @Mock
    CustomReportCellValue customReportCellValue;

    @Mock
    ValueMap valueMap;

    @BeforeEach
    void init() throws NoSuchFieldException {
        MockitoAnnotations.initMocks(this);
        when(mockResource.getValueMap()).thenReturn(valueMap);
        when(mockResource.getValueMap().containsKey(anyString())).thenReturn(true);
        when(mockResource.getValueMap().get("prop1", String.class)).thenReturn("prop1");
        when(mockResource.getValueMap().get("prop2", String.class)).thenReturn("prop2");
        PrivateAccessor.setField(stringCustomReportCellCSVExporter, "property1", "prop1");
        PrivateAccessor.setField(stringCustomReportCellCSVExporter, "property2", "prop2");
        PrivateAccessor.setField(stringCustomReportCellCSVExporter, "format", "");
        PrivateAccessor.setField(customReportCellValue, "value", "value");
        PrivateAccessor.setField(customReportCellValue, "result", mockResource);
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("prop1", ARRAY_VALUE[0]);
        properties.put("prop2", ARRAY_VALUE[1]);
    }

    @Test
    void getValue() throws Throwable {
        assertEquals("prop1is/image/prop2",stringCustomReportCellCSVExporter.getValue(mockResource));
    }
}
