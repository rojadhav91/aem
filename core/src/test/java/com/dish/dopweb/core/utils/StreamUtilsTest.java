package com.dish.dopweb.core.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StreamUtilsTest {

  List<Integer> list;

  @BeforeEach
  void setup() {
    list = List.of(1, 2, 3, 4);
  }

  @Test
  void ofIterable() {
    assertNotNull(StreamUtils.stream(list));
    assertEquals(4, StreamUtils.stream(list).count());
    assertTrue(StreamUtils.stream(list).allMatch(item -> list.contains(item)));
  }

  @Test
  void ofIterator() {
    assertNotNull(StreamUtils.stream(list.iterator()));
    assertEquals(4, StreamUtils.stream(list.iterator()).count());
    assertTrue(StreamUtils.stream(list.iterator()).allMatch(item -> list.contains(item)));
  }

  @Test
  void ofEnumeration() {
    assertNotNull(StreamUtils.stream(Collections.enumeration(list)));
    assertEquals(4, StreamUtils.stream(Collections.enumeration(list)).count());
    assertTrue(
        StreamUtils.stream(Collections.enumeration(list)).allMatch(item -> list.contains(item)));
  }
}
