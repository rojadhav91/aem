package com.dish.dopweb.core.configs.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApigeeAuthTokenRequestConfigImplTest {
    private final String CLIENT_ID = "5QvVmPiwaGFUMRsldht4z7AkLjwUAegr";
    private final String CLIENT_SECRET = "wvlwU6cA2UYOsBJB";
    private final String ENDPOINT = "https://test.api.dishcloud.io/generic/platform/v1/oauth2/token";

    ApigeeAuthTokenRequestConfigImpl apigeeAuthTokenRequestConfigService = Mockito.spy(new ApigeeAuthTokenRequestConfigImpl());

    @Mock
    ApigeeAuthTokenRequestConfigImpl.ApigeeAuthTokenRequestConfig apigeeAuthTokenRequestConfig;

    @Test
    public void testClientId(){
        when(apigeeAuthTokenRequestConfig.clientId()).thenReturn(CLIENT_ID);
        apigeeAuthTokenRequestConfigService.activate(apigeeAuthTokenRequestConfig);
        Assertions.assertEquals(CLIENT_ID,apigeeAuthTokenRequestConfigService.getClientId());
    }

    @Test
    public void testClientSecret(){
        when(apigeeAuthTokenRequestConfig.clientSecret()).thenReturn(CLIENT_SECRET);
        apigeeAuthTokenRequestConfigService.activate(apigeeAuthTokenRequestConfig);
        Assertions.assertEquals(CLIENT_SECRET,apigeeAuthTokenRequestConfigService.getClientSecret());
    }

    @Test
    public void testApiEndPoint(){
        when(apigeeAuthTokenRequestConfig.apigeeAuthTokenEndpoint()).thenReturn(ENDPOINT);
        apigeeAuthTokenRequestConfigService.activate(apigeeAuthTokenRequestConfig);
        Assertions.assertEquals(ENDPOINT,apigeeAuthTokenRequestConfigService.getApigeeAuthTokenEndpoint());
    }
}