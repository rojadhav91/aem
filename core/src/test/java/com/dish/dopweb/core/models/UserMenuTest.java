package com.dish.dopweb.core.models;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class UserMenuTest {

    private UserMenu userMenu;
    private final AemContext context = new AemContext();

    @Mock
    private ModelFactory modelFactory;

    @BeforeEach
    public void setup() throws Exception {
        context.addModelsForClasses(UserMenu.class);
        Resource resource = context.load().json("/com/dish/dopweb/core/models/usermenu.json",
                "/dopweb/components/structure/usermenu");
        userMenu =
                Objects.requireNonNull(context.getService(ModelFactory.class))
                        .createModel(resource, UserMenu.class);
    }

    @Test
    void testUserLinkItems(){
        assertNotNull(userMenu.getLinkItems());
    }


    @Test
    void testHeadingText(){assertEquals("Hello World",userMenu.getHeadingText());}
}
