package com.dish.dopweb.core.configs.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CheckServiceCoverageConfigImplTest {

    private final String ENDPOINT = "https://test.api.dishcloud.io/wireless/retail/v1/service-qualification-service/queryServiceCoverage";

    CheckServiceCoverageConfigImpl checkServiceCoverageConfigService = Mockito.spy(new CheckServiceCoverageConfigImpl());

    @Mock
    CheckServiceCoverageConfigImpl.CheckServiceCoverageConfig checkServiceCoverageConfig;

    @Test
    public void testApiEndPoint() {
        when(checkServiceCoverageConfig.checkServiceCoverageEndpoint()).thenReturn(ENDPOINT);
        checkServiceCoverageConfigService.activate(checkServiceCoverageConfig);
        Assertions.assertEquals(ENDPOINT, checkServiceCoverageConfigService.getCheckServiceCoverageEndpoint());
    }
}
