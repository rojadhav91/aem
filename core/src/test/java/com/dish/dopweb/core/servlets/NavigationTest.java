package com.dish.dopweb.core.servlets;

import com.adobe.cq.wcm.core.components.models.ClientLibraries;
import com.day.cq.commons.Externalizer;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.dish.dopweb.core.configs.NavigationConfig;
import com.dish.dopweb.core.utils.MockExternalizerFactory;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextBuilder;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.testing.mock.caconfig.MockContextAwareConfig;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.sling.testing.mock.caconfig.ContextPlugins.CACONFIG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class NavigationTest {

    public static final String SAMPLE_PAGE_JSON = "/com/dish/dopweb/core/servlets/samplePage.json";
    private static final String SAMPLE_PAGE_PATH = "/content/dopweb/sample-page";

    private final AemContext context = new AemContextBuilder().plugin(CACONFIG).build();
    private final Navigation navigation = Mockito.spy(new Navigation());

    @Mock private RequestResponseFactory requestResponseFactory;
    @Mock private SlingRequestProcessor requestProcessor;
    @Mock private ClientLibraries clientLibraries;
    @Mock private HttpServletResponse mockHttpServletResponse;
    @Mock private HttpServletRequest mockHttpServletRequest;

    @BeforeEach
    void setup() throws Exception {
        PrivateAccessor.setField(navigation, "requestResponseFactory", requestResponseFactory);
        PrivateAccessor.setField(navigation, "requestProcessor", requestProcessor);

        context.load().json("/com/dish/dopweb/core/servlets/header-activation-xf.json", "/content/experience-fragments/boost/us/en/site/header/new-header-activation");
        context.load().json("/com/dish/dopweb/core/servlets/reduced-header-xf.json", "/content/experience-fragments/boost/us/en/site/reduced-header/master");
        context.load().json("/com/dish/dopweb/core/servlets/footer-xf.json", "/content/experience-fragments/boost/us/en/site/footer/master");

        context.create().resource("/content/dopweb", "sling:configRef", "/conf/dopweb");
        context.load().json(SAMPLE_PAGE_JSON, SAMPLE_PAGE_PATH);
        context.currentPage(SAMPLE_PAGE_PATH);
        context.currentResource(SAMPLE_PAGE_PATH);

        context.registerAdapter(
                ResourceResolver.class,
                Externalizer.class,
                MockExternalizerFactory.getExternalizerService());
        context.registerAdapter(SlingHttpServletRequest.class, ClientLibraries.class, clientLibraries);

        doReturn("<header>")
                .doReturn("<footer>")
                .when(navigation)
                .getMarkup(any(String.class), any(SlingHttpServletRequest.class));

        // set client library calls
        when(clientLibraries.getCssIncludes())
                .thenReturn(
                        "<link href=\"/etc.clientlibs/css/path.css\" rel=\"stylesheet\" type=\"text/css\" />");
        when(clientLibraries.getJsIncludes())
                .thenReturn("<script src=\"/etc.clientlibs/js/path.js\"></script>");
    }

    @Test
    void doPost() {
        final MockSlingHttpServletRequest request = context.request();
        final MockSlingHttpServletResponse response = context.response();

        MockContextAwareConfig.registerAnnotationClasses(context, NavigationConfig.class);
        MockContextAwareConfig.writeConfiguration(
                context,
                SAMPLE_PAGE_PATH,
                NavigationConfig.class,
                Map.of(
                        "headerXF", new String[] {"/content/experience-fragments/boost/us/en/site/header/new-header-activation"},
                        "footerXF", new String[] {"/content/experience-fragments/boost/us/en/site/footer/master"},
                        "navigationCategories", new String[] {"dopweb.navigation"}));

        navigation.doPost(request, response);

      final HashMap result = new Gson().fromJson(response.getOutputAsString(), HashMap.class);
        final List<String> actualJsResourcePaths = (List<String>) result.get("jsResourcePaths");
        final List<String> actualCssResourcePaths = (List<String>) result.get("cssResourcePaths");
        assertEquals(HttpServletResponse.SC_OK, response.getStatus());
        assertTrue((Boolean) result.get("success"));
        assertEquals("/etc.clientlibs/js/path.js", actualJsResourcePaths.get(0));
        assertEquals("/etc.clientlibs/css/path.css", actualCssResourcePaths.get(0));
        assertNotNull(result.get("New Header Activation"));
        assertNotNull(result.get("Footer"));
    }

    @Test
    void whenNavigationConfigIsNotAvailable() {
        final MockSlingHttpServletRequest request = context.request();
        final MockSlingHttpServletResponse response = context.response();

        doReturn("").when(navigation).getMarkup(any(), any(SlingHttpServletRequest.class));

        navigation.doPost(request, response);
       final HashMap result = new Gson().fromJson(response.getOutputAsString(), HashMap.class);

        assertEquals(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response.getStatus());
        assertFalse((Boolean) result.get("success"));
        assertTrue(((List<String>) result.get("jsResourcePaths")).isEmpty());
        assertTrue(((List<String>) result.get("cssResourcePaths")).isEmpty());
    }

    @Test
    void testForAbsolutePaths() {
        final MockSlingHttpServletRequest request = context.request();
        final MockSlingHttpServletResponse response = context.response();
        request.setParameterMap(ImmutableMap.of("getAbsolutePaths", "yes"));

        MockContextAwareConfig.registerAnnotationClasses(context, NavigationConfig.class);
        MockContextAwareConfig.writeConfiguration(
                context,
                SAMPLE_PAGE_PATH,
                NavigationConfig.class,
                Map.of(
                        "headerXF", new String[] {"/content/experience-fragments/boost/us/en/site/header/new-header-activation"},
                        "footerXF", new String[] {"/content/experience-fragments/boost/us/en/site/footer/master"},
                        "navigationCategories", new String[] {"dopweb.navigation"}));

        navigation.doPost(request, response);

       final HashMap result = new Gson().fromJson(response.getOutputAsString(), HashMap.class);
        final List<String> actualJsResourcePaths = (List<String>) result.get("jsResourcePaths");
        final List<String> actualCssResourcePaths = (List<String>) result.get("cssResourcePaths");
        assertEquals(HttpServletResponse.SC_OK, response.getStatus());
        assertTrue((Boolean) result.get("success"));
        assertEquals("https://publish.org/etc.clientlibs/js/path.js", actualJsResourcePaths.get(0));
        assertEquals("https://publish.org/etc.clientlibs/css/path.css", actualCssResourcePaths.get(0));
        assertNotNull(result.get("New Header Activation"));
        assertNotNull(result.get("Footer"));
    }

    @Test
    void testForAbsolutePathsCloudFront() {
        final MockSlingHttpServletRequest request = context.request();
        final MockSlingHttpServletResponse response = context.response();
        request.setParameterMap(ImmutableMap.of("getAbsolutePaths", "yes"));

        MockContextAwareConfig.registerAnnotationClasses(context, NavigationConfig.class);
        MockContextAwareConfig.writeConfiguration(
                context,
                SAMPLE_PAGE_PATH,
                NavigationConfig.class,
                Map.of(
                        "headerXF", new String[] {"/content/experience-fragments/boost/us/en/site/header/new-header-activation"},
                        "footerXF", new String[] {"/content/experience-fragments/boost/us/en/site/footer/master"},
                        "navigationCategories", new String[] {"dopweb.navigation"}));

        request.setHeader("User-Agent", "Amazon CloudFront");
        request.setRemoteHost("remotehost");
        navigation.doPost(request, response);

        final HashMap result = new Gson().fromJson(response.getOutputAsString(), HashMap.class);
        final List<String> actualJsResourcePaths = (List<String>) result.get("jsResourcePaths");
        final List<String> actualCssResourcePaths = (List<String>) result.get("cssResourcePaths");
        assertEquals(HttpServletResponse.SC_OK, response.getStatus());
        assertTrue((Boolean) result.get("success"));
        assertEquals("https://commerce.com/etc.clientlibs/js/path.js", actualJsResourcePaths.get(0));
        assertEquals("https://commerce.com/etc.clientlibs/css/path.css", actualCssResourcePaths.get(0));
        assertNotNull(result.get("New Header Activation"));
        assertNotNull(result.get("Footer"));
    }
}
