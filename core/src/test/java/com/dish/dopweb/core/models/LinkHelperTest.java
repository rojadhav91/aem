package com.dish.dopweb.core.models;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.dish.dopweb.core.utils.MockExternalizerFactory;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.osgi.service.cm.ConfigurationException;

import java.util.Map;
import java.util.Objects;

import static com.day.cq.wcm.api.NameConstants.PN_REDIRECT_TARGET;
import static com.dish.dopweb.core.utils.RunModeUtils.doInAuthorInstance;
import static com.dish.dopweb.core.utils.RunModeUtils.doInPublishInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(AemContextExtension.class)
class LinkHelperTest {

  public final AemContext context = new AemContext();

  @ParameterizedTest(name = "[{index}]: Link: {0}")
  @CsvSource(
      value = {
        "/content/dopweb/en,/content/dopweb/en.html",
        "/content/dopweb/en?p=1,/content/dopweb/en.html?p=1",
        "/content/dopweb/en#p=1,/content/dopweb/en.html#p=1"
      })
  void testFormattedLinkWhenLinkShouldBeChanged(String testLink, String expectedLink)
      throws ConfigurationException {

    context.request().setAttribute("link", testLink);
    LinkHelper linkUtil = Objects.requireNonNull(context.request().adaptTo(LinkHelper.class));

    doInAuthorInstance(context, () -> assertEquals(expectedLink, linkUtil.getFormattedLink()));
    doInPublishInstance(context, () -> assertEquals(expectedLink, linkUtil.getFormattedLink()));
  }

  /** This test case check for external link , for external link it should append '.html'. */
  @ParameterizedTest(name = "[{index}]: Link: {0}")
  @CsvSource(
      value = {
        "https://www.dish.com/index.aspx,https://www.dish.com/index.aspx",
        "/content/dopweb/en.html,/content/dopweb/en.html",
        "/content/dam/my-image,/content/dam/my-image",
        ","
      })
  void testFormattedLinkWhenLinkShouldBeUnchanged(String testLink, String expectedLink)
      throws ConfigurationException {
    context.request().setAttribute("link", testLink);
    LinkHelper linkUtil = Objects.requireNonNull(context.request().adaptTo(LinkHelper.class));

    doInAuthorInstance(context, () -> assertEquals(expectedLink, linkUtil.getFormattedLink()));
    doInPublishInstance(context, () -> assertEquals(expectedLink, linkUtil.getFormattedLink()));
  }

  @ParameterizedTest(name = "[{index}]: Link: {0}")
  @CsvSource(
      value = {
        "/path/to/external/page,/path/to/external/page",
        "/path/to/external/page.html,/path/to/external/page.html",
        "/content/boost/us/es.html,/content/boost/us/es.html",
        "/content/boost/us/es,/content/boost/us/es.html"
      })
  void testRedirectLink(String redirectLink, String expectedLink) throws ConfigurationException {

    context.request().setAttribute("link", "/content/redirect/my-page.html");
    context
        .create()
        .resource(
            "/content/redirect/my-page/jcr:content", Map.of(PN_REDIRECT_TARGET, redirectLink));
    LinkHelper linkUtil = Objects.requireNonNull(context.request().adaptTo(LinkHelper.class));

    doInAuthorInstance(context, () -> assertEquals(expectedLink, linkUtil.getRedirectURL()));
    doInPublishInstance(context, () -> assertEquals(expectedLink, linkUtil.getRedirectURL()));
  }

  @Test
  void testActualPageName() {
    context.request().setAttribute("link", "/content/dopweb/my-page.html");
    LinkHelper linkUtil = Objects.requireNonNull(context.request().adaptTo(LinkHelper.class));
    assertEquals("my-page", linkUtil.getActualPageName());
  }

  @Test
  void testAbsolutePageUrlForBoost() throws ConfigurationException {
    context.request().setAttribute("link", "/content/boost/my-page.html");
    final Page page = context.create().page("/content/boost/my-page");
    context.currentPage(page);
    context.currentPage("/content/boost/my-page");
    context.registerService(Externalizer.class, MockExternalizerFactory.getExternalizerService());
    LinkHelper linkUtil = Objects.requireNonNull(context.request().adaptTo(LinkHelper.class));
    final Runnable callback =
        () ->
            assertEquals(
                "https://commerce.com/content/boost/my-page.html", linkUtil.getCanonicalUrl());
    doInAuthorInstance(context, callback);
    doInPublishInstance(context, callback);
  }
}
