package com.dish.dopweb.core.models;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class SVGModelTest {

  @InjectMocks SVGModel model;

  @Mock ResourceResolverFactory resourceResolverFactory;
  @Mock ResourceResolver resourceResolver;

  @Mock Resource resource;

  @Mock  Asset fileAsset;

  @Mock InputStream inputStream;

  @Mock Rendition rendition;

  @Test
  void testGetIconDropdownForResource() throws NoSuchFieldException, RepositoryException, LoginException {
    final String testIconUrl = "/content/dam/boost/web/icons/settings.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Asset.class)).thenReturn(fileAsset);
    when(fileAsset.getRendition(Mockito.anyString())).thenReturn(rendition);
    when(rendition.getStream()).thenReturn(inputStream);
    model.init();
    assertEquals("", model.getSvgContent());
  }

  @Test
  void testGetIconDropdownForNoResource() throws NoSuchFieldException, LoginException {
    final String testIconUrl = "/content/dam/boost/web/icons/settings.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(null);
    model.init();
    assertEquals(StringUtils.EMPTY, model.getSvgContent());
  }

  @Test
  void testgetResourceResolverException() throws LoginException, NoSuchFieldException {
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenThrow(LoginException.class);
    assertNull(model.getResourceResolver("apps-resources-service"));
  }

  @Test
  void getResourceStreamNodeException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "/content/dam/boost/web/icons/settings.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Asset.class)).thenReturn(fileAsset);
    when(fileAsset.getRendition(Mockito.anyString())).thenReturn(rendition);
    when(rendition.getStream()).thenReturn(inputStream);
    model.init();
    assertEquals("",model.getSvgContent());
  }

  @Test
  void getResourceStreamJcrContentException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "/content/dam/boost/web/icons/settings.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Asset.class)).thenReturn(fileAsset);
    when(fileAsset.getRendition(Mockito.anyString())).thenReturn(rendition);
    when(rendition.getStream()).thenReturn(inputStream);
    model.init();
    assertEquals("",model.getSvgContent());
  }

  @Test
  void getResourceStreamJcrDataException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "/content/dam/boost/web/icons/settings.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Asset.class)).thenReturn(fileAsset);
    when(fileAsset.getRendition(Mockito.anyString())).thenReturn(rendition);
    when(rendition.getStream()).thenReturn(inputStream);
    model.init();
    assertEquals("",model.getSvgContent());
  }

}
