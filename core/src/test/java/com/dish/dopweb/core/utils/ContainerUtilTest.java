package com.dish.dopweb.core.utils;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(AemContextExtension.class)
class ContainerUtilTest {
    public static final String PADDINGTABLETVALUES = "paddingtabletvalues";
    public static final String TABLETDIRECTION = "tabletdirection";
    private final AemContext context = new AemContext();
    private static final String CMP_CONTAINER_PADDING = "cmp-container__padding";

    @BeforeEach
    void setUp() {
        context.load().json("/com/dish/dopweb/core/utils/container.json", "/rootcontainer");
    }

    @Test
    void getResourceIterator() {
        final Resource resource = context.currentResource("/rootcontainer/container/tabletPaddingMenuItems");
        ContainerUtil.getResourceIterator(resource).forEachRemaining(resource1 -> {
            assertEquals("4", resource1.getValueMap().get("paddingtabletvalues", String.class));
        });
    }

    @Test
    void makeClassList() {
        final Resource resource = context.currentResource("/rootcontainer/container/tabletPaddingMenuItems");
            final List<String> classList = ContainerUtil.makeClassList(ContainerUtil.getResourceIterator(resource), PADDINGTABLETVALUES, TABLETDIRECTION, CMP_CONTAINER_PADDING);
            assertEquals(1, classList.size());
    }

    @Test
    void constructPaddingClass() {
        final Resource resource = context.currentResource("/rootcontainer/container/tabletPaddingMenuItems/item0");
        final String paddingtabletvalues = resource.getValueMap().get("paddingtabletvalues", String.class);
        final String direction = resource.getChild("tabletdirection").getValueMap().get("direction", String.class);
        assertEquals("cmp-container__padding-right-4", ContainerUtil.constructPaddingClass(paddingtabletvalues, direction, CMP_CONTAINER_PADDING ));
    }
}