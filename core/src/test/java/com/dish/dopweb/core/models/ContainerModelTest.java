package com.dish.dopweb.core.models;

import com.dish.dopweb.core.utils.ContainerUtil;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(AemContextExtension.class)
class ContainerModelTest {
    private final AemContext context = new AemContext();
    ContainerModel containerModel;
    private static final String CMP_CONTAINER_PADDING = "cmp-container__padding";
    private static final String DESKTOP_VALUES = "paddingdesktopvalues";
    private static final String DESKTOP_DIRECTION = "desktopdirection";
    private static final String TABLET_VALUES = "paddingtabletvalues";
    private static final String TABLET_DIRECTION = "tabletdirection";
    private static final String MOBILE_VALUES = "paddingmobilevalues";
    private static final String MOBILE_DIRECTION = "mobiledirection";

    @BeforeEach
    void setUp() {
        context.addModelsForClasses(ContainerModel.class);
        context.load().json("/com/dish/dopweb/core/utils/container.json", "/rootcontainer");
    }

    @Test
    void getDesktopPaddingMenuItems() throws NoSuchFieldException {
        final Resource resource = context.currentResource("/rootcontainer/container/desktopPaddingMenuItems");
        containerModel = context.request().adaptTo(ContainerModel.class);
        PrivateAccessor.setField(containerModel, "desktopPaddingMenuItems", resource);
        assertAll(
                () -> assertEquals(1, containerModel.getDesktopPaddingMenuItems().size()),
                () -> assertEquals("cmp-container__padding-right-2", containerModel.getDesktopPaddingMenuItems().get(0))
        );
    }

    @Test
    void getTabletPaddingMenuItems() throws NoSuchFieldException {
        final Resource resource = context.currentResource("/rootcontainer/container/tabletPaddingMenuItems");
        containerModel = context.request().adaptTo(ContainerModel.class);
        PrivateAccessor.setField(containerModel, "tabletPaddingMenuItems", resource);
        assertAll(
                () -> assertEquals(1, containerModel.getTabletPaddingMenuItems().size()),
                () -> assertEquals("cmp-container__padding-right-4", containerModel.getTabletPaddingMenuItems().get(0))
        );
    }

    @Test
    void getMobilePaddingMenuItems() throws NoSuchFieldException {
        final Resource resource = context.currentResource("/rootcontainer/container/mobilePaddingMenuItems");
        containerModel = context.request().adaptTo(ContainerModel.class);
        PrivateAccessor.setField(containerModel, "mobilePaddingMenuItems", resource);
        assertAll(
                () -> assertEquals(1, containerModel.getMobilePaddingMenuItems().size()),
                () -> assertEquals("cmp-container__padding-top-8", containerModel.getMobilePaddingMenuItems().get(0))
        );
    }
}