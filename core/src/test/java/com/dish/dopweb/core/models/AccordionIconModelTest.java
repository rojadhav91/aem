package com.dish.dopweb.core.models;

import com.adobe.cq.wcm.core.components.models.Accordion;
import com.adobe.cq.wcm.core.components.models.ListItem;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.models.factory.ModelFactory;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.osgi.framework.Constants;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class AccordionIconModelTest {

    private final AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK);

    AccordionIconModel accordionIconModel;

    @Mock
    private List<ListItem> items;

    @Mock
    private Accordion accordion;

    @Mock
    private ModelFactory modelFactory;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(AccordionIconModel.class);
        aemContext.load().json("/com/dish/dopweb/core/models/accordion.json", "/accordion");

    }

    @Test
    void init() throws NoSuchFieldException {
    }

    @Test
    void getAccordionOptions() {
        aemContext.currentResource("/accordion/accordion");
        accordionIconModel = aemContext.request().adaptTo(AccordionIconModel.class);
        assertEquals("defaultview", accordionIconModel.getAccordionOptions());
    }

    @Test
    void getIconList() {
        aemContext.currentResource("/accordion/accordion");
        accordionIconModel = aemContext.request().adaptTo(AccordionIconModel.class);
        assertEquals(2, accordionIconModel.getIconList().size());
        assertEquals("/content/dam/genesis/web/en/genesis-logos/Coverage Map (1).png", aemContext.registerService(accordionIconModel.getIconList().get(0)));
    }

    @Test
    void getIconMap() throws NoSuchFieldException {
        aemContext.load().json("/com/dish/dopweb/core/models/accordionitemslist.json", "/acc");
        aemContext.currentResource("/acc");
        accordion = aemContext.currentResource().adaptTo(Accordion.class);
        accordionIconModel = aemContext.request().adaptTo(AccordionIconModel.class);
        PrivateAccessor.setField(accordionIconModel, "accordion", accordion);
    }

    @Test
    void getIocn() {
        aemContext.currentResource("/accordion/accordion");
        accordionIconModel = aemContext.request().adaptTo(AccordionIconModel.class);
        assertEquals("/content/dam/genesis/web/en/genesis-logos/Coverage Map (1).png", accordionIconModel.getIconList().get(0));
    }
}