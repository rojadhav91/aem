package com.dish.dopweb.core.utils;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class BrandUtilTest {
    private String boost = "/content/boost/us/en/list-comp-test.html";
    private String boostBrand = "boost";
    private String genesis = "/content/genesis/us/en/list-comp-test.html";
    private String genesisBrand = "genesis";


    @Test
    public void getBoostBrand(){
        Assert.assertEquals(boostBrand, BrandUtil.getBrand(boost));
    }

    @Test
    public void getGenesisBrand(){
        Assert.assertEquals(genesisBrand, BrandUtil.getBrand(genesis));
    }

    @Test
    public void checkNull(){
        Assert.assertNull(BrandUtil.getBrand(""));
    }

}
