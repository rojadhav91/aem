package com.dish.dopweb.core.models;

import com.dish.dopweb.core.configs.SmartyStreetConfig;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, AemContextExtension.class})
class LeadgenAddressValidationModelTest {

    private LeadgenAddressValidationModel currentResource;
    private final AemContext context = new AemContext();
    private static final String APIKEY = "65464645645646";
    private static final String ENDPOINT = "https://us-street.api.smartystreets.com/street-address";
    @Mock
    SmartyStreetConfig smartyStreetConfig;

    @Test
    void testGetApiKey() throws NoSuchFieldException {

        currentResource = new LeadgenAddressValidationModel();
        when(smartyStreetConfig.getApiKey()).thenReturn(APIKEY);
        PrivateAccessor.setField(currentResource,"smartyStreetConfig", smartyStreetConfig);
        assertEquals(APIKEY,currentResource.getApiKey());
    }
}
