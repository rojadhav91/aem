package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.LaunchScriptConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LaunchScriptConfigImplTest {
    private final String ADOBE_LAUNCH_SCRIPT = "/dummy/adobe-launch-script";
    LaunchScriptConfigImpl launchScriptConfigService = new LaunchScriptConfigImpl();

    @Mock
    LaunchScriptConfigImpl.Config launchScriptConfig;

    @Test
    public void testLaunchScript(){
        when(launchScriptConfig.launchScript()).thenReturn(ADOBE_LAUNCH_SCRIPT);
        launchScriptConfigService.activate(launchScriptConfig);
        Assertions.assertEquals(ADOBE_LAUNCH_SCRIPT,launchScriptConfigService.getLaunchScript());
    }
}
