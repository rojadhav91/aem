package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.SmartyStreetConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SmartyStreetConfigServiceImplTest {
    private final String APIKEY = "33843130097947213";
    private final String ENDPOINT = "https://us-street.api.smartystreets.com/street-address";

    SmartyStreetConfigImpl smartyStreetConfigService = new SmartyStreetConfigImpl();

    @Mock
    SmartyStreetConfigImpl.SmartyStreetConfig smartyStreetConfig;

    @Test
    public void testApiKey(){
        when(smartyStreetConfig.apiKey()).thenReturn(APIKEY);
        smartyStreetConfigService.activate(smartyStreetConfig);
        Assertions.assertEquals(APIKEY,smartyStreetConfigService.getApiKey());
    }

    @Test
    public void testApiEndPoint(){
        when(smartyStreetConfig.smartyStreetEndpoint()).thenReturn(ENDPOINT);
        smartyStreetConfigService.activate(smartyStreetConfig);
        Assertions.assertEquals(ENDPOINT,smartyStreetConfigService.getSmartyStreetEndpoint());
    }
}
