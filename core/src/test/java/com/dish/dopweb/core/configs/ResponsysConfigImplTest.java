package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.ResponsysConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ResponsysConfigImplTest {
    private final String RESPONSYS_ENDPOINT = "www.responsys.com/dummy";

    ResponsysConfigImpl responsysConfigImpl = new ResponsysConfigImpl();

    @Mock
    ResponsysConfigImpl.Config responsysConfig;

    @Test
    public void testEndpoint(){
        when(responsysConfig.responsysEndpoint()).thenReturn(RESPONSYS_ENDPOINT);
        responsysConfigImpl.activate(responsysConfig);
        Assertions.assertEquals(RESPONSYS_ENDPOINT,responsysConfigImpl.getResponsysEndpoint());
    }

}
