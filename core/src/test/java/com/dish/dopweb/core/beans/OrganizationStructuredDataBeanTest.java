package com.dish.dopweb.core.beans;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class OrganizationStructuredDataBeanTest {

    @InjectMocks
    OrganizationStructuredDataBean dataBean = new OrganizationStructuredDataBean();


    @Test
    public void testGetterSetter() {
        dataBean.setContext("https://schema.org");
        dataBean.setType("Organization");
        dataBean.setName("example legal name");
        dataBean.setDescription("This is test page for organization structured data");
        dataBean.setUrl("http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html");
        dataBean.setLogo("http:localhost:4502/content/dam/boost/web/en/Banner-Background.png");
        dataBean.setSameAs(new String[]{"https://example.com/sample1", "https://example.com/sample2"});

        Assert.assertEquals("https://schema.org", dataBean.getContext());
        Assert.assertEquals("Organization", dataBean.getType());
        Assert.assertEquals("example legal name", dataBean.getName());
        Assert.assertEquals("This is test page for organization structured data", dataBean.getDescription());
        Assert.assertEquals("http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html", dataBean.getUrl());
        Assert.assertEquals("http:localhost:4502/content/dam/boost/web/en/Banner-Background.png", dataBean.getLogo());
        Assert.assertEquals(2, dataBean.getSameAs().length);
    }

}
