package com.dish.dopweb.core.configs.impl;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.dish.dopweb.core.configs.SitemapExternalizerCaConfig;
import com.dish.dopweb.core.models.ExternalizerModel;
import com.dish.dopweb.core.utils.MockExternalizerFactory;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(AemContextExtension.class)
class SitemapConfigImplTest {
    AemContext aemContext = new AemContext();

    SitemapConfigImpl sitemapConfig;

    Page page;
    
    MockSlingHttpServletRequest request;
    
    MockSlingHttpServletResponse response;

    Externalizer externalizer;

    ExternalizerModel externalizerModel;

    SitemapExternalizerCaConfig sitemapExternalizerCaConfig;
    
    SitemapConfigImpl.SitemapConfigService configService;

    @Mock
    Resource resource;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        sitemapConfig = aemContext.registerService(new SitemapConfigImpl());
        configService = mock(SitemapConfigImpl.SitemapConfigService.class);
        Externalizer externalizerService = MockExternalizerFactory.getExternalizerService();
        when(externalizerService.absoluteLink(ArgumentMatchers.any(SlingHttpServletRequest.class), anyString(), anyString())).thenReturn("http://localhost:4503/content/boost/us/en");
        when(externalizerService.externalLink(ArgumentMatchers.any(ResourceResolver.class), anyString(), anyString())).thenReturn("http://localhost:4503/content/boost/us/en");
        PrivateAccessor.setField(sitemapConfig, "externalizer", externalizerService);

        
        when(configService.includeLastmodified()).thenReturn(true);
        when(configService.characterEncoding()).thenReturn("UTF-8");
        when(configService.extensionlessURLs()).thenReturn(false);
        when(configService.removedSlash()).thenReturn(false);
        when(configService.useVanityURLs()).thenReturn(true);
        when(configService.externalizerDomain()).thenReturn("publish");
        when(configService.changeFrequencyProperties()).thenReturn(new String[] {"daily"});
        when(configService.priorityProperties()).thenReturn(new String[] {});
        when(configService.damFolderProperty()).thenReturn("assetPaths");
        when(configService.damAssetMIMETypes()).thenReturn(new String[] {"image/png"});
        
        aemContext.load().json("/com/dish/dopweb/core/servlets/enPage.json", "/page");
        aemContext.load().json("/com/dish/dopweb/core/servlets/sitemap.json", "/content/dam/boost/web/en/boost-homepage");
        resource = aemContext.currentResource("/content/dam/boost/web/en/boost-homepage");
        page = aemContext.currentPage("/page");
        request = aemContext.request();
        response = aemContext.response();
    }

    @Test
    void activate() {
    }

    @Test
    void doGetIncludeInheritValue() throws ServletException, IOException {
        when(configService.includeInheritValue()).thenReturn(true);
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
    
    @Test
    void doGetNotIncludeInheritValue() throws ServletException, IOException {
        when(configService.includeInheritValue()).thenReturn(false);
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
    
    @Test
    void doGetExternalizerDomain() throws ServletException, IOException {
        when(configService.externalizerDomain()).thenReturn("publish");
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
    
    @Test
    void doGetNoExternalizerDomain() throws ServletException, IOException {
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
    
    @Test
    void doGetExtensionlessURLs() throws ServletException, IOException {
        when(configService.extensionlessURLs()).thenReturn(true);
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
    
    @Test
    void doGetNoExtensionlessURLs() throws ServletException, IOException {
        when(configService.extensionlessURLs()).thenReturn(false);
        sitemapConfig.activate(configService);
        sitemapConfig.doGet(request, response);
    }
}
