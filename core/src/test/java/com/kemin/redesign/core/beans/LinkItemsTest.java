package com.dish.dopweb.core.beans;

import com.dish.dopweb.core.models.LanguageSelector;
import com.dish.dopweb.core.models.TopNav;
import com.dish.dopweb.core.models.UserMenu;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class LinkItemsTest {

    private LinkItems linkItems;
    private final AemContext context = new AemContext();

    @Mock
    private ModelFactory modelFactory;

    @BeforeEach
    public void setup() throws Exception {
        context.addModelsForClasses(LinkItems.class);
        Resource resource = context.load().json("/com/dish/dopweb/core/models/usermenu.json",
                "/dopweb/components/structure/usermenu");
        linkItems =
                context.resourceResolver()
                        .getResource("/dopweb/components/structure/usermenu/linkItems/item0")
                        .adaptTo(LinkItems.class);
    }

    @Test
    void testLinkTitle(){
        assertEquals("Link1",linkItems.getLinkTitle());
    }

    @Test
    void testLinkUrl(){
        assertEquals("/content/we-retail/ca",linkItems.getLinkUrl());
    }

}
