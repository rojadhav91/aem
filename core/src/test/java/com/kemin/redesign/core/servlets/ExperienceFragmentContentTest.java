package com.dish.dopweb.core.servlets;

import com.adobe.granite.ui.clientlibs.LibraryType;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.factory.ModelFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class ExperienceFragmentContentTest {

  private final AemContext context = new AemContext();

  private final ExperienceFragmentContent servlet = Mockito.spy(ExperienceFragmentContent.class);

  @Mock
  private SlingSettingsService slingSettingsService;

  @BeforeEach
  void setup() {
    context
        .load()
        .json(
            "/com/dish/dopweb/core/servlets/boost-magento-en-experience-fragments.json",
            "/content/experience-fragments/boost/us/en/site/magento/order");
  }

  @Test
  @SuppressWarnings("unchecked")
  void doGet() throws NoSuchFieldException {
    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    final String expectedResponse =
        "{\"createdAt\":\"2022-03-04T03:58:46.146+0530\",\"jsResourcePaths\":[\"/etc.clientlib/path/to/file.css\"],\"XF\":[{\"Blocks\":[{\"Id\":\"Block01\",\"Name\":\"master\",\"LastModified\":\"2022-03-04T03:36:02.000+05:30\",\"Markup\":\"\\u003ch1\\u003exf-markup\\u003c/h1\\u003e\"}],\"Page\":\"/order/confirmation/test-experience-fragment\"}],\"cssResourcePaths\":[\"/etc.clientlib/path/to/file.js\"]}";

    when(slingSettingsService.getRunModes()).thenReturn(Set.of("author"));
    PrivateAccessor.setField(servlet, "slingSettingsService", slingSettingsService);

    doReturn(List.of("/etc.clientlib/path/to/file.css"))
        .doReturn(List.of("/etc.clientlib/path/to/file.js"))
        .when(servlet)
        .clientLibraryPaths(any(LibraryType.CSS.getClass()), any(SlingHttpServletRequest.class));

    doReturn("<h1>xf-markup</h1>")
        .when(servlet)
        .getMarkup(any(SlingHttpServletRequest.class), any(Resource.class));

    request.setParameterMap(ImmutableMap.of("brand", "boost", "language", "en"));

    servlet.doGet(request, response);

    final String resultString = response.getOutputAsString();
    final Map<String, Object> result = new Gson().fromJson(resultString, HashMap.class);
    final List<Object> xf = (List<Object>) result.get("XF");
    final Map<String, Object> XF = (Map<String, Object>) xf.get(0);
    final String page = (String) XF.get("Page");

    final List<Map<String, Map<String, String>>> blocks =
        (List<Map<String, Map<String, String>>>) XF.get("Blocks");

    final Map<String, Map<String, String>> block = blocks.get(0);

    assertNotNull(result);
    assertEquals("Block01", block.get("Id"));
    assertEquals("2022-03-04T03:36:02.000+05:30", block.get("LastModified"));
    assertEquals("<h1>xf-markup</h1>", block.get("Markup"));
    assertEquals("/order/confirmation/test-experience-fragment", page);
    assertEquals(1, xf.size());
  }
}
