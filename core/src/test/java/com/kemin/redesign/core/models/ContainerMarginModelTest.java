package com.dish.dopweb.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.jcr.Session;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class ContainerMarginModelTest {
  private final AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK);
  @Mock
  ResourceResolver resourceResolver;

  @Mock
  Resource resource;

  @Mock
  SlingHttpServletRequest request;



  @Mock
  private QueryBuilder queryBuilder;

  @Mock
  private PredicateGroup predicate;

  @Mock
  private Session session ;

  @Mock
  private Query query;

  HashMap<String, String> queryMap;

  JSONObject jsonObject = new JSONObject();
  private List<String> styles = new ArrayList<>();

  @InjectMocks
  ContainerMarginModel containerMarginModel;

  @Mock
  private SearchResult searchResult;
  private List<Resource> queryResults = new ArrayList<>();


  @BeforeEach
  void setUp() {
    aemContext.addModelsForClasses(ContainerMarginModel.class);
    aemContext.load().json("/com/dish/dopweb/core/models/container.json", "/container");

  }

  @Test
  void init() throws NoSuchFieldException {
    aemContext.currentResource("/container/container");
    containerMarginModel = aemContext.request().adaptTo(ContainerMarginModel.class);
    String styleIds[]={"1614074978195","1614075079985"};
    PrivateAccessor.setField(containerMarginModel, "styleIds", styleIds);
    styles = Arrays.asList(styleIds);
    when(request.getResourceResolver()).thenReturn(resourceResolver);
    when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
     when(resourceResolver.adaptTo(Session.class)).thenReturn(session);

    //when(resource.getValueMap().get("cq:styleIds",String.class)).thenReturn(String.valueOf(styleIds));

    aemContext.registerAdapter(ResourceResolver.class, QueryBuilder.class, queryBuilder);
    lenient().when(queryBuilder.createQuery(any(PredicateGroup.class), any(Session.class))).thenReturn(query);
    lenient().when(query.getResult()).thenReturn(searchResult);
    lenient().when(searchResult.getResources()).thenReturn(queryResults.iterator());

    final Map<String, Object> map = new ConcurrentHashMap<>();
    map.put("path", "/conf/genesis/settings/wcm/policies/dopweb/components/container/policy_1612959689644");
    map.put("type", "nt:unstructured");
    map.put("1_group.p.or", "true");
    styles.forEach(s -> {
      final int index = styles.indexOf(s);
      map.put("1_group." + index + "_property", "cq:styleId");
      map.put("1_group." + index + "_property.value", s);
    });
//
//    when(queryBuilder.createQuery(Mockito.any(PredicateGroup.class), Mockito.eq(session))).thenReturn(query);

  }

//  @Test
//  void getStyleDesktopClasses() {}
//
//  @Test
//  void getStyleTabletClasses() {}
//
//  @Test
//  void getStyleMobileClasses() {}
}