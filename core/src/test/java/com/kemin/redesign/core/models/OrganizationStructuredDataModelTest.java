
package com.dish.dopweb.core.models;

import com.dish.dopweb.core.beans.OrganizationStructuredDataBean;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static junitx.util.PrivateAccessor.setField;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class OrganizationStructuredDataModelTest {
    AemContext aemContext = new AemContext(ResourceResolverType.JCR_MOCK);

    @InjectMocks
    OrganizationStructuredDataModel dataModel;

    @Mock
    OrganizationStructuredDataBean dataBean;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        setField(dataModel, "structuredHomePage", "true");
        setField(dataModel, "legalName", "example legal name");
        setField(dataModel, "logo", "http:localhost:4502/content/dam/boost/web/en/Banner-Background.png");
        setField(dataModel, "sameAs",  new String[]{"https://example.com/sample1", "https://example.com/sample2"});
        setField(dataModel, "description", "This is test page for organization structured data.");
        setField(dataModel, "request", aemContext.request());
        setField(dataModel, "falseStatus", false);
        setField(dataModel, "jsonString", "{\n" +
                "  \"@context\": \"https://schema.org\",\n" +
                "  \"@type\": \"Organization\",\n" +
                "  \"name\": \"example legal name\",\n" +
                "  \"url\": \"http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html\",\n" +
                "  \"sameAs\": [\n" +
                "    \"https://example.com/abc\",\n" +
                "    \"https://example.com/sample1\"\n" +
                "  ]\n" +
                "}");
    }

    @Test
    void init() {
        OrganizationStructuredDataBean dataBean = new OrganizationStructuredDataBean();
        dataBean.setContext("https://schema.org");
        dataBean.setType("Organization");
        dataBean.setName("legalName");
        dataBean.setUrl("http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html");
        dataBean.setLogo("http:localhost:4502/content/dam/boost/web/en/Banner-Background.png");
        dataModel.init();
    }

    @Test
    void getJsonString() {
        String testString = "{\n" +
                "  \"@context\": \"https://schema.org\",\n" +
                "  \"@type\": \"Organization\",\n" +
                "  \"name\": \"example legal name\",\n" +
                "  \"url\": \"http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html\",\n" +
                "  \"sameAs\": [\n" +
                "    \"https://example.com/abc\",\n" +
                "    \"https://example.com/sample1\"\n" +
                "  ]\n" +
                "}";
        Assert.assertEquals(testString, dataModel.getJsonString());
        dataModel.init();
    }

    @Test
    void isAllFieldSetEmpty(){
        Assert.assertEquals(true, StringUtils.isBlank("") && StringUtils.isBlank("") && ArrayUtils.isEmpty(new String[] {}));
    }

    @Test
    void isAllFieldSet() throws NoSuchFieldException {
        setField(dataModel, "legalName", null);
        setField(dataModel, "logo", null);
        setField(dataModel, "sameAs",  new String[]{});
        Assert.assertEquals(false, StringUtils.isBlank("legalName") && StringUtils.isBlank("logo") && ArrayUtils.isEmpty(new String[]{}));
        dataModel.init();
    }
}
