package com.dish.dopweb.core.models;

import com.day.cq.wcm.api.Page;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.resource.*;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ContainerHelperTest {
    @InjectMocks
    private ContainerHelper resource;

    private final AemContext context = new AemContext();

    @Mock
    private ModelFactory modelFactory;

    @Mock
    Page currentPage;

    @Mock
    private Resource img;

    @Mock
    private ValueMap valueMap;

    @Mock
    private ModifiableValueMap modifiableValueMap;

    @Mock
    private ResourceResolver resourceResolver;

    String[] coreColor;
    String[] boostColor;
    String[] dishColor;
    String[] ontechColor;
    String[] genesisColor;
    List<String[]> allColors = new ArrayList<>();

    @BeforeEach
    public void setup() throws NoSuchFieldException {

        coreColor = ArrayUtils.toArray("--color-brand-orange1","--color-brand-mid-blue");
        PrivateAccessor.setField(resource,"coreColor",coreColor);
        boostColor = ArrayUtils.toArray("--color-brand-orange1","--color-brand-mid-blue");
        PrivateAccessor.setField(resource,"boostColor",boostColor);
        dishColor = ArrayUtils.toArray("--color-brand-orange1","--color-brand-mid-blue");
        PrivateAccessor.setField(resource,"dishColor",dishColor);
        ontechColor = ArrayUtils.toArray("--color-brand-orange1","--color-brand-mid-blue");
        PrivateAccessor.setField(resource,"ontechColor",ontechColor);
        genesisColor = ArrayUtils.toArray("--color-brand-orange1","--color-brand-mid-blue");
        PrivateAccessor.setField(resource,"genesisColor",genesisColor);
    }

    @Test
    void testGetGradientStyleCoreColor() throws NoSuchFieldException {
        when(currentPage.getPath()).thenReturn("/content/genesis/us/en");
        PrivateAccessor.setField(resource,"gradient","45deg");
        assertNotNull(resource.getContainerStyle());
    }

    @Test
    void testGetGradientStyleBoostColor() throws NoSuchFieldException {
        when(currentPage.getPath()).thenReturn("/content/genesis/us/en");
        PrivateAccessor.setField(resource,"gradient","45deg");
        assertNotNull(resource.getContainerStyle());
    }

    @Test
    void testGetGradientStyleDishColor() throws NoSuchFieldException {
        when(currentPage.getPath()).thenReturn("/content/genesis/us/en");
        PrivateAccessor.setField(resource,"gradient","45deg");
        assertNotNull(resource.getContainerStyle());
    }

    @Test
    void testGetGradientStyleOntechColor() throws NoSuchFieldException {
        when(currentPage.getPath()).thenReturn("/content/genesis/us/en");
        PrivateAccessor.setField(resource,"gradient","45deg");
        assertNotNull(resource.getContainerStyle());
    }

    @Test
    void testGetContainerStyleForNoGradientValue() throws NoSuchFieldException {
        when(currentPage.getPath()).thenReturn("/content/genesis/us/en");
        assertNotNull(resource.getContainerStyle());
    }

  @Test
  void testBorderGetStyles(){
      context.addModelsForClasses(ContainerHelper.class);
      context.load().json("/com/dish/dopweb/core/models/containerhelper.json",
              "/content/us/en");
      assertNotNull(resource);

    ContainerHelper model =
        Objects.requireNonNull(context.resourceResolver().getResource("/content/us/en/container"))
            .adaptTo(ContainerHelper.class);
      context.registerService(ModelFactory.class, modelFactory,
              org.osgi.framework.Constants.SERVICE_RANKING, Integer.MAX_VALUE);
    String expectedOutput =
        "border-top: 0.0rem solid var(--container-border-color);"
            + "border-right: 0.0rem solid var(--container-border-color);"
            + "border-left: 0.0rem solid var(--container-border-color);"
            + "border-bottom: 0.0rem solid var(--container-border-color)";
    assertEquals(
        expectedOutput,
        Optional.ofNullable(model)
            .map(ContainerHelper::getBorder)
            .map(Border::getStyles)
            .orElse(null));
  }

  @Test
    void testGetBgDesktopImage() throws NoSuchFieldException {
      when(img.getValueMap()).thenReturn(valueMap);
      when(valueMap.get("backgroundImageReference",String.class)).thenReturn("desktopImageUrl");
      PrivateAccessor.setField(resource, "imgDesktop", img);

      assertEquals("desktopImageUrl", resource.getBgDesktopImage());
  }

  @Test
    void testGetBgTabletImage() throws NoSuchFieldException {
      when(img.getValueMap()).thenReturn(valueMap);
      when(valueMap.get("backgroundImageReference",String.class)).thenReturn("tabletImageUrl");
      PrivateAccessor.setField(resource, "imgTablet", img);

      assertEquals("tabletImageUrl", resource.getBgTabletImage());
  }

    @Test
    void testGetBgPhoneImage() throws NoSuchFieldException {
        when(img.getValueMap()).thenReturn(valueMap);
        when(valueMap.get("backgroundImageReference",String.class)).thenReturn("phoneImageUrl");
        PrivateAccessor.setField(resource, "imgPhone", img);

        assertEquals("phoneImageUrl", resource.getBgPhoneImage());
    }

    @Test
    void testSetGradient() throws NoSuchFieldException {
        resource.setGradient("45deg");

        assertEquals("45deg", PrivateAccessor.getField(resource, "gradient"));
    }

    @Test
    void testSetImgDesktop() throws PersistenceException, NoSuchFieldException {
        when(img.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        when(modifiableValueMap.put("backgroundImageReference","imagePath")).thenReturn("imagePath");
        when(img.getResourceResolver()).thenReturn(resourceResolver);
        resource.setImgDesktop("imagePath");

        assertEquals(img, PrivateAccessor.getField(resource, "imgDesktop"));
    }
}
