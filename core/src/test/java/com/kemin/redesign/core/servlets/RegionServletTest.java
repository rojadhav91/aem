package com.kemin.redesign.core.servlets;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.ApplicationConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.mockito.Mockito;


@ExtendWith(AemContextExtension.class)
class RegionServletTest {

    public MockSlingHttpServletRequest req;
    public MockSlingHttpServletResponse res;
    public final AemContext context = new AemContext();
    private RegionServlet fixture = new RegionServlet();
    private static final String CONTEXT_PATH = "/context";


    @BeforeEach
    public void setup() {
        req = context.request();
        res = context.response();
        context.load().json("/regionservlet/test-content.json", "/content");
    }

    @Test
    void doGetContentType() throws ServletException, IOException {
        setProperties();
        fixture.doGet(req, res);
        assertEquals("application/json", res.getContentType());
    }

    @Test
    void doGetEmptyResponseOutput() throws ServletException, IOException {
        setProperties();
        fixture.doGet(req, res);
        assertEquals("{\"currentRegionLang\":{\"selectedLanguage\":\"\",\"selectedRegion\":\"\"}}", res.getOutputAsString());
    }

    private void setProperties() {
        Map<String, Object> params = new HashMap<>();
        params.put(ResourceResolverFactory.SUBSERVICE, ApplicationConstants.KEMIN_REDESIGN_REPO_READ);
        ResourceResolver resourceResolver = Mockito.mock(ResourceResolver.class);
        ResourceResolverFactory resourceResolverFactory = Mockito.mock(ResourceResolverFactory.class);
        Resource resource = Mockito.mock(Resource.class);
        Page page = Mockito.mock(Page.class);
        fixture.setResourceResolverFactory(resourceResolverFactory);
        try {
            Mockito.when(resourceResolverFactory.getServiceResourceResolver(params)).thenReturn(resourceResolver);
            Mockito.when(resourceResolver.getResource("/content/kemin-redesign")).thenReturn(resource);
            Mockito.when(resource.adaptTo(Page.class)).thenReturn(page);
        } catch (LoginException loginException) {
            loginException.printStackTrace();
        }
    }
}
