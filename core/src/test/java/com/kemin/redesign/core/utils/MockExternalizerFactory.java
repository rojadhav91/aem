/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~ Copyright 2017 Adobe Systems Incorporated
~
~ Licensed under the Apache License, Version 2.0 (the "License");
~ you may not use this file except in compliance with the License.
~ You may obtain a copy of the License at
~
~     http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing, software
~ distributed under the License is distributed on an "AS IS" BASIS,
~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
~ See the License for the specific language governing permissions and
~ limitations under the License.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
package com.dish.dopweb.core.utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.day.cq.commons.Externalizer;
import org.apache.sling.api.resource.ResourceResolver;
import org.mockito.ArgumentMatchers;

public class MockExternalizerFactory {

  public static final String PUBLISH_ROOT = "https://publish.org";
  public static final String AUTHOR_ROOT = "https://author:4502";
  public static final String BOOST_ROOT = "https://commerce.com";
  public static final String GENESIS_ROOT = "https://genesis.com";

  public static Externalizer getExternalizerService() {
    Externalizer externalizer = mock(Externalizer.class);
    when(externalizer.publishLink(
            ArgumentMatchers.any(ResourceResolver.class), ArgumentMatchers.anyString()))
        .then(invocationOnMock -> PUBLISH_ROOT + invocationOnMock.getArgument(1));
    when(externalizer.authorLink(
            ArgumentMatchers.any(ResourceResolver.class), ArgumentMatchers.anyString()))
        .then(invocationOnMock -> AUTHOR_ROOT + invocationOnMock.getArgument(1));
    when(externalizer.externalLink(
            ArgumentMatchers.any(ResourceResolver.class),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()))
        .then(
            invocationOnMock -> {
              String root = "";
              switch ((String) invocationOnMock.getArgument(1)) {
                case "author":
                  root = AUTHOR_ROOT;
                  break;
                case "publish":
                  root = PUBLISH_ROOT;
                  break;
                case "boost":
                  root = BOOST_ROOT;
                  break;
                case "genesis":
                  root = GENESIS_ROOT;
                  break;
                default:
                  root = "";
              }
              return root + invocationOnMock.getArgument(2);
            });
    return externalizer;
  }
}
