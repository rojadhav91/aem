package com.dish.dopweb.core.models;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(AemContextExtension.class)
class BorderTest {

  private final AemContext context = new AemContext();

  private Border border;

  @BeforeEach
  void setup() {
    final Resource borderResource =
        context
            .create()
            .resource(
                "/content/us/en/border",
                Map.of(
                    "width", "20",
                    "unit", "rem",
                    "top", "true",
                    "right", "true",
                    "left", "true",
                    "bottom", "true"));

    border = borderResource.adaptTo(Border.class);
  }

  @Test
  void getStyles() {
    String output =
        "border-top: 20.0rem solid var(--container-border-color);"
            + "border-right: 20.0rem solid var(--container-border-color);"
            + "border-left: 20.0rem solid var(--container-border-color);"
            + "border-bottom: 20.0rem solid var(--container-border-color)";
    assertEquals(output, border.getStyles());
  }
}
