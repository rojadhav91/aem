package com.dish.dopweb.core.models;

import com.day.cq.wcm.api.Page;
import com.dish.dopweb.core.configs.SitemapExternalizerCaConfig;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextBuilder;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.testing.mock.caconfig.MockContextAwareConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.apache.sling.testing.mock.caconfig.ContextPlugins.CACONFIG;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class ExternalizerModelTest {

    private final AemContext aemContext = new AemContextBuilder().plugin(CACONFIG).build();

    @InjectMocks
    ExternalizerModel externalizerModel;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(ExternalizerModel.class);
        aemContext.create().resource("/content/boost", "sling:configRef", "/conf/boost");
        Page page = aemContext.create().page("/content/boost/us/en");
        aemContext.currentPage(page);
        Resource resource = aemContext.currentResource(page.getPath());
        //resource = aemContext.create().resource("/content/boost/page1");
        MockContextAwareConfig.registerAnnotationClasses(aemContext, SitemapExternalizerCaConfig.class);
        MockContextAwareConfig.writeConfiguration(
                aemContext,
                resource.getPath(),
                SitemapExternalizerCaConfig.class,
                Map.of(
                        "externalizerCaConfigDomain", "publish"));
    }

    @Test
    void getSitemapExternalizerCaConfig() {
        ExternalizerModel externalizerModel = aemContext.request().adaptTo(ExternalizerModel.class);
        SitemapExternalizerCaConfig sitemapExternalizerCaConfig = externalizerModel.getSitemapExternalizerCaConfig();
        String s = sitemapExternalizerCaConfig.externalizerCaConfigDomain();
        assertEquals("publish", s);
    }
}