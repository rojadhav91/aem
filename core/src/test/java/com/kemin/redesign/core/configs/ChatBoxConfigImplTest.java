package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.ChatBoxConfigImpl;
import com.dish.dopweb.core.configs.impl.LaunchScriptConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ChatBoxConfigImplTest {

    private final String APP_ID = "dish-dop-appid";
    private final String API_HOSTNAME = "/dummy/dish-dop-api-hostname";

    ChatBoxConfigImpl chatBoxConfigService = new ChatBoxConfigImpl();

    @Mock
    ChatBoxConfigImpl.Config chatBoxConfig;

    @Test
    public void testAppId(){
        when(chatBoxConfig.appId()).thenReturn(APP_ID);
        chatBoxConfigService.activate(chatBoxConfig);
        Assertions.assertEquals(APP_ID,chatBoxConfigService.getAppId());
    }

    @Test
    public void testAppHostname(){
        when(chatBoxConfig.aPIHostName()).thenReturn(API_HOSTNAME);
        chatBoxConfigService.activate(chatBoxConfig);
        Assertions.assertEquals(API_HOSTNAME,chatBoxConfigService.getAPIHostName());
    }
}
