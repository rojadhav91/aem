package com.dish.dopweb.core.models;

import com.dish.dopweb.core.utils.ResourceResolverUtil;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class})
class SVGModelTest {

  @InjectMocks SVGModel model;

  @Mock ResourceResolverFactory resourceResolverFactory;
  @Mock ResourceResolver resourceResolver;

  @Mock Resource resource;

  @Mock Node node;
  @Mock Property jcrData;
  @Mock Node jcrContent;
  @Mock Binary jcrBinary;


  @Test
  void testGetIconDropdownForResource() throws NoSuchFieldException, RepositoryException, LoginException {
    final String testIconUrl = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons/Icons_UI_Shopping_Cart.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Node.class)).thenReturn(node);
    when(node.getNode("jcr:content")).thenReturn(jcrContent);
    when(jcrContent.getProperty("jcr:data")).thenReturn(jcrData);
    InputStream inputStream =
            new ByteArrayInputStream("<svg>file-content</svg>".getBytes(Charset.defaultCharset()));
    when(jcrData.getBinary()).thenReturn(jcrBinary);
    when(jcrBinary.getStream()).thenReturn(inputStream);
    model.init();
    assertEquals("<svg>file-content</svg>", model.getSvgContent());
  }

  @Test
  void testGetIconDropdownForNoResource() throws NoSuchFieldException, LoginException {
    final String testIconUrl = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons/Icons_UI_Shopping_Cart.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);

    when(resourceResolver.getResource(testIconUrl)).thenReturn(null);
    model.init();
    assertEquals(StringUtils.EMPTY, model.getSvgContent());
  }

  @Test
  void testgetResourceResolverException() throws LoginException, NoSuchFieldException {
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenThrow(LoginException.class);
    assertNull(model.getResourceResolver("apps-resources-service"));
  }

  @Test
  void getResourceStreamNodeException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons/Icons_UI_Shopping_Cart.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Node.class)).thenReturn(node);
    when(node.getNode(Mockito.any())).thenThrow(RepositoryException.class);
    model.init();
    assertEquals("",model.getSvgContent());
  }

  @Test
  void getResourceStreamJcrContentException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons/Icons_UI_Shopping_Cart.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Node.class)).thenReturn(node);
    when(node.getNode("jcr:content")).thenReturn(jcrContent);
    when(jcrContent.getProperty(Mockito.any())).thenThrow(RepositoryException.class);
    model.init();
    assertEquals("",model.getSvgContent());
  }

  @Test
  void getResourceStreamJcrDataException() throws LoginException, NoSuchFieldException, RepositoryException {
    final String testIconUrl = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons/Icons_UI_Shopping_Cart.svg";
    PrivateAccessor.setField(model, "iconUrl", testIconUrl);
    PrivateAccessor.setField(model, "resourceResolverFactory", resourceResolverFactory);
    when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    when(resourceResolver.getResource(testIconUrl)).thenReturn(resource);
    when(resource.adaptTo(Node.class)).thenReturn(node);
    when(node.getNode("jcr:content")).thenReturn(jcrContent);
    when(jcrContent.getProperty("jcr:data")).thenReturn(jcrData);
    when(jcrData.getBinary()).thenThrow(RepositoryException.class);
    model.init();
    assertEquals("",model.getSvgContent());
  }

}
