package com.dish.dopweb.core.beans;

import com.dish.dopweb.core.models.TopNav;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.models.factory.ModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})

public class TopNavMenuItemsTest {

    private TopNavMenuItems resource;
    private final AemContext context = new AemContext();

    @Mock
    private ModelFactory modelFactory;

    @BeforeEach
    public void setup() throws Exception {
        context.addModelsForClasses(TopNav.class);
        context.load().json("/com/dish/dopweb/core/models/topnav.json",
                "/content/experience-fragment/dopweb/headerxf");
        resource = context.resourceResolver()
                .getResource("/content/experience-fragment/dopweb/headerxf/jcr:content/root/header/topnav/menuItems/item0")
                .adaptTo(TopNavMenuItems.class);
        assertNotNull(resource);
        context.registerService(ModelFactory.class, modelFactory,
                org.osgi.framework.Constants.SERVICE_RANKING, Integer.MAX_VALUE);
    }

    @Test
    void testGetTitle(){
        assertNotNull(resource.getTitle());
    }

    @Test
    void testGetPageTitle(){
        assertNotNull(resource.getPageTitle());
    }

    @Test
    void testGetPagePath(){
        assertNotNull(resource.getPagePath());
    }

    @Test
    void testGetMenuItemType(){
        assertNotNull(resource.getMenuItemType());
    }
}
