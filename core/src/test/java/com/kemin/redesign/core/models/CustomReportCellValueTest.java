package com.dish.dopweb.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import java.util.HashMap;
import java.util.Map;

@ExtendWith({MockitoExtension.class})
class CustomReportCellValueTest {
    private static final String[] ARRAY_VALUE = new String[] { "/val1/", "val2" };

    @Mock
    private Resource mockResource;

    Map<String, Object> properties;

    @BeforeEach
    void init() {

        MockitoAnnotations.initMocks(this);

        properties = new HashMap<String, Object>();
        properties.put("prop1", ARRAY_VALUE[0]);
        properties.put("prop2", ARRAY_VALUE[1]);
    }

    @Test
    void testNotFound() {
        when(mockResource.getValueMap()).thenReturn(new ValueMapDecorator(properties));

        CustomReportCellValue rcv = new CustomReportCellValue(mockResource, " ", " ");
        assertFalse(rcv.isArray());
        assertEquals(" ",rcv.getValue());
        assertEquals(" ", rcv.getSingleValue());
    }

    @Test
    void testSingle() {
        when(mockResource.getValueMap()).thenReturn(new ValueMapDecorator(properties));

        CustomReportCellValue rcv = new CustomReportCellValue(mockResource, "prop1", "prop2");
        assertFalse(rcv.isArray());
        assertEquals("/val1/is/image/val2", rcv.getValue());
        assertEquals("/val1/is/image/val2", rcv.getSingleValue());
    }

    @Test
    void testEmptyConstructor(){
        CustomReportCellValue crcv = new CustomReportCellValue();
        assertNull(crcv.getValue());
    }
}
