package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.MagentoAPIConfigServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MagentoAPIConfigServiceImplTest {
    private final String HOSTURL = "https://testhosturl.com/checkEmail";
    private final String BEARER_TOKEN = "by7rn9exiouuc755zfeyaoipwjitn5ju";

    MagentoAPIConfigServiceImpl magentoAPIConfigService = new MagentoAPIConfigServiceImpl();

    @Mock
    MagentoAPIConfigServiceImpl.MagentoAPIConfig magentoAPIConfig;

    @Test
    void testGetHost(){
        when(magentoAPIConfig.host()).thenReturn(HOSTURL);
        magentoAPIConfigService.activate(magentoAPIConfig);
        Assertions.assertEquals(HOSTURL,magentoAPIConfigService.getHost());
    }

    @Test
    void testGetBearerToken(){
        when(magentoAPIConfig.bearerToken()).thenReturn(BEARER_TOKEN);
        magentoAPIConfigService.activate(magentoAPIConfig);
        Assertions.assertEquals(BEARER_TOKEN, magentoAPIConfigService.getBearerToken());
    }
}
