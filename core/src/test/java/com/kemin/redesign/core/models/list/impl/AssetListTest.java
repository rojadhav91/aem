package com.kemin.redesign.core.models.list.impl;

import com.day.cq.dam.api.Asset;
import com.kemin.redesign.core.models.list.AssetListItem;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@ExtendWith({AemContextExtension.class})
class AssetListTest {

  private static final String CURRENT_PAGE = "/content/list";
  
  private static final String CONTEXT_PATH = "/context";

  private static final String LIST_1 = CURRENT_PAGE + "/jcr:content/root/staticListType";
  
  private static final String LIST_2 = CURRENT_PAGE + "/jcr:content/root/childrenListType";

  public final AemContext context = new AemContext();

  @Mock Resource resource;

  @Mock
  private AssetListItem linkBean;

  @Mock
  private AssetListItem linkBean2;

  @BeforeEach
  public void setUp() {
    context.addModelsForClasses(AssetList.class);
    context.load().json("/assetlist/test-content.json", "/content");
    MockitoAnnotations.initMocks(this);
  }

  @Test
  void testProperties() {
    AssetList list = getListUnderTest(LIST_1);
    Assertions.assertEquals("assetDownload", list.displayAs);
    Assertions.assertEquals("static", list.listFrom);
    Assertions.assertEquals("assets", list.fixedListFrom);
    Assertions.assertEquals("0", list.properties.get("maxItems"));
  }

  @Test
  void testType() {
    AssetList list = getListUnderTest(LIST_1);
    Assertions.assertEquals("assets", list.getType());
  }

  @Test
  void testEmptyType() {
    AssetList list = getListUnderTest("/content/empty");
    Assertions.assertEquals("", list.getType());
  }

  @Test
  void testFixedAssetList() {
    List<AssetListItem> expected = prepareExpectList();
    AssetList list = getListUnderTest(LIST_1);
    //List<AssetListItem> actual = list.getAssetItems();
    //Assertions.assertEquals(expected.size(), actual.size());
  }

  @Test
  void testEmptyChildAssetList() {
    List<AssetListItem> expected = Collections.EMPTY_LIST;
    AssetList list = getListUnderTest(LIST_2);
    List<AssetListItem> actual = list.getAssetItems();
    Assertions.assertEquals(expected, actual);
  }

  /**
   * Gets the list under test.
   *
   * @param resourcePath the resource path
   * @return the list under test
   */
  private AssetList getListUnderTest(String resourcePath) {
    Resource resource = context.resourceResolver().getResource(resourcePath);
    context.request().setContextPath(CONTEXT_PATH);
    context.currentResource(resource);
    return context.request().adaptTo(AssetList.class);
  }

  /**
   * Gets the asset under test.
   *
   * @param resourcePath the resource path
   * @return the asset under test
   */
  protected Asset getAssetUnderTest(String resourcePath) {
    context.currentResource(resourcePath);
    Resource resource = context.resourceResolver().getResource(resourcePath);
    return resource.adaptTo(Asset.class);
  }

  /**
   * Prepare expect list.
   *
   * @return the list
   */
  List<AssetListItem> prepareExpectList() {
    List<AssetListItem> expected = new ArrayList<>();
    expected.add(linkBean);
    expected.add(linkBean2);
    return expected;
  }
}
