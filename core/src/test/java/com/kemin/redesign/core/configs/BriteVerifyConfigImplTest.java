package com.dish.dopweb.core.configs;

import com.dish.dopweb.core.configs.impl.BriteVerifyConfigImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BriteVerifyConfigImplTest {

    private final String BRITEVERIFY_BOOST_KEY = "abcde";
    private final String BRITEVERIFY_GENESIS_KEY = "xyz";
    private final String BRITEVERIFY_ENDPOINT = "www.briteverify.com/dummy";

    BriteVerifyConfigImpl briteVerifyConfigImpl = new BriteVerifyConfigImpl();

    @Mock
    BriteVerifyConfigImpl.Config briteVerifyConfig;

    @Test
    public void testBoostKey(){
        when(briteVerifyConfig.briteVerifyBoostKey()).thenReturn(BRITEVERIFY_BOOST_KEY);
        briteVerifyConfigImpl.activate(briteVerifyConfig);
        Assertions.assertEquals(BRITEVERIFY_BOOST_KEY,briteVerifyConfigImpl.getBriteVerifyBoostKey());
    }

    @Test
    public void testGenesisKey(){
        when(briteVerifyConfig.briteVerifyGenesisKey()).thenReturn(BRITEVERIFY_GENESIS_KEY);
        briteVerifyConfigImpl.activate(briteVerifyConfig);
        Assertions.assertEquals(BRITEVERIFY_GENESIS_KEY,briteVerifyConfigImpl.getBriteVerifyGenesisKey());
    }

    @Test
    public void testEndpoint(){
        when(briteVerifyConfig.briteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
        briteVerifyConfigImpl.activate(briteVerifyConfig);
        Assertions.assertEquals(BRITEVERIFY_ENDPOINT,briteVerifyConfigImpl.getBriteVerifyEndpoint());
    }
}
