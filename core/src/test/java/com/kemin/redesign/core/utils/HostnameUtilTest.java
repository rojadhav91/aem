package com.dish.dopweb.core.utils;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.MalformedURLException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class HostnameUtilTest {

    @Test
    void getHostName() {
        Assert.assertEquals("http://localhost:4502", HostnameUtil.getHostName("http://localhost:4502/content/boost/us/test-pages/breadcrum-test.html"));
    }
}