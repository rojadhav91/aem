package com.dish.dopweb.core.models.internal.v1.form;



import com.dish.dopweb.core.models.form.Text;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;



import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class TextImplTest {

    private final AemContext aemContext = new AemContext();
    Text text;


    @BeforeEach
    void setup() {
        aemContext.addModelsForClasses(Text.class);
        aemContext.load().json("/com/dish/dopweb/core/models/internal.v1/form/text.json", "/text");

    }

    @Test
    void getIconTest(){
        aemContext.currentResource("/text/text");
        text = aemContext.request().adaptTo(Text.class);
        assertEquals("/content/dam/boost/web/icons/settings.svg", text.getIcon());

    }

    @Test
    void getIconPositionTest(){
        aemContext.currentResource("/text/text");
        text = aemContext.request().adaptTo(Text.class);
        assertEquals("left", text.getIconPosition());
    }

}
