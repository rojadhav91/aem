package com.dish.dopweb.core.servlets;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.dish.dopweb.core.configs.*;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.wcm.testing.mock.aem.junit5.AemContext;
import junitx.util.PrivateAccessor;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.i18n.ResourceBundleProvider;
import org.apache.sling.settings.SlingSettingsService;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.io.IOException;
import java.util.*;

import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class ApiProxyServletTest {
  private static final String ZIPCODES_TAGS_PATH = "/content/cq:tags/dop-web/zipcodes";
  private static final String BETA_USER_EMAILS_PATH = "/content/cq:tags/genesis/beta-users-email";
  private static final String SUCCESS = "success";
  private static final String STATUS = "status";
  private static final String FAILURE = "failure";
  private static final String ERROR_CODE = "errorCode";
  private final String BRITEVERIFY_KEY = "abcde";
  private final String BRITEVERIFY_ENDPOINT = "www.briteverify.com/dummy";
  private final String RESPONSYS_ENDPOINT = "www.responsys.com/dummy";
  private final String OMNISEND_KEY = "omni";
  private final String OMNISEND_ENDPOINT = "www.omnisend.com/dummy";
  private final String APIGEE_ENDPOINT = "www.apigee.com/dummy";
  private final String SERVICECOVERAGE_ENDPOINT = "www.servicecoverage.com/dummy";
  private final String CURRENT_PAGE_PATH = "content/genesis/us/jcr:content/";

  private final AemContext context = new AemContext();
  private final ApiProxyServlet servlet = Mockito.spy(new ApiProxyServlet());
  @Mock CloseableHttpClient httpClient;
  @Mock CloseableHttpResponse httpResponse;
  @Mock StatusLine statusLine;
  @Mock ResourceBundleProvider resourceBundleProvider;
  @Mock ResourceResolver resourceResolver;
  @Mock TagManager tagManager;
  @Mock Tag zipTag;
  @Mock Tag emailTag;
  @Mock Resource marketsResource;
  @Mock Resource marketResource;
  @Mock Resource zipcodesResource;
  @Mock ValueMap valuemap;
  @Mock Resource zipcode;
  @Mock HttpEntity httpEntity;
  @Mock MagentoAPIConfigService magentoAPIConfigService;
  @Mock SlingSettingsService slingSettingsService;
  @Mock ApigeeAuthTokenRequestConfig apigeeAuthTokenRequestConfig;
  @Mock CheckServiceCoverageConfig checkServiceCoverageConfig;
  @Mock CFMarketsConfigService cfMarketsConfigService;

  @BeforeEach
  void setup() {
    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
    // find
    when(resourceResolver.resolve("/content/cq:tags/genesis/markets")).thenReturn(marketsResource);
    when(marketsResource.listChildren())
        .thenReturn(List.of(marketResource).iterator())
        .thenReturn(List.of(marketResource).iterator());
    when(marketResource.listChildren())
        .thenReturn(List.of(zipcodesResource).iterator())
        .thenReturn(List.of(zipcodesResource).iterator());

    // hasZipcode mocks
    when(zipcodesResource.getName()).thenReturn("zip-codes").thenReturn("zip-codes");

    when(zipcodesResource.listChildren()).thenReturn(List.of(zipcode).iterator());

    // isPublished mocks
    when(zipcode.adaptTo(ValueMap.class)).thenReturn(valuemap);
    when(valuemap.get(NameConstants.PN_PAGE_LAST_REPLICATION_ACTION, String.class))
        .thenReturn("Activate");

    when(zipcode.adaptTo(Tag.class)).thenReturn(zipTag);
    when(zipTag.getName()).thenReturn("12345");

    context.requestPathInfo().setResourcePath(CURRENT_PAGE_PATH);
  }

  @Test
  void doPostBoost() throws IOException, NoSuchFieldException {
    String apiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String apiResponse2 =
        "<html>\n"
            + "\n"
            + "<head>\n"
            + "\t<title>Untitled</title>\n"
            + "</head>\n"
            + "\n"
            + "<body>\n"
            + "\tSUCCESS\n"
            + "</body>\n"
            + "\n"
            + "</html>";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);
    ResponsysConfig responsysConfig = mock(ResponsysConfig.class);
    PrivateAccessor.setField(servlet, "responsysConfig", responsysConfig);
    when(briteVerifyConfig.getBriteVerifyBoostKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    when(responsysConfig.getResponsysEndpoint()).thenReturn(RESPONSYS_ENDPOINT);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(apiResponse).doReturn(apiResponse2).when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    request.setParameterMap(
        ImmutableMap.of(
            "site", "boost", "email", "test@test.com", "zip", "85001", "consent", "true"));

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("responsys", jsonObject.get("api").getAsString());
  }

  @Test
  void doPostWithInvalidEmailBoost() throws IOException, NoSuchFieldException {
    String apiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity111.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"invalid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    when(briteVerifyConfig.getBriteVerifyBoostKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(apiResponse).when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    request.setParameterMap(
        ImmutableMap.of(
            "site", "boost", "email", "test@test.com", "zip", "85001", "consent", "true"));

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("briteVerify", jsonObject.get("api").getAsString());
  }

  @Test
  void doPostWithTrueRoleAddrBoost() throws IOException, NoSuchFieldException {
    String apiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validitycom\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": true\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    when(briteVerifyConfig.getBriteVerifyBoostKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(apiResponse).when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    request.setParameterMap(ImmutableMap.of("site", "boost", "email", "test@test.com"));

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("briteVerify", jsonObject.get("api").getAsString());
  }

  @Test
  @Disabled(value = "Disabled temporarily to let the git pipeline runs correctly.")
  void testDoPostGenesisSuccessWithAddress() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String apiResponse2 =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\",\n"
            + "    \"access_token\": \"Srm5WIQg9BKKaBZvnaZUax8SmS0c\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse).doReturn(apiResponse2).when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);
    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);
    when(apigeeAuthTokenRequestConfig.getApigeeAuthTokenEndpoint()).thenReturn(APIGEE_ENDPOINT);
    when(apigeeAuthTokenRequestConfig.getClientId()).thenReturn("abcde");
    when(apigeeAuthTokenRequestConfig.getClientSecret()).thenReturn("abcde");
    PrivateAccessor.setField(servlet, "apigeeAuthTokenRequestConfig", apigeeAuthTokenRequestConfig);
    when(checkServiceCoverageConfig.getCheckServiceCoverageEndpoint()).thenReturn(SERVICECOVERAGE_ENDPOINT);
    PrivateAccessor.setField(servlet, "checkServiceCoverageConfig", checkServiceCoverageConfig);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("beValidation", "true")
            .put("serviceAvailability", "true")
            .put("email","test@test.com")
            .put("address", "47 Waybrook Ct, Columbia SC 29212").build();

    request.setParameterMap(parameters);

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    try {
      servlet.doPost(request, response);
    }catch(Exception e){
      e.printStackTrace();
    }
    final String outputAsString = response.getOutputAsString();
    System.out.println("284 :   : jsonObject "+outputAsString);
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void doPostGenesisFailureWithAddress() throws IOException, NoSuchFieldException {

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn("").when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);
    when(httpResponse.getEntity()).thenReturn(httpEntity);
    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("beValidation", "true")
            .put("email","test@test.com")
            .put("zip","85001")
            .put("address", "123St").build();

    request.setParameterMap(parameters);

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    try {
      servlet.doPost(request, response);
    }catch(Exception e){
      e.printStackTrace();
    }
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void doPostGenesisFailureForInvalidZip() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);
    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","venkata.com")
            .put("zip","56489")
            .put("beValidation", "true")
            .put("zipValidation", "true").build();

    request.setParameterMap(parameters);

    when(cfMarketsConfigService.isValidZipcode("56489")).thenReturn(false);
    PrivateAccessor.setField(servlet,"cfMarketsConfigService", cfMarketsConfigService);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("invalid-zip", jsonObject.get(ERROR_CODE).getAsString());
  }

  @Test
  void doPostGenesisSuccessWithoutAddress() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","valid@dish.com")
            .put("zip","12345")
            .put("beValidation", "true")
            .put("emailWhitelist", "true")
            .put("emailValidation","true").build();

    request.setParameterMap(parameters);

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void doPostGenesisSuccessForEmailNotWhitelisted() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","Invalid@dish.com")
            .put("zip","12345")
            .put("beValidation", "true")
            .put("emailValidation","true")
            .put("emailWhitelist", "true").build();

    request.setParameterMap(parameters);

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("email-not-whitelisted", jsonObject.get(ERROR_CODE).getAsString());
  }

  @Test
  void doPostGenesisSuccessForEmailDoesntAlreadyExists() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    MagentoAPIConfigService magentoAPIConfigService = mock(MagentoAPIConfigService.class);
    when(magentoAPIConfigService.getHost()).thenReturn("host");
    when(magentoAPIConfigService.getBearerToken()).thenReturn("bearer-token");
    PrivateAccessor.setField(servlet, "magentoAPIConfigService", magentoAPIConfigService);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","Invalid@dish.com")
            .put("zip","12345")
            .put("beValidation", "true")
            .put("emailValidation","true")
            .put("emailWhitelist", "false").build();

    request.setParameterMap(parameters);

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);
try {
  servlet.doPost(request, response);
}catch(Exception e){
  e.printStackTrace();
}
    final String outputAsString = response.getOutputAsString();
    System.out.println("json respon"+outputAsString);
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);

    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void doPostGenesisFailureForInvalidEmail() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"invalid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    // BriteVerify mocks
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    // OmniSend mocks
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","Invalid@dish.com")
            .put("zip","12345")
            .put("beValidation", "true")
            .put("emailWhitelist", "false")
            //.put("address", "47 Waybrook Ct, Columbia SC 29212")
            .put("emailValidation","true").build();

    request.setParameterMap(parameters);


    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    System.out.println("call at {}"+"750");
    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("invalid-email", jsonObject.get(ERROR_CODE).getAsString());
  }

  @Test
  void doPostGenesisSuccessForWhitelistedEmailDomain() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"unknown\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    // BriteVerify mocks
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    // OmniSend mocks
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    // DomainWhitelist mocks
    DomainWhitelistConfig domainWhitelistConfig = mock(DomainWhitelistConfig.class);
    when(domainWhitelistConfig.getWhitelistedDomains())
        .thenReturn(List.of("dish.com", "ontech.com"));
    PrivateAccessor.setField(servlet, "domainWhitelistConfig", domainWhitelistConfig);

    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","valid@dish.com")
            .put("zip","12345")
            .put("beValidation", "true")
            .put("emailWhitelist", "true")
            .put("emailValidation","true").build();

    request.setParameterMap(parameters);

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void doPostGenesisFailureForSignUpError() throws IOException, NoSuchFieldException {


    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    // BriteVerify mocks
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    // OmniSend mocks
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn("").when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("email","Invalid@dish.com")
            .put("zip","12345")
            .put("beValidation", "false")
            .put("emailWhitelist", "false")
            .put("signupEnabled","true").build();

    request.setParameterMap(parameters);

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
    Assertions.assertEquals("signup-error", jsonObject.get(ERROR_CODE).getAsString());
  }

  @Test
  void doPostGenesisSuccessForSignUpDisabled() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"invalid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    // BriteVerify mocks
    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    // OmniSend mocks
    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse).doReturn("").when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    request.setParameterMap(
        Map.of(
            "site",
            "genesis",
            "email",
            "Invalid@dish.com",
            "zip",
            "12345",
            "beValidation",
            "false",
            "emailWhitelist",
            "false",
            "signupEnabled",
            "false"));

    when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);

    when(tagManager.resolve(BETA_USER_EMAILS_PATH)).thenReturn(emailTag);
    when(emailTag.listChildren()).thenReturn(List.of(emailTag).iterator());
    when(emailTag.getTitle()).thenReturn("valid@dish.com");

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void testDoPostGenesisFailureWithoutAddress() throws IOException, NoSuchFieldException {
    String briteVerifyApiResponse =
        "{\n"
            + "  \"email\": {\n"
            + "  \"address\": \"sales@validity.com\",\n"
            + "  \"account\": \"sales\",\n"
            + "  \"domain\": \"validity.com\",\n"
            + "  \"status\": \"valid\",\n"
            + "  \"connected\": null,\n"
            + "  \"disposable\": false,\n"
            + "  \"role_address\": false\n"
            + "  },\n"
            + "  \"duration\": 0.035602396\n"
            + "}";
    String OmniSendApiResponse =
        "{\n"
            + "    \"email\": \"austin.powers@example.com\",\n"
            + "    \"contactID\": \"609aca7119719750899f92d8\",\n"
            + "    \"phone\": \"3031237300\"\n"
            + "}";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    BriteVerifyConfig briteVerifyConfig = mock(BriteVerifyConfig.class);
    when(briteVerifyConfig.getBriteVerifyGenesisKey()).thenReturn(BRITEVERIFY_KEY);
    when(briteVerifyConfig.getBriteVerifyEndpoint()).thenReturn(BRITEVERIFY_ENDPOINT);
    PrivateAccessor.setField(servlet, "briteVerifyConfig", briteVerifyConfig);

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(servlet.getHttpClient()).thenReturn(httpClient);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    doReturn(briteVerifyApiResponse)
        .doReturn(OmniSendApiResponse)
        .when(servlet)
        .getResult(httpResponse);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);
    when(statusLine.getStatusCode()).thenReturn(200);

    context
        .bundleContext()
        .registerService(ResourceBundleProvider.class.getName(), resourceBundleProvider, null);
    when(resourceBundleProvider.getResourceBundle("base1", null))
        .thenReturn(
            new ListResourceBundle() {
              @Override
              protected Object[][] getContents() {
                return new Object[][] {{"Invalid zip", "translated key"}};
              }
            });

    ResourceBundle bundle = request.getResourceBundle("base1", null);
    when(request.getResourceBundle(null)).thenReturn(bundle);

    request.setParameterMap(
        ImmutableMap.of("site", "genesis", "email", "test@test.com", "zip", "85001"));

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    servlet.doPost(request, response);
    final String outputAsString = response.getOutputAsString();
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(SUCCESS, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void testDoGet() {
    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();
    request.setParameterMap(ImmutableMap.of("zipcodes", "85001"));
    servlet.doGet(request, response);
    final String outputAsString = response.getOutputAsString();
    Assertions.assertEquals("Invalid parameter", response.getOutputAsString());
  }

  @Test
  void testGetAuthTokenFailure() throws NoSuchFieldException {
    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);

    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);

    when(apigeeAuthTokenRequestConfig.getApigeeAuthTokenEndpoint()).thenReturn(APIGEE_ENDPOINT);
    when(apigeeAuthTokenRequestConfig.getClientId()).thenReturn("abcde");
    when(apigeeAuthTokenRequestConfig.getClientSecret()).thenReturn("abcde");
    PrivateAccessor.setField(servlet, "apigeeAuthTokenRequestConfig", apigeeAuthTokenRequestConfig);

    when(checkServiceCoverageConfig.getCheckServiceCoverageEndpoint()).thenReturn(SERVICECOVERAGE_ENDPOINT);
    PrivateAccessor.setField(servlet, "checkServiceCoverageConfig", checkServiceCoverageConfig);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("beValidation", "true")
            .put("serviceAvailability", "true")
            .put("email","test@test.com")
            .put("address", "47 Waybrook Ct, Columbia SC 29212").build();

    request.setParameterMap(parameters);

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    try {
      servlet.doPost(request, response);
    }catch(Exception e){
      e.printStackTrace();
    }
    final String outputAsString = response.getOutputAsString();
    System.out.println("284 :   : jsonObject "+outputAsString);
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
  }

  @Test
  void testServiceCoverageFailure() throws NoSuchFieldException, IOException {
    String briteVerifyApiResponse =
            "{\n"
                    + "  \"email\": {\n"
                    + "  \"address\": \"sales@validity.com\",\n"
                    + "  \"account\": \"sales\",\n"
                    + "  \"domain\": \"validity.com\",\n"
                    + "  \"status\": \"valid\",\n"
                    + "  \"connected\": null,\n"
                    + "  \"disposable\": false,\n"
                    + "  \"role_address\": false\n"
                    + "  },\n"
                    + "  \"duration\": 0.035602396\n"
                    + "}";
    String apiResponse2 =
            "{\n  \"access_token\": \"Srm5WIQg9BKKaBZvnaZUax8SmS0c\"\n }";
    String serviceCoverage2 = "";

    final MockSlingHttpServletRequest request = context.request();
    final MockSlingHttpServletResponse response = context.response();

    OmniSendConfig omniSendConfig = mock(OmniSendConfig.class);
    PrivateAccessor.setField(servlet, "omniSendConfig", omniSendConfig);

    when(omniSendConfig.getOmniSendEndpoint()).thenReturn(OMNISEND_ENDPOINT);
    when(omniSendConfig.getOmniSendKey()).thenReturn(OMNISEND_KEY);
    when(servlet.getHttpClient()).thenReturn(httpClient);
    doReturn(briteVerifyApiResponse).doReturn(apiResponse2).doReturn(serviceCoverage2).when(servlet).getResult(httpResponse);
    when(httpClient.execute(Mockito.any(HttpPost.class))).thenReturn(httpResponse);
    when(statusLine.getStatusCode()).thenReturn(200);
    when(httpResponse.getStatusLine()).thenReturn(statusLine);
    when(magentoAPIConfigService.getHost()).thenReturn("localhost");
    PrivateAccessor.setField(servlet,"magentoAPIConfigService", magentoAPIConfigService);
    when(apigeeAuthTokenRequestConfig.getApigeeAuthTokenEndpoint()).thenReturn(APIGEE_ENDPOINT);
    when(apigeeAuthTokenRequestConfig.getClientId()).thenReturn("abcde");
    when(apigeeAuthTokenRequestConfig.getClientSecret()).thenReturn("abcde");
    PrivateAccessor.setField(servlet, "apigeeAuthTokenRequestConfig", apigeeAuthTokenRequestConfig);
    when(checkServiceCoverageConfig.getCheckServiceCoverageEndpoint()).thenReturn(SERVICECOVERAGE_ENDPOINT);
    PrivateAccessor.setField(servlet, "checkServiceCoverageConfig", checkServiceCoverageConfig);

    ImmutableMap parameters = ImmutableMap.<String, String>builder()
            .put("site","genesis")
            .put("beValidation", "true")
            .put("serviceAvailability", "true")
            .put("email","test@test.com")
            .put("address", "47 Waybrook Ct, Columbia SC 29212").build();

    request.setParameterMap(parameters);

    when(servlet.resourceResolver()).thenReturn(resourceResolver);

    try {
      servlet.doPost(request, response);
    }catch(Exception e){
      e.printStackTrace();
    }
    final String outputAsString = response.getOutputAsString();
    System.out.println("284 :   : jsonObject "+outputAsString);
    final JsonObject jsonObject = new Gson().fromJson(outputAsString, JsonObject.class);
    Assertions.assertEquals(FAILURE, jsonObject.get(STATUS).getAsString());
  }
}
