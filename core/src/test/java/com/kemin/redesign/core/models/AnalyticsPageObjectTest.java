package com.dish.dopweb.core.models;

import com.dish.dopweb.core.configs.LaunchScriptCaConfig;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextBuilder;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.testing.mock.caconfig.MockContextAwareConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Map;

import static org.apache.sling.testing.mock.caconfig.ContextPlugins.CACONFIG;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
class AnalyticsPageObjectTest {
  public static final String EN_PAGE_JSON = "/com/dish/dopweb/core/servlets/enPage.json";
  private static final String EN_PAGE_PATH = "/content/dopweb/en";

  private final AemContext context = new AemContextBuilder().plugin(CACONFIG).build();

  AnalyticsPageObject model;

  @BeforeEach
  void setup() {
    context.create().resource("/content/dopweb", "sling:configRef", "/conf/dopweb");
    context.load().json(EN_PAGE_JSON, EN_PAGE_PATH);
    context.currentPage(EN_PAGE_PATH);
    context.request().setServerName("dish.com");
    context.request().setParameterMap(Map.of("key", "value"));
  }

  @Test
  void testForLanguagePage() {
    context.currentPage(EN_PAGE_PATH);
    context.runMode("prod");
    model = context.request().adaptTo(AnalyticsPageObject.class);

    MockContextAwareConfig.registerAnnotationClasses(context, LaunchScriptCaConfig.class);
    MockContextAwareConfig.writeConfiguration(
            context,
            EN_PAGE_PATH,
            LaunchScriptCaConfig.class,
            Map.of(
                    "launchScript", "mock-launch-script-url.js",
                    "dataLayerPagePrefix", "BMWeb"));

    assertEquals("BMWeb|home", model.getPageName());
    assertEquals("dish.com", model.getDomain());
    assertEquals("en", model.getLanguage());
    assertEquals("http://dish.com/", model.getUrl());
    assertEquals("key=value", model.getQsp());
    assertEquals("home", model.getSiteSection());
    assertEquals("home", model.getSiteSubSection());
    assertEquals("home", model.getSiteSubSubSection());
    assertEquals("AEM:PROD:dopweb", model.getPlatform());
    assertEquals("/content/dopweb/en", model.getPageUrl());
  }

  @Test
  void testForPageAtLevelTwo() {
    context.currentPage(EN_PAGE_PATH + "/footer/careers");
    context.runMode("qa");
    model = context.request().adaptTo(AnalyticsPageObject.class);

    assertEquals("careers", model.getPageName());
    assertEquals("dish.com", model.getDomain());
    assertEquals("en", model.getLanguage());
    assertEquals("http://dish.com/", model.getUrl());
    assertEquals("key=value", model.getQsp());
    assertEquals("footer", model.getSiteSection());
    assertEquals("careers", model.getSiteSubSection());
    assertEquals("", model.getSiteSubSubSection());
    assertEquals("AEM:QA:dopweb", model.getPlatform());
  }
}
