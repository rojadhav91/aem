package com.kemin.redesign.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.kemin.redesign.core.ApplicationConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import java.util.List;

@Component(service = ParticipantStepChooser.class, immediate = true, property = {
		"chooser.label = Kemin-Redesign-choose reviewer" })
@ServiceDescription("Reviewer chooser")
public class ChooseReviewerStep implements ParticipantStepChooser {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	public String getParticipant(WorkItem workItem, WorkflowSession wfSession, MetaDataMap metaDataMap)
			throws WorkflowException {
		String participant = StringUtils.EMPTY;
		if (workItem.getWorkflowData().getMetaDataMap().containsKey(ApplicationConstants.GROUP_NAME)) {
			participant = workItem.getWorkflowData().getMetaDataMap().get(ApplicationConstants.GROUP_NAME,
					String.class);
		} else {
			Workflow wf = workItem.getWorkflow();
			List<HistoryItem> wfHistory = wfSession.getHistory(wf);
			if (!wfHistory.isEmpty()) {
				participant = "administrators";
			} else {
				participant = "admin";
			}
		}
		return participant;
	}
}