package com.kemin.redesign.core.models.image.impl;

import com.kemin.redesign.core.models.image.Image;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
    adaptables = Resource.class,
    adapters = Image.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageImpl implements Image {

  @ValueMapValue @Getter private String altText;

  @ValueMapValue @Getter private String fileReference;

  @ValueMapValue @Getter private String linkUrl;
}
