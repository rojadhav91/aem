package com.kemin.redesign.core.servlets;

import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.utils.CommonUtils;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;


/**
 * The Servlet is used to return region and languages under path /content/kemin-redesign
 */
@Component(service = Servlet.class, property = {
        "sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/kemin-redesign/regionSelector", "extensions=" + "json"})
@ServiceDescription("JSON Servlet to read the region from /content/kemin-redesign")
public class RegionServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionServlet.class);

    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException {
        JSONObject obj = new JSONObject();
        try (ResourceResolver resourceResolver = CommonUtils.getResourceResolver(resourceResolverFactory, ApplicationConstants.KEMIN_REDESIGN_REPO_READ)) {
            Resource regionResource = resourceResolver.getResource(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH);
            if (regionResource != null) {
                LOGGER.debug("regionResource is not null");
                JSONArray regionLanguageArray = CommonUtils.getRegionLanguageList(regionResource);
                setSelectedRegionLang(req, obj);
                setOutput(obj, regionLanguageArray);
            }
        } catch (LoginException loginException) {
            LOGGER.debug("loginException occurred while reading resolver", loginException);
        }
        resp.setContentType("application/json");
        resp.getWriter().write(obj.toString());
    }

    private void setSelectedRegionLang(SlingHttpServletRequest req, JSONObject obj) {
        String currentPagePath = "";
        if (req.getParameter("currentPagePath") != null) {
            currentPagePath = req.getParameter("currentPagePath");
            LOGGER.debug("currentPagePath is not null:{}",currentPagePath);
        }
        JSONObject selectedRegion = CommonUtils.getCurrentRegionLangFromUri(currentPagePath);
        try {
            obj.put("currentRegionLang", selectedRegion);
        } catch (JSONException jsonException) {
            LOGGER.debug("jsonException occurred while setting output", jsonException);
        }
    }

    private void setOutput(JSONObject obj, JSONArray regionLanguageArray) {
        if (regionLanguageArray.length() > 0) {
            try {
                obj.put("regionList", regionLanguageArray);
            } catch (JSONException jsonException) {
                LOGGER.debug("jsonException occurred while setting output", jsonException);
            }
        }
    }

    public void setResourceResolverFactory(
            ResourceResolverFactory resourceResolverFactory) {
        this.resourceResolverFactory = resourceResolverFactory;
    }
}
