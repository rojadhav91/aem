package com.kemin.redesign.core.models.explorefocus.impl;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.kemin.redesign.core.models.explorefocus.LinkItem;

@Model(
    adaptables = Resource.class,
    adapters = LinkItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkItemImpl implements LinkItem {

  @ValueMapValue @Getter private String url;

  @ValueMapValue @Getter private String title;
}
