package com.dish.dopweb.core.configs;

/**
 * Chatbox configuration
 */
public interface ChatBoxConfig {
    /**
     * Get appid.
     * @return appId
     */
    String getAppId();
    /**
     * Get appHostname.
     * @return apphostname
     */
    String getAPIHostName();
}
