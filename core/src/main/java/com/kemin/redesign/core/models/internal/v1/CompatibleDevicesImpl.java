package com.dish.dopweb.core.models.internal.v1;

import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.tagging.Tag;
import com.dish.dopweb.core.beans.DeviceCompatibility;
import com.dish.dopweb.core.beans.ModelFamily;
import com.dish.dopweb.core.beans.SupportedDevices;
import com.dish.dopweb.core.beans.TagItems;
import com.dish.dopweb.core.models.CompatibleDevices;
import com.google.gson.Gson;
import org.apache.sling.api.SlingException;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @author imran.padshah
 */
@Model(
        adaptables   = Resource.class,
        adapters     = CompatibleDevices.class,
        resourceType = CompatibleDevicesImpl.RESOURCE_TYPE
)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION
)
@SuppressWarnings("PMD")
public class CompatibleDevicesImpl implements CompatibleDevices
{
    public static final String RESOURCE_TYPE         = "dopweb/components/compatibilitycheck";
    public static final String tagsPath              = "/content/cq:tags/dop-web/devices-compatibility";
    private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
    private final Logger logger                      = LoggerFactory.getLogger(CompatibleDevicesImpl.class);
    private String jsonStr;

    @OSGiService
    private static transient ResourceResolverFactory resourceResolverFactory;

    @PostConstruct
    protected void initModel()
    {
        if(resourceResolverFactory == null)
        {
            logger.info("resourceResolverFactory service ref is null");
            jsonStr = null;
            return;
        }
        ResourceResolver resolver = null;

        try
        {
            resolver = getResourceResolver(APP_RESOURCE_SERVICE);
            if(resolver == null) return;

            Resource tagRes = resolver.resolve(tagsPath);
            //Level 1 tags
            List<DeviceCompatibility> deviceCompatibility = new ArrayList<DeviceCompatibility>();
            tagRes.getChildren().forEach(res1 -> {
                Tag level1Tag = res1.adaptTo(Tag.class);
                logger.info("Level 1 Tag title is: " + level1Tag.getTitle());
                //Level 2 Tags
                List<ModelFamily> mfList = new ArrayList<ModelFamily>();
                res1.getChildren().forEach(res2 -> {
                    Tag level2Tag       = res2.adaptTo(Tag.class);
                    logger.info("Level 2 Tag title is: " + level2Tag.getTitle());
                    //Level 3 tags
                    List<TagItems> deviceList = new ArrayList<TagItems>();
                    res2.getChildren().forEach(res3 -> {
                        Tag level3Tag = res3.adaptTo(Tag.class);
                        logger.info("Level 3 Tag title is: " + level3Tag.getTitle());
                        TagItems ti = new TagItems();
                        ti.setId(level3Tag.getName());
                        ti.setName(level3Tag.getTitle());
                        deviceList.add(ti);

                    });

                    ModelFamily mf = new ModelFamily();
                    mf.setId(level2Tag.getName());
                    mf.setName(level2Tag.getTitle());
                    deviceList.sort(Comparator.comparing(TagItems::getName, String::compareToIgnoreCase));
                    mf.setDevices(deviceList);

                    mfList.add(mf);
                });

                DeviceCompatibility l1TagInfo = new DeviceCompatibility();
                l1TagInfo.setId(level1Tag.getName());
                l1TagInfo.setName(level1Tag.getTitle());
                mfList.sort(Comparator.comparing(ModelFamily::getName, String::compareToIgnoreCase));
                l1TagInfo.setModelFamily(mfList);
                deviceCompatibility.add(l1TagInfo);
            });

            Gson gson = new Gson();
            SupportedDevices sd = new SupportedDevices();
            deviceCompatibility.sort(Comparator.comparing(DeviceCompatibility::getName, String::compareToIgnoreCase));
            sd.setDeviceCompatibility(deviceCompatibility);
            logger.info("Devices Size is: " + sd.getDeviceCompatibility().size());
            jsonStr = gson.toJson(sd);
        }
        catch(SlingException e)
        {
            logger.error("Failed to process the tags : " + e);
            jsonStr=null;
        }
        finally
        {
            if(resolver != null && resolver.isLive()) resolver.close();
        }

        return;
    } //initiModel

    @Override
    public String getDevices() {
        return jsonStr;
    }


    private ResourceResolver getResourceResolver(String serviceName)
    {
        ResourceResolver resolver = null;
        final Map<String, Object > param = Collections.singletonMap(ResourceResolverFactory.SUBSERVICE, (Object) serviceName);

        try
        {
            resolver = resourceResolverFactory.getServiceResourceResolver(param);
        }
        catch (LoginException e)
        {
            logger.error("Could not ger service user: {}", e);
        }
        return resolver;
    } //getResourceResolver

}
