package com.kemin.redesign.core.models.navigation;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Iterator;
import java.util.List;

@ConsumerType
public interface Navigation extends ComponentExporter {
    List<Page> getNavigationItemList();
    String getRootPage();
    Page getCurrentPage();
    String getThumbnailURL();
    String getNavigationIconUrl();
}
