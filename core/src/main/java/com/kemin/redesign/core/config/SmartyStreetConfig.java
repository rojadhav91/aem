package com.dish.dopweb.core.configs;

/**
 * Configuration Service for Smarty Street
 */
public interface SmartyStreetConfig {
    /**
     * key for SmartyStreet
     * @return key
     */
    String getApiKey();

    /**
     * Omnisend endpoint
     * @return endoint
     */
    String getSmartyStreetEndpoint();
}
