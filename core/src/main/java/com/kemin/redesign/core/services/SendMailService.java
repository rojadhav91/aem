package com.kemin.redesign.core.services;

import org.apache.sling.api.resource.ResourceResolver;

import java.util.List;
import java.util.Map;

/**
 * Simple Mail Service Interface
 */
public interface SendMailService {

  /**
   *
   * @param templatePath templatePath
   * @param emailParams emailParams
   * @param recipients recipients
   * @param resolver resolver
   * @return List<String>
   */
  List<String> sendEmail(String templatePath, Map<String, String> emailParams, String[] recipients,
      ResourceResolver resolver);

}
