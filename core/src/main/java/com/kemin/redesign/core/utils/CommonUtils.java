package com.kemin.redesign.core.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Template;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kemin.redesign.core.beans.SupplierLocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.ApplicationConstants;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;

public class CommonUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtils.class);

    /**
     * This method iterate region and languages under /content/kemin-redesign and
     * sends as json array where language list is part of region
     *
     * @param regionResource the region resource
     * @return the region language list
     */
    public static JSONArray getRegionLanguageList(Resource regionResource) {
        JSONArray array = new JSONArray();
        Page parentPage = regionResource.adaptTo(Page.class);
        Iterator<Page> it = parentPage.listChildren();
        if (it != null) {
            LOGGER.debug("regionParent page is not null and have children");
            it.forEachRemaining(regionPage -> setRegion(array, regionPage));
        }
        return array;
    }

    /**
     * This method sets the region properties and calls the languages of that
     * particular region
     *
     * @param array      the array
     * @param regionPage the region page
     */
    private static void setRegion(JSONArray array, Page regionPage) {
        JSONObject object = new JSONObject();
        if (regionPage != null && !regionPage.getName().equalsIgnoreCase("language-masters")) {
            try {
                object.put("regionTitle", regionPage.getTitle());
                object.put("regionName", regionPage.getName());
                object.put("regionPath", regionPage.getPath());
                object.put("languageList", getRegionLanguage(regionPage));
                array.put(object);
            } catch (JSONException jsonException) {
                LOGGER.debug("jsonException occurred while setting the region properties:{}", regionPage);
            }

        }
    }

    /**
     * sets the language for a particular region
     *
     * @param regionPage the region page
     * @return the region language
     */
    private static JSONArray getRegionLanguage(Page regionPage) {
        Iterator<Page> it = regionPage.listChildren();
        JSONArray languageArray = new JSONArray();
        if (it != null) {
            it.forEachRemaining(languagePage -> setLanguage(languageArray, languagePage));
        }
        return languageArray;
    }

    private static void setLanguage(JSONArray languageArray, Page languagePage) {
        JSONObject object = new JSONObject();
        if (languagePage != null && !languagePage.getName().equalsIgnoreCase("language-masters")) {
            try {
                object.put("languageTitle", languagePage.getTitle());
                object.put("languageName", languagePage.getName());
                object.put("languagePath", languagePage.getPath());
                languageArray.put(object);
            } catch (JSONException jsonException) {
                LOGGER.debug("jsonException occurred while setting the language properties:{}", languagePage);
            }
        }
    }

    public static ResourceResolver getResourceResolver(ResourceResolverFactory resourceResolverFactory, String user)
            throws LoginException {
        Map<String, Object> param = new HashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, user);
        return resourceResolverFactory.getServiceResourceResolver(param);
    }

    /**
     * Gets the current region lang from uri.
     *
     * @param currentPagePath the current page path
     * @return the current region lang from uri
     */
    public static JSONObject getCurrentRegionLangFromUri(String currentPagePath) {
        JSONObject object = new JSONObject();
        String selectedRegion = getSelectedRegion(currentPagePath);
        String selectedLanguage = getSelectedLanguage(currentPagePath);
        try {
            object.put("selectedRegion", selectedRegion);
            object.put("selectedLanguage", selectedLanguage);
        } catch (JSONException jsonException) {
            LOGGER.debug("jsonException occurred while setting the language properties", jsonException);
        }
        return object;
    }

    public static String getSelectedRegion(String currentPagePath) {
        String selectedRegion = "";
        if (StringUtils.isNotBlank(currentPagePath)
                && currentPagePath.startsWith(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH)) {
            selectedRegion = currentPagePath.replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + "/", "");
            String[] regionArray = selectedRegion.split("/");
            if (regionArray.length > 0) {
                selectedRegion = regionArray[0];
            }
        }
        return selectedRegion;
    }

    public static String getSelectedLanguage(String currentPagePath) {
        String selectedLanguage = "";
        if (StringUtils.isNotBlank(currentPagePath)
                && currentPagePath.startsWith(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH)) {
            selectedLanguage = currentPagePath.replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + "/", "");
            String[] selectedLangArray = selectedLanguage.split("/");
            if (selectedLangArray.length > 1) {
                selectedLanguage = selectedLangArray[1];
            }
        }
        return selectedLanguage;
    }

    public static String thumbnail(Page page) {
        ValueMap metadata = page.getProperties("image");
        String thumbnail = org.apache.commons.lang.StringUtils.EMPTY;
        if (metadata != null && !metadata.isEmpty()) {
            thumbnail = metadata.get("fileReference", String.class);
        } else {
            thumbnail = page.getPath() + ".thumb" + ".png";
        }
        return thumbnail;
    }

    public static List<Page> prepareNavigationItemList(Resource navigation, ResourceResolver resourceResolver) {
        List<Page> headerNavigationItemList = new ArrayList<>();
        Iterator<Resource> children;
        String rootPath = "";
        if (navigation != null) {
            children = navigation.listChildren();
            while (children.hasNext()) {
                Resource childResource = children.next();

                ValueMap properties = childResource.adaptTo(ValueMap.class);
                if (properties != null) {
                    rootPath = properties.get("rootPage", String.class);
                    if (resourceResolver.getResource(rootPath) == null) {
                        continue;
                    }
                }
            }

            Page navPage = resourceResolver.getResource(rootPath).adaptTo(Page.class);
            Iterator<Page> rootPageIterator = navPage.listChildren();
            while (rootPageIterator.hasNext()) {
                Page childPage = rootPageIterator.next();

                if (childPage != null) {
                    if (checkIfNotHidden(childPage)) {
                        headerNavigationItemList.add(childPage);
                    }

                }
            }
        }
        return headerNavigationItemList;
    }

    private static HashMap<Page, String> tagStrucure;

    public static HashMap<Page, String> getTagStructure() {
        return tagStrucure;
    }

    public static List<Page> prepareNavigationItemListPage(Page page) {
        List<Page> navigationItemList = new ArrayList<>();
        Iterator<Page> rootPageIterator = page.listChildren();
        while (rootPageIterator.hasNext()) {
            Page childPage = rootPageIterator.next();
            if (childPage != null) {
                if (checkIfNotHidden(childPage)) {
                    navigationItemList.add(childPage);
                }
            }
        }

        return navigationItemList;
    }

    public static List<Page> prepareNavigationItemListPageCompany(Page page) {
        List<Page> navigationItemList = new ArrayList<>();
        Iterator<Page> rootPageIterator = page.listChildren();
        while (rootPageIterator.hasNext()) {
            Page childPage = rootPageIterator.next();
            if (childPage != null) {
                if (checkIfNotHidden(childPage)) {
                    Template tmp = childPage.getTemplate();
                    if (tmp.getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_COMPANY_TEMPLATE)) {
                        navigationItemList.add(childPage);
                    }

                }
            }
        }

        return navigationItemList;
    }

    private static boolean showHidden = false;

    public static boolean checkIfNotHidden(Page page) {
        return !page.isHideInNav() || showHidden;
    }

    public static boolean filterPage(Page p, String propertyName, String propertyValue) {
        boolean isPage = false;
        ValueMap props = p.getProperties();
        String templatePath = props.get(propertyName, String.class);
        if (propertyValue.equals(templatePath)) {
            isPage = true;
        } else {
            isPage = false;
        }
        return isPage;
    }

    public static String getThumbnailImage(Page page) {
        String thumbnailImage = "";
        Node selectedPageNode = page.adaptTo(Node.class);
        try {
            if (selectedPageNode != null) {
                Node spn = selectedPageNode.getNode("jcr:content");
                Node imageNode = JcrUtils.getNodeIfExists(spn, "image");
                if ((imageNode != null) && (imageNode.hasProperty("fileReference"))) {
                    thumbnailImage = imageNode.getProperty("fileReference").getString();
                }
            }

        } catch (RepositoryException repositoryException) {
            LOGGER.error("repositoryException occurred while getting thumbnailImage");
        }
        return thumbnailImage;
    }

    public static String getNavigationIconByContentFragements(Page requestingPage, ResourceResolver resourceResolver) {
        String iconImage = "";
        String fragmentPath = "/content/dam/kemin-redesign/navigation-icons";
        // 1 /content/kemin-redesign/na/en-ca/homepage/market/animal-nutrition/ammu-curb
        try {
            if (requestingPage != null && requestingPage.getDepth() > 3) {

                Page rootPage = requestingPage.getAbsoluteParent(2);//na
                Page languagePage = requestingPage.getAbsoluteParent(3);//en-ca

                String currentPagePath = requestingPage.getPath();
                Locale locale = requestingPage.getLanguage();
                String currentPageLocale = rootPage.getName() + "/" + languagePage.getName().toLowerCase();//na/en-us
                //String pathToSearchFrag = fragmentPath + "/" + currentPageLocale;


                LOGGER.info(" : locale=" + currentPageLocale + "  fragemt path:=" + fragmentPath + "  current page path;=" + currentPagePath);
                String qury = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + fragmentPath + "]) " +
                        " AND s.[jcr:content/data/master/navPagePath]='" + currentPagePath + "' " +
                        " AND s.[jcr:content/data/master/navLocale]='" + currentPageLocale + "'";
                LOGGER.info("qq==" + qury);
                Iterator<Resource> fragmentResources = resourceResolver.findResources(qury, Query.JCR_SQL2);
                while (fragmentResources.hasNext()) {
                    ContentFragment contentFragment = fragmentResources.next().adaptTo(ContentFragment.class);

                    if (contentFragment != null) {
                        ContentElement keywordElement = contentFragment.getElement("navIconPath");
                        FragmentData keywordFragmentData = keywordElement.getValue();
                        String navIconPath = keywordFragmentData.getValue(String.class);
                        return navIconPath;
                    }
                }
                if (iconImage != null && iconImage.equals("") && requestingPage.getDepth() > 3) {
                    return getNavigationIconByContentFragements(requestingPage.getParent(), resourceResolver);
                }
            }

        } catch (
                Exception e) {
            LOGGER.error("Exception in fetching icons for navigation component :" + e.toString());
        }
        return iconImage;

    }


    public static List<SupplierLocation> convertJsonToLocationList(Resource resource) {
        List<SupplierLocation> locations = new ArrayList<>();
        if(resource!=null){
            try {
                Asset asset = resource.adaptTo(Asset.class);
                Resource original = asset.getOriginal();
                InputStream content = original.adaptTo(InputStream.class);
                StringBuilder sb = new StringBuilder();
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(content, StandardCharsets.UTF_8));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                ObjectMapper mapper = new ObjectMapper();

                SupplierLocation[] supplierLocations = mapper.readValue(sb.toString(), SupplierLocation[].class);
                if (supplierLocations != null) {
                    locations = Arrays.asList(supplierLocations);
                }
            } catch (Exception jsonException) {
                LOGGER.error("Error In Parsing JSON");
            }
        }
        return locations;
    }


    private CommonUtils() {
    }

}