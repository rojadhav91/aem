package com.aembootcamp.core.models;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.resource.path.Path;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.designer.Style;

@Model(adaptables = SlingHttpServletRequest.class,
resourceType = Breadcrumb.RESOURCE_TYPE,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Breadcrumb {
	protected static final String RESOURCE_TYPE="aem-bbotcamp/components/structure/breadcrumb";

	private int startLevel=3;

	private boolean showHidden=false;;

	private boolean hideCurrent=true;
	
	private Page homepage;



	    public Page getHomepage() {
		return homepage = findRootPage();
	}

		@ScriptVariable
	    private Page currentPage;
	    
	    private List<Page> list=new ArrayList<>();
	    
	    
	    private boolean checkIfNotHidden(Page page) {
	        return !page.isHideInNav() || showHidden;
	    }
	    
	    @PostConstruct
	    public void initNav() {
	    	
	    	
	        List<Page> items = new ArrayList<>();
	        int currentLevel = currentPage.getDepth();
	        
	        while (startLevel < currentLevel) {
	        	  
	            Page page = currentPage.getAbsoluteParent(startLevel);
	            if (page != null && page.getContentResource() != null) {
	            	
	                boolean isActivePage = page.equals(currentPage);
	                if (isActivePage && hideCurrent) {
	                    break;
	                }
	                if (checkIfNotHidden(page)) {
	                    items.add(page);
	                }
	            }
	            startLevel++;
	        }
	        list = items;
	    }

		public List<Page> getList() {
			return list;
		}
		
		private Page findRootPage() {
            Page page = currentPage;
            while (true) {
                Page parent = page.getParent();
                if (parent == null) {
                    return page;
                } else {
                    page = parent;
                }
            }
        }
    }