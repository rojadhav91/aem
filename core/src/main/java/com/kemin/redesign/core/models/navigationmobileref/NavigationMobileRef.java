package com.kemin.redesign.core.models.navigationmobileref;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface NavigationMobileRef extends ComponentExporter {
    List<Page> getNavigationItemList();

    String getSocialLinksComponentPath();

    String getThumbnailURL();
}
