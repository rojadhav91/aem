package com.kemin.redesign.core.models.product.impl;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class TierTagList {
    private String tier1Tag;
    private int tier1TagCount;
    private Map<String, Integer> tier2TagDetails;
}
