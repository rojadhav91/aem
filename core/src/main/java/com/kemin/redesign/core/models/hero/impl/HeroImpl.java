package com.kemin.redesign.core.models.hero.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.hero.Hero;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Hero.class, ComponentExporter.class},
        resourceType = HeroImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class HeroImpl implements Hero {
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/hero";

    @ValueMapValue @Getter private String fileReference;

    @ValueMapValue @Getter private String altText;

    @ValueMapValue @Getter private String title;

    @ValueMapValue @Getter private String buttonName;

    @ValueMapValue @Getter private String buttonLink;

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}