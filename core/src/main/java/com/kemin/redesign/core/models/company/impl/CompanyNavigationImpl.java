package com.kemin.redesign.core.models.company.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.Template;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.company.ComapnyNavigation;
import com.kemin.redesign.core.utils.CommonUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.List;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = {ComapnyNavigation.class,
        ComponentExporter.class}, resourceType = CompanyNavigationImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
public class CompanyNavigationImpl implements ComapnyNavigation {

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/navigation-company";
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyNavigationImpl.class);

    @SlingObject
    private ResourceResolver resourceResolver;
    List<Page> headerNavigationItemList;
    Page parent;

    @ScriptVariable
    private Page currentPage;

    @ChildResource(injectionStrategy = InjectionStrategy.DEFAULT)


    @ValueMapValue
    @Getter
    private String rootPath;

    @PostConstruct
    protected void init() {
         parent=getParentPage(currentPage.getParent());
        PageManager pm=resourceResolver.adaptTo(PageManager.class);
        Page p=pm.getPage(rootPath);
            headerNavigationItemList = CommonUtils.prepareNavigationItemListPageCompany(parent);
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    @Override
    public List<Page> getNavigationItemList() {
        return headerNavigationItemList;
    }

    @Override
    public String getThumbnailURL() {
        if (currentPage != null) {
            return CommonUtils.thumbnail(parent);
        } else {
            return "";
        }
    }

    @Override
    public String getNavigationIconUrl() {
        if (currentPage != null) {

            String url= CommonUtils.getNavigationIconByContentFragements(currentPage,resourceResolver);
            if(url==null||"".equals(url)){
                return getThumbnailURL();
            }else{
                return url;
            }
        } else {
            return "";
        }
    }

    @Override
    public Page getCurrentPage() {
        return currentPage;
    }

    public Page getParentPage(Page page) {
        Page parent = null;
        int depth = page.getDepth();
        for (int count = 0; count < depth - 3; count++) {
            if (page.getParent(count) != null) {
                Template tmp = page.getParent(count).getTemplate();
                if (tmp != null && !tmp.getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_COMPANY_TEMPLATE)) {
                    parent = page.getParent(count);
                    break;
                }
            }

        }
        return parent;
    }
    @Override
    public String getRootPage() {
        if ((parent != null) && (parent.getNavigationTitle() != null)) {
            return parent.getNavigationTitle();
        } else if (parent != null) {
            return parent.getTitle();
        } else {
            return "";
        }
    }

}