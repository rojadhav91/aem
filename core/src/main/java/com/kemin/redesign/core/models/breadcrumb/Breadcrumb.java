package com.kemin.redesign.core.models.breadcrumb;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface Breadcrumb extends ComponentExporter {
    Page getHomePage();
    List<Page> getPageList();
    Page getParent();
    Page getCurrentPage();
    int getPageDepth();
    String getIsProduct();
    boolean getIsSearchPage();
    String getHomePageForSearchPage();
}
