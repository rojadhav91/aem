package com.kemin.redesign.core.models.gallery;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Collection;

@ConsumerType
public interface Gallery extends ComponentExporter  {
	Collection<GalleryItem> getGalleryTiles();
	String getTitle();
}
