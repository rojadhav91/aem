package com.kemin.redesign.core.models.product.impl;

import lombok.Data;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class ProductPageBean {
    private String productURL;
    private String productDescription;
    private String productCatalogName;
    private String productLogoImage;
    private Set<String> tags;
    private int productNumber;
    private String tagClass;
}
