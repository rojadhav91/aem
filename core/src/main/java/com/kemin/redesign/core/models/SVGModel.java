package com.dish.dopweb.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Model to get svg file contents.
 */
@Model(
        adaptables = Resource.class,
        resourceType = SVGModel.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SVGModel {
    protected static final String RESOURCE_TYPE = "dopweb/clientlibs/clientlib-site/resources/images/icons/buttons";
    private static final String JCR_CONTENT = "jcr:content";
    private static final String JCR_DATA = "jcr:data";
    private static final String ETC_CLIENTLIBS = "/etc.clientlibs/";
    private static final String EMPTY_SPACE = "";
    private final Logger logger = LoggerFactory.getLogger(SVGModel.class);
    private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
    @Getter
    protected String svgContent;
    @ValueMapValue
    private String iconUrl;

    @OSGiService
    private static transient ResourceResolverFactory resourceResolverFactory;

    @PostConstruct
    protected void init() {

        try (ResourceResolver resourceResolver = getResourceResolver(APP_RESOURCE_SERVICE)){
            if (Objects.nonNull(iconUrl) && Objects.nonNull(resourceResolver)) {
                final InputStream stream = getResourceStream(resourceResolver);

                final StringBuilder stringBuilder = new StringBuilder();
                if (Objects.nonNull(stream)) {
                    String line;
                    try (BufferedReader bufferedReader =
                                 new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
                        line = bufferedReader.readLine();
                        while (line != null) {
                            stringBuilder.append(line);
                            line = bufferedReader.readLine();
                        }
                    } catch (IOException e) {
                        logger.error("Could not read content of svg file ({})", iconUrl);
                    }
                } else {
                    logger.error("Could not get stream from asset path: {}", iconUrl);
                }
                svgContent = stringBuilder.toString();
            }
        }
    }

    private InputStream getResourceStream(final ResourceResolver resourceResolver) {
        return Optional.ofNullable(resourceResolver.getResource(iconUrl.replace(ETC_CLIENTLIBS,EMPTY_SPACE)))
                .map(nodeResource -> nodeResource.adaptTo(Node.class))
                .map(node -> {
                    try {
                        return node.getNode(JCR_CONTENT);
                    } catch (RepositoryException e) {
                        logger.error("Could not get JCR content");
                    }
                    return node;
                })
                .map(jcrContent -> {
                    try {
                        return jcrContent.getProperty(JCR_DATA);
                    } catch (RepositoryException e) {
                        logger.error("Could not get JCR data");
                    }
                    return null;
                })
                .map(jcrData -> {
                    try {
                        return jcrData.getBinary().getStream();
                    } catch (RepositoryException e) {
                        logger.error("Could not get icon stream");
                    }
                    return null;
                })
                .orElse(null);
    }

    /**
     * Get service resource resolver
     * @param serviceParam
     * @return resourceResolver
     */
    public ResourceResolver getResourceResolver(final String serviceParam){
        ResourceResolver resolver = null;
        final ConcurrentHashMap<String, Object> param = getServiceParams(serviceParam);
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(param);
        } catch (LoginException e) {
            logger.error("Could not ger service user: {}", e);
        }
        return resolver;
    }

    /**
     * Create and Get service mapping
     * @param serviceParam
     * @return
     */
    private ConcurrentHashMap<String, Object> getServiceParams(final String serviceParam) {
        final ConcurrentHashMap<String, Object> param = new ConcurrentHashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, serviceParam);
        return param;
    }
}
