package com.kemin.redesign.core.models.header;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface HeaderItem {

  String getLinkUrl();

  String getLinkTitle();
}
