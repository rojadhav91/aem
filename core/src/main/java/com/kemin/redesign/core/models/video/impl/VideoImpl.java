package com.kemin.redesign.core.models.video.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.video.Video;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import javax.annotation.PostConstruct;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Video.class, ComponentExporter.class},
        resourceType = VideoImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class VideoImpl implements Video{

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/video";

    private static final String TYPE_EMBED="embed";
    private static final String TYPE_DAM="dem";
    private static final String TYPE_YOUTUBE="youTube";
    
    @ValueMapValue @Getter private String videoType;

    @ValueMapValue @Getter private String damLink;

    @ValueMapValue @Getter private String youTubeId;

    @ValueMapValue @Getter private String embedId;

    @ValueMapValue @Getter private String title;

    @ValueMapValue @Getter private String caption;

    @ValueMapValue @Getter private String metaDescription;

    @ValueMapValue @Getter private String thumbnail;

    @ValueMapValue @Getter private String playButtonColor;

    @Getter private String youTubeLink;

    @Getter private String videoId = "";
    
    @PostConstruct
    protected void init(){
        youTubeLink = "https://www.youtube.com/embed/" + youTubeId + "?enablejsapi=1&version=3&playerapiid=ytplayer";

        if(TYPE_EMBED.equalsIgnoreCase(videoType)) {
        	videoId = embedId;
        }else if(TYPE_DAM.equalsIgnoreCase(videoType)) {
        	videoId = damLink;
        }else if(TYPE_YOUTUBE.equalsIgnoreCase(videoType)) {
        	videoId = youTubeLink;
        }
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}

