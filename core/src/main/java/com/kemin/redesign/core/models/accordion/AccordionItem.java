package com.kemin.redesign.core.models.accordion;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface AccordionItem {
    String getId();
    String getHeading();
}
