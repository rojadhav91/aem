package com.dish.dopweb.core.models;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is to convert response - address components in to json
 */
public class Components {
    @Getter
    @Setter
    @SerializedName("city_name")
    private String city="";

    @Getter
    @Setter
    @SerializedName("state_abbreviation")
    private String state="";

    @Getter
    @Setter
    @SerializedName("zipcode")
    private String zipcode="";
}
