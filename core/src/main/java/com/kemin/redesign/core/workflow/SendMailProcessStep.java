package com.kemin.redesign.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.services.SendMailService;
import com.kemin.redesign.core.utils.WorkFlowUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import java.util.*;

/**
 * Workflow process step to send email Notifications
 */
@Component(service = WorkflowProcess.class, property = { "process.label = Kemin-Redesign-Send Mail Process Step",
		Constants.SERVICE_DESCRIPTION + "=Sends email to the authors", })
public class SendMailProcessStep implements WorkflowProcess {

	private static Logger log = LoggerFactory.getLogger(SendMailProcessStep.class);
	private static final String WCM_EDITOR_URL_TOUCH_DEFAULT = "/editor.html";
	private static String REJECT = "reject";
	private static String BASIC_AUTHORS = "basic-authors";
	private static String POWER_AUTHORS = "power-authors";
	private static String KEMIN_ADMIN = "-admin";
	private static String NOTIFY_EDITORIAL = "notify-editorial";
	private static String NOTIFY_LEGAL = "notify-legal";
	private static String NOTIFY_AUTHOR = "notify-author";
	private static String EMAIL_PROPERTY = "./profile/email";
	private static String NAME_PROPERTY = "./profile/familyName";
	private static String REJECT_TEMPLATE = "/etc/notification/email/kemin/reject-content/en.txt";
	private static String EDITORIAL_NOTIFICATION = "/etc/notification/email/kemin/editorial-notification/en.txt";
	private static String LEGAL_NOTIFICATION = "/etc/notification/email/kemin/legal-notification/en.txt";
	private static String AUTHOR_NOTIFICATION = "/etc/notification/email/kemin/author-notification/en.txt";

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Reference
	private SendMailService sendMailService;

	/**
	 * This method executes the workflow process step to send email notifications.
	 *
	 * @param workItem        workItem
	 * @param workflowSession workflowSession
	 * @param map             map
	 */
	@Override
	public void execute(final WorkItem workItem, final WorkflowSession workflowSession, final MetaDataMap map)
			throws WorkflowException {
		log.debug("[execute] inside SendMailProcess execute method.");
		String wfNodeDescription = workItem.getNode().getDescription();
		String templatePath = StringUtils.EMPTY;
		log.debug("[execute] templatePath : {}", templatePath);
		ResourceResolver resourceResolver = null;
		try {
			// Get the ResourceResolver from workflow session
			Session session = workflowSession.adaptTo(Session.class);
			resourceResolver = getResourceResolver(session);
			UserManager manager = resourceResolver.adaptTo(UserManager.class);
			if (null != manager) {
				Authorizable authorizable = manager.getAuthorizable(workItem.getWorkflow().getInitiator());
				List<String> recipientsList = new ArrayList<>();
				if (wfNodeDescription.contains(REJECT)) {
					workItem.getWorkflowData().getMetaDataMap().put("isActivated", "false");
					templatePath = REJECT_TEMPLATE;
					recipientsList = getRecipientList(workItem, authorizable, REJECT, manager, workflowSession);
				} else if (wfNodeDescription.equals(NOTIFY_EDITORIAL)) {
					templatePath = EDITORIAL_NOTIFICATION;
					recipientsList = getRecipientList(workItem, authorizable, NOTIFY_EDITORIAL, manager,
							workflowSession);
				} else if (wfNodeDescription.equals(NOTIFY_LEGAL)) {
					templatePath = LEGAL_NOTIFICATION;
					recipientsList = getRecipientList(workItem, authorizable, NOTIFY_LEGAL, manager, workflowSession);
				} else if (wfNodeDescription.equals(NOTIFY_AUTHOR)) {
					templatePath = AUTHOR_NOTIFICATION;
					recipientsList = getRecipientList(workItem, authorizable, NOTIFY_AUTHOR, manager, workflowSession);
				}
				// Array of email recipients
				String[] recipients = recipientsList.toArray(new String[0]);
				// Set the dynamic variables of your email template
				Map<String, String> emailParams = prepareEmailParams(resourceResolver, authorizable, workItem,
						wfNodeDescription, workflowSession);

				for(String recipient:recipients){
					log.debug("recipient:"+recipient);
				}
				log.debug("email params {}", emailParams);
				if (null != emailParams && recipients.length > 0 && StringUtils.isNotEmpty(templatePath)) {
					sendMailService.sendEmail(templatePath, emailParams, recipients, resourceResolver);
				} else {
					log.debug("[FAIL-EMAIL-SEND-002][execute][CONTINUE] Not sending emails");
				}
			} else {
				log.debug("[FAIL-EMAIL-SEND-003][execute][CONTINUE] unable to adapt resolver to UserManager");
			}
			log.debug("[execute] exiting SendMailProcess execute method.");

		} catch (LoginException | RepositoryException e) {
			log.error("[REPOSITORY-EXCEPTION-001]RepositoryException in SendMailProcessStep {}", e);
		} finally {
			if (null != resourceResolver) {
				resourceResolver.close();
			}
		}
	}

	/**
	 * This method prepares the emailParams required to send email. It returns null
	 * if there are some issues in preparing it, else returns a Map of values to
	 * load the mail template with actual email content. emailParams considered here
	 * are Name, Author asset path, Publish asset path.
	 *
	 * @param resourceResolver resourceResolver
	 * @param authorizable     authorizable
	 * @param workItem         workItem
	 * @return Map<String , String>
	 */
	private Map<String, String> prepareEmailParams(final ResourceResolver resourceResolver,
			final Authorizable authorizable, final WorkItem workItem, String wfNodeDescription,
			WorkflowSession workflowSession) throws WorkflowException {
		Map<String, String> emailParams = new HashMap<>();
		Date currentDate = new Date();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(currentDate);
		if (wfNodeDescription.equals(NOTIFY_LEGAL)) {
			cal.add(Calendar.DATE, 7);
		} else {
			cal.add(Calendar.DATE, 3);
		}
		if (workItem.getMetaDataMap().containsKey("comment")) {
			emailParams.put("comment", workItem.getMetaDataMap().get("comment", String.class));
		}
		emailParams.put("reviewTypeForAuthorAndReject", WorkFlowUtil.getReviewTypeByHistory(workItem, workflowSession));
		try {
			if (!authorizable.isGroup() && authorizable.hasProperty(NAME_PROPERTY)) {
				emailParams.put("initiator name", authorizable.getProperty(NAME_PROPERTY)[0].getString());
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		String expiryDate = cal.getTime().toString();
		emailParams.put("expiry date", expiryDate);
		WorkFlowUtil.fillEmailInfo(resourceResolver, workItem, authorizable, emailParams);

		Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
		if (null != externalizer) {
			emailParams.put("author_content_path",
					externalizer.authorLink(resourceResolver, WCM_EDITOR_URL_TOUCH_DEFAULT + workItem.getContentPath())
							+ ".html");
			emailParams.put("compare_content_path",
					externalizer.publishLink(resourceResolver, workItem.getContentPath()) + ".html");
			emailParams.put("preview_content_path",
					externalizer.authorLink(resourceResolver, workItem.getContentPath()) + ".html?wcmmode=disabled");
			return emailParams;
		} else {
			return null;
		}
	}

	/**
	 * this method gets resourceResolver from Session.
	 *
	 * @param session session
	 * @return ResourceResolver
	 */
	private ResourceResolver getResourceResolver(final Session session) throws LoginException {
		return resourceResolverFactory.getResourceResolver(
				Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
	}

	/**
	 * @param authorizable
	 * @return
	 */
	private List<String> getRecipientList(WorkItem workItem, Authorizable authorizable, String type,
			UserManager manager, WorkflowSession workflowSession) {
		List<String> recipientsList = new ArrayList<>();
		try {
			Value[] email;
			if (type.equals(REJECT) || type.equals(NOTIFY_AUTHOR)) {
				email = new Value[0];
				if (authorizable.hasProperty(EMAIL_PROPERTY)) {
					email = authorizable.getProperty(EMAIL_PROPERTY);
				}
			} else {
				MetaDataMap metaDataMap = workItem.getWorkflowData().getMetaDataMap();
				List<Value> emailList = new ArrayList<Value>();
				String businessUnit = getBusinessUnit(authorizable);
				if (type.equals(NOTIFY_EDITORIAL)) {
					String groupName = businessUnit + "-reviewer-editorial";
					Authorizable group = manager.getAuthorizable(groupName);
					if (null != group) {
						emailList = getEmailList(workItem, group);
						metaDataMap.put(ApplicationConstants.GROUP_NAME, groupName);
					}
				} else if (type.equals(NOTIFY_LEGAL)) {
					String groupName = businessUnit + "-reviewer-legal";
					Authorizable group = manager.getAuthorizable(groupName);
					if (null != group) {
						emailList = getEmailList(workItem, group);
						metaDataMap.put(ApplicationConstants.GROUP_NAME, groupName);
					}
				}
				workflowSession.updateWorkflowData(workItem.getWorkflow(), workItem.getWorkflowData());
				email = emailList.toArray(new Value[0]);
			}

			if (null != email) {
				if (email.length > 0) {
					for (Value value : email) {
						recipientsList.add(value.getString());
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("[REPOSITORY-EXCEPTION-002]RepositoryException in SendMailProcessStep {}", e);
		}
		return recipientsList;
	}

	/**
	 *
	 * @param authorizable
	 * @return
	 */
	private String getBusinessUnit(Authorizable authorizable) {
		Iterator<Group> authGroups = null;
		String businessUnit = StringUtils.EMPTY;
		try {
			authGroups = authorizable.declaredMemberOf();
			if (!authorizable.isGroup()) {
				while (authGroups.hasNext()) {
					Group group = authGroups.next();
					String groupId = group.getID();
					if (groupId.contains(BASIC_AUTHORS) || groupId.contains(POWER_AUTHORS)
							|| groupId.contains(KEMIN_ADMIN)) {
						businessUnit = groupId.split("-")[0];
						break;
					}
				}
			}
		} catch (RepositoryException e) {
			log.error("[REPOSITORY-EXCEPTION-003]RepositoryException in SendMailProcessStep {}", e);
		}
		return businessUnit;
	}

	/**
	 * @param workItem
	 * @param group
	 * @return
	 */
	private List<Value> getEmailList(WorkItem workItem, Authorizable group) {
		List<Value> emailList = new ArrayList<Value>();
		Group reviewerGroup = (Group) group;
		Value[] email = new Value[0];
		try {
			Iterator<Authorizable> members = reviewerGroup.getDeclaredMembers();
			while (members.hasNext()) {
				Authorizable member = members.next();
				if (member.hasProperty(EMAIL_PROPERTY)) {
					email = member.getProperty(EMAIL_PROPERTY);
					Collections.addAll(emailList, email);
				}
			}
		} catch (RepositoryException e) {
			log.error("[REPOSITORY-EXCEPTION-004]RepositoryException in SendMailProcessStep {}", e);
		}
		return emailList;
	}
}
