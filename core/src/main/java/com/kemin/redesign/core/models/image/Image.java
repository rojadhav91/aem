package com.kemin.redesign.core.models.image;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface Image {

  String getFileReference();

  String getAltText();

  String getLinkUrl();
}
