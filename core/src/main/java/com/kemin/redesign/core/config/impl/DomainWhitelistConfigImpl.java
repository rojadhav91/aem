package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.DomainWhitelistConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import java.util.List;

/** Implementation class for Whitelisted Domains config */
@Component(service = DomainWhitelistConfig.class, immediate = true)
@Designate(ocd = DomainWhitelistConfigImpl.Config.class, factory = false)
@ServiceDescription("A service used to configure Whitelisted Domains.")
@ServiceVendor("Dish DOP")
public class DomainWhitelistConfigImpl implements DomainWhitelistConfig {

  @Getter
  @SuppressWarnings("PMD.SingularField")
  private List<String> whitelistedDomains;

  /**
   * Set BriteVerify configuration
   *
   * @param config
   */
  @Activate
  public void activate(final DomainWhitelistConfigImpl.Config config) {
    whitelistedDomains = List.of(config.whitelistedDomains());
  }

  @ObjectClassDefinition(
      name = "DISH DOP Whitelisted Domains Configuration",
      description = "The configuration for validating email entered by customer.")
  public @interface Config {

    @AttributeDefinition(
        name = "Email Whitelist Domains",
        description = "List of whitelisted domains for leadgen form")
    String[] whitelistedDomains() default {};
  }

}
