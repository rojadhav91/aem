package com.kemin.redesign.core.models.herocarousel.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.herocarousel.HeroCarousel;
import com.kemin.redesign.core.models.herocarousel.HeroImage;

import javax.annotation.PostConstruct;
import lombok.Getter;
import java.util.UUID;
/** Kemin ExploreByFocusAnchor Sling Model Implementation Class. */
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {HeroCarousel.class, ComponentExporter.class},
	    resourceType = HeroCarouselImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class HeroCarouselImpl implements HeroCarousel {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/hero-carousel";

	@ChildResource @Getter private Collection<HeroImage> heroImages;

	@ValueMapValue @Getter private int timeLapse;
	
	private String id="";

	@PostConstruct
	protected void init() {
		id="heroCarousel_"+UUID.randomUUID().toString().substring(24);
    	if(heroImages==null)
    		heroImages = new ArrayList<HeroImage>();
	}
	
	@Override
	public String getId() {
	  return id;
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
