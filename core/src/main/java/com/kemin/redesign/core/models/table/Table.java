package com.kemin.redesign.core.models.table;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface Table extends ComponentExporter {
    String getTableData();
    String getTitle();
    String getDescription();
}
