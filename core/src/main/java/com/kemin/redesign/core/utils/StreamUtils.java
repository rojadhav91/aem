package com.dish.dopweb.core.utils;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/** Stream Utils to get stream of different collection types. */
public final class StreamUtils {
  private StreamUtils() {}

  /** Stream of Iterable. */
  public static <T> Stream<T> stream(final Iterable<T> iterable) {
    return stream(iterable.iterator());
  }

  /** Stream of Iterator. */
  public static <T> Stream<T> stream(final Iterator<T> iterator) {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(iterator, Spliterator.NONNULL), false);
  }

  /** Stream of Enumeration. */
  public static <T> Stream<T> stream(final Enumeration<T> enumeration) {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(enumeration.asIterator(), Spliterator.ORDERED), false);
  }
}
