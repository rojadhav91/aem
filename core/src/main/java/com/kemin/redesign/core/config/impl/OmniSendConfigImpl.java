package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.OmniSendConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Implementation class for BriteVerify config
 */
@Component(service = OmniSendConfig.class, immediate = true)
@Designate(ocd = OmniSendConfigImpl.Config.class, factory = false)
@ServiceDescription("A service used to configure Briteverify API")
@ServiceVendor("Dish DOP")
public class OmniSendConfigImpl implements OmniSendConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String omniSendKey;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String omniSendEndpoint;

    /**
     * Set BriteVerify configuration
     * @param config
     */
    @Activate
    public void activate(final OmniSendConfigImpl.Config config) {

        omniSendKey = config.omniSendKey();
        omniSendEndpoint = config.omniSendEndpoint();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Omnisend Configuration",
            description = "The configuration for storing data  entered by customer.")
    public @interface Config {

        @AttributeDefinition(name = "Omnisend Key", description = "Api Key for Omnisend")
        String omniSendKey() default  "";

        @AttributeDefinition(name = "BriteVerify Endpoint", description = "Omnisend Api Endpoint")
        String omniSendEndpoint() default  "";
    }
}
