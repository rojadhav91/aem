package com.dish.dopweb.core.models;

import com.dish.dopweb.core.configs.SitemapExternalizerCaConfig;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import java.util.Optional;

/**
 * Externalizer Sling Model.
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExternalizerModel {

    @Self
    private SlingHttpServletRequest request;

    /**
     * This method is used to return caconfig object for sitemap externalizer domain.
     * @return SitemapExternalizerCaConfig
     */
    public SitemapExternalizerCaConfig getSitemapExternalizerCaConfig() {
        return Optional.ofNullable(request.getResource())
                .map(resource1 -> resource1.adaptTo(ConfigurationBuilder.class))
                .map(configurationBuilder -> configurationBuilder.as(SitemapExternalizerCaConfig.class))
                .orElse(null);
    }

}
