package com.kemin.redesign.core.models.testimonial;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface TestimonialItem {

    String getQuote();

    String getAuthorInfo();

    String getBackgroundImageDesktop();

    String getBackgroundImageMobile();

    String getInitialCssClass();

    void setInitialCssClass(String cssClassName);
}
