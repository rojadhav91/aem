package com.kemin.redesign.core.models.blog.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import com.kemin.redesign.core.utils.CommonUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.blog.BlogCatalog;
import com.kemin.redesign.core.services.CatalogPageSearchService;

import lombok.extern.slf4j.Slf4j;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class BlogCatalogImpl.
 */
@Slf4j
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = {BlogCatalog.class,
        ComponentExporter.class}, resourceType = BlogCatalogImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BlogCatalogImpl implements BlogCatalog {
	
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    /** The Constant RESOURCE_TYPE. */
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/blog-catalog";

    /** The current page. */
    @ScriptVariable
    private Page currentPage;

    JSONArray productItemListJSON;

    /** The resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** The blog category beans. */
    List<BlogPagesBean> blogMarketPageList;

    /** The blog pages list. */
    List<BlogPagesBean> blogPagesList;

    /** The Catalog page search service. */
    @OSGiService
    CatalogPageSearchService CatalogPageSearchService;

    /** The default image path. */
    @ValueMapValue
    @Getter
    protected String defaultImagePath;


    /**
     * Inits the blog catalog.
     */
    @PostConstruct
    public void initBlogCatalog() {
        productItemListJSON = new JSONArray();
        if (getIsBlogHomePage()) {
            getBlogCategory(currentPage);
        }

        if (getIsBlogMarketPage() || getIsBlogHomePage()) {
        	LOGGER.debug("page is either blog home page or blog market page");
            blogPagesList = CatalogPageSearchService.getBlogPages(currentPage, defaultImagePath);
        }
        if(blogPagesList != null){
            for(BlogPagesBean b:blogPagesList){
                productItemListJSON.put(new JSONObject(b).toString());
            }
        }


    }

    /**
     * Gets the blog category.
     *
     * @param blogHomePage the blog home page
     * @return the blog category
     */
    private void getBlogCategory(Page blogHomePage) {
        blogMarketPageList = new ArrayList<>();
        Iterator<Page> it = blogHomePage.listChildren();
        Page categoryPage;
        BlogPagesBean bean;
        LOGGER.debug("going to get the blog market pages for home page:{}",blogHomePage.getPath());
        while (it.hasNext()) {
            categoryPage = it.next();
            //get all the blog market pages
            boolean isBlogMarketPage = CommonUtils.filterPage(categoryPage, "cq:template", ApplicationConstants.KEMIN_REDESIGN_BLOG_MARKET_PAGE_TEMPLATE);
            if (isBlogMarketPage) {
                bean = new BlogPagesBean();
                bean.setTitle(categoryPage.getTitle());
                bean.setPath(categoryPage.getPath());
                bean.setParentPageTitle(categoryPage.getParent().getTitle());
                bean.setThumbnailImage(getThumbnailImage(categoryPage));
                blogMarketPageList.add(bean);
            }
        }
    }


    /**
     * Gets the blog categories.
     *
     * @return the blog categories
     */
    @Override
    public List<BlogPagesBean> getBlogCategories() {
        return blogMarketPageList;
    }

    /**
     * Gets the checks if is blog home page.
     *
     * @return the checks if is blog home page
     */
    @Override
    public boolean getIsBlogHomePage() {
        if (currentPage.getTemplate().getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_BLOG_HOME_PAGE_TEMPLATE)) {
            return true;
        }
        return false;
    }

    /**
     * Gets the thumbnail image.
     *
     * @param page the page
     * @return the thumbnail image
     */
    private String getThumbnailImage(Page page) {
        String thumbnailImage = CommonUtils.getThumbnailImage(page);
        if (!StringUtils.isNotBlank(thumbnailImage)) {
            thumbnailImage = defaultImagePath;
        }
        return thumbnailImage;
    }

    /**
     * Gets the checks if is blog market page.
     *
     * @return the checks if is blog market page
     */
    @Override
    public boolean getIsBlogMarketPage() {
        if (currentPage.getTemplate().getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_BLOG_MARKET_PAGE_TEMPLATE)) {
            return true;
        }
        return false;
    }

    /**
     * Gets the blog pages.
     *
     * @return the blog pages
     */
    @Override
    public List<BlogPagesBean> getBlogPages() {
        return blogPagesList;
    }

    @Override
    public String getBlogPagesInJson() {
        return productItemListJSON.toString();
    }

    @Override
    public String currentPageTitle() {
        return null;
    }

    /**
     * Gets the exported type.
     *
     * @return the exported type
     */
    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

}
