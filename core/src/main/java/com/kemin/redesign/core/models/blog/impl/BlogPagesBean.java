package com.kemin.redesign.core.models.blog.impl;

import com.day.cq.wcm.api.Page;
import lombok.Data;

@Data
public class BlogPagesBean {

    private String title;
    private String path;
    private String thumbnailImage;
    private String parentPageTitle;

}
