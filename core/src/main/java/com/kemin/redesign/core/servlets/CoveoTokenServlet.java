/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.kemin.redesign.core.servlets;

import com.kemin.redesign.core.services.CoveoConfigService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=JSON Servlet get a coveo token for a user",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/coveo/token" })
public class CoveoTokenServlet extends SlingSafeMethodsServlet {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CoveoTokenServlet.class);

    @Reference
    private CoveoConfigService configuration;

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
            throws ServletException, IOException {
        log.info("creating new coveo token");
        HttpPost post = new HttpPost("https://platform.cloud.coveo.com/rest/search/v2/token");
        post.addHeader("Authorization", String.format("Bearer %s", configuration.getApiKey()));
        post.addHeader("Content-Type", "application/json");
        post.setEntity(new StringEntity(String.format(
                "{\"userIds\": [ { \"name\": \"anonymous\", \"provider\": \"Email Security Provider\" } ], \"searchHub\": \"%s\"}",
                configuration.getSearchHub())));
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            try (CloseableHttpResponse response = client.execute(post)) {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    try (InputStream instream = entity.getContent()) {
                        // do something useful
                        resp.getWriter().write(EntityUtils.toString(entity));
                    }
                }
            }
        }
    }
}
