package com.kemin.redesign.core.models.header;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface SubCategoryItem {

  String getLinkLabel();

  String getLink();

  void setLinkLabel(String title);

}
