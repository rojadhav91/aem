package com.kemin.redesign.core.models.anchor.impl;
import com.kemin.redesign.core.models.anchor.AnchorItem;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
        adaptables = Resource.class,
        adapters = AnchorItem.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class AnchorItemImpl implements AnchorItem{

    @ValueMapValue @Getter private String title;

    @ValueMapValue @Getter private String id;

}
