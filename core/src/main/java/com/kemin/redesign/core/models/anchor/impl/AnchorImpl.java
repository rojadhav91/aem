package com.kemin.redesign.core.models.anchor.impl;
import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.anchor.Anchor;
import com.kemin.redesign.core.models.anchor.AnchorItem;
import com.kemin.redesign.core.models.testimonial.TestimonialItem;
import com.kemin.redesign.core.utils.StreamUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Anchor.class, ComponentExporter.class},
        resourceType = AnchorImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)

public class AnchorImpl implements Anchor {
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/anchor";
    @ChildResource
    private Resource navigations;

    @Getter private Collection<AnchorItem> anchorItems;

    @SlingObject ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        anchorItems = Optional.ofNullable(navigations)
                .map(Resource::getChildren)
                .map(StreamUtils::stream)
                .orElseGet(Stream::empty)
                .map(resource -> resource.adaptTo(AnchorItem.class))
                .collect(Collectors.toList());


    }
    public Collection<AnchorItem> getAnchorItems(){
        return anchorItems;
    }
    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

}
