package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * CA Config to enable/disable chat.
 */
@Configuration(
    label = "Chat Configuration",
    description = "Enable chat functionality")
public @interface ChatConfig {

  /**
   * Enable Chat.
   *
   * @return boolean
   */
  @Property(label = "Enable chat on site.")
  boolean chatEnabled();

  /**
   * Enable Authenticated Chat.
   *
   * @return boolean
   */
  @Property(label = "Enable authenticated chat.")
  boolean chatAuthenticationEnabled();
}
