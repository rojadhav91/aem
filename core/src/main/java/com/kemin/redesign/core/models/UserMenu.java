package com.dish.dopweb.core.models;

import com.dish.dopweb.core.beans.LinkItems;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Collection;

/**
 * User Authenticated Menu Sling Model.
 */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        resourceType = "dopweb/components/structure/usermenu",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class UserMenu {

    @ValueMapValue
    @Getter
    private String headingText;

    @ChildResource
    @Getter
    private Collection<LinkItems> linkItems;

    @ValueMapValue
    @Getter
    private String menuViews;

    @PostConstruct
    protected void init() {
        linkItems = CollectionUtils.emptyIfNull(linkItems);
    }
}
