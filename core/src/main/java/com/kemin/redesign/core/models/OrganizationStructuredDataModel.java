package com.dish.dopweb.core.models;

import com.dish.dopweb.core.beans.OrganizationStructuredDataBean;
import com.dish.dopweb.core.utils.HostnameUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Named;

/**
 * Organization Structured Data Model.
 */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class OrganizationStructuredDataModel {

    @ValueMapValue
    private String legalName;

    @ValueMapValue
    private String logo;

    @ValueMapValue
    private String[] sameAs;

    @ValueMapValue
    private String structuredHomePage;

    @ValueMapValue
    @Named("jcr:description")
    private String description;

    @SlingObject
    private SlingHttpServletRequest request;

    private String jsonString;

    protected Boolean falseStatus = false;

    /**
     * Post construct method
     */
    @PostConstruct
    public void init() {
    if (!StringUtils.isBlank(structuredHomePage) && falseStatus.equals(isAllFieldSet())) {
        final OrganizationStructuredDataBean structuredDataBean = new OrganizationStructuredDataBean();
        structuredDataBean.setContext("https://schema.org");
        structuredDataBean.setType("Organization");
        if (StringUtils.isNotBlank(legalName)) {
            structuredDataBean.setName(legalName);
        }
        if (StringUtils.isNotBlank(description)) {
            structuredDataBean.setDescription(description);
        }
        if (StringUtils.isNotBlank(request.getRequestURL())) {
            structuredDataBean.setUrl(request.getRequestURL().toString());
        }
        if (StringUtils.isNotBlank(logo)) {
            final String hostName = HostnameUtil.getHostName(request.getRequestURL().toString());
            structuredDataBean.setLogo(hostName + logo);
        }
        if (ArrayUtils.isNotEmpty(sameAs)) {
            structuredDataBean.setSameAs(sameAs);
        }

        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        jsonString = gson.toJson(structuredDataBean);
    }
 }

    /**
     * This methos is checking all the fields authored or not
     * @return true or false
     */
 public Boolean isAllFieldSet(){
        Boolean fieldStatus = false;
     if (StringUtils.isBlank(legalName) && StringUtils.isBlank(logo) && ArrayUtils.isEmpty(sameAs)) {
         fieldStatus = true;
     }
     return fieldStatus;
 }

    public String getJsonString() {
        return jsonString;
    }
}
