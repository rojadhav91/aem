package com.kemin.redesign.core.models.header.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.header.Feature;
import com.kemin.redesign.core.models.header.FeatureItem;
import com.kemin.redesign.core.utils.StreamUtils;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.day.cq.wcm.foundation.List.log;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Feature.class, ComponentExporter.class},
        resourceType = com.kemin.redesign.core.models.header.impl.FeatureImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)

public class FeatureImpl implements Feature {
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/header";

    @ChildResource
    private Resource featureItemNavigation;

    @Getter
    private Collection<FeatureItem> featureItems;

    @PostConstruct
    protected void init() {
        featureItems = Optional.ofNullable(featureItemNavigation)
                .map(Resource::getChildren)
                .map(StreamUtils::stream)
                .orElseGet(Stream::empty)
                .map(resource -> resource.adaptTo(FeatureItem.class))
                .collect(Collectors.toList());
        getFeatureItems();
        }


public Collection<FeatureItem> getFeatureItems(){
        return featureItems;
}

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
