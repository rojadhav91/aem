package com.kemin.redesign.core.models.accordion.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.accordion.Accordion;
import com.kemin.redesign.core.models.accordion.AccordionItem;
import com.kemin.redesign.core.utils.StreamUtils;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters ={Accordion.class, ComponentExporter.class},
        resourceType = AccordionImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class AccordionImpl implements Accordion {
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/accordion";
    public static final String HEADING = "heading";
    public static final String PDP_ACCORDION_ID = "pdp-accordion-id-";

    @Getter private String id;

    @ValueMapValue @Getter private String title;

    @ChildResource private Resource accordions;

    @Getter private Collection<AccordionItem> accordionItems;

    @PostConstruct
    protected void init(){
        accordionItems =
                Optional.ofNullable(accordions)
                .map(Resource::getChildren)
                        .map(StreamUtils::stream)
                        .orElseGet(Stream::empty)
                        .map(resource -> resource.adaptTo(AccordionItem.class))
                        .collect(Collectors.toList());

        int uniqueId = (int)(Math.random()*100);
        if(StringUtils.isNotBlank(title)){
            String smallTitle = StringUtils.substring(title,0,10).trim();
            id = smallTitle.replaceAll("\\s+","-") + "-" + uniqueId;
        }else{
            id = PDP_ACCORDION_ID + uniqueId;
        }
    }

    @Override @NonNull
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
