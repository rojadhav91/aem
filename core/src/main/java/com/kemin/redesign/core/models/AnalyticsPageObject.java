package com.dish.dopweb.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.dish.dopweb.core.configs.LaunchScriptCaConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.settings.SlingSettingsService;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.apache.sling.models.annotations.injectorspecific.InjectionStrategy.OPTIONAL;

/** Sling model to get page level attributes for datalayer. */
@Model(
    adaptables = {SlingHttpServletRequest.class},
    resourceType = AnalyticsPageObject.RESOURCE_TYPE,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AnalyticsPageObject {
  public static final String SLASH = "/";
  public static final String PIPE = "|";
  public static final String HOME = "home";
  protected static final String RESOURCE_TYPE = "dopweb/components/structure/analytics-page-object";

  @ScriptVariable(injectionStrategy = OPTIONAL)
  public Page currentPage;

  @SlingObject private Resource resource;
  @Inject private SlingSettingsService slingSettingsService;
  @Self private SlingHttpServletRequest request;

  private PageManager pageManager() {
    return resource.getResourceResolver().adaptTo(PageManager.class);
  }

  /**
   * Get current page title.
   *
   * @return String
   */
  public String getPageName() {
    return isHomePage()
        ? pagePrefix().concat(HOME)
        : pagePrefix().concat(getCurrentPage().getName());
  }

  private String pagePrefix() {
    return Optional.ofNullable(resource)
        .map(resource1 -> resource1.adaptTo(ConfigurationBuilder.class))
        .map(cb -> cb.as(LaunchScriptCaConfig.class))
        .map(LaunchScriptCaConfig::dataLayerPagePrefix)
        .filter(StringUtils::isNotBlank)
        .map(s -> s.concat(PIPE))
        .orElse("");
  }

  private boolean isHomePage() {
    final String language = SLASH + getLanguage();
    return getPagePath().contains(language)
        && StringUtils.substringAfter(getPagePath(), language).isBlank();
  }

  /**
   * Get Domain name.
   *
   * @return String
   */
  public String getDomain() {
    return request.getServerName();
  }

  /**
   * Get current page language.
   *
   * @return String
   */
  public String getLanguage() {
    return getCurrentPage().getLanguage().getLanguage();
  }

  /**
   * Get Site Section.
   *
   * @return String
   */
  public String getSiteSection() {
    return getSubSection(1);
  }

  /**
   * Get Site Sub Section.
   *
   * @return String
   */
  public String getSiteSubSection() {
    return getSubSection(2);
  }

  /**
   * Get Site Sub Sub Section.
   *
   * @return String
   */
  public String getSiteSubSubSection() {
    return getSubSection(3);
  }

  private String getSubSection(final int pageLevel) {
    String pageTitle;
    final String language = SLASH + getLanguage();
    if (!isHomePage() && pageLevel > 0) {
      final String path = StringUtils.substringAfter(getPagePath(), language);
      pageTitle = path.split(SLASH).length > pageLevel ? path.split(SLASH)[pageLevel] : "";
    } else {
      pageTitle = HOME;
    }
    return pageTitle;
  }

  /**
   * Get Page URL.
   *
   * @return String
   */
  public String getUrl() {
    return request.getRequestURL().toString();
  }

  /**
   * Get Query String Parameters .
   *
   * @return String
   */
  public String getQsp() {
    return request.getQueryString();
  }

  /**
   * Get platform with RunMode and brand.
   *
   * @return String
   */
  public String getPlatform() {
    final String brand = getPagePath().split(SLASH)[2];
    return "AEM".concat(":").concat(getRunMode()).concat(":").concat(brand);
  }

  private String getRunMode() {
    final List<String> allEnvironments = List.of("DEV", "PROD", "QA", "STAGE");
    return slingSettingsService.getRunModes().stream()
        .map(String::toUpperCase)
        .filter(allEnvironments::contains)
        .findFirst()
        .orElse("DEV");
  }

  private String getPagePath() {
    return getCurrentPage().getPath();
  }

  private Page getCurrentPage() {
    return Optional.ofNullable(currentPage)
        .orElseGet(() -> pageManager().getContainingPage(resource));
  }
  
  /**
   * Get Current Page URL.
   *
   * @return String
   */
  public String getPageUrl() {
    return getPagePath();
  }
}
