package com.kemin.redesign.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.kemin.redesign.core.utils.WorkFlowUtil;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.*;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import java.util.Collections;
import java.util.Iterator;

//Sling Imports

//This is a component so it can provide or consume services
@Component(service = WorkflowProcess.class, property = { "process.label = Kemin-Redesign-Editorial Reviewer Check",
		Constants.SERVICE_DESCRIPTION + "=Workflow to decide whether to check  editorial reviewer", })
public class EditorialReviewerCheckProcess implements WorkflowProcess {

	private static final String POWER_AUTHORS = "power-authors";
	private static final String SKIP_EDITORIAL = "skipEditorial";

	/**
	 * Default log.
	 */
	protected final Logger log = LoggerFactory.getLogger(EditorialReviewerCheckProcess.class);
	@Reference
	ResourceResolverFactory resourceResolverFactory;

	public void execute(WorkItem workItem, WorkflowSession wfsession, MetaDataMap args) throws WorkflowException {

		ResourceResolver resourceResolver = null;
		try {
			resourceResolver = getResourceResolver(wfsession.adaptTo(Session.class));
			WorkFlowUtil.setPageInfoToWorkFlow(resourceResolver, workItem, wfsession);
			Resource pageResource = resourceResolver
					.getResource(workItem.getWorkflowData().getPayload().toString() + "/" + JcrConstants.JCR_CONTENT);
			UserManager manager = resourceResolver.adaptTo(UserManager.class);
			Authorizable authorizable = null;
			authorizable = manager.getAuthorizable(workItem.getWorkflow().getInitiator());
			boolean isPower = false;
			Iterator<Group> authGroups = authorizable.declaredMemberOf();
			if (!authorizable.isGroup()) {
				while (authGroups.hasNext()) {
					Group group = authGroups.next();
					String groupId = group.getID();
					if (groupId.contains(POWER_AUTHORS)) {
						isPower = true;
						break;
					}
				}
			}
			if (pageResource != null) {
				ValueMap pageProperties = pageResource.getValueMap();
				if (pageProperties.containsKey(SKIP_EDITORIAL)) {
					if (pageProperties.get(SKIP_EDITORIAL).toString().equals("true")) {
						wfsession.complete(workItem, wfsession.getRoutes(workItem, false).get(1));
					} else {
						wfsession.complete(workItem, wfsession.getRoutes(workItem, false).get(0));
					}
				} else {
					wfsession.complete(workItem, wfsession.getRoutes(workItem, false).get(0));
				}
			}
		} catch (Exception e) {
			log.error("exception occured in EditorialReviewerCheckProcess {}", e);
		}
	}

	private ResourceResolver getResourceResolver(Session session) throws LoginException {
		return resourceResolverFactory.getResourceResolver(
				Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
	}

}