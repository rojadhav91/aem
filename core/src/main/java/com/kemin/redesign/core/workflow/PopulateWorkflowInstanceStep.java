package com.kemin.redesign.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import java.util.Collections;
import java.util.List;

//Sling Imports

//This is a component so it can provide or consume services
@Component(service = WorkflowProcess.class, property = { "process.label = Kemin-Redesign-Populate instance process",
		Constants.SERVICE_DESCRIPTION
				+ "=Step that populates the data in the workflow instance for the workflow report", })
public class PopulateWorkflowInstanceStep implements WorkflowProcess {

	private static String EMAIL_PROPERTY = "./profile/email";
	private static String EDITORIAL_REVIEWER = "Editorial Reviewer";
	private static String LEGAL_REVIEWER = "Legal Reviewer";
	private static String WORKITEM_TITLE = "_title";
	/**
	 * Default log.
	 */
	protected final Logger log = LoggerFactory.getLogger(PopulateWorkflowInstanceStep.class);
	@Reference
	ResourceResolverFactory resourceResolverFactory;

	public void execute(WorkItem workItem, WorkflowSession wfsession, MetaDataMap args) throws WorkflowException {
		ResourceResolver resourceResolver = null;
		try {
			resourceResolver = getResourceResolver(wfsession.adaptTo(Session.class));
			UserManager manager = resourceResolver.adaptTo(UserManager.class);
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			List<HistoryItem> history = wfsession.getHistory(workItem.getWorkflow());
			String editorEmail = StringUtils.EMPTY;
			String legalEmail = StringUtils.EMPTY;
			if (manager != null) {
				for (int i = 0; i < history.size(); i++) {
					WorkItem wrkItm = history.get(i).getWorkItem();
					if (wrkItm.getWorkflowData().getMetaDataMap().containsKey(WORKITEM_TITLE)) {
						if (EDITORIAL_REVIEWER.equals(wrkItm.getWorkflowData().getMetaDataMap().get(WORKITEM_TITLE))) {
							String editorUserId = history.get(i).getUserId().split(" ")[0].trim();
							Authorizable editor = manager.getAuthorizable(editorUserId);
							editorEmail = getEmailId(editor);
						} else if (LEGAL_REVIEWER
								.equals(wrkItm.getWorkflowData().getMetaDataMap().get(WORKITEM_TITLE))) {
							String legalUserId = history.get(i).getUserId().split(" ")[0].trim();
							Authorizable legal = manager.getAuthorizable(legalUserId);
							legalEmail = getEmailId(legal);
						}
					}
				}
			}
			workItem.getWorkflowData().getMetaDataMap().put("legalApprover", legalEmail);
			workItem.getWorkflowData().getMetaDataMap().put("editorialApprover", editorEmail);
			Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
			String pagePath = workItem.getContentPath();
			if (null != pageManager) {
				Page page = pageManager.getPage(pagePath);
				if (null != page) {
					workItem.getWorkflowData().getMetaDataMap().put("pageTitle", page.getTitle());
				}
			}
			if (null != externalizer) {
				workItem.getWorkflowData().getMetaDataMap().put("fullyQualifiedUrl",
						externalizer.authorLink(resourceResolver, pagePath) + ".html");
			}
			if (!(workItem.getWorkflowData().getMetaDataMap().containsKey("isActivated"))) {
				workItem.getWorkflowData().getMetaDataMap().put("isActivated", "true");
			}
		} catch (LoginException e) {
			log.error("LoginException in PopulateWorkflowInstanceStep {}", e);
		} catch (RepositoryException e) {
			log.error("RepositoryException in PopulateWorkflowInstanceStep {}", e);
		}
	}

	/**
	 * @return User email ID
	 */
	private String getEmailId(Authorizable user) {
		String userEmail = StringUtils.EMPTY;
		Value[] email = new Value[0];
		try {
			if (null != user) {
				if (!user.isGroup() && user.hasProperty(EMAIL_PROPERTY)) {
					email = user.getProperty(EMAIL_PROPERTY);
					userEmail = email[0].getString();
				}
			}
		} catch (RepositoryException e) {
			log.error("RepositoryException in PopulateWorkflowInstanceStep {}", e);
		}
		return userEmail;
	}

	/**
	 * @return resource resolver
	 */
	private ResourceResolver getResourceResolver(Session session) throws LoginException {
		return resourceResolverFactory.getResourceResolver(
				Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
	}
}