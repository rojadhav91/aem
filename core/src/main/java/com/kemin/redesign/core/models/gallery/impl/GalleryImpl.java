package com.kemin.redesign.core.models.gallery.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.gallery.Gallery;
import com.kemin.redesign.core.models.gallery.GalleryItem;
import com.kemin.redesign.core.models.social.SocialLinks;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.stream.Collectors;

/** Kemin Social Links Sling Model Implementation Class. */
@Slf4j
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {Gallery.class, ComponentExporter.class},
	    resourceType = GalleryImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class GalleryImpl implements Gallery {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/image-gallery";

	@ChildResource @Getter private Collection<GalleryItem> galleryTiles;
	@ValueMapValue @Getter String title;
	
	@PostConstruct
	protected void init() {
		galleryTiles = CollectionUtils.emptyIfNull(galleryTiles).stream().filter(item ->StringUtils.isNotBlank(item.getImage()) && StringUtils.isNotBlank(item.getCaption())).collect(Collectors.toList());
		log.info(galleryTiles.toString());
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
