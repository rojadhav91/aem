package com.kemin.redesign.core.models.dynamicmedia.impl;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.models.dynamicmedia.DynamicImage;
import com.kemin.redesign.core.utils.CommonUtils;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = { DynamicImage.class,
		ComponentExporter.class }, resourceType = DynamicImageImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class DynamicImageImpl implements DynamicImage {

	public static final Logger LOGGER = LoggerFactory.getLogger(DynamicImageImpl.class);

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/dynamicimage";

	private static final String DAM_SCENE7_NAME_PROP = "dam:scene7Name";
	private static final String DAM_SCENE7_FILE_PROP = "dam:scene7File";
	private static final String DAM_SCENE7_DOMAIN_PROP = "dam:scene7Domain";

	private static String CONTENT_URL = "is/content/";
	private static String IMAGE_SERVER = "is/image/";
	private static String VIEWER_PATH = "s7viewers/";
	
	private static String PRESETS = "presets";

	private boolean isDymImage = true;
	@ScriptVariable
	private Page currentPage;

	private String mediaId;

	private String localeLanguage = "en";

	private String dynamicMediaUrl;

	private String damScene7Domain;

	private String damScene7File;

	private String altText;

	private String assetName;
	
	private String presets;

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	@SlingObject
	Resource resource;

	@PostConstruct
	protected void init() {
		
		if (slingHttpServletRequest != null) {
			LOGGER.debug("slingHttpServletRequest.getResourcePath():"+ slingHttpServletRequest.getRequestPathInfo().getSelectorString());
			String[] selectors = slingHttpServletRequest.getRequestPathInfo().getSelectors();
			if(selectors.length>1 && selectors[0].toLowerCase().startsWith(PRESETS)) {
				presets = selectors[1];
			}
		}

		localeLanguage = CommonUtils.getSelectedLanguage(currentPage.getPath());
		if(StringUtils.isNotBlank(localeLanguage)&& localeLanguage.contains("-")) {
			localeLanguage = localeLanguage.substring(0,localeLanguage.indexOf("-"));
		}

		if (resource != null) {
			mediaId = "dynamicmedia-" + getHashCode(resource.getPath());
			
			Asset asset = resource.adaptTo(Asset.class);
			if (asset != null) {
				assetName = asset.getName();
				String damScene7Name = StringUtils.defaultIfBlank("", asset.getMetadataValue(DAM_SCENE7_NAME_PROP));
				damScene7File = StringUtils.defaultIfBlank("", asset.getMetadataValue(DAM_SCENE7_FILE_PROP));
				damScene7Domain = StringUtils.defaultIfBlank("", asset.getMetadataValue(DAM_SCENE7_DOMAIN_PROP));

				if (StringUtils.isNotEmpty(damScene7Domain)) {
					dynamicMediaUrl = damScene7Domain + IMAGE_SERVER + damScene7File;
					if(StringUtils.isNotBlank(presets)){
						dynamicMediaUrl+="?$"+presets+"$";
					}
				} else {
					dynamicMediaUrl = resource.getPath();
					isDymImage = false;
				}

				altText = StringUtils.defaultIfBlank(asset.getMetadataValue(DamConstants.DC_TITLE),
						asset.getMetadataValue(DamConstants.DC_DESCRIPTION));
				altText = StringUtils.defaultIfBlank(altText, damScene7Name);
			}
		}
	}

	private int getHashCode(String d) {
		if (StringUtils.isBlank(d))
			return 1;
		int h = 0, g = 0;
		for (int i = d.length() - 1; i >= 0; i--) {
			int c = Character.codePointAt(d, i);
			h = ((h << 6) & 0xfffffff) + c + (c << 14);
			if ((g = h & 0xfe00000) != 0)
				h = (h ^ (g >> 21));
		}
		return h;
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}

	@Override
	public String getDomainHost() {
		return damScene7Domain;
	}

	@Override
	public String getMediaId() {
		return mediaId;
	}

	@Override
	public String getViewerPath() {
		return damScene7Domain + VIEWER_PATH;
	}

	@Override
	public String getImageServer() {
		return damScene7Domain + IMAGE_SERVER;
	}

	@Override
	public String getVideoServer() {
		return damScene7Domain + CONTENT_URL;
	}

	@Override
	public String getContentUrl() {
		return damScene7Domain + CONTENT_URL;
	}

	@Override
	public String getImageUrl() {
		return dynamicMediaUrl;
	}

	@Override
	public String getLocaleLanguage() {
		return localeLanguage;
	}

	@Override
	public boolean isDymImage() {
		return isDymImage;
	}

	@Override
	public String getAssetPath() {
		return damScene7File;
	}

	@Override
	public String getAssetName() {
		return assetName;
	}

	@Override
	public String getAltText() {
		return altText;
	}

	@Override
	public String getPresets() {
		return presets;
	}

}