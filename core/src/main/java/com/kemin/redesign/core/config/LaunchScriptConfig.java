package com.dish.dopweb.core.configs;

/**
 * Adobe Launch Script Configuration Class
 */
public interface LaunchScriptConfig {
    /**
     * Get Adobe Launch Script.
     * @return launchScript
     */
    String getLaunchScript();
}
