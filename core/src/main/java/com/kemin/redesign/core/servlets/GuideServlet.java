package main.java.com.aembootcamp.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.json.simple.JSONArray;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import com.day.cq.wcm.commons.ResourceIterator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Guide Detail servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.resourceTypes=" + "aem-bootcamp/components/general/guide-listing",
		"sling.servlet.extensions=" + "json", "sling.servlet.selectors=" + "guides"

})
public class GuideServlet extends SlingSafeMethodsServlet {

	private static final Logger LOGGER = Logger.getLogger(GuideServlet.class);
	private List<String> list;

	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
			throws ServletException, IOException {
		list = new ArrayList<String>();
		Iterator resourceIterator = guideQueryExc(req,req.getParameter("rootPath"));
		guideIterator(resourceIterator);
		PrintWriter out = resp.getWriter();
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		out.print(list);
		out.flush();
	}

	private Iterator guideQueryExc(SlingHttpServletRequest req, String uri) {
		ResourceResolver resourceResolver = req.getResourceResolver();
		Session session = resourceResolver.adaptTo(Session.class);
		String queryString = "SELECT * FROM [cq:PageContent] WHERE [cq:template] = '/apps/aem-bootcamp/templates/guide-detail' AND ISDESCENDANTNODE(["+uri+"])";
		Iterator resourceIterator = resourceResolver.findResources(queryString, Query.JCR_SQL2);
		return resourceIterator;
	}

	private void guideIterator(Iterator resourceIterator) {
		try {
			while(resourceIterator.hasNext()) {
				GuideFeed guide = new GuideFeed();
				Resource res = (Resource) resourceIterator.next();
				Iterator<Resource> itr=res.listChildren();
				while(itr.hasNext()) {
					Resource reso=res.getChild(itr.next().getPath());
					ValueMap valuemap=reso.getValueMap();
					if(valuemap.get("title", String.class) != null) {
						if (valuemap.get("fileReference", String.class) != null) {
							guide.setHeroImagePath(valuemap.get("fileReference", String.class));
						}
						if (valuemap.get("date", String.class) != null) {
							guide.setGuideTitle(valuemap.get("title", String.class));
							guide.setGuidepublishDate(valuemap.get("date", String.class));
							String uri=reso.getPath().replace("/jcr:content/guide-detail", ".html");
							guide.setGuideDetailLink(uri);
						}
					}
				}
				convertToJson(guide);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	private void convertToJson(GuideFeed guide) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			list.add(mapper.writeValueAsString(guide));
		} catch (JsonProcessingException e) {
			LOGGER.error(e);
		}
	}
}