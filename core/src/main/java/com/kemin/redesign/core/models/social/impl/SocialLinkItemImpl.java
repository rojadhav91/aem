package com.kemin.redesign.core.models.social.impl;

import com.kemin.redesign.core.models.social.SocialLinkItem;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
    adaptables = Resource.class,
    adapters = SocialLinkItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialLinkItemImpl implements SocialLinkItem {

  @ValueMapValue @Getter private String link;

  @ValueMapValue @Getter private String platform;
}
