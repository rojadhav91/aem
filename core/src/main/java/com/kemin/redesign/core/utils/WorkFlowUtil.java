package com.kemin.redesign.core.utils;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.kemin.redesign.core.ApplicationConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.resource.*;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import java.util.*;

public class WorkFlowUtil {
    
    private static Logger log = LoggerFactory.getLogger(WorkFlowUtil.class);

    private static final String EDITORIAL_REVIEWER = "Editorial Reviewer";
    private static final String LEGAL_REVIEWER = "Legal Reviewer";
    private static final String PROCESS_ARGS = "PROCESS_ARGS";
    
    public static void setPageInfoToWorkFlow(ResourceResolver resourceResolver, WorkItem workItem, WorkflowSession workflowSession) {
        try {
            MetaDataMap workflowMetaDataMap = workItem.getWorkflow().getWorkflowData().getMetaDataMap();
            ValueMap pageProperties = getPageValueMap(resourceResolver, workItem);
            String priority = getStringProperty(pageProperties, ApplicationConstants.PRIORITY);
            String skipReviewMessage = getStringProperty(pageProperties, ApplicationConstants.SKIP_REVIEW_MESSAGE);
            String workflowStartComment = getStringFromMetaData(workflowMetaDataMap, ApplicationConstants.WORKFLOW_START_COMMENT);

            if (StringUtils.isNotEmpty(workflowStartComment)) {
                workflowStartComment = skipReviewMessage + "\r\n" + workflowStartComment;
            } else {
                workflowStartComment = skipReviewMessage;
            }
            workflowMetaDataMap.put(ApplicationConstants.WORKFLOW_START_COMMENT, workflowStartComment);
            workflowMetaDataMap.put(ApplicationConstants.WORKFLOW_TASK_PRIORITY, priority);
            workflowMetaDataMap.put(ApplicationConstants.WORKFLOW_TASK_DUE_DATE, getDueDate(pageProperties));
            workflowSession.updateWorkflowData(workItem.getWorkflow(), workItem.getWorkflow().getWorkflowData());
        } catch (Exception e) {
            log.error("Write page info to workflow error", e);
            e.printStackTrace();
        }
    }
    
    public static void fillEmailInfo(ResourceResolver resourceResolver, WorkItem workItem, Authorizable initiator, Map<String, String> emailParams) {
        try {
            ValueMap pageProperties = getPageValueMap(resourceResolver, workItem);
            String pageTitle = getStringProperty(pageProperties, JcrConstants.JCR_TITLE);
            String comment = getStringProperty(pageProperties, ApplicationConstants.SKIP_REVIEW_MESSAGE);
            String priority = getStringProperty(pageProperties, ApplicationConstants.PRIORITY);
            Date dueDate = getDueDate(pageProperties);
            
            String firstName = getStringFromValue(initiator.getProperty(ApplicationConstants.FIRST_NAME));
            String lastName = getStringFromValue(initiator.getProperty(ApplicationConstants.LAST_NAME));
            
            MetaDataMap workflowMetaData = workItem.getWorkflow().getWorkflowData().getMetaDataMap();
            String workFlowTitle = getStringFromMetaData(workflowMetaData, ApplicationConstants.WORKFLOW_TITLE);
            emailParams.put("priority", priority);
            emailParams.put("initiatorName", firstName + " " + lastName);
            emailParams.put("workFlowTitle", workFlowTitle);
            emailParams.put("reviewType", getReviewType(workItem));
            emailParams.put("pageTitle", pageTitle);
            emailParams.put("dueDate", dueDate.toString());

            Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
            if (null != externalizer) {
                emailParams.put("pageUrl", externalizer.authorLink(resourceResolver,"/sites.html"+workItem.getContentPath()));
            }

            emailParams.put("initialComment", comment);
        }
        catch (RepositoryException e) {
            log.error("Get Page Info Error", e);
        }
    }
    
    private static String getStringFromMetaData(MetaDataMap workflowMetaData, String key) {
        String value = StringUtils.EMPTY;
        if (workflowMetaData.get(key) != null) {
            value = workflowMetaData.get(key).toString();
        }
        return value;
    }
    
    private static ValueMap getPageValueMap(ResourceResolver resourceResolver, WorkItem workItem) {
        Resource pageResource = resourceResolver.getResource(workItem.getWorkflowData().getPayload().toString() + "/" + JcrConstants.JCR_CONTENT);
        return pageResource.getValueMap();
    }
    
    private static Date getDueDate(ValueMap pageProperties) {
        Date dueDate;
        if (pageProperties.get(ApplicationConstants.DUE_DATE) != null) {
            GregorianCalendar orginalDate = (GregorianCalendar) pageProperties.get(ApplicationConstants.DUE_DATE);
            dueDate = orginalDate.getTime();
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR_OF_DAY, ApplicationConstants.DUE_DATE_HOURS);
            dueDate = calendar.getTime();
        }
        return dueDate;
    }
    
    public static String getStringProperty(ValueMap pageProperties, String key) {
        String value = (String) pageProperties.get(key);
        if (value == null) {
            value = StringUtils.EMPTY;
        }
        return value;
    }
    
    private static String getReviewType(WorkItem workItem) {
        String workItemDescription = workItem.getNode().getDescription();
        String reviewType = "";
        if (ApplicationConstants.NOTIFY_EDITORIAL.equals(workItemDescription)) {
            reviewType = ApplicationConstants.REVIEW_TYPE_EDITORIAL;
        } else if (ApplicationConstants.NOTIFY_LEGAL.equals(workItemDescription)) {
            reviewType = ApplicationConstants.REVIEW_TYPE_LEGAL;
        }
        return reviewType;
    }
    
    private static String getStringFromValue(Value[] values) throws RepositoryException {
        String value = "";
        if (values != null && values.length > 0) {
            value = values[0].getString();
        }
        return value;
    }
    
    public static ResourceResolver getResourceResolver(ResourceResolverFactory resourceResolverFactory, Session session) throws LoginException {
        return resourceResolverFactory
                .getResourceResolver(Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
    }

    public static String getReviewTypeByHistory(WorkItem workItem, WorkflowSession wfsession) throws WorkflowException {
        List<HistoryItem> history = wfsession.getHistory(workItem.getWorkflow());
        boolean hasEditorial = false;
        boolean hasLegal = false;
        for (int i = 0; i < history.size(); i++) {
            WorkItem wrkItm = history.get(i).getWorkItem();
            String title = wrkItm.getNode().getTitle();
            if (EDITORIAL_REVIEWER.equals(title)) {
                hasEditorial = true;
            } else if (LEGAL_REVIEWER.equals(title)) {
                hasLegal = true;
            }
        }

        if (hasLegal) {
            return ApplicationConstants.REVIEW_TYPE_LEGAL;
        } else if (hasEditorial) {
            return ApplicationConstants.REVIEW_TYPE_EDITORIAL;
        } else {
            return StringUtils.EMPTY;
        }
    }

    public static String getArgsAsString(MetaDataMap dataMap) {
        return dataMap.get(PROCESS_ARGS, String.class);
    }
}