package com.dish.dopweb.core.beans;

import lombok.Getter;
import lombok.Setter;

/** @author imran.padshah */
@SuppressWarnings("PMD")
@Getter
@Setter
public class TagItems {
  private String id;
  private String name;
}
