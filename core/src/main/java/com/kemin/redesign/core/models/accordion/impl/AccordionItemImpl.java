package com.kemin.redesign.core.models.accordion.impl;

import com.kemin.redesign.core.models.accordion.AccordionItem;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Model(
        adaptables = Resource.class,
        adapters = AccordionItem.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Slf4j
public class AccordionItemImpl implements AccordionItem{
    public static final String ID = "id";
    @ValueMapValue @Getter private String id;
    @ValueMapValue @Getter private String heading;

    @SlingObject
    Resource resource;

    @PostConstruct
    protected void init(){
        try {
            String uniqueId = String.join("-", getClass().getSimpleName(), String.valueOf(Math.abs(resource.getPath().hashCode() - 1)));
            if(!resource.getValueMap().containsKey(ID)){
                Optional.ofNullable(resource.adaptTo(ModifiableValueMap.class))
                        .ifPresent(modifiableValueMap -> modifiableValueMap.put(ID, uniqueId));
                resource.getResourceResolver().commit();
            }
        } catch (PersistenceException e) {
            log.error("Error occurred while storing accordion item Id:{}",e.getMessage());
        }
    }
}
