package com.kemin.redesign.core.models.footer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.footer.LinkItem;
import com.kemin.redesign.core.models.footer.LinksAndDisclaimer;
import lombok.Getter;
/** Kemin Footer Links And Disclaimer Sling Model Implementation Class. */
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {LinksAndDisclaimer.class, ComponentExporter.class},
	    resourceType = LinksAndDisclaimerImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class LinksAndDisclaimerImpl implements LinksAndDisclaimer {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/footer";

	@ChildResource @Getter private Collection<LinkItem> links;

	@ValueMapValue @Getter private String disclaimer;
	
	@ValueMapValue @Named("fileReference") @Getter private String logo;
	
	@ValueMapValue @Getter private String logoLink;
	
	@PostConstruct
	protected void init() {
		links = CollectionUtils.emptyIfNull(links).stream().filter(item ->StringUtils.isNotBlank(item.getUrl()) && StringUtils.isNotBlank(item.getTitle())).collect(Collectors.toList());
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
