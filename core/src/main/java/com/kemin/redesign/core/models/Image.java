package com.aembootcamp.core.models;

import javax.annotation.Resource;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = SlingHttpServletRequest.class, resourceType = Image.resourceType, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Image {
	protected static final String resourceType = "aem-bootcamp/components/general/hero-image";

	@ValueMapValue(name = "fileReference")
	private String fileReference;

	@ValueMapValue(name = "title")
	private String title;

	@ValueMapValue(name = "alttext")
	private String alttext;

	public String getFileReference() {
		return fileReference;
	}

	public String getTitle() {
		return title;
	}

	public String getAlttext() {
		return alttext;
	}
}