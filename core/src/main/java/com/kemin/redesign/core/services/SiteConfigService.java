package com.kemin.redesign.core.services;

import java.util.List;

public interface SiteConfigService {
    public List<String> getBreadcrumbHiddenPaths();
}
