package com.kemin.redesign.core.models.breadcrumb.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.utils.CommonUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.models.breadcrumb.Breadcrumb;
import com.kemin.redesign.core.services.SiteConfigService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = { Breadcrumb.class,
		ComponentExporter.class }, resourceType = BreadcrumbImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BreadcrumbImpl implements Breadcrumb {
	protected static final String RESOURCE_TYPE = "kemin-redesign/components/breadcrumb";

	@ScriptVariable
	private Page currentPage;
	private List<Page> list = new ArrayList<>();
	private int startLevel = 5;
	private boolean showHidden = false;
	private boolean hideCurrent = true;

	@Inject
	private SiteConfigService siteConfigService;

	@PostConstruct
	public void initBreadCrumb() {
		List<Page> items = new ArrayList<>();
		int currentLevel = currentPage.getDepth();
		while (startLevel < currentLevel) {
			Page page = currentPage.getAbsoluteParent(startLevel);
			if (page != null && page.getContentResource() != null) {
				boolean isActivePage = page.equals(currentPage);
				if (isActivePage && hideCurrent) {
					break;
				}
				if (checkIfNotHidden(page)) {
					items.add(page);
				}
			}
			startLevel++;
		}
		list = items;
	}

	@Override
	public List<Page> getPageList() {
		return list;
	}

	@Override
	public Page getParent() {
		return currentPage.getParent();
	}

	@Override
	public Page getCurrentPage() {
		return currentPage;
	}

	@Override
	public int getPageDepth() {
		return currentPage.getDepth();
	}

	@Override
	public String getIsProduct() {
		if (currentPage.getTitle().contains("product")) {
			return "product";
		}
		return null;
	}

	@Override
	public Page getHomePage() {
		return currentPage.getAbsoluteParent(4);
	}

	private boolean checkIfNotHidden(Page page) {
		return (!page.isHideInNav() || showHidden)
				&& !siteConfigService.getBreadcrumbHiddenPaths().contains(page.getName().toLowerCase());
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}


	@Override
	public boolean getIsSearchPage() {
		return isSearchPage();
	}

	/**
	 * Gets the home page for search page.
	 * This method is required to get the home page since search page is on same level as home
	 * @return the home page for search page
	 */
	@Override
	public String getHomePageForSearchPage() {
		if (isSearchPage()) {
			Page parentPage = currentPage.getParent();
			if (parentPage != null) {
				Iterator<Page> it = parentPage.listChildren();
				while (it.hasNext()) {
					Page childPage = it.next();
					boolean isHomePage = CommonUtils.filterPage(childPage, "cq:template", ApplicationConstants.KEMIN_REDESIGN_HOME_PAGE_TEMPLATE);
					if (isHomePage) {
						return childPage.getPath();
					}
				}
			}
		}
		return null;
	}

	private boolean isSearchPage() {
		boolean isSearchPage  = false;
		if (currentPage.getTemplate().getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_SEARCH_PAGE_TEMPLATE))
			isSearchPage =  true;
		return isSearchPage;
	}
}
