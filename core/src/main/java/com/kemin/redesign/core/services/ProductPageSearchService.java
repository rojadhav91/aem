package com.kemin.redesign.core.services;

import com.kemin.redesign.core.models.product.impl.TierTagList;
import org.json.JSONException;
import java.util.List;


public interface ProductPageSearchService {
    String getProductItemListJSON(String path, String[] prodAppCat) throws JSONException;
    String getAllTagsToPageJSON();
    List<TierTagList> getTierTagList();
}
