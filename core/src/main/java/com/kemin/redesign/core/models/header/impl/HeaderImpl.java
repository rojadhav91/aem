package com.kemin.redesign.core.models.header.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.models.header.*;
import com.kemin.redesign.core.models.image.Image;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;

/** Customer Portal Header Sling Model Implementation Class. */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Header.class, ComponentExporter.class},
        resourceType = HeaderImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class HeaderImpl implements Header {

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/structure/header";

    private static final Logger LOGGER = LoggerFactory.getLogger(HeaderImpl.class);

    @ChildResource @Getter private Image logo;

    @ChildResource @Getter private Collection<HeaderItem> headerLinks;

    @ValueMapValue @Getter private boolean displayCountryLanguageSelector;

    @ValueMapValue @Getter private String helpPageUrl;

    @SlingObject ResourceResolver resourceResolver;

    @ValueMapValue @Getter protected String signIn;

    @ValueMapValue @Getter protected String productCatalog;

    @ValueMapValue @Getter protected String news;

    @ValueMapValue @Getter protected String blog;

    @ValueMapValue @Getter protected String contactUs;

    @ChildResource @Getter private Collection<CategoryItem> productCategoryItem;

    @ChildResource @Getter private Collection<CategoryItem> serviceCategoryItem;

    @PostConstruct
    protected void init() {
        headerLinks = CollectionUtils.emptyIfNull(headerLinks);
        signIn = checkUrlAvailability(signIn);
        productCatalog = checkUrlAvailability(productCatalog);
        news = checkUrlAvailability(news);
        blog = checkUrlAvailability(blog);
        contactUs = checkUrlAvailability(contactUs);
        productCategoryItem = CollectionUtils.emptyIfNull(productCategoryItem);
        serviceCategoryItem = CollectionUtils.emptyIfNull(serviceCategoryItem);
    }

    public String checkUrlAvailability(String linkUrl){
        return resourceResolver.getResource(linkUrl)!=null ? linkUrl:"";
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    private void setCategoryPageTitle(CategoryItem item) {
        if (!StringUtils.isNotBlank(item.getLinkLabel())) {
            LOGGER.debug("component title is null, going to get title from category page");
            Page page = resourceResolver.getResource(item.getLink()) != null ? resourceResolver.getResource(item.getLink()).adaptTo(Page.class) : null;
            if (page != null) {
                item.setLinkLabel(page.getTitle());
            }
        }
            List<SubCategoryItem> subCategoryItemList = item.getSubCategoryItem();
            if (CollectionUtils.isNotEmpty(subCategoryItemList)) {
                subCategoryItemList.forEach(this :: setSubCategoryPageTitle);
            }
    }

    private void setSubCategoryPageTitle(SubCategoryItem item) {
        if (!StringUtils.isNotBlank(item.getLinkLabel()) && resourceResolver.getResource(item.getLink()) != null) {
            LOGGER.debug("component title is null, going to get title from subcategory page");
            Resource res = resourceResolver.getResource(item.getLink());
            Page page = res.adaptTo(Page.class);
            if(page!=null){
                item.setLinkLabel(page.getTitle());
            }
        }
    }

    public Collection<CategoryItem> getProductCategoryList() {
        if (CollectionUtils.isNotEmpty(productCategoryItem)) {
            productCategoryItem.forEach(this :: setCategoryPageTitle);
        }
        return productCategoryItem;
    }

    public Collection<CategoryItem> getServiceCategoryList() {
        if (CollectionUtils.isNotEmpty(serviceCategoryItem)) {
            serviceCategoryItem.forEach(this :: setCategoryPageTitle);
        }
        return serviceCategoryItem;
    }
}
