package com.kemin.redesign.core.servlets;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import com.day.cq.dam.api.Asset;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.beans.Coordinate;
import com.kemin.redesign.core.beans.SupplierLocation;
import com.kemin.redesign.core.utils.CommonUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.query.Query;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Component(service = Servlet.class, property = {
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.resourceTypes=" + "kemin-redesign/components/store-locator",
        "sling.servlet.selectors=" + "locations",
        "sling.servlet.extensions=" + "json"})
@ServiceDescription("JSON Servlet to read the locations from content fragment and provide to supplier locator component")
public class SupplierLocatorLocationsServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SupplierLocatorLocationsServlet.class);

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {

        List<SupplierLocation> allLocations = getLocationsFromDAMJSON(request);
        String locationJson = new Gson().toJson(allLocations);
        response.setContentType("application/json");
        response.getWriter().write(locationJson);
    }

    private List<SupplierLocation> getLocationsFromDAMJSON(SlingHttpServletRequest request) {
        List<SupplierLocation> locations = new ArrayList<>();
        try {
            String qury = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + ApplicationConstants.KEMIN_REDESIGN_SUPPLIER_LOCATION_DIR + "]) " +
                    " and  NAME() ='" + ApplicationConstants.KEMIN_REDESIGN_SUPPLIER_LOCATION_FILE + "' ";

            ResourceResolver resourceResolver = request.getResourceResolver();
            Iterator<Resource> resourcesDam = resourceResolver.findResources(qury, Query.JCR_SQL2);
            while (resourcesDam.hasNext()) {
                Resource resource = resourcesDam.next();
                return CommonUtils.convertJsonToLocationList(resource);
            }
        } catch (Exception e) {
            LOGGER.error("Error in fetching JSON File from location  :" + e.toString());
        }
        return locations;
    }

    private List<SupplierLocation> getLocations(SlingHttpServletRequest request) {
        List<SupplierLocation> locations = new ArrayList<>();
        //LOGGER.info( " : locale=" + currentPageLocale + "  fragemt path:=" + fragmentPath + "  current page path;=" + currentPagePath);
        String qury = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + ApplicationConstants.KEMIN_REDESIGN_SUPPLIER_LOCATOR_CONTENT_FRAGMENTS + "]) order by s.[jcr:created] asc ";

        LOGGER.info("query :" + qury);
        try {


            ResourceResolver resourceResolver = request.getResourceResolver();
            Iterator<Resource> fragmentResources = resourceResolver.findResources(qury, Query.JCR_SQL2);

            while (fragmentResources.hasNext()) {
                ContentFragment contentFragment = fragmentResources.next().adaptTo(ContentFragment.class);
                if (contentFragment != null) {
                    ContentElement keywordElement = contentFragment.getElement("title");
                    FragmentData keywordFragmentData = keywordElement.getValue();
                    String title = keywordFragmentData.getValue(String.class);

                    keywordElement = contentFragment.getElement("image");
                    keywordFragmentData = keywordElement.getValue();
                    String image = keywordFragmentData.getValue(String.class);

                    keywordElement = contentFragment.getElement("address_1");
                    keywordFragmentData = keywordElement.getValue();
                    String address1 = keywordFragmentData.getValue(String.class);

                    keywordElement = contentFragment.getElement("address_2");
                    keywordFragmentData = keywordElement.getValue();
                    String address2 = keywordFragmentData.getValue(String.class);

                    keywordElement = contentFragment.getElement("coords_lat");
                    keywordFragmentData = keywordElement.getValue();
                    double latitude = keywordFragmentData.getValue(Double.class);

                    keywordElement = contentFragment.getElement("coords_long");
                    keywordFragmentData = keywordElement.getValue();
                    double longitude = keywordFragmentData.getValue(Double.class);

                    keywordElement = contentFragment.getElement("place_id");
                    keywordFragmentData = keywordElement.getValue();
                    String placeId = keywordFragmentData.getValue(String.class);

                    keywordElement = contentFragment.getElement("details");
                    keywordFragmentData = keywordElement.getValue();
                    String details = keywordFragmentData.getValue(String.class);

                    SupplierLocation location = new SupplierLocation();

                    Coordinate coordinate = new Coordinate();
                    location.setTitle(title);
                    location.setImage(image);
                    location.setAddress1(address1);
                    location.setAddress2(address2);
                    location.setPlaceId(placeId);
                    location.setDetails(details);
                    coordinate.setLat(latitude);
                    coordinate.setLng(longitude);
                    location.setCoords(coordinate);
                    locations.add(location);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Exception in fetching icons for navigation component :" + e.toString());
        }
        return locations;
    }


}
