package com.kemin.redesign.core.models.anchor;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;
import java.util.Collection;
import java.util.List;

@ConsumerType

public interface Anchor extends ComponentExporter{
    public Collection<AnchorItem> getAnchorItems();


}
