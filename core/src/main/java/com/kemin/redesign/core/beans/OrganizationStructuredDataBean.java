package com.dish.dopweb.core.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This pojo is to get and set Organization Structure data values.
 */
@SuppressWarnings("PMD")
public class OrganizationStructuredDataBean {
    @SerializedName("@context")
    @Expose
    private String context;

    @SerializedName("@type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("logo")
    @Expose
    private String logo;

    @SerializedName("sameAs")
    private String[] sameAs;


    public String getContext() {
        return context;
    }

    public void setContext(final String context) {
        this.context = context;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(final String logo) {
        this.logo = logo;
    }

    public String[] getSameAs() {
        return sameAs.clone();
    }

    public void setSameAs(final String[] sameAs) {
         this.sameAs = sameAs.clone();
    }
}
