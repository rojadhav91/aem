package com.kemin.redesign.core.models.social;

import java.util.Collection;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface SocialLinks extends ComponentExporter  {
	Collection<SocialLinkItem> getSocialLinks();
}
