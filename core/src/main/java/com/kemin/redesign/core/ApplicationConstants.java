package com.kemin.redesign.core;

public class ApplicationConstants {

    public static final String KEMIN_REDESIGN_REPO_READ = "kemin-redesign-repository-read";
    public static final String KEMIN_REDESIGN_BASE_PATH = "/content/kemin-redesign";
    public static final String KEMIN_REDESIGN_XF_PATH = "/content/experience-fragments/kemin-redesign";
    public static final String LANGUAGE_MASTER = "language-masters";
    public static final String JCR_CONTENT = "jcr:content";
    public static final String PATH_SEPARATOR = "/";
    public static final String SOCIAL_LINKS = "social_links";
    public static final String PAR_CONTENT = "parcontent";
    public static final String GROUP_NAME = "groupName";
    public static final String PRIORITY = "priority";
    public static final String SKIP_REVIEW_MESSAGE = "skipReviewMessage";
    public static final String WORKFLOW_START_COMMENT = "startComment";
    public static final String WORKFLOW_TASK_PRIORITY = "taskPriority";
    public static final String WORKFLOW_TASK_DUE_DATE = "taskDueDate";
    public static final String FIRST_NAME = "./profile/givenName";
    public static final String LAST_NAME = "./profile/familyName";
    public static final String WORKFLOW_TITLE = "workflowTitle";
    public static final String DUE_DATE = "dueDate";
    public static final int    DUE_DATE_HOURS = 48;
    public static final String NOTIFY_EDITORIAL = "notify-editorial";
    public static final String REVIEW_TYPE_EDITORIAL = "Editorial";
    public static final String NOTIFY_LEGAL = "notify-legal";
    public static final String REVIEW_TYPE_LEGAL = "Legal";
    public static final String WORKITEM_COMMENT = "comment";
    public static final String WORKITEM_LEGAL_COMMENT = "Legal Reviewer Comment";
    public static final String WORKITEM_INITATOR_COMMENT = "Initiator Comment";
    public static final String WORKITEM_EDITORIAL_COMMENT = "Editorial Reviewer Comment";
    public static final String WORKITEM_LEGAL_COMMENT_NAME = "legalComment";
    public static final String WORKITEM_INITATOR_COMMENT_NAME = "initorComment";
    public static final String WORKITEM_EDITORIAL_COMMENT_NAME = "editorialComment";
    public static final String KEMIN_REDESIGN_TEMPLATE_ROOT="/conf/kemin-redesign/settings/wcm/templates";
    public static final String KEMIN_REDESIGN_CATEGORY_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/category-page";
    public static final String KEMIN_REDESIGN_SUBCATEGORY_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/sub-category-page";
    public static final String KEMIN_REDESIGN_PRODUCT_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/product-detail-page";
    public static final String KEMIN_REDESIGN_PUBLISH_WORKFLOW="/var/workflow/models/kemin-redesign-publish-workflow";
    public static final String KEMIN_REDESIGN_PRODUCT_CATALOG_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/product-catalog-page";
    public static final String KEMIN_REDESIGN_SEARCH_PAGE_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/search-results-page";
    public static final String KEMIN_REDESIGN_HOME_PAGE_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/home-page";
    public static final String KEMIN_REDESIGN_BLOG_HOME_PAGE_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/blog-home-page";
    public static final String KEMIN_REDESIGN_BLOG_MARKET_PAGE_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/blog-market-page";
    public static final String KEMIN_REDESIGN_BLOG_PAGE_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/blog-page";
    public static final String KEMIN_REDESIGN_SUPPLIER_LOCATOR_CONTENT_FRAGMENTS = "/content/dam/kemin-redesign/content-fragments-for-supplier-locator-map";
    public static final String KEMIN_REDESIGN_COMPANY_TEMPLATE = KEMIN_REDESIGN_TEMPLATE_ROOT+"/company-page";
    public static final String KEMIN_REDESIGN_SUPPLIER_LOCATION_DIR = "/content/dam/kemin-redesign/supplier-locations";
    public static final String KEMIN_REDESIGN_SUPPLIER_LOCATION_FILE = "supplier_locations.json";

    private ApplicationConstants() {}
}
