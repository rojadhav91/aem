package com.kemin.redesign.core.models.header;

import com.adobe.cq.export.json.ComponentExporter;
import com.kemin.redesign.core.models.testimonial.TestimonialItem;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Collection;

@ConsumerType
public interface Feature extends ComponentExporter {
    Collection<FeatureItem> getFeatureItems();
}
