package com.kemin.redesign.core.models.timeline;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface Milestone {
	String getImage();
	String getYear();
	String getPeriod();
	String getDescription();
}
