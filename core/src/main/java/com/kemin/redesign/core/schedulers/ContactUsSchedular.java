package com.aembootcamp.core.schedulers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.nodetype.ConstraintViolationException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.jcr.JsonItemWriter;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.adobe.acs.commons.email.EmailService;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.mailer.MailService;

@Designate(ocd = ContactUsConfig.class)
@Component(immediate = true, service = Runnable.class)
public class ContactUsSchedular implements Runnable {
	private static final Logger LOGGER = Logger.getLogger(ContactUsSchedular.class);
	private static final String CONTENT = "/content/aem-bootcamp/contact-us/";
	private static final String TEMPLATE_PATH = "/etc/notification/email/aem-bootcamp/email.txt";
	private int schedulerId;
	static StringBuffer sb = new StringBuffer();
	static StringBuffer deletedNode = new StringBuffer();
	private static FileWriter file;

	@Reference
	private ResourceResolverFactory factory;
	private ResourceResolver resourceResolver;

	@Reference
	private Scheduler scheduler;

	@Reference(target = "(service.pid=com.day.cq.mailer.DefaultMailService)")
	private MailService defaultMailService;

	@Activate
	protected void activate(ContactUsConfig config) {
		schedulerId = config.schdulerName().hashCode();
	}

	@Modified
	protected void modified(ContactUsConfig config) {
		removeScheduler();
		schedulerId = config.schdulerName().hashCode();
		addScheduler(config);
	}

	@Deactivate
	protected void deactivate(ContactUsConfig config) {
		removeScheduler();
	}

	private void removeScheduler() {
		LOGGER.info("Removing scheduler: {}");
		scheduler.unschedule(String.valueOf(schedulerId));
	}

	private void addScheduler(ContactUsConfig config) {
		if (config.enabled()) {
			ScheduleOptions scheduleOptions = scheduler.EXPR(config.cronExpression());
			scheduleOptions.name(config.schdulerName());
			scheduleOptions.canRunConcurrently(false);
			scheduler.schedule(this, scheduleOptions);
			LOGGER.info("Scheduler added");
		} else {
			LOGGER.info("Scheduler is disabled");
		}
	}

	@Override
	public void run() {
		sb.delete(0, sb.length());
		deletedNode.delete(0, deletedNode.length());
	        Path path1 = Paths.get("C:\\data\\data.json");
	        try {
	        	new FileWriter("C:\\data\\data.json", false).close();    
	        }
	        catch (IOException e) {
	           LOGGER.error(e);
	        }
		fetchNode();
	}
	public void fetchNode() {
		try {
			Resource fetchResource = getResourceContent(CONTENT);
			if (fetchResource != null) {
				Node node = fetchResource.adaptTo(Node.class);
				if (node != null) {
					NodeIterator checkNode = node.getNodes();
					while (checkNode.hasNext()) {
						Node imageNode = checkNode.nextNode();
						if (imageNode != null) {
							NodeIterator childNodes = imageNode.getNodes();
							while (childNodes.hasNext()) {
								Node nestedNode = childNodes.nextNode();
								if (nestedNode != null) {
									sb.append(nodeToJson(nestedNode));	
									sb.append(", ");
								}
							}
						}
					}
				}
			}
			createFile(sb);
			removeNode();
			sendMail("Admin", "test@gmail.com", "BackUp of users", deletedNode);
		} catch (RepositoryException | NullPointerException e) {
			LOGGER.info(e.getMessage(), e);
		}
	}

	private void removeNode() {
		Session session;
		try {
			Resource fetchResource = getResourceContent(CONTENT);
			Node node = fetchResource.adaptTo(Node.class);
			session = resourceResolver.adaptTo(Session.class);
			if (session != null) {
				NodeIterator checkNode = node.getNodes();
				while (checkNode.hasNext()) {
					boolean flag = false;
					Node imageNode = checkNode.nextNode();
					PropertyIterator props = imageNode.getProperties();
					while (props.hasNext()) {
						String property = props.nextProperty().getValue().toString();
						if (property.equals("nt:unstructured")) 
							flag = true;
						deletedNode.append(imageNode.getName());
						deletedNode.append(", ");
					}
					if (flag == true) {
						imageNode.remove();
						session.save();
					}
				}
			}
		} catch (RepositoryException e) {
			LOGGER.error(e);
		}
	}

	public void createFile(StringBuffer data) {
			try {
			File initialFile = new File("C:/data/data.json");
			FileWriter file = new FileWriter(initialFile);
			file.write("["+myJSON+"]");
			InputStream targetStream = new FileInputStream(initialFile);
			String path = "/content/aem-bootcamp/backup/contact-us/";
			String mimetype = null;
			String fileName = "data.json";
			writeToClientLib(fileName, path, targetStream);
			file.close();
			}catch (IOException | LoginException e) {
				LOGGER.error(e);
			}
		
	}

	private void writeToClientLib(String fileName, String path, InputStream targetStream) throws LoginException {
		ResourceResolver resourceResolver = factory.getAdministrativeResourceResolver(null);
		InputStream is = new ByteArrayInputStream(myJSON.getBytes());
		String newFile = path + fileName;
		AssetManager assetmanger = resourceResolver.adaptTo(AssetManager.class);
		if (assetmanger != null) {
			Asset asset = assetmanger.createAsset(newFile, is, "application/json", true);
		}
	}

	String myJSON = "";
	JSONObject newJson=null;
	public JSONObject nodeToJson(Node node) {
		final StringWriter stringWriter = new StringWriter();
		final JsonItemWriter jsonWriter = new JsonItemWriter(null);
		JSONObject jsonObject = null;
		try {
			jsonWriter.dump(node, stringWriter, -1);
			jsonObject = new JSONObject(stringWriter.toString());
		} catch (RepositoryException | JSONException e) {
			LOGGER.error("Could not create JSON", e);
		}
		myJSON = myJSON + jsonObject +",";
		return jsonObject;
	}

	public Resource getResourceContent(String path) {
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put(ResourceResolverFactory.SUBSERVICE, "ContactUsServiceUser");
		Resource resource = null;
		try {
			resourceResolver = factory.getServiceResourceResolver(paraMap);
			if (resourceResolver == null)
				LOGGER.info("Could not obtain a CRX User for the Service:''");
			resource = resourceResolver.getResource(path);
		} catch (LoginException e) {
		}
		return resource;
	}

	private void sendMail(String firstName, String email, String subject, StringBuffer message) {
		Map<String, String> emailParams = new HashMap<String, String>();
		emailParams.put("email", email);
		emailParams.put("FirstName", firstName);
		emailParams.put("subject", subject);
		emailParams.put("Message", message.toString());
		String[] recipients = { email };
		try {
			ResourceResolver resourceResolver = factory.getAdministrativeResourceResolver(null);
			Node nodeTemplate = resourceResolver.getResource(TEMPLATE_PATH).adaptTo(Node.class);
			if (nodeTemplate != null) {
				final MailTemplate mailTemplate = MailTemplate.create(TEMPLATE_PATH, nodeTemplate.getSession());
				HtmlEmail email2 = mailTemplate.getEmail(StrLookup.mapLookup(emailParams), HtmlEmail.class);
				email2.setSubject("Backup notification");
				email2.addTo(email);
				email2.setCharset("UTF-8");
				email2.setMsg(deletedNode.toString());
				defaultMailService.send(email2);
				LOGGER.info("Email sent successfully to the recipients");
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
}