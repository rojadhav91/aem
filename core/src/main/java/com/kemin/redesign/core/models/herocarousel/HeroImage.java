package com.kemin.redesign.core.models.herocarousel;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface HeroImage {
	String getHeroImage();
	String getTitle();
	String getDescription();
	String getLinkLabel();
	String getLinkUrl();
	String getTransitionLabel();
	String getTransitionIcon();
}
