package com.aembootcamp.core.schedulers;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "AEM BootCamp Contact Us Service", description = "AEM BootCamp Contact Us Service")
public @interface ContactUsConfig {

	@AttributeDefinition(
			name = "Scheduler name", 
			description = "Name of the scheduler", 
			type = AttributeType.STRING)
	        public String schdulerName() default "Custom Sling Scheduler Configuration";
	
	@AttributeDefinition(
			name = "Enabled", 
			description = "True, if scheduler service is enabled", 
			type = AttributeType.BOOLEAN)
	        public boolean enabled() default false;
	
	@AttributeDefinition(
			name = "Cron Expression", 
			description = "Cron expression used by the scheduler", 
			type = AttributeType.STRING)
	public String cronExpression() default "0/30 * * * * ?";
}