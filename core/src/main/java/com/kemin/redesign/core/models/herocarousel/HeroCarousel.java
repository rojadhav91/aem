package com.kemin.redesign.core.models.herocarousel;

import java.util.Collection;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface HeroCarousel extends ComponentExporter  {
	int getTimeLapse();
	Collection<HeroImage> getHeroImages();
	String getId();
}
