package com.kemin.redesign.core.services;

import java.util.List;
import java.util.Map;

import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.models.blog.impl.BlogPagesBean;

public interface CatalogPageSearchService {
    public List<BlogPagesBean> getBlogPages(Page page,String defaultImagePath);
    public Map<String,String> prepareQueryMap(String path, String type, String propertyName, String propertyValue);
}
