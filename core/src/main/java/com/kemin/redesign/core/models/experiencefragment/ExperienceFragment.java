package com.kemin.redesign.core.models.experiencefragment;

import org.osgi.annotation.versioning.ConsumerType;
import com.adobe.cq.export.json.ComponentExporter;
/**
 * Defines the {@code ExperienceFragment} Sling Model used for the {@code /apps/kemin-redesign/components/experiencefragment} component.
 */
@ConsumerType
public interface ExperienceFragment extends ComponentExporter{
	/**
	 * Returns path of the fragment
	 * @return fragmentPath
	 */
	String getFragmentPath();
	String getChatPath();

	boolean isTemplateView();
}