package com.aembootcamp.core.models;

import javax.mail.Quota.Resource;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
resourceType = Contact.RESOURCETYPE )
public class Contact {
	
	protected static final String RESOURCETYPE="aem-bootcamp/components/general/contact-form";
	
	@ValueMapValue(name = "companyTitle")
	private String companyTitle;
	
	@ValueMapValue(name = "companyAddress")
	private String companyAddress;
	
	@ValueMapValue(name ="companyPhone")
	private String companyPhone;
	
	@ValueMapValue(name = "companyEmail")
	private String companyEmail;
	
	@ValueMapValue(name = "firstName")
	@Default(values="Your First Name")
	private String firstName;
	
	@ValueMapValue(name ="lastName")
	@Default(values ="Your Last Name")
	private String lastName;
	
	@ValueMapValue(name ="email")
	@Default(values="Your Email")
	private String email;
	
	@ValueMapValue(name = "subject")
	@Default(values ="Subject")
	private String subject;
	
	@ValueMapValue(name ="message")
	@Default(values = "Please write something for us")
	private String message;
	
	@PostConstruct
	public void initContactForm() {
		
	}

	public String getCompanyTitle() {
		return companyTitle;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}





	

}
