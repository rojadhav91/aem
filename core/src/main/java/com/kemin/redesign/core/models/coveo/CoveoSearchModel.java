/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.kemin.redesign.core.models.coveo;

import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.services.CoveoConfigService;
import com.kemin.redesign.core.utils.CommonUtils;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.regex.*;

import static org.apache.sling.api.resource.ResourceResolver.PROPERTY_RESOURCE_TYPE;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })
public class CoveoSearchModel {
	public static final Logger LOGGER = LoggerFactory.getLogger(CoveoSearchModel.class);
	
    @ValueMapValue(name=PROPERTY_RESOURCE_TYPE, injectionStrategy=InjectionStrategy.OPTIONAL)
    @Default(values="No resourceType")
    protected String resourceType;
    
    @ScriptVariable
	private Page currentPage;
    
    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private CoveoConfigService configuration;

    private String language;

    private String locale;
    
    private String region;

    @PostConstruct
    protected void init() {
    	String currentPagePath = currentPage.getPath();
        LOGGER.debug("currentPagePath:"+currentPagePath);
        try {
        	Pattern p = Pattern.compile("/(\\w{2})/(\\w{2}(?:-\\w{2}){0,1})/");
			Matcher m = p.matcher(currentPagePath);
			if (m.find()) {
				region = m.group(1);
				locale = m.group(2);
				
				if(Pattern.compile("-").matcher(locale).find()) {
					language = locale.split("-")[0];
				} else {
					language = locale;
				}
			}
			
        } catch(Exception e) {
        }
        
    }
    
    public String getRegion() {
    	return region;
    }

    public String getLocale(){
        return locale;
    }

    public String getLanguage() {
        return language;
    }

    public String getSearchHub() {
        return this.configuration.getSearchHub();
    }

    public String getThumbnailURL() {
        if(currentPage != null) {
            return CommonUtils.thumbnail(currentPage);
        } else {
            return "";
        }
    }

    public String getNavigationIconURL(){
        if (currentPage != null) {

            String url= CommonUtils.getNavigationIconByContentFragements(currentPage,resourceResolver);
            if(url==null||"".equals(url)){
                return getThumbnailURL();
            }else{
                return url;
            }
        } else {
            return "";
        }
    }
}
