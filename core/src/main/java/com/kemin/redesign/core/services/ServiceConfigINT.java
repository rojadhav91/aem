package com.aembootcamp.core.services;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "AEM Bootcamp OSGi Service",
description = "AEM Bootcamp OSGi Service")
public @ interface ServiceConfigINT {

@AttributeDefinition(name = "Social share widget property",
description = "Social share widget property",
type = AttributeType.STRING)
String getConfigValue();

@AttributeDefinition(name = "Users",
description = "List of users",
type = AttributeType.STRING)
String[] getUsers();
}