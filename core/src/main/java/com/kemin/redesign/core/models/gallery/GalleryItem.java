package com.kemin.redesign.core.models.gallery;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface GalleryItem {
	String getCaption();
	String getImage();
}
