package com.kemin.redesign.core.models.header;

import com.adobe.cq.export.json.ComponentExporter;
import com.kemin.redesign.core.models.image.Image;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Collection;

/** Consumer for Header Component. */
@ConsumerType
public interface Header extends ComponentExporter {
  Collection<HeaderItem> getHeaderLinks();

  Image getLogo();

  boolean isDisplayCountryLanguageSelector();

  //Collection<LanguageSelectorGroup> getCountryLanguages();

  String getHelpPageUrl();
  
  Collection<CategoryItem> getProductCategoryItem();
}
