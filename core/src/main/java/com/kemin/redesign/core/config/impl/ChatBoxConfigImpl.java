package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.ChatBoxConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Component(service = ChatBoxConfig.class, immediate = true)
@Designate(ocd = ChatBoxConfigImpl.Config.class, factory = false)
@ServiceDescription("A service used to get Chatbox Id and hostname")
@ServiceVendor("Dish DOP")
/**
 * Chatbox confoguration implementation class
 */
public class ChatBoxConfigImpl implements ChatBoxConfig {

    /**
     * Get the chatbox appid
     */
    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String appId;

    /**
     * Get the chatbox apphostname
     */
    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String aPIHostName;

    /**
     * Set appid and hostname using config
     * @param config
     */
    @Activate
    public void activate(final Config config) {
        appId = config.appId();
        aPIHostName = config.aPIHostName();
    }

    @ObjectClassDefinition(
        name = "Dish DOP Chatbox config",
        description = "The configuration for the Chatbox appId and hostname.")
    public @interface Config {

        @AttributeDefinition(name = "App Id", description = "Chatbox appid")
        String appId() default
            "";
        @AttributeDefinition(name = "API Hostname", description = "Chatbox api hostname")
        String aPIHostName() default
            "";
    }

}
