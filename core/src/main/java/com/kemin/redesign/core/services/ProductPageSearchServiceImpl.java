package com.kemin.redesign.core.services;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.google.common.collect.Sets;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.product.impl.ProductPageBean;
import com.kemin.redesign.core.models.product.impl.TierTagList;
import com.kemin.redesign.core.utils.CommonUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.*;

@Component(service = ProductPageSearchService.class, immediate = true)
public class ProductPageSearchServiceImpl implements ProductPageSearchService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    /**
     * Injecting the QueryBuilder dependency
     */
    @Reference
    private QueryBuilder queryBuilder;
    /**
     * Session object
     */
    Map<String, Set<String>> tagList;
    private Session session;
    Map<String, Set<String>> allTagsToPageMap;
    JSONArray productItemListJSON;
    JSONObject allTagsToPageJSON;
    TierTagList tierTagList;
    List<TierTagList> listTierTagList;

    private List<ProductPageBean> pageInfo = new ArrayList<>();
    private ResourceResolver resourceResolver = null;
    private ProductPageBean productPageBean;
    private Map<String, Set<String>> tier1TagList = new HashMap<>();
    private List<String> tier2TagList;
    private static int productNumber = 1;

    protected void activate(ProductPageSearchService config) {
        LOGGER.debug("####ProductPageSearchService activate method");
    }

    @Override
    public String getAllTagsToPageJSON() {
        return null != allTagsToPageJSON ? allTagsToPageJSON.toString() : null;
    }

    @Override
    public List<TierTagList> getTierTagList() {
        return listTierTagList;
    }
    public void getProductPages(String path, String[] prodAppCat) throws JSONException {
        if (null != path && path != "") {
            allTagsToPageMap = new HashMap<>();
            tagList = new HashMap<>();
            allTagsToPageJSON = new JSONObject();
            productItemListJSON = new JSONArray();
            try {
                resourceResolver = CommonUtils.getResourceResolver(resourceResolverFactory, ApplicationConstants.KEMIN_REDESIGN_REPO_READ);
                session = resourceResolver.adaptTo(Session.class);
                Query query = queryBuilder.createQuery(PredicateGroup.create(buildSearchPredicate(path, prodAppCat)), session);
                SearchResult result = query.getResult();
                //LOGGER.info("Got the result...");
                List<Hit> hits = result.getHits();
                int productNumber = 1;
                //LOGGER.info("now reading the results. ");
                for (Hit hit : hits) {
                    Page page = hit.getResource().adaptTo(Page.class);
                    productPageBean = new ProductPageBean();
                    //LOGGER.info("\n Page {} ", page.getPath());
                    ValueMap properties = hit.getProperties();
                    String[] prodAppCategories = (String[]) properties.get("productApplication");
                    tier1TagList = new HashMap<>();
                    Boolean flag = false;
                    if (null != prodAppCategories && prodAppCategories.length > 0) {
                        try {
                            for (String cat : prodAppCategories) {
                                String[] tierTags = cat.split((":"))[1].split(("/"));
                                String tier1Tag = tierTags.length > 1 ? tierTags[1] : "";
                                String tier2Tag = tierTags.length > 2 ? tierTags[2] : null;
                                tier2TagList = new ArrayList<>();
                                if (prodAppCat!=null && (Arrays.asList(prodAppCat).contains(cat) || checkTier1Existance(prodAppCat, cat))) {
                                    flag = true;
                                    if (!tier1TagList.isEmpty() && tier1TagList.containsKey(tier1Tag) && null != tier1TagList.get(tier1Tag)) {
                                        if (tier1TagList.get(tier1Tag).contains(null))
                                            tier1TagList.get(tier1Tag).remove(null);
                                        tier2TagList.addAll(tier1TagList.get(tier1Tag));
                                    }
                                    tier2TagList.add(tier2Tag);
                                    tier1TagList.put(tier1Tag, tier2TagList.size() > 0 ? Sets.newHashSet(tier2TagList) : null);
                                    if (tagList.containsKey(tier1Tag) && null != tagList.get(tier1Tag)) {
                                        if (tagList.get(tier1Tag).contains(null))
                                            tagList.get(tier1Tag).remove(null);
                                        tier2TagList.addAll(tagList.get(tier1Tag));
                                    }
                                    tagList.put(tier1Tag, tier2TagList.size() > 0 ? Sets.newHashSet(tier2TagList) : null);
                                    break;
                                } else if (prodAppCat == null) {
                                    if (!tier1TagList.isEmpty() && tier1TagList.containsKey(tier1Tag) && null != tier1TagList.get(tier1Tag)) {
                                        if (tier1TagList.get(tier1Tag).contains(null))
                                            tier1TagList.get(tier1Tag).remove(null);
                                        tier2TagList.addAll(tier1TagList.get(tier1Tag));
                                    }
                                    tier2TagList.add(tier2Tag);
                                    tier1TagList.put(tier1Tag, tier2TagList.size() > 0 ? Sets.newHashSet(tier2TagList) : null);
                                    if (tagList.containsKey(tier1Tag) && null != tagList.get(tier1Tag)) {
                                        if (tagList.get(tier1Tag).contains(null))
                                            tagList.get(tier1Tag).remove(null);
                                        tier2TagList.addAll(tagList.get(tier1Tag));
                                    }
                                    tagList.put(tier1Tag, tier2TagList.size() > 0 ? Sets.newHashSet(tier2TagList) : null);
                                }
                            }
                            if((prodAppCat!=null && flag == true) || prodAppCat==null) {
                                productPageBean.setProductURL(page.getPath());
                                productPageBean.setProductDescription((String) properties.get("productDescription"));
                                productPageBean.setProductCatalogName((String) properties.get("prodCatalogName"));
                                productPageBean.setProductLogoImage((String) properties.get("prodImage"));
                                productPageBean.setTags(createPageTags(tier1TagList));
                                productPageBean.setTagClass(String.join(" ", productPageBean.getTags()));
                                productPageBean.setProductNumber(productNumber);
                                productNumber++;
                                createAllTagsToPageMap(productPageBean.getTags(), new JSONObject(productPageBean).toString());
                                //LOGGER.info("Tags for given page are {}", new JSONObject(tier1TagList).toString() + "\n and list is" + new JSONObject(pageTags));
                                pageInfo.add(productPageBean);
                            }

                        } catch (Exception rex) {
                            LOGGER.debug(String.format("could not retrieve node properties: %1s", rex));
                        }
                    }
                }
            } catch (Exception le) {
                LOGGER.info("Login Exception occured : " + le.toString());
            }
            createAllTagsToPageJSON();
            createTierTagList();
        }
    }
    @Override
    public String getProductItemListJSON(String path, String[] prodAppCat) throws JSONException {
        getProductPages(path, prodAppCat);
        return null != productItemListJSON ? productItemListJSON.toString() : null;
    }

    public boolean checkTier1Existance(String[] prodAppCat, String prodAppCatItem) {
        for (String content : prodAppCat) {
            if (StringUtils.contains(prodAppCatItem, content)) {
                return true;
            }
        }
        return false;
    }

    private Set<String> createPageTags(Map<String, Set<String>> pageTagMap) {
        Set<String> keySet = pageTagMap.keySet();
        Set<String> pageTags = new HashSet<>();
        ArrayList<String> listOfKeys = new ArrayList<String>(keySet);
        for (String key : listOfKeys) {
            if (key != null)
                pageTags.add(key);
        }
        Collection<Set<String>> values = pageTagMap.values();
        ArrayList<Set<String>> listOfValues = new ArrayList<Set<String>>(values);
        for (Set<String> tagSet : listOfValues) {
            for (String val : tagSet) {
                if (val != null)
                    pageTags.add(val);
            }
        }
        return pageTags;
    }

    private void createAllTagsToPageMap(Set<String> pageTagMap, String productJSON) {
        Set<String> pageList;
        productItemListJSON.put(productJSON);
        for (String tag : pageTagMap) {
            pageList = new HashSet<>();
            if (null != tag && StringUtils.EMPTY != tag && allTagsToPageMap.containsKey(tag))
                pageList.addAll(allTagsToPageMap.get(tag));
            if (null != tag && StringUtils.EMPTY != tag) {
                pageList.add(productJSON);
                allTagsToPageMap.put(tag, pageList);
            }
        }
    }

    private void createAllTagsToPageJSON() throws JSONException {
        for (Map.Entry<String, Set<String>> item : allTagsToPageMap.entrySet()) {
            allTagsToPageJSON.put(item.getKey(), item.getValue());
        }
    }

    private void createTierTagList() {
        listTierTagList = new ArrayList<>();
        Map<String, Integer> tagDetailMap;
        for (Map.Entry<String, Set<String>> tagItem : tagList.entrySet()) {
            tierTagList = new TierTagList();
            tierTagList.setTier1Tag(tagItem.getKey());
            tierTagList.setTier1TagCount(allTagsToPageMap.get(tagItem.getKey()).size());
            tagDetailMap = new HashMap<>();
            Set<String> tiersTagItems = tagItem.getValue();
            for (String tier2TagItem : tiersTagItems) {
                if (null != tier2TagItem) {
                    tagDetailMap.put(tier2TagItem, allTagsToPageMap.get(tier2TagItem).size());
                }
            }
            tierTagList.setTier2TagDetails(tagDetailMap);
            listTierTagList.add(tierTagList);
        }
    }

    public Map<String, String> buildSearchPredicate(String path, String[] prodAppCat) {
        final Map<String, String> predicate = new HashMap<>();
        predicate.put("type", "cq:Page");
        predicate.put("path", path);
        predicate.put("property", "jcr:content/cq:template");
        predicate.put("property.value", ApplicationConstants.KEMIN_REDESIGN_PRODUCT_TEMPLATE);
        predicate.put("p.limit", "-1");
       /* if (null!=prodAppCat && prodAppCat.length > 0) {
            int i = 1;
            for (String catItem: prodAppCat) {
                predicate.put("1_property", "jcr:content/productApplication");
                predicate.put("1_property." + i + "_value", catItem);
                i++;
            }
        }*/
        return predicate;
    }
}
