package com.kemin.redesign.core.models.product;

import com.adobe.cq.export.json.ComponentExporter;
import com.kemin.redesign.core.models.product.impl.TierTagList;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface ProductCatalog extends ComponentExporter {
    String getProductItemListJSON();
    String getAllTagsToPageJSON();
    List<TierTagList> getTierTagList();
}
