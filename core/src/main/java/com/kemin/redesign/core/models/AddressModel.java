package com.dish.dopweb.core.models;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

/**
 * THis class is to convert address fileds in to JSON
 */
public class AddressModel {

    @Getter
    @Setter
    @SerializedName("delivery_line_1")
    private String address1="";

    @Getter
    @Setter
    @SerializedName("last_line")
    private String address2="";

    @Getter
    @Setter
    @SerializedName("components")
    private Components components;

}
