package com.kemin.redesign.core.services;

import com.adobe.granite.workflow.WorkflowException;
import com.kemin.redesign.core.ApplicationConstants;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

public class PopulateComment implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(PopulateComment.class);
    private String comments;
    private ResourceResolverFactory resourceResolverFactory;
    private String nodePath;
    
    public PopulateComment(ResourceResolverFactory resourceResolverFactory, String comments, String nodePath) {
        this.comments = comments;
        this.resourceResolverFactory = resourceResolverFactory;
        this.nodePath = nodePath;
    }
    
    @Override
    public void run() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    populateComment();
                } catch (WorkflowException | RepositoryException | LoginException e) {
                    LOG.error(e.getMessage());
                }
                
            }
        }, 5000);
    }
    
    private void populateComment() throws WorkflowException, RepositoryException, LoginException {
        Map<String, Object> resourceResolverParams = new ConcurrentHashMap<>();
        resourceResolverParams.put(ResourceResolverFactory.SUBSERVICE, "keminuser");
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(resourceResolverParams);
        Session session = resourceResolver.adaptTo(Session.class);
        try {
            Node node = JcrUtils.getNodeIfExists(nodePath + "/history", session);
            NodeIterator nodeIterator = node.getNodes();
            while (nodeIterator.hasNext()) {
                Node child = nodeIterator.nextNode();
                Node workItemNode = JcrUtils.getNodeIfExists(child, "workItem");
                if (workItemNode.hasProperty("nodeId")) {
                    String nodeID = workItemNode.getProperty("nodeId").getString();
                    if (nodeID.equals("node33")) {
                        Node meta = JcrUtils.getNodeIfExists(workItemNode, "metaData");
                        meta.setProperty(ApplicationConstants.WORKITEM_COMMENT, comments);
                        session.save();
                    }
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Get node error", e);
        }
        
    }
}
