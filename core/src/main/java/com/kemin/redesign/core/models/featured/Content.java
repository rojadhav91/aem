package com.kemin.redesign.core.models.featured;

import com.adobe.cq.export.json.ComponentExporter;
import com.kemin.redesign.core.beans.PageBean;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface Content extends ComponentExporter {
    List<PageBean> getPageItemList();
    String getMoreLink();
}
