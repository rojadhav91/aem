package com.dish.dopweb.core.models;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Optional;

/**
 * Content Fragment Markets Model.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContentFragmentMarketsModel {
    
    @Inject @Self
    private Resource resource;
    
    private Optional<ContentFragment> contentFragment;
    
    /**
     * Init model method.
     */
    @PostConstruct
    public void init() {
        contentFragment = Optional.ofNullable(resource.adaptTo(ContentFragment.class));
    }
    
    /**
     * Get Market Status.
     *
     * @return market status
     */
    public String getStatus() {
        return contentFragment.map(cf -> cf.getElement("status")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
    
    /**
     * Get Market Id.
     *
     * @return market id
     */
    public String getId() {
        return contentFragment.map(cf -> cf.getElement("marketId")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
    
    /**
     * Get Market Name.
     *
     * @return market name
     */
    public String getName() {
        return contentFragment.map(cf -> cf.getElement("marketName")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
    
    /**
     * Get Market Start Date.
     *
     * @return market start date
     */
    public String getStartDate() {
        return contentFragment.map(cf -> cf.getElement("startDate")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
    
    /**
     * Get Market End Date.
     *
     * @return market end date
     */
    public String getEndDate() {
        return contentFragment.map(cf -> cf.getElement("endDate")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
    
    /**
     * Get Market Zipcodes Array.
     *
     * @return market zipcodes array
     */
    public String getZipcodes() {
        return contentFragment.map(cf -> cf.getElement("zipcodes")).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
}
