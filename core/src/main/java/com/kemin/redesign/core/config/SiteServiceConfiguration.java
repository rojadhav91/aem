package com.kemin.redesign.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Kemin Redesign Site Configuration", description = "For general site configuration")
public @interface SiteServiceConfiguration {

	@AttributeDefinition(name = "Paths hidden", description = "Paths hidden from breadcrumb")
	public String[] getBreadcrumbHiddenPaths() default {"products","markets","services"};

}
