package com.kemin.redesign.core.models.table.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.table.Table;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Table.class, ComponentExporter.class},
        resourceType = TableImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class TableImpl implements Table{
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/table";

    @ValueMapValue @Getter private String tableData;
    @ValueMapValue @Getter private String title;
    @ValueMapValue @Getter private String description;

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}

