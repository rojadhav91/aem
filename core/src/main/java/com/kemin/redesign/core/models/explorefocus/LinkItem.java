package com.kemin.redesign.core.models.explorefocus;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface LinkItem {
	String getUrl();

	String getTitle();
}
