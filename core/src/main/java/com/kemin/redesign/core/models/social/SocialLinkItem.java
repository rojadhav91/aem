package com.kemin.redesign.core.models.social;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface SocialLinkItem {
	String getLink();

	String getPlatform();
}
