package com.kemin.redesign.core.models.navigation.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.jcr.query.Query;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.navigation.Navigation;
import com.kemin.redesign.core.utils.CommonUtils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = {Navigation.class,
        ComponentExporter.class}, resourceType = NavigationImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
public class NavigationImpl implements Navigation {

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/navigation";

    private static final Logger LOGGER = LoggerFactory.getLogger(NavigationImpl.class);

    @SlingObject
    private ResourceResolver resourceResolver;
    List<Page> headerNavigationItemList;
    Page parent;

    @ScriptVariable
    private Page currentPage;

    @ChildResource(injectionStrategy = InjectionStrategy.DEFAULT)
    private Resource navigations;

    @ValueMapValue
    @Getter
    private String rootPage;

    @PostConstruct
    protected void init() {
        parent = getParentPage(currentPage);
        if (parent != null) {
            headerNavigationItemList = CommonUtils.prepareNavigationItemListPage(parent);
        } else {
            headerNavigationItemList = Collections.emptyList();
        }

    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    @Override
    public List<Page> getNavigationItemList() {
        return headerNavigationItemList;
    }

    @Override
    public String getThumbnailURL() {
        if (currentPage != null) {
            return CommonUtils.thumbnail(parent);
        } else {
            return "";
        }
    }

    @Override
    public String getNavigationIconUrl() {
        if (currentPage != null) {

            String url= CommonUtils.getNavigationIconByContentFragements(currentPage,resourceResolver);
            if(url==null||"".equals(url)){
                return getThumbnailURL();
            }else{
                return url;
            }
        } else {
            return "";
        }
    }




    public Page getParentPage(Page page) {
        Page parent = null;
        int depth = currentPage.getDepth();
        for (int count = 0; count < depth - 3; count++) {
            if (page.getParent(count) != null) {
                Template tmp = page.getParent(count).getTemplate();
                if (tmp == null) {
                    LOGGER.warn("no template found for page: " + page.getPath());
                } else if (tmp.getPath().equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_CATEGORY_TEMPLATE)) {
                    parent = page.getParent(count);
                    break;
                }
            }

        }
        return parent;
    }

    @Override
    public Page getCurrentPage() {
        return currentPage;
    }

    @Override
    public String getRootPage() {
        if ((parent != null) && (parent.getNavigationTitle() != null)) {
            return parent.getNavigationTitle();
        } else if (parent != null) {
            return parent.getTitle();
        } else {
            return "";
        }
    }



}