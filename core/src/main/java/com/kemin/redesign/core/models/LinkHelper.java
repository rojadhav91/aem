package com.dish.dopweb.core.models;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.inject.Inject;
import java.util.Optional;

import static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT;
import static com.day.cq.wcm.api.NameConstants.PN_REDIRECT_TARGET;

/** LinkHelper use to manage adding .html or not for pathfield. */
@Model(
    adaptables = SlingHttpServletRequest.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkHelper {

  public static final String BOOST_EXTERNALIZER_KEY = "boost";
  public static final String GENESIS_EXTERNALIZER_KEY = "genesis";
  protected static final String HTML_EXT = ".html";
  protected static final String CONTENT_ROOT = "/content";
  protected static final String GENESIS_ROOT = CONTENT_ROOT + "/genesis";
  protected static final String DAM_ROOT = "/content/dam";
  protected static final String SLASH = "/";
  @ScriptVariable public Page currentPage;

  @Inject private String link;
  @SlingObject private ResourceResolver resourceResolver;
  @OSGiService private Externalizer externalizer;
  @SlingObject private Resource resource;

  /**
   * Check whether internal/external link and add html accordingly
   *
   * @return formatted link
   */
  public String getFormattedLink(final String path) {
    if (StringUtils.isNotBlank(path)) {
      link = path;
    }
    return getFormattedLink();
  }

  /**
   * Check whether it is a internal/external link and add html accordingly
   *
   * @return formatted link
   */
  public String getFormattedLink() {
    String formattedLink = link;
    if (StringUtils.isNotEmpty(link)) {
      // link start with content, but not /content/dam and has the HTML extension
      if (link.startsWith(CONTENT_ROOT) && !link.startsWith(DAM_ROOT) && !link.contains(HTML_EXT)) {
        if (link.indexOf('?') >= 0) {
          // link has question mark, add html extension before it
          formattedLink = insertHtmlBefore(link, '?');
        } else if (link.indexOf('#') >= 0) {
          // link has hash, add html extension before it
          formattedLink = insertHtmlBefore(link, '#');
        } else {
          formattedLink = link + HTML_EXT;
        }
      } else {
        formattedLink = link;
      }
    }
    return formattedLink;
  }

  private String insertHtmlBefore(final String url, final char beforeChar) {
    final String formattedUrl;
    final int charIndex = url.indexOf(beforeChar);
    if (charIndex >= 0) {
      formattedUrl = url.substring(0, charIndex) + HTML_EXT + url.substring(charIndex);
    } else {
      formattedUrl = url;
    }

    return formattedUrl;
  }

  /**
   * Returns redirect URL if present on the page otherwise the original link.
   *
   * @return page URL
   */
  public String getRedirectURL() {
    return Optional.of(link)
        .map(path -> path.contains(HTML_EXT) ? path.substring(0, link.indexOf(HTML_EXT)) : path)
        .map(path -> resourceResolver.getResource(path))
        .map(pageResource -> pageResource.getChild(JCR_CONTENT))
        .map(Resource::getValueMap)
        .map(valueMap -> valueMap.get(PN_REDIRECT_TARGET, String.class))
        .map(this::getFormattedLink)
        .orElse(getFormattedLink());
  }

  /**
   * Return the actual name of the page.
   *
   * @return actualPageName
   */
  public String getActualPageName() {
    return Optional.of(link)
        .filter(StringUtils::isNotBlank)
        .map(path -> path.endsWith(HTML_EXT) ? StringUtils.substringBefore(path, HTML_EXT) : path)
        .map(path -> StringUtils.substringAfterLast(path, SLASH))
        .orElse("");
  }

  private Page getCurrentPage() {
    return Optional.ofNullable(currentPage)
        .orElseGet(() -> pageManager().getContainingPage(resource));
  }

  private PageManager pageManager() {
    return resource.getResourceResolver().adaptTo(PageManager.class);
  }

  /**
   * Get CloudFront Canonical URL of current page.
   *
   * @return canonicalUrl
   */
  public String getCanonicalUrl() {
    final String pagePath = getCurrentPage().getPath();
    final String externalizerKey =
        pagePath.startsWith(GENESIS_ROOT) ? GENESIS_EXTERNALIZER_KEY : BOOST_EXTERNALIZER_KEY;
    return externalizer.externalLink(resourceResolver, externalizerKey, getFormattedLink(pagePath));
  }

  /**
   * Get the mapped path of the resource.
   *
   * @return mapped resource path
   */
  public String getMappedResourcePath() {
    return resourceResolver.map(resource.getPath());
  }
}
