package com.kemin.redesign.core.beans;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coordinate {
    private double lng;
    private double lat;
}
