package com.dish.dopweb.core.configs;
/**
 * Responsys config
 */
public interface ResponsysConfig {

    /**
     * Responsys endpoint
     * @return endoint
     */
    String getResponsysEndpoint();
}
