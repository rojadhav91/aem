package com.kemin.redesign.core.models.header;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface FeatureItem  {

    String getFeatureLink();

    String getFeatureTitle();

}
