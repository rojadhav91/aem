package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.SmartyStreetConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration Service Implementation
 */
@Component(service = SmartyStreetConfig.class, immediate = true)
@Designate(ocd = SmartyStreetConfigImpl.SmartyStreetConfig.class, factory = false)
@ServiceDescription("A service used to configure Smartystreet API")
@ServiceVendor("Dish DOP")
public class SmartyStreetConfigImpl implements SmartyStreetConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String apiKey;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String smartyStreetEndpoint;

    /**
     * Set BriteVerify configuration
     * @param config
     */
    @Activate
    public void activate(final SmartyStreetConfigImpl.SmartyStreetConfig config) {

        apiKey = config.apiKey();
        smartyStreetEndpoint = config.smartyStreetEndpoint();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Omnisend Configuration",
            description = "The configuration for storing data  entered by customer.")
    public @interface SmartyStreetConfig {

        @AttributeDefinition(name = "SmartyStreet Key", description = "Api Key for SmartyStreet")
        String apiKey() default  "33843130097947213";

        @AttributeDefinition(name = "SmartyStreet Endpoint", description = "SmartyStreet Api Endpoint")
        String smartyStreetEndpoint() default  "https://us-street.api.smartystreets.com/street-address";
    }
}
