package com.kemin.redesign.core.models.timeline.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.timeline.Milestone;
import com.kemin.redesign.core.models.timeline.Timeline;

import lombok.Getter;
/** Kemin ExploreByFocusAnchor Sling Model Implementation Class. */
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {Timeline.class, ComponentExporter.class},
	    resourceType = TimelineImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class TimelineImpl implements Timeline {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/timeline";

	@ChildResource @Getter private Collection<Milestone> milestones;
	@ValueMapValue @Getter private String heading;
	
    @PostConstruct
    protected void init(){
    	if(milestones==null)
    		milestones = new ArrayList<Milestone>();
    }
    
	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
