package com.dish.dopweb.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.policies.ContentPolicy;
import com.day.cq.wcm.api.policies.ContentPolicyManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.jcr.Session;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Container Padding Model.
 */
@SuppressWarnings({"PMD.AvoidStringBufferField", "PMD.AvoidCatchingGenericException"})
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContainerPaddingModel {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainerPaddingModel.class);

    @SlingObject
    private SlingHttpServletRequest request;

    @ValueMapValue
    @Named("cq:styleIds")
    private String[] styleIds;

    @OSGiService
    private transient QueryBuilder queryBuilder;

    private String styleDesktopClasses = StringUtils.EMPTY;
    private String styleTabletClasses = StringUtils.EMPTY;
    private String styleMobileClasses = StringUtils.EMPTY;

    final private transient StringBuilder dektopBuilder = new StringBuilder("");
    final private transient StringBuilder tabletBuilder = new StringBuilder("");
    final private transient StringBuilder mobileBuilder = new StringBuilder("");

    private List<String> styles = new ArrayList<>();

    /**
     * init method to create padding string
     */
    @PostConstruct
    public void init() {
        try {
            final ContentPolicy contentPolicy = paddingPolicyInfo(request);
            if (StringUtils.isNotBlank(contentPolicy.getPath()) && styleIds != null) {
                styles = Arrays.asList(styleIds);
                final Map<String, Object> map = new ConcurrentHashMap<>();
                map.put("path", contentPolicy.getPath());
                map.put("type", "nt:unstructured");
                map.put("1_group.p.or", "true");
                styles.forEach(s -> {
                    final int index = styles.indexOf(s);
                    map.put("1_group." + index + "_property", "cq:styleId");
                    map.put("1_group." + index + "_property.value", s);
                });
                final Query query1 = queryBuilder.createQuery(PredicateGroup.create(map), request.getResourceResolver().adaptTo(Session.class));
                final SearchResult result1 = query1.getResult();
                final Iterator<Resource> resources1 = result1.getResources();
                resources1.forEachRemaining(resource -> {
                    final String styleGroupLabel = Optional.ofNullable(resource)
                            .map(Resource::getParent)
                            .map(Resource::getParent)
                            .map(Resource::getValueMap)
                            .map(valueMap -> valueMap.get("cq:styleGroupLabel", String.class))
                            .orElse("");
                    setStyleBuilder(styleGroupLabel, resource);
                });
                setStyleClasses();
            }
        }catch (Exception e){
            LOGGER.error("Unable to get policy classes :: {0} ", e);
        }
    }

    private void setStyleClasses() {
        styleDesktopClasses = dektopBuilder.toString();
        styleTabletClasses = tabletBuilder.toString();
        styleMobileClasses = mobileBuilder.toString();
        if (StringUtils.isNotBlank(styleDesktopClasses) && styleDesktopClasses.endsWith(",")) {
            styleDesktopClasses = styleDesktopClasses.substring(0, styleDesktopClasses.lastIndexOf(','));
        }
        if (StringUtils.isNotBlank(styleTabletClasses) && styleTabletClasses.endsWith(",")) {
            styleTabletClasses = styleTabletClasses.substring(0, styleTabletClasses.lastIndexOf(','));
        }
        if (StringUtils.isNotBlank(styleMobileClasses) && styleMobileClasses.endsWith(",")) {
            styleMobileClasses = styleMobileClasses.substring(0, styleMobileClasses.lastIndexOf(','));
        }
    }

    private void setStyleBuilder(final String styleGroupLabel, final Resource resource) {
        if ("Padding Desktop".equals(styleGroupLabel)) {
            final String styleClass = resource.getValueMap().get("cq:styleClasses", String.class);
            dektopBuilder.append(styleClass).append(',');
        } else if ("Padding Tablet".equals(styleGroupLabel)) {
            final String styleClass = resource.getValueMap().get("cq:styleClasses", String.class);
            tabletBuilder.append(styleClass).append(',');
        } else if ("Padding Mobile".equals(styleGroupLabel)) {
            final String styleClass = resource.getValueMap().get("cq:styleClasses", String.class);
            mobileBuilder.append(styleClass).append(',');
        }
    }

    /**
     * Desktop style getter
     *
     * @return
     */
    public String getStyleDesktopClasses() {
        return styleDesktopClasses;
    }

    /**
     * Tablet style getter
     *
     * @return
     */
    public String getStyleTabletClasses() {
        return styleTabletClasses;
    }

    /**
     * Mobile style getter
     *
     * @return
     */
    public String getStyleMobileClasses() {
        return styleMobileClasses;
    }

    private ContentPolicy paddingPolicyInfo(final SlingHttpServletRequest request) {
        return Optional.ofNullable(request)
                .map(SlingHttpServletRequest::getResourceResolver)
                .map(resourceResolver1 -> resourceResolver1.getResource(request.getRequestPathInfo().getResourcePath()))
                .filter(Objects::nonNull)
                .map(resource -> resource.getResourceResolver().adaptTo(ContentPolicyManager.class))
                .map(contentPolicyManager -> contentPolicyManager.getPolicy(request.getResourceResolver().getResource(request.getRequestPathInfo().getResourcePath())))
                .orElse(null);
    }

}
