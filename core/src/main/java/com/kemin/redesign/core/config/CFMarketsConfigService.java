package com.dish.dopweb.core.configs;

import java.util.List;

/**
 * CF Markets Config Service.
 */
public interface CFMarketsConfigService {
    
    /**
     * Validates if zipcode is valid based on cf active markets.
     *
     * @param zipcode zipcode to evaluate
     * @return true/false if zipcode is valid
     */
    boolean isValidZipcode(String zipcode);
    
    /**
     * Returns list of all cf active markets zipcodes.
     *
     * @return list of zipcodes
     */
    List<String> getZipcodes();
}
