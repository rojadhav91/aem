package com.dish.dopweb.core.models.internal.v1.form;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.dish.dopweb.core.models.form.Text;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.*;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
//import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;



/** Sling model implementation for the form text component */
//@SuppressFBWarnings(value="NM_SAME_SIMPLE_NAME_AS_SUPERCLASS",
//        justification="The Name have to be the same")
@Model(adaptables = SlingHttpServletRequest.class,
        adapters = {Text.class, ComponentExporter.class},
        resourceType = TextImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class TextImpl implements Text {
    public static final  String RESOURCE_TYPE = "/apps/dopweb/components/form/text";
    
    @Delegate @Self @Via(type = ResourceSuperType.class)
    private com.adobe.cq.wcm.core.components.models.form.Text text;


    @ValueMapValue
    private String iconFolderPath;

    @ValueMapValue
    private String iconPosition;
    
    @Override
    public String getIcon(){
        return iconFolderPath;
    }

    @Override
    public String getIconPosition(){
        return iconPosition;
    }
}
