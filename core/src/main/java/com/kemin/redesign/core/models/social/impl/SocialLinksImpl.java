package com.kemin.redesign.core.models.social.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.social.SocialLinkItem;
import com.kemin.redesign.core.models.social.SocialLinks;
import lombok.Getter;
/** Kemin Social Links Sling Model Implementation Class. */
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {SocialLinks.class, ComponentExporter.class},
	    resourceType = SocialLinksImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class SocialLinksImpl implements SocialLinks {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/social-link";

	@ChildResource @Getter private Collection<SocialLinkItem> socialLinks;
	
	@PostConstruct
	protected void init() {
		socialLinks = CollectionUtils.emptyIfNull(socialLinks).stream().filter(item ->StringUtils.isNotBlank(item.getLink()) && StringUtils.isNotBlank(item.getPlatform())).collect(Collectors.toList());
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
