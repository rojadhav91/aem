package com.kemin.redesign.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(
		name = "Coveo Configuration", 
		description = "This configuration reads the values to communicate with Coveo")
public @interface CoveoConfiguration {

    @AttributeDefinition(
			name = "API Key", 
			description = "Coveo API Key")
	public String getApiKey();

    @AttributeDefinition(
        name = "Organization ID", 
        description = "Coveo Organization ID")
    public String getOrganization();

    @AttributeDefinition(
        name = "Search Hub", 
        description = "Coveo Search Hub")
    public String getSearchHub();

    @AttributeDefinition(
        name = "Search Page Url", 
        description = "Coveo Search Page Url")
    public String getSearchPageUrl();

}
