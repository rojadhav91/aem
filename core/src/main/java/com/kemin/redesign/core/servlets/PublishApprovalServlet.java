package com.kemin.redesign.core.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.model.WorkflowModel;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.kemin.redesign.core.ApplicationConstants;

@Component(service = Servlet.class, property = {
		Constants.SERVICE_DESCRIPTION + "= Invoke PublishApproval Workflow Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_POST,
		"sling.servlet.paths=" + "/bin/kemin-redesign/publishApproval" })
public class PublishApprovalServlet extends SlingAllMethodsServlet {

	protected final transient Logger log = LoggerFactory.getLogger(this.getClass());

	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");

	private static final long serialVersionUID = 1L;

	@Reference
	private transient WorkflowService workflowService;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		String pagePath = request.getParameter("page_path");
		if (StringUtils.isBlank(pagePath)) {
			response.getWriter().write("page_path is blank");
			return;
		}
		String dueDate = request.getParameter("dueDate");
		String priority = request.getParameter("priority");
		String legalReview = request.getParameter("legalReview");
		String buReview = request.getParameter("buReview");
		String message = request.getParameter("message");
		JsonObject jsonObject = new JsonObject();
		try {
			ResourceResolver resolver = request.getResourceResolver();
			Resource res = resolver.getResource(pagePath + "/jcr:content");
			ModifiableValueMap vm = res.adaptTo(ModifiableValueMap.class);
			if (StringUtils.isNotBlank(dueDate)) {
				try {
					Date date = dateFormatter.parse(dueDate);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					vm.put("dueDate", calendar);
				} catch (ParseException ex) {
					log.error("DueDate parse error:" + dueDate, ex);
				}
			}

			if (StringUtils.isNotBlank(priority)) {
				vm.put("priority", priority);
			}

			if (StringUtils.isNotBlank(legalReview)) {
				vm.put("skipEditorial", "on".equalsIgnoreCase(legalReview) ? "true" : "false");
			}

			if (StringUtils.isNotBlank(buReview)) {
				vm.put("skipLegal", "on".equalsIgnoreCase(buReview) ? "true" : "false");
			}

			if (StringUtils.isNotBlank(message)) {
				vm.put("skipReviewMessage", message);
			}

			resolver.commit();

			final Session session = resolver.adaptTo(Session.class);
			final WorkflowSession wfSession = workflowService.getWorkflowSession(session);

			WorkflowModel wfModel = wfSession.getModel(ApplicationConstants.KEMIN_REDESIGN_PUBLISH_WORKFLOW);
			WorkflowData wfData = wfSession.newWorkflowData("JCR_PATH", pagePath);

			wfSession.startWorkflow(wfModel, wfData);
			jsonObject.add("status",new JsonPrimitive("success"));
			jsonObject.add("message", new JsonPrimitive("Publish Approval Workflow Started for page " + pagePath));
			response.getWriter().write(jsonObject.toString());
		} catch (WorkflowException e) {
			log.error(e.getMessage(), e);
			jsonObject.add("status",new JsonPrimitive("exception"));
			jsonObject.add("message", new JsonPrimitive("WorkflowException:" + e.getMessage()));
		} catch (PersistenceException e) {
			log.error(e.getMessage(), e);
			jsonObject.add("status",new JsonPrimitive("exception"));
			jsonObject.add("message", new JsonPrimitive("PersistenceException:" + e.getMessage()));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			jsonObject.add("status",new JsonPrimitive("exception"));
			jsonObject.add("message", new JsonPrimitive("Exception:" + e.getMessage()));
		}

	}

}