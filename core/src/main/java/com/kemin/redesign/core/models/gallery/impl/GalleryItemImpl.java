package com.kemin.redesign.core.models.gallery.impl;

import com.kemin.redesign.core.models.gallery.GalleryItem;
import com.kemin.redesign.core.models.social.SocialLinkItem;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
    adaptables = Resource.class,
    adapters = GalleryItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GalleryItemImpl implements GalleryItem {

  //@ValueMapValue @Getter private String image;

  @ValueMapValue  private String caption;

 // @ValueMapValue @Getter private String fileReference;
 @ValueMapValue @Getter private String image;

  @Override
  public String getCaption() {
    if (caption == null) {
      return "no-caption";
    }
    return caption;
      }

}
