package com.kemin.redesign.core.models.banner;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

/** Consumer for Header Component. */
@ConsumerType
public interface Banner extends ComponentExporter {
	String getBackgroundImage();
	String getId();
	//String getWidthOption();
}
