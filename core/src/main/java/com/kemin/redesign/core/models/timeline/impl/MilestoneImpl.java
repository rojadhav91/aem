package com.kemin.redesign.core.models.timeline.impl;

import lombok.Getter;

import java.util.Date;

import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.kemin.redesign.core.models.timeline.Milestone;

@Model(
    adaptables = Resource.class,
    adapters = Milestone.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MilestoneImpl implements Milestone {

  @ValueMapValue @Named("fileReference") @Getter private String image;

  @ValueMapValue @Named("desc") @Getter private String description;
  
  @ValueMapValue @Getter private String year;
  
  @ValueMapValue @Getter private String period;

}
