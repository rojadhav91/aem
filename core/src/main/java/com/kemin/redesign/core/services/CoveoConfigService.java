package com.kemin.redesign.core.services;

public interface CoveoConfigService {
    public String getApiKey();
    public String getOrganization();
    public String getSearchHub();
    public String getSearchPageUrl();
}
