package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Service Coverage CA Config.
 */
@Configuration(
        label = "Service Coverage Brand Configuration",
        description = "Configure the brand for the service coverage validation")
public @interface CheckServiceCoverageCaConfig {

    /**
     * Check Service Coverage Brand
     * @return brand
     */
    @Property(label = "Brand")
    String getBrand();
}
