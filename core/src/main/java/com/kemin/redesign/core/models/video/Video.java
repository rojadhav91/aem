package com.kemin.redesign.core.models.video;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface Video extends ComponentExporter {
    String getVideoType();
    String getDamLink();
    String getYouTubeId();
    String getEmbedId();
    String getTitle();
    String getCaption();
    String getMetaDescription();
    String getThumbnail();
    String getPlayButtonColor();
    String getVideoId();
}
