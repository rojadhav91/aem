package com.dish.dopweb.core.configs;

/**
 * Config to het magento host to check email
 */
public interface MagentoAPIConfigService {
    /**
     * This method will get host url authored in OSGI
     * @return
     */
    String getHost();

    /**
     * This method will get Bearer token authored in OSGI
     * @return bearer token
     */
    String getBearerToken();
}
