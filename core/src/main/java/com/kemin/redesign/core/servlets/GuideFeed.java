package main.java.com.aembootcamp.core.servlets;

public class GuideFeed {
private String guideTitle;
private String heroImagePath;
private String guidepublishDate;
private String guideDetailLink;

public GuideFeed() {
	super();
}

public String getGuideTitle() {
	return guideTitle;
}
public void setGuideTitle(String guideTitle) {
	this.guideTitle = guideTitle;
}
public String getHeroImagePath() {
	return heroImagePath;
}
public void setHeroImagePath(String heroImagePath) {
	this.heroImagePath = heroImagePath;
}
public String getGuidepublishDate() {
	return guidepublishDate;
}
public void setGuidepublishDate(String guidepublishDate) {
	this.guidepublishDate = guidepublishDate;
}
public String getGuideDetailLink() {
	return guideDetailLink;
}
public void setGuideDetailLink(String guideDetailLink) {
	this.guideDetailLink = guideDetailLink;
}

}
