package com.kemin.redesign.core.services;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kemin.redesign.core.config.SiteServiceConfiguration;

@Component(service = SiteConfigService.class, immediate = true )
@Designate(ocd = SiteServiceConfiguration.class)
public class SiteConfigServiceImpl implements SiteConfigService {
    
    private final Logger log = LoggerFactory.getLogger(getClass());

    private List<String> breadcrumbHiddenPathList = new ArrayList<String>();

    protected void activate(SiteServiceConfiguration config) {
        String[] breadcrumbHiddenPaths = ((String[])config.getBreadcrumbHiddenPaths());
        if(breadcrumbHiddenPaths != null && breadcrumbHiddenPaths.length > 0){
        	for(String hiddenPath:breadcrumbHiddenPaths){
        		breadcrumbHiddenPathList.add(hiddenPath.toLowerCase());
            }
        }
    }

	@Override
	public List<String> getBreadcrumbHiddenPaths() {
		return breadcrumbHiddenPathList;
	}
}
