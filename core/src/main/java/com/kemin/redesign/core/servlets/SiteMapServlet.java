package com.kemin.redesign.core.servlets;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.PageManager;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.config.SiteMapServiceConfiguration;
import com.kemin.redesign.core.utils.CommonUtils;

@Component(service = Servlet.class, configurationPid = "com.kemin.redesign.core.servlets.SiteMapServlet", property = {
		Constants.SERVICE_DESCRIPTION + "=Kemin Redesign Page and Asset Site Map Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, 
		"sling.servlet.resourceTypes=" + "kemin-redesign/components/page",
		"sling.servlet.selectors=sitemap", "sling.servlet.extensions=xml" })
@Designate(ocd = SiteMapServiceConfiguration.class)
public final class SiteMapServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = -6275186354652768726L;

	private static final Logger log = LoggerFactory.getLogger(SiteMapServlet.class);

	private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");
    private static final String PROP_VALUE = "value";
    private static final String PROP_TITLE = "jcr:title";
	private static final String NS = "http://www.sitemaps.org/schemas/sitemap/0.9";
	private static final String NS_XHTM = "http://www.w3.org/1999/xhtml";

	@Reference
	private transient Externalizer externalizer;
	
    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

	private String externalizerDomain;

	private boolean includeInheritValue;

	private boolean includeLastModified;

	private String[] changefreqProperties;

	private String[] priorityProperties;

	private String damAssetProperty;

	private List<String> damAssetTypes;

	private List<String> excludeFromSiteMapProperty;

	private String characterEncoding;

	private boolean extensionlessUrls;

	private Map<String, String> urlRewrites;

	private boolean removeTrailingSlash;

	private List<String> excludedPageTemplates;

	private boolean useVanityUrl;
	
	private String hrefLangListPath;

	@Activate
	protected void activate(SiteMapServiceConfiguration config) {
		this.externalizerDomain = config.externalizerDomain();
		this.includeLastModified = config.includeLastMod();
		this.includeInheritValue = config.includeInherit();
		this.changefreqProperties = config.changeFreqProperties();
		this.priorityProperties = PropertiesUtil.toStringArray(config.priorityProperties(), new String[0]);
		this.damAssetProperty = config.damassetsProperty();
		this.damAssetTypes = Arrays.asList(PropertiesUtil.toStringArray(config.damassetsTypes(), new String[0]));
		this.excludeFromSiteMapProperty = Arrays
				.asList(PropertiesUtil.toStringArray(config.excludeProperty(), new String[0]));
		this.characterEncoding = config.characterEncoding();
		this.extensionlessUrls = config.extensionlessUrls();
		this.urlRewrites = toMap(PropertiesUtil.toStringArray(config.urlRewrites(), new String[0]), ":", true, "",
				false);
		this.removeTrailingSlash = config.removeSlash();
		this.excludedPageTemplates = Arrays
				.asList(PropertiesUtil.toStringArray(config.excludeTemplates(), new String[0]));
		this.useVanityUrl = config.useVanity();
		this.hrefLangListPath = config.hrefLangListPath();
	}

	private Map<String, String> toMap(final String[] values, final String separator, final boolean allowValuelessKeys,
			final String defaultValue, final boolean allowMultipleSeparators) {

		final Map<String, String> map = new LinkedHashMap<String, String>();

		if (values == null || values.length < 1) {
			return map;
		}

		for (final String value : values) {
			final String[] tmp = StringUtils.split(value, separator, allowMultipleSeparators ? 2 : -1);

			if (tmp.length == 1 && allowValuelessKeys) {
				if (StringUtils.startsWith(value, separator)) {
					continue;
				}

				if (StringUtils.stripToNull(tmp[0]) != null) {
					map.put(StringUtils.trim(tmp[0]), StringUtils.trimToEmpty(defaultValue));
				}
			} else if (tmp.length == 2 && StringUtils.stripToNull(tmp[0]) != null) {
				map.put(StringUtils.trim(tmp[0]), StringUtils.trimToEmpty(tmp[1]));
			}
		}

		return map;
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(request.getResponseContentType());
		if (StringUtils.isNotEmpty(this.characterEncoding)) {
			response.setCharacterEncoding(characterEncoding);
		}
		ResourceResolver resourceResolver = request.getResourceResolver();
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		Page page = pageManager.getContainingPage(request.getResource());

		String requestPath = page.getPath();

		XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
		XMLStreamWriter stream = null;
		try {
			stream = outputFactory.createXMLStreamWriter(response.getWriter());
			stream.writeStartDocument("UTF-8", "1.0");

			log.debug("requestPath:"+requestPath);
			if (requestPath.equals(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH)) {
				stream.writeStartElement("", "sitemapindex", NS);
				stream.writeAttribute("xmlns", NS);
				writeRootSitemapNodes(resourceResolver, page, stream);
				stream.writeEndElement();
			} else {
				stream.writeStartElement("", "urlset", NS);
				stream.writeNamespace("", NS);
				stream.writeNamespace("xhtml", NS_XHTM);
				// first do the current page
				write(page, stream, request);

				for (Iterator<Page> children = page.listChildren(new PageFilter(false, true), true); children
						.hasNext();) {
					write(children.next(), stream, request);
				}

				if (damAssetTypes.size() > 0 && damAssetProperty.length() > 0) {
					for (Resource assetFolder : getAssetFolders(page, resourceResolver)) {
						writeAssets(stream, assetFolder, request);
					}
				}

				stream.writeEndElement();
			}
			stream.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (XMLStreamException e) {
					log.warn("Can not close xml stream writer", e);
				}
			}
		}
	}

	private void writeRootSitemapNodes(ResourceResolver resolver, Page requestPage, XMLStreamWriter stream)
			throws XMLStreamException {
		Iterator<Page> children = requestPage.listChildren();
		while (children.hasNext()) {
			Page child = children.next();
			if (child.getName().length() == 2) {
				Iterator<Page> childChildren = child.listChildren();
				while (childChildren.hasNext()) {
					Page childChild = childChildren.next();
					String loc = childChild.getPath().replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH, "");
					stream.writeStartElement("sitemap");
					writeElement(stream, "loc", externalizer.publishLink(resolver, loc) + ".sitemap.xml");
					if (includeLastModified && childChild.getLastModified()!=null) {
						writeElement(stream, "lastmod", DATE_FORMAT.format(childChild.getLastModified()));
					}
					stream.writeEndElement();
				}
			}
		}
	}

	private Collection<Resource> getAssetFolders(Page page, ResourceResolver resolver) {
		List<Resource> allAssetFolders = new ArrayList<Resource>();
		ValueMap properties = page.getProperties();
		String[] configuredAssetFolderPaths = properties.get(damAssetProperty, String[].class);
		if (configuredAssetFolderPaths != null) {
			// Sort to aid in removal of duplicate paths.
			Arrays.sort(configuredAssetFolderPaths);
			String prevPath = "#";
			for (String configuredAssetFolderPath : configuredAssetFolderPaths) {
				// Ensure that this folder is not a child folder of another
				// configured folder, since it will already be included when
				// the parent folder is traversed.
				if (StringUtils.isNotBlank(configuredAssetFolderPath) && !configuredAssetFolderPath.equals(prevPath)
						&& !StringUtils.startsWith(configuredAssetFolderPath, prevPath + "/")) {
					Resource assetFolder = resolver.getResource(configuredAssetFolderPath);
					if (assetFolder != null) {
						prevPath = configuredAssetFolderPath;
						allAssetFolders.add(assetFolder);
					}
				}
			}
		}
		return allAssetFolders;
	}

	private String applyUrlRewrites(String url) {
		try {
			String path = URI.create(url).getPath();
			for (Map.Entry<String, String> rewrite : urlRewrites.entrySet()) {
				if (path.startsWith(rewrite.getKey())) {
					return url.replaceFirst(rewrite.getKey(), rewrite.getValue());
				}
			}
			return url;
		} catch (IllegalArgumentException e) {
			return url;
		}
	}

	@SuppressWarnings("squid:S1192")
	private void write(Page page, XMLStreamWriter stream, SlingHttpServletRequest request) throws XMLStreamException {
		if (isHiddenByPageProperty(page) || isHiddenByPageTemplate(page)) {
			return;
		}
		stream.writeStartElement(NS, "url");
		String loc = getPageLocation(page, request);

		loc = applyUrlRewrites(loc);

		writeElement(stream, "loc", loc);
		writeHreflangTag(page, stream, request);
		
		if (includeLastModified) {
			Calendar cal = page.getLastModified();
			if (cal != null) {
				writeElement(stream, "lastmod", DATE_FORMAT.format(cal));
			}
		}

		if (includeInheritValue) {
			HierarchyNodeInheritanceValueMap hierarchyNodeInheritanceValueMap = new HierarchyNodeInheritanceValueMap(
					page.getContentResource());
			writeFirstPropertyValue(stream, "changefreq", changefreqProperties, hierarchyNodeInheritanceValueMap);
			writeFirstPropertyValue(stream, "priority", priorityProperties, hierarchyNodeInheritanceValueMap);
		} else {
			ValueMap properties = page.getProperties();
			writeFirstPropertyValue(stream, "changefreq", changefreqProperties, properties);
			writeFirstPropertyValue(stream, "priority", priorityProperties, properties);
		}

		stream.writeEndElement();
	}

    /**
     * It writes the hreflang for all countries and languages.
     *
     * @param page   - Page for which hreflang has to be writtern.
     * @param stream - Writes to
     */
    private void writeHreflangTag(Page page, XMLStreamWriter stream, SlingHttpServletRequest request) throws XMLStreamException {
        Map<String, String> hrefLangMap = getHrefLangMap(page);
        for (String key : hrefLangMap.keySet()) {
            String loc = getLangHrefPageLoc(page, hrefLangMap.get(key), request);
            if (isExist(page, hrefLangMap.get(key))) {
                writeHreflangTag(stream, key, trimURL(loc));
            }
        }
    }


    private Map<String, String> getHrefLangMap(final Page currentPage) {
        final Map<String, String> hrefLangMap = new HashMap<>();
		try (ResourceResolver resourceResolver = CommonUtils.getResourceResolver(resourceResolverFactory, ApplicationConstants.KEMIN_REDESIGN_REPO_READ)) {
	        Resource themeList = resourceResolver.getResource(hrefLangListPath);
	        if (themeList != null) {
	        	Iterator<Resource> childItr = themeList.getChildren().iterator();
	            while (childItr.hasNext()) {
	                ValueMap vm = childItr.next().adaptTo(ValueMap.class);
	                hrefLangMap.put(vm.get(PROP_TITLE, ""), vm.get(PROP_VALUE,""));
	            }
	        }
		} catch (LoginException e) {
			log.error("Could not get a service resolver", e);
		}

        return hrefLangMap;
    }
    private String getLangHrefPageLoc(Page page, String hrefLang, SlingHttpServletRequest request) {
        return getPageLocation(page, request).replace(getMasterRegionAndLanguage(page), hrefLang);
    }
    /**
     * Get Page external URL.
     *
     * @param page - Page
     * @return - External URL.
     */
    private String getPageLocation(Page page, SlingHttpServletRequest request) {
        String loc = "";
        
		if (useVanityUrl && !StringUtils.isEmpty(page.getVanityUrl())) {
			loc = externalizeUri(request, page.getVanityUrl());
		} else if (!extensionlessUrls) {
			loc = externalizeUri(request, String.format("%s.html", page.getPath()));
		} else {
			String urlFormat = removeTrailingSlash ? "%s" : "%s/";
			loc = externalizeUri(request, String.format(urlFormat, page.getPath()));
		}
		
        return loc;
    }

    /**
     * Writes the hreflang tag
     *
     * @param stream   - Stream through which hreflang will be written
     * @param hreflang - hreflang from the configuration
     * @param href     -href from the configuration
     * @throws XMLStreamException - If any error encountered while writing the tag.
     */
    private void writeHreflangTag(XMLStreamWriter stream, String hreflang, String href)
        throws XMLStreamException {
        String elementName = "link";
        stream.writeStartElement(NS_XHTM, elementName);
        stream.writeAttribute("rel", "alternate");
        stream.writeAttribute("hreflang", hreflang);
        stream.writeAttribute("href", href);
        stream.writeEndElement();
    }
    private boolean isExist(final Page page, final String hreflang) {
        String path = page.getPath().replace(getMasterRegionAndLanguage(page), hreflang);
        Page hrefLangPage = page.getPageManager().getPage(path);
        return hrefLangPage != null;
    }
    //it will returns master region and language  like language-masters/en
    private String getMasterRegionAndLanguage(Page page) {
        return CommonUtils.getSelectedRegion(page.getPath())+"/" + CommonUtils.getSelectedLanguage(page.getPath());
    }
    /**
     * Remove the extra content in url which will not be available in dispatcher.
     *
     * @param url - URL to be trimmed
     * @return - trimmed URL
     */
    private String trimURL(String url) {
        //remove the html extension from the url
        url = url.replace(".html", "");
        //remove /home if it is not home page
        if (url.contains("/home/")) {
            url = url.replace("/home/", "/");
        }
        //remove /content/kemin for all pages
        url = url.replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH+"/", "/");
        return url;
    }
    
	private boolean isHiddenByPageProperty(Page page) {
		boolean flag = false;
		if (this.excludeFromSiteMapProperty != null) {
			for (String pageProperty : this.excludeFromSiteMapProperty) {
				flag = flag || page.getProperties().get(pageProperty, Boolean.FALSE);
			}
		}
		return flag;
	}

	private boolean isHiddenByPageTemplate(Page page) {
		boolean flag = false;
		if (this.excludedPageTemplates != null) {
			for (String pageTemplate : this.excludedPageTemplates) {
				flag = flag
						|| page.getProperties().get("cq:template", StringUtils.EMPTY).equalsIgnoreCase(pageTemplate);
			}
		}
		return flag;
	}

	private String externalizeUri(SlingHttpServletRequest request, String path) {
		if (StringUtils.isNotBlank(externalizerDomain)) {
			return externalizer.externalLink(request.getResourceResolver(), externalizerDomain, path);
		} else {
			log.debug(
					"No externalizer domain configured, take into account current host header {} and current scheme {}",
					request.getServerName(), request.getScheme());
			return externalizer.absoluteLink(request, request.getScheme(), path);
		}
	}

	private void writeAsset(Asset asset, XMLStreamWriter stream, SlingHttpServletRequest request)
			throws XMLStreamException {
		stream.writeStartElement(NS, "url");

		String loc = externalizeUri(request, asset.getPath());
		writeElement(stream, "loc", loc);

		if (includeLastModified) {
			long lastModified = asset.getLastModified();
			if (lastModified > 0) {
				writeElement(stream, "lastmod", DATE_FORMAT.format(lastModified));
			}
		}

		Resource contentResource = asset.adaptTo(Resource.class).getChild(JcrConstants.JCR_CONTENT);
		if (contentResource != null) {
			if (includeInheritValue) {
				HierarchyNodeInheritanceValueMap hierarchyNodeInheritanceValueMap = new HierarchyNodeInheritanceValueMap(
						contentResource);
				writeFirstPropertyValue(stream, "changefreq", changefreqProperties, hierarchyNodeInheritanceValueMap);
				writeFirstPropertyValue(stream, "priority", priorityProperties, hierarchyNodeInheritanceValueMap);
			} else {
				ValueMap properties = contentResource.getValueMap();
				writeFirstPropertyValue(stream, "changefreq", changefreqProperties, properties);
				writeFirstPropertyValue(stream, "priority", priorityProperties, properties);
			}
		}

		stream.writeEndElement();
	}

	private void writeAssets(final XMLStreamWriter stream, final Resource assetFolder,
			final SlingHttpServletRequest request) throws XMLStreamException {
		for (Iterator<Resource> children = assetFolder.listChildren(); children.hasNext();) {
			Resource assetFolderChild = children.next();
			if (assetFolderChild.isResourceType(DamConstants.NT_DAM_ASSET)) {
				Asset asset = assetFolderChild.adaptTo(Asset.class);

				if (damAssetTypes.contains(asset.getMimeType())) {
					writeAsset(asset, stream, request);
				}
			} else {
				writeAssets(stream, assetFolderChild, request);
			}
		}
	}

	private void writeFirstPropertyValue(final XMLStreamWriter stream, final String elementName,
			final String[] propertyNames, final ValueMap properties) throws XMLStreamException {
		for (String prop : propertyNames) {
			String value = properties.get(prop, String.class);
			if (value != null) {
				writeElement(stream, elementName, value);
				break;
			}
		}
	}

	@SuppressWarnings("squid:S1144")
	private void writeFirstPropertyValue(final XMLStreamWriter stream, final String elementName,
			final String[] propertyNames, final InheritanceValueMap properties) throws XMLStreamException {
		for (String prop : propertyNames) {
			String value = properties.get(prop, String.class);
			if (value == null) {
				value = properties.getInherited(prop, String.class);
			}
			if (value != null) {
				writeElement(stream, elementName, value);
				break;
			}
		}
	}

	private void writeElement(final XMLStreamWriter stream, final String elementName, final String text)
			throws XMLStreamException {
		stream.writeStartElement(NS, elementName);
		stream.writeCharacters(text);
		stream.writeEndElement();
	}

}
