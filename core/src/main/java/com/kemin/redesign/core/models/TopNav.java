package com.dish.dopweb.core.models;

import com.dish.dopweb.core.beans.TopNavMenuItems;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * TopNav sling model.
 */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        resourceType = "dopweb/components/structure/topnav",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TopNav {

    @ChildResource
    private List<TopNavMenuItems> menuItems;

    public List<TopNavMenuItems> getMenuItems() {
        return Optional.ofNullable(menuItems)
                .orElse(Collections.emptyList());
    }
}
