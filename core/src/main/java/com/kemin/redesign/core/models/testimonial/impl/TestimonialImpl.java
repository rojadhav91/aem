package com.kemin.redesign.core.models.testimonial.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.testimonial.Testimonial;
import com.kemin.redesign.core.models.testimonial.TestimonialItem;
import com.kemin.redesign.core.utils.StreamUtils;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Testimonial.class, ComponentExporter.class},
        resourceType = TestimonialImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class TestimonialImpl implements Testimonial {
    protected static final String RESOURCE_TYPE = "kemin-redesign/components/testimonial";

    @ValueMapValue @Getter private String ctaName;

    @ValueMapValue @Getter private String ctaLink;

    @ChildResource
    private Resource testimonials;

    @Getter
    private Collection<TestimonialItem> testimonialItems;

    @PostConstruct
    protected void init() {
        testimonialItems = Optional.ofNullable(testimonials)
                .map(Resource::getChildren)
                .map(StreamUtils::stream)
                .orElseGet(Stream::empty)
                .map(resource -> resource.adaptTo(TestimonialItem.class))
                .collect(Collectors.toList());

        testimonialItems.stream().findFirst().ifPresent(first -> first.setInitialCssClass("active"));
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }
}
