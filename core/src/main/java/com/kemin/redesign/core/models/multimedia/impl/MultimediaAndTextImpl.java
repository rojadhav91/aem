package com.kemin.redesign.core.models.multimedia.impl;

import com.kemin.redesign.core.models.multimedia.MultimediaAndText;
import com.kemin.redesign.core.models.video.Video;

import lombok.Getter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {MultimediaAndText.class},
        resourceType = MultimediaAndTextImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MultimediaAndTextImpl implements MultimediaAndText {
	
	protected static final String RESOURCE_TYPE = "kemin-redesign/components/multimedia-text";
	
	@Self @Via(type = ResourceSuperType.class)
	private Video video;
	
	@ValueMapValue private String mediaTextSplit;
	
	@ValueMapValue @Getter private String layout;
	@ValueMapValue @Getter private String backgroundColor;
	@ValueMapValue @Getter private String mediaType;
	
	@ValueMapValue(name="image/file") @Getter private String imageSrc;
	@ValueMapValue(name="image/alt") @Getter private String imageAlt;
	
	@ValueMapValue(name="text/title") @Getter private String title;
	@ValueMapValue(name="text/description") @Getter private String description;
	@ValueMapValue(name="buttons/item0/text") @Getter private String firstButtonText;
	@ValueMapValue(name="buttons/item0/link") @Getter private String firstButtonLink; 
	@ValueMapValue(name="buttons/item0/style") @Getter private String firstButtonStyle; 
	@ValueMapValue(name="buttons/item1/text") @Getter private String secondButtonText;
	@ValueMapValue(name="buttons/item1/link") @Getter private String secondButtonLink;
	@ValueMapValue(name="buttons/item1/style") @Getter private String secondButtonStyle; 

	@Override
	public String getExportedType() {
		return video.getExportedType();
	}
	@Override
	public String getVideoType() {
		return video.getVideoType();
	}
	@Override
	public String getDamLink() {
		return video.getDamLink();
	}
	@Override
	public String getYouTubeId() {
		return video.getYouTubeId();
	}
	@Override
	public String getEmbedId() {
		return video.getEmbedId();
	}
	@Override
	public String getCaption() {
		return video.getCaption();
	}
	@Override
	public String getMetaDescription() {
		return video.getMetaDescription();
	}
	@Override
	public String getThumbnail() {
		return video.getThumbnail();
	}
	@Override
	public String getPlayButtonColor() {
		return video.getPlayButtonColor();
	}
	
	@Override
	public String getVideoId() {
		return video.getVideoId();
	}
	
	@Override
	public int getMediaColumnCount() {
		if("even".equals(mediaTextSplit)) {
			return 6;
		}
		return 7;
	}
	
	@Override
	public int getTextColumnCount() {
		if("even".equals(mediaTextSplit)) {
			return 6;
		}
		return 5;
	}
}
