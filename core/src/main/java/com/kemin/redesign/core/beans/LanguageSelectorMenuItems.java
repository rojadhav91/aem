package com.dish.dopweb.core.beans;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * Language Selector Menu Items Sling Model
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LanguageSelectorMenuItems {

    @ValueMapValue
    @Getter
    private String languageTitle;

    @ValueMapValue
    @Getter
    private String languageIsoCode;

    @ValueMapValue
    @Getter
    private Boolean isDefaultLanguage;

}
