package com.kemin.redesign.core.services;

import com.kemin.redesign.core.config.CoveoConfiguration;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = CoveoConfigService.class, immediate = true )
@Designate(ocd = CoveoConfiguration.class)
public class CoveoConfigServiceImpl implements CoveoConfigService {
    
    private final Logger log = LoggerFactory.getLogger(getClass());

    private CoveoConfiguration config;

    protected void activate(CoveoConfiguration config) {
        log.debug("####In my TokenServiceImpl activate method");
        this.config = config;
        log.debug("####Org set to {}", config.getOrganization());
    }

    @Override
    public String getApiKey() {
        return config.getApiKey();
    }

    @Override
    public String getOrganization() {
        return config.getOrganization();
    }

    @Override
    public String getSearchHub() {
        return config.getSearchHub();
    }

    @Override
    public String getSearchPageUrl() {
        return config.getSearchPageUrl();
    }
    
}
