package com.kemin.redesign.core.models.header.impl;

import com.kemin.redesign.core.models.header.HeaderItem;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/** Customer Portal Header Child Resource */
@Model(
    adaptables = Resource.class,
    adapters = HeaderItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderItemImpl implements HeaderItem {

  @ValueMapValue @Getter private String linkUrl;

  @ValueMapValue @Getter private String linkTitle;
}
