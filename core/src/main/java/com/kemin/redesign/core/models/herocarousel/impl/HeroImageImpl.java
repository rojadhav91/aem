package com.kemin.redesign.core.models.herocarousel.impl;

import lombok.Getter;

import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.kemin.redesign.core.models.herocarousel.HeroImage;

@Model(
    adaptables = Resource.class,
    adapters = HeroImage.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeroImageImpl implements HeroImage {

  @ValueMapValue @Named("fileReference") @Getter private String heroImage;

  @ValueMapValue @Getter private String title;
  
  @ValueMapValue @Named("desc") @Getter private String description;
  
  @ValueMapValue @Getter private String linkLabel;
  
  @ValueMapValue @Getter private String linkUrl;
  
  @ValueMapValue @Getter @Named("transLabel") private String transitionLabel;
  
  @ValueMapValue @Named("iconFileReference") @Getter private String transitionIcon;

}
