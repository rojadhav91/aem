package com.kemin.redesign.core.models.dynamicmedia;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface DynamicImage extends ComponentExporter {
	String getDomainHost();

	String getLocaleLanguage();
	
	boolean isDymImage();

	String getViewerPath();

	String getImageServer();

	String getVideoServer();

	String getContentUrl();
	
	String getMediaId();
	
	String getImageUrl();
	
	String getAssetPath();
	
	String getAssetName();
	
	String getAltText();
	
	String getPresets();
}
