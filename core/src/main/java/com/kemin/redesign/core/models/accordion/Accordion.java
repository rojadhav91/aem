package com.kemin.redesign.core.models.accordion;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Collection;

@ConsumerType
public interface Accordion extends ComponentExporter {
    String getId();
    String getTitle();
    Collection<AccordionItem> getAccordionItems();
}
