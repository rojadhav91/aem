package com.kemin.redesign.core.models.featured.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.beans.PageBean;
import com.kemin.redesign.core.models.featured.Content;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.commons.lang.StringUtils;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {Content.class, ComponentExporter.class},
        resourceType = ContentImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
public class ContentImpl implements Content {

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/featured-content";

    @SlingObject
    private ResourceResolver resourceResolver;
    List<PageBean> featuredPageItemList;

    @ChildResource(injectionStrategy = InjectionStrategy.DEFAULT)
    private Resource featuredpages;

    @ValueMapValue @Getter private String moreLink;
    @ValueMapValue @Getter protected String defaultImagePath;

    @PostConstruct
    protected void init() throws RepositoryException {
        featuredPageItemList = new ArrayList<>();

        Iterator<Resource> children;
        String thumbnailPath;

        if (featuredpages != null) {
            children = featuredpages.listChildren();
            while (children.hasNext()) {
                Resource childResource = children.next();
                ValueMap properties = childResource.adaptTo(ValueMap.class);
                if (properties != null) {
                    String rootPath = properties.get("rootPath", String.class);
                    PageBean pageBean = new PageBean();
                    if(resourceResolver.getResource(rootPath)==null){
                        log.warn(rootPath+" is not existing");
                        continue;
                    }
                    Page selectedPage = resourceResolver.getResource(rootPath).adaptTo(Page.class);
                    assert selectedPage != null;
                    if(properties.containsKey("moreLink")){
                        moreLink = properties.get("moreLink", String.class);
                        pageBean.setMoreLink(moreLink);
                    } else{
                        pageBean.setMoreLink("Learn More");
                    }
                    pageBean.setTitle(selectedPage.getNavigationTitle());
                    pageBean.setURL(selectedPage.getPath());
                    String url = pageBean.getURL()+".html";
                    pageBean.setURL(url);
                    Node selectedPageNode = selectedPage.adaptTo(Node.class);
                    Node spn = selectedPageNode.getNode("jcr:content");

                    if(spn.hasProperty("jcr:description")){
                        String desc = spn.getProperty("jcr:description").getString();
                        String abbr_desc = StringUtils.abbreviate(desc, 150);
                        pageBean.setDescription(abbr_desc);
                    }

                    Node imageNode = JcrUtils.getNodeIfExists(spn,"image");

                    if(properties.containsKey("thumbnailPath")){
                        thumbnailPath = properties.get("thumbnailPath", String.class);
                        pageBean.setPublishedThumbnailImage(thumbnailPath);
                    }else if((imageNode!=null)&&(imageNode.hasProperty("fileReference"))){
                        pageBean.setPublishedThumbnailImage(imageNode.getProperty("fileReference").getString());
                    }else{
                        pageBean.setPublishedThumbnailImage(defaultImagePath);
                    }
                    featuredPageItemList.add(pageBean);
                }

            }
        }
        for(PageBean p:featuredPageItemList){
            log.info(p.getTitle() + p.getDescription()+p.getPublishedThumbnailImage());
        }
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    @Override
    public List<PageBean> getPageItemList() {

        return featuredPageItemList;
    }
}

