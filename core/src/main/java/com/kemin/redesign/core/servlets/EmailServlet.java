package com.aembootcamp.core.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.jcr.JsonItemWriter;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.acs.commons.email.EmailService;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;

import org.apache.log4j.Logger;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Email service servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_POST,
		"sling.servlet.resourceTypes=" + "aem-bootcamp/components/general/contact-form",
		"sling.servlet.extensions=" + "html", "sling.servlet.selectors=" + "contact"

})
public class EmailServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static final String TEMPLATE_PATH = "/etc/notification/email/aem-bootcamp/email.txt";
	private static final Logger LOGGER = Logger.getLogger(EmailServlet.class);
	private static final String CONTENT = "/content/aem-bootcamp/contact-us/";
	private static final String NODE = "nt:unstructured";
	private String myJSON = "";
	StringBuilder sb = new StringBuilder();

	@Reference
	EmailService emailService;

	@Reference
	private ResourceResolverFactory factory;
	private ResourceResolver resourceResolver;

	@Override
	protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
			throws ServletException, IOException {
		String FirstName = req.getParameter("fname");
		String LastName = req.getParameter("lname");
		String Email = req.getParameter("email");
		String Subject = req.getParameter("subject");
		String Message = req.getParameter("message");
		 resp.setStatus(HttpServletResponse.SC_OK);
		 sendMail(FirstName, LastName, Email, Subject, Message);
		 createNode(FirstName,LastName,Email);
	}

	private void createNode(String firstName, String lastName, String email) {
		try {

			Resource resource = getResourceContent(CONTENT);

			boolean flag = false;
			if (resource != null) {
				Node node = resource.adaptTo(Node.class);
				if (node != null) {
					NodeIterator checkNode = node.getNodes();

					while (checkNode.hasNext()) {
						Node imageNode = checkNode.nextNode();
						if (imageNode != null) {
							if (imageNode.getName().equals(getCurrentTimeStamp())) {
								flag = true;
							}
						}
					}
					if (flag == true) {
						Resource resource1 = getResourceContent(CONTENT + getCurrentTimeStamp());
						if (resource1 != null) {
							Node childNode = resource1.adaptTo(Node.class);
							if (childNode != null) {
								Node nest = childNode.addNode(email, NODE);
								nest.setProperty("FirstName", firstName);
								nest.setProperty("LstName", lastName);
							}
						}
					} else {
						Node newNode = node.addNode(getCurrentTimeStamp(), NODE);
						Node nest = newNode.addNode(email, "nt:unstructured");
						nest.setProperty("FirstName", firstName);
						nest.setProperty("LstName", lastName);
					}
				}
				resourceResolver.commit();
			}

		} catch (RepositoryException | PersistenceException e) {
			LOGGER.info(e.getMessage(), e);
		}

	}

	private void sendMail(String firstName, String lastName, String email, String subject, String message) {
		Map<String, String> emailParams = new HashMap<String, String>();
		emailParams.put("email", email);
		emailParams.put("FirstName", firstName);
		emailParams.put("LastName", lastName);
		emailParams.put("subject", subject);
		emailParams.put("Message", message);
		String[] recipients = { email };
		List<String> failureList = emailService.sendEmail(TEMPLATE_PATH, emailParams, recipients);
		if (failureList.isEmpty()) {
			LOGGER.info("Email sent successfully to the recipients");
		} else {
			LOGGER.info("Email sent failed");
		}
	}

	public Resource getResourceContent(String path) {
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put(ResourceResolverFactory.SUBSERVICE, "ContactUsServiceUser");
		Resource resource = null;
		try {

			resourceResolver = factory.getServiceResourceResolver(paraMap);
			if (resourceResolver == null)
				LOGGER.info("Could not obtain a CRX User for the Service:''");
			resource = resourceResolver.getResource(path);
		} catch (LoginException e) {

		}
		return resource;
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat formDate = new SimpleDateFormat("ddMMyyyy");
		String strDate = formDate.format(new Date());
		return strDate;
	}	
}