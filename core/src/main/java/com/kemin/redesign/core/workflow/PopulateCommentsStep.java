package com.kemin.redesign.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.services.PopulateComment;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

// Sling Imports
// This is a component so it can provide or consume services
@Component(service = WorkflowProcess.class, property = { "process.label = Populate Comments Step",
		Constants.SERVICE_DESCRIPTION + "=Populate Comments Step", })
public class PopulateCommentsStep implements WorkflowProcess {
    private static String EDITORIAL_REVIEWER = "Editorial Reviewer";
    private static String LEGAL_REVIEWER = "Legal Reviewer";
    /**
     * Default log.
     */
    protected final Logger log = LoggerFactory.getLogger(PopulateCommentsStep.class);
    
    @Reference 
    ResourceResolverFactory resourceResolverFactory;
    
    public void execute(WorkItem workItem, WorkflowSession wfsession, MetaDataMap args) throws WorkflowException {
        List<HistoryItem> history = wfsession.getHistory(workItem.getWorkflow());
        StringBuilder reviewComments = new StringBuilder(ApplicationConstants.WORKITEM_INITATOR_COMMENT).append(":");
        String initorComment = workItem.getWorkflow().getMetaDataMap().get("startComment", String.class);
        reviewComments.append(initorComment).append(" ");
        String editorialComment = StringUtils.EMPTY;
        String legalComment = StringUtils.EMPTY;
        for (int i = 0; i < history.size(); i++) {
            WorkItem wrkItm = history.get(i).getWorkItem();
            String title = wrkItm.getNode().getTitle();
            if (EDITORIAL_REVIEWER.equals(title)) {
                editorialComment = wrkItm.getMetaDataMap().get(ApplicationConstants.WORKITEM_COMMENT, String.class);
                reviewComments.append(ApplicationConstants.WORKITEM_EDITORIAL_COMMENT).append(":").append(editorialComment).append(" ");
            } else if (LEGAL_REVIEWER.equals(title)) {
                legalComment = wrkItm.getMetaDataMap().get(ApplicationConstants.WORKITEM_COMMENT, String.class);
                reviewComments.append(ApplicationConstants.WORKITEM_LEGAL_COMMENT).append(":").append(legalComment);
            }
        }
        workItem.getWorkflowData().getMetaDataMap().put(ApplicationConstants.WORKITEM_INITATOR_COMMENT_NAME, initorComment);
        workItem.getWorkflowData().getMetaDataMap().put(ApplicationConstants.WORKITEM_EDITORIAL_COMMENT_NAME, editorialComment);
        workItem.getWorkflowData().getMetaDataMap().put(ApplicationConstants.WORKITEM_LEGAL_COMMENT_NAME, legalComment);
        String id = workItem.getWorkflow().getId();
        PopulateComment populateComment = new PopulateComment(resourceResolverFactory, reviewComments.toString(), id);
        populateComment.run();
    }
}