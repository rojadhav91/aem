package com.kemin.redesign.core.models.list.impl;

import com.adobe.cq.export.json.ExporterConstants;
import com.adobe.cq.wcm.core.components.models.List;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.wcm.api.Page;
import com.drew.lang.annotations.NotNull;
import com.drew.lang.annotations.Nullable;
import com.kemin.redesign.core.models.list.AssetListItem;
import lombok.Getter;
import lombok.experimental.Delegate;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import java.text.Collator;
import java.util.*;

/** Kemin Redesign Delegate List Class */
@Model(
    adaptables = SlingHttpServletRequest.class,
    adapters = List.class,
    resourceType = "kemin-redesign/components/list",
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class AssetList implements List {

  @Self
  @Via(
      type =
          ResourceSuperType
              .class)
  @Delegate(excludes = DelegationExclusion.class)
  private List delegate;

  private static final Logger LOGGER = LoggerFactory.getLogger(AssetList.class);

  @ValueMapValue protected String listFrom;

  @ValueMapValue protected String childListFrom;

  @ValueMapValue protected String fixedListFrom;

  @ValueMapValue protected String assetFolder;

  @ValueMapValue @Getter protected String displayAs;

  @ScriptVariable protected ValueMap properties;

  @ScriptVariable private Page currentPage;

  @SlingObject private ResourceResolver resourceResolver;

  private static final String PN_MAX_ITEMS = "maxItems";

  private static final String PN_ASSETS = "assets";

  private static final int MAX_ITEMS_DEFAULT = 0;

  public String getType() {
    if (StringUtils.isNotBlank(listFrom)) {
      return listFrom.equalsIgnoreCase("children") ? childListFrom : fixedListFrom;
    }
    return "";
  }

  /**
   * This method return assets list in case asset is chosen as list type and for pages core
   * component methods gets called from HTL Gets the asset items.
   *
   * @return the asset items
   */
  public java.util.List<AssetListItem> getAssetItems() {
    String type = getType();
    java.util.List<AssetListItem> list = new ArrayList<>();
    LOGGER.debug("type of list is:{} and listfrom is :{}", type, listFrom);

    if (type.equalsIgnoreCase(PN_ASSETS) && listFrom.equalsIgnoreCase("children")) {
      prepareListChildren(list);
    }
    if (type.equalsIgnoreCase(PN_ASSETS) && listFrom.equalsIgnoreCase("static")) {
      prepareListFixed(list);
    }

    if (!list.isEmpty()) {
      OrderByAsset orderAssetBy =
          OrderByAsset.fromString(properties.get(PN_ORDER_BY, StringUtils.EMPTY));
      if (orderAssetBy != null) {
        AssetSortOrder assetSortOrder =
            AssetSortOrder.fromString(properties.get(PN_SORT_ORDER, AssetSortOrder.ASC.value));
        LOGGER.debug(
            "going to sort the list through orderby:{} and sortby:{}",
            orderAssetBy,
            assetSortOrder);
        list.sort(new ListSort(orderAssetBy, assetSortOrder, currentPage.getLanguage()));
      }
      LOGGER.debug("size of the list is:{}", list.size());
      int maxItems = properties.get(PN_MAX_ITEMS, MAX_ITEMS_DEFAULT);
      if (maxItems != 0 && maxItems <= list.size()) {
        list = list.subList(0, maxItems);
      }
    }
    return list;
  }

  /**
   * This prepare assets list for fixed list Prepare list fixed.
   *
   * @param list the list
   */
  private void prepareListFixed(java.util.List<AssetListItem> list) {
    java.util.List<String> assetList = Arrays.asList(this.properties.get(PN_ASSETS, new String[0]));
    LOGGER.debug("assetlist size is :{} in case of fixed list", assetList.size());
    assetList.forEach(
        assetPath -> {
          Resource resource = resourceResolver.getResource(assetPath);
          if (resource != null) {
            Asset asset = resource.adaptTo(Asset.class);
            if (asset != null) {
              setAssetItem(list, asset);
            }
          }
        });
  }

  /**
   * This prepare assets list for children of asset folder Prepare list children.
   *
   * @param list the list
   */
  private void prepareListChildren(java.util.List<AssetListItem> list) {
    Resource resource = resourceResolver.getResource(assetFolder);
    if (null != resource) {
      Iterator<Asset> resourceIterator = DamUtil.getAssets(resource);
      LOGGER.debug("creating a list for children asset");
      resourceIterator.forEachRemaining(asset -> setAssetItem(list, asset));
    }
  }

  /**
   * Sets the asset item.
   *
   * @param list the list
   * @param asset the asset
   */
  private void setAssetItem(java.util.List<AssetListItem> list, Asset asset) {
    if (null != asset) {
      AssetListItem item = new AssetListItem();
      String url =
          asset.getPath() != null ? asset.getPath() : org.apache.commons.lang.StringUtils.EMPTY;
      String previewAssetImage =
          asset.getRendition("cq5dam.thumbnail.319.319.png") != null
              ? asset.getRendition("cq5dam.thumbnail.319.319.png").getPath()
              : org.apache.commons.lang.StringUtils.EMPTY;
      String title = getMetdataValue(asset);
      String lastModified =
          asset.getMetadataValueFromJcr("jcr:lastModified") != null
              ? asset.getMetadataValueFromJcr("jcr:lastModified")
              : org.apache.commons.lang.StringUtils.EMPTY;
      item.setUrl(url);
      item.setTitle(title);
      item.setLastModified(lastModified);
      item.setAssetPreviewImage(previewAssetImage);
      list.add(item);
    }
  }
  private String getMetdataValue(Asset asset) {
    Node node = asset.adaptTo(Node.class);
    String title = StringUtils.EMPTY;
    try {
      if (null != node){
        node = node.getNode("jcr:content/metadata");
        if(node.hasProperty("dc:title")) {
          title = node.getProperty("dc:title").getString();
        }
      }
    } catch (PathNotFoundException e) {
        LOGGER.error("Exception occured while getting title from asset metadata : PathNotFound");
    } catch (RepositoryException e) {
        LOGGER.error("Exception occured while getting title from asset metadata  : RepositoryException");
    }
    return title.length()>40 ? title.substring(0,39)+"..." : title;
  }
  private interface DelegationExclusion {

    String getAssetItems();
  }

  private enum OrderByAsset {
    TITLE("title"),

    MODIFIED("modified");

    private final String value;

    OrderByAsset(String value) {
      this.value = value;
    }

    public static OrderByAsset fromString(String value) {
      for (OrderByAsset s : values()) {
        if (StringUtils.equals(value, s.value)) {
          return s;
        }
      }
      return null;
    }
  }

  private enum AssetSortOrder {
    ASC("asc"),

    DESC("desc");

    private final String value;

    AssetSortOrder(String value) {
      this.value = value;
    }

    public static AssetSortOrder fromString(String value) {
      for (AssetSortOrder s : values()) {
        if (StringUtils.equals(value, s.value)) {
          return s;
        }
      }
      return ASC;
    }
  }

  private static class ListSort implements Comparator<AssetListItem> {

    @Nullable private final AssetSortOrder sortOrder;

    @NotNull private final Comparator<AssetListItem> assetComparator;

    ListSort(
        @Nullable final OrderByAsset orderBy,
        @NotNull final AssetSortOrder sortOrder,
        @NotNull Locale locale) {
      this.sortOrder = sortOrder;

      if (orderBy == OrderByAsset.MODIFIED) {

        this.assetComparator =
            (a, b) -> ObjectUtils.compare(a.getLastModified(), b.getLastModified(), true);
      } else if (orderBy == OrderByAsset.TITLE) {
        Collator collator = Collator.getInstance(locale);
        collator.setStrength(Collator.PRIMARY);

        Comparator<String> titleComparator = Comparator.nullsLast(collator);
        this.assetComparator = (a, b) -> titleComparator.compare(a.getTitle(), b.getTitle());
      } else {
        this.assetComparator = (a, b) -> 0;
      }
    }

    @Override
    public int compare(@NotNull final AssetListItem item1, @NotNull final AssetListItem item2) {
      int i = this.assetComparator.compare(item1, item2);
      if (sortOrder == AssetSortOrder.DESC) {
        i = i * -1;
      }
      return i;
    }
  }
}
