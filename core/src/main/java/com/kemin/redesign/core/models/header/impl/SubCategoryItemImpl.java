package com.kemin.redesign.core.models.header.impl;

import com.kemin.redesign.core.models.header.CategoryItem;
import com.kemin.redesign.core.models.header.SubCategoryItem;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.Collection;

/** Customer Portal Header Child Resource */
@Model(
    adaptables = Resource.class,
    adapters = SubCategoryItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubCategoryItemImpl implements SubCategoryItem {

  @ValueMapValue @Getter private String linkLabel;

  @ValueMapValue @Getter private String link;

  @Override
  public void setLinkLabel(String title) {
    this.linkLabel = title;
  }

}
