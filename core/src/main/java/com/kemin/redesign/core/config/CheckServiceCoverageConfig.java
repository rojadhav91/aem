package com.dish.dopweb.core.configs;

/**
 * Configuration for Check Service Coverage
 */
public interface CheckServiceCoverageConfig {

    /**
     * Check Service Coverage Endpoint
     * @return endpoint
     */
    String getCheckServiceCoverageEndpoint();
}
