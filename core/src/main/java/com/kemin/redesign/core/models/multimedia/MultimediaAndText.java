package com.kemin.redesign.core.models.multimedia;

import com.kemin.redesign.core.models.video.Video;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface MultimediaAndText extends Video {
	
	String getMediaType();
	int getMediaColumnCount();
	int getTextColumnCount();
	String getLayout();
	String getBackgroundColor();
	
	String getImageSrc();
	String getImageAlt();
	
	String getTitle();
	String getDescription();
	String getFirstButtonText();
	String getFirstButtonLink();
	String getFirstButtonStyle();
	String getSecondButtonText();
	String getSecondButtonLink();
	String getSecondButtonStyle();   
}
