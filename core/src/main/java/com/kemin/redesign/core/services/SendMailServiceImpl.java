package com.kemin.redesign.core.services;

import com.adobe.acs.commons.email.EmailService;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Service implementation for sending email notifications.
 */
@Component(service = SendMailService.class, immediate = true )
public class SendMailServiceImpl implements SendMailService {

  private static Logger log = LoggerFactory.getLogger(SendMailServiceImpl.class);
  @Reference
  private EmailService emailService;

  /**
   * @param templatePath templatePath
   * @param emailParams emailParams
   * @param recipients recipients
   * @param resolver resolver
   */
  @Override
  public List<String> sendEmail(final String templatePath, final Map<String, String> emailParams,
      final String[] recipients, final ResourceResolver resolver) {

    // emailService.sendEmail(..) returns a list of all the recipients that could not be sent the email
    // An empty list indicates 100% success
    log.debug("emailService {},templatePath {},emailparams {},recipients {}",emailService,templatePath,emailParams,recipients);
    List<String> failureList = emailService.sendEmail(templatePath, emailParams, recipients);
    return failureList;
  }
}
