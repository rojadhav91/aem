package com.dish.dopweb.core.models.internal.v1;

import com.dish.dopweb.core.configs.CFMarketsConfigService;
import com.dish.dopweb.core.models.LeadgenGenesis;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.*;

/**
 * Sling model implementation for the Leadgen Genesis.
 */
@Model(
        adaptables = Resource.class,
        adapters = LeadgenGenesis.class,
        resourceType = LeadgenGenesisImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Slf4j
public class LeadgenGenesisImpl implements LeadgenGenesis {
    public static final String RESOURCE_TYPE = "dopweb/components/commerce/productteaser";
    private static final String NO_ZIPCODES_FOUND = "No zipcodes found";
    public static final String DIVUUID = "divuuid";
    private final transient Logger logger = LoggerFactory.getLogger(LeadgenGenesisImpl.class);
    
    @OSGiService
    private CFMarketsConfigService cfMarketsConfigService;

    @ValueMapValue
    private String successDiv;

    @ChildResource
    private Resource errorDivSelector;

    final private List<String> errorCodes = new ArrayList<>();
    final private List<String> uuidList = new ArrayList<>();

    /**
     * Post construct method
     */
    @SuppressWarnings("PMD.IdenticalCatchBranches")
    @PostConstruct
    public void init() {
        if (errorDivSelector != null) {
            final Iterator<Resource> resourceIterator = errorDivSelector.listChildren();
            resourceIterator.forEachRemaining(resource -> {
                final String errorCode = resource.getValueMap().get("errorCode", String.class);
                if (StringUtils.isNotBlank(errorCode)) {
                    errorCodes.add(errorCode);
                    final ResourceResolver resourceResolver = resource.getResourceResolver();
                    final Node node = resource.adaptTo(Node.class);
                    try {
                        node.setProperty(DIVUUID, errorCode.toUpperCase(Locale.getDefault()));
                        resourceResolver.commit();
                    } catch (RepositoryException e) {
                        logger.error("Unable to set property of node :: {} ", e);
                    } catch (PersistenceException e) {
                        logger.error("Unable to set property of node :: {} ", e);
                    }
                }
                final String divuuid = resource.getValueMap().get(DIVUUID, String.class);
                if (StringUtils.isNotBlank(divuuid)) {
                    uuidList.add(divuuid);
                }
            });
                deleteDivLeadgen(errorDivSelector);
        }
    }

    @SuppressWarnings({"PMD.AvoidCatchingGenericException", "PMD.AvoidDeeplyNestedIfStmts"})
    private void deleteDivLeadgen(final Resource resource) {
        if (resource != null){
            try {
                final Iterator<Resource> resourceIterator = Optional.ofNullable(resource)
                        .map(Resource::getParent)
                        .map(Resource::listChildren)
                        .orElse(Collections.emptyIterator());

                resourceIterator.forEachRemaining(resource1 -> {
                    if (resource1.getName().contains("genesis-leadgenform-")) {
                        final String replacedString = resource1.getName().replace("genesis-leadgenform-", "");
                        if (!uuidList.isEmpty() && !uuidList.contains(replacedString)) {
                            final Node node = resource1.adaptTo(Node.class);
                            if (node != null) {
                                final ResourceResolver resourceResolver = resource1.getResourceResolver();
                                try {
                                    node.remove();
                                } catch (RepositoryException e) {
                                    logger.error("Deleting node RepositoryException :: {} ", e.getMessage());
                                }
                                try {
                                    resourceResolver.commit();
                                } catch (PersistenceException e) {
                                    logger.error("Deleting node PersistenceException :: {} ", e.getMessage());
                                }
                            }
                        }
                    }
                });
            }catch (Exception e){
                logger.error("Error while deleting the div leadgen {} ", e.getMessage());
            }
        }
    }

    /**
     * Method to get UUID
     */
    @Override
    public String getUuid() {
        return UUID.randomUUID().toString();
    }

    /**
     * Method to get SuccessDiv
     */
    @Override
    public String getSuccessDiv() {
        return successDiv;
    }

    /**
     * Method to get ErrorDivNameList
     */
    @Override
    public List<String> getErrorDivNameList() {
        return Collections.unmodifiableList(errorCodes);
    }

    @SuppressWarnings("PMD.OptimizableToArrayCall")
    @Override
    public String[] getUuidArr() {
        return uuidList.toArray(new String[uuidList.size()]);
    }

    @Override
    public String getZipcodes() {
        String response = NO_ZIPCODES_FOUND;
        final List<String> zipcodes = cfMarketsConfigService.getZipcodes();
        if (!zipcodes.isEmpty()) {
            response = new Gson().toJson(zipcodes);
        }
        
        return response;
    }
}
