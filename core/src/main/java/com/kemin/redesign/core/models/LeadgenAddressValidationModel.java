package com.dish.dopweb.core.models;

import com.dish.dopweb.core.configs.SmartyStreetConfig;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

/**
 * Sling model to consume Smarty Street Service
 */
@Model(adaptables = Resource.class)
public class LeadgenAddressValidationModel {
    @OSGiService
    private SmartyStreetConfig smartyStreetConfig;

    /**
     * Retrieve api key value from service and return to leadgen form
     * @return
     */
    public String getApiKey(){
        return smartyStreetConfig.getApiKey();
    }
}
