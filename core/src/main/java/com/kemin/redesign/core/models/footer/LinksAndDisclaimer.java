package com.kemin.redesign.core.models.footer;

import java.util.Collection;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface LinksAndDisclaimer extends ComponentExporter  {
	String getDisclaimer();
	Collection<LinkItem> getLinks();
	String getLogo();
	String getLogoLink();
}
