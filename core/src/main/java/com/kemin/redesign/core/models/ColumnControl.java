package com.aembootcamp.core.models;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class,
resourceType = ColumnControl.RESOURCE_TYPE,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
		)
public class ColumnControl {

	
	protected static final String RESOURCE_TYPE="aem-bbotcamp/components/general/column-control";
	
	private List<String> columns;
	
	
	@ValueMapValue(name="layout")
	private String layout;
	

	@PostConstruct
	public void initColumnControl() {
		columns = new ArrayList<String>();	
		if( layout != null) {
			int columnSize = 12 / Integer.parseInt(layout);
			 for (int i = 0; i < columnSize; i++) {
	               columns.add("col-md-"+layout);
	            }		
		}
		}
	
	
	public String getLayout() {
		return layout;
	}

	public List<String> getColumns() {
		return columns;
	}
}
