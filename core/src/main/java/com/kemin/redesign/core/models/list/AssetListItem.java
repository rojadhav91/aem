package com.kemin.redesign.core.models.list;

import lombok.Data;

@Data
public class AssetListItem {

  private String title;
  private String url;
  private String lastModified;
  private String assetPreviewImage;

  public String getURL() {
    return url;
  }
}
