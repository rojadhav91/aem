package com.aembootcamp.core.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.dam.api.Asset;
import com.day.cq.tagging.JcrTagManagerFactory;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scene7.ipsapi.StringArray;

@Model(adaptables = SlingHttpServletRequest.class, resourceType = GuideDetails.resourceType, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GuideDetails {
	protected static final String resourceType = "aem-bootcamp/components/general/guide-detail";
	private static final Logger LOGGER = Logger.getLogger(GuideDetails.class);
	@ValueMapValue(name = "title")
	private String title;

	@ValueMapValue(name = "date")
	private String date;

	@ValueMapValue(name = "description")
	private String description;
	
	@ValueMapValue(name = "category")
	private String category;

	private String lastModified;
	
	@ScriptVariable
    Page currentPage;
	
	@SlingObject
	ResourceResolver resourceResolver;
	
	@Self
	SlingHttpServletRequest request;
	
	public String getCategory() {
		return category;
	}
	public String getLastModified() {
		SimpleDateFormat formDate = new SimpleDateFormat("MMM dd, yyyy");
		String strDate = formDate.format(new Date());
		return strDate;
	}
	public String getTitle() {
		return title;
	}

	public String getDate() {
		return date;
	}

	public String getDescription() {
		return description;
	}

	@PostConstruct
	public void initGuide() {
		List<String> tagsList = new ArrayList <String>();
		ValueMap pageProperties = currentPage.getProperties();
		String[] socialMedia = pageProperties.get("category", String[].class);
		TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
		for(String str:socialMedia) {	
			Tag tag=tagManager.resolve(str);
			tagsList.add(tag.getTitle());
		}
		category=tagsList.toString().replaceAll("[\\[\\]\\(\\)]", "");
	}
}