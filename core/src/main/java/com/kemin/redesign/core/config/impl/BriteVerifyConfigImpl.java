package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.BriteVerifyConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;



/**
 * Implementation class for BriteVerify config
 */
@Component(service = BriteVerifyConfig.class, immediate = true)
@Designate(ocd = BriteVerifyConfigImpl.Config.class, factory = false)
@ServiceDescription("A service used to configure Briteverify API")
@ServiceVendor("Dish DOP")
public class BriteVerifyConfigImpl implements BriteVerifyConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String briteVerifyBoostKey;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String briteVerifyGenesisKey;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String briteVerifyEndpoint;

    /**
     * Set BriteVerify configuration
     * @param config
     */
    @Activate
    public void activate(final BriteVerifyConfigImpl.Config config) {

        briteVerifyBoostKey = config.briteVerifyBoostKey();
        briteVerifyGenesisKey = config.briteVerifyGenesisKey();
        briteVerifyEndpoint = config.briteVerifyEndpoint();
    }

    @ObjectClassDefinition(
            name = "Dish DOP BriteVerify Configuration",
            description = "The configuration for validating email entered by customer.")
    public @interface Config {

        @AttributeDefinition(name = "BriteVerify Key", description = "Api Key for Boost BriteVerify")
        String briteVerifyBoostKey() default  "";

        @AttributeDefinition(name = "BriteVerify Key", description = "Api Key for Genesis BriteVerify")
        String briteVerifyGenesisKey() default  "";

        @AttributeDefinition(name = "BriteVerify Endpoint", description = "BriteVerify Api Endpoint")
        String briteVerifyEndpoint() default  "";
    }
}

