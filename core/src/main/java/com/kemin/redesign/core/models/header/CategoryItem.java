package com.kemin.redesign.core.models.header;

import com.day.cq.wcm.api.Page;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Collection;
import java.util.List;

@ConsumerType
public interface CategoryItem {

  String getLinkLabel();

  String getLink();

  String getLinkIcon();

  List<SubCategoryItem> getSubCategoryItem();

  void setLinkLabel(String title);
}
