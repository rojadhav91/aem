package com.dish.dopweb.core.beans;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * User Menu Link Items Sling Model
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkItems {

    @ValueMapValue
    @Getter
    private String linkTitle;

    @ValueMapValue
    @Getter
    private String linkUrl;

    @ValueMapValue
    @Default(values = "alwaysShow")
    @Getter
    private String displayLinksOnAuth;

    @ValueMapValue
    @Default(values = "false")
    @Getter
    private String showMenuSeperator;

    @ValueMapValue
    @Default(values = "false")
    @Getter
    private String showMenuSpacer;

    @ValueMapValue
    @Getter
    private String myaacounticons;

}
