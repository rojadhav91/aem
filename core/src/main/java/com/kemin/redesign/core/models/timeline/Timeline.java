package com.kemin.redesign.core.models.timeline;

import java.util.Collection;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface Timeline extends ComponentExporter  {
	Collection<Milestone> getMilestones();
	String getHeading();
}
