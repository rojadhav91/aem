package com.aembootcamp.core.services;
import java.lang.annotation.Annotation;
import javax.servlet.Servlet;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import javassist.bytecode.ConstantAttribute;

@Component(service = OSGIConfig.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ServiceConfigINT.class)
public class AemCustomService implements OSGIConfig {

	private String configValue;
	private String[] users;

	@Activate
	public void activate(ServiceConfigINT serviceConfigINT) {
		configValue = serviceConfigINT.getConfigValue();
		users = serviceConfigINT.getUsers();
	}

	@Override
	public String[] getUsers() {
		return users;
	}

	@Override
	public String getConfigValue() {
		return configValue;
	}

}
