package com.kemin.redesign.core.models.footer.impl;

import com.kemin.redesign.core.models.footer.LinkItem;
import com.kemin.redesign.core.models.social.SocialLinkItem;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
    adaptables = Resource.class,
    adapters = LinkItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkItemImpl implements LinkItem {

  @ValueMapValue @Getter private String url;

  @ValueMapValue @Getter private String title;
}
