/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.kemin.redesign.core.models.coveo;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.kemin.redesign.core.services.CoveoConfigService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.*;

import static org.apache.sling.api.resource.ResourceResolver.PROPERTY_RESOURCE_TYPE;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class})
public class CoveoGlobalSearchBarModel {

    @ValueMapValue(name=PROPERTY_RESOURCE_TYPE, injectionStrategy=InjectionStrategy.OPTIONAL)
    @Default(values="No resourceType")
    protected String resourceType;

    @SlingObject
    private Resource currentResource;
    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private CoveoConfigService configuration;

    @Inject
    private Page currentPage;

    private String popularQueries;

    private String language;

    private String region;
    
    private String locale;

    private String searchPagePath;

    public static String EN = "en";
    public static String EN_US = "en-us";
    public static String NA = "na";
    
    @PostConstruct
    protected void init() {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        String currentPagePath = Optional.ofNullable(pageManager)
                .map(pm -> pm.getContainingPage(currentResource))
                .map(Page::getPath).orElse("");
        if(currentPagePath!=null && currentPage!=null && currentPagePath.startsWith("/content/experience-fragments")){
            popularQueries = "yes";
            currentPagePath = currentPage.getPath();
        }

        try {
        	Pattern p = Pattern.compile("/(\\w{2})/(\\w{2}(?:-\\w{2}){0,1})/");
			Matcher m = p.matcher(currentPagePath);
			if (m.find()) {
				region = m.group(1);
				locale = m.group(2);
				
				if(Pattern.compile("-").matcher(locale).find()) {
					language = locale.split("-")[0];
				} else {
					language = locale;
				}
                if(StringUtils.isBlank(language)) {
                    language = EN;
                }
                if(StringUtils.isBlank(locale)) {
                    locale = EN_US;
                }
                if(StringUtils.isBlank(region)) {
                    region = NA;
                }
        
			}
            
            Pattern p2 = Pattern.compile("((?:.*)/(?:\\w{2})/(?:\\w{2}(?:-\\w{2}){0,1})/)");
            Matcher m2 = p2.matcher(currentPagePath);
			if (m2.find()) {
				String homepath = m2.group(1);
                searchPagePath = homepath + this.configuration.getSearchPageUrl();
			} else {
                Pattern p3 = Pattern.compile("((?:.*)/(?:\\w{2})/)");
                Matcher m3 = p3.matcher(currentPagePath);    
                if (m3.find()) {
                    String homepath = m3.group(1);
                    searchPagePath = homepath + this.configuration.getSearchPageUrl();
                }
            }

        } catch(Exception e) {
            searchPagePath = this.configuration.getSearchPageUrl();
        }
    }

    public String getLocale(){
        return locale;
    }

    public String getLanguage() {
        return language;
    }

    public String getRegion() {
        return region;
    }

    public String getSearchPageUrl() {
        return searchPagePath;
    }

    public String getSearchHub() {
        return this.configuration.getSearchHub();
    }

    public String getPopularQueries(){return popularQueries;}

}
