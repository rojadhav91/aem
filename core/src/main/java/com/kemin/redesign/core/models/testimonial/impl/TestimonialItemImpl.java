package com.kemin.redesign.core.models.testimonial.impl;

import com.kemin.redesign.core.models.testimonial.TestimonialItem;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(
        adaptables = Resource.class,
        adapters = TestimonialItem.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TestimonialItemImpl implements TestimonialItem {

    @ValueMapValue @Getter private String quote;

    @ValueMapValue @Getter private String authorInfo;

    @ValueMapValue @Getter private String backgroundImageDesktop;

    @ValueMapValue @Getter private String backgroundImageMobile;

    @Getter @Setter private String initialCssClass;


}

