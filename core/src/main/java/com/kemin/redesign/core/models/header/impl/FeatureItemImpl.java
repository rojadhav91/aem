package com.kemin.redesign.core.models.header.impl;

import com.kemin.redesign.core.models.header.FeatureItem;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import static com.day.cq.wcm.foundation.List.log;

@Model(
        adaptables = Resource.class,
        adapters = FeatureItem.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

@Slf4j
public class FeatureItemImpl  implements FeatureItem {

    @ValueMapValue @Getter private String featureLink ;
    @ValueMapValue @Getter private String featureTitle;


}
