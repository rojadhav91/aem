package com.aembootcamp.core.models;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;

@Model(adaptables = SlingHttpServletRequest.class, resourceType = OpenGraphTaggingModel.resourceType, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class OpenGraphTaggingModel {

	protected static final String resourceType = "aem-bootcamp/components/structure/page-shell";
	private static final Logger LOGGER = Logger.getLogger(OpenGraphTaggingModel.class);

	@SlingObject
	ResourceResolver resorceResolver;

	@ScriptVariable
	Page currentPage;

	@OSGiService
	private Externalizer externalizer;


	@ValueMapValue(name = "title")
	private String title;

	@ValueMapValue(name = "description")
	private String description;

	@ValueMapValue(name = "image")
	private String image;

	@ValueMapValue(name = "type")
	@Default(values = "website")
	private String type;

	@ValueMapValue(name = "card")
	@Default(values = "summary")
	private String card;

	private String url;

	public String getUrl() {
		return url;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		if(description == null) {
			description=currentPage.getDescription();
		}
		return description;
	}

	public String getImage() {
		return image;
	}

	public String getType() {
		return type;
	}

	public String getCard() {
		return card;
	}

	@PostConstruct
	public void intiTag() {

		url = intiExternalize(currentPage.getPath());
		image = intiExternalize(image).replace(".html", "");
		if (title == null) {
			title = currentPage.getTitle();
		}
	}

	public String intiExternalize(String path) {
		String myExternalizedUrl = externalizer.publishLink(resorceResolver, path) + ".html";
		return myExternalizedUrl;
	}
}