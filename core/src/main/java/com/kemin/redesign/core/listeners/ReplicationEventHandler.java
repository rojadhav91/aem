package com.kemin.redesign.core.listeners;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.replication.dispatcher.DispatcherFlusher;
import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationEvent;
import com.day.cq.replication.ReplicationException;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.utils.CommonUtils;

@Component(service = EventHandler.class, immediate = true, property = {
Constants.SERVICE_DESCRIPTION + "=Kemin Redesign - Sitemap Flush Event Handler",
EventConstants.EVENT_TOPIC + "=" + ReplicationEvent.EVENT_TOPIC })
public class ReplicationEventHandler implements EventHandler {
	private static final String SLASH = "/";
	private static final String DOUBLE_SLASH = "//";
	private static final String XML_SITEMAP_SFX = ".sitemap.xml";
	private static final String HTML_SITEMAP_SFX = "/home/sitemap.html";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	private SlingSettingsService slingSettingsService;

	@Reference
	private DispatcherFlusher dispatcherFlusher;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Override
	public void handleEvent(Event event) {
		log.info("Replication action :: {} ", ReplicationEvent.fromEvent(event).getReplicationAction());
		try (ResourceResolver resourceResolver = CommonUtils.getResourceResolver(resourceResolverFactory, ApplicationConstants.KEMIN_REDESIGN_REPO_READ)) {
			handleEvent(resourceResolver, event);
		} catch (LoginException e) {
			log.error("Could not get a service resolver", e);
		}
	}

	public void handleEvent(ResourceResolver resolver, Event event) {
		log.debug("Invoked on publish instance");
		if (resolver != null && event != null && dispatcherFlusher != null) {

			log.debug("Invoked on publish instance - executed");

			final ReplicationAction action = ReplicationEvent.fromEvent(event).getReplicationAction();
			final ReplicationActionType replicationActionType = action.getType();
			final String[] paths = action.getPaths();
			log.debug("Invoked on publish instance - replicationActionType:" + replicationActionType);

			if (ReplicationActionType.ACTIVATE.equals(replicationActionType)
					|| ReplicationActionType.DEACTIVATE.equals(replicationActionType)
					|| ReplicationActionType.DELETE.equals(replicationActionType)) {
				flushSiteMap(paths, resolver, replicationActionType);
			}
		}
	}

	private void flushSiteMap(String[] paths, ResourceResolver resolver, ReplicationActionType replicationActionType) {
		log.debug("Invoked on publish instance - deleteOnPublish executed");
		for (String path : paths) {
			log.debug("Invoked on publish instance - deleteOnPublish - path: {}", path);
			if (path.startsWith(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH)) {
				String xmlSiteMapPath = xmlSiteMapPath(path, ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + SLASH);
				if (StringUtils.isNotEmpty(xmlSiteMapPath)) {
					try {
						dispatcherFlusher.flush(resolver, replicationActionType, false, xmlSiteMapPath);
						log.info("flushed xml sitemap path: {}", xmlSiteMapPath);
					} catch (ReplicationException e) {
						log.error(e.getMessage(), e);
					}
				}
				
		        String htmlSiteMapPath = htmlSiteMapPath(path, ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + SLASH);
		        if (StringUtils.isNotEmpty(htmlSiteMapPath)) {
		            try {
		                dispatcherFlusher.flush(resolver, replicationActionType, false, htmlSiteMapPath);
		                log.info("flushed html sitemap path: {}", htmlSiteMapPath);
		            }
		            catch (ReplicationException e) {
		                log.error(e.getMessage(), e);
		            }
		        }
			}
		}
	}

	private String xmlSiteMapPath(String path, String root) {
		if (StringUtils.isNotEmpty(path)) {
			String[] parts = path.replace(root, StringUtils.EMPTY).split(SLASH);
			log.debug("parts:" + Arrays.toString(parts));
			if (parts.length > 1 && !parts[0].equals("language-masters")) {
				// build xml sitemap path
				return (root + SLASH + parts[0] + SLASH + parts[1] + XML_SITEMAP_SFX).replace(DOUBLE_SLASH, SLASH);
			}
		}
		return StringUtils.EMPTY;
	}
	
    private String htmlSiteMapPath(String path, String root) {
        if (StringUtils.isNotEmpty(path) && path.startsWith(root)) {
            String[] parts = path.replace(root, "").split(SLASH);
            if (parts.length > 1 && !parts[0].equals("language-masters")) {
                // build xml sitemap path
                return root + parts[0] + SLASH + parts[1] + HTML_SITEMAP_SFX;
            }
        }
        return StringUtils.EMPTY;
    }
}