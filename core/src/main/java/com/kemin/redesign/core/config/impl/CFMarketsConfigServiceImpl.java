package com.dish.dopweb.core.configs.impl;

import com.day.crx.JcrConstants;
import com.dish.dopweb.core.configs.CFMarketsConfigService;
import com.dish.dopweb.core.models.ContentFragmentMarketsModel;
import com.dish.dopweb.core.utils.ResourceResolverUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * CF Markets Config Service.
 */
@Component(service = CFMarketsConfigService.class, immediate = true)
@Designate(ocd = CFMarketsConfigServiceImpl.Config.class)
public class CFMarketsConfigServiceImpl implements CFMarketsConfigService {
    
    private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
    private static final String ACTIVATE_REPLICATION_ACTION = "Activate";
    private static final String ACTIVE = "active";
    
    private String cfMarketsDAMPath;
    
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    private SlingSettingsService slingSettingsService;
    
    /**
     * Activate Service Config.
     *
     * @param config CFMarketsConfigService
     */
    @Activate
    @Modified
    public void activate(final CFMarketsConfigServiceImpl.Config config) {
        cfMarketsDAMPath = config.cfMarketsDAMPath();
    }
    
    /**
     * Validates if zipcode is valid based on cf active markets.
     *
     * @param zipcode zipcode to evaluate
     * @return true/false if zipcode is valid
     */
    @Override
    public boolean isValidZipcode(final String zipcode) {
        return getZipcodesList().contains(zipcode);
    }
    
    /**
     * Returns list of all cf active markets zipcodes.
     *
     * @return list of zipcodes
     */
    @Override
    public List<String> getZipcodes() {
        return Collections.unmodifiableList(getZipcodesList());
    }
    
    private List<String> getZipcodesList() {
        final List<String> zipcodes = new ArrayList<>();
        try (ResourceResolver resourceResolver = ResourceResolverUtil.getServiceResourceResolver(resourceResolverFactory, APP_RESOURCE_SERVICE)) {
            final List<ContentFragmentMarketsModel> activeCFMarkets = getActiveCFMarkets(resourceResolver);
            for (final ContentFragmentMarketsModel activeCFMarket : activeCFMarkets) {
                final String zipcodesStringValue = activeCFMarket.getZipcodes();
                final JsonArray jsonArray = new Gson().fromJson(zipcodesStringValue, JsonArray.class);
                if (Objects.nonNull(jsonArray)) {
                    jsonArray.forEach(zipcode -> zipcodes.add(zipcode.getAsString()));
                }
            }
        }
        
        return zipcodes;
    }
    
    @SuppressWarnings("PMD.AvoidDeeplyNestedIfStmts")
    private List<ContentFragmentMarketsModel> getActiveCFMarkets(final ResourceResolver resolver) {
        final List<ContentFragmentMarketsModel> activeCFMarkets = new ArrayList<>();
        
        if (StringUtils.isNotBlank(cfMarketsDAMPath)) {
            final Iterator<Resource> marketsIterator = Optional.ofNullable(resolver.getResource(cfMarketsDAMPath))
                    .map(Resource::getChildren)
                    .map(Iterable::iterator)
                    .orElse(null);
            
              if (marketsIterator != null) {
                  while (marketsIterator.hasNext()) {
                      final Resource marketResource = marketsIterator.next();
                      if (JcrConstants.JCR_CONTENT.equals(marketResource.getName())) {
                          continue;
                      }
                      if (isAuthor(slingSettingsService)) {
                          final String replicateProperty = Optional.ofNullable(marketResource.getChild(JcrConstants.JCR_CONTENT))
                                  .map(contentResource -> contentResource.adaptTo(ValueMap.class))
                                  .map(valueMap -> valueMap.getOrDefault("cq:lastReplicationAction", String.class))
                                  .map(Object::toString)
                                  .orElse(StringUtils.EMPTY);
                          
                          if (!ACTIVATE_REPLICATION_ACTION.equals(replicateProperty)) {
                              continue;
                          }
                      }
                      final ContentFragmentMarketsModel contentFragmentMarket = marketResource.adaptTo(ContentFragmentMarketsModel.class);
                      if (Objects.nonNull(contentFragmentMarket) && ACTIVE.equals(contentFragmentMarket.getStatus())) {
                          activeCFMarkets.add(contentFragmentMarket);
                      }
                  }
              }
        }
        
        return activeCFMarkets;
    }
    
    private boolean isAuthor(final SlingSettingsService slingSettingsService) {
        return slingSettingsService.getRunModes().contains("author");
    }
    
    @ObjectClassDefinition(name = "Dish DOP CF Markets Configuration", description = "The configuration for accessing dam cf markets data.")
    public @interface Config {
        
        @AttributeDefinition(name = "DAM CF Markets Path", description = "DAM Path for CF Markets")
        String cfMarketsDAMPath() default "";
    }
}
