package com.kemin.redesign.core.models.product.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.kemin.redesign.core.models.product.ProductCatalog;
import com.kemin.redesign.core.services.ProductPageSearchService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {ProductCatalog.class, ComponentExporter.class},
        resourceType = ProductCatalogImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
public class ProductCatalogImpl implements ProductCatalog {
    public static final String RESOURCE_TYPE = "kemin-redesign/components/product-catalog";
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @ValueMapValue
    @Getter
    protected String path;

    @ValueMapValue
    @Getter
    protected String[] productApplication;

    @ScriptVariable
    Page currentPage;
    @SlingObject
    Resource resource;
    @Inject
    SlingHttpServletRequest request;
    /**
     * The blog pages list.
     */
    List<TierTagList> tierTagList;
    String allTagsToPageJSON, productItemListJSON;
    @OSGiService
    ProductPageSearchService productPageSearchService;

    @PostConstruct
    public void init() throws JSONException, PersistenceException {
        WCMMode mode = WCMMode.fromRequest(request);
        if (StringUtils.isEmpty(path) && mode.name().equals("EDIT")) {
            path = getDefaultPath(currentPage.getPath());
            ModifiableValueMap map = resource.adaptTo(ModifiableValueMap.class);
            map.put("path", path);
            resource.getResourceResolver().commit();
        }
        productItemListJSON = productPageSearchService.getProductItemListJSON(path, productApplication);
        tierTagList = productPageSearchService.getTierTagList();
        allTagsToPageJSON = productPageSearchService.getAllTagsToPageJSON();
    }

    @Override
    public String getProductItemListJSON() {
        return productItemListJSON;
    }

    @Override
    public String getAllTagsToPageJSON() {
        return allTagsToPageJSON;
    }

    @Override
    public List<TierTagList> getTierTagList() {
        return tierTagList;
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    public String getDefaultPath(String path) {
        String defaultPath = "";
        String[] pathArray = path.split("/");
        for (int i = 0; i < pathArray.length; i++) {
            if (i <= 4) {
                defaultPath += pathArray[i] + "/";
            } else
                break;
        }
        return defaultPath + "home";
    }
}
