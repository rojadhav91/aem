package com.dish.dopweb.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class will return the host name when passed the page url.
 */
final public class HostnameUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HostnameUtil.class);
    public static final String CONNECTOR = "://";

    private HostnameUtil() {
    }

    /**
     * This method will return hostname
     * @param url
     * @return http://localhost:4502
     */
    public static String getHostName(final String url){
        String hostName = null;
        try {
            final URL urlObj = new URL(url);
            hostName = urlObj.getProtocol() + CONNECTOR + urlObj.getAuthority();
        } catch (MalformedURLException e) {
            LOGGER.error("Exception while returning hostname :: {} ", e);
        }
        return hostName;
    }
}
