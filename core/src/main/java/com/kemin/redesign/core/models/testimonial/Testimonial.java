package com.kemin.redesign.core.models.testimonial;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;
import java.util.Collection;

@ConsumerType
public interface Testimonial extends ComponentExporter {

    String getCtaName();

    String getCtaLink();

    Collection<TestimonialItem> getTestimonialItems();
}
