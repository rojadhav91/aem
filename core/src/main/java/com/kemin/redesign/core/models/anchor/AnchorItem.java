package com.kemin.redesign.core.models.anchor;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface AnchorItem {
    public String getId();
    public String getTitle();
}
