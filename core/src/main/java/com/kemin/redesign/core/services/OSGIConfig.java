package com.aembootcamp.core.services;
public interface OSGIConfig {

	String[] getUsers();
	String getConfigValue();
}
