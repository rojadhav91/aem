package com.kemin.redesign.core.models.experiencefragment.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.query.Query;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.experiencefragment.ExperienceFragment;

@Model(adaptables = SlingHttpServletRequest.class, adapters = ExperienceFragment.class, resourceType = ExperienceFragmentImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class ExperienceFragmentImpl implements ExperienceFragment {

	public static final Logger LOGGER = LoggerFactory.getLogger(ExperienceFragmentImpl.class);

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/experiencefragment";

	private static final String LANGUAGE_PATH_QUERY = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE(["
			+ ApplicationConstants.KEMIN_REDESIGN_XF_PATH+"/"+ApplicationConstants.LANGUAGE_MASTER+"]) ";

	@Inject
	private Page currentPage;

	@Inject
	private ResourceResolver resolver;

	private String fragmentPath;

	@ValueMapValue
	private String fragmentVariationPath;

	@ValueMapValue @Getter
	private String chatPath;

	@Override
	public String getFragmentPath() {
		return fragmentPath;
	}

	@Override
	public boolean isTemplateView() {
		return !currentPage.getProperties().containsKey("stickyEmailEnabled");
	}

	@PostConstruct
	protected void init() {
		fragmentPath = getFragmentPath(currentPage, resolver);
	}

	/**
	 * get the localised fragment path based on the page properties
	 * 
	 * @return fragmentPath
	 */
	private String getFragmentPath(Page page, ResourceResolver resolver) {
		String fragPath = null;
		if (StringUtils.isNotBlank(fragmentVariationPath)) {
			fragPath = getLocalisedFragmentPath(fragmentVariationPath, page, resolver);
			LOGGER.debug("localisedFragmentPath:" + fragPath);

			if (resolver.getResource(fragPath) == null) {
				LOGGER.debug("failed to get resource for fragPath:" + fragPath);
				fragPath = fragmentVariationPath;
			}

		}
		return fragPath;
	}

	private String getLocalisedFragmentPath(String xfPath, Page page, ResourceResolver resolver) {
		String cCode = extractCountryCode(page);
		String lCode = extractLanguageCode(page);
		String rPath = extractNonLocalePath(xfPath);
		String localisedFragmentPath = xfPath;
		if (cCode.equalsIgnoreCase(ApplicationConstants.LANGUAGE_MASTER)) {
			List<String> pathList = findXFPathsByLanguage(lCode, resolver);
			for (String p : pathList) {
				LOGGER.debug("searched path for language master:" + p + "/" + rPath);
				if (resolver.getResource(p + "/" + rPath) != null) {
					localisedFragmentPath = p + "/" + rPath;
					break;
				}
			}
		} else {
			localisedFragmentPath = ApplicationConstants.KEMIN_REDESIGN_XF_PATH + "/" + cCode + "/" + lCode + "/"
					+ rPath;
		}

		return localisedFragmentPath;
	}

	private List<String> findXFPathsByLanguage(String lCode, ResourceResolver resolver) {
		List<String> pathList = new ArrayList<String>();
		String query = LANGUAGE_PATH_QUERY + " and (NAME(s) like '%" + lCode + "%')";
		LOGGER.debug("findXFPathsByLanguage query:" + query);
		Iterator<Resource> itr = resolver.findResources(query, Query.JCR_SQL2);
		while (itr.hasNext()) {
			Resource res = itr.next();
			String path = res.getPath().replace(ApplicationConstants.KEMIN_REDESIGN_XF_PATH+"/","");
			String[] paths = path.split("/");
			if(paths.length>1 && paths[1].startsWith(lCode)){
				pathList.add(res.getPath());
			}
		}
		return pathList;
	}

	private String extractCountryCode(Page page) {
		String cCode = page.getPath().replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + "/", "");
		String[] cCodeArry = cCode.split("/");
		if (cCodeArry.length > 0) {
			cCode = cCodeArry[0];
		}
		return cCode.toLowerCase();
	}

	private String extractLanguageCode(Page page) {
		String lCode = page.getPath().replace(ApplicationConstants.KEMIN_REDESIGN_BASE_PATH + "/", "");
		String[] lCodeArry = lCode.split("/");
		if (lCodeArry.length > 1) {
			lCode = lCodeArry[1];
		}
		return lCode.toLowerCase();
	}

	private String extractNonLocalePath(String xfPath) {
		String rPath = xfPath.replace(ApplicationConstants.KEMIN_REDESIGN_XF_PATH + "/", "");
		String[] rPathArry = rPath.split("/");
		if (rPathArry.length > 2) {
			List<String> partList = new ArrayList<String>();
			for (int i = 2; i < rPathArry.length; i++) {
				partList.add(rPathArry[i]);
			}
			rPath = String.join("/", partList);
		}
		return rPath;
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}

}