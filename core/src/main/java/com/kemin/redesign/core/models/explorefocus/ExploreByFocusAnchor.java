package com.kemin.redesign.core.models.explorefocus;

import java.util.Collection;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface ExploreByFocusAnchor extends ComponentExporter  {
	Collection<LinkItem> getLinks();
}
