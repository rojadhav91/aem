package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Navigation CA Config for XF Path and category name.
 */
@Configuration(
        label = "Sitemap Externalizer Domain Configuration",
        description = "Configure the Sitemap Externalizer Domain.")
public @interface SitemapExternalizerCaConfig {

    /**
     * property for sitemap externalizer domain.
     *
     * @return domain name
     */
    @Property(label = "Sitemap Externalizer Domain")
    String externalizerCaConfigDomain();
}
