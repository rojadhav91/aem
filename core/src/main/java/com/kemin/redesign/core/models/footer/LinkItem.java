package com.kemin.redesign.core.models.footer;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface LinkItem {
	String getUrl();

	String getTitle();
}
