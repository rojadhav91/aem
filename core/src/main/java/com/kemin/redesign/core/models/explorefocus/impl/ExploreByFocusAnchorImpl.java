package com.kemin.redesign.core.models.explorefocus.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.explorefocus.ExploreByFocusAnchor;
import com.kemin.redesign.core.models.explorefocus.LinkItem;

import lombok.Getter;
/** Kemin ExploreByFocusAnchor Sling Model Implementation Class. */
@Model(
	    adaptables = {Resource.class, SlingHttpServletRequest.class},
	    adapters = {ExploreByFocusAnchor.class, ComponentExporter.class},
	    resourceType = ExploreByFocusAnchorImpl.RESOURCE_TYPE,
	    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	@Exporter(
	    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
	    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class ExploreByFocusAnchorImpl implements ExploreByFocusAnchor {

	protected static final String RESOURCE_TYPE = "kemin-redesign/components/explore-focus";

	@ChildResource @Getter private Collection<LinkItem> links;
	
	@PostConstruct
	protected void init() {
		links = CollectionUtils.emptyIfNull(links).stream().filter(item ->StringUtils.isNotBlank(item.getUrl()) && StringUtils.isNotBlank(item.getTitle())).collect(Collectors.toList());
	}

	@Override
	public String getExportedType() {
		return RESOURCE_TYPE;
	}
}
