package com.kemin.redesign.core.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
//This class only blongs to supplier Locator Json array
public class SupplierLocation {

    private String title;
    private String image;
    private String address1;
    private String address2;
    private Coordinate coords;
    private String placeId;
    private String details;
}
