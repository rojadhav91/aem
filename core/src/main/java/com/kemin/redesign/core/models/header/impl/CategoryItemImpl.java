package com.kemin.redesign.core.models.header.impl;

import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.models.header.CategoryItem;
import com.kemin.redesign.core.models.header.SubCategoryItem;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

/** Customer Portal Header Child Resource */
@Model(
    adaptables = Resource.class,
    adapters = CategoryItem.class,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CategoryItemImpl implements CategoryItem {

  @ValueMapValue @Getter  private String linkLabel;

  @ValueMapValue @Getter private String link;

  @ValueMapValue @Getter private String linkIcon;

  @Inject
  private @Getter  List<SubCategoryItem> subCategoryItem;

  @Override
  public void setLinkLabel(String title) {
    this.linkLabel = title;
  }
}
