package com.kemin.redesign.core.services;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.blog.impl.BlogPagesBean;
import com.kemin.redesign.core.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** The Class CatalogPageSearchServiceImpl. */
@Component(service = CatalogPageSearchService.class, immediate = true)
public class CatalogPageSearchServiceImpl implements CatalogPageSearchService {

  /** The logger. */
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  /** The query builder. */
  @Reference private QueryBuilder queryBuilder;

  /** The resource resolver factory. */
  @Reference private ResourceResolverFactory resourceResolverFactory;

  /**
   * Activate.
   *
   * @param config the config
   */
  protected void activate(CatalogPageSearchService config) {
    LOGGER.debug("####CatalogPageSearchService activate method");
  }

  /**
   * Gets the blog pages.
   *
   * @param page the page
   * @param defaultImagePath the default image path
   * @return the blog pages
   */
  @Override
  public List<BlogPagesBean> getBlogPages(Page page, String defaultImagePath) {
    ResourceResolver resourceResolver = null;
    Session session = null;
    List<BlogPagesBean> blogList = new ArrayList<>();
    try {
      resourceResolver =
          CommonUtils.getResourceResolver(
              resourceResolverFactory, ApplicationConstants.KEMIN_REDESIGN_REPO_READ);
      session = resourceResolver.adaptTo(Session.class);
      BlogPagesBean bean;
      Map<String, String> queryParameters =
          prepareQueryMap(
              page.getPath(),
              "cq:page",
              "jcr:content/cq:template",
              ApplicationConstants.KEMIN_REDESIGN_BLOG_PAGE_TEMPLATE);
      LOGGER.debug("going to execute the query of blog page");
      Query query = queryBuilder.createQuery(PredicateGroup.create(queryParameters), session);
      SearchResult result = query.getResult();
      Iterator<Resource> it = result.getResources();
      Page currentPage;
      LOGGER.debug("going to iterate the blog page results for page path:{}", page.getPath());
      while (it.hasNext()) {
        bean = new BlogPagesBean();
        currentPage = it.next().adaptTo(Page.class);
        bean.setTitle(currentPage.getTitle());
        bean.setPath(currentPage.getPath());
        Page p = getParentPage(currentPage);
        if (p != null) {
          bean.setParentPageTitle(p.getTitle());
        }
        bean.setThumbnailImage(getThumbnailImage(currentPage, defaultImagePath));
        blogList.add(bean);
      }

    } catch (LoginException loginException) {
      LOGGER.error("loginException occurred while executing blogpage query");
    } finally {
      if (session != null && session.isLive()) {
        session.logout();
      }
      if (resourceResolver != null && resourceResolver.isLive()) {
        resourceResolver.close();
      }
    }
    return blogList;
  }

  public Page getParentPage(Page page) {
    Page parent = null;
    int depth = page.getDepth();
    for (int count = 0; count < depth - 3; count++) {
      if (page.getParent(count) != null) {
        Template tmp = page.getParent(count).getTemplate();
        if (tmp == null) {
          LOGGER.warn("no template found for page: " + page.getPath());
        } else if (tmp.getPath()
            .equalsIgnoreCase(ApplicationConstants.KEMIN_REDESIGN_BLOG_MARKET_PAGE_TEMPLATE)) {
          parent = page.getParent(count);
          break;
        }
      }
    }
    return parent;
  }

  /**
   * Prepare query map.
   *
   * @param path the path
   * @param type the type
   * @param propertyName the property name
   * @param propertyValue the property value
   * @return the map
   */
  @Override
  public Map<String, String> prepareQueryMap(
      String path, String type, String propertyName, String propertyValue) {
    Map<String, String> queryParameters = new HashMap<>();
    queryParameters.put("path", path);
    queryParameters.put("type", type);
    queryParameters.put("property", propertyName);
    queryParameters.put("property.value", propertyValue);
    queryParameters.put("p.limit", "-1");
    return queryParameters;
  }

  /**
   * Gets the thumbnail image.
   *
   * @param page the page
   * @param defaultImagePath the default image path
   * @return the thumbnail image
   */
  private String getThumbnailImage(Page page, String defaultImagePath) {
    String thumbnailImage = CommonUtils.getThumbnailImage(page);
    if (!StringUtils.isNotBlank(thumbnailImage)) {
      thumbnailImage = defaultImagePath;
    }
    return thumbnailImage;
  }
}
