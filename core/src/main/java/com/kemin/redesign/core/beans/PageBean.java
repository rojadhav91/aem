package com.kemin.redesign.core.beans;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageBean {

	private String title;
	private String URL;
	private String description;
	private String publishedThumbnailImage;
	private String moreLink;
	private boolean hasThumbnailImage;

}