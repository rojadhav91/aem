package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.MagentoAPIConfigService;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Implementation class for Magento config
 */
@Component(service = MagentoAPIConfigService.class, immediate = true)
@Designate(ocd = MagentoAPIConfigServiceImpl.MagentoAPIConfig.class, factory = false)
public class MagentoAPIConfigServiceImpl implements MagentoAPIConfigService {
    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String host;
    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String bearerToken;



    /**
     * This method instatiates base url immediately upon service activated
     * @param magentoAPIConfig
     */
    @Activate
    public void activate(final MagentoAPIConfigServiceImpl.MagentoAPIConfig magentoAPIConfig){
        host = magentoAPIConfig.host();
        bearerToken = magentoAPIConfig.bearerToken();

    }

    @ObjectClassDefinition(
            name = "Dish Magento API Server Configuration",
            description = "Magento API server configuration for Email check")
    public @interface MagentoAPIConfig{
        @AttributeDefinition(
                name = "Magento URL",
                description = "URL to check email exist on server"
        )
        String host() default "";

        @AttributeDefinition(
                name = "Magento Email Exists Bearer Token",
                description = "Magento Email Exists Bearer Token"
        )
        String bearerToken() default "";
    }
}
