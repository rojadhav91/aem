package com.kemin.redesign.core.models.banner.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.kemin.redesign.core.models.banner.Banner;
import com.kemin.redesign.core.models.header.Header;
import com.kemin.redesign.core.models.header.HeaderItem;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import java.util.Collection;
import java.util.UUID;

/** Customer Portal Header Sling Model Implementation Class. */
@Model(
    adaptables = {Resource.class, SlingHttpServletRequest.class},
    adapters = {Banner.class, ComponentExporter.class},
    resourceType = BannerImpl.RESOURCE_TYPE,
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
    name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
    extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class BannerImpl implements Banner {

  protected static final String RESOURCE_TYPE = "kemin-redesign/components/banner";

  @ValueMapValue @Named("fileReference") @Getter private String backgroundImage;

  //@ValueMapValue @Getter private String widthOption;

  private String id="";
  @PostConstruct
  protected void init() {
	  id="banner_"+UUID.randomUUID().toString().substring(24);
  }
  
  @Override
  public String getId() {
    return id;
  }
  
  @Override
  public String getExportedType() {
    return RESOURCE_TYPE;
  }
}
