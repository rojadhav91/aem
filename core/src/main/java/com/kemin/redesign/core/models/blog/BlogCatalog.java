package com.kemin.redesign.core.models.blog;

import com.adobe.cq.export.json.ComponentExporter;
import com.kemin.redesign.core.models.blog.impl.BlogPagesBean;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;


@ConsumerType
public interface BlogCatalog extends ComponentExporter {
    List<BlogPagesBean> getBlogCategories();
    boolean getIsBlogHomePage();
    boolean getIsBlogMarketPage();
    List<BlogPagesBean> getBlogPages();
    String getBlogPagesInJson();
    String currentPageTitle();
}
