package com.kemin.redesign.core.models.navigationmobileref.impl;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.day.cq.wcm.api.Page;
import com.kemin.redesign.core.ApplicationConstants;
import com.kemin.redesign.core.models.navigationmobileref.NavigationMobileRef;
import com.kemin.redesign.core.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

import javax.jcr.query.Query;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = {NavigationMobileRef.class, ComponentExporter.class},
        resourceType = NavigationMobileRefImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(
        name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
public class NavigationMobileRefImpl implements NavigationMobileRef {

    private static final Logger LOGGER = LoggerFactory.getLogger(NavigationMobileRefImpl.class);

    protected static final String RESOURCE_TYPE = "kemin-redesign/components/navigation-mobile-reference";

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    @ValueMapValue
    protected String path;

    @Override
    public List<Page> getNavigationItemList() {
        Resource navigation = getNavigationNode();
        return CommonUtils.prepareNavigationItemList(navigation, resourceResolver);
    }

    @Override
    public String getSocialLinksComponentPath() {
        String navPath = getNavigationPath();
        Resource socialLinkRes = null;
        String socialLinks = "";
        if (StringUtils.isNotBlank(navPath)) {
            Resource navigationRes = resourceResolver.getResource(navPath);
            if (navigationRes != null) {
                socialLinkRes = navigationRes.getChild(ApplicationConstants.SOCIAL_LINKS);
                if (socialLinkRes != null) {
                    socialLinks = socialLinkRes.getPath();
                }
                socialLinks = getSocialLinksPathAuthoredParContent(socialLinkRes, socialLinks, navigationRes);
                LOGGER.debug("socialLinksResourcePath is :{}", socialLinks);
            }
        }
        return socialLinks;
    }

    private String getSocialLinksPathAuthoredParContent(Resource socialLinkRes, String socialLinks, Resource navigationRes) {
        if (socialLinkRes == null && !StringUtils.isNotBlank(socialLinks) && navigationRes.getChild(ApplicationConstants.PAR_CONTENT) != null) {
            Resource parContent = navigationRes.getChild(ApplicationConstants.PAR_CONTENT);
            if (parContent != null) {
                socialLinkRes = parContent.getChild(ApplicationConstants.SOCIAL_LINKS);
                if (socialLinkRes != null) {
                    LOGGER.debug("socialLinksResourcePath is authored under par content");
                    socialLinks = socialLinkRes.getPath();
                }
            }
        }
        return socialLinks;
    }

    @Override
    public String getExportedType() {
        return RESOURCE_TYPE;
    }

    private Resource getNavigationNode() {
        String navPath = getNavigationPath();
        Resource navigationResource = null;
        if (StringUtils.isNotBlank(navPath)) {
            Resource navigationRes = resourceResolver.getResource(navPath);
            if (navigationRes != null) {
                navigationResource = navigationRes.getChild("navigations");
            }
        }
        return navigationResource;
    }

    @Override
    public String getThumbnailURL() {
        return CommonUtils.thumbnail(currentPage);
    }

    private String getNavigationPath() {
        String navPath = "";
        
        if (currentPage.getPath() != null && currentPage.getPath().startsWith("/content")) {
        	if(StringUtils.isNotBlank(path)) {
	            navPath = currentPage.getPath() + ApplicationConstants.PATH_SEPARATOR + ApplicationConstants.JCR_CONTENT + StringUtils.substringAfter(path, ApplicationConstants.JCR_CONTENT);
	            LOGGER.debug("navigation Component path is:{}", navPath);
        	}else {
        		navPath = findNavigation();
        	}
        }
        return navPath;
    }
    
	private String findNavigation() {
		String navPath="";
		String query = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE(["+currentPage.getPath()+"]) and s.[sling:resourceType]='kemin-redesign/components/navigation'";
		LOGGER.debug("findNavigation query:" + query);
		Iterator<Resource> itr = resourceResolver.findResources(query, Query.JCR_SQL2);
		while (itr.hasNext()) {
			navPath = itr.next().getPath();
			break;
		}
		return navPath;
	}
}

