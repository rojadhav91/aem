package com.kemin.redesign.core.models.hero;

import com.adobe.cq.export.json.ComponentExporter;
import org.osgi.annotation.versioning.ConsumerType;

/** Consumer for Hero Component. */
@ConsumerType
public interface Hero extends ComponentExporter {
    String getFileReference();
    String getAltText();
    String getTitle();
    String getButtonName();
    String getButtonLink();

}