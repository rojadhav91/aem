package com.dish.dopweb.core.models;

import com.dish.dopweb.core.beans.LanguageSelectorMenuItems;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import javax.annotation.PostConstruct;
import java.util.Collection;

/**
 * Language Selector Sling Model.
 */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        resourceType = "dopweb/components/structure/languageselector",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LanguageSelector {

    @ChildResource
    @Getter
    private Collection<LanguageSelectorMenuItems> languageSelectorMenuItems;

    @PostConstruct
    protected void init() {
        languageSelectorMenuItems = CollectionUtils.emptyIfNull(languageSelectorMenuItems);
    }

}
