package com.dish.dopweb.core.models;

import com.adobe.cq.wcm.core.components.models.Accordion;
import com.adobe.cq.wcm.core.components.models.ListItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This model is created to return view options and map for icons
 */
@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AccordionIconModel {
    private final transient Logger logger = LoggerFactory.getLogger(AccordionIconModel.class);

    @ValueMapValue
    @Default(values="defaultview")
    private String accordionOptions;

    @ChildResource
    private Resource iconViewItems;

    @Self
    @Via(type = ResourceSuperType.class)
    private Accordion accordion;

    final private List<String> iconList = new ArrayList<>();
    final private Map<String, String> iconMap = new ConcurrentHashMap<>();

     private List<ListItem> items;

    /**
     * Post construct method
     */
    @SuppressWarnings({"PMD.AvoidCatchingGenericException"})
    @PostConstruct
    public void init() {
        try {
            final Iterator<Resource> resourceIterator = Optional.ofNullable(iconViewItems)
                    .map(Resource::listChildren)
                    .orElse(Collections.emptyIterator());
            resourceIterator.forEachRemaining(resource -> {
                final String iconPath = resource.getValueMap().get("fileReference", String.class);
                iconList.add(iconPath);
            });
        }catch (Exception e){
            logger.error("Error while creating icon list :: {} ", e);
        }
    }

    private String getIocn(final int index) {
        String iconpath = StringUtils.EMPTY;
        if (!iconList.isEmpty()){
            iconpath = iconList.get(index);
        }
        return iconpath;
    }


    public String getAccordionOptions() {
        return accordionOptions;
    }

    public List<String> getIconList() {
        return Collections.unmodifiableList(iconList);
    }

    public Map<String, String> getIconMap() {
        if (accordion != null) {
            items = accordion.getItems();
            items.forEach(listItem -> {
                final int index = items.indexOf(listItem);
                iconMap.put(listItem.getName(), getIocn(index));
            });
        }
        return iconMap;
    }
}
