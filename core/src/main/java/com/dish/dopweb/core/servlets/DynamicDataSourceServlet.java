package com.dish.dopweb.core.servlets;


import com.adobe.cq.commerce.common.ValueMapDecorator;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.wcm.api.policies.ContentPolicy;
import com.day.cq.wcm.api.policies.ContentPolicyManager;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/** Servlet to get dynamic dropdown icons information */
@Component(
        service = {Servlet.class},
        property = {
                ServletResolverConstants.SLING_SERVLET_NAME + "=" + "Dynamic Dropdown for form text icons",
                Constants.SERVICE_DESCRIPTION
                        + "="
                        + "Returns Data source with all icons in a specific path",
                ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + "=" + DynamicDataSourceServlet.RESOURCE_TYPE,
        })


public class DynamicDataSourceServlet extends SlingSafeMethodsServlet {

        /** Servlet resource Type constant. */
        protected static final String RESOURCE_TYPE = "/apps/dopweb/components/dropdowns/icons";

        /**  DynamicDataSourceServlet constants. */
        private static final long serialVersionUID = 1L;
        private static final String TAG = DynamicDataSourceServlet.class.getSimpleName();
        private static final Logger LOGGER = LoggerFactory.getLogger(DynamicDataSourceServlet.class);
        //private static final Map<String, String> DATA = new ConcurrentHashMap<>();

        /**
         * Will create a DataSource with dropdown icons information
         *
         * @param request request
         * @param response response
         */
        @SuppressWarnings("PMD.AvoidDeeplyNestedIfStmts")
        @Override
        protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
                try {
                        final Map<String, String> dataMap = new ConcurrentHashMap<>();
                        final ResourceResolver resourceResolver = request.getResourceResolver();
                        final String path = getIconsPath(request, resourceResolver);

                        // Getting resource from specific dam path
                        Resource resource = null;
                        if ( path != null ) {
                                resource = resourceResolver.getResource(path);
                        }
                        if (resource != null) {
                                // Converting resource to a Node
                                final Node currentNode = resource.adaptTo(Node.class);
                                if (currentNode != null) {
                                        final  NodeIterator nodeIterator = currentNode.getNodes();
                                        // Getting node properties
                                        while (nodeIterator.hasNext()) {
                                                final Node node = nodeIterator.nextNode();
                                                final String value = path + "/" + node.getName();

                                                if (node.getPrimaryNodeType().isNodeType("dam:Asset")) {
                                                        dataMap.put(value, node.getName().split("\\.")[0]);
                                                }
                                        }
                                }
                        }
                        // Creating the data source object
                        @SuppressWarnings({"unchecked", "rawtypes"})
                        final DataSource dataSource = new SimpleDataSource(new TransformIterator(dataMap.keySet().iterator(), input -> {
                                String dropValue = (String) input;
                                ValueMap valueMap = new ValueMapDecorator(new HashMap<>());
                                valueMap.put("value", dropValue);
                                valueMap.put("text", dataMap.get(dropValue));
                                return new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", valueMap);
                        }));
                        request.setAttribute(DataSource.class.getName(), dataSource);
                } catch (RepositoryException e) {
                        LOGGER.error("{}: exception occurred: {}", TAG, e.getMessage());
                }
        }

        private String getIconsPath(final SlingHttpServletRequest request, final ResourceResolver resourceResolver) {
                final String textResourcePath = request.getRequestPathInfo().getSuffix();
                Resource textResource = null;
                if (textResourcePath != null) {
                        textResource = resourceResolver.getResource(textResourcePath);
                }

                ContentPolicyManager policyManager = null;
                if (textResource != null) {
                        policyManager = textResource.getResourceResolver().adaptTo(ContentPolicyManager.class);
                }

                ContentPolicy contentPolicy = null;
                if (textResource != null && policyManager != null ) {
                        contentPolicy = policyManager.getPolicy(textResource);
                }

                String path = null;
                if (contentPolicy != null) {
                        path = contentPolicy.getProperties().get("iconPath", String.class);
                }

                return path;
        }
}
