package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Navigation CA Config for XF Path and category name.
 */
@Configuration(
    label = "Navigation Configuration",
    description = "Configure the variation paths for Header, Footer XF, and Clientlibs Categories.")
public @interface NavigationConfig {

  /** order one. */
  int ORDER_ONE = 1;
  /** order two. */
  int ORDER_TWO = 2;
  /** order three. */
  int ORDER_THREE = 3;

  /**
   * property header xf path.
   *
   * @return headerxfpath
   */
  @Property(label = "Header XF Variation Path", order = ORDER_ONE)
  String[] headerXF();

  /**
   * property footer xf path.
   *
   * @return footerxfpath
   */
  @Property(label = "Footer XF Variation Path", order = ORDER_TWO)
  String[] footerXF();

  /**
   * property navigation clientlibs categories.
   *
   * @return navigationCategories
   */
  @Property(label = "Navigation Client Library Category Names", order = ORDER_THREE)
  String[] navigationCategories();
}
