package com.dish.dopweb.core.models;
import com.adobe.cq.commerce.graphql.client.GraphqlResponse;
import com.adobe.cq.commerce.magento.graphql.*;
import com.adobe.cq.commerce.magento.graphql.gson.Error;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
@Model(adaptables = SlingHttpServletRequest.class)
@Slf4j
public class GraphQL {

  @Self SlingHttpServletRequest request;
  public Map<String, String> manufacturerMap() {

    AttributeInput attributeInput =
        new AttributeInput().setAttributeCode("manufacturer").setEntityType("catalog_product");
    QueryQuery query =
        Operations.query(
            queryQuery ->
                queryQuery.customAttributeMetadata(
                    List.of(attributeInput),
                    customAttributeMetadataQuery ->
                        customAttributeMetadataQuery.items(
                            attributeQuery ->
                                attributeQuery.attributeOptions(
                                    attributeOptionQuery ->
                                        attributeOptionQuery.value().label()))));
    GraphqlResponse<Query, Error> response =
        DOPGraphqlClient.create(request).execute(query.toString());
    Optional.ofNullable(response.getErrors())
        .ifPresent(
            errors ->
                errors.forEach(
                    error ->
                        log.error(
                            "An error has occured {} ({})",
                            error.getMessage(),
                            error.getCategory())));
    Map<String, String> manufacturerMap =
        response.getData().getCustomAttributeMetadata().getItems().stream()
            .map(Attribute::getAttributeOptions)
            .flatMap(Collection::stream)
            .collect(Collectors.toMap(AttributeOption::getValue, AttributeOption::getLabel));
    log.error("find me -> {}", manufacturerMap);
    return manufacturerMap;
  }
}