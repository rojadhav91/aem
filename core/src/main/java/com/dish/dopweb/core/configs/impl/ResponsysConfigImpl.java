package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.ResponsysConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
/**
 * Implementation class for Responsys config
 */
@Component(service = ResponsysConfig.class, immediate = true)
@Designate(ocd = ResponsysConfigImpl.Config.class, factory = false)
@ServiceDescription("A service used to configure Responsys API")
@ServiceVendor("Dish DOP")
public class ResponsysConfigImpl implements ResponsysConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String responsysEndpoint;

    /**
     * Set Responsys configuration
     * @param config
     */
    @Activate
    public void activate(final ResponsysConfigImpl.Config config) {

        responsysEndpoint = config.responsysEndpoint();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Responsys Configuration",
            description = "The configuration for storing email and zip entered by customer.")
    public @interface Config {

        @AttributeDefinition(name = "Responsys Endpoint", description = "Responsys Api Endpoint")
        String responsysEndpoint() default  "";
    }
}
