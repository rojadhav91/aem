package com.dish.dopweb.core.models.internal.v1.form;

import com.adobe.cq.export.json.ComponentExporter;
import com.adobe.cq.export.json.ExporterConstants;
import com.dish.dopweb.core.models.TextAttributeModel;
import com.dish.dopweb.core.models.form.Text;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
//import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import javax.annotation.PostConstruct;
//import org.graalvm.compiler.core.common.SuppressFBWarnings;

/** Sling model implementation for the form text component */
//@SuppressFBWarnings(value="NM_SAME_SIMPLE_NAME_AS_SUPERCLASS",
//        justification="The Name have to be the same")
@Model(adaptables = SlingHttpServletRequest.class,
        adapters = {Text.class, ComponentExporter.class},
        resourceType = TextImpl.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME,
        extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@Slf4j
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class TextImpl implements Text {
  public static final String RESOURCE_TYPE = "/apps/dopweb/components/form/text";

  @Delegate
  @Self
  @Via(type = ResourceSuperType.class)
  private com.adobe.cq.wcm.core.components.models.form.Text text;

  @SlingObject private Resource resource;

  @SlingObject private SlingHttpServletRequest slingHttpServletRequest;

  @ValueMapValue private String iconFolderPath;

  @ValueMapValue private String iconPosition;

  TextAttributeModel textAttributeModel;

  @Override
  public String getIcon() {
    return iconFolderPath;
  }

  @Override
  public String getIconPosition() {
    return iconPosition;
  }
  /*
  get file name
   */
  @PostConstruct
  public void init() {
    textAttributeModel = slingHttpServletRequest.adaptTo(TextAttributeModel.class);
  }
  /*
  get file name
   */
  public String getFieldId() {
    String id = "";
    if (StringUtils.isNotEmpty(textAttributeModel.getFieldId())) {
      id = textAttributeModel.getFieldId();
    } else {
      id = text.getId();
    }
    return id;
  }
  /*
  get file name
   */
  public String getRequiredMsg() {
    String requiredMsg = "";
    if (StringUtils.isNotEmpty(textAttributeModel.getRequiredMsg())) {
      requiredMsg = textAttributeModel.getRequiredMsg();
    } else {
      requiredMsg = text.getRequiredMessage();
    }
    return requiredMsg;
  }
/*
get file name
 */
  public String getFieldName() {
    String name = "";
    if (StringUtils.isNotEmpty(textAttributeModel.getFieldName())) {
      name = textAttributeModel.getFieldName();
    } else {
      name = text.getName();
    }
    return name;
  }
  /*
get file name
 */
  public String getPlaceHoldermsg() {
    String placeholder = "";
    if (StringUtils.isNotEmpty(textAttributeModel.getPlaceholderMsg())) {
      placeholder = textAttributeModel.getPlaceholderMsg();
    } else {
      placeholder = text.getPlaceholder();
    }
    return placeholder;
  }
  /*
  get file name
   */
  public boolean getRequireFlag() {
    boolean flag;
    if (textAttributeModel.isRequiredFlag()) {
      flag = textAttributeModel.isRequiredFlag();
    } else {
      flag = text.isRequired();
    }
    return flag;
  }
  /*
  get file name
   */
  public String getLabelTitle() {
    String title = "";
    if (StringUtils.isNotEmpty(textAttributeModel.getLabelTitle())) {
      title = textAttributeModel.getLabelTitle();
    } else {
      title = text.getTitle();
    }
    return title;
  }
}
