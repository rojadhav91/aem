package com.dish.dopweb.core.models;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author imran.padshah
 */
@ConsumerType
@SuppressWarnings("PMD")
public interface CompatibleDevices
{

    /**
     * Returns the list of devices from tags
     * @return devicelist in JSON format
     */
    default String getDevices()
    {
        return null;
    } //getDevices

} //CompatibleDevices
