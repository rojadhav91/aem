package com.dish.dopweb.core.configs.impl;

import com.adobe.acs.commons.util.ParameterUtil;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.PageManager;
import com.dish.dopweb.core.configs.SitemapExternalizerCaConfig;
import com.dish.dopweb.core.models.ExternalizerModel;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.oak.commons.PropertiesUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.net.URI;
import java.util.*;

@Component(
        service = Servlet.class,
        immediate = true,
        property = {
                Constants.SERVICE_DESCRIPTION + "=DISH Sitemap Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.selectors" + "=" + "sitemap",
                "sling.servlet.extensions" + "=" + "xml"
        })
@Designate(ocd = SitemapConfigImpl.SitemapConfigService.class)

/**
 * This servlet service is used to generate sitemap on the fly.
 */
@SuppressWarnings({"PMD.UnsynchronizedStaticFormatter", "PMD.GodClass", "PMD.TooManyFields", "PMD.UseTryWithResources", "PMD.CyclomaticComplexity", "PMD.OnlyOneReturn", "PMD.UseConcurrentHashMap", "PMD.CloseResource"})
public class SitemapConfigImpl extends SlingSafeMethodsServlet {

    public static final String CHANGEFREQ = "changefreq";
    public static final String PRIORITY = "priority";
    private final transient Logger logger = LoggerFactory.getLogger(SitemapConfigImpl.class);
    private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");
    private static final long serialVersionUID = -4341410431709866912L;

    private transient Page page;

    private String externalizerDomain;
    private boolean includeLastmodified;
    private String[] changeFrequencyProperties;
    private String[] priorityProperties;
    private String damFolderProperty;
    private String[] damAssetMIMETypes;
    private String excludePages;
    private String[] urlRewrites1;
    private boolean includeInheritValue;
    private boolean extensionlessURLs;
    private boolean removedSlash;
    private String characterEncoding;
    private String excludeTemplates;
    private boolean useVanityURLs;

    @Reference
    private transient Externalizer externalizer;

    private String loc;

    private static final String NAMESPACE = "http://www.sitemaps.org/schemas/sitemap/0.9";


    @ObjectClassDefinition(
            name = "Dish Sitemap Confiuration",
            description = "Page and Asset Site Map Servlet Configuration"
    )

    public @interface SitemapConfigService {
        @AttributeDefinition(
                name = "Sling Resource Type",
                description = "Sling Resource Type for the Home Page component or components.")
        String[] sling_servlet_resourceTypes() default {""};

        @AttributeDefinition(
                name = "Externalizer Domain",
                description = "Must correspond to a configuration of the Externalizer component. If blank the externalization will prepend the current request's scheme combined with the current request's host header.")
        String externalizerDomain() default "publish";

        @AttributeDefinition(
                name = "Include Last Modified",
                description = "If true, the last modified value will be included in the sitemap.")
        boolean includeLastmodified() default false;

        @AttributeDefinition(
                name = "Change Frequency Properties",
                description = "The set of JCR property names which will contain the change frequency value.")
        String[] changeFrequencyProperties() default "";

        @AttributeDefinition(
                name = "Priority Properties",
                description = "The set of JCR property names which will contain the priority value.")
        String[] priorityProperties() default "";

        @AttributeDefinition(
                name = "DAM Folder Property",
                description = "The JCR property name which will contain DAM folders to include in the sitemap.")
        String damFolderProperty() default "";

        @AttributeDefinition(
                name = "DAM Asset MIME Types",
                description = "MIME types allowed for DAM assets.")
        String[] damAssetMIMETypes() default "";

        @AttributeDefinition(
                name = "Exclude Pages (by properties of boolean values) from Sitemap Property",
                description = "The boolean [cq:Page]/jcr:content property name which indicates if the Page should be hidden from the Sitemap.")
        String excludePages() default "";

        @AttributeDefinition(
                name = "URL Rewrites",
                description = "Colon separated URL rewrites to adjust the <loc> to match your dispatcher's apache rewrites")
        String[] urlRewrites() default "";

        @AttributeDefinition(
                name = "Include Inherit Value",
                description = "If true searches for the frequency and priority attribute in the current page if null looks in the parent.")
        boolean includeInheritValue() default false;

        @AttributeDefinition(
                name = "Extensionless URLs",
                description = "If true, page links included in sitemap are generated without .html extension and the path is included with a trailing slash, e.g. /content/geometrixx/en/.")
        boolean extensionlessURLs() default false;

        @AttributeDefinition(
                name = "Remove Trailing Slash from Extensionless URLs",
                description = "Only relevant if Extensionless URLs is selected. If true, the trailing slash is removed from extensionless page links, e.g. /content/geometrixx/en.")
        boolean removedSlash() default false;

        @AttributeDefinition(
                name = "Character Encoding",
                description = "If not set, the container's default is used (ISO-8859-1 for Jetty)")
        String characterEncoding() default "";

        @AttributeDefinition(
                name = "Exclude Pages (by Template) from Sitemap",
                description = "Excludes pages that have a matching value at [cq:Page]/jcr:content@cq:Template")
        String excludeTemplates() default "";

        @AttributeDefinition(
                name = "Use Vanity URLs",
                description = "Use the Vanity URL for generating the Page URL")
        boolean useVanityURLs() default true;
    }

    @Activate
    @Modified
    protected void activate(final SitemapConfigImpl.SitemapConfigService sitemapConfigService) {
        this.externalizerDomain = sitemapConfigService.externalizerDomain();
        this.includeLastmodified = sitemapConfigService.includeLastmodified();
        this.changeFrequencyProperties = sitemapConfigService.changeFrequencyProperties();
        this.priorityProperties = sitemapConfigService.priorityProperties();
        this.damFolderProperty = sitemapConfigService.damFolderProperty();
        this.damAssetMIMETypes = sitemapConfigService.damAssetMIMETypes();
        this.excludePages = sitemapConfigService.excludePages();
        this.urlRewrites1 = sitemapConfigService.urlRewrites();
        this.includeInheritValue = sitemapConfigService.includeInheritValue();
        this.extensionlessURLs = sitemapConfigService.extensionlessURLs();
        this.removedSlash = sitemapConfigService.removedSlash();
        this.characterEncoding = sitemapConfigService.characterEncoding();
        this.excludeTemplates = sitemapConfigService.excludeTemplates();
        this.useVanityURLs = sitemapConfigService.useVanityURLs();
    }

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {

        final ExternalizerModel externalizerModel = Optional.ofNullable(request)
                .map(slingHttpServletRequest -> slingHttpServletRequest.adaptTo(ExternalizerModel.class))
                .orElse(null);

        final String caConfigDomainName = Optional.ofNullable(externalizerModel)
                .map(externalizerModel1 -> externalizerModel1.getSitemapExternalizerCaConfig())
                .map(SitemapExternalizerCaConfig::externalizerCaConfigDomain)
                .orElse(null);

        this.externalizerDomain = caConfigDomainName;

        response.setContentType(request.getResponseContentType());
        final String characterEncoding = this.characterEncoding;
        if (StringUtils.isNotBlank(characterEncoding)) {
            response.setCharacterEncoding(characterEncoding);
        }
        final ResourceResolver resourceResolver = request.getResourceResolver();
        PageManager pageManager;
        if (resourceResolver != null) {
            pageManager = resourceResolver.adaptTo(PageManager.class);
            if (pageManager != null) {
                page = pageManager.getContainingPage(request.getResource());
            }
        }
        final XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
        XMLStreamWriter stream = null;
        try {
            stream = outputFactory.createXMLStreamWriter(response.getWriter());
            stream.writeStartDocument("1.0");

            stream.writeStartElement("", "urlset", NAMESPACE);
            stream.writeNamespace("", NAMESPACE);

            // first do the current page
            write(page, stream, request);

            final Iterator<Page> children = page.listChildren(new PageFilter(false, true), true);
            final XMLStreamWriter finalStream = stream;
            children.forEachRemaining(page1 -> {
                try {
                    write(page1, finalStream, request);
                } catch (XMLStreamException e) {
                    logger.error("Error while writing stream :: {0}", e);
                }
            });

            final String[] damAssetMIMETypes = this.damAssetMIMETypes;
            final String damFolderProperty = this.damFolderProperty;
            if (damAssetMIMETypes.length > 0 && damFolderProperty.length() > 0) {
                for (final Resource assetFolder : getAssetFolders(page, resourceResolver)) {
                    writeAssets(stream, assetFolder, request);
                }
            }

            stream.writeEndElement();

            stream.writeEndDocument();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (XMLStreamException e) {
                    logger.error("Can not close xml stream writer", e);
                }
            }
        }
    }

    private Collection<Resource> getAssetFolders(final Page page, final ResourceResolver resolver) {
        final List<Resource> allAssetFolders = new ArrayList<>();
        final ValueMap properties = page.getProperties();
        final String damFolderProperty = this.damFolderProperty;
        final String[] configuredAssetFolderPaths = properties.get(damFolderProperty, String[].class);
        if (configuredAssetFolderPaths != null) {
            // Sort to aid in removal of duplicate paths.
            Arrays.sort(configuredAssetFolderPaths);
            String prevPath = "#";
            for (final String configuredAssetFolderPath : configuredAssetFolderPaths) {
                // Ensure that this folder is not a child folder of another
                // configured folder, since it will already be included when
                // the parent folder is traversed.
                if (StringUtils.isNotBlank(configuredAssetFolderPath) && !configuredAssetFolderPath.equals(prevPath)
                        && !StringUtils.startsWith(configuredAssetFolderPath, prevPath + "/")) {
                    prevPath = getPrevPath(resolver, allAssetFolders, prevPath, configuredAssetFolderPath);
                }
            }
        }
        return allAssetFolders;
    }

    private String getPrevPath(final ResourceResolver resolver, final List<Resource> allAssetFolders, final String prevPath, final String configuredAssetFolderPath) {
        String prevPathLocal = prevPath;
        final Resource assetFolder = resolver.getResource(configuredAssetFolderPath);
        if (assetFolder != null) {
            prevPathLocal = configuredAssetFolderPath;
            allAssetFolders.add(assetFolder);
        }
        return prevPathLocal;
    }

    private void writeAssets(final XMLStreamWriter stream, final Resource assetFolder, final SlingHttpServletRequest request)
            throws XMLStreamException {
        final Iterator<Resource> children1 = assetFolder.listChildren();
        children1.forEachRemaining(resource -> {
            final Resource assetFolderChild = resource;
            if (assetFolderChild.isResourceType(DamConstants.NT_DAM_ASSET)) {
                final Asset asset = assetFolderChild.adaptTo(Asset.class);

                final String[] damAssetMIMETypes = this.damAssetMIMETypes;
                final List<String> damAssetMIMEType = Arrays.asList(damAssetMIMETypes);
                if (damAssetMIMEType.contains(asset.getMimeType())) {
                    try {
                        writeAsset(asset, stream, request);
                    } catch (XMLStreamException e) {
                        logger.error("Error while writing to assets :: {0} ", e);
                    }
                }
            } else {
                try {
                    writeAssets(stream, assetFolderChild, request);
                } catch (XMLStreamException e) {
                    logger.error("Error while writing to assets :: {0} ", e);
                }
            }
        });
    }

    private void writeAsset(final Asset asset, final XMLStreamWriter stream, final SlingHttpServletRequest request) throws XMLStreamException {
        stream.writeStartElement(NAMESPACE, "url");

        final String loc = externalizeUri(request, asset.getPath());
        writeElement(stream, "loc", loc);

        final boolean includeLastmodified = this.includeLastmodified;
        if (includeLastmodified) {
            final long lastModified = asset.getLastModified();
            if (lastModified > 0) {
                writeElement(stream, "lastmod", DATE_FORMAT.format(lastModified));
            }
        }

        final Resource contentResource = asset.adaptTo(Resource.class);
        Resource child = null;
        if (contentResource != null) {
            child = contentResource.getChild(JcrConstants.JCR_CONTENT);
        }
        if (child != null) {
            final boolean includeInheritValue = this.includeInheritValue;
            final String[] changeFrequencyProperties = this.changeFrequencyProperties;
            final String[] priorityProperties = this.priorityProperties;
            if (includeInheritValue) {
                final HierarchyNodeInheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(
                        contentResource);
                writeFirstPropertyValue(stream, CHANGEFREQ, changeFrequencyProperties, inheritanceValueMap);
                writeFirstPropertyValue(stream, PRIORITY, priorityProperties, inheritanceValueMap);
            } else {
                final ValueMap properties = contentResource.getValueMap();
                writeFirstPropertyValue(stream, CHANGEFREQ, changeFrequencyProperties, properties);
                writeFirstPropertyValue(stream, PRIORITY, priorityProperties, properties);
            }
        }

        stream.writeEndElement();
    }

    private String externalizeUri(final SlingHttpServletRequest request, final String path) {
        String externalizeUriVar;
        try {
            if (StringUtils.isNotBlank(externalizerDomain)) {
                externalizeUriVar = externalizer.externalLink(request.getResourceResolver(), externalizerDomain, path);
            } else {
                logger.debug("No externalizer domain configured, take into account current host header {} and current scheme {}", request.getServerName(), request.getScheme());
                externalizeUriVar = externalizer.absoluteLink(request, request.getScheme(), path);
            }
        } catch (IllegalArgumentException e) {
            logger.debug("No externalizer domain found for header {} and current scheme {}", request.getServerName(), request.getScheme());
            externalizeUriVar = externalizer.absoluteLink(request, request.getScheme(), path);
        }

        return externalizeUriVar;
    }

    private void writeElement(final XMLStreamWriter stream, final String elementName, final String text)
            throws XMLStreamException {
        stream.writeStartElement(NAMESPACE, elementName);
        stream.writeCharacters(text);
        stream.writeEndElement();
    }

    private void writeFirstPropertyValue(final XMLStreamWriter stream, final String elementName,
                                         final String[] propertyNames, final ValueMap properties) throws XMLStreamException {
        for (final String prop : propertyNames) {
            final String value = properties.get(prop, String.class);
            if (value != null) {
                writeElement(stream, elementName, value);
                break;
            }
        }
    }

    @SuppressWarnings("squid:S1144")
    private void writeFirstPropertyValue(final XMLStreamWriter stream, final String elementName,
                                         final String[] propertyNames, final InheritanceValueMap properties) throws XMLStreamException {
        for (final String prop : propertyNames) {
            String value = properties.get(prop, String.class);
            if (value == null) {
                value = properties.getInherited(prop, String.class);
            }
            if (value != null) {
                writeElement(stream, elementName, value);
                break;
            }
        }
    }

    @SuppressWarnings("squid:S1192")
    private void write(final Page page, final XMLStreamWriter stream, final SlingHttpServletRequest request) throws XMLStreamException {
        if (isHiddenByPageProperty(page) || isHiddenByPageTemplate(page)) {
            return;
        }
        stream.writeStartElement(NAMESPACE, "url");
        // String loc;

        final boolean useVanityUrl = this.useVanityURLs;
        final boolean extensionlessUrls = this.extensionlessURLs;
        final boolean removeTrailingSlash = this.removedSlash;
        if (useVanityUrl && !StringUtils.isEmpty(page.getVanityUrl())) {
            loc = externalizeUri(request, page.getVanityUrl());
        } else if (!extensionlessUrls) {
            loc = externalizeUri(request, String.format("%s.html", page.getPath()));
        } else {
            final String urlFormat = removeTrailingSlash ? "%s" : "%s/";
            loc = externalizeUri(request, String.format(urlFormat, page.getPath()));
        }

        loc = applyUrlRewrites(loc);

        writeElement(stream, "loc", loc);
        final boolean includeLastModified = this.includeLastmodified;
        if (includeLastModified) {
            final Calendar cal = page.getLastModified();
            if (cal != null) {
                writeElement(stream, "lastmod", DATE_FORMAT.format(cal));
            }
        }

        final boolean includeInheritValue = this.includeInheritValue;
        final String[] changefreqProperties = this.changeFrequencyProperties;
        final String[] priorityProperties = this.priorityProperties;
        if (includeInheritValue) {
            final HierarchyNodeInheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(
                    page.getContentResource());
            writeFirstPropertyValue(stream, CHANGEFREQ, changefreqProperties, inheritanceValueMap);
            writeFirstPropertyValue(stream, PRIORITY, priorityProperties, inheritanceValueMap);
        } else {
            final ValueMap properties = page.getProperties();
            writeFirstPropertyValue(stream, CHANGEFREQ, changefreqProperties, properties);
            writeFirstPropertyValue(stream, PRIORITY, priorityProperties, properties);
        }

        stream.writeEndElement();
    }

    private boolean isHiddenByPageProperty(final Page page) {
        boolean flag = false;
        final String excludePages = this.excludePages;
        final List<String> excludeFromSiteMapProperty = Arrays.asList(PropertiesUtil.toStringArray(excludePages, new String[0]));
        if (excludeFromSiteMapProperty != null) {
            for (final String pageProperty : excludeFromSiteMapProperty) {
                flag = flag || page.getProperties().get(pageProperty, Boolean.FALSE);
            }
        }
        return flag;
    }

    private boolean isHiddenByPageTemplate(final Page page) {
        boolean flag = false;
        final List<String> excludedPageTemplates = Arrays.asList(PropertiesUtil.toStringArray(excludeTemplates, new String[0]));
        if (excludedPageTemplates != null) {
            for (final String pageTemplate : excludedPageTemplates) {
                flag = flag || page.getProperties().get("cq:template", StringUtils.EMPTY).equalsIgnoreCase(pageTemplate);
            }
        }
        return flag;
    }

    private String applyUrlRewrites(final String url) {
        try {
            final String[] urlRewritesArr = this.urlRewrites1;
            final Map<String, String> urlRewrites = (Map<String, String>) ParameterUtil.toMap(PropertiesUtil.toStringArray(urlRewritesArr, new String[0]), ":", true, "");
            final String path = URI.create(url).getPath();
            for (final Map.Entry<String, String> rewrite : urlRewrites.entrySet()) {
                if (path.startsWith(rewrite.getKey())) {
                    return url.replaceFirst(rewrite.getKey(), rewrite.getValue());
                }
            }
        } catch (IllegalArgumentException e) {
            logger.error("Error while apply url rewrites :: {} ", e);
        }
        return url;
    }
}
