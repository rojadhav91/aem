package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Facebook Domain Verification CA Config is for setting the analytics content value.
 */
@Configuration(
        label = "Facebook Domain Verification Configuration",
        description = "Configure the facebook domain verification content.")
public @interface FacebookDomainVerificationCaConfig {

    /**
     * property for facebook domain verification content.
     *
     * @return facebook domain verification content
     */
    @Property(label = "Facebook Domain Verification Content")
    String facebookDomainVerificationContent();
}
