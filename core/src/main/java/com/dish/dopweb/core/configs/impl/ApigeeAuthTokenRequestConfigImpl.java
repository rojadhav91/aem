package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.ApigeeAuthTokenRequestConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration Service Implementation
 */
@Component(service = ApigeeAuthTokenRequestConfig.class, immediate = true)
@Designate(ocd = ApigeeAuthTokenRequestConfigImpl.ApigeeAuthTokenRequestConfig.class, factory = false)
@ServiceDescription("A service used to configure Service Coverage API")
@ServiceVendor("Dish DOP")
public class ApigeeAuthTokenRequestConfigImpl implements ApigeeAuthTokenRequestConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String apigeeAuthTokenEndpoint;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String clientId;

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String clientSecret;

    /**
     * Set configuration
     * @param config
     */
    @Activate
    public void activate(final ApigeeAuthTokenRequestConfigImpl.ApigeeAuthTokenRequestConfig config) {

        apigeeAuthTokenEndpoint = config.apigeeAuthTokenEndpoint();
        clientId = config.clientId();
        clientSecret = config.clientSecret();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Apigee Authorization Token Configuration",
            description = "The configuration for Apigee Authorization Token")
    public @interface ApigeeAuthTokenRequestConfig {

        @AttributeDefinition(name = "Apigee Auth Token Endpoint", description = "Apigee Auth Token Api Endpoint")
        String apigeeAuthTokenEndpoint() default  "";

        @AttributeDefinition(name = "Apigee Auth Client ID", description = "Client ID for Apigee Auth Token")
        String clientId() default  "";

        @AttributeDefinition(name = "Apigee Auth Client Secret", description = "Client Secret for Apigee Auth Token")
        String clientSecret() default  "";
    }
}
