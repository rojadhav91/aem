package com.dish.dopweb.core.servlets;

import com.adobe.granite.ui.clientlibs.LibraryType;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.NameConstants;

import com.dish.dopweb.core.utils.ClientLibraryUtils;
import com.dish.dopweb.core.utils.MarkupUtils;
import com.google.gson.Gson;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;


import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.day.crx.JcrConstants.*;
import static org.apache.sling.api.servlets.HttpConstants.METHOD_GET;

/** Servlet to return all the XF variation's markup in order. */
@Component(
    service = Servlet.class,
    property = {
      ServletResolverConstants.SLING_SERVLET_NAME + "=" + "Experience Fragment Content Servlet",
      Constants.SERVICE_DESCRIPTION
          + "="
          + "Returns experience fragments content and client libraries.",
      ServletResolverConstants.SLING_SERVLET_METHODS + "=" + METHOD_GET,
      ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/services/getContent",
      ServletResolverConstants.SLING_SERVLET_SELECTORS + "=" + "fetchXF",
      ServletResolverConstants.SLING_SERVLET_EXTENSIONS + "=" + "json"
    })
@Slf4j
public class ExperienceFragmentContent extends SlingSafeMethodsServlet {

  private static final long serialVersionUID = 779035839272822337L;

  public static final String ROOT_PATH_FORMAT =
      "/content/experience-fragments/%s/us/%s/site/magento";
  private static final String[] CATEGORIES = {"dopweb.xf"};
  public static final String CQ_LAST_MODIFIED = "cq:lastModified";
  public static final String BRAND = "brand";
  public static final String LANGUAGE = "language";
  public static final String XF_KEY = "XF";
  public static final String CSS_RESOURCE_PATHS = "cssResourcePaths";
  public static final String JS_RESOURCE_PATHS = "jsResourcePaths";
  public static final String CREATED_AT = "createdAt";
  public static final String CQ_LAST_REPLICATION_ACTION = "cq:lastReplicationAction";

  @Reference private transient RequestResponseFactory requestResponseFactory;
  @Reference private transient SlingRequestProcessor requestProcessor;
  private String rootPath;

  @Reference
  private transient SlingSettingsService slingSettingsService;

  private final SimpleDateFormat dateFormatter =
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);

  @Override
  protected void doGet(
      @NonNull final SlingHttpServletRequest request,
      @NonNull final SlingHttpServletResponse response) {

    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");

    final Map<String, Object> result = getPropertiesMap(request);

    if (validateMap(result)) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    try {
      response.getWriter().write(new Gson().toJson(result));
    } catch (IOException e) {
      log.error("Unable to write to the response, {}", e.getMessage());
    }
  }

  @SuppressWarnings("unchecked")
  private boolean validateMap(final Map<String, Object> result) {
    return result == null
        || result.isEmpty()
        || ((List<Object>) result.get(XF_KEY)).isEmpty()
        || ((List<String>) result.get(JS_RESOURCE_PATHS)).isEmpty()
        || ((List<String>) result.get(CSS_RESOURCE_PATHS)).isEmpty();
  }

  private Map<String, Object> getPropertiesMap(final SlingHttpServletRequest request) {
    final Map<String, Object> responseMap = new ConcurrentHashMap<>();
    responseMap.put(CREATED_AT, dateFormatter.format(new Date().getTime()));
    responseMap.put(JS_RESOURCE_PATHS, clientLibraryPaths(LibraryType.JS, request));
    responseMap.put(CSS_RESOURCE_PATHS, clientLibraryPaths(LibraryType.CSS, request));
    responseMap.put(XF_KEY, getData(request));

    return responseMap;
  }

  protected List<String> clientLibraryPaths(
      final LibraryType type, final SlingHttpServletRequest request) {
    return ClientLibraryUtils.clientLibraryPaths(CATEGORIES, type, request);
  }

  private List<Map<String, Object>> getData(final SlingHttpServletRequest request) {
    final String brand = request.getParameter(BRAND);
    final String language = request.getParameter(LANGUAGE);
    List<Map<String, Object>> result;

    rootPath = String.format(ROOT_PATH_FORMAT, brand, language);
    try (ResourceResolver resolver = request.getResourceResolver()) {
      final Resource rootResource = resolver.resolve(rootPath);
      result = findAllXfs(rootResource, request);
    }

    return result;
  }

  private List<Map<String, Object>> findAllXfs(
      final Resource resource, final SlingHttpServletRequest request) {
    final List<Map<String, Object>> blocks = new ArrayList<>();

    final Iterator<Resource> resourceIterator = resource.listChildren();
    while (resourceIterator.hasNext()) {
      final Resource childResource = resourceIterator.next();
      final String primaryType = childResource.getValueMap().get(JCR_PRIMARYTYPE, "");
      final boolean isXF = primaryType.equals(NameConstants.NT_PAGE);

      if (isXF) {
        if (isAuthor(slingSettingsService)) {
          if ("Activate".equals(lastReplicationAction(childResource))) {
            blocks.add(getXFVariantsMarkup(childResource, request));
          }
        } else {
          blocks.add(getXFVariantsMarkup(childResource, request));
        }
      } else {
        blocks.addAll(findAllXfs(childResource, request));
      }
    }
    return blocks;
  }

  private Map<String, Object> getXFVariantsMarkup(
      final Resource xfResource, final SlingHttpServletRequest request) {

    final List<Map<String, Object>> blocks = new ArrayList<>();
    Map<String, Object> block;

    int blockId = 1;
    final Iterator<Resource> resourceIterator = xfResource.listChildren();
    while (resourceIterator.hasNext()) {
      final Resource variation = resourceIterator.next();

      if (isJcrContent(variation)) {
        continue;
      }
      if (isAuthor(slingSettingsService) && !"Activate".equals(lastReplicationAction(variation))) {
          continue;
      }

      block = new LinkedHashMap<>();
      block.put("Id", String.format("Block%02d", blockId++));
      block.put("LastModified", lastModifiedOrCreated(variation));
      block.put("Markup", getMarkup(request, variation));
      blocks.add(block);
    }

    final Map<String, Object> xfObject = new ConcurrentHashMap<>();
    xfObject.put("Page", StringUtils.substringAfter(xfResource.getPath(), rootPath));
    xfObject.put("Blocks", blocks);

    return xfObject;
  }

  protected String getMarkup(final SlingHttpServletRequest request, final Resource variation) {
    final StringBuilder strMarkup= new StringBuilder();
    strMarkup.setLength(0);
   final Optional<String> res = Optional.ofNullable(request.getResourceResolver()
           .getResource(variation.getPath()+"/jcr:content"))
           .map(resource -> resource.adaptTo(ValueMap.class))
                   .map(property -> property.get("mode", String.class));
    final String markup = MarkupUtils.getMarkup(
            variation.getPath(),
            request.getResourceResolver(),
            requestResponseFactory,
            requestProcessor);
    if(res.isPresent()){
      strMarkup.append(markup.replaceFirst("cmp-container", "cmp-container "+res.get()));
   }else{
      strMarkup.append( markup.replaceFirst("cmp-container", "cmp-container day"));
    }
    return strMarkup.toString();
  }

  private boolean isJcrContent(final Resource resource) {
    return resource.getName().equals(JCR_CONTENT);
  }

  private String lastModifiedOrCreated(final Resource resource) {
    return Optional.of(resource)
        .map(res -> fromJcrContent(CQ_LAST_MODIFIED, resource))
        .orElseGet(() -> fromJcrContent(JCR_CREATED, resource));
  }

 private String lastReplicationAction(final Resource resource) {
    return Optional.of(resource)
            .map(res -> fromJcrContent(CQ_LAST_REPLICATION_ACTION, resource))
            .orElse(null);
  }

  private String fromJcrContent(final String property, final Resource variation) {
    return Optional.ofNullable(variation)
        .map(res -> res.getChild(JCR_CONTENT))
        .map(Resource::getValueMap)
        .map(vm -> vm.get(property, String.class))
        .orElse(null);
  }

  private boolean isAuthor(final SlingSettingsService slingSettingsService) {
    return slingSettingsService.getRunModes().contains("author");
  }
}
