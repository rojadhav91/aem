package com.dish.dopweb.core.utils;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.settings.SlingSettingsService;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.dish.dopweb.core.utils.StreamUtils.stream;

/** Utility class for AEM Tags. */
@Slf4j
public final class TagUtils {

  public static final String ZIP_CODES = "zip-codes";
  public static final String ACTIVATE = "Activate";

  private TagUtils() {
    throw new IllegalStateException("Utility class");
  }

  /** Get tag names list. */
  public static List<String> getTagNames(
      final String tagsPath, final ResourceResolver resourceResolver) {
    return allZipcodes(tagsPath, Tag::getName, resourceResolver);
  }

  /** Get tag titles list. */
  public static List<String> getTagTitles(
      final String tagsPath, final ResourceResolver resourceResolver) {
    return getTagsAsList(tagsPath, Tag::getTitle, resourceResolver);
  }

  private static List<String> getTagsAsList(
      final String tagsPath,
      final Function<Tag, String> keyExtractor,
      final ResourceResolver resourceResolver) {

    return Optional.ofNullable(resourceResolver)
        .map(rr -> rr.adaptTo(TagManager.class))
        .map(tm -> tm.resolve(tagsPath))
        .map(
            tag ->
                stream(tag.listChildren())
                    .map(keyExtractor)
                    .collect(Collectors.toUnmodifiableList()))
        .orElse(Collections.emptyList());
  }

  private static List<String> allZipcodes(
      final String tagsPath,
      final Function<Tag, String> keyExtractor,
      final ResourceResolver resourceResolver) {
    return stream(resourceResolver.resolve(tagsPath).listChildren())
        .filter(TagUtils::hasZipCodes)
        .map(TagUtils::getZipCodes)
        .filter(Objects::nonNull)
        .map(Resource::listChildren)
        .flatMap(StreamUtils::stream)
        .map(resource -> resource.adaptTo(Tag.class))
        .filter(Objects::nonNull)
        .map(keyExtractor)
        .collect(Collectors.toUnmodifiableList());
  }

//  private static boolean isPublished(final Resource zipcode) {
//    return Optional.ofNullable(zipcode.adaptTo(ValueMap.class))
//        .map(vm -> vm.get(PN_PAGE_LAST_REPLICATION_ACTION, String.class))
//        .map(ACTIVATE::equals)
//        .orElse(false);
//  }

  private static Resource getZipCodes(final Resource market) {
    return stream(market.listChildren())
        .filter(resource -> ZIP_CODES.equals(resource.getName()))
        .findFirst()
        .orElse(null);
  }

  private static boolean hasZipCodes(final Resource market) {
    return stream(market.listChildren()).map(Resource::getName).anyMatch(ZIP_CODES::equals);
  }

    /**
     *
     * @param zipcodesTagsPath
     * @param resourceResolver
     * @return List<String> activatedTags
     */
    @SuppressWarnings({"PMD.AvoidCatchingGenericException"})
    public static List<String> getActivateTags(final String zipcodesTagsPath, final ResourceResolver resourceResolver, final SlingSettingsService slingSettingsService) {
        List<String> activatedTagList = new ArrayList<>();
        if(isAuthor(slingSettingsService)) {
            try {
                final Iterator<Resource> resourceIterator = resourceResolver.resolve(zipcodesTagsPath).listChildren();
                  while (resourceIterator.hasNext()) {
                      final Resource resource = resourceIterator.next();
                      if (TagUtils.hasZipCodes(resource)) {
                          final Resource child = resource.getChild("zip-codes");
                          if (child != null) {
                              final Iterator<Resource> resourceIterator1 = child.listChildren();
                              while (resourceIterator1.hasNext()) {
                                  final Resource resource1 = resourceIterator1.next();
                                  final String replicateProperty = resource1.getValueMap().get("cq:lastReplicationAction", String.class);
                                  addActivateTagToList(activatedTagList, resource1, replicateProperty);
                              }
                          }
                      }
                  }

            } catch (Exception e) {
                log.error("Unable to create activated tag list, {}", e.getMessage());
            }
        }else{
            activatedTagList = getTagNames(zipcodesTagsPath, resourceResolver);
        }
        return activatedTagList;
    }

    private static Boolean isAuthor(final SlingSettingsService slingSettingsService) {
       return slingSettingsService.getRunModes().contains("author");
    }

    private static void addActivateTagToList(final List<String> activatedTagList, final Resource resource1, final String replicateProperty) {
        if (StringUtils.isNotBlank(replicateProperty) && ACTIVATE.equals(replicateProperty)) {
            final Tag tag = resource1.adaptTo(Tag.class);
            if (tag != null) {
                activatedTagList.add(tag.getName());
            }
        }
    }

    /**
     *
     * @param tagsPath
     * @param resourceResolver
     * @param slingSettingsService
     * @return List<String> zipCodes
     */
    @SuppressWarnings({"PMD.AvoidCatchingGenericException"})
    public static List<String> getZipCodesByRunMode(final String tagsPath, final ResourceResolver resourceResolver, final SlingSettingsService slingSettingsService){
        List<String> zipCodes;
        if (isAuthor(slingSettingsService)){
            zipCodes = getActivateTags(tagsPath, resourceResolver, slingSettingsService);
        } else {
            zipCodes = getTagNames(tagsPath, resourceResolver);
        }
        return zipCodes;
    }
}
