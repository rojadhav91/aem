package com.dish.dopweb.core.models.form;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.osgi.annotation.versioning.ConsumerType;

/** Form Text sling model interface. */
@SuppressFBWarnings(value="NM_SAME_SIMPLE_NAME_AS_INTERFACE",
        justification="The Name have to be the same")
@ConsumerType
@SuppressWarnings({"PMD.ShortClassName","PMD.CommentRequired"})
public interface Text extends com.adobe.cq.wcm.core.components.models.form.Text {
    
    String getIcon();

    String getIconPosition();


}
