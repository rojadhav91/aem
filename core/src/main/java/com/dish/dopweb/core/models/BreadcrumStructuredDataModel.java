package com.dish.dopweb.core.models;

import com.adobe.cq.wcm.core.components.models.Breadcrumb;
import com.adobe.cq.wcm.core.components.models.NavigationItem;
import com.dish.dopweb.core.utils.HostnameUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.via.ResourceSuperType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.json.JsonException;
import java.util.Collection;

/**
 * Breadcrumb Sling Model that creates json schema for breadcrumb.
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        resourceType = "dopweb/components/breadcrumb",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BreadcrumStructuredDataModel {
    private final Logger logger = LoggerFactory.getLogger(BreadcrumStructuredDataModel.class);

    @Self
    @Via(type = ResourceSuperType.class)
    private Breadcrumb breadcrumb;

    private String jsonString;

    @SlingObject
    private SlingHttpServletRequest request;

    /**
     * Postconstruct method to process
     */
    @PostConstruct
    public void init() {
        final Collection<NavigationItem> items = breadcrumb.getItems();
        if (!items.isEmpty()) {
            final JsonObject jsonObject = buildSchema();
            final JsonArray jsonArray = new JsonArray();
            final int[] index = {0};
            items.forEach(navigationItem -> {
                final JsonObject listElementSchema = createListElementSchema(navigationItem, ++index[0]);
                jsonArray.add(listElementSchema);
            });
            jsonObject.add("itemListElement", jsonArray);
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();
            jsonString = gson.toJson(jsonObject);
        }
    }

    private JsonObject createListElementSchema(final NavigationItem navigationItem, final int index) {
        final JsonObject jsonObject = new JsonObject();
        final JsonObject innerObj = new JsonObject();
        try {
            jsonObject.addProperty("@type", "ListItem");
            jsonObject.addProperty("position", index);
            innerObj.addProperty("@id", getHostName() + navigationItem.getURL());
            innerObj.addProperty("name", navigationItem.getTitle());
            jsonObject.add("item", innerObj);
        } catch (JsonException e) {
            logger.error("Error while creating element schema :: {} ", e);
        }
        return jsonObject;
    }

    private JsonObject buildSchema() {
        final JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("@context", "https://schema.org");
            jsonObject.addProperty("@type", "BreadcrumbList");
        } catch (JsonException e) {
            logger.error("Error while building schema :: {} ", e);
        }
        return jsonObject;
    }

    /**
     * This method will return hostname
     *
     * @return hostname
     */
    public String getHostName() {
            return HostnameUtil.getHostName(request.getRequestURL().toString());
    }

    public String getJsonString() {
        return jsonString;
    }
}
