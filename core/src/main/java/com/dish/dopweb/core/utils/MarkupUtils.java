package com.dish.dopweb.core.utils;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.ScriptEvaluationException;
import org.apache.sling.engine.SlingRequestProcessor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.apache.sling.api.servlets.HttpConstants.METHOD_GET;

/** Utility Class to get markup. */
@Slf4j
public final class MarkupUtils {

  public static final String ROOT_NODE_PATH = "/jcr:content/root.html";

  private MarkupUtils() {
    throw new IllegalStateException("Utility class.");
  }

  /**
   * Get markup for path specified.
   *
   * @return markup
   */
  public static String getMarkup(
      final String variationPath,
      final ResourceResolver resolver,
      final RequestResponseFactory requestResponseFactory,
      final SlingRequestProcessor requestProcessor) {
    String html = "";

    String requestPath =
        variationPath.endsWith(".html")
            ? StringUtils.substringBefore(variationPath, ".html")
            : variationPath;

    requestPath = requestPath.concat(ROOT_NODE_PATH);

    if (StringUtils.isNotBlank(requestPath)) {
      /* Setup request */
      final HttpServletRequest req = requestResponseFactory.createRequest(METHOD_GET, requestPath);
      WCMMode.DISABLED.toRequest(req);

      /* Setup response */
      final ByteArrayOutputStream out = getByteArrayOutputStream();
      final HttpServletResponse resp = requestResponseFactory.createResponse(out);

      try {
        requestProcessor.processRequest(req, resp, resolver);
        html = out.toString(StandardCharsets.UTF_8).trim();
        log.info("Fetched markup for path {}", requestPath);
      } catch (ServletException | IOException | ScriptEvaluationException n) {
        log.error("Unable to process request using SlingRequestProcessor at path {}", requestPath);
      }
    } else {
      log.error("Experience Fragment path in CA config is either null or empty");
    }

    // RequestResponseFactory does not implement getStatus method.
    if (html.isEmpty()
        || html.charAt(0) != '<'
        || html.contains("/apps/sling/servlet/errorhandler") && html.contains("No resource found")
        || html.contains("res_type=dopweb/components/structure/errorpage")) {
      log.error("Bad markup for header or footer at path {}", requestPath);
      html = "";
    }
    return html;
  }

  private static ByteArrayOutputStream getByteArrayOutputStream() {
    return new ByteArrayOutputStream();
  }
}
