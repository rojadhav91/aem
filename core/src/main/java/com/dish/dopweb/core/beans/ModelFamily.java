package com.dish.dopweb.core.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/** @author imran.padshah */
@SuppressWarnings("PMD")
@Getter
@Setter
public class ModelFamily {
  private String id;
  private String name;
  private List<TagItems> devices;
}
