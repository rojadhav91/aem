package com.dish.dopweb.core.models;

import com.day.cq.dam.api.Asset;
import lombok.Getter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Model to get svg file contents.
 */
@Model(
        adaptables = Resource.class,
        resourceType = SVGModel.RESOURCE_TYPE,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SVGModel {
    protected static final String RESOURCE_TYPE = "/content/dam/boost/web/icons/";

    private final Logger logger = LoggerFactory.getLogger(SVGModel.class);
    private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
    @Getter
    protected String svgContent;
    @ValueMapValue
    private String iconUrl;

    @OSGiService
    private static transient ResourceResolverFactory resourceResolverFactory;

    @PostConstruct
    protected void init() {

        try (ResourceResolver resourceResolver = getResourceResolver(APP_RESOURCE_SERVICE)){
            if (Objects.nonNull(iconUrl) && Objects.nonNull(resourceResolver)) {
                final InputStream stream = getResourceStream(resourceResolver);
                final StringBuilder stringBuilder = new StringBuilder();
                if (Objects.nonNull(stream)) {
                    String line;
                    try (BufferedReader bufferedReader =
                                 new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
                        line = bufferedReader.readLine();
                        while (line != null) {
                            stringBuilder.append(line);
                            line = bufferedReader.readLine();
                        }
                    } catch (IOException e) {
                        logger.error("Could not read content of svg file ({})", iconUrl);
                    }
                } else {
                    logger.error("Could not get stream from asset path: {}", iconUrl);
                }
                svgContent = stringBuilder.toString();
            }
        }
    }

    private InputStream getResourceStream(final ResourceResolver resourceResolver) {
        final Resource resource = resourceResolver.getResource(iconUrl);
        InputStream inputStream = null;
        if (resource != null) {
            final Asset fileAsset = resource.adaptTo(Asset.class);
            if(fileAsset != null) {
                inputStream = fileAsset.getRendition("original").getStream();
            }
        }
        return inputStream;
    }

    /**
     * Get service resource resolver
     * @param serviceParam
     * @return resourceResolver
     */
    public ResourceResolver getResourceResolver(final String serviceParam){
        ResourceResolver resolver = null;
        final ConcurrentHashMap<String, Object> param = getServiceParams(serviceParam);
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(param);
        } catch (LoginException e) {
            logger.error("Could not ger service user: {}", e);
        }
        return resolver;
    }

    /**
     * Create and Get service mapping
     * @param serviceParam
     * @return
     */
    private ConcurrentHashMap<String, Object> getServiceParams(final String serviceParam) {
        final ConcurrentHashMap<String, Object> param = new ConcurrentHashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, serviceParam);
        return param;
    }
}