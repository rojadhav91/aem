package com.dish.dopweb.core.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** Utility class to get ResourceResolver from a serviceName of parameter map. */
@Slf4j
public final class ResourceResolverUtil {

  private ResourceResolverUtil() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * This method will return resource resolver.
   *
   * @param resourceResolverFactory a resourceResolverFactory.
   * @param params authentication information.
   * @return resourceResolver.
   */
  public static ResourceResolver getServiceResourceResolver(
      final ResourceResolverFactory resourceResolverFactory, final Map<String, Object> params) {

    ResourceResolver resolver = null;
    if (resourceResolverFactory != null) {
      try {
        resolver = resourceResolverFactory.getServiceResourceResolver(params);
      } catch (LoginException e) {
        log.error("Error while getting resource resolver", e);
      }
    }
    return resolver;
  }

  /**
   * This method will return resource resolver.
   *
   * @param resourceResolverFactory a resourceResolverFactory.
   * @param serviceName a service name.
   * @return resourceResolver
   */
  public static ResourceResolver getServiceResourceResolver(
      final ResourceResolverFactory resourceResolverFactory, final String serviceName) {
    final Map<String, Object> params = new ConcurrentHashMap<>();
    params.put(ResourceResolverFactory.SUBSERVICE, serviceName);
    return getServiceResourceResolver(resourceResolverFactory, params);
  }
}
