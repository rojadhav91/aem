package com.dish.dopweb.core.configs;

import java.util.List;

/** Domain Whitelist config. */
public interface DomainWhitelistConfig {

  /**
   * List of whitelisted email domains.
   *
   * @return string array of whitelisted domains.
   */
  List<String> getWhitelistedDomains();
}
