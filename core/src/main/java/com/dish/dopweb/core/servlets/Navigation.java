package com.dish.dopweb.core.servlets;

import com.adobe.granite.ui.clientlibs.LibraryType;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.dish.dopweb.core.configs.NavigationConfig;
import com.dish.dopweb.core.utils.ClientLibraryUtils;
import com.dish.dopweb.core.utils.MarkupUtils;
import com.google.gson.Gson;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.day.cq.wcm.api.NameConstants.NT_PAGE;
import static org.apache.sling.api.servlets.HttpConstants.METHOD_GET;
import static org.apache.sling.api.servlets.HttpConstants.METHOD_POST;

/** Servlet to get header/footer markup and clientlibs path. */
@Component(
    service = Servlet.class,
    property = {
      ServletResolverConstants.SLING_SERVLET_NAME + "=" + "Header_Footer Endpoint",
      Constants.SERVICE_DESCRIPTION
          + "="
          + "Returns header and footer experience fragments and client library",
      ServletResolverConstants.SLING_SERVLET_METHODS + "=" + METHOD_GET,
      ServletResolverConstants.SLING_SERVLET_METHODS + "=" + METHOD_POST,
      ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES
          + "="
          + Navigation.SERVLET_RESOURCE_TYPE,
      ServletResolverConstants.SLING_SERVLET_SELECTORS + "=" + Navigation.SELECTOR,
      ServletResolverConstants.SLING_SERVLET_EXTENSIONS + "=" + Navigation.EXTENSION
    })
public class Navigation extends SlingAllMethodsServlet {

  public static final String HEADER_MARKUP = "headerMarkup";
  public static final String FOOTER_MARKUP = "footerMarkup";
  public static final String CSS_RESOURCE_PATHS = "cssResourcePaths";
  public static final String JS_RESOURCE_PATHS = "jsResourcePaths";

  /** Servlet resource Type constant. */
  protected static final String SERVLET_RESOURCE_TYPE = NT_PAGE;

  /** Servlet selector constant. */
  protected static final String SELECTOR = "navigation";

  /** Servlet extension constant. */
  protected static final String EXTENSION = "json";

  private static final long serialVersionUID = -4213561166266847100L;
  public static final String CREATED_AT = "createdAt";
  private final transient Logger logger = LoggerFactory.getLogger(Navigation.class);
  /** Service to create HTTP Servlet requests and responses. */
  @Reference private transient RequestResponseFactory requestResponseFactory;
  /** Service to process requests through Sling. */
  @Reference private transient SlingRequestProcessor requestProcessor;

  private transient NavigationConfig navigationConfig;

  /**
   * Will route POST requests to doGet.
   *
   * @param request request
   * @param response response
   */
  @Override
  protected void doPost(
      @NonNull final SlingHttpServletRequest request,
      @NonNull final SlingHttpServletResponse response) {
    doGet(request, response);
  }

  /**
   * Will handle GET requests.
   *
   * @param request request
   * @param response response
   */
  @SuppressWarnings({"PMD.CloseResource"})
  @Override
  protected void doGet(
      @NonNull final SlingHttpServletRequest request,
      @NonNull final SlingHttpServletResponse response) {

    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");

    navigationConfig = getNavigationConfig(request);
    final ResourceResolver resourceResolver =  request.getResourceResolver();

    final Map<String, Object> result = getPropertiesMap(request, resourceResolver);
    resourceResolver.close();
    final boolean success = validateMap(result);

    result.put("success", success);
    if (!success) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    try {
      response.getWriter().write(new Gson().toJson(result));
    } catch (IOException e) {
      logger.error("Unable to write to the response, {}", e.getMessage());
    }
  }

  private NavigationConfig getNavigationConfig(final SlingHttpServletRequest request) {
    return Optional.of(request.getResource())
        .map(resource -> resource.adaptTo(ConfigurationBuilder.class))
        .map(configurationBuilder -> configurationBuilder.as(NavigationConfig.class))
        .orElse(null);
  }

  @SuppressWarnings({"PMD.OnlyOneReturn", "PMD.EmptyStatementNotInLoop"})
  private boolean validateMap(final Map<String, Object> result) {
    for (final Map.Entry<String,Object> stringObjectEntry : result.entrySet()){
      if(JS_RESOURCE_PATHS.equals(stringObjectEntry.getKey()) || CSS_RESOURCE_PATHS.equals(stringObjectEntry.getKey())){
        if(((List<String>) result.get(CSS_RESOURCE_PATHS)).isEmpty() || ((List<String>) result.get(JS_RESOURCE_PATHS)).isEmpty()){
           return false;
        }
      }else {
        if(StringUtils.isBlank(stringObjectEntry.getValue().toString())){
          return false;
        }
      }
    };
    return true;
  }

  private Map<String, Object> getPropertiesMap(final SlingHttpServletRequest request, final ResourceResolver resourceResolver) {
    final SimpleDateFormat dateFormatter =
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);

    final Map<String, Object> result = new ConcurrentHashMap<>();

    final List<String> headerXFList = Arrays.asList(getHeaderXFPath());
    headerXFList.forEach(xfItem -> {
      final Resource resource = resourceResolver.getResource(xfItem);
      final String xfTitle = getXFMap(resource);
      if(StringUtils.isNotBlank(xfTitle)){
        result.put(xfTitle, getMarkup(resource.getPath(), request));
      }
    });

    final List<String> footerXFList = Arrays.asList(getFooterXFPath());
    footerXFList.forEach(xfItem -> {
      final Resource resource = resourceResolver.getResource(xfItem);
      final String xfTitle = getXFMap(resource);
      if(StringUtils.isNotBlank(xfTitle)){
        result.put(xfTitle, getMarkup(resource.getPath(), request));
      }
    });

    result.put(CSS_RESOURCE_PATHS, clientLibraryPaths(LibraryType.CSS, request));
    result.put(JS_RESOURCE_PATHS, clientLibraryPaths(LibraryType.JS, request));
    result.put(CREATED_AT, dateFormatter.format(new Date().getTime()));

    return result;
  }

  private String getXFMap(final Resource resource) {
    String jcrTitle = StringUtils.EMPTY;
    final String xfvariation = Optional.ofNullable(resource)
            .map(resource1 -> resource1.getChild("jcr:content"))
            .filter(resource1 -> resource1 != null)
            .map(resource1 -> resource1.getValueMap().get("cq:xfVariantType", String.class))
            .filter(s -> "web".equals(s))
            .orElse("notweb");

    if("web".equals(xfvariation)){
      jcrTitle = Optional.ofNullable(resource)
              .map(resource1 -> resource1.getChild("jcr:content"))
              .map(Resource::getValueMap)
              .map(valueMap -> valueMap.get("jcr:title", String.class))
              .orElse(StringUtils.EMPTY);
    }else {
      logger.error("The selected XF path is not a variation ::  XF path :: {} ", resource.getPath());
    }
    return jcrTitle;
  }

  private List<String> clientLibraryPaths(
      final LibraryType css, final SlingHttpServletRequest request) {
    return ClientLibraryUtils.clientLibraryPaths(getNavigationCategories(), css, request);
  }

  protected String getMarkup(final String path, final SlingHttpServletRequest request) {
    return MarkupUtils.getMarkup(
        path, request.getResourceResolver(), requestResponseFactory, requestProcessor);
  }

  private String[] getHeaderXFPath() {
    return Optional.ofNullable(navigationConfig)
        .map(NavigationConfig::headerXF)
        .orElse(null);
  }

  private String[] getFooterXFPath() {
    return Optional.ofNullable(navigationConfig)
        .map(NavigationConfig::footerXF)
        .orElse(null);
  }

  private String[] getNavigationCategories() {
    return Optional.ofNullable(navigationConfig)
        .map(NavigationConfig::navigationCategories)
        .orElse(null);
  }
}
