package com.dish.dopweb.core.models;

import java.util.List;

/** Leadgen Genesis sling model interface. */
public interface LeadgenGenesis {

  /** Get published zipcodes list stored in aem tags. */
  String getZipcodes();

  /** Get UUID. */
  String getUuid();

  /** Get ErrorDivNameList. */
  List<String> getErrorDivNameList();

  /** Get SuccessDiv. */
  String getSuccessDiv();

  /** Get arr */
  String[] getUuidArr();

}
