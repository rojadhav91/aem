package com.dish.dopweb.core.models;

import com.dish.dopweb.core.utils.ContainerUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import java.util.Iterator;
import java.util.List;

/**
 * Container model to get padding style
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContainerModel {
    private static final String CMP_CONTAINER_PADDING = "cmp-container__padding";
    private static final String DESKTOP_VALUES = "paddingdesktopvalues";
    private static final String DESKTOP_DIRECTION = "desktopdirection";
    private static final String TABLET_VALUES = "paddingtabletvalues";
    private static final String TABLET_DIRECTION = "tabletdirection";
    private static final String MOBILE_VALUES = "paddingmobilevalues";
    private static final String MOBILE_DIRECTION = "mobiledirection";

    @ChildResource
    private Resource desktopPaddingMenuItems;

    @ChildResource
    private Resource tabletPaddingMenuItems;

    @ChildResource
    private Resource mobilePaddingMenuItems;

    public List<String> getDesktopPaddingMenuItems() {
        final Iterator<Resource> desktopIterator = ContainerUtil.getResourceIterator(desktopPaddingMenuItems);
        return ContainerUtil.makeClassList(desktopIterator, DESKTOP_VALUES, DESKTOP_DIRECTION, CMP_CONTAINER_PADDING);
    }


    public List<String> getTabletPaddingMenuItems() {
        final Iterator<Resource> tabletIterator = ContainerUtil.getResourceIterator(tabletPaddingMenuItems);
        return ContainerUtil.makeClassList(tabletIterator, TABLET_VALUES, TABLET_DIRECTION, CMP_CONTAINER_PADDING);
    }

    public List<String> getMobilePaddingMenuItems() {
        final Iterator<Resource> mobileIterator = ContainerUtil.getResourceIterator(mobilePaddingMenuItems);
        return ContainerUtil.makeClassList(mobileIterator, MOBILE_VALUES, MOBILE_DIRECTION, CMP_CONTAINER_PADDING);
    }
}
