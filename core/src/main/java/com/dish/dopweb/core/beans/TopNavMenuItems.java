package com.dish.dopweb.core.beans;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.Optional;

/**
 * TopNavMenuItems sling model.
 **/
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TopNavMenuItems {

    @SlingObject
    private ResourceResolver resourceResolver;

    @ValueMapValue
    private String pagePath;

    @ValueMapValue
    private String pageTitle;

    @ValueMapValue
    private String menuItemType;

    @ValueMapValue
    private String displayLinksOnAuth;

    public String getPagePath() {
        return pagePath;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public String getMenuItemType() {
        return menuItemType;
    }

    public String getDisplayLinksOnAuth() {
        return displayLinksOnAuth;
    }

    /**
     * Get page title.
     * @return pageTitle.
     */
    public String getTitle() {
        return Optional.ofNullable(pageTitle)
                .orElse(Optional.ofNullable(resourceResolver.getResource(pagePath))
                .map(resource -> resource.adaptTo(Page.class))
                .map(Page::getTitle)
                .orElse(""));
    }
}
