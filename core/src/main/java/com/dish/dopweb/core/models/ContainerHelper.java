package com.dish.dopweb.core.models;

import com.day.cq.wcm.api.Page;
import com.dish.dopweb.core.utils.BrandUtil;
import lombok.Getter;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Model(
        adaptables = {Resource.class, SlingHttpServletRequest.class},
        resourceType = "dopweb/components/container",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
/** ContainerHelper class. */
@SuppressWarnings("PMD.UseVarargs")
public class ContainerHelper{
    private static final String BACKGROUND_IMAGE_REF = "backgroundImageReference";
    private static final String BACKGROUND_COLOR = "background-color:";
    private static final String BACKGROUND_GRADIENT = "background:linear-gradient";
    private static final int COLOR_LENGTH = 1;
    private static final String COMMA = ",";
    private static final String SEMICOLON = ";";
    private static final String VAR = "var(";
    private static final String BRACKET_OPEN = "(";
    private static final String BRACKET_CLOSE = ")";

    @ChildResource private Resource imgDesktop;
    @ChildResource private Resource imgTablet;
    @ChildResource private Resource imgPhone;

    @ValueMapValue @Default private String[] coreColor;
    @ValueMapValue @Default private String[] boostColor;
    @ValueMapValue @Default private String[] dishColor;
    @ValueMapValue @Default private String[] ontechColor;
    @ValueMapValue @Default private String[] genesisColor;

    @ChildResource @Getter private Border border;

    @ValueMapValue private String gradient;
    private String containerStyle;

    @ScriptVariable
    private Page currentPage;

    /**
     * This method is used to
     * make a gradient style using two colors.
     * @return gradientStyle
     */
    public String getContainerStyle() {

        List<String[]> allColors = new ArrayList<>();
        if (BrandUtil.getBrand(currentPage.getPath()).equals(BrandUtil.BRAND_BOOST)){
            allColors = List.of(coreColor,boostColor,dishColor,ontechColor);
        }else if (BrandUtil.getBrand(currentPage.getPath()).equals(BrandUtil.BRAND_GENESIS)){
            allColors = List.of(coreColor,genesisColor);
        }

        if (StringUtils.isNotBlank(gradient)){
            allColors.stream()
                    .filter(color -> ArrayUtils.isNotEmpty(color) && color.length>COLOR_LENGTH)
                    .findFirst()
                    .ifPresent(this::setGradientStyle);

        }else{
            allColors.stream()
                    .filter(ArrayUtils::isNotEmpty)
                    .findFirst()
                    .ifPresent(this::setBackgroundColorStyle);
        }

        return containerStyle;
    }

    /**
     * This method create gradient style
     * based on color passed
     * @param color colorArray
     */
    public void setGradientStyle(final String[] color){
        final StringBuilder styleBuilder = new StringBuilder();
        containerStyle = styleBuilder.append(BACKGROUND_GRADIENT).append(BRACKET_OPEN).append(gradient)
                    .append(COMMA).append(VAR)
                    .append(color[0]).append(BRACKET_CLOSE)
                    .append(COMMA).append(VAR)
                    .append(color[1]).append(BRACKET_CLOSE).append(BRACKET_CLOSE).append(SEMICOLON).toString();
    }

    /**
     * This method create background color style
     * based on passed color
     * @param color colorArray
     */
    public void setBackgroundColorStyle(final String[] color){
        final StringBuilder styleBuilder = new StringBuilder();
        containerStyle = styleBuilder.append(BACKGROUND_COLOR).append(VAR)
                .append(color[0]).append(BRACKET_CLOSE).append(SEMICOLON).toString();
    }
    /**
     * Desktop background image.
     *
     * @return bgImage
     */
    public String getBgDesktopImage() {
        return getBackgroundImageUrl(imgDesktop).orElse("");
    }

    /**
     * Tablet background image.
     *
     * @return bgImage
     */
    public String getBgTabletImage() {
        return getBackgroundImageUrl(imgTablet).orElseGet(this::getBgDesktopImage);
    }

    /**
     * Phone background image.
     *
     * @return bgImage
     */
    public String getBgPhoneImage() {
        return getBackgroundImageUrl(imgPhone).orElseGet(this::getBgDesktopImage);
    }

    private Optional<String> getBackgroundImageUrl(final Resource resource) {
        return Optional.ofNullable(resource)
                .map(Resource::getValueMap)
                .map(vm -> vm.get(BACKGROUND_IMAGE_REF, String.class));
    }

    /**
     * Set desktop image with blank
     * image path
     * @param imagePath image path
     * @throws PersistenceException throws when resource commit fail
     */
    public void setImgDesktop(final String imagePath) throws PersistenceException {
        final ModifiableValueMap map = this.imgDesktop.adaptTo(ModifiableValueMap.class);
        if (map!=null){
            map.put(BACKGROUND_IMAGE_REF,imagePath);
        }
        this.imgDesktop.getResourceResolver().commit();
    }

    /**
     * Set gradient value
     * @param gradient gradient value
     */
    public void setGradient(final String gradient) {
        this.gradient = gradient;
    }

}
