package com.dish.dopweb.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** Utility class to get brand name on passing page url. */
public final class BrandUtil {
    public static final String GENESIS = "/content/genesis/";
    public static final String BOOST = "/content/boost/";
    public static final String GENESIS_XF = "/content/experience-fragments/genesis";
    public static final String BOOST_XF = "/content/experience-fragments/boost";

    public static final String BRAND_GENESIS = "genesis";
    public static final String BRAND_BOOST = "boost";

    private static final Map<String, String> BRANDMAP = new ConcurrentHashMap<>();

    private BrandUtil() {
    }

    static {
        BRANDMAP.put(GENESIS, BRAND_GENESIS);
        BRANDMAP.put(BOOST, BRAND_BOOST);
        BRANDMAP.put(GENESIS_XF, BRAND_GENESIS);
        BRANDMAP.put(BOOST_XF, BRAND_BOOST);
    }

    /**
     * to get brand name
     *
     * @param pagePath a path string
     * @return a brand name
     */
    public static String getBrand(final String pagePath) {
        String brand = null;
        for (final Map.Entry<String, String> eachEntry : BRANDMAP.entrySet()) {
            if (StringUtils.startsWith(pagePath, eachEntry.getKey())) {
                brand = eachEntry.getValue();
            }
        }
        return brand;
    }

}
