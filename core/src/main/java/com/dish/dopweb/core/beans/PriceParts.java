package com.dish.dopweb.core.beans;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/***
 * Class to get integral and fractional part of a number stored in string.
 */
@Getter
public class PriceParts {
  public static final String DECIMAL_POINT = ".";
  private final String price;
  private final String integral;
  private final String fractional;

  /***
   * Returns PriceParts object.
   * @param price
   */
  public PriceParts(@NonNull final String price) {
    this.price = price;
    integral = StringUtils.substringBefore(price, ".");
    fractional = StringUtils.substringAfter(price, ".");
  }
}
