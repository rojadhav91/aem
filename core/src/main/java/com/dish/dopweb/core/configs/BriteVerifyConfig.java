package com.dish.dopweb.core.configs;

/**
 * Briteverify config
 */
public interface BriteVerifyConfig {


    /**
     * key for briteverify Boost
     * @return key
     */
    String getBriteVerifyBoostKey();

    /**
     * key for briteverify Genesis
     * @return key
     */
    String getBriteVerifyGenesisKey();

    /**
     * BriteVerify endpoint
     * @return endoint
     */
    String getBriteVerifyEndpoint();

}
