package com.dish.dopweb.core.models;

import com.adobe.acs.commons.reports.api.ReportCellCSVExporter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom Report Cell CSV Exporter.
 */
@Model(adaptables = Resource.class)
public class StringCustomReportCellCSVExporter implements ReportCellCSVExporter {
    @Inject
    private String property1;

    @Inject
    private String property2;

    @Inject
    @Optional
    private String format;

    /**
     * Export the cell to CSV
     * @param result
     * @return
     */
    @Override
    public String getValue(final Object result) {
        final Resource resource = (Resource) result;
        final CustomReportCellValue val = new CustomReportCellValue(resource, property1, property2);

        final List<String> values = new ArrayList<>();
        if (val.getValue() != null && !val.isArray()) {
                values.add(val.getSingleValue());
        }
        if (StringUtils.isNotBlank(format)) {
            for (int i = 0; i < values.size(); i++) {
                values.set(i, String.format(format, values.get(i)));
            }
        }
        return StringUtils.join(values, ";");
    }
}
