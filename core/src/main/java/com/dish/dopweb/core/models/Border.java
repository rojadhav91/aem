package com.dish.dopweb.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.ArrayList;
import java.util.List;

/** Model to represent Border properties. */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Border {

  @ValueMapValue private float width;
  @ValueMapValue private boolean top;
  @ValueMapValue private boolean right;
  @ValueMapValue private boolean left;
  @ValueMapValue private boolean bottom;
  @ValueMapValue private String unit;

  /** Get border styles list. */
  public List<String> getStylesList() {
    final List<String> styles = new ArrayList<>();
    final String propertyValue =
        String.format("%s%s solid var(--container-border-color)", width, unit);

    if (top) {
      styles.add("border-top: " + propertyValue);
    }
    if (right) {
      styles.add("border-right: " + propertyValue);
    }
    if (left) {
      styles.add("border-left: " + propertyValue);
    }
    if (bottom) {
      styles.add("border-bottom: " + propertyValue);
    }

    return styles;
  }

  /**
   * Get css styles separated by semicolon.
   *
   * @return String of border styles.
   */
  public String getStyles() {
    return String.join(";", getStylesList());
  }
}
