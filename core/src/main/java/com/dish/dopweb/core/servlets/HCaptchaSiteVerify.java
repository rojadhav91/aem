package com.dish.dopweb.core.servlets;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import com.dish.dopweb.core.configs.HCaptchaConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** HCaptcha Servlet to call site verify endpoint */
@Component(
        service = {Servlet.class},
        property = {
                ServletResolverConstants.SLING_SERVLET_NAME + "=" + "HCaptcha call to SiteVerify endpoint",
                Constants.SERVICE_DESCRIPTION
                        + "="
                        + "Call to SiteVerify endpoint",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST,
                ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + "=genesis/components/leadgengenesis",
                ServletResolverConstants.SLING_SERVLET_SELECTORS + "=siteverify",
                ServletResolverConstants.SLING_SERVLET_EXTENSIONS + "=api"
        })
@Slf4j
public class HCaptchaSiteVerify  extends SlingAllMethodsServlet {

    @Reference
    private transient HCaptchaConfig hCaptchaConfig;

    private static final long serialVersionUID = 1L;
    protected static final String VERIFY_URL = "https://hcaptcha.com/siteverify";
    public static final String SECRET = "secret";
    public static final String RESPONSE = "response";
    public static final String CLIENT_RESPONSE = "hCaptchaResponse";
    public static final String APPLICATION_JSON = "application/json";

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
        validateSiteVerify(request, response);
    }

    private void validateSiteVerify(final SlingHttpServletRequest request, final SlingHttpServletResponse response ) throws IOException {
        response.setContentType(APPLICATION_JSON);
        response.setCharacterEncoding("UTF-8");
        final String hCaptchaResponse = request.getParameter(CLIENT_RESPONSE);
        log("HCaptcha Response Parameter: " + hCaptchaResponse);
    
        final HttpPost httpPost = new HttpPost(VERIFY_URL);
        final List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair(SECRET, hCaptchaConfig.getHCaptchaSiteSecretKey()));
        urlParameters.add(new BasicNameValuePair(RESPONSE, hCaptchaResponse));
        httpPost.setEntity(new UrlEncodedFormEntity(urlParameters, Consts.UTF_8));
    
        try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
            final String siteVerifyResponse = EntityUtils.toString(httpResponse.getEntity());
            log("HCaptcha SiteVerify Response: " + siteVerifyResponse);
            response.getWriter().write(siteVerifyResponse);
        }
    }
}
