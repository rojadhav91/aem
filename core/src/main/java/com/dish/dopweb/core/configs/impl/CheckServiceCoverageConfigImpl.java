package com.dish.dopweb.core.configs.impl;

import com.dish.dopweb.core.configs.CheckServiceCoverageConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration Service Implementation
 */
@Component(service = CheckServiceCoverageConfig.class, immediate = true)
@Designate(ocd = CheckServiceCoverageConfigImpl.CheckServiceCoverageConfig.class, factory = false)
@ServiceDescription("A service used to configure Service Coverage API")
@ServiceVendor("Dish DOP")
public class CheckServiceCoverageConfigImpl implements CheckServiceCoverageConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String checkServiceCoverageEndpoint;

    /**
     * Set configuration
     * @param config
     */
    @Activate
    public void activate(final CheckServiceCoverageConfigImpl.CheckServiceCoverageConfig config) {

        checkServiceCoverageEndpoint = config.checkServiceCoverageEndpoint();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Check Service Coverage Configuration",
            description = "The configuration for checking availability of coverage")
    public @interface CheckServiceCoverageConfig {

        @AttributeDefinition(name = "Service Coverage Endpoint", description = "Service Coverage Api Endpoint")
        String checkServiceCoverageEndpoint() default  "";
    }
}
