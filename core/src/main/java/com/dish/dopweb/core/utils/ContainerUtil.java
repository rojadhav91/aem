package com.dish.dopweb.core.utils;

import org.apache.sling.api.resource.Resource;

import java.util.*;

/**
 * Util class for container styles
 */
final public class ContainerUtil {
    private static final String HYPHEN = "-";

    private ContainerUtil() {
    }

    /**
     *  The method is to return resource iterator
     * @param resource
     * @return
     */
    public static Iterator<Resource> getResourceIterator(final Resource resource){
        return Optional.ofNullable(resource)
                .map(Resource::listChildren)
                .orElse(Collections.emptyIterator());
    }

    /**
     * This method is written to retun a class list for data attribute.
     * @param desktopIterator
     * @param values
     * @param directions
     * @param classPrefix
     * @return
     */
    public static List<String> makeClassList(final Iterator<Resource> desktopIterator, final String values, final String directions, final String classPrefix) {
        final List<String> containerValueList = new ArrayList<>();
        desktopIterator.forEachRemaining(resource -> {
            final String desktopvalues = resource.getValueMap().get(values, String.class);
            final String direction = resource.getChild(directions).getValueMap().get("direction", String.class);
            final String paddingClass = constructPaddingClass(desktopvalues, direction, classPrefix);
            containerValueList.add(paddingClass);
        });
        return containerValueList;
    }

    /**
     * This method is written to construct class name
     * @param desktopvalues
     * @param direction
     * @param classPrefix
     * @return
     */
    public static String constructPaddingClass(final String desktopvalues, final String direction, final String classPrefix) {
        final StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(classPrefix)
                .append(HYPHEN)
                .append(direction)
                .append(HYPHEN)
                .append(desktopvalues).toString();
    }
}

