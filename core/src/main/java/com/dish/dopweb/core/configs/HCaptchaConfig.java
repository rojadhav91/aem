package com.dish.dopweb.core.configs;

/**
 * Configuration for Check Service Coverage
 */
public interface HCaptchaConfig {

    /**
     * Check Service Coverage Endpoint
     * @return endpoint
     */
    String getHCaptchaSiteSecretKey();
}
