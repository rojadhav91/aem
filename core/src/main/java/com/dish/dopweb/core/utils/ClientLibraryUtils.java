package com.dish.dopweb.core.utils;

import com.adobe.acs.commons.util.ModeUtil;
import com.adobe.cq.wcm.core.components.models.ClientLibraries;
import com.adobe.granite.ui.clientlibs.LibraryType;
import com.day.cq.commons.Externalizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/** Utility class to get client libraries paths. */
@Slf4j
public final class ClientLibraryUtils {

  private static Externalizer externalizer;

  public static final String BOOST_EXTERNALIZER_KEY = "boost";

  /** Request header used to determine whether the request is from CloudFront. */
  public static final String USER_AGENT = "User-Agent";

  /** USER_AGENT header value for CloudFront URLs. */
  public static final String CLOUDFRONT = "Amazon CloudFront";

  private ClientLibraryUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Get Clientlibrary paths for specified category and specified type.
   *
   * @param categories
   * @param type
   * @param request
   * @return client libraries list.
   */
  public static List<String> clientLibraryPaths(
      final String[] categories, final LibraryType type, final SlingHttpServletRequest request) {

    externalizer = request.getResourceResolver().adaptTo(Externalizer.class);

    final List<String> clientLibraryPaths = new ArrayList<>();
    for (final String category : categories) {
      clientLibraryPaths.addAll(clientLibraryPath(category, type, request));
    }

    if (clientLibraryPaths.isEmpty()) {
      final String categoriesString = Arrays.toString(categories);
      log.error("Could not find any client libraries for {} categories", categoriesString);
    }
    return clientLibraryPaths;
  }

  private static List<String> clientLibraryPath(
      final String category, final LibraryType type, final SlingHttpServletRequest request) {

    request.setAttribute("categories", category);
    final ClientLibraries clientLibraries = request.adaptTo(ClientLibraries.class);
    List<String> paths =
        Optional.ofNullable(clientLibraries)
            .map(clientLibraries1 -> getSourcePaths(clientLibraries1, type))
            .map(Arrays::asList)
            .orElse(Collections.emptyList());

    if (paths.isEmpty()) {
      log.warn("Could not find {} client library for {} category", type, category);
    } else {
      final String getAbsolutePaths = request.getParameter("getAbsolutePaths");
      final String yes = "yes";
      if (Objects.nonNull(getAbsolutePaths) && yes.equals(getAbsolutePaths)) {
        final List<String> absolutePaths = new ArrayList<>();
        for (final String path : paths) {
          absolutePaths.add(getAbsolutePath(path, request));
        }
        paths = absolutePaths;
      }
    }
    return paths;
  }

  private static String[] getSourcePaths(
      final ClientLibraries clientLibraries, final LibraryType type) {
    String[] strings = new String[0];

    if (type.equals(LibraryType.CSS)) {
      final String cssIncludes = clientLibraries.getCssIncludes();
      strings = StringUtils.substringsBetween(cssIncludes, "href=\"", "\"");
    } else if (type.equals(LibraryType.JS)) {
      final String jsIncludes = clientLibraries.getJsIncludes();
      strings = StringUtils.substringsBetween(jsIncludes, "src=\"", "\"");
    }

    return strings;
  }

  private static String getAbsolutePath(final String path, final SlingHttpServletRequest request) {
    String externalLink;

    try (ResourceResolver resourceResolver = request.getResourceResolver()) {
      if (isCloudFrontUrl(request)) {
        externalLink = externalizer.externalLink(resourceResolver, BOOST_EXTERNALIZER_KEY, path);
      } else {
        externalLink =
            ModeUtil.isAuthor()
                ? externalizer.authorLink(resourceResolver, path)
                : externalizer.publishLink(resourceResolver, path);
      }
    }
    return externalLink;
  }

  private static boolean isCloudFrontUrl(final SlingHttpServletRequest request) {
    return Optional.of(request)
        .map(HttpServletRequest::getHeaderNames)
        .map(StreamUtils::stream)
        .map(
            headerNames ->
                headerNames
                    .filter(USER_AGENT::equals)
                    .map(request::getHeader)
                    .map(CLOUDFRONT::contains)
                    .findFirst()
                    .orElse(false))
        .orElse(false);
  }
}
