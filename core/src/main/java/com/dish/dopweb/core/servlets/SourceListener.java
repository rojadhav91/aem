package com.dish.dopweb.core.servlets;

import com.dish.dopweb.core.utils.ResourceResolverUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.List;

@Component(
    service = ResourceChangeListener.class,
    property = {
      ResourceChangeListener.PATHS + "=" + "/content/genesis",
      ResourceChangeListener.CHANGES + "=" + "ADDED",
      ResourceChangeListener.CHANGES + "=" + "CHANGED",
      ResourceChangeListener.CHANGES + "=" + "REMOVED"
    })
@Slf4j
@SuppressWarnings("PMD")
public class SourceListener implements ResourceChangeListener {
  private final  Logger logger = LoggerFactory.getLogger(SourceListener.class);

  @Reference private ResourceResolverFactory resourceResolverFactory;
  private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
  private static final String NODE = "genesis-leadgenform";

  @Override
  public void onChange(List<ResourceChange> list) {
    list.forEach(
        resourceChange -> {
          if (resourceChange.getPath().contains(NODE)) {
            final ResourceResolver serviceResourceResolver =
                ResourceResolverUtil.getServiceResourceResolver(resourceResolverFactory, APP_RESOURCE_SERVICE);
            final Resource resource = serviceResourceResolver.getResource(resourceChange.getPath());
            if (resource.getName().contains(NODE)) {
              setResourceType(serviceResourceResolver, resource);
            } else if (resource.getParent().getName().contains(NODE)) {
              setResourceType(serviceResourceResolver, resource.getParent());
            }
          }
        });
  }

  private void setResourceType(
      final ResourceResolver serviceResourceResolver, final Resource resource) {
    try {
      if (resource != null) {
        final Node node = resource.adaptTo(Node.class);
        if (node != null) {
          node.setProperty("sling:resourceType", "genesis/components/leadgengenesis");
          serviceResourceResolver.commit();
        }
      }
    } catch (RepositoryException | PersistenceException e) {
      logger.error(e.getMessage());
    }
  }
}
