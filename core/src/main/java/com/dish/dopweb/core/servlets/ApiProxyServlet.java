package com.dish.dopweb.core.servlets;

import com.day.cq.commons.jcr.JcrConstants;
import com.dish.dopweb.core.configs.BriteVerifyConfig;
import com.dish.dopweb.core.configs.CFMarketsConfigService;
import com.dish.dopweb.core.configs.DomainWhitelistConfig;
import com.dish.dopweb.core.configs.OmniSendConfig;
import com.dish.dopweb.core.configs.ResponsysConfig;
import com.dish.dopweb.core.configs.MagentoAPIConfigService;
import com.dish.dopweb.core.configs.ApigeeAuthTokenRequestConfig;
import com.dish.dopweb.core.configs.CheckServiceCoverageConfig;
import com.dish.dopweb.core.configs.CheckServiceCoverageCaConfig;
import com.dish.dopweb.core.models.AddressModel;
import com.dish.dopweb.core.utils.ResourceResolverUtil;
import com.dish.dopweb.core.utils.TagUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.Servlet;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static javax.servlet.http.HttpServletResponse.SC_OK;

/** Proxy Servlet for LeadGen API calls */
@Component(
    service = {Servlet.class},
    property = {
      ServletResolverConstants.SLING_SERVLET_NAME + "=" + "Proxy Servlet for LeadGen API calls",
      Constants.SERVICE_DESCRIPTION
          + "="
          + "Makes API calls to BriteVerify and Responsys for Leadgen form",
      ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST,
      ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET,
      ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + "=genesis/components/leadgengenesis",
      ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + "=dopweb/components/leadgen",
      ServletResolverConstants.SLING_SERVLET_SELECTORS + "=leadgen",
      ServletResolverConstants.SLING_SERVLET_EXTENSIONS + "=api"
    })
@SuppressWarnings({"PMD.TooManyMethods", "PMD.GodClass","PMD.CommentSize", "PMD.CyclomaticComplexity"})
public class ApiProxyServlet extends SlingAllMethodsServlet {
  public static final String STATUS = "status";
  public static final String AUTHORIZATION = "Authorization";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String VALID = "valid";
  public static final String UNKNOWN = "unknown";
  public static final String ACCEPT_ALL = "accept_all";
  public static final String EMAIL = "email";
  public static final String DISPOSABLE = "disposable";
  public static final String ROLE_ADDR = "role_address";
  public static final String API_KEY = "ApiKey: ";
  public static final String APPLICATION_JSON = "application/json";
  public static final String SUCCESS = "success";
  public static final String FAILURE = "failure";
  public static final String BOOST = "boost";
  public static final String OMNISEND_PAYLOAD = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\" } } }],\"postalCode\":\"%s\",\"address\":\"%s\",\"customProperties\":%s}";
  public static final String OMNISEND_PAYLOAD_CITY = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":" +
                                                "{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\" } } }]," +
                                                "\"address\":\"%s\",\"city\":\"%s\"," +
                                                "\"state\":\"%s\",\"postalCode\":\"%s\",\"customProperties\":%s}";
  public static final String OMNISEND_BODY_SAMPLE =
          "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"customProperties\":{\"invalidZip\":\"%b\"}}";
  public static final String OMNISEND_BODY_SAMPLE2 =
          "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"address\":\"%s\"}";
  public static final String GENESIS = "genesis";
  public static final String X_API_KEY = "X-API-KEY";
  public static final String API = "api";
  public static final String BRITE_VERIFY = "briteVerify";
  public static final String OMNISEND = "omnisend";
  public static final String TRUE = "true";
  public static final String RESPONSYS = "responsys";
  public static final String RI_FORM =
          "X0Gzc2X%3DAQpglLjHJlTQGzbRzeeL2Oc77zcYouOggzd3merhGEYs0IPpVXMtX%3DAQpglLjHJlTQGt6DnzdgWszdAzgR58NtnGWzezaip21DTkWr1";
  public static final String EI_FORM = "Elsz0qdiyvKBnO8rGTLINZE";
  public static final String DI_FORM = "rd4p2nj5mbo453v922sebr0j0vd0r7khlq1ssbq925no11g3odk0";
  public static final String EMAIL_PERMISSION_STATUS = "I";
  public static final String SITE = "site";
  public static final String ADDRESS = "address";
  public static final String ZIP = "zip";
  public static final String ERROR_MSG = "errorMsg";
  public static final String PHONE = "phone";
  public static final String CONSENT = "consent";
  private static final long serialVersionUID = -4341410431061866912L;
  private static final String APP_RESOURCE_SERVICE = "apps-resources-service";
  private static final String ZIPCODES_TAGS_PATH = "/content/cq:tags/genesis/markets";
  private static final String BETA_USER_EMAILS_PATH = "/content/cq:tags/genesis/beta-users-email";
  private static final String QP_ZIPCODES = "zipcodes";
  private static final String NO_ZIPCODES_FOUND = "No zipcodes found";
  public static final String BEARER = "Bearer ";
  public static final String API_RESPONSE_STATUS_CODE = "apiResponseStatusCode";
  public static final String API_RESPONSE_MESSAGE = "apiResponseMessage";
  public static final String SUCCEEDED ="succeeded";
  private final transient Logger logger = LoggerFactory.getLogger(ApiProxyServlet.class);

  @Reference
  private transient BriteVerifyConfig briteVerifyConfig;
  @Reference
  private transient OmniSendConfig omniSendConfig;
 //@Reference
 // private transient SmartyStreetConfig smartyStreetConfig;
  @Reference
  private transient ResponsysConfig responsysConfig;
  @Reference
  private transient ResourceResolverFactory resourceResolverFactory;
  @Reference
  private transient DomainWhitelistConfig domainWhitelistConfig;
  @Reference
  private transient MagentoAPIConfigService magentoAPIConfigService;
  @Reference
  private transient SlingSettingsService slingSettingsService;
  @Reference
  private transient ApigeeAuthTokenRequestConfig apigeeAuthTokenRequestConfig;
  @Reference
  private transient CheckServiceCoverageConfig checkServiceCoverageConfig;
  @Reference
  private transient CFMarketsConfigService cfMarketsConfigService;

  public static final String INVALID_ZIP_PAYLOAD = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"customProperties\":{\"flowError\":\"Invalid zip\"}}";
  public static final String INVALID_EMAIL_PAYLOAD = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"customProperties\":{\"flowError\":\"Email not whitelisted\"}}";
  public static final String EMAIL_IN_SYSTEM_PAYLOAD = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"customProperties\":{\"flowError\":\"Email already in the system\"}}";
  public static final String SIGNUP_PAYLOAD = "{\"createdAt\":\"%s\",\"tags\":[\"source: AEM Genesis\"],\"identifiers\":[{\"type\":\"email\",\"id\":\"%s\",\"channels\":{\"email\":{\"status\":\"subscribed\",\"statusDate\":\"%s\"}}}],\"postalCode\":\"%s\",\"customProperties\":{\"signup\":\"true\"}}";
  public static final String EMAIL_WHITELIST_ENABLED = "emailWhitelist";
  public static final String BE_VALIDATION_ENABLED = "beValidation";
  public static final String SIGNUP_ENABLED = "signupEnabled";
  public static final String ERROR_CODE = "errorCode";
  public static final String BE_ZIP_CODE_VALIDATION_ENABLED = "zipValidation";
  public static final String BE_EMAIL_VALIDATION_ENABLED = "emailValidation";
  public static final String SERVICE_AVAILABILITY_ENABLED = "serviceAvailability";


  /**
   * Will make Post call
   *
   * @param request  request
   * @param response response
   */
  @Override
  protected void doPost(
          final SlingHttpServletRequest request, final SlingHttpServletResponse response)
          throws IOException {
    makeLeadGenApiCalls(request, response);
  }

  @Override
  protected void doGet(
          final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    zipcodes(request, response);
  }

  private void zipcodes(
          final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    final String zipcodes =
            Optional.ofNullable(request.getParameter(QP_ZIPCODES))
                    .filter(TRUE::equals)
                    .map(param -> zipcodes())
                    .map(zips -> zips.isEmpty() ? NO_ZIPCODES_FOUND : zips)
                    .map(zips -> new Gson().toJson(zips))
                    .orElse("Invalid parameter");

    try {
      response.getWriter().write(zipcodes);
    } catch (IOException e) {
      logger.error("Cannot write to response: {}", e.getMessage());
    }
  }

  private List<String> zipcodes() {
     return TagUtils.getZipCodesByRunMode(ZIPCODES_TAGS_PATH, resourceResolver(), slingSettingsService);
  }

  private List<String> betaUserEmails() {
    return TagUtils.getTagTitles(BETA_USER_EMAILS_PATH, resourceResolver());
  }

  protected ResourceResolver resourceResolver() {
    return ResourceResolverUtil.getServiceResourceResolver(
            resourceResolverFactory, APP_RESOURCE_SERVICE);
  }

  private void makeLeadGenApiCalls(
          final SlingHttpServletRequest request, final SlingHttpServletResponse response)
          throws IOException {
    final String site = request.getParameter(SITE);
    final String email = request.getParameter(EMAIL);
    final String zipCode = request.getParameter(ZIP);

    response.setContentType(APPLICATION_JSON);
    response.setCharacterEncoding("UTF-8");

    if (BOOST.equals(site)) {
      final JsonObject jsonObject = new JsonObject();
      final String briteVerifyResponse = makeBriteVerifyApiCall(email, site);

      if (SUCCESS.equals(briteVerifyResponse)) {
        final String responsysResponse = makeResponsysApiCall(email, zipCode);
        if (SUCCESS.equals(responsysResponse)) {
          jsonObject.addProperty(STATUS, SUCCESS);
          jsonObject.addProperty(API, RESPONSYS);
        } else {
          jsonObject.addProperty(STATUS, FAILURE);
          jsonObject.addProperty(API, RESPONSYS);
        }
      } else {
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(API, BRITE_VERIFY);
      }

      response.getWriter().write(jsonObject.toString());
    } else if (GENESIS.equals(site)) {
      final JsonObject jsonObject = handleGenesisLeadgenForm(request);
      if (jsonObject.get(STATUS).getAsString().equals(FAILURE)) {
        response.setStatus(400);
      }
      response.getWriter().write(jsonObject.toString());
    }
  }

  private boolean isValidZipcode(final String zipCode) {
    return cfMarketsConfigService.isValidZipcode(zipCode);
  }

  private boolean isBetaUserEmail(final String email) {
    return betaUserEmails().stream().anyMatch(email::equalsIgnoreCase);
  }

  @SuppressWarnings({"PMD.CyclomaticComplexity", "PMD.EmptyIfStmt"})
  private JsonObject handleGenesisLeadgenForm(final SlingHttpServletRequest request) {

    final String site = request.getParameter(SITE);
    final String email = request.getParameter(EMAIL);
    final String zipCode = request.getParameter(ZIP);
    final String address = request.getParameter(ADDRESS);
    final String signupEnabled = request.getParameter(SIGNUP_ENABLED);
    final String emailWhitelistEnabled = request.getParameter(EMAIL_WHITELIST_ENABLED);
    final String backendValidationEnabled = request.getParameter(BE_VALIDATION_ENABLED);
    final String emailValidationEnabled = request.getParameter(BE_EMAIL_VALIDATION_ENABLED);
    final String zipCodeValidationEnabled = request.getParameter(BE_ZIP_CODE_VALIDATION_ENABLED);
    final String serviceAvailabilityEnabled = request.getParameter(SERVICE_AVAILABILITY_ENABLED);

    final String currentPagePath = request.getRequestPathInfo().getResourcePath().split(JcrConstants.JCR_CONTENT)[0];
    Resource resource;
    String brand = null;
    String network = null;

    if (currentPagePath != null) {
      try(ResourceResolver resourceResolver = request.getResourceResolver()){
        resource = resourceResolver.getResource(currentPagePath);
        brand = Optional.ofNullable(resource)
                .map(resource1 -> resource1.adaptTo(ConfigurationBuilder.class))
                .map(cb -> cb.as(CheckServiceCoverageCaConfig.class))
                .map(CheckServiceCoverageCaConfig::getBrand)
                .filter(StringUtils::isNotBlank)
                .orElse("");

        network = Optional.ofNullable(resource)
                .map(resource1 -> resource1.adaptTo(ConfigurationBuilder.class))
                .map(cb -> cb.as(CheckServiceCoverageCaConfig.class))
                .map(CheckServiceCoverageCaConfig::getNetwork)
                .filter(StringUtils::isNotBlank)
                .orElse("");

      }
    }
    logger.info("Brand: {}", brand);
    logger.info("Network: {}", network);

    JsonObject jsonObject = new JsonObject();

    if (Boolean.parseBoolean(backendValidationEnabled)) {
      jsonObject = backendValidations(emailValidationEnabled, emailWhitelistEnabled, zipCodeValidationEnabled, serviceAvailabilityEnabled, email, site, zipCode, address, brand, network);
    } else { // Backend validation disabled
      final boolean isSignupEnabled = Boolean.parseBoolean(signupEnabled);
      final String customProperty = isSignupEnabled ? "{\"signUp\":\"true\"}" : "{\"signUp\":\"false\"}";
      if (newMakeOmniSendApiCall(email, zipCode, address, customProperty).equals(FAILURE) && isSignupEnabled) {
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "signup-error");
      } else { // Signup success
        jsonObject.addProperty(STATUS, SUCCESS);
      }
    }

    logger.debug("returning JSONOBJECT : {}", jsonObject);
    return jsonObject;
  }

  @SuppressWarnings({"PMD.ExcessiveParameterList"})
  private JsonObject backendValidations(final String emailValidationEnabled, final String emailWhitelistEnabled,
          final String zipCodeValidationEnabled, final String serviceAvailabilityEnabled, final String email, final String site,
          final String zipCode, final String address, final String brand, final String network) {
    JsonObject jsonObject = emailValidations(emailValidationEnabled, emailWhitelistEnabled, email, site, zipCode, address);
    boolean failureFlag = jsonObject.get(STATUS).getAsString().equals(FAILURE);
    
    if (!failureFlag && Boolean.parseBoolean(zipCodeValidationEnabled)) { // Zipcode Validation Enabled and no error yet
      final boolean validZipcode = isValidZipcode(zipCode);
      if (validZipcode) { // Valid zipcode
        jsonObject.addProperty(STATUS, SUCCESS);
      } else { // Invalid zipcode
        newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Invalid zip\"}");
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "invalid-zip");
        failureFlag = true;
      }
    }

    if (!failureFlag && StringUtils.isNotBlank(address)) {
      jsonObject = addressValidations(address, serviceAvailabilityEnabled, email, zipCode, brand, network);
      failureFlag = jsonObject.get(STATUS).getAsString().equals(FAILURE);
    }

    if (!failureFlag) { // No error at all then save in omnisend
      if(jsonObject.has("city")){
        newMakeOmniSendApiCall(email, jsonObject.get("state").getAsString(),jsonObject.get("city").getAsString(),zipCode,address,null);
      } else {
        newMakeOmniSendApiCall(email, zipCode, address, null);
      }
    }
    
    return jsonObject;
  }
  
  private JsonObject emailValidations(final String emailValidationEnabled, final String emailWhitelistEnabled, final String email, final String site,
          final String zipCode, final String address) {
    boolean failureFlag = false;
    final JsonObject jsonObject = new JsonObject();
    if (Boolean.parseBoolean(emailValidationEnabled)) { // Email Validation enabled
      final boolean validEmail = makeBriteVerifyApiCall(email, site).equals(SUCCESS);
      if (validEmail) { // Valid email
        jsonObject.addProperty(STATUS, SUCCESS);
      } else { // Invalid email
        newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Invalid email\"}");
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "invalid-email");
        failureFlag = true;
      }
    }
    
    if (!failureFlag && Boolean.parseBoolean(emailWhitelistEnabled)) { // If email whitelist enable and no error yet
      if (isBetaUserEmail(email)) { // Email whitelisted
        jsonObject.addProperty(STATUS, SUCCESS);
      } else { // Email not whitelisted
        newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Email not in whitelist\"}");
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "email-not-whitelisted");
        failureFlag = true;
      }
    }
    
    if (!failureFlag) { // Check email exists if no error yet
      if (StringUtils.isNotBlank(email) && checkWithMagentoEmailExist(email).equals(SUCCESS)) { // Email exists on Magento
        newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Email already in system\"}");
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "email-already-exist");
      } else { // Email doesn't exist on Magento
        jsonObject.addProperty(STATUS, SUCCESS);
      }
    }
    
    return jsonObject;
  }
/*
  private void updateResponseJson(final AddressModel addressModel, final JsonObject jsonObject) {
    jsonObject.addProperty(STATUS, SUCCESS);
    jsonObject.addProperty("address", addressModel.getAddress1());
    jsonObject.addProperty("address2", addressModel.getAddress2());
    jsonObject.addProperty("city", addressModel.getComponents().getCity());
    jsonObject.addProperty("state", addressModel.getComponents().getState());
  }*/

  private JsonObject addressValidations(final String address, final String serviceAvailabilityEnabled, final String email, final String zipCode, final String brand,
                                        final String network) {
    final JsonObject jsonObject = new JsonObject();

    logger.debug("Address provided in input :{}", address);
    final AddressModel[] addressModels = validateSmartystreetAddress(address);

    if (addressModels != null && addressModels.length > 0) {
      logger.info("method : validateSmartystreetAddress : response from smarty street {}", addressModels[0].toString());

      jsonObject.addProperty(STATUS, SUCCESS);
      jsonObject.addProperty("address", addressModels[0].getAddress1());
      jsonObject.addProperty("address2", addressModels[0].getAddress2());
      jsonObject.addProperty("city", addressModels[0].getComponents().getCity());
      jsonObject.addProperty("state", addressModels[0].getComponents().getState());

      //Check Service coverage
      if (Boolean.parseBoolean(serviceAvailabilityEnabled) && serviceCoverage(brand, network, addressModels).equals(FAILURE)) {
        logger.debug("Service availability error");
        newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Service availability error\"}");
        jsonObject.addProperty(STATUS, FAILURE);
        jsonObject.addProperty(ERROR_CODE, "service-availability-error");
      }
    } else {
      logger.debug("invalid response from smarty street ");
      newMakeOmniSendApiCall(email, zipCode, address, "{\"flowError\":\"Invalid address\"}");
      jsonObject.addProperty(STATUS, FAILURE);
      jsonObject.addProperty(ERROR_CODE, "invalid-address");
    }
    return jsonObject;
  }

  private AddressModel[] validateSmartystreetAddress(final String address) {
    AddressModel[] addressModel = null;
    try {
      logger.debug("method : validateSmartystreetAddress : start");
      //System.out.println("start"+address);
      final String url ="https://us-street.api.smartystreets.com/street-address"+ //smartyStreetConfig.getSmartyStreetEndpoint()+
              "?key="+ "33843130097947213" + //smartyStreetConfig.getApiKey() +
              "&street=" + URLEncoder.encode(address, "UTF-8") +
              "&address-type=us-street-freeform";
      //final ObjectMapper mapper = new ObjectMapper();
      logger.debug("method : validateSmartystreetAddress : url {}",url);
      //System.out.println("url"+url);
      try (CloseableHttpClient client = HttpClients.createDefault()) {

        final HttpGet request = new HttpGet(url);
        request.addHeader("Referer","https://commerce-test.dish.com");

        final ResponseHandler<AddressModel[]> responseHandler = httpResponse -> {
          final int status = httpResponse.getStatusLine().getStatusCode();
          AddressModel[] responseModels = null;
          if (status >=200 && status <300){
            final HttpEntity entity = httpResponse.getEntity();
            //System.out.println("url"+entity.toString());
            logger.debug("method : validateSmartystreetAddress : ENTiTY: response from smarty street {}",entity.toString());
            //responseModels = updateAddressComponents(entity);
            final Gson gson = new Gson();
            //final String responseMsg = CharStreams.toString(new InputStreamReader(httpEntity.getContent(), StandardCharsets.UTF_8));
            final String responseMsg = EntityUtils.toString(entity);
            logger.debug("response message from smartstreet : {}",responseMsg);
            //System.out.println("response message : "+responseMsg);
            responseModels = gson.fromJson(responseMsg, AddressModel[].class);
          }else {
            //System.out.println("url status "+status);
            logger.debug("method : validateSmartystreetAddress : error response in ELSE  : :response from smarty street {}",status);
          }
          return  responseModels;
        };
        logger.debug("excecuting request {}",request);
        addressModel = client.execute(request, responseHandler);

      }
    }catch (MalformedURLException e){
      logger.error("Exception while validating address - street smart",e);
      //System.out.println("MalformedURLException"+e.getMessage());
    } catch (IOException e) {
      logger.error("IOException while validating address - street smart",e);
      //System.out.println("IO URLException"+e.getMessage());
    }
    if(null !=addressModel){
      logger.info("method : validateSmartystreetAddress : returning  {}: end", addressModel.length);
    }

    //System.out.println("returning AM "+addressModel);
    return addressModel;
  }
/*
  private AddressModel[] updateAddressComponents(final HttpEntity httpEntity){
    AddressModel[] addressModels=null;
    try {
      logger.info("method : updateAddressComponents : start");
      System.out.println("method : updateAddressComponents : start");
      final Gson gson = new Gson();
      //final String responseMsg = CharStreams.toString(new InputStreamReader(httpEntity.getContent(), StandardCharsets.UTF_8));
      final String responseMsg = EntityUtils.toString(httpEntity);
      System.out.println("response message : "+responseMsg);
      addressModels = gson.fromJson(responseMsg, AddressModel[].class);
    } catch (IOException e){
      logger.error("Exception while extracting address components",e);
      System.out.println("IOEXep"+e.getMessage());
    }
    logger.info("method : updateAddressComponents : returning null : end");
    return  addressModels;

  }*/

  /*
  private String makeOmniSendApiCall(final String email, final String zipCode, final String payload) {
    final HttpPost httpPost = new HttpPost(omniSendConfig.getOmniSendEndpoint());
    httpPost.addHeader(X_API_KEY, omniSendConfig.getOmniSendKey());
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    final String zuluTime = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
    //    String smsConsent;
    //    if (StringUtils.isEmpty(consent)) {
    //      smsConsent = "subscribed";
    //    } else {
    //      if (TRUE.equals(consent)) {
    //        smsConsent = "subscribed";
    //      } else {
    //        smsConsent = "nonSubscribed";
    //      }
    //    }
    final String omnisendPayload =
            String.format(payload, zuluTime, email, zuluTime, zipCode, null);

    logger.debug("OmniSend Payload is {}", omnisendPayload);
    String result = FAILURE;
    try {
      httpPost.setEntity(new StringEntity(omnisendPayload));
      final String apiResponse = makePostRequestApi(httpPost);
      if (StringUtils.isNotEmpty(apiResponse)) {
        logger.error("Success Omnisend");
        result = SUCCESS;
      } else {
        logger.error("Omnisend API call failure ");
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("Omnisend httpPost set entity", e);
    }
    return result;
  }
*/
  /*
  private String makeOmniSendApiCallWithAddress(
          final String email, final String zipCode, final String address) {
    final HttpPost httpPost = new HttpPost(omniSendConfig.getOmniSendEndpoint());
    httpPost.addHeader(X_API_KEY, omniSendConfig.getOmniSendKey());
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    final String zuluTime = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

    final String omnisendPayload =
            String.format(OMNISEND_BODY_SAMPLE2, zuluTime, email, zuluTime, zipCode, address);

    logger.debug("OmniSend Payload is {}", omnisendPayload);
    String result = FAILURE;
    try {
      httpPost.setEntity(new StringEntity(omnisendPayload));
      final String apiResponse = makePostRequestApi(httpPost);
      if (StringUtils.isNotEmpty(apiResponse)) {
        logger.error("Success Omnisend");
        result = SUCCESS;
      } else {
        logger.error("Omnisend API call failure ");
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("Omnisend httpPost set entity", e);
    }
    return result;
  }
*/

  private String makeResponsysApiCall(final String email, final String zipCode) {
    final HttpPost httpPost = new HttpPost(responsysConfig.getResponsysEndpoint());
    String result = FAILURE;
    final List<NameValuePair> form = new ArrayList<>();
    form.add(new BasicNameValuePair("_ri_", RI_FORM));
    form.add(new BasicNameValuePair("_ei_", EI_FORM));
    form.add(new BasicNameValuePair("_di_", DI_FORM));
    form.add(new BasicNameValuePair("EMAIL_PERMISSION_STATUS_", EMAIL_PERMISSION_STATUS));
    form.add(new BasicNameValuePair("EMAIL_ADDRESS_", email));
    form.add(new BasicNameValuePair("POSTAL_CODE_", zipCode));
    httpPost.setEntity(new UrlEncodedFormEntity(form, Consts.UTF_8));
    final String apiResponse = makePostRequestApi(httpPost);
    if (StringUtils.isNotEmpty(apiResponse)
            && StringUtils.containsIgnoreCase(apiResponse, SUCCESS)) {
      result = SUCCESS;
      logger.debug("Responsys call SUCCESS for email : {}, zip :{}", email, zipCode);
    } else {
      logger.debug(
              "Responsys call FAILURE for email : {}, zip :{}, apiResponse received : {}",
              email,
              zipCode,
              apiResponse);
    }
    return result;
  }

  private String serviceCoverage(final String brand, final String network, final AddressModel... addressModels){
    String result = FAILURE;

    final String token = getAuthToken();
    logger.info(token);

    if (StringUtils.isEmpty(token)) {
      logger.error("Check Service Coverage - Unable to generate auth token ");
    } else {
      final HttpPost httpPost = new HttpPost(checkServiceCoverageConfig.getCheckServiceCoverageEndpoint());
      httpPost.addHeader(AUTHORIZATION, BEARER + " " + token);
      httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);

      try {
        httpPost.setEntity(new StringEntity(String.format("{ \"address\": { \"zipCode\": \"%s\", \"address1\": \"%s\", \"address2\": \"%s\", \"city\": \"%s\", " +
                        "\"state\": \"%s\", \"country\": \"US\"}, \"brand\": [ \"%s\"], \"networks\": [ \"%s\" ] }", addressModels[0].getComponents().getZipcode(),
                addressModels[0].getAddress1(), addressModels[0].getAddress2(), addressModels[0].getComponents().getCity(), addressModels[0].getComponents().getState(), brand, network)));

        logger.debug("Check service coverage entity: {}", EntityUtils.toString(httpPost.getEntity()));
        final String apiResponse = makePostRequestApi(httpPost);

        logger.debug("Check service coverage response: {}", apiResponse);
        if (StringUtils.isNotEmpty(apiResponse)) {
          final Gson gson = new Gson();
          final JsonObject responseObject = gson.fromJson(apiResponse, JsonObject.class);
          if (SUCCEEDED.equals(responseObject.get("networkResponseStatus").getAsString())) {
            final JsonArray jsonArray = responseObject.get("networkResult").getAsJsonArray();
            final boolean flag = jsonArray.get(0).getAsJsonObject().get("coverageAvailable").getAsBoolean();
            result = flag ? SUCCESS : FAILURE;
          }
        } else {
          logger.error("Check Service Coverage API call failure ");
        }
      } catch (IOException e) {
        logger.error("Check Service Coverage httpPost set entity {}", e.getMessage(), e);
      }
    }
    return result;
  }

  private String getAuthToken() {
    String result = "";
    final HttpPost httpPost = new HttpPost(apigeeAuthTokenRequestConfig.getApigeeAuthTokenEndpoint());

    final String encoding = Base64.getEncoder().encodeToString((apigeeAuthTokenRequestConfig.getClientId() + ":" + apigeeAuthTokenRequestConfig.getClientSecret()).getBytes(StandardCharsets.UTF_8));
    httpPost.addHeader(AUTHORIZATION, "Basic " + encoding);
    httpPost.addHeader(CONTENT_TYPE, "application/x-www-form-urlencoded");

    final EntityBuilder builder = EntityBuilder.create();
    builder.setParameters(
            new BasicNameValuePair("grant_type", "client_credentials"));
    httpPost.setEntity(builder.build());

    final String apiResponse = makePostRequestApi(httpPost);
    if (StringUtils.isNotEmpty(apiResponse)) {
      logger.info(apiResponse);
      final JsonObject jsonObject = new Gson().fromJson(apiResponse, JsonObject.class);
      final JsonElement accessToken = jsonObject.get("access_token");
      result = accessToken.getAsString();
    } else {
      logger.error("Authorization Token call failure ");
    }

    return result;
  }

  @SuppressWarnings("PMD.CyclomaticComplexity")
  private String makeBriteVerifyApiCall(final String email, final String site) {
    final HttpPost httpPost = new HttpPost(briteVerifyConfig.getBriteVerifyEndpoint());
    if (BOOST.equals(site)) {
      httpPost.addHeader(AUTHORIZATION, API_KEY + briteVerifyConfig.getBriteVerifyBoostKey());
    } else if (GENESIS.equals(site)) {
      httpPost.addHeader(AUTHORIZATION, API_KEY + briteVerifyConfig.getBriteVerifyGenesisKey());
    }
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    String result = FAILURE;
    try {
      httpPost.setEntity(new StringEntity("{ \"email\": \"" + email + "\"}"));
      final String apiResponse = makePostRequestApi(httpPost);
      if (StringUtils.isNotEmpty(apiResponse)) {
        final JsonObject jsonObject = new Gson().fromJson(apiResponse, JsonObject.class);
        final JsonObject emailResponse = jsonObject.getAsJsonObject(EMAIL);
        String status = emailResponse.get(STATUS).getAsString();
        final boolean disposable = emailResponse.get(DISPOSABLE).getAsBoolean();
        final boolean roleAddr = emailResponse.get(ROLE_ADDR).getAsBoolean();

        if (status.equals(UNKNOWN) && isWhitelistedDomain(email)) {
          status = VALID;
        }

        if ((VALID.equals(status) || ACCEPT_ALL.equals(status)) && !disposable && !roleAddr) {
          result = SUCCESS;
        } else {
          logger.error("Entered invalid email ID:  {}", email);
        }
      } else {
        logger.error("BriteVerify API call failure ");
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("BriteVerify httpPost set entity {}", e.getMessage(), e);
    }
    return result;
  }

  private boolean isWhitelistedDomain(final String emailId) {
    return Optional.of(emailId)
            .map(this::getDomainFromEmail)
            .map(domain -> domainWhitelistConfig.getWhitelistedDomains().contains(domain))
            .orElse(false);
  }

  private String getDomainFromEmail(final String email) {
    return email.contains("@") ? email.substring(email.indexOf('@') + 1) : StringUtils.EMPTY;
  }

  private String makePostRequestApi(final HttpPost httpPost) {
    String result = "";
    int statusCode;
    try (CloseableHttpClient httpClient = getHttpClient();
         CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
      statusCode = httpResponse.getStatusLine().getStatusCode();
      logger.debug("Post request : {},  Response code :{}", httpPost, statusCode);
      if (statusCode == SC_OK) {
        result = getResult(httpResponse);
      }
    } catch (ClientProtocolException e) {
      logger.error("ClientProtocolException in leadgen api call : {} ", e.getMessage(), e);
    } catch (IOException e) {
      logger.error("IOException : {} ", e.getMessage(), e);
    }
    return result;
  }
  
  private Map<String, String> makePostRequestApiResponseMap(final HttpPost httpPost) {
    final Map<String, String> result = new ConcurrentHashMap<>();
    try (CloseableHttpClient httpClient = getHttpClient();
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
      final int statusCode = httpResponse.getStatusLine().getStatusCode();
      logger.debug("Post request : {},  Response code :{}", httpPost, statusCode);
      result.put(API_RESPONSE_STATUS_CODE, String.valueOf(statusCode));
      result.put(API_RESPONSE_MESSAGE, getResult(httpResponse));
    } catch (ClientProtocolException e) {
      logger.error("ClientProtocolException in leadgen api call : {} ", e.getMessage(), e);
    } catch (IOException e) {
      logger.error("IOException : {} ", e.getMessage(), e);
    }
    return result;
  }

  protected String getResult(final CloseableHttpResponse httpResponse) throws IOException {
    return EntityUtils.toString(httpResponse.getEntity());
  }

  protected CloseableHttpClient getHttpClient() {
    return HttpClients.createDefault();
  }

  @SuppressWarnings("PMD.UnusedFormalParameter")
  private String checkWithMagentoEmailExist(final String email) {
    final HttpPost httpPost = new HttpPost(magentoAPIConfigService.getHost());
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    String result = StringUtils.EMPTY;
    try {
      httpPost.setHeader(AUTHORIZATION, BEARER + magentoAPIConfigService.getBearerToken());
      httpPost.setEntity(new StringEntity("{\"request\":{ \"email\": \"" + email + "\"}}"));
      final Map<String, String> responseMap = makePostRequestApiResponseMap(httpPost);
      final String apiResponseMessage = responseMap.getOrDefault(API_RESPONSE_MESSAGE, StringUtils.EMPTY);
      final String apiResponseStatusCode = responseMap.getOrDefault(API_RESPONSE_STATUS_CODE, "400");
      final int statusCode = Integer.parseInt(apiResponseStatusCode);
      if (statusCode == SC_OK) {
        final Gson gson = new Gson();
        final Map<String, Object> map = gson.fromJson(apiResponseMessage, Map.class);
        if (StringUtils.isNotEmpty(apiResponseMessage) && TRUE.equals(map.get(STATUS))) {
          result = SUCCESS;
        } else {
          logger.error("Magento API call failure so considering email does not already exist {}, response code: {}", email, apiResponseStatusCode);
        }
      } else {
        logger.error("Magento API call failure so considering email does not already exist {}, response code: {}", email, apiResponseStatusCode);
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("Magento httpPost set entity", e);
    }
    return result;
  }

  @SuppressWarnings("PMD.UseIndexOfChar")
  private String newMakeOmniSendApiCall(final String email,final String state, final String city, final String zipCode, final String address, final String customProperties) {

    final HttpPost httpPost = new HttpPost(omniSendConfig.getOmniSendEndpoint());
    httpPost.addHeader(X_API_KEY, omniSendConfig.getOmniSendKey());
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    final String zuluTime = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

    String omnisendPayload = String.format(OMNISEND_PAYLOAD_CITY, zuluTime, email, zuluTime, address, city, state, zipCode, customProperties);
    omnisendPayload = this.getFormattedJsonString(omnisendPayload);



    logger.debug("OmniSend Payload with city and state  is {}", omnisendPayload);
    String result = FAILURE;
    try {
      httpPost.setEntity(new StringEntity(omnisendPayload));
      final String apiResponse = makePostRequestApi(httpPost);
      if (StringUtils.isNotEmpty(apiResponse)) {
        logger.debug("Success Omnisend");
        result = SUCCESS;
      } else {
        logger.error("Omnisend API call failure ");
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("Omnisend httpPost set entity", e);
    }
    return result;
  }

  @SuppressWarnings("PMD.UseIndexOfChar")
  private String newMakeOmniSendApiCall(final String email, final String zipCode, final String address, final String customProperties) {

    final HttpPost httpPost = new HttpPost(omniSendConfig.getOmniSendEndpoint());
    httpPost.addHeader(X_API_KEY, omniSendConfig.getOmniSendKey());
    httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
    final String zuluTime = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();

    String omnisendPayload = String.format(OMNISEND_PAYLOAD, zuluTime, email, zuluTime, zipCode, address, customProperties);
    omnisendPayload = this.getFormattedJsonString(omnisendPayload);

    logger.debug("OmniSend Payload is {}", omnisendPayload);
    String result = FAILURE;
    try {
      httpPost.setEntity(new StringEntity(omnisendPayload));
      final String apiResponse = makePostRequestApi(httpPost);
      if (StringUtils.isNotEmpty(apiResponse)) {
        logger.error("Success Omnisend");
        result = SUCCESS;
      } else {
        logger.error("Omnisend API call failure ");
      }
    } catch (UnsupportedEncodingException e) {
      logger.error("Omnisend httpPost set entity", e);
    }
    return result;
  }

  /**
   * format the request payload
   * @param theString
   * @return
   */
  @SuppressWarnings("PMD.UseConcurrentHashMap")
  private String getFormattedJsonString(final String theString){
    final Type type = new TypeToken<Map<String, Object>>() {}.getType();
    final Map<String, Object> data = new Gson().fromJson(theString, type);

    for (final Iterator<Map.Entry<String, Object>> it = data.entrySet().iterator(); it.hasNext();) {
      final Map.Entry<String, Object> entry = it.next();
      if (entry.getValue() == null || entry.getValue().equals("null")) {
        it.remove();
      } else if (entry.getValue().getClass().equals(List.class)) {
        if (((List<?>) entry.getValue()).isEmpty()) {
          it.remove();
        }
      } else if (entry.getValue() instanceof Map){ //removes empty json objects {}
        final Map<?, ?> mObj = (Map<?, ?>)entry.getValue();
        if(mObj.isEmpty()) {
          it.remove();
        }
      }
    }

    return new GsonBuilder().setPrettyPrinting().create().toJson(data);
  }
}
