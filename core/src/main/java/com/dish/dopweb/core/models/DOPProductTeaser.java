package com.dish.dopweb.core.models;

import com.adobe.cq.commerce.core.components.models.productteaser.ProductTeaser;
import com.dish.dopweb.core.beans.PriceParts;

/** DOP Product Teaser is the sling model interface for the CIF Product Teaser component. */
public interface DOPProductTeaser {

  /** Price with integral and decimal parts. */
  PriceParts getPrice();

  /** Show button in product teaser. */
  boolean isShowButton();

  /** Get Product Manufacturer Name. */
  String getManufacturerName();

  /** Get Core CIF Product Teaser. */
  ProductTeaser getProductTeaser();

  /**
   * Returns url of swatch image of the product for display for this {@code ProductTeaser}
   *
   * @return url of the swatch image for the product or {@code null}
   */
  String getImage();
}
