package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Site Brandname CA Config is for chat authentication or any to decide the brandname.
 */
@Configuration(
        label = "Site Brand Name Configuration",
        description = "Configure the site brand name.")
public @interface SiteBrandNameCaConfig {

    /**
     * property for site brand name.
     *
     * @return brand name
     */
    @Property(label = "Site Brand Name")
    String siteBrandName();
}
