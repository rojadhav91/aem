package com.dish.dopweb.core.models;

import com.adobe.acs.commons.models.injectors.annotation.I18N;
import com.day.cq.i18n.I18n;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;

import java.util.HashMap;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextAttributeModel {
    @I18N
    private I18n i18n;

    @Getter
    @RequestAttribute
    private String fieldId;
    @Getter @RequestAttribute
    @I18N
    private String requiredMsg;
    @Getter @RequestAttribute
    private String labelTitle;
    @Getter @RequestAttribute
    private String placeholderMsg;
    @Getter @RequestAttribute
    private boolean requiredFlag;
    @Getter @RequestAttribute
    private String fieldName;

    public HashMap<String,String> attrMap;

    /**
     * Create attribute map and
     * @return attribute map
     */
    public HashMap<String, String> getAttrMap() {
        attrMap = new HashMap<>();
        attrMap.put("fieldId",fieldId);
        attrMap.put("requiredMsg", i18n.get(requiredMsg));
        attrMap.put("labelTitle",labelTitle);
        attrMap.put("placeholderMsg",i18n.get(placeholderMsg));
        attrMap.put("requiredFlag",Boolean.toString(requiredFlag));
        attrMap.put("fieldName",fieldName);
        return attrMap;
    }

}
