package com.dish.dopweb.core.configs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Navigation CA Config for XF Path and category name.
 */
@Configuration(
        label = "Launch Script Configuration",
        description = "Configure the url for adobe analytics")
public @interface LaunchScriptCaConfig {

    /**
     * property launch script .
     *
     * @return launchScript
     */
    @Property(label = "Launch Script")
    String launchScript();

    /**
     * Data Layer Web Page Prefix .
     *
     * @return page prefix
     */
    @Property(label = "Data Layer Web Page Prefix")
    String dataLayerPagePrefix();

}
