package com.dish.dopweb.core.configs;

/**
 * Configuration for Apigee Authorization Token Request
 */
public interface ApigeeAuthTokenRequestConfig {
    /**
     * Apigee Authorization Token Endpoint
     * @return endpoint
     */
    String getApigeeAuthTokenEndpoint();

    /**
     * Client id for Apigee
     * @return client_id
     */
    String getClientId();

    /**
     * Client secret for Apigee
     * @return client_secret
     */
    String getClientSecret();
}
