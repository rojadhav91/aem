package com.dish.dopweb.core.models;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;


/**
 * Container model to get padding style
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ButtonModelMargin {
    private static final String CMP_CONTAINER_MARGIN = "cmp-button__bm-";
    private String desktopButtonMarginMenuItems;
    private String tabletButtonMarginMenuItems;
    private String mobileButtonMarginMenuItems;

    @SlingObject
    private Resource currentResource;

    public String getDesktopButtonMarginMenuItems() {
        String desktopmargin = CMP_CONTAINER_MARGIN+"0";
        if(StringUtils.isNotEmpty(desktopButtonMarginMenuItems)){
            desktopmargin = CMP_CONTAINER_MARGIN+desktopButtonMarginMenuItems;
        }
        return desktopmargin;
    }

    public String getTabletButtonMarginMenuItems() {
        String tabletpmargin = CMP_CONTAINER_MARGIN+"0";
        if(StringUtils.isNotEmpty(tabletButtonMarginMenuItems)){
            tabletpmargin = CMP_CONTAINER_MARGIN+tabletButtonMarginMenuItems;
        }
        return tabletpmargin;
    }

    public String getMobileButtonMarginMenuItems() {
        String mobilemargin = CMP_CONTAINER_MARGIN+"0";
        if(StringUtils.isNotEmpty(mobileButtonMarginMenuItems)){
            mobilemargin = CMP_CONTAINER_MARGIN+mobileButtonMarginMenuItems;
        }
        return mobilemargin;
    }

    /**
     * postconstruct to get button bottom margin values
     */
    @PostConstruct
    public void init(){

        final ValueMap properties = currentResource.adaptTo(ValueMap.class);
        if(properties != null){
            desktopButtonMarginMenuItems = properties.get("marginButtondesktopvalues", String.class);
            tabletButtonMarginMenuItems = properties.get("marginButtontabletvalues", String.class);
            mobileButtonMarginMenuItems = properties.get("marginButtonmobilevalues", String.class);
        }
    }
}
