package com.dish.dopweb.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

/**
 * Custom Report Cell Value
 */
@Model(
        adaptables = {SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class CustomReportCellValue {
    private static final String IS_IMAGE = "is/image/";
    private static final String EMPTY_VALUE = " ";
    @ValueMapValue
    private String property1;

    @ValueMapValue
    private String property2;

    @RequestAttribute
    private Resource result;
    private Object value;

    /**
     * An empty constructor
     */
    @SuppressWarnings("PMD.UncommentedEmptyConstructor")
    public CustomReportCellValue(){
        
    }

    /**
     * Paramterized constructor
     * @param result - multiple properties
     * @param property1
     * @param property2
     */
    public CustomReportCellValue(final Resource result, final String property1, final String property2) {
        this.result = result;
        this.property1 = property1;
        this.property2 = property2;

        this.init();
    }


    /**
     * Form a single value using two different properties
     * @return a single value
     */
    public String getSingleValue() {
        String finalValue;
        if (isPropertyPresent(this.property1) && isPropertyPresent(this.property2)){
            finalValue = this.result.getValueMap().get(this.property1, String.class)
                    +IS_IMAGE
                    +this.result.getValueMap().get(this.property2, String.class);
        }else{
            finalValue = EMPTY_VALUE;
        }

        return finalValue;
    }

    private boolean isPropertyPresent(final String property){
        return this.result.getValueMap().containsKey(property);
    }
    /**
     * Cell value
     * @return cell value
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * Initialize the value from result's valuemap
     */
    @SuppressWarnings("PMD.ConfusingTernary")
    @PostConstruct
    private void init() {
        String finalValue;
        if (isPropertyPresent(this.property1) && isPropertyPresent(this.property2)){
            finalValue = this.result.getValueMap().get(this.property1, String.class)
                    +IS_IMAGE
                    +this.result.getValueMap().get(this.property2, String.class);
        }else{
            finalValue = EMPTY_VALUE;
        }
        this.value = finalValue;
    }

    /**
     * Check whether values contains multiple values
     * @return boolean
     */
    public boolean isArray() {
        return this.value != null && this.value.getClass().isArray();
    }

}
