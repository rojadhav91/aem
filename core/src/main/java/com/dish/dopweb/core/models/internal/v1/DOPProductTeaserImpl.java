package com.dish.dopweb.core.models.internal.v1;

import com.adobe.cq.commerce.core.components.models.productteaser.ProductTeaser;
import com.dish.dopweb.core.beans.PriceParts;
import com.dish.dopweb.core.models.DOPProductTeaser;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;

/** Sling model implementation for the CIF Product Teaser interface. */
@Model(
    adaptables = SlingHttpServletRequest.class,
    adapters = DOPProductTeaser.class,
    resourceType = DOPProductTeaserImpl.RESOURCE_TYPE, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Slf4j
public class DOPProductTeaserImpl implements DOPProductTeaser {

  public static final String RESOURCE_TYPE = "dopweb/components/commerce/productteaser";
  public static final String DYNAMIC_MEDIA_MODIFIERS = "?hei=264";

  @Getter @ValueMapValue private boolean showButton;

  @Getter
  @ValueMapValue
  @Default(values = StringUtils.EMPTY)
  private String manufacturerName;

  @Self
  @Getter
  @Via(type = ResourceSuperType.class)
  private ProductTeaser productTeaser;

  /** Price with integral and decimal parts. */
  @Override
  public PriceParts getPrice() {
    return new PriceParts(productTeaser.getFormattedPrice());
  }

  @Override
  public String getImage() {
    return productTeaser.getImage().concat(DYNAMIC_MEDIA_MODIFIERS);
  }
}
