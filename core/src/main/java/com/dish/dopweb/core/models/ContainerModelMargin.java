package com.dish.dopweb.core.models;

import com.dish.dopweb.core.utils.ContainerUtil;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import java.util.Iterator;
import java.util.List;

/**
 * Container model to get padding style
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContainerModelMargin {
    private static final String CMP_CONTAINER_MARGIN = "cmp-container__margin";
    private static final String DESKTOP_VALUES = "margindesktopvalues";
    private static final String TABLET_VALUES = "margintabletvalues";
    private static final String MOBILE_VALUES = "marginmobilevalues";
    private static final String DESKTOP_DIRECTION = "desktopdirectionMargin";
    private static final String TABLET_DIRECTION = "tabletdirectionMargin";
    private static final String MOBILE_DIRECTION = "mobiledirectionMargin";


    @ChildResource
    private Resource desktopMarginMenuItems;

    @ChildResource
    private Resource tabletMarginMenuItems;

    @ChildResource
    private Resource mobileMarginMenuItems;


    public List<String> getDesktopMarginMenuItems() {
        final Iterator<Resource> desktopIterator = ContainerUtil.getResourceIterator(desktopMarginMenuItems);
        return ContainerUtil.makeClassList(desktopIterator, DESKTOP_VALUES, DESKTOP_DIRECTION, CMP_CONTAINER_MARGIN);
    }


    public List<String> getTabletMarginMenuItems() {
        final Iterator<Resource> tabletIterator = ContainerUtil.getResourceIterator(tabletMarginMenuItems);
        return ContainerUtil.makeClassList(tabletIterator, TABLET_VALUES, TABLET_DIRECTION, CMP_CONTAINER_MARGIN);
    }

    public List<String> getMobileMarginMenuItems() {
        final Iterator<Resource> mobileIterator = ContainerUtil.getResourceIterator(mobileMarginMenuItems);
        return ContainerUtil.makeClassList(mobileIterator, MOBILE_VALUES, MOBILE_DIRECTION, CMP_CONTAINER_MARGIN);
    }
}