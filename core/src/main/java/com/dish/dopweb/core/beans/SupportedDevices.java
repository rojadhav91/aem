package com.dish.dopweb.core.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/** @author imran.padshah */
@SuppressWarnings("PMD")
public class SupportedDevices {
  @Getter @Setter private List<DeviceCompatibility> deviceCompatibility;
}
