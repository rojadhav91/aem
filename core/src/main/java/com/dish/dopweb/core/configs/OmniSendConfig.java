package com.dish.dopweb.core.configs;
/**
 * Briteverify config
 */
public interface OmniSendConfig {
    /**
     * key for OmniSend
     * @return key
     */
    String getOmniSendKey();

    /**
     * Omnisend endpoint
     * @return endoint
     */
    String getOmniSendEndpoint();
}
