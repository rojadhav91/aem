package com.dish.dopweb.core.configs.impl;


import com.dish.dopweb.core.configs.HCaptchaConfig;
import lombok.Getter;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration Service Implementation for HCaptcha
 */
@Component(service = HCaptchaConfig.class, immediate = true)
@Designate(ocd = HCaptchaConfigImpl.HCaptchaConfig.class, factory = false)
@ServiceDescription("A service used to configure HCaptcha")
@ServiceVendor("Dish DOP")
public class HCaptchaConfigImpl implements HCaptchaConfig {

    @SuppressWarnings("PMD.DefaultPackage")
    @Getter
    String hCaptchaSiteSecretKey;

    /**
     * Set configuration
     * @param config
     */
    @Activate
    public void activate(final HCaptchaConfigImpl.HCaptchaConfig config) {

        hCaptchaSiteSecretKey = config.HCaptchaSiteSecretKey();
    }

    @ObjectClassDefinition(
            name = "Dish DOP Set Secret Site Key Configuration",
            description = "The configuration for HCaptcha Secret Site Key")
    public @interface HCaptchaConfig {

        @AttributeDefinition(name = "Secret Site Key", description = "The configuration for HCaptcha Secret Site Key")
        String HCaptchaSiteSecretKey() default  "";
    }
}
