/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.faqpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class faqpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_faqpanelmodel = null;
Collection var_collectionvar5_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_faqpanelmodel = renderContext.call("use", com.techcombank.core.models.FAQPanelModel.class.getName(), obj());
out.write("\r\n    <div class=\"faq-panel\" data-showmore=\"5\">\r\n        <div class=\"faq-panel-container\">\r\n            <div class=\"list-answer-question\" data-id=\"view-all\">\r\n                ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_faqpanelmodel, "faqCFList");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object faqcf : var_collectionvar5_list_coerced$) {
                                {
                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                    if (var_traversal14) {
                                        out.write("\r\n                    <div class=\"answer-question\">\r\n                        <div class=\"question\" data-tracking-click-event=\"faq\"");
                                        {
                                            String var_attrcontent15 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                            out.write(" data-tracking-web-interaction-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                            out.write("\"");
                                        }
                                        {
                                            String var_attrcontent16 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(faqcf, "faqTitle"), "attribute"))) + "'}");
                                            out.write(" data-tracking-click-info-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                            out.write("\"");
                                        }
                                        out.write(">\r\n                            <p>");
                                        {
                                            Object var_17 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(faqcf, "faqTitle"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_17));
                                        }
                                        out.write("</p>\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                        </div>\r\n                        <div class=\"answer\">");
                                        {
                                            String var_18 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(faqcf, "faqDescription"), "html"))) + "\r\n                        ");
                                            out.write(renderContext.getObjectModel().toString(var_18));
                                        }
                                        out.write("</div>\r\n                    </div>\r\n                ");
                                    }
                                }
                                var_index12++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n            </div>\r\n            ");
{
    Object var_testvariable19 = renderContext.getObjectModel().resolveProperty(_global_faqpanelmodel, "viewMoreLabel");
    if (renderContext.getObjectModel().toBoolean(var_testvariable19)) {
        out.write("\r\n                <div class=\"read-more\">\r\n                    <span>");
        {
            Object var_20 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_faqpanelmodel, "viewMoreLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_20));
        }
        out.write("</span>\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                </div>\r\n            ");
    }
}
out.write("\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

