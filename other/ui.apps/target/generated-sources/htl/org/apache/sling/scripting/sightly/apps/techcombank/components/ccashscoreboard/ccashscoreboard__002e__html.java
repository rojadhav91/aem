/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.ccashscoreboard;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class ccashscoreboard__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _dynamic_title = bindings.get("title");
Object _dynamic_criteriatitle = bindings.get("criteriatitle");
Object _dynamic_averagescoretitle = bindings.get("averagescoretitle");
Object _dynamic_totalscoretitle = bindings.get("totalscoretitle");
Object _dynamic_caption = bindings.get("caption");
_global_model = renderContext.call("use", com.techcombank.core.models.CCashScoreBoardModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "title");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"c-cash-score-board\">\r\n            <div class=\"title\">");
            {
                Object var_1 = renderContext.call("xss", _dynamic_title, "text");
                out.write(renderContext.getObjectModel().toString(var_1));
            }
            out.write("</div>\r\n            <div class=\"criteria-title\">");
            {
                Object var_2 = renderContext.call("xss", _dynamic_criteriatitle, "text");
                out.write(renderContext.getObjectModel().toString(var_2));
            }
            out.write("</div>\r\n            <div class=\"average-score-title\">");
            {
                Object var_3 = renderContext.call("xss", _dynamic_averagescoretitle, "text");
                out.write(renderContext.getObjectModel().toString(var_3));
            }
            out.write("</div>\r\n            <div class=\"total-score-title\">");
            {
                Object var_4 = renderContext.call("xss", _dynamic_totalscoretitle, "text");
                out.write(renderContext.getObjectModel().toString(var_4));
            }
            out.write("</div>\r\n            <div class=\"caption\">");
            {
                Object var_5 = renderContext.call("xss", _dynamic_caption, "text");
                out.write(renderContext.getObjectModel().toString(var_5));
            }
            out.write("</div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar6 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions7_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions7 = obj().with("isEmpty", var_templateoptions7_field$_isempty);
            callUnit(out, renderContext, var_templatevar6, var_templateoptions7);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

