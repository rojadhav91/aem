/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.offerlisting;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class offerlisting__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_offerlisting = null;
Object _dynamic_resource = bindings.get("resource");
Collection var_collectionvar61_list_coerced$ = null;
Collection var_collectionvar81_list_coerced$ = null;
Collection var_collectionvar111_list_coerced$ = null;
Collection var_collectionvar148_list_coerced$ = null;
Collection var_collectionvar168_list_coerced$ = null;
Collection var_collectionvar198_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_offerlisting = renderContext.call("use", com.techcombank.core.models.OfferListingModel.class.getName(), obj());
out.write("\r\n");
{
    boolean var_testvariable5 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable5) {
        out.write("\r\n<section class=\"container section__margin-medium\">\r\n    <div class=\"content-wrapper offer-listing-filter__body\">\r\n        <div");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" data-url");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                {
                    boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                    if (var_shoulddisplayattr13) {
                        out.write(" data-component-name");
                        {
                            boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                            if (!var_istrueattr12) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "localeLanguage");
            {
                Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                {
                    boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                    if (var_shoulddisplayattr17) {
                        out.write(" data-language-label");
                        {
                            boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                            if (!var_istrueattr16) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "expiryDayLabel");
            {
                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                {
                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                    if (var_shoulddisplayattr21) {
                        out.write(" data-day-label");
                        {
                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                            if (!var_istrueattr20) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "expiryLabel");
            {
                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                {
                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                    if (var_shoulddisplayattr25) {
                        out.write(" data-expiry-label");
                        {
                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                            if (!var_istrueattr24) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "emptyPromotionLabel");
            {
                Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                {
                    boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                    if (var_shoulddisplayattr29) {
                        out.write(" data-empty-promotion-label");
                        {
                            boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                            if (!var_istrueattr28) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "resultCountText");
            {
                Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                {
                    boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                    if (var_shoulddisplayattr33) {
                        out.write(" data-search-count");
                        {
                            boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                            if (!var_istrueattr32) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue34 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "viewMore");
            {
                Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
                {
                    boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                    if (var_shoulddisplayattr37) {
                        out.write(" data-view-more");
                        {
                            boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                            if (!var_istrueattr36) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"offer-listing-promotions\">\r\n            <div class=\"offer-listing-promotions__wrapper\">\r\n                <div class=\"offer-filter__container enhanced-offer-filter__container\" data-tracking-click-event=\"articleFilter\" data-tracking-click-info-value=\"{'articleFilter':''}\"");
        {
            String var_attrcontent38 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent38));
            out.write("\"");
        }
        out.write(">\r\n                    <div class=\"offer-filter__title filter--border-bottom\">\r\n                        <h6>");
        {
            Object var_39 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "sortFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_39));
        }
        out.write("</h6>\r\n                    </div>\r\n                    <div class=\"offer-filter__sort-by-wrap offer-filter__checkbox analytics-ol-sort\">\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <div class=\"checkbox-item__wrapper\">\r\n                                <input class=\"input__radio\" name=\"sort\" type=\"radio\"");
        {
            Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "mostPopular");
            {
                Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                {
                    boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                    if (var_shoulddisplayattr43) {
                        out.write(" data-sort");
                        {
                            boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                            if (!var_istrueattr42) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-value=\"most-popular\" checked/>\r\n                                <div>");
        {
            Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "mostPopular"), "text");
            out.write(renderContext.getObjectModel().toString(var_44));
        }
        out.write("</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <div class=\"checkbox-item__wrapper\">\r\n                                <input class=\"input__radio\" name=\"sort\" type=\"radio\"");
        {
            Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "latest");
            {
                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                {
                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                    if (var_shoulddisplayattr48) {
                        out.write(" data-sort");
                        {
                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                            if (!var_istrueattr47) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-value=\"latest\"/>\r\n                                <div>");
        {
            Object var_49 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "latest"), "text");
            out.write(renderContext.getObjectModel().toString(var_49));
        }
        out.write("</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <div class=\"checkbox-item__wrapper\">\r\n                                <input class=\"input__radio\" name=\"sort\" type=\"radio\"");
        {
            Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "moreTime");
            {
                Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
                {
                    boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                    if (var_shoulddisplayattr53) {
                        out.write(" data-sort");
                        {
                            boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                            if (!var_istrueattr52) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-value=\"more-time-to-consider\"/>\r\n                                <div>");
        {
            Object var_54 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "moreTime"), "text");
            out.write(renderContext.getObjectModel().toString(var_54));
        }
        out.write("</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <div class=\"checkbox-item__wrapper\">\r\n                                <input class=\"input__radio\" name=\"sort\" type=\"radio\"");
        {
            Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "expiringSoon");
            {
                Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                {
                    boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                    if (var_shoulddisplayattr58) {
                        out.write(" data-sort");
                        {
                            boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                            if (!var_istrueattr57) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-value=\"expiring-soon\"/>\r\n                                <div>");
        {
            Object var_59 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "expiringSoon"), "text");
            out.write(renderContext.getObjectModel().toString(var_59));
        }
        out.write("</div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"offer-filter__title filter--border-bottom\">\r\n                        <h6>");
        {
            Object var_60 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_60));
        }
        out.write("</h6>\r\n                    </div>\r\n                    <div class=\"offer-filter__checkbox product-checkbox analytics-product-type\">\r\n                        ");
        {
            Object var_collectionvar61 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags");
            {
                long var_size62 = ((var_collectionvar61_list_coerced$ == null ? (var_collectionvar61_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar61)) : var_collectionvar61_list_coerced$).size());
                {
                    boolean var_notempty63 = (var_size62 > 0);
                    if (var_notempty63) {
                        {
                            long var_end66 = var_size62;
                            {
                                boolean var_validstartstepend67 = (((0 < var_size62) && true) && (var_end66 > 0));
                                if (var_validstartstepend67) {
                                    if (var_collectionvar61_list_coerced$ == null) {
                                        var_collectionvar61_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar61);
                                    }
                                    long var_index68 = 0;
                                    for (Object filter : var_collectionvar61_list_coerced$) {
                                        {
                                            boolean var_traversal70 = (((var_index68 >= 0) && (var_index68 <= var_end66)) && true);
                                            if (var_traversal70) {
                                                out.write("\r\n                            <div class=\"offer-filter__checkbox-item\">\r\n                                <label class=\"checkbox-item__wrapper\">\r\n                                    <input class=\"input__checkbox\" type=\"checkbox\"");
                                                {
                                                    Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags"), filter);
                                                    {
                                                        Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                                                            if (var_shoulddisplayattr74) {
                                                                out.write(" data-product");
                                                                {
                                                                    boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                                                    if (!var_istrueattr73) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue75 = filter;
                                                    {
                                                        Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                                                            if (var_shoulddisplayattr78) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                                                                    if (!var_istrueattr77) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                    <span>");
                                                {
                                                    Object var_79 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags"), filter), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_79));
                                                }
                                                out.write("</span>\r\n                                </label>\r\n                            </div>\r\n                        ");
                                            }
                                        }
                                        var_index68++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar61_list_coerced$ = null;
        }
        out.write("\r\n                    </div>\r\n\r\n                    <div class=\"offer-filter__title filter--border-bottom\">\r\n                        <h6>");
        {
            Object var_80 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "categoryFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_80));
        }
        out.write("</h6>\r\n                    </div>\r\n\r\n                    <div class=\"offer-filter__checkbox category-checkbox analytics-ol-categories\">\r\n                        ");
        {
            Object var_collectionvar81 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags");
            {
                long var_size82 = ((var_collectionvar81_list_coerced$ == null ? (var_collectionvar81_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar81)) : var_collectionvar81_list_coerced$).size());
                {
                    boolean var_notempty83 = (var_size82 > 0);
                    if (var_notempty83) {
                        {
                            long var_end86 = var_size82;
                            {
                                boolean var_validstartstepend87 = (((0 < var_size82) && true) && (var_end86 > 0));
                                if (var_validstartstepend87) {
                                    if (var_collectionvar81_list_coerced$ == null) {
                                        var_collectionvar81_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar81);
                                    }
                                    long var_index88 = 0;
                                    for (Object filter : var_collectionvar81_list_coerced$) {
                                        {
                                            boolean var_traversal90 = (((var_index88 >= 0) && (var_index88 <= var_end86)) && true);
                                            if (var_traversal90) {
                                                out.write("\r\n                            <div class=\"offer-filter__checkbox-item\">\r\n                                <label class=\"checkbox-item__wrapper\">\r\n                                    <input class=\"input__checkbox\"");
                                                {
                                                    Object var_attrvalue91 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter);
                                                    {
                                                        Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                                                            if (var_shoulddisplayattr94) {
                                                                out.write(" data-category");
                                                                {
                                                                    boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                                                                    if (!var_istrueattr93) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue95 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter);
                                                    {
                                                        Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
                                                            if (var_shoulddisplayattr98) {
                                                                out.write(" name");
                                                                {
                                                                    boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                                                                    if (!var_istrueattr97) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" type=\"checkbox\"");
                                                {
                                                    Object var_attrvalue99 = filter;
                                                    {
                                                        Object var_attrcontent100 = renderContext.call("xss", var_attrvalue99, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr102 = (((null != var_attrcontent100) && (!"".equals(var_attrcontent100))) && ((!"".equals(var_attrvalue99)) && (!((Object)false).equals(var_attrvalue99))));
                                                            if (var_shoulddisplayattr102) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr101 = (var_attrvalue99.equals(true));
                                                                    if (!var_istrueattr101) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent100));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                    <span>");
                                                {
                                                    Object var_103 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_103));
                                                }
                                                out.write("</span>\r\n                                </label>\r\n                            </div>\r\n                        ");
                                            }
                                        }
                                        var_index88++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar81_list_coerced$ = null;
        }
        out.write("\r\n                    </div>\r\n\r\n                    <div class=\"offer-filter__title filter--border-bottom\">\r\n                        <h6>");
        {
            Object var_104 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_104));
        }
        out.write("</h6>\r\n                    </div>\r\n\r\n                    <div class=\"offer-filter__dropdown analytics-ol-merchant merchant-desktop-element\">\r\n                        <div class=\"dropdown__wrapper\">\r\n                            <div class=\"dropdown__display\">\r\n                                <span class=\"display__text text--ellipsis\"");
        {
            Object var_attrvalue105 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantPlaceholder");
            {
                Object var_attrcontent106 = renderContext.call("xss", var_attrvalue105, "attribute");
                {
                    boolean var_shoulddisplayattr108 = (((null != var_attrcontent106) && (!"".equals(var_attrcontent106))) && ((!"".equals(var_attrvalue105)) && (!((Object)false).equals(var_attrvalue105))));
                    if (var_shoulddisplayattr108) {
                        out.write(" data-placeholder");
                        {
                            boolean var_istrueattr107 = (var_attrvalue105.equals(true));
                            if (!var_istrueattr107) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent106));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">");
        {
            Object var_109 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantPlaceholder"), "text");
            out.write(renderContext.getObjectModel().toString(var_109));
        }
        out.write("</span>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                            </div>\r\n                            <div class=\"dropdown__list dropdown--ease-transition --bordered --shadow\">\r\n                                <ul>\r\n                                    <li class=\"dropdown__item\" value=\"all\">");
        {
            Object var_110 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "selectAllMerchant"), "text");
            out.write(renderContext.getObjectModel().toString(var_110));
        }
        out.write("</li>\r\n                                    ");
        {
            Object var_collectionvar111 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags");
            {
                long var_size112 = ((var_collectionvar111_list_coerced$ == null ? (var_collectionvar111_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar111)) : var_collectionvar111_list_coerced$).size());
                {
                    boolean var_notempty113 = (var_size112 > 0);
                    if (var_notempty113) {
                        {
                            long var_end116 = var_size112;
                            {
                                boolean var_validstartstepend117 = (((0 < var_size112) && true) && (var_end116 > 0));
                                if (var_validstartstepend117) {
                                    if (var_collectionvar111_list_coerced$ == null) {
                                        var_collectionvar111_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar111);
                                    }
                                    long var_index118 = 0;
                                    for (Object merchant : var_collectionvar111_list_coerced$) {
                                        {
                                            boolean var_traversal120 = (((var_index118 >= 0) && (var_index118 <= var_end116)) && true);
                                            if (var_traversal120) {
                                                out.write("<li class=\"dropdown__item\"");
                                                {
                                                    Object var_attrvalue121 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags"), merchant);
                                                    {
                                                        Object var_attrcontent122 = renderContext.call("xss", var_attrvalue121, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr124 = (((null != var_attrcontent122) && (!"".equals(var_attrcontent122))) && ((!"".equals(var_attrvalue121)) && (!((Object)false).equals(var_attrvalue121))));
                                                            if (var_shoulddisplayattr124) {
                                                                out.write(" data-merchant");
                                                                {
                                                                    boolean var_istrueattr123 = (var_attrvalue121.equals(true));
                                                                    if (!var_istrueattr123) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent122));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue125 = merchant;
                                                    {
                                                        Object var_attrcontent126 = renderContext.call("xss", var_attrvalue125, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr128 = (((null != var_attrcontent126) && (!"".equals(var_attrcontent126))) && ((!"".equals(var_attrvalue125)) && (!((Object)false).equals(var_attrvalue125))));
                                                            if (var_shoulddisplayattr128) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr127 = (var_attrvalue125.equals(true));
                                                                    if (!var_istrueattr127) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent126));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">");
                                                {
                                                    String var_129 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags"), merchant), "text"))) + "\r\n                                    ");
                                                    out.write(renderContext.getObjectModel().toString(var_129));
                                                }
                                                out.write("</li>\n");
                                            }
                                        }
                                        var_index118++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar111_list_coerced$ = null;
        }
        out.write("\r\n                                </ul>\r\n                                <input class=\"dropdown-selected-value merchant-desktop-element\" hidden id=\"merchant\" value=\"-1\"/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!--RESPONSIVE VIEW-->\r\n                <div class=\"offer-listing-filter__button\">\r\n                    <div class=\"search-primary-container\">\r\n                        <div class=\"search-box\">\r\n                            <div class=\"input-container\">\r\n                                <div class=\"search-box_icon\">\r\n                                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/search-primary-icon.svg\"/>\r\n                                </div>\r\n                                <input aria-invalid=\"false\" id=\"searchAreaMobile\" autocomplete=\"off\" placeholder=\"Search for promotion\" type=\"text\" aria-autocomplete=\"list\" autocapitalize=\"none\" spellcheck=\"false\" class=\"ui-autocomplete-input search-input\"/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"offer-listing-filter__button\">\r\n                    <button class=\"btn-open-filter\">\r\n                        <span>");
        {
            Object var_130 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filter"), "text");
            out.write(renderContext.getObjectModel().toString(var_130));
        }
        out.write("</span>\r\n                        <span class=\"tcb-filter-button-icon\">\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/filter.svg\"/>\r\n                        </span>\r\n                    </button>\r\n                </div>\r\n\r\n                <div class=\"offer-listing-filter__button\">\r\n                    <div class=\"offer-filter__dropdown analytics-ol-sort-mobile sort-mobile-element\">\r\n                        <div class=\"dropdown__wrapper\">\r\n                            <div class=\"dropdown__display\">\r\n                                <div class=\"sort-title\">\r\n                                    <small class=\"display__title\">");
        {
            Object var_131 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "sortFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_131));
        }
        out.write("</small>\r\n                                    <span class=\"display__text text--ellipsis\"");
        {
            Object var_attrvalue132 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "mostPopular");
            {
                Object var_attrcontent133 = renderContext.call("xss", var_attrvalue132, "attribute");
                {
                    boolean var_shoulddisplayattr135 = (((null != var_attrcontent133) && (!"".equals(var_attrcontent133))) && ((!"".equals(var_attrvalue132)) && (!((Object)false).equals(var_attrvalue132))));
                    if (var_shoulddisplayattr135) {
                        out.write(" data-placeholder");
                        {
                            boolean var_istrueattr134 = (var_attrvalue132.equals(true));
                            if (!var_istrueattr134) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent133));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">");
        {
            Object var_136 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "mostPopular"), "text");
            out.write(renderContext.getObjectModel().toString(var_136));
        }
        out.write("</span>\r\n                                </div>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                            </div>\r\n                            <div class=\"dropdown__list dropdown--ease-transition --bordered --shadow\">\r\n                                <ul>\r\n                                    <li class=\"dropdown__item\" value=\"most-popular\">");
        {
            Object var_137 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "mostPopular"), "text");
            out.write(renderContext.getObjectModel().toString(var_137));
        }
        out.write("</li>\r\n                                    <li class=\"dropdown__item\" value=\"latest\">");
        {
            Object var_138 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "latest"), "text");
            out.write(renderContext.getObjectModel().toString(var_138));
        }
        out.write("</li>\r\n                                    <li class=\"dropdown__item\" value=\"more-time-to-consider\">");
        {
            Object var_139 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "moreTime"), "text");
            out.write(renderContext.getObjectModel().toString(var_139));
        }
        out.write("</li>\r\n                                    <li class=\"dropdown__item\" value=\"expiring-soon\">");
        {
            Object var_140 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "expiringSoon"), "text");
            out.write(renderContext.getObjectModel().toString(var_140));
        }
        out.write("</li>\r\n\r\n                                </ul>\r\n                                <input class=\"dropdown-selected-value sort-mobile-element\" hidden id=\"merchantTypeMobile\" value=\"-1\"/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"offer-cards__container\">\r\n                    <div class=\"offer-cards__wrapper\">\r\n                        <div class=\"search-area\">\r\n                            <div class=\"search-primary-container\">\r\n                                <div class=\"search-box\">\r\n                                    <div class=\"input-container\">\r\n                                        <div class=\"search-box_icon\">\r\n                                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/search-primary-icon.svg\"/>\r\n                                        </div>\r\n                                        <input aria-invalid=\"false\" id=\"searchAreaDesktop\" autocomplete=\"off\"");
        {
            Object var_attrvalue141 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "searchPlaceHolderText");
            {
                Object var_attrcontent142 = renderContext.call("xss", var_attrvalue141, "attribute");
                {
                    boolean var_shoulddisplayattr144 = (((null != var_attrcontent142) && (!"".equals(var_attrcontent142))) && ((!"".equals(var_attrvalue141)) && (!((Object)false).equals(var_attrvalue141))));
                    if (var_shoulddisplayattr144) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr143 = (var_attrvalue141.equals(true));
                            if (!var_istrueattr143) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent142));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"text\" aria-autocomplete=\"list\" autocapitalize=\"none\" spellcheck=\"false\" class=\"ui-autocomplete-input search-input\"/>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"news-filter\">\r\n                                <div class=\"information-filter__load-more\">\r\n                                    <button type=\"button\" class=\"load-more__button\">\r\n                                        <span>");
        {
            Object var_145 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "viewResult"), "text");
            out.write(renderContext.getObjectModel().toString(var_145));
        }
        out.write("</span>\r\n                                        <span class=\"load-more__button-icon\">\r\n                                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg\" alt=\"red-arrow\"/>\r\n                                            </span>\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <!--TOTAL COUNT-->\r\n                        <div class=\"promotion-total-count\"></div>\r\n                        <div class=\"card-promotion__list\"></div>\r\n                        <div class=\"pagination\">\r\n                            <div class=\"pag-container\"></div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"tcb-modal offer-listing-enhance-mobile\" hidden=\"\">\r\n                    <div class=\"offer-filter__header\">\r\n                        <span class=\"title\">");
        {
            Object var_146 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filter"), "text");
            out.write(renderContext.getObjectModel().toString(var_146));
        }
        out.write("</span>\r\n                    </div>\r\n                    <div class=\"news_filter-group\">\r\n\r\n                        <div class=\"offer-filter__title filter--border-bottom\">\r\n                            <h6>");
        {
            Object var_147 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_147));
        }
        out.write("</h6>\r\n                        </div>\r\n\r\n                        <div class=\"offer-filter__checkbox product_filter product-checkbox-mobile\">\r\n                            ");
        {
            Object var_collectionvar148 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags");
            {
                long var_size149 = ((var_collectionvar148_list_coerced$ == null ? (var_collectionvar148_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar148)) : var_collectionvar148_list_coerced$).size());
                {
                    boolean var_notempty150 = (var_size149 > 0);
                    if (var_notempty150) {
                        {
                            long var_end153 = var_size149;
                            {
                                boolean var_validstartstepend154 = (((0 < var_size149) && true) && (var_end153 > 0));
                                if (var_validstartstepend154) {
                                    if (var_collectionvar148_list_coerced$ == null) {
                                        var_collectionvar148_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar148);
                                    }
                                    long var_index155 = 0;
                                    for (Object filter : var_collectionvar148_list_coerced$) {
                                        {
                                            boolean var_traversal157 = (((var_index155 >= 0) && (var_index155 <= var_end153)) && true);
                                            if (var_traversal157) {
                                                out.write("\r\n                                <div class=\"offer-filter__checkbox-item-mobile\">\r\n                                    <label class=\"checkbox-item__wrapper\">\r\n                                        <input class=\"input__checkbox\" type=\"checkbox\"");
                                                {
                                                    Object var_attrvalue158 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags"), filter);
                                                    {
                                                        Object var_attrcontent159 = renderContext.call("xss", var_attrvalue158, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr161 = (((null != var_attrcontent159) && (!"".equals(var_attrcontent159))) && ((!"".equals(var_attrvalue158)) && (!((Object)false).equals(var_attrvalue158))));
                                                            if (var_shoulddisplayattr161) {
                                                                out.write(" data-product");
                                                                {
                                                                    boolean var_istrueattr160 = (var_attrvalue158.equals(true));
                                                                    if (!var_istrueattr160) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent159));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue162 = filter;
                                                    {
                                                        Object var_attrcontent163 = renderContext.call("xss", var_attrvalue162, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr165 = (((null != var_attrcontent163) && (!"".equals(var_attrcontent163))) && ((!"".equals(var_attrvalue162)) && (!((Object)false).equals(var_attrvalue162))));
                                                            if (var_shoulddisplayattr165) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr164 = (var_attrvalue162.equals(true));
                                                                    if (!var_istrueattr164) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent163));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <span>");
                                                {
                                                    Object var_166 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "cardTypeTags"), filter), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_166));
                                                }
                                                out.write("</span>\r\n                                    </label>\r\n                                </div>\r\n                            ");
                                            }
                                        }
                                        var_index155++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar148_list_coerced$ = null;
        }
        out.write("\r\n                        </div>\r\n                        <!-- Filter title -->\r\n                        <div class=\"offer-filter__title filter--border-bottom\">\r\n                            <h6>");
        {
            Object var_167 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "categoryFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_167));
        }
        out.write("</h6>\r\n                            <span class=\"material-symbols-outlined btn-close hidden\">close</span>\r\n                        </div>\r\n\r\n                        <div class=\"offer-filter__checkbox category_filter category-checkbox-mobile\">\r\n                            ");
        {
            Object var_collectionvar168 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags");
            {
                long var_size169 = ((var_collectionvar168_list_coerced$ == null ? (var_collectionvar168_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar168)) : var_collectionvar168_list_coerced$).size());
                {
                    boolean var_notempty170 = (var_size169 > 0);
                    if (var_notempty170) {
                        {
                            long var_end173 = var_size169;
                            {
                                boolean var_validstartstepend174 = (((0 < var_size169) && true) && (var_end173 > 0));
                                if (var_validstartstepend174) {
                                    if (var_collectionvar168_list_coerced$ == null) {
                                        var_collectionvar168_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar168);
                                    }
                                    long var_index175 = 0;
                                    for (Object filter : var_collectionvar168_list_coerced$) {
                                        {
                                            boolean var_traversal177 = (((var_index175 >= 0) && (var_index175 <= var_end173)) && true);
                                            if (var_traversal177) {
                                                out.write("\r\n                                <div class=\"offer-filter__checkbox-item-mobile\">\r\n                                    <label class=\"checkbox-item__wrapper\">\r\n                                        <input class=\"input__checkbox\"");
                                                {
                                                    Object var_attrvalue178 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter);
                                                    {
                                                        Object var_attrcontent179 = renderContext.call("xss", var_attrvalue178, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr181 = (((null != var_attrcontent179) && (!"".equals(var_attrcontent179))) && ((!"".equals(var_attrvalue178)) && (!((Object)false).equals(var_attrvalue178))));
                                                            if (var_shoulddisplayattr181) {
                                                                out.write(" data-category");
                                                                {
                                                                    boolean var_istrueattr180 = (var_attrvalue178.equals(true));
                                                                    if (!var_istrueattr180) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent179));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue182 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter);
                                                    {
                                                        Object var_attrcontent183 = renderContext.call("xss", var_attrvalue182, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr185 = (((null != var_attrcontent183) && (!"".equals(var_attrcontent183))) && ((!"".equals(var_attrvalue182)) && (!((Object)false).equals(var_attrvalue182))));
                                                            if (var_shoulddisplayattr185) {
                                                                out.write(" name");
                                                                {
                                                                    boolean var_istrueattr184 = (var_attrvalue182.equals(true));
                                                                    if (!var_istrueattr184) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent183));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" type=\"checkbox\"");
                                                {
                                                    Object var_attrvalue186 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter);
                                                    {
                                                        Object var_attrcontent187 = renderContext.call("xss", var_attrvalue186, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr189 = (((null != var_attrcontent187) && (!"".equals(var_attrcontent187))) && ((!"".equals(var_attrvalue186)) && (!((Object)false).equals(var_attrvalue186))));
                                                            if (var_shoulddisplayattr189) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr188 = (var_attrvalue186.equals(true));
                                                                    if (!var_istrueattr188) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent187));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <span>");
                                                {
                                                    Object var_190 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "filterTags"), filter), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_190));
                                                }
                                                out.write("</span>\r\n                                    </label>\r\n                                </div>\r\n                            ");
                                            }
                                        }
                                        var_index175++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar168_list_coerced$ = null;
        }
        out.write("\r\n                        </div>\r\n\r\n                        <div class=\"offer-filter__title filter--border-bottom\">\r\n                            <h6>");
        {
            Object var_191 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantFilter"), "text");
            out.write(renderContext.getObjectModel().toString(var_191));
        }
        out.write("</h6>\r\n                        </div>\r\n\r\n                        <div class=\"offer-filter__dropdown analytics-ol-merchant-mobile merchant-mobile-element\">\r\n                            <div class=\"dropdown__wrapper\">\r\n                                <div class=\"dropdown__display merchant-dropdown-display\">\r\n                                    <span class=\"display__text text--ellipsis\"");
        {
            Object var_attrvalue192 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantPlaceholder");
            {
                Object var_attrcontent193 = renderContext.call("xss", var_attrvalue192, "attribute");
                {
                    boolean var_shoulddisplayattr195 = (((null != var_attrcontent193) && (!"".equals(var_attrcontent193))) && ((!"".equals(var_attrvalue192)) && (!((Object)false).equals(var_attrvalue192))));
                    if (var_shoulddisplayattr195) {
                        out.write(" data-placeholder");
                        {
                            boolean var_istrueattr194 = (var_attrvalue192.equals(true));
                            if (!var_istrueattr194) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent193));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">");
        {
            Object var_196 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantPlaceholder"), "text");
            out.write(renderContext.getObjectModel().toString(var_196));
        }
        out.write("</span>\r\n                                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                                </div>\r\n                                <div class=\"dropdown__list merchant-dropdown-list dropdown--ease-transition --bordered --shadow\">\r\n                                    <ul>\r\n                                        <li class=\"dropdown__item\" value=\"all\">");
        {
            Object var_197 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "selectAllMerchant"), "text");
            out.write(renderContext.getObjectModel().toString(var_197));
        }
        out.write("</li>\r\n                                        ");
        {
            Object var_collectionvar198 = renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags");
            {
                long var_size199 = ((var_collectionvar198_list_coerced$ == null ? (var_collectionvar198_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar198)) : var_collectionvar198_list_coerced$).size());
                {
                    boolean var_notempty200 = (var_size199 > 0);
                    if (var_notempty200) {
                        {
                            long var_end203 = var_size199;
                            {
                                boolean var_validstartstepend204 = (((0 < var_size199) && true) && (var_end203 > 0));
                                if (var_validstartstepend204) {
                                    if (var_collectionvar198_list_coerced$ == null) {
                                        var_collectionvar198_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar198);
                                    }
                                    long var_index205 = 0;
                                    for (Object merchant : var_collectionvar198_list_coerced$) {
                                        {
                                            boolean var_traversal207 = (((var_index205 >= 0) && (var_index205 <= var_end203)) && true);
                                            if (var_traversal207) {
                                                out.write("<li class=\"dropdown__item\"");
                                                {
                                                    Object var_attrvalue208 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags"), merchant);
                                                    {
                                                        Object var_attrcontent209 = renderContext.call("xss", var_attrvalue208, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr211 = (((null != var_attrcontent209) && (!"".equals(var_attrcontent209))) && ((!"".equals(var_attrvalue208)) && (!((Object)false).equals(var_attrvalue208))));
                                                            if (var_shoulddisplayattr211) {
                                                                out.write(" data-merchant");
                                                                {
                                                                    boolean var_istrueattr210 = (var_attrvalue208.equals(true));
                                                                    if (!var_istrueattr210) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent209));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue212 = merchant;
                                                    {
                                                        Object var_attrcontent213 = renderContext.call("xss", var_attrvalue212, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr215 = (((null != var_attrcontent213) && (!"".equals(var_attrcontent213))) && ((!"".equals(var_attrvalue212)) && (!((Object)false).equals(var_attrvalue212))));
                                                            if (var_shoulddisplayattr215) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr214 = (var_attrvalue212.equals(true));
                                                                    if (!var_istrueattr214) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent213));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">");
                                                {
                                                    String var_216 = ("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_offerlisting, "merchantTypeTags"), merchant), "text")));
                                                    out.write(renderContext.getObjectModel().toString(var_216));
                                                }
                                                out.write("</li>\n");
                                            }
                                        }
                                        var_index205++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar198_list_coerced$ = null;
        }
        out.write("\r\n                                    </ul>\r\n                                    <input class=\"dropdown-selected-value merchant-mobile-element\" hidden id=\"merchant\" value=\"-1\"/>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                    <div class=\"tcb-modal_action-bar\">\r\n                        <div class=\"tcb-apply-filter-button tcb-button--light-black\">");
        {
            String var_217 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_offerlisting, "apply"), "text"))) + "\r\n                            ");
            out.write(renderContext.getObjectModel().toString(var_217));
        }
        out.write("<img alt=\"Red arrow\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg\"/>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
    }
}
out.write("\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

