/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.enhancedfilterpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class enhancedfilterpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_filterpanel = null;
Collection var_collectionvar8_list_coerced$ = null;
Collection var_collectionvar23_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_filterpanel = renderContext.call("use", com.techcombank.core.models.EnhancedFilterPanelModel.class.getName(), obj());
out.write("\r\n       <div class=\"filter-panel\" itemscope itemtype=\"https://schema.org/FAQPage\">\r\n       \t\t");
{
    Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_global_filterpanel, "viewAll");
    if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
        out.write("<div class=\"filter-panel__content\">\r\n            \t<div class=\"content-wrapper filter-panel__container\">\r\n            \t\t<h6 class=\"filter-panel__title\">");
        {
            Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_filterpanel, "categoryLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_6));
        }
        out.write("</h6>\r\n                \t<div class=\"filter-panel__items\">\r\n                \t\t<div class=\"filter__item\">\r\n                        \t<button type=\"button\" class=\"filter-panel__button filter-selected\" data-id=\"all\">");
        {
            String var_7 = (("\r\n                            \t" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_filterpanel, "viewAll"), "text"))) + "\r\n                        \t");
            out.write(renderContext.getObjectModel().toString(var_7));
        }
        out.write("</button>\r\n                    \t</div>\r\n                    \t");
        {
            Object var_collectionvar8 = renderContext.getObjectModel().resolveProperty(_global_filterpanel, "tags");
            {
                long var_size9 = ((var_collectionvar8_list_coerced$ == null ? (var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8)) : var_collectionvar8_list_coerced$).size());
                {
                    boolean var_notempty10 = (var_size9 > 0);
                    if (var_notempty10) {
                        {
                            long var_end13 = var_size9;
                            {
                                boolean var_validstartstepend14 = (((0 < var_size9) && true) && (var_end13 > 0));
                                if (var_validstartstepend14) {
                                    if (var_collectionvar8_list_coerced$ == null) {
                                        var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8);
                                    }
                                    long var_index15 = 0;
                                    for (Object item : var_collectionvar8_list_coerced$) {
                                        {
                                            boolean var_traversal17 = (((var_index15 >= 0) && (var_index15 <= var_end13)) && true);
                                            if (var_traversal17) {
                                                out.write("\r\n                        \t<div class=\"filter__item\">\r\n                            \t<button type=\"button\" class=\"filter-panel__button\"");
                                                {
                                                    Object var_attrvalue18 = item;
                                                    {
                                                        Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                            if (var_shoulddisplayattr21) {
                                                                out.write(" data-id");
                                                                {
                                                                    boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                    if (!var_istrueattr20) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">");
                                                {
                                                    String var_22 = (("\r\n                                \t" + renderContext.getObjectModel().toString(renderContext.call("xss", item, "text"))) + "\r\n                            \t");
                                                    out.write(renderContext.getObjectModel().toString(var_22));
                                                }
                                                out.write("</button>\r\n                        \t</div>\r\n                    \t");
                                            }
                                        }
                                        var_index15++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar8_list_coerced$ = null;
        }
        out.write("\r\n                \t</div>\r\n        \t\t</div>\r\n\t\t\t</div>");
    }
}
out.write("\r\n\t\t\t<div class=\"list-items\">\r\n\t\t\t\t");
{
    Object var_collectionvar23 = renderContext.getObjectModel().resolveProperty(_global_filterpanel, "expFragments");
    {
        long var_size24 = ((var_collectionvar23_list_coerced$ == null ? (var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23)) : var_collectionvar23_list_coerced$).size());
        {
            boolean var_notempty25 = (var_size24 > 0);
            if (var_notempty25) {
                {
                    long var_end28 = var_size24;
                    {
                        boolean var_validstartstepend29 = (((0 < var_size24) && true) && (var_end28 > 0));
                        if (var_validstartstepend29) {
                            if (var_collectionvar23_list_coerced$ == null) {
                                var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23);
                            }
                            long var_index30 = 0;
                            for (Object item : var_collectionvar23_list_coerced$) {
                                {
                                    boolean var_traversal32 = (((var_index30 >= 0) && (var_index30 <= var_end28)) && true);
                                    if (var_traversal32) {
                                        out.write("<div>\r\n\t\t\t\t");
                                        {
                                            Object var_resourcecontent33 = renderContext.call("includeResource", null, obj().with("path", renderContext.getObjectModel().resolveProperty(item, "path")).with("selectors", "content").with("wcmmode", "disabled"));
                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent33));
                                        }
                                        out.write("\r\n\t\t\t\t</div>\n");
                                    }
                                }
                                var_index30++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar23_list_coerced$ = null;
}
out.write("\r\n\t\t\t</div>\r\n\t   </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

