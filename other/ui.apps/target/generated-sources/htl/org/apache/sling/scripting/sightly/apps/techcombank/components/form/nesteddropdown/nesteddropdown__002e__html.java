/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.nesteddropdown;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class nesteddropdown__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_nestedmodel = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_nestedmodel = renderContext.call("use", com.techcombank.core.models.NestedDropdownModel.class.getName(), obj());
out.write("\r\n\r\n<div");
{
    String var_attrcontent5 = ("dropdown section " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "analytics")) ? "analytics-form-field" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
{
    Object var_attrvalue6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "requiredMessage"), "html");
    {
        boolean var_shoulddisplayattr9 = ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6)));
        if (var_shoulddisplayattr9) {
            out.write(" data-required");
            {
                boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                if (!var_istrueattr8) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue6));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(">\r\n    <div class=\"cmp-form-options__drop-down\">\r\n        <label>");
{
    Object var_10 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "fieldLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_10));
}
out.write("<span>*</span></label>\r\n        <div");
{
    String var_attrcontent11 = ((("dropdown-wrapper " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "item"), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "item"), "city-list")) ? "" : "disabled"), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
    out.write("\"");
}
{
    Object var_attrvalue12 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "placeholderText"), "html");
    {
        boolean var_shoulddisplayattr15 = ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12)));
        if (var_shoulddisplayattr15) {
            out.write(" data-placeholder");
            {
                boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                if (!var_istrueattr14) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue12));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(" data-selected=\"\">\r\n            <div class=\"dropdown-inputbase\" role=\"button\">\r\n                <input class=\"dropdown-input\"");
{
    Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_global_nestedmodel, "fieldName");
    {
        Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
        {
            boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
            if (var_shoulddisplayattr19) {
                out.write(" name");
                {
                    boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                    if (!var_istrueattr18) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" aria-hidden=\"true\" tabindex=\"-1\" value=\"\"/>\r\n                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\" alt=\"\"/>\r\n            </div>\r\n            <ul class=\"list-dropdown\" hidden>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

