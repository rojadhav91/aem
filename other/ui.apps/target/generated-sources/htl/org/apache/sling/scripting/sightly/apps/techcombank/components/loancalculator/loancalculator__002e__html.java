/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.loancalculator;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class loancalculator__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_calculator = null;
Object _global_template = null;
Collection var_collectionvar21_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n\r\n");
_global_calculator = renderContext.call("use", com.techcombank.core.models.LoanCalculator.class.getName(), obj());
out.write("\r\n    ");
{
    Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
        out.write("\r\n        <p");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n    ");
    }
}
out.write("\r\n    <div>\r\n        <section class=\"container insurance-calculation decrease-interest-loan\"");
{
    Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_calculator, "totalLabel");
    {
        Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
        {
            boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
            if (var_shoulddisplayattr13) {
                out.write(" data-total");
                {
                    boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                    if (!var_istrueattr12) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n            ");
{
    boolean var_testvariable14 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_calculator, "tabs"), ""));
    if (var_testvariable14) {
        out.write("\r\n                <div class=\"tcb-tabs\"");
        {
            Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_calculator, "tabs");
            {
                Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                {
                    boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                    if (var_shoulddisplayattr18) {
                        out.write(" data-tabs");
                        {
                            boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                            if (!var_istrueattr17) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></div>\r\n            ");
    }
}
out.write("\r\n            <div class=\"insurance-calculation__container\">\r\n                <div class=\"insurance-calculation__content\">\r\n                    <div class=\"insurance-calculation__panel loan-realestate__panel\" data-id=\"0\">\r\n                        ");
_global_template = renderContext.call("use", "leftpanel.html", obj());
out.write("\r\n                            ");
{
    Object var_templatevar19 = renderContext.getObjectModel().resolveProperty(_global_template, "leftpanel");
    {
        Object var_templateoptions20_field$_calculator = _global_calculator;
        {
            java.util.Map var_templateoptions20 = obj().with("calculator", var_templateoptions20_field$_calculator);
            callUnit(out, renderContext, var_templatevar19, var_templateoptions20);
        }
    }
}
out.write("\r\n                        \r\n                        <!-- check the type -->\r\n                        ");
{
    Object var_collectionvar21 = renderContext.getObjectModel().resolveProperty(_global_calculator, "calculatorFields");
    {
        long var_size22 = ((var_collectionvar21_list_coerced$ == null ? (var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21)) : var_collectionvar21_list_coerced$).size());
        {
            boolean var_notempty23 = (var_size22 > 0);
            if (var_notempty23) {
                {
                    long var_end26 = var_size22;
                    {
                        boolean var_validstartstepend27 = (((0 < var_size22) && true) && (var_end26 > 0));
                        if (var_validstartstepend27) {
                            if (var_collectionvar21_list_coerced$ == null) {
                                var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21);
                            }
                            long var_index28 = 0;
                            for (Object calculationtype : var_collectionvar21_list_coerced$) {
                                {
                                    long calculationtypelist_field$_index = var_index28;
                                    {
                                        boolean var_traversal30 = (((var_index28 >= 0) && (var_index28 <= var_end26)) && true);
                                        if (var_traversal30) {
                                            out.write("\r\n                            ");
                                            {
                                                boolean var_testvariable31 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(calculationtype, "calculationTypeDropdown"), "decreasingBalanceSheet"));
                                                if (var_testvariable31) {
                                                    out.write("\r\n                                ");
_global_template = renderContext.call("use", "decreasingbalancesheet.html", obj());
                                                    out.write("\r\n                                    ");
                                                    {
                                                        Object var_templatevar32 = renderContext.getObjectModel().resolveProperty(_global_template, "decreasingbalancesheet");
                                                        {
                                                            Object var_templateoptions33_field$_calculationtype = calculationtype;
                                                            {
                                                                Object var_templateoptions33_field$_calculator = _global_calculator;
                                                                {
                                                                    long var_templateoptions33_field$_index = calculationtypelist_field$_index;
                                                                    {
                                                                        java.util.Map var_templateoptions33 = obj().with("calculationType", var_templateoptions33_field$_calculationtype).with("calculator", var_templateoptions33_field$_calculator).with("index", var_templateoptions33_field$_index);
                                                                        callUnit(out, renderContext, var_templatevar32, var_templateoptions33);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("\r\n                                \r\n                            ");
                                                }
                                            }
                                            out.write("\r\n\r\n                            ");
                                            {
                                                boolean var_testvariable34 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(calculationtype, "calculationTypeDropdown"), "tableOfFixedMonthlyPayments"));
                                                if (var_testvariable34) {
                                                    out.write("\r\n                                ");
_global_template = renderContext.call("use", "fixedmonthlypayments.html", obj());
                                                    out.write("\r\n                                    ");
                                                    {
                                                        Object var_templatevar35 = renderContext.getObjectModel().resolveProperty(_global_template, "fixedmonthlypayments");
                                                        {
                                                            Object var_templateoptions36_field$_calculationtype = calculationtype;
                                                            {
                                                                Object var_templateoptions36_field$_calculator = _global_calculator;
                                                                {
                                                                    long var_templateoptions36_field$_index = calculationtypelist_field$_index;
                                                                    {
                                                                        java.util.Map var_templateoptions36 = obj().with("calculationType", var_templateoptions36_field$_calculationtype).with("calculator", var_templateoptions36_field$_calculator).with("index", var_templateoptions36_field$_index);
                                                                        callUnit(out, renderContext, var_templatevar35, var_templateoptions36);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("\r\n                                \r\n                            ");
                                                }
                                            }
                                            out.write("\r\n                            ");
                                            {
                                                boolean var_testvariable37 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(calculationtype, "calculationTypeDropdown"), "carLoan"));
                                                if (var_testvariable37) {
                                                    out.write("\r\n                                ");
_global_template = renderContext.call("use", "autoloan.html", obj());
                                                    out.write("\r\n                                    ");
                                                    {
                                                        Object var_templatevar38 = renderContext.getObjectModel().resolveProperty(_global_template, "autoloan");
                                                        {
                                                            Object var_templateoptions39_field$_calculationtype = calculationtype;
                                                            {
                                                                Object var_templateoptions39_field$_calculator = _global_calculator;
                                                                {
                                                                    long var_templateoptions39_field$_index = calculationtypelist_field$_index;
                                                                    {
                                                                        java.util.Map var_templateoptions39 = obj().with("calculationType", var_templateoptions39_field$_calculationtype).with("calculator", var_templateoptions39_field$_calculator).with("index", var_templateoptions39_field$_index);
                                                                        callUnit(out, renderContext, var_templatevar38, var_templateoptions39);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("\r\n                                \r\n                            ");
                                                }
                                            }
                                            out.write("\r\n                        ");
                                        }
                                    }
                                }
                                var_index28++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar21_list_coerced$ = null;
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"modal\">\r\n                <div class=\"modal-backdrop\" aria-hidden=\"true\"></div>\r\n                <div class=\"modal-dialog\">\r\n                    <div class=\"modal-content\">\r\n                        <div class=\"modal-header\">\r\n                            <span class=\"modal-title\">");
{
    Object var_40 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "modalHeader"), "text");
    out.write(renderContext.getObjectModel().toString(var_40));
}
out.write("</span>\r\n                            <span class=\"material-symbols-outlined close-modal\">close</span>\r\n                        </div>\r\n                        <div class=\"modal-inner-content\">\r\n                            <div class=\"table-sum\">\r\n                                <table class=\"table-sum-content\">\r\n                                    <thead class=\"tbl-header\">\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_41 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "serialNoLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_41));
}
out.write("</span></span>\r\n                                        </th>\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_42 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "repaymentPeriodLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_42));
}
out.write("</span></span>\r\n                                        </th>\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "remainingAmountLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_43));
}
out.write("</span></span>\r\n                                        </th>\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "originalAmountLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_44));
}
out.write("</span></span>\r\n                                        </th>\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "interestAmountLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_45));
}
out.write("</span></span>\r\n                                        </th>\r\n                                        <th class=\"table-cell cell-header\" role=\"columnheader\" scope=\"row\">\r\n                                            <span><span>");
{
    Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "sumLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_46));
}
out.write("</span></span>\r\n                                        </th>\r\n                                    </thead>\r\n                                    <tbody class=\"tbl-body\"></tbody>\r\n                                    <tbody class=\"footer-table\">\r\n                                        <tr class=\"footer-tbl\">\r\n                                            <td class=\"table-cell footer-cell\">");
{
    Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_calculator, "totalLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_47));
}
out.write("</td>\r\n                                            <td class=\"table-cell footer-cell\" colspan=\"2\"></td>\r\n                                            <td class=\"table-cell footer-cell origin_debt\"></td>\r\n                                            <td class=\"table-cell footer-cell profit\"></td>\r\n                                            <td class=\"table-cell footer-cell total_origin_and_profit\"></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

