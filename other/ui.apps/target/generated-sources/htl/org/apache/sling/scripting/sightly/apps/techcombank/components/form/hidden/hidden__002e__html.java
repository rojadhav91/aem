/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.hidden;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class hidden__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_field = null;
Object _global_templates = null;
Object _global_appendtext = null;
_global_field = renderContext.call("use", com.techcombank.core.models.FormHiddenFieldModel.class.getName(), obj());
_global_templates = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_appendtext = (((renderContext.getObjectModel().toString(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_field, "name")) ? "name=" : renderContext.getObjectModel().resolveProperty(_global_field, "name")))) + renderContext.getObjectModel().toString(renderContext.getObjectModel().resolveProperty(_global_field, "name"))) + renderContext.getObjectModel().toString(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_field, "value")) ? " | value=" : renderContext.getObjectModel().resolveProperty(_global_field, "value"))))) + renderContext.getObjectModel().toString(renderContext.getObjectModel().resolveProperty(_global_field, "value")));
out.write("<input type=\"hidden\"");
{
    Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_global_field, "id");
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" id");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue4 = renderContext.getObjectModel().resolveProperty(_global_field, "name");
    {
        Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
        {
            boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
            if (var_shoulddisplayattr7) {
                out.write(" name");
                {
                    boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                    if (!var_istrueattr6) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue8 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_field, "checkboxValueText")) ? renderContext.getObjectModel().resolveProperty(_global_field, "valueText") : renderContext.getObjectModel().resolveProperty(_global_field, "value"));
    {
        Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
        {
            boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
            if (var_shoulddisplayattr11) {
                out.write(" value");
                {
                    boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                    if (!var_istrueattr10) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>");
{
    Object var_templatevar12 = renderContext.getObjectModel().resolveProperty(_global_templates, "placeholder");
    {
        boolean var_templateoptions13_field$_isempty = true;
        {
            Object var_templateoptions13_field$_emptytextappend = _global_appendtext;
            {
                Object var_templateoptions13_field$_classappend = null;
                {
                    java.util.Map var_templateoptions13 = obj().with("isEmpty", var_templateoptions13_field$_isempty).with("emptyTextAppend", var_templateoptions13_field$_emptytextappend).with("classAppend", var_templateoptions13_field$_classappend);
                    callUnit(out, renderContext, var_templatevar12, var_templateoptions13);
                }
            }
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

