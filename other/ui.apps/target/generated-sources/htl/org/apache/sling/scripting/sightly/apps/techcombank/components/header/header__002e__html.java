/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.header;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class header__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_headermodel = null;
Object _dynamic_component = bindings.get("component");
Collection var_collectionvar52_list_coerced$ = null;
Collection var_collectionvar64_list_coerced$ = null;
Collection var_collectionvar89_list_coerced$ = null;
Collection var_collectionvar112_list_coerced$ = null;
Collection var_collectionvar134_list_coerced$ = null;
Collection var_collectionvar146_list_coerced$ = null;
Collection var_collectionvar164_list_coerced$ = null;
Collection var_collectionvar220_list_coerced$ = null;
Collection var_collectionvar256_list_coerced$ = null;
Collection var_collectionvar276_list_coerced$ = null;
Collection var_collectionvar288_list_coerced$ = null;
Collection var_collectionvar312_list_coerced$ = null;
Collection var_collectionvar334_list_coerced$ = null;
Collection var_collectionvar345_list_coerced$ = null;
Collection var_collectionvar367_list_coerced$ = null;
Collection var_collectionvar377_list_coerced$ = null;
_global_headermodel = renderContext.call("use", com.techcombank.core.models.HeaderModel.class.getName(), obj());
out.write("\r\n    <div");
{
    String var_attrcontent0 = ("header_layout " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage")) ? "prelogin-header" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent0));
    out.write("\"");
}
out.write(">\r\n        <div class=\"header-navigation\">\r\n            <!-- Primary Nav -->\r\n            <div class=\"navigation_primary content-wrapper\">\r\n\r\n                <div class=\"navigation_primary-wrapper\">\r\n                    <!-- Left content Start -->\r\n                    <div class=\"navigation-primary_left\">\r\n                        <!-- LOGO code START.. only to be displayed when Secondary Nav is not present -->\r\n                        ");
{
    Object var_testvariable1 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable1)) {
        out.write("\r\n                            <div class=\"header-logo\" itemscope itemtype=\"https://schema.org/Organization\">\r\n                                <meta itemprop=\"name\" content=\"Techcombank\"/>\r\n                                <a itemprop=\"url\"");
        {
            Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageLink");
            {
                Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "uri");
                {
                    boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                    if (var_shoulddisplayattr5) {
                        out.write(" href");
                        {
                            boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                            if (!var_istrueattr4) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
        {
            String var_attrcontent6 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | Header Logo'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
            out.write("\"");
        }
        {
            String var_attrcontent7 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageLinkInteractionType"), "attribute"))) + "'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent7));
            out.write("\"");
        }
        out.write(">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 1024px)\"");
        {
            Object var_attrvalue8 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath");
            {
                Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
                {
                    boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
                    if (var_shoulddisplayattr11) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                            if (!var_istrueattr10) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                        <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath");
            {
                Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                {
                    boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                    if (var_shoulddisplayattr15) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                            if (!var_istrueattr14) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath");
            {
                Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                {
                    boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                    if (var_shoulddisplayattr19) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                            if (!var_istrueattr18) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                        <source media=\"(min-width: 1025px)\"");
        {
            Object var_attrvalue20 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath");
            {
                Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
                {
                    boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (!((Object)false).equals(var_attrvalue20))));
                    if (var_shoulddisplayattr23) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr22 = (var_attrvalue20.equals(true));
                            if (!var_istrueattr22) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>                                        \r\n                                        <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath");
            {
                Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                {
                    boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                    if (var_shoulddisplayattr27) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                            if (!var_istrueattr26) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath");
            {
                Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                {
                    boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                    if (var_shoulddisplayattr31) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                            if (!var_istrueattr30) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                        <img itemprop=\"logo\"");
        {
            Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath");
            {
                Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "uri");
                {
                    boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                    if (var_shoulddisplayattr35) {
                        out.write(" src");
                        {
                            boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                            if (!var_istrueattr34) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageAltText");
            {
                Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                {
                    boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                    if (var_shoulddisplayattr39) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                            if (!var_istrueattr38) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    </picture>\r\n                                </a>\r\n                            </div>\r\n                        ");
    }
}
out.write("\r\n                        <!-- LOGO code END.. -->\r\n                        <!-- See all Start -->\r\n                        ");
{
    Object var_testvariable40 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductEnabled");
    if (renderContext.getObjectModel().toBoolean(var_testvariable40)) {
        out.write("\r\n                            <a");
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductsURL");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "uri");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" href");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"navigation-primary_item navigation-primary_item-dropdown_list\" data-tracking-click-event=\"linkClick\"");
        {
            String var_attrcontent45 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductsLink"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
            out.write("\"");
        }
        {
            String var_attrcontent46 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductsURLInteractionType"), "attribute"))) + "'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent46));
            out.write("\"");
        }
        out.write(">\r\n                                <img class=\"icon-back\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/dropdown-arrow.svg\"/>\r\n                                <span class=\"dropdown_holder\">");
        {
            Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductsLink"), "text");
            out.write(renderContext.getObjectModel().toString(var_47));
        }
        out.write("</span>\r\n                            </a>\r\n                        ");
    }
}
out.write("\r\n                        <!-- See all End -->\r\n                        <!-- different page dropdown Start -->\r\n                        ");
{
    Object var_testvariable48 = ((renderContext.getObjectModel().toBoolean((((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductEnabled"))) ? renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu") : (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductEnabled")))))) ? (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage"))) : (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductEnabled"))) ? renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu") : (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "allProductEnabled")))))));
    if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
        out.write("\r\n                            <div id=\"dropdown_btn\" class=\"navigation-primary_item navigation-primary_item-dropdown_list show-active-arrow\">\r\n                                <span class=\"dropdown_holder\">\r\n                                    ");
        {
            boolean var_testvariable49 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePageMenuListed")));
            if (var_testvariable49) {
                {
                    String var_50 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "defaultMenuLabel"), "text"))) + "\r\n                                    ");
                    out.write(renderContext.getObjectModel().toString(var_50));
                }
            }
        }
        out.write("\r\n                                    ");
        {
            Object var_testvariable51 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePageMenuListed");
            if (renderContext.getObjectModel().toBoolean(var_testvariable51)) {
                out.write("\r\n                                        ");
                {
                    Object var_collectionvar52 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu");
                    {
                        long var_size53 = ((var_collectionvar52_list_coerced$ == null ? (var_collectionvar52_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar52)) : var_collectionvar52_list_coerced$).size());
                        {
                            boolean var_notempty54 = (var_size53 > 0);
                            if (var_notempty54) {
                                {
                                    long var_end57 = var_size53;
                                    {
                                        boolean var_validstartstepend58 = (((0 < var_size53) && true) && (var_end57 > 0));
                                        if (var_validstartstepend58) {
                                            if (var_collectionvar52_list_coerced$ == null) {
                                                var_collectionvar52_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar52);
                                            }
                                            long var_index59 = 0;
                                            for (Object landingmenuitems : var_collectionvar52_list_coerced$) {
                                                {
                                                    boolean var_traversal61 = (((var_index59 >= 0) && (var_index59 <= var_end57)) && true);
                                                    if (var_traversal61) {
                                                        out.write("\r\n                                            ");
                                                        {
                                                            boolean var_testvariable62 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePrimaryMenuPagePath")));
                                                            if (var_testvariable62) {
                                                                {
                                                                    String var_63 = ("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "text")));
                                                                    out.write(renderContext.getObjectModel().toString(var_63));
                                                                }
                                                            }
                                                        }
                                                        out.write("\r\n                                        ");
                                                    }
                                                }
                                                var_index59++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var_collectionvar52_list_coerced$ = null;
                }
                out.write("\r\n                                    ");
            }
        }
        out.write("\r\n                                </span>\r\n                                <span class=\"dropdown-arrow material-symbols-outlined\">\r\n                                    expand_more\r\n                                </span>\r\n                                <ul class=\"header_list_dropdown\">\r\n                                    ");
        {
            Object var_collectionvar64 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu");
            {
                long var_size65 = ((var_collectionvar64_list_coerced$ == null ? (var_collectionvar64_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar64)) : var_collectionvar64_list_coerced$).size());
                {
                    boolean var_notempty66 = (var_size65 > 0);
                    if (var_notempty66) {
                        {
                            long var_end69 = var_size65;
                            {
                                boolean var_validstartstepend70 = (((0 < var_size65) && true) && (var_end69 > 0));
                                if (var_validstartstepend70) {
                                    if (var_collectionvar64_list_coerced$ == null) {
                                        var_collectionvar64_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar64);
                                    }
                                    long var_index71 = 0;
                                    for (Object landingmenuitems : var_collectionvar64_list_coerced$) {
                                        {
                                            boolean var_traversal73 = (((var_index71 >= 0) && (var_index71 <= var_end69)) && true);
                                            if (var_traversal73) {
                                                out.write("\r\n                                        <li");
                                                {
                                                    String var_attrcontent74 = ("dropdown-item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePrimaryMenuPagePath"))) ? "active" : ""), "attribute")));
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent74));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                            <a");
                                                {
                                                    Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL");
                                                    {
                                                        Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                                                            if (var_shoulddisplayattr78) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                                                                    if (!var_istrueattr77) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingTarget");
                                                    {
                                                        Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
                                                            if (var_shoulddisplayattr82) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                                                                    if (!var_istrueattr81) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent83 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent83));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent84 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURLInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_85 = ("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "text")));
                                                    out.write(renderContext.getObjectModel().toString(var_85));
                                                }
                                                out.write("</a>\r\n                                        </li>\r\n                                    ");
                                            }
                                        }
                                        var_index71++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar64_list_coerced$ = null;
        }
        out.write("\r\n                                </ul>\r\n                            </div>\r\n                        ");
    }
}
out.write("\r\n                        <!-- different page dropdown End -->\r\n                        <!--  Left Listing start: Explore item text START-->\r\n                        ");
{
    boolean var_testvariable86 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage")));
    if (var_testvariable86) {
        out.write("\r\n                         ");
        {
            Object var_testvariable87 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "discoverLabel");
            if (renderContext.getObjectModel().toBoolean(var_testvariable87)) {
                out.write("\r\n                           \t <div class=\"navigation-primary_item discover_dropdown_btn\">\r\n                                  <span class=\"link_text\"><span>");
                {
                    Object var_88 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "discoverLabel"), "text");
                    out.write(renderContext.getObjectModel().toString(var_88));
                }
                out.write("</span></span>\r\n                           \t </div>\r\n                         ");
            }
        }
        out.write("\r\n                            <!--  Left Listing start: Explore item text END-->\r\n                            <!-- Left listing: menu Start-->\r\n                            ");
        {
            Object var_collectionvar89 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "discoverMenu");
            {
                long var_size90 = ((var_collectionvar89_list_coerced$ == null ? (var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89)) : var_collectionvar89_list_coerced$).size());
                {
                    boolean var_notempty91 = (var_size90 > 0);
                    if (var_notempty91) {
                        {
                            long var_end94 = var_size90;
                            {
                                boolean var_validstartstepend95 = (((0 < var_size90) && true) && (var_end94 > 0));
                                if (var_validstartstepend95) {
                                    if (var_collectionvar89_list_coerced$ == null) {
                                        var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89);
                                    }
                                    long var_index96 = 0;
                                    for (Object discovermenuitems : var_collectionvar89_list_coerced$) {
                                        {
                                            boolean var_traversal98 = (((var_index96 >= 0) && (var_index96 <= var_end94)) && true);
                                            if (var_traversal98) {
                                                out.write("\r\n                                <div");
                                                {
                                                    String var_attrcontent99 = ("navigation-primary_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "currentActivePage"))) ? "show-active-arrow" : ""), "attribute")));
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent99));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                    <a");
                                                {
                                                    Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURL");
                                                    {
                                                        Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                            if (var_shoulddisplayattr103) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                    if (!var_istrueattr102) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverTarget");
                                                    {
                                                        Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                                                            if (var_shoulddisplayattr107) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                                                    if (!var_istrueattr106) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent108 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuLabel"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent108));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent109 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURLInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                                    out.write("\"");
                                                }
                                                out.write(">                                        \r\n                                        <span class=\"link_text\">");
                                                {
                                                    Object var_110 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuLabel"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_110));
                                                }
                                                out.write("</span>\r\n                                    </a>\r\n                                </div>\r\n                            ");
                                            }
                                        }
                                        var_index96++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar89_list_coerced$ = null;
        }
        out.write("\r\n                            <!-- Left listing: menu End-->\r\n                        ");
    }
}
out.write("\r\n                    </div>\r\n                    <!-- Left content End -->\r\n\r\n                    <!-- Right content Start -->\r\n                    <div class=\"navigation-primary_right\">\r\n                        <!-- Search -->\r\n                        ");
{
    Object var_testvariable111 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "turnOnSearchOnNavigation");
    if (renderContext.getObjectModel().toBoolean(var_testvariable111)) {
        out.write("<a class=\"search-primary-btn\" href=\"#search-primary-modal\">\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/search-primary-icon.svg\" alt=\"search-icon\"/>\r\n                        </a>");
    }
}
out.write("\r\n                        <!-- Right listing: menu Start-->\r\n                        ");
{
    Object var_collectionvar112 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "rightNavigationMenu");
    {
        long var_size113 = ((var_collectionvar112_list_coerced$ == null ? (var_collectionvar112_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar112)) : var_collectionvar112_list_coerced$).size());
        {
            boolean var_notempty114 = (var_size113 > 0);
            if (var_notempty114) {
                {
                    long var_end117 = var_size113;
                    {
                        boolean var_validstartstepend118 = (((0 < var_size113) && true) && (var_end117 > 0));
                        if (var_validstartstepend118) {
                            if (var_collectionvar112_list_coerced$ == null) {
                                var_collectionvar112_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar112);
                            }
                            long var_index119 = 0;
                            for (Object rightnavigationmenuitem : var_collectionvar112_list_coerced$) {
                                {
                                    boolean rightnavigationmenuitemlist_field$_last = (var_index119 == (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.SUB.eval(var_size113, 1)).longValue()));
                                    {
                                        boolean var_traversal121 = (((var_index119 >= 0) && (var_index119 <= var_end117)) && true);
                                        if (var_traversal121) {
                                            out.write("\r\n                            <div");
                                            {
                                                String var_attrcontent122 = ((("navigation-primary_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "currentActivePage"))) ? "show-active-arrow" : ""), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (rightnavigationmenuitemlist_field$_last ? "has-right-border" : ""), "attribute")));
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent122));
                                                out.write("\"");
                                            }
                                            out.write(">\r\n                                <a");
                                            {
                                                Object var_attrvalue123 = renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationPageURL");
                                                {
                                                    Object var_attrcontent124 = renderContext.call("xss", var_attrvalue123, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr126 = (((null != var_attrcontent124) && (!"".equals(var_attrcontent124))) && ((!"".equals(var_attrvalue123)) && (!((Object)false).equals(var_attrvalue123))));
                                                        if (var_shoulddisplayattr126) {
                                                            out.write(" href");
                                                            {
                                                                boolean var_istrueattr125 = (var_attrvalue123.equals(true));
                                                                if (!var_istrueattr125) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent124));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            {
                                                Object var_attrvalue127 = renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationPageURL");
                                                {
                                                    Object var_attrcontent128 = renderContext.call("xss", var_attrvalue127, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr130 = (((null != var_attrcontent128) && (!"".equals(var_attrcontent128))) && ((!"".equals(var_attrvalue127)) && (!((Object)false).equals(var_attrvalue127))));
                                                        if (var_shoulddisplayattr130) {
                                                            out.write(" target");
                                                            {
                                                                boolean var_istrueattr129 = (var_attrvalue127.equals(true));
                                                                if (!var_istrueattr129) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent128));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
                                            {
                                                String var_attrcontent131 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationLabel"), "attribute"))) + "'}");
                                                out.write(" data-tracking-click-info-value=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent131));
                                                out.write("\"");
                                            }
                                            {
                                                String var_attrcontent132 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationPageURLInteractionType"), "attribute"))) + "'}}");
                                                out.write(" data-tracking-web-interaction-value=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent132));
                                                out.write("\"");
                                            }
                                            out.write(">\r\n                                    <span class=\"link_text\">");
                                            {
                                                Object var_133 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightnavigationmenuitem, "navigationLabel"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_133));
                                            }
                                            out.write("</span>\r\n                                </a>\r\n                            </div>\r\n                        ");
                                        }
                                    }
                                }
                                var_index119++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar112_list_coerced$ = null;
}
out.write("\r\n                        <!-- Right listing: menu End-->\r\n                        <!-- Lang dropdown Start -->\r\n                        <div class=\"language_dropdown_item\">\r\n                            <span class=\"material-symbols-outlined\">public</span>\r\n                            <span class=\"dropdown_holder\">\r\n                                ");
{
    Object var_collectionvar134 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "languageDetails");
    {
        long var_size135 = ((var_collectionvar134_list_coerced$ == null ? (var_collectionvar134_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar134)) : var_collectionvar134_list_coerced$).size());
        {
            boolean var_notempty136 = (var_size135 > 0);
            if (var_notempty136) {
                {
                    long var_end139 = var_size135;
                    {
                        boolean var_validstartstepend140 = (((0 < var_size135) && true) && (var_end139 > 0));
                        if (var_validstartstepend140) {
                            if (var_collectionvar134_list_coerced$ == null) {
                                var_collectionvar134_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar134);
                            }
                            long var_index141 = 0;
                            for (Object languageitems : var_collectionvar134_list_coerced$) {
                                {
                                    boolean var_traversal143 = (((var_index141 >= 0) && (var_index141 <= var_end139)) && true);
                                    if (var_traversal143) {
                                        out.write("\r\n                                    ");
                                        {
                                            boolean var_testvariable144 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(languageitems, "languageUrl"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "currentAliasActivePage")));
                                            if (var_testvariable144) {
                                                {
                                                    String var_145 = ("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(languageitems, "language"), "text")));
                                                    out.write(renderContext.getObjectModel().toString(var_145));
                                                }
                                            }
                                        }
                                        out.write("\r\n                                ");
                                    }
                                }
                                var_index141++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar134_list_coerced$ = null;
}
out.write("\r\n                            </span>\r\n                            <span class=\"dropdown-arrow material-symbols-outlined\">\r\n                                expand_more\r\n                            </span>\r\n                            <ul class=\"language_dropdown\">\r\n                                ");
{
    Object var_collectionvar146 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "languageDetails");
    {
        long var_size147 = ((var_collectionvar146_list_coerced$ == null ? (var_collectionvar146_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar146)) : var_collectionvar146_list_coerced$).size());
        {
            boolean var_notempty148 = (var_size147 > 0);
            if (var_notempty148) {
                {
                    long var_end151 = var_size147;
                    {
                        boolean var_validstartstepend152 = (((0 < var_size147) && true) && (var_end151 > 0));
                        if (var_validstartstepend152) {
                            if (var_collectionvar146_list_coerced$ == null) {
                                var_collectionvar146_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar146);
                            }
                            long var_index153 = 0;
                            for (Object languageitems : var_collectionvar146_list_coerced$) {
                                {
                                    boolean var_traversal155 = (((var_index153 >= 0) && (var_index153 <= var_end151)) && true);
                                    if (var_traversal155) {
                                        out.write("\r\n                                    <li");
                                        {
                                            String var_attrcontent156 = ("dropdown-item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(languageitems, "languageUrl"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "currentAliasActivePage"))) ? "active" : ""), "attribute")));
                                            out.write(" class=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent156));
                                            out.write("\"");
                                        }
                                        out.write(">\r\n                                        <a");
                                        {
                                            Object var_attrvalue157 = renderContext.getObjectModel().resolveProperty(languageitems, "languageUrl");
                                            {
                                                Object var_attrcontent158 = renderContext.call("xss", var_attrvalue157, "uri");
                                                {
                                                    boolean var_shoulddisplayattr160 = (((null != var_attrcontent158) && (!"".equals(var_attrcontent158))) && ((!"".equals(var_attrvalue157)) && (!((Object)false).equals(var_attrvalue157))));
                                                    if (var_shoulddisplayattr160) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr159 = (var_attrvalue157.equals(true));
                                                            if (!var_istrueattr159) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent158));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(" x-cq-linkchecker=\"skip\">\r\n                                            <span>");
                                        {
                                            Object var_161 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(languageitems, "language"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_161));
                                        }
                                        out.write("</span>\r\n                                        </a>\r\n                                    </li>\r\n                                ");
                                    }
                                }
                                var_index153++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar146_list_coerced$ = null;
}
out.write("\r\n                            </ul>\r\n                        </div>\r\n                        <!-- Lang dropdown End -->\r\n                        <!-- END -->\r\n                        <!-- Login Start : only visible when no secondary Nav -->\r\n                        ");
{
    Object var_testvariable162 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable162)) {
        out.write("\r\n                            <div class=\"navigation-secondary_actions\">\r\n                                <div id=\"login-btn\" class=\"link_component-link\" target=\"_blank\">\r\n\r\n                                    <span class=\"link_text\">\r\n                                        <div class=\"navigation-secondary_actions-icon\">\r\n                                            <span>");
        {
            Object var_163 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelText"), "text");
            out.write(renderContext.getObjectModel().toString(var_163));
        }
        out.write("</span> \r\n                                            <img src=\"https://d1kndcit1zrj97.cloudfront.net/uploads/white_arrow_4fd026b4ee.svg?w=32&amp;q=75\"/>\r\n                                        </div>\r\n                                    </span>\r\n                                    <div class=\"login__drop-down\">\r\n                                        ");
        {
            Object var_collectionvar164 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginData");
            {
                long var_size165 = ((var_collectionvar164_list_coerced$ == null ? (var_collectionvar164_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar164)) : var_collectionvar164_list_coerced$).size());
                {
                    boolean var_notempty166 = (var_size165 > 0);
                    if (var_notempty166) {
                        {
                            long var_end169 = var_size165;
                            {
                                boolean var_validstartstepend170 = (((0 < var_size165) && true) && (var_end169 > 0));
                                if (var_validstartstepend170) {
                                    if (var_collectionvar164_list_coerced$ == null) {
                                        var_collectionvar164_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar164);
                                    }
                                    long var_index171 = 0;
                                    for (Object logindataitems : var_collectionvar164_list_coerced$) {
                                        {
                                            boolean var_traversal173 = (((var_index171 >= 0) && (var_index171 <= var_end169)) && true);
                                            if (var_traversal173) {
                                                out.write("\r\n                                            <div class=\"drop-down__item\">\r\n                                                <a");
                                                {
                                                    Object var_attrvalue174 = renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginURL");
                                                    {
                                                        Object var_attrcontent175 = renderContext.call("xss", var_attrvalue174, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr177 = (((null != var_attrcontent175) && (!"".equals(var_attrcontent175))) && ((!"".equals(var_attrvalue174)) && (!((Object)false).equals(var_attrvalue174))));
                                                            if (var_shoulddisplayattr177) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr176 = (var_attrvalue174.equals(true));
                                                                    if (!var_istrueattr176) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent175));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent178 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginText"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent178));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent179 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginURLInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent179));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_180 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginText"), "text"))) + "\r\n                                                ");
                                                    out.write(renderContext.getObjectModel().toString(var_180));
                                                }
                                                out.write("</a>\r\n                                            </div>\r\n                                        ");
                                            }
                                        }
                                        var_index171++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar164_list_coerced$ = null;
        }
        out.write("\r\n                                    </div>\r\n                                </div>\r\n                                <!-- Hamburger : Start -->\r\n                                <div class=\"hambuger-icon\"><span></span><span></span><span></span></div>\r\n                                <!-- Hamburger : End -->\r\n                            </div>\r\n                        ");
    }
}
out.write("\r\n                        <!-- Login End -->\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- Primary Nav END -->\r\n\r\n            <!-- Secondary Nav Start-->\r\n            <div class=\"navigation_secondary\">\r\n                <div class=\"navigation_secondary-wrapper content-wrapper\">\r\n                    <!-- Header Logo Start-->\r\n\r\n                    <div class=\"header-logo\" itemscope itemtype=\"https://schema.org/Organization\">\r\n                        <meta itemprop=\"name\" content=\"Techcombank\"/>\r\n                        <a itemprop=\"url\"");
{
    Object var_attrvalue181 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageLink");
    {
        Object var_attrcontent182 = renderContext.call("xss", var_attrvalue181, "uri");
        {
            boolean var_shoulddisplayattr184 = (((null != var_attrcontent182) && (!"".equals(var_attrcontent182))) && ((!"".equals(var_attrvalue181)) && (!((Object)false).equals(var_attrvalue181))));
            if (var_shoulddisplayattr184) {
                out.write(" href");
                {
                    boolean var_istrueattr183 = (var_attrvalue181.equals(true));
                    if (!var_istrueattr183) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent182));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent185 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | Header Logo'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent185));
    out.write("\"");
}
{
    String var_attrcontent186 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageLinkInteractionType"), "attribute"))) + "'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent186));
    out.write("\"");
}
out.write(">\r\n                            <picture>\r\n                                <source media=\"(max-width: 1024px)\"");
{
    Object var_attrvalue187 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath"), "html");
    {
        boolean var_shoulddisplayattr190 = ((!"".equals(var_attrvalue187)) && (!((Object)false).equals(var_attrvalue187)));
        if (var_shoulddisplayattr190) {
            out.write(" srcset");
            {
                boolean var_istrueattr189 = (var_attrvalue187.equals(true));
                if (!var_istrueattr189) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue187));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue191 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath"), "html");
    {
        boolean var_shoulddisplayattr194 = ((!"".equals(var_attrvalue191)) && (!((Object)false).equals(var_attrvalue191)));
        if (var_shoulddisplayattr194) {
            out.write(" srcset");
            {
                boolean var_istrueattr193 = (var_attrvalue191.equals(true));
                if (!var_istrueattr193) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue191));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue195 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoMobileImagePath"), "html");
    {
        boolean var_shoulddisplayattr198 = ((!"".equals(var_attrvalue195)) && (!((Object)false).equals(var_attrvalue195)));
        if (var_shoulddisplayattr198) {
            out.write(" srcset");
            {
                boolean var_istrueattr197 = (var_attrvalue195.equals(true));
                if (!var_istrueattr197) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue195));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1025px)\"");
{
    Object var_attrvalue199 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath"), "html");
    {
        boolean var_shoulddisplayattr202 = ((!"".equals(var_attrvalue199)) && (!((Object)false).equals(var_attrvalue199)));
        if (var_shoulddisplayattr202) {
            out.write(" srcset");
            {
                boolean var_istrueattr201 = (var_attrvalue199.equals(true));
                if (!var_istrueattr201) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue199));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue203 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath"), "html");
    {
        boolean var_shoulddisplayattr206 = ((!"".equals(var_attrvalue203)) && (!((Object)false).equals(var_attrvalue203)));
        if (var_shoulddisplayattr206) {
            out.write(" srcset");
            {
                boolean var_istrueattr205 = (var_attrvalue203.equals(true));
                if (!var_istrueattr205) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue203));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue207 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath"), "html");
    {
        boolean var_shoulddisplayattr210 = ((!"".equals(var_attrvalue207)) && (!((Object)false).equals(var_attrvalue207)));
        if (var_shoulddisplayattr210) {
            out.write(" srcset");
            {
                boolean var_istrueattr209 = (var_attrvalue207.equals(true));
                if (!var_istrueattr209) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue207));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                                <img");
{
    Object var_attrvalue211 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoWebImagePath"), "html");
    {
        boolean var_shoulddisplayattr214 = ((!"".equals(var_attrvalue211)) && (!((Object)false).equals(var_attrvalue211)));
        if (var_shoulddisplayattr214) {
            out.write(" src");
            {
                boolean var_istrueattr213 = (var_attrvalue211.equals(true));
                if (!var_istrueattr213) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue211));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(" itemprop=\"logo\"");
{
    Object var_attrvalue215 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "logoImageAltText");
    {
        Object var_attrcontent216 = renderContext.call("xss", var_attrvalue215, "attribute");
        {
            boolean var_shoulddisplayattr218 = (((null != var_attrcontent216) && (!"".equals(var_attrcontent216))) && ((!"".equals(var_attrvalue215)) && (!((Object)false).equals(var_attrvalue215))));
            if (var_shoulddisplayattr218) {
                out.write(" alt");
                {
                    boolean var_istrueattr217 = (var_attrvalue215.equals(true));
                    if (!var_istrueattr217) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent216));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                            </picture>\r\n                        </a>\r\n                    </div>\r\n                    <!-- Header Logo End-->\r\n\r\n                    <!-- Nav First Level Start -->\r\n                    ");
{
    boolean var_testvariable219 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage")));
    if (var_testvariable219) {
        out.write("\r\n                        <div class=\"navigation-secondary_menu\">\r\n                            ");
        {
            Object var_collectionvar220 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "secondaryMenuNavLevel1");
            {
                long var_size221 = ((var_collectionvar220_list_coerced$ == null ? (var_collectionvar220_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar220)) : var_collectionvar220_list_coerced$).size());
                {
                    boolean var_notempty222 = (var_size221 > 0);
                    if (var_notempty222) {
                        {
                            long var_end225 = var_size221;
                            {
                                boolean var_validstartstepend226 = (((0 < var_size221) && true) && (var_end225 > 0));
                                if (var_validstartstepend226) {
                                    if (var_collectionvar220_list_coerced$ == null) {
                                        var_collectionvar220_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar220);
                                    }
                                    long var_index227 = 0;
                                    for (Object secondarymenunavlevel1item : var_collectionvar220_list_coerced$) {
                                        {
                                            boolean var_traversal229 = (((var_index227 >= 0) && (var_index227 <= var_end225)) && true);
                                            if (var_traversal229) {
                                                out.write("\r\n                                <!-- active class should get added to a tag -->\r\n                                <div");
                                                {
                                                    String var_attrcontent230 = ((("navigation-secondary_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_headermodel, "activeSecondaryMenuPagePath"), renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "menuLinkUrl"))) ? "active" : ""), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "secondaryMenuNavLevel2")) ? "secondary-nav" : ""), "attribute")));
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent230));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                    <a");
                                                {
                                                    Object var_attrvalue231 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "menuLinkUrl");
                                                    {
                                                        Object var_attrcontent232 = renderContext.call("xss", var_attrvalue231, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr234 = (((null != var_attrcontent232) && (!"".equals(var_attrcontent232))) && ((!"".equals(var_attrvalue231)) && (!((Object)false).equals(var_attrvalue231))));
                                                            if (var_shoulddisplayattr234) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr233 = (var_attrvalue231.equals(true));
                                                                    if (!var_istrueattr233) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent232));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue235 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "openinnewtab");
                                                    {
                                                        Object var_attrcontent236 = renderContext.call("xss", var_attrvalue235, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr238 = (((null != var_attrcontent236) && (!"".equals(var_attrcontent236))) && ((!"".equals(var_attrvalue235)) && (!((Object)false).equals(var_attrvalue235))));
                                                            if (var_shoulddisplayattr238) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr237 = (var_attrvalue235.equals(true));
                                                                    if (!var_istrueattr237) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent236));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent239 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "menuLabelText"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent239));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent240 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "menuLinkUrlInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent240));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                        <span>");
                                                {
                                                    Object var_241 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "menuLabelText"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_241));
                                                }
                                                out.write("</span>\r\n                                    </a>\r\n                                    <!-- active class should get added to span tag hving materal-symbol-outlined class -->\r\n                                    <span class=\"icon material-symbols-outlined\">\r\n                                        navigate_next\r\n                                    </span>\r\n                                </div>\r\n                            ");
                                            }
                                        }
                                        var_index227++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar220_list_coerced$ = null;
        }
        out.write("\r\n                        </div>\r\n                    ");
    }
}
out.write("\r\n                    <!-- Nav First Level End -->\r\n\r\n                    <!-- Login Section Start -->\r\n                    <div class=\"navigation-secondary_actions\">\r\n                        ");
{
    boolean var_testvariable242 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage")));
    if (var_testvariable242) {
        out.write("\r\n                            <a");
        {
            Object var_attrvalue243 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelLinkUrl");
            {
                Object var_attrcontent244 = renderContext.call("xss", var_attrvalue243, "uri");
                {
                    boolean var_shoulddisplayattr246 = (((null != var_attrcontent244) && (!"".equals(var_attrcontent244))) && ((!"".equals(var_attrvalue243)) && (!((Object)false).equals(var_attrvalue243))));
                    if (var_shoulddisplayattr246) {
                        out.write(" href");
                        {
                            boolean var_istrueattr245 = (var_attrvalue243.equals(true));
                            if (!var_istrueattr245) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent244));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" id=\"login-btn\" class=\"link_component-link\"");
        {
            Object var_attrvalue247 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "openinnewtab");
            {
                Object var_attrcontent248 = renderContext.call("xss", var_attrvalue247, "attribute");
                {
                    boolean var_shoulddisplayattr250 = (((null != var_attrcontent248) && (!"".equals(var_attrcontent248))) && ((!"".equals(var_attrvalue247)) && (!((Object)false).equals(var_attrvalue247))));
                    if (var_shoulddisplayattr250) {
                        out.write(" target");
                        {
                            boolean var_istrueattr249 = (var_attrvalue247.equals(true));
                            if (!var_istrueattr249) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent248));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-tracking-click-event=\"linkClick\"");
        {
            String var_attrcontent251 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelText"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent251));
            out.write("\"");
        }
        {
            String var_attrcontent252 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelLinkUrlInteractionType"), "attribute"))) + "'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent252));
            out.write("\"");
        }
        out.write(">\r\n                                <span class=\"link_text\">\r\n                                    <div class=\"navigation-secondary_actions-icon\">\r\n                                        <span>");
        {
            Object var_253 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelText"), "text");
            out.write(renderContext.getObjectModel().toString(var_253));
        }
        out.write("</span>\r\n                                        <img src=\"https://d1kndcit1zrj97.cloudfront.net/uploads/white_arrow_4fd026b4ee.svg?w=32&amp;q=75\"/>\r\n                                    </div>\r\n                                </span>\r\n                            </a>\r\n                        ");
    }
}
out.write("\r\n                        <!--  -->\r\n                        ");
{
    Object var_testvariable254 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable254)) {
        out.write("\r\n                            <div id=\"login-btn\" class=\"link_component-link\" target=\"_blank\">\r\n\r\n                                <span class=\"link_text\">\r\n                                    <div class=\"navigation-secondary_actions-icon\">\r\n                                        <span>");
        {
            Object var_255 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginLabelText"), "text");
            out.write(renderContext.getObjectModel().toString(var_255));
        }
        out.write("</span>\r\n                                        <img src=\"https://d1kndcit1zrj97.cloudfront.net/uploads/white_arrow_4fd026b4ee.svg?w=32&amp;q=75\"/>\r\n                                    </div>\r\n                                </span>\r\n                                <div class=\"login__drop-down\">\r\n                                    ");
        {
            Object var_collectionvar256 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "loginData");
            {
                long var_size257 = ((var_collectionvar256_list_coerced$ == null ? (var_collectionvar256_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar256)) : var_collectionvar256_list_coerced$).size());
                {
                    boolean var_notempty258 = (var_size257 > 0);
                    if (var_notempty258) {
                        {
                            long var_end261 = var_size257;
                            {
                                boolean var_validstartstepend262 = (((0 < var_size257) && true) && (var_end261 > 0));
                                if (var_validstartstepend262) {
                                    if (var_collectionvar256_list_coerced$ == null) {
                                        var_collectionvar256_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar256);
                                    }
                                    long var_index263 = 0;
                                    for (Object logindataitems : var_collectionvar256_list_coerced$) {
                                        {
                                            boolean var_traversal265 = (((var_index263 >= 0) && (var_index263 <= var_end261)) && true);
                                            if (var_traversal265) {
                                                out.write("\r\n                                        <div class=\"drop-down__item\">\r\n                                            <a");
                                                {
                                                    Object var_attrvalue266 = renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginURL");
                                                    {
                                                        Object var_attrcontent267 = renderContext.call("xss", var_attrvalue266, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr269 = (((null != var_attrcontent267) && (!"".equals(var_attrcontent267))) && ((!"".equals(var_attrvalue266)) && (!((Object)false).equals(var_attrvalue266))));
                                                            if (var_shoulddisplayattr269) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr268 = (var_attrvalue266.equals(true));
                                                                    if (!var_istrueattr268) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent267));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent270 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginText"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent270));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent271 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginURLInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent271));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_272 = ("\r\n                                             " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(logindataitems, "dropdownLoginText"), "text")));
                                                    out.write(renderContext.getObjectModel().toString(var_272));
                                                }
                                                out.write("</a>\r\n                                        </div>\r\n                                    ");
                                            }
                                        }
                                        var_index263++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar256_list_coerced$ = null;
        }
        out.write("\r\n                                </div>\r\n                            </div>\r\n                        ");
    }
}
out.write("\r\n                        <!--  -->\r\n\r\n                        <div class=\"hambuger-icon\">\r\n                            <span></span>\r\n                            <span></span>\r\n                            <span></span>\r\n                        </div>\r\n                    </div>\r\n                    <!-- Login Section End -->\r\n                </div>\r\n            </div>\r\n            <!-- Secondary Nav End-->\r\n        </div>\r\n\r\n        <!-- Secondar Nav Sub : large3 -->\r\n        <div class=\"header-content\">\r\n            <!-- Secondary Nav Start-->\r\n            <div class=\"navigation_sub\">\r\n                <!-- Primary Nav Mobile Start-->\r\n                <div class=\"mobile-menu\">\r\n                    <div class=\"mobile-main-nav mobile-xl\">\r\n                        <div class=\"mobile-button\">\r\n                            <div class=\"navigation-primary_item navigation-primary_item-dropdown_list mobile-menu-dropdown\">\r\n                                <span class=\"dropdown_holder\">\r\n                                    ");
{
    boolean var_testvariable273 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePageMenuListed")));
    if (var_testvariable273) {
        {
            String var_274 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "defaultMenuLabel"), "text"))) + "\r\n                                    ");
            out.write(renderContext.getObjectModel().toString(var_274));
        }
    }
}
out.write("\r\n                                    ");
{
    Object var_testvariable275 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePageMenuListed");
    if (renderContext.getObjectModel().toBoolean(var_testvariable275)) {
        out.write("\r\n                                        ");
        {
            Object var_collectionvar276 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu");
            {
                long var_size277 = ((var_collectionvar276_list_coerced$ == null ? (var_collectionvar276_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar276)) : var_collectionvar276_list_coerced$).size());
                {
                    boolean var_notempty278 = (var_size277 > 0);
                    if (var_notempty278) {
                        {
                            long var_end281 = var_size277;
                            {
                                boolean var_validstartstepend282 = (((0 < var_size277) && true) && (var_end281 > 0));
                                if (var_validstartstepend282) {
                                    if (var_collectionvar276_list_coerced$ == null) {
                                        var_collectionvar276_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar276);
                                    }
                                    long var_index283 = 0;
                                    for (Object landingmenuitems : var_collectionvar276_list_coerced$) {
                                        {
                                            boolean var_traversal285 = (((var_index283 >= 0) && (var_index283 <= var_end281)) && true);
                                            if (var_traversal285) {
                                                out.write("\r\n                                            ");
                                                {
                                                    boolean var_testvariable286 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePrimaryMenuPagePath")));
                                                    if (var_testvariable286) {
                                                        {
                                                            String var_287 = ("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "text")));
                                                            out.write(renderContext.getObjectModel().toString(var_287));
                                                        }
                                                    }
                                                }
                                                out.write("\r\n                                        ");
                                            }
                                        }
                                        var_index283++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar276_list_coerced$ = null;
        }
        out.write("\r\n                                    ");
    }
}
out.write("\r\n                                </span>\r\n                                <div class=\"dropdown-arrow materal-symbols-outlined active mobile-arrow-down\">\r\n                                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/dropdown-icon.svg\"/>\r\n                                </div>\r\n                                <div class=\"header_list_dropdown\">\r\n                                    <ul>\r\n                                        ");
{
    Object var_collectionvar288 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "landingPageMenu");
    {
        long var_size289 = ((var_collectionvar288_list_coerced$ == null ? (var_collectionvar288_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar288)) : var_collectionvar288_list_coerced$).size());
        {
            boolean var_notempty290 = (var_size289 > 0);
            if (var_notempty290) {
                {
                    long var_end293 = var_size289;
                    {
                        boolean var_validstartstepend294 = (((0 < var_size289) && true) && (var_end293 > 0));
                        if (var_validstartstepend294) {
                            if (var_collectionvar288_list_coerced$ == null) {
                                var_collectionvar288_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar288);
                            }
                            long var_index295 = 0;
                            for (Object landingmenuitems : var_collectionvar288_list_coerced$) {
                                {
                                    boolean var_traversal297 = (((var_index295 >= 0) && (var_index295 <= var_end293)) && true);
                                    if (var_traversal297) {
                                        out.write("\r\n                                            <li");
                                        {
                                            String var_attrcontent298 = (("dropdown-item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "activePrimaryMenuPagePath"))) ? "active" : ""), "attribute"))) + " ");
                                            out.write(" class=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent298));
                                            out.write("\"");
                                        }
                                        out.write(">\r\n                                                <a");
                                        {
                                            Object var_attrvalue299 = renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURL");
                                            {
                                                Object var_attrcontent300 = renderContext.call("xss", var_attrvalue299, "uri");
                                                {
                                                    boolean var_shoulddisplayattr302 = (((null != var_attrcontent300) && (!"".equals(var_attrcontent300))) && ((!"".equals(var_attrvalue299)) && (!((Object)false).equals(var_attrvalue299))));
                                                    if (var_shoulddisplayattr302) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr301 = (var_attrvalue299.equals(true));
                                                            if (!var_istrueattr301) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent300));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue303 = renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingTarget");
                                            {
                                                Object var_attrcontent304 = renderContext.call("xss", var_attrvalue303, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr306 = (((null != var_attrcontent304) && (!"".equals(var_attrcontent304))) && ((!"".equals(var_attrvalue303)) && (!((Object)false).equals(var_attrvalue303))));
                                                    if (var_shoulddisplayattr306) {
                                                        out.write(" target");
                                                        {
                                                            boolean var_istrueattr305 = (var_attrvalue303.equals(true));
                                                            if (!var_istrueattr305) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent304));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(" data-tracking-click-event=\"linkClick\"");
                                        {
                                            String var_attrcontent307 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "attribute"))) + "'}");
                                            out.write(" data-tracking-click-info-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent307));
                                            out.write("\"");
                                        }
                                        {
                                            String var_attrcontent308 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingPageURLInteractionType"), "attribute"))) + "'}}");
                                            out.write(" data-tracking-web-interaction-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent308));
                                            out.write("\"");
                                        }
                                        out.write(">");
                                        {
                                            String var_309 = ("\r\n                                                     " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(landingmenuitems, "landingMenuTitle"), "text")));
                                            out.write(renderContext.getObjectModel().toString(var_309));
                                        }
                                        out.write("</a>\r\n                                            </li>\r\n                                        ");
                                    }
                                }
                                var_index295++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar288_list_coerced$ = null;
}
out.write("\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        ");
{
    boolean var_testvariable310 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_headermodel, "enableHomePage")));
    if (var_testvariable310) {
        out.write("\r\n                            <div class=\"discover_dropdown_btn\">\r\n                                <div class=\"discover_label\">\r\n                                    <span class=\"link_text\"><span>");
        {
            Object var_311 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_headermodel, "discoverLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_311));
        }
        out.write("</span></span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"discover_category\">\r\n                                ");
        {
            Object var_collectionvar312 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "discoverMenu");
            {
                long var_size313 = ((var_collectionvar312_list_coerced$ == null ? (var_collectionvar312_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar312)) : var_collectionvar312_list_coerced$).size());
                {
                    boolean var_notempty314 = (var_size313 > 0);
                    if (var_notempty314) {
                        {
                            long var_end317 = var_size313;
                            {
                                boolean var_validstartstepend318 = (((0 < var_size313) && true) && (var_end317 > 0));
                                if (var_validstartstepend318) {
                                    if (var_collectionvar312_list_coerced$ == null) {
                                        var_collectionvar312_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar312);
                                    }
                                    long var_index319 = 0;
                                    for (Object discovermenuitems : var_collectionvar312_list_coerced$) {
                                        {
                                            boolean var_traversal321 = (((var_index319 >= 0) && (var_index319 <= var_end317)) && true);
                                            if (var_traversal321) {
                                                out.write("\r\n                                    <div");
                                                {
                                                    String var_attrcontent322 = ("navigation-primary_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURL"), renderContext.getObjectModel().resolveProperty(_global_headermodel, "currentActivePage"))) ? "show-active-arrow" : ""), "attribute")));
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent322));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                        <a");
                                                {
                                                    Object var_attrvalue323 = renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURL");
                                                    {
                                                        Object var_attrcontent324 = renderContext.call("xss", var_attrvalue323, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr326 = (((null != var_attrcontent324) && (!"".equals(var_attrcontent324))) && ((!"".equals(var_attrvalue323)) && (!((Object)false).equals(var_attrvalue323))));
                                                            if (var_shoulddisplayattr326) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr325 = (var_attrvalue323.equals(true));
                                                                    if (!var_istrueattr325) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent324));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue327 = renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverTarget");
                                                    {
                                                        Object var_attrcontent328 = renderContext.call("xss", var_attrvalue327, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr330 = (((null != var_attrcontent328) && (!"".equals(var_attrcontent328))) && ((!"".equals(var_attrvalue327)) && (!((Object)false).equals(var_attrvalue327))));
                                                            if (var_shoulddisplayattr330) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr329 = (var_attrvalue327.equals(true));
                                                                    if (!var_istrueattr329) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent328));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent331 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuLabel"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent331));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent332 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuPageURLInteractionType"), "attribute"))) + "'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent332));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                                            <span class=\"link_text\">");
                                                {
                                                    Object var_333 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(discovermenuitems, "discoverMenuLabel"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_333));
                                                }
                                                out.write("</span>\r\n                                        </a>\r\n                                    </div>\r\n                                ");
                                            }
                                        }
                                        var_index319++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar312_list_coerced$ = null;
        }
        out.write("\r\n                            </div>\r\n                            \r\n                    ");
    }
}
out.write("</div>\r\n                    <div class=\"mobile-menu-items\"></div>\r\n                </div>\r\n                <!-- Primary Nav Mobile End-->\r\n\r\n                <div class=\"navigation_sub_wraper\">\r\n                    <div class=\"navigation_sub_inner\">\r\n                        <div class=\"mobile-xl\">\r\n                            <div class=\"back_menu\">\r\n                                <span class=\"material-symbols-outlined active\">\r\n                                    navigate_before\r\n                                </span>\r\n                                <span class=\"back-menu-text\"></span>\r\n                            </div>\r\n                        </div>\r\n\r\n                        ");
{
    Object var_collectionvar334 = renderContext.getObjectModel().resolveProperty(_global_headermodel, "secondaryMenuNavLevel1");
    {
        long var_size335 = ((var_collectionvar334_list_coerced$ == null ? (var_collectionvar334_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar334)) : var_collectionvar334_list_coerced$).size());
        {
            boolean var_notempty336 = (var_size335 > 0);
            if (var_notempty336) {
                {
                    long var_end339 = var_size335;
                    {
                        boolean var_validstartstepend340 = (((0 < var_size335) && true) && (var_end339 > 0));
                        if (var_validstartstepend340) {
                            if (var_collectionvar334_list_coerced$ == null) {
                                var_collectionvar334_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar334);
                            }
                            long var_index341 = 0;
                            for (Object secondarymenunavlevel1item : var_collectionvar334_list_coerced$) {
                                {
                                    boolean var_traversal343 = (((var_index341 >= 0) && (var_index341 <= var_end339)) && true);
                                    if (var_traversal343) {
                                        out.write("\r\n                            ");
                                        {
                                            Object var_testvariable344 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "secondaryMenuNavLevel2");
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable344)) {
                                                out.write("\r\n                                <div class=\"navigation_sub_item\">\r\n                                    <div class=\"navigation_tab_title\">\r\n                                        ");
                                                {
                                                    Object var_collectionvar345 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "secondaryMenuNavLevel2");
                                                    {
                                                        long var_size346 = ((var_collectionvar345_list_coerced$ == null ? (var_collectionvar345_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar345)) : var_collectionvar345_list_coerced$).size());
                                                        {
                                                            boolean var_notempty347 = (var_size346 > 0);
                                                            if (var_notempty347) {
                                                                {
                                                                    long var_end350 = var_size346;
                                                                    {
                                                                        boolean var_validstartstepend351 = (((0 < var_size346) && true) && (var_end350 > 0));
                                                                        if (var_validstartstepend351) {
                                                                            if (var_collectionvar345_list_coerced$ == null) {
                                                                                var_collectionvar345_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar345);
                                                                            }
                                                                            long var_index352 = 0;
                                                                            for (Object secondarymenunavlevel2item : var_collectionvar345_list_coerced$) {
                                                                                {
                                                                                    boolean secondarymenunavlevel2itemlist_field$_first = (var_index352 == 0);
                                                                                    {
                                                                                        boolean var_traversal354 = (((var_index352 >= 0) && (var_index352 <= var_end350)) && true);
                                                                                        if (var_traversal354) {
                                                                                            out.write("\r\n                                            <div");
                                                                                            {
                                                                                                String var_attrcontent355 = ("tab_item " + renderContext.getObjectModel().toString(renderContext.call("xss", (secondarymenunavlevel2itemlist_field$_first ? "active" : ""), "attribute")));
                                                                                                out.write(" class=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent355));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            out.write(">\r\n                                                <a");
                                                                                            {
                                                                                                Object var_attrvalue356 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subTabMenuLinkUrl");
                                                                                                {
                                                                                                    Object var_attrcontent357 = renderContext.call("xss", var_attrvalue356, "uri");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr359 = (((null != var_attrcontent357) && (!"".equals(var_attrcontent357))) && ((!"".equals(var_attrvalue356)) && (!((Object)false).equals(var_attrvalue356))));
                                                                                                        if (var_shoulddisplayattr359) {
                                                                                                            out.write(" href");
                                                                                                            {
                                                                                                                boolean var_istrueattr358 = (var_attrvalue356.equals(true));
                                                                                                                if (!var_istrueattr358) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent357));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            {
                                                                                                Object var_attrvalue360 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "openinnewtab");
                                                                                                {
                                                                                                    Object var_attrcontent361 = renderContext.call("xss", var_attrvalue360, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr363 = (((null != var_attrcontent361) && (!"".equals(var_attrcontent361))) && ((!"".equals(var_attrvalue360)) && (!((Object)false).equals(var_attrvalue360))));
                                                                                                        if (var_shoulddisplayattr363) {
                                                                                                            out.write(" target");
                                                                                                            {
                                                                                                                boolean var_istrueattr362 = (var_attrvalue360.equals(true));
                                                                                                                if (!var_istrueattr362) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent361));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                            {
                                                                                                String var_attrcontent364 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subTabMenuLabelText"), "attribute"))) + "'}");
                                                                                                out.write(" data-tracking-click-info-value=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent364));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            {
                                                                                                String var_attrcontent365 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subTabMenuLinkUrlInteractionType"), "attribute"))) + "'}}");
                                                                                                out.write(" data-tracking-web-interaction-value=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent365));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            out.write(">                                                \r\n                                                    <span>");
                                                                                            {
                                                                                                Object var_366 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subTabMenuLabelText"), "text");
                                                                                                out.write(renderContext.getObjectModel().toString(var_366));
                                                                                            }
                                                                                            out.write("</span>\r\n                                                </a>\r\n                                                <span class=\"material-symbols-outlined\">arrow_forward</span>\r\n                                            </div>\r\n                                        ");
                                                                                        }
                                                                                    }
                                                                                }
                                                                                var_index352++;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    var_collectionvar345_list_coerced$ = null;
                                                }
                                                out.write("\r\n                                    </div>\r\n                                    <div class=\"navigation_tab_content\">\r\n                                        ");
                                                {
                                                    Object var_collectionvar367 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel1item, "secondaryMenuNavLevel2");
                                                    {
                                                        long var_size368 = ((var_collectionvar367_list_coerced$ == null ? (var_collectionvar367_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar367)) : var_collectionvar367_list_coerced$).size());
                                                        {
                                                            boolean var_notempty369 = (var_size368 > 0);
                                                            if (var_notempty369) {
                                                                {
                                                                    long var_end372 = var_size368;
                                                                    {
                                                                        boolean var_validstartstepend373 = (((0 < var_size368) && true) && (var_end372 > 0));
                                                                        if (var_validstartstepend373) {
                                                                            if (var_collectionvar367_list_coerced$ == null) {
                                                                                var_collectionvar367_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar367);
                                                                            }
                                                                            long var_index374 = 0;
                                                                            for (Object secondarymenunavlevel2item : var_collectionvar367_list_coerced$) {
                                                                                {
                                                                                    boolean var_traversal376 = (((var_index374 >= 0) && (var_index374 <= var_end372)) && true);
                                                                                    if (var_traversal376) {
                                                                                        out.write("\r\n                                            <div class=\"tab_content\">\r\n                                                <div class=\"navigation_sub_list\">\r\n                                                    ");
                                                                                        {
                                                                                            Object var_collectionvar377 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "secondaryMenuNavLevel3");
                                                                                            {
                                                                                                long var_size378 = ((var_collectionvar377_list_coerced$ == null ? (var_collectionvar377_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar377)) : var_collectionvar377_list_coerced$).size());
                                                                                                {
                                                                                                    boolean var_notempty379 = (var_size378 > 0);
                                                                                                    if (var_notempty379) {
                                                                                                        {
                                                                                                            long var_end382 = var_size378;
                                                                                                            {
                                                                                                                boolean var_validstartstepend383 = (((0 < var_size378) && true) && (var_end382 > 0));
                                                                                                                if (var_validstartstepend383) {
                                                                                                                    if (var_collectionvar377_list_coerced$ == null) {
                                                                                                                        var_collectionvar377_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar377);
                                                                                                                    }
                                                                                                                    long var_index384 = 0;
                                                                                                                    for (Object secondarymenunavlevel3item : var_collectionvar377_list_coerced$) {
                                                                                                                        {
                                                                                                                            long secondarymenunavlevel3itemlist_field$_index = var_index384;
                                                                                                                            {
                                                                                                                                boolean var_traversal386 = (((var_index384 >= 0) && (var_index384 <= var_end382)) && true);
                                                                                                                                if (var_traversal386) {
                                                                                                                                    out.write("\r\n                                                        <a");
                                                                                                                                    {
                                                                                                                                        String var_attrcontent387 = ("navigation_sub_tab_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((secondarymenunavlevel3itemlist_field$_index > 3) ? "default-hide" : ""), "attribute")));
                                                                                                                                        out.write(" class=\"");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent387));
                                                                                                                                        out.write("\"");
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue388 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuLinkUrl");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent389 = renderContext.call("xss", var_attrvalue388, "uri");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr391 = (((null != var_attrcontent389) && (!"".equals(var_attrcontent389))) && ((!"".equals(var_attrvalue388)) && (!((Object)false).equals(var_attrvalue388))));
                                                                                                                                                if (var_shoulddisplayattr391) {
                                                                                                                                                    out.write(" href");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr390 = (var_attrvalue388.equals(true));
                                                                                                                                                        if (!var_istrueattr390) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent389));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue392 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "openinnewtab");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent393 = renderContext.call("xss", var_attrvalue392, "attribute");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr395 = (((null != var_attrcontent393) && (!"".equals(var_attrcontent393))) && ((!"".equals(var_attrvalue392)) && (!((Object)false).equals(var_attrvalue392))));
                                                                                                                                                if (var_shoulddisplayattr395) {
                                                                                                                                                    out.write(" target");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr394 = (var_attrvalue392.equals(true));
                                                                                                                                                        if (!var_istrueattr394) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent393));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                                                                    {
                                                                                                                                        String var_attrcontent396 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuLabelText"), "attribute"))) + "'}");
                                                                                                                                        out.write(" data-tracking-click-info-value=\"");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent396));
                                                                                                                                        out.write("\"");
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        String var_attrcontent397 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuLinkUrlInteractionType"), "attribute"))) + "'}}");
                                                                                                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent397));
                                                                                                                                        out.write("\"");
                                                                                                                                    }
                                                                                                                                    out.write(">                                                            \r\n                                                            ");
                                                                                                                                    {
                                                                                                                                        Object var_testvariable398 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuIconImage");
                                                                                                                                        if (renderContext.getObjectModel().toBoolean(var_testvariable398)) {
                                                                                                                                            out.write(" \r\n                                                            <div class=\"navigation_sub_item-image\">                                                                                                                         \r\n                                                                <picture>                            \t\t\t\t\t\t\t\t\t\t\r\n                            \t\t\t\t\t    <source media=\"(max-width: 360px)\"");
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue399 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuIconMobileImagePath"), "html");
                                                                                                                                                {
                                                                                                                                                    boolean var_shoulddisplayattr402 = ((!"".equals(var_attrvalue399)) && (!((Object)false).equals(var_attrvalue399)));
                                                                                                                                                    if (var_shoulddisplayattr402) {
                                                                                                                                                        out.write(" srcset");
                                                                                                                                                        {
                                                                                                                                                            boolean var_istrueattr401 = (var_attrvalue399.equals(true));
                                                                                                                                                            if (!var_istrueattr401) {
                                                                                                                                                                out.write("=\"");
                                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrvalue399));
                                                                                                                                                                out.write("\"");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("/>\r\n\t\t\t\t\t\t\t\t    <source media=\"(max-width: 576px)\"");
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue403 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuIconMobileImagePath"), "html");
                                                                                                                                                {
                                                                                                                                                    boolean var_shoulddisplayattr406 = ((!"".equals(var_attrvalue403)) && (!((Object)false).equals(var_attrvalue403)));
                                                                                                                                                    if (var_shoulddisplayattr406) {
                                                                                                                                                        out.write(" srcset");
                                                                                                                                                        {
                                                                                                                                                            boolean var_istrueattr405 = (var_attrvalue403.equals(true));
                                                                                                                                                            if (!var_istrueattr405) {
                                                                                                                                                                out.write("=\"");
                                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrvalue403));
                                                                                                                                                                out.write("\"");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("/>                            \t\t\t\t\t\t\t\t\t\t                                     \r\n\t\t\t\t\t\t\t\t    <source media=\"(min-width: 1080px)\"");
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue407 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuWebIconImagePath"), "html");
                                                                                                                                                {
                                                                                                                                                    boolean var_shoulddisplayattr410 = ((!"".equals(var_attrvalue407)) && (!((Object)false).equals(var_attrvalue407)));
                                                                                                                                                    if (var_shoulddisplayattr410) {
                                                                                                                                                        out.write(" srcset");
                                                                                                                                                        {
                                                                                                                                                            boolean var_istrueattr409 = (var_attrvalue407.equals(true));
                                                                                                                                                            if (!var_istrueattr409) {
                                                                                                                                                                out.write("=\"");
                                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrvalue407));
                                                                                                                                                                out.write("\"");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("/>\r\n\t\t\t\t\t\t\t\t    <source media=\"(min-width: 1200px)\"");
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue411 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuWebIconImagePath"), "html");
                                                                                                                                                {
                                                                                                                                                    boolean var_shoulddisplayattr414 = ((!"".equals(var_attrvalue411)) && (!((Object)false).equals(var_attrvalue411)));
                                                                                                                                                    if (var_shoulddisplayattr414) {
                                                                                                                                                        out.write(" srcset");
                                                                                                                                                        {
                                                                                                                                                            boolean var_istrueattr413 = (var_attrvalue411.equals(true));
                                                                                                                                                            if (!var_istrueattr413) {
                                                                                                                                                                out.write("=\"");
                                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrvalue411));
                                                                                                                                                                out.write("\"");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("/>\r\n                                   \t\t\t\t    <img");
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue415 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuWebIconImagePath"), "html");
                                                                                                                                                {
                                                                                                                                                    boolean var_shoulddisplayattr418 = ((!"".equals(var_attrvalue415)) && (!((Object)false).equals(var_attrvalue415)));
                                                                                                                                                    if (var_shoulddisplayattr418) {
                                                                                                                                                        out.write(" src");
                                                                                                                                                        {
                                                                                                                                                            boolean var_istrueattr417 = (var_attrvalue415.equals(true));
                                                                                                                                                            if (!var_istrueattr417) {
                                                                                                                                                                out.write("=\"");
                                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrvalue415));
                                                                                                                                                                out.write("\"");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            {
                                                                                                                                                Object var_attrvalue419 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "imageAltText");
                                                                                                                                                {
                                                                                                                                                    Object var_attrcontent420 = renderContext.call("xss", var_attrvalue419, "attribute");
                                                                                                                                                    {
                                                                                                                                                        boolean var_shoulddisplayattr422 = (((null != var_attrcontent420) && (!"".equals(var_attrcontent420))) && ((!"".equals(var_attrvalue419)) && (!((Object)false).equals(var_attrvalue419))));
                                                                                                                                                        if (var_shoulddisplayattr422) {
                                                                                                                                                            out.write(" alt");
                                                                                                                                                            {
                                                                                                                                                                boolean var_istrueattr421 = (var_attrvalue419.equals(true));
                                                                                                                                                                if (!var_istrueattr421) {
                                                                                                                                                                    out.write("=\"");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent420));
                                                                                                                                                                    out.write("\"");
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("/>\r\n                            \t\t\t\t\t</picture>                            \t\t\t\t\t\r\n                                                            </div>\r\n                                                            ");
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    out.write("\r\n                                                            <div class=\"navigation_sub_item-content\">\r\n                                                                <p class=\"navigation_sub_item-title\">");
                                                                                                                                    {
                                                                                                                                        String var_423 = (("\r\n                                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subMenuLabelText"), "text"))) + "\r\n                                                                ");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_423));
                                                                                                                                    }
                                                                                                                                    out.write("</p>\r\n                                                                <span class=\"navigation_sub_item-des\">");
                                                                                                                                    {
                                                                                                                                        Object var_424 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel3item, "subTabMenuLabelDesc"), "text");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_424));
                                                                                                                                    }
                                                                                                                                    out.write("</span>\r\n                                                            </div>\r\n                                                        </a>\r\n                                                    ");
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        var_index384++;
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            var_collectionvar377_list_coerced$ = null;
                                                                                        }
                                                                                        out.write("\r\n                                                </div>\r\n                                                ");
                                                                                        {
                                                                                            Object var_testvariable425 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "viewAllSubTab")) ? renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "viewLessSubTab") : renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "viewAllSubTab")));
                                                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable425)) {
                                                                                                out.write("\r\n                                                    <div class=\"navigation_sub_item-viewall\" id=\"viewAllButton\">\r\n                                                        <a class=\"link_component-link collapsed\">");
                                                                                                {
                                                                                                    Object var_426 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "viewAllSubTab"), "text");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_426));
                                                                                                }
                                                                                                out.write("<span></span></a>\r\n                                                        <a class=\"link_component-link expanded\">");
                                                                                                {
                                                                                                    Object var_427 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "viewLessSubTab"), "text");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_427));
                                                                                                }
                                                                                                out.write("<span></span></a>\r\n                                                        <span class=\"material-symbols-outlined\">expand_more</span>\r\n                                                    </div>\r\n                                                ");
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n\r\n                                                <div class=\"navigation_tab_bottom\">\r\n                                                    ");
                                                                                        {
                                                                                            Object var_testvariable428 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkIcon");
                                                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable428)) {
                                                                                                out.write("\r\n                                                        <a");
                                                                                                {
                                                                                                    Object var_attrvalue429 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkUrl");
                                                                                                    {
                                                                                                        Object var_attrcontent430 = renderContext.call("xss", var_attrvalue429, "uri");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr432 = (((null != var_attrcontent430) && (!"".equals(var_attrcontent430))) && ((!"".equals(var_attrvalue429)) && (!((Object)false).equals(var_attrvalue429))));
                                                                                                            if (var_shoulddisplayattr432) {
                                                                                                                out.write(" href");
                                                                                                                {
                                                                                                                    boolean var_istrueattr431 = (var_attrvalue429.equals(true));
                                                                                                                    if (!var_istrueattr431) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent430));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                {
                                                                                                    Object var_attrvalue433 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuTargetLeft");
                                                                                                    {
                                                                                                        Object var_attrcontent434 = renderContext.call("xss", var_attrvalue433, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr436 = (((null != var_attrcontent434) && (!"".equals(var_attrcontent434))) && ((!"".equals(var_attrvalue433)) && (!((Object)false).equals(var_attrvalue433))));
                                                                                                            if (var_shoulddisplayattr436) {
                                                                                                                out.write(" target");
                                                                                                                {
                                                                                                                    boolean var_istrueattr435 = (var_attrvalue433.equals(true));
                                                                                                                    if (!var_istrueattr435) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent434));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
                                                                                                {
                                                                                                    String var_attrcontent437 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkText"), "attribute"))) + "'}");
                                                                                                    out.write(" data-tracking-click-info-value=\"");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent437));
                                                                                                    out.write("\"");
                                                                                                }
                                                                                                {
                                                                                                    String var_attrcontent438 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkUrllInteractionType"), "attribute"))) + "'}}");
                                                                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent438));
                                                                                                    out.write("\"");
                                                                                                }
                                                                                                out.write(">\r\n                                                            <picture>                            \t\t\t\t\t\t\t\t\t\t\r\n                            \t\t\t\t\t<source media=\"(max-width: 360px)\"");
                                                                                                {
                                                                                                    Object var_attrvalue439 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkMobileIconPath");
                                                                                                    {
                                                                                                        Object var_attrcontent440 = renderContext.call("xss", var_attrvalue439, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr442 = (((null != var_attrcontent440) && (!"".equals(var_attrcontent440))) && ((!"".equals(var_attrvalue439)) && (!((Object)false).equals(var_attrvalue439))));
                                                                                                            if (var_shoulddisplayattr442) {
                                                                                                                out.write(" srcset");
                                                                                                                {
                                                                                                                    boolean var_istrueattr441 = (var_attrvalue439.equals(true));
                                                                                                                    if (!var_istrueattr441) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent440));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write("/>\r\n\t\t\t\t\t\t\t\t<source media=\"(max-width: 576px)\"");
                                                                                                {
                                                                                                    Object var_attrvalue443 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkMobileIconPath");
                                                                                                    {
                                                                                                        Object var_attrcontent444 = renderContext.call("xss", var_attrvalue443, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr446 = (((null != var_attrcontent444) && (!"".equals(var_attrcontent444))) && ((!"".equals(var_attrvalue443)) && (!((Object)false).equals(var_attrvalue443))));
                                                                                                            if (var_shoulddisplayattr446) {
                                                                                                                out.write(" srcset");
                                                                                                                {
                                                                                                                    boolean var_istrueattr445 = (var_attrvalue443.equals(true));
                                                                                                                    if (!var_istrueattr445) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent444));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write("/>                            \t\t\t\t\t\t\t\t\t\t                                     \r\n\t\t\t\t\t\t\t\t<source media=\"(min-width: 1080px)\"");
                                                                                                {
                                                                                                    Object var_attrvalue447 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkWebIconPath");
                                                                                                    {
                                                                                                        Object var_attrcontent448 = renderContext.call("xss", var_attrvalue447, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr450 = (((null != var_attrcontent448) && (!"".equals(var_attrcontent448))) && ((!"".equals(var_attrvalue447)) && (!((Object)false).equals(var_attrvalue447))));
                                                                                                            if (var_shoulddisplayattr450) {
                                                                                                                out.write(" srcset");
                                                                                                                {
                                                                                                                    boolean var_istrueattr449 = (var_attrvalue447.equals(true));
                                                                                                                    if (!var_istrueattr449) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent448));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write("/>\r\n\t\t\t\t\t\t\t\t<source media=\"(min-width: 1200px)\"");
                                                                                                {
                                                                                                    Object var_attrvalue451 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkWebIconPath");
                                                                                                    {
                                                                                                        Object var_attrcontent452 = renderContext.call("xss", var_attrvalue451, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr454 = (((null != var_attrcontent452) && (!"".equals(var_attrcontent452))) && ((!"".equals(var_attrvalue451)) && (!((Object)false).equals(var_attrvalue451))));
                                                                                                            if (var_shoulddisplayattr454) {
                                                                                                                out.write(" srcset");
                                                                                                                {
                                                                                                                    boolean var_istrueattr453 = (var_attrvalue451.equals(true));
                                                                                                                    if (!var_istrueattr453) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent452));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write("/>                                   \t\t\t\t\t\t\t\t\t\r\n                                                                <img");
                                                                                                {
                                                                                                    Object var_attrvalue455 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkWebIconPath");
                                                                                                    {
                                                                                                        Object var_attrcontent456 = renderContext.call("xss", var_attrvalue455, "uri");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr458 = (((null != var_attrcontent456) && (!"".equals(var_attrcontent456))) && ((!"".equals(var_attrvalue455)) && (!((Object)false).equals(var_attrvalue455))));
                                                                                                            if (var_shoulddisplayattr458) {
                                                                                                                out.write(" src");
                                                                                                                {
                                                                                                                    boolean var_istrueattr457 = (var_attrvalue455.equals(true));
                                                                                                                    if (!var_istrueattr457) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent456));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write("/>\r\n                            \t\t\t\t    </picture>\r\n                                                            <span>");
                                                                                                {
                                                                                                    Object var_459 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkText"), "text");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_459));
                                                                                                }
                                                                                                out.write("</span>\r\n                                                        </a>\r\n                                                    ");
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n                                                        <a");
                                                                                        {
                                                                                            Object var_attrvalue460 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkUrlRight");
                                                                                            {
                                                                                                Object var_attrcontent461 = renderContext.call("xss", var_attrvalue460, "uri");
                                                                                                {
                                                                                                    boolean var_shoulddisplayattr463 = (((null != var_attrcontent461) && (!"".equals(var_attrcontent461))) && ((!"".equals(var_attrvalue460)) && (!((Object)false).equals(var_attrvalue460))));
                                                                                                    if (var_shoulddisplayattr463) {
                                                                                                        out.write(" href");
                                                                                                        {
                                                                                                            boolean var_istrueattr462 = (var_attrvalue460.equals(true));
                                                                                                            if (!var_istrueattr462) {
                                                                                                                out.write("=\"");
                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent461));
                                                                                                                out.write("\"");
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        {
                                                                                            Object var_attrvalue464 = renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuTargetRight");
                                                                                            {
                                                                                                Object var_attrcontent465 = renderContext.call("xss", var_attrvalue464, "attribute");
                                                                                                {
                                                                                                    boolean var_shoulddisplayattr467 = (((null != var_attrcontent465) && (!"".equals(var_attrcontent465))) && ((!"".equals(var_attrvalue464)) && (!((Object)false).equals(var_attrvalue464))));
                                                                                                    if (var_shoulddisplayattr467) {
                                                                                                        out.write(" target");
                                                                                                        {
                                                                                                            boolean var_istrueattr466 = (var_attrvalue464.equals(true));
                                                                                                            if (!var_istrueattr466) {
                                                                                                                out.write("=\"");
                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent465));
                                                                                                                out.write("\"");
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        out.write(" class=\"link_component-link\" data-tracking-click-event=\"linkClick\"");
                                                                                        {
                                                                                            String var_attrcontent468 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkTextRight"), "attribute"))) + "'}");
                                                                                            out.write(" data-tracking-click-info-value=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent468));
                                                                                            out.write("\"");
                                                                                        }
                                                                                        {
                                                                                            String var_attrcontent469 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkUrlRightInteractionType"), "attribute"))) + "'}}");
                                                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent469));
                                                                                            out.write("\"");
                                                                                        }
                                                                                        out.write(">\r\n                                                            <span class=\"link-text\">");
                                                                                        {
                                                                                            Object var_470 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(secondarymenunavlevel2item, "subMenuBottomLinkTextRight"), "text");
                                                                                            out.write(renderContext.getObjectModel().toString(var_470));
                                                                                        }
                                                                                        out.write("</span>\r\n                                                            <span class=\"material-symbols-outlined\"><img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/right-detail-arrow.svg\" alt=\"\"/></span>\r\n                                                        </a>\r\n                                                </div>\r\n                                            </div>\r\n                                        ");
                                                                                    }
                                                                                }
                                                                                var_index374++;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    var_collectionvar367_list_coerced$ = null;
                                                }
                                                out.write("\r\n                                    </div>\r\n                                </div>\r\n                            ");
                                            }
                                        }
                                        out.write("\r\n                        ");
                                    }
                                }
                                var_index341++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar334_list_coerced$ = null;
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- Secondary Nav End-->\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

