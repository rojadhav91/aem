/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.fixingrate;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class fixingrate__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_fixingratemodel = null;
Object _dynamic_currentpage = bindings.get("currentpage");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_fixingratemodel = renderContext.call("use", com.techcombank.core.models.FixingRateModel.class.getName(), obj());
out.write("\r\n    <section class=\"container fixing-rate\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-url");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n        <div class=\"exchange-rate__header\">\r\n            <div class=\"title-cmp\">\r\n                <div class=\"title-cmp__title\">");
{
    Object var_9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "fixingRateTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_9));
}
out.write("</div>\r\n            </div>\r\n            <div class=\"exchange-rate__header-select analytics-active-link\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent10 = (("{'calculatorName':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "', 'calculatorField':'TimeStamp' }");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
    out.write("\"");
}
out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Calculators','type': 'other'}}\">\r\n                <div class=\"header-select__data date-time-wrapper__input-extra\">\r\n                    <div class=\"header-select__data-input calendar_real_estate\">\r\n                        <div class=\"data-input__prefix\">\r\n                            <div class=\"data-input__calendar-icon\">\r\n                                <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
        {
            boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
            if (var_shoulddisplayattr14) {
                out.write(" srcset");
                {
                    boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                    if (!var_istrueattr13) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
        {
            boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
            if (var_shoulddisplayattr18) {
                out.write(" srcset");
                {
                    boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                    if (!var_istrueattr17) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
        {
            boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
            if (var_shoulddisplayattr22) {
                out.write(" srcset");
                {
                    boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                    if (!var_istrueattr21) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
        {
            boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
            if (var_shoulddisplayattr26) {
                out.write(" srcset");
                {
                    boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                    if (!var_istrueattr25) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <img");
{
    Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconAlt");
    {
        Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
        {
            boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
            if (var_shoulddisplayattr30) {
                out.write(" alt");
                {
                    boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                    if (!var_istrueattr29) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "uri");
        {
            boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
            if (var_shoulddisplayattr34) {
                out.write(" src");
                {
                    boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                    if (!var_istrueattr33) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                </picture>\r\n                            </div>\r\n                            <p>");
{
    Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "dayLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_35));
}
out.write("</p>\r\n                        </div>\r\n                        <div class=\"data-input__suffix\">\r\n                            <p class=\"calendar__input-field\"></p>\r\n                            <span class=\"data-input__arrow-icon\">\r\n                  <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\"/>\r\n                </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"calendar-popup\">\r\n                        <div class=\"month-line\">\r\n                            <span class=\"month-data\"></span>\r\n                            <div>\r\n                                <img class=\"prev-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-left-icon.svg\"/>\r\n                                <img class=\"next-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-right-icon.svg\"/>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"date-line\">\r\n                        </div>\r\n                        <div class=\"day-tbl\">\r\n                            <div class=\"week line1\"></div>\r\n                            <div class=\"week line2\"></div>\r\n                            <div class=\"week line3\"></div>\r\n                            <div class=\"week line4\"></div>\r\n                            <div class=\"week line5\"></div>\r\n                            <div class=\"week line6\"></div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"header-select__data time-select\"");
{
    Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "timeIconAlt");
    {
        Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
        {
            boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
            if (var_shoulddisplayattr39) {
                out.write(" icon-alt");
                {
                    boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                    if (!var_istrueattr38) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "timeIconMobileImagePath");
    {
        Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
        {
            boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
            if (var_shoulddisplayattr43) {
                out.write(" icon-mobile-path");
                {
                    boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                    if (!var_istrueattr42) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "timeIconWebImagePath");
    {
        Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
        {
            boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
            if (var_shoulddisplayattr47) {
                out.write(" icon-web-path");
                {
                    boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                    if (!var_istrueattr46) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "timeLabel");
    {
        Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
        {
            boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
            if (var_shoulddisplayattr51) {
                out.write(" title");
                {
                    boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                    if (!var_istrueattr50) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"table-content-container exchange-rate__table hidden\">\r\n            <div class=\"exchange-rate__table-content\">\r\n                <div class=\"exchange-rate__table-records table-header\">\r\n                    <div class=\"table__first-column first-row first-column\">\r\n                        <p>");
{
    Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "currencyLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_52));
}
out.write("</p>\r\n                    </div>\r\n                    <div class=\"table-records__data\">\r\n                        <div class=\"table-records__data-content first-row\">\r\n                            <p>");
{
    Object var_53 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "colOneHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_53));
}
out.write("</p>\r\n                        </div>\r\n                        <div class=\"table-records__data-content first-row last-column\">\r\n                            <p>");
{
    Object var_54 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "colTwoHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_54));
}
out.write("</p>\r\n                        </div>\r\n                        <div class=\"table-records__data-content\">\r\n                            <p>");
{
    Object var_55 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "colSubHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_55));
}
out.write("</p>\r\n                        </div>\r\n                        <div class=\"table-records__data-content last-column\">\r\n                            <p>");
{
    Object var_56 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_fixingratemodel, "colSubHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_56));
}
out.write("</p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"exchange-rate-table-content\">\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

