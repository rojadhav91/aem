/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.eventlist;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class eventlist__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.EventListingModel.class.getName(), obj());
out.write("\r\n\r\n<section class=\"calendar-event\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_model, "eventsCFRootPath");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-cfPath");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <!-- card-product-filter.html -->\r\n    <div class=\"container mobile-not-show\" data-tracking-click-event=\"articleFilter\" data-tracking-click-info-value=\"{'articleFilter':''}\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Calendar events','type': 'other'}}\">\r\n        <div class=\"card-multiple-select\" data-wrap=\"false\"");
{
    Object var_attrvalue9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "eventTypeLabel"), "html");
    {
        boolean var_shoulddisplayattr12 = ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9)));
        if (var_shoulddisplayattr12) {
            out.write(" data-title");
            {
                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                if (!var_istrueattr11) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue9));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue13 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "tags"), "html");
    {
        boolean var_shoulddisplayattr16 = ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13)));
        if (var_shoulddisplayattr16) {
            out.write(" data-options");
            {
                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                if (!var_istrueattr15) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue13));
                    out.write("'");
                }
            }
        }
    }
}
out.write(">\r\n        </div>\r\n    </div>\r\n    <!-- news-filter-list.html -->\r\n    <div class=\"container event-filter mobile-shadow section__margin-small\"");
{
    Object var_attrvalue17 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabel"), "html");
    {
        boolean var_shoulddisplayattr20 = ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17)));
        if (var_shoulddisplayattr20) {
            out.write(" data-viewmore");
            {
                boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                if (!var_istrueattr19) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue17));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(" data-register=\"Register\">\r\n        <div class=\"content-wrapper news-filter__wrapper\">\r\n            <!-- Side bar Offer filter -->\r\n            <div class=\"news_filter-group\">\r\n                <div class=\"news_filter__header mobile-only\">");
{
    String var_21 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "filterLabel"), "html"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_21));
}
out.write("<div class=\"news_filter__close-button\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_white.svg\"/>\r\n                    </div>\r\n                </div>\r\n                <div class=\"news_filter-group__content\">\r\n                    <div class=\"card-multiple-select mobile-only\"");
{
    Object var_attrvalue22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "eventTypeLabel"), "html");
    {
        boolean var_shoulddisplayattr25 = ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22)));
        if (var_shoulddisplayattr25) {
            out.write(" data-title");
            {
                boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                if (!var_istrueattr24) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue22));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue26 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "tags"), "html");
    {
        boolean var_shoulddisplayattr29 = ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26)));
        if (var_shoulddisplayattr29) {
            out.write(" data-options");
            {
                boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                if (!var_istrueattr28) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue26));
                    out.write("'");
                }
            }
        }
    }
}
out.write(">\r\n                    </div>\r\n                    <!-- Filter title -->\r\n                    <div class=\"offer-filter__title margin-bottom__short\">\r\n                        <h6>");
{
    Object var_30 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "showMeLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_30));
}
out.write("</h6>\r\n                    </div>\r\n                    <!-- Sort checkbox item -->\r\n                    <div class=\"offer-filter__sort-by-wrap offer-filter__checkbox flex-show\">\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <label class=\"checkbox-item__wrapper\"><input class=\"input__checkbox\" name=\"sort\" type=\"checkbox\" checked value=\"upcoming\"/>\r\n                                <div>");
{
    Object var_31 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "upcomingEventsLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_31));
}
out.write("</div>\r\n                            </label>\r\n                        </div>\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <label class=\"checkbox-item__wrapper\"><input class=\"input__checkbox\" name=\"sort\" type=\"checkbox\" checked value=\"before\"/>\r\n                                <div>");
{
    Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "pastEventsLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_32));
}
out.write("</div>\r\n                            </label>\r\n                        </div>\r\n                    </div>\r\n                    <!-- Filter title -->\r\n                    <div class=\"offer-filter__title margin-bottom__short\">\r\n                        <h6>");
{
    String var_33 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "selectDateLabel"), "html")));
    out.write(renderContext.getObjectModel().toString(var_33));
}
out.write("</h6>\r\n                    </div>\r\n                    <!-- Datepicker item -->\r\n                    <div style=\"display: flex;\">\r\n                        <div class=\"tcb-date-picker\">\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"offer-filter__apply mobile-only\">\r\n                        <div class=\"tcb-button tcb-button--light-black\">");
{
    String var_34 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "applyFilterLabel"), "html"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_34));
}
out.write("</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"news_open-filter-button\"");
{
    Object var_attrvalue35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "filterLabel"), "html");
    {
        boolean var_shoulddisplayattr38 = ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35)));
        if (var_shoulddisplayattr38) {
            out.write(" data-title");
            {
                boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                if (!var_istrueattr37) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue35));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue39 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "filterLabel"), "html");
    {
        boolean var_shoulddisplayattr42 = ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39)));
        if (var_shoulddisplayattr42) {
            out.write(" data-action");
            {
                boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                if (!var_istrueattr41) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue39));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(">\r\n                <button class=\"btn-open-filter\">\r\n                    <span>");
{
    Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "filterLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_43));
}
out.write("</span>\r\n                    <div class=\"btn-open-filter__icon\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-filter-red.svg\" width=\"16\" height=\"16\"/>\r\n                    </div>\r\n                </button>\r\n                <div class=\"btn-open-filter-sticky\">\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-filter-red.svg\" width=\"16\" height=\"16\"/>\r\n                </div>\r\n            </div>\r\n            <!-- Offer cards container -->\r\n            <div class=\"offer-cards__container\">\r\n                <div class=\"news-tab-content_container\">\r\n                </div>\r\n                <div class=\"information-filter__load-more\">\r\n                    <button type=\"button\" class=\"load-more__button\">\r\n                        <span>");
{
    Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabel"), "html");
    out.write(renderContext.getObjectModel().toString(var_44));
}
out.write("</span>\r\n                        <div class=\"load-more__button-icon\">\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\" width=\"16\" height=\"16\"/>\r\n                        </div>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

