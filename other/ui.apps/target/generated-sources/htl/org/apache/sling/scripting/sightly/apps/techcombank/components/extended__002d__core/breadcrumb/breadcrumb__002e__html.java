/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.extended__002d__core.breadcrumb;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class breadcrumb__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_breadcrumb = null;
Object _global_extendmodel = null;
Object _global_template = null;
Collection var_collectionvar13_list_coerced$ = null;
Object _dynamic_properties = bindings.get("properties");
Collection var_attrmap33_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
_global_breadcrumb = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Breadcrumb.class.getName(), obj());
_global_extendmodel = renderContext.call("use", com.techcombank.core.models.BreadcrumbModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
{
    boolean var_testvariable0 = ((!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "items"), "size"), 0))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_extendmodel, "hideBreadcrumb"))));
    if (var_testvariable0) {
        out.write("<nav");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "id");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" id");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"hero-breadcrumb-container tcb-container\"");
        {
            Object var_attrvalue5 = renderContext.call("i18n", "Breadcrumb", obj().with("i18n", null));
            {
                Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                {
                    boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                    if (var_shoulddisplayattr8) {
                        out.write(" aria-label");
                        {
                            boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                            if (!var_istrueattr7) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "data"), "json");
            {
                Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                {
                    boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                    if (var_shoulddisplayattr12) {
                        out.write(" data-cmp-data-layer");
                        {
                            boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                            if (!var_istrueattr11) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n    ");
        {
            Object var_collectionvar13 = renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "items");
            {
                long var_size14 = ((var_collectionvar13_list_coerced$ == null ? (var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13)) : var_collectionvar13_list_coerced$).size());
                {
                    boolean var_notempty15 = (var_size14 > 0);
                    if (var_notempty15) {
                        {
                            long var_end18 = var_size14;
                            {
                                boolean var_validstartstepend19 = (((0 < var_size14) && true) && (var_end18 > 0));
                                if (var_validstartstepend19) {
                                    out.write("<ol class=\"cmp-breadcrumb__list\" itemscope itemtype=\"http://schema.org/BreadcrumbList\">");
                                    if (var_collectionvar13_list_coerced$ == null) {
                                        var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13);
                                    }
                                    long var_index20 = 0;
                                    for (Object navitem : var_collectionvar13_list_coerced$) {
                                        {
                                            long navitemlist_field$_count = (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.ADD.eval(var_index20, 1)).longValue());
                                            {
                                                boolean var_traversal22 = (((var_index20 >= 0) && (var_index20 <= var_end18)) && true);
                                                if (var_traversal22) {
                                                    out.write("\r\n        <li");
                                                    {
                                                        String var_attrcontent23 = ("breadcrumb-item cmp-breadcrumb__item" + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navitem, "active")) ? " cmp-breadcrumb__item--active" : ""), "attribute")));
                                                        out.write(" class=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent24 = (("color:" + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navitem, "active")) ? renderContext.getObjectModel().resolveProperty(_dynamic_properties, "activePageColor") : renderContext.getObjectModel().resolveProperty(_dynamic_properties, "previousPagesColor")), "styleToken"))) + ";");
                                                        out.write(" style=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        Object var_attrvalue25 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navitem, "active")) ? "page" : false);
                                                        {
                                                            Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                if (var_shoulddisplayattr28) {
                                                                    out.write(" aria-current");
                                                                    {
                                                                        boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                        if (!var_istrueattr27) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(navitem, "data"), "json");
                                                        {
                                                            Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                if (var_shoulddisplayattr32) {
                                                                    out.write(" data-cmp-data-layer");
                                                                    {
                                                                        boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                        if (!var_istrueattr31) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\">\r\n            ");
                                                    {
                                                        Object var_unwrapcondition34 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navitem, "active")) ? renderContext.getObjectModel().resolveProperty(navitem, "active") : (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(navitem, "link"), "valid")))));
                                                        if (!renderContext.getObjectModel().toBoolean(var_unwrapcondition34)) {
                                                            out.write("<a");
                                                            {
                                                                Object var_attrmap33 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(navitem, "link"), "htmlAttributes");
                                                                out.write(" class=\"cmp-breadcrumb__item-link\" itemprop=\"item\"");
                                                                {
                                                                    boolean var_attrvalue35 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "data")) ? true : false);
                                                                    {
                                                                        Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (false != var_attrvalue35)));
                                                                            if (var_shoulddisplayattr38) {
                                                                                out.write(" data-cmp-clickable");
                                                                                {
                                                                                    boolean var_istrueattr37 = (var_attrvalue35 == true);
                                                                                    if (!var_istrueattr37) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                                {
                                                                    String var_attrcontent39 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navitem, "title"), "attribute"))) + "'}");
                                                                    out.write(" data-tracking-click-info-value=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                    out.write("\"");
                                                                }
                                                                {
                                                                    String var_attrcontent40 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                    out.write("\"");
                                                                }
                                                                {
                                                                    boolean var_ignoredattributes41_field$_data__002d__tracking__002d__click__002d__info__002d__value = true;
                                                                    {
                                                                        boolean var_ignoredattributes41_field$_data__002d__cmp__002d__clickable = true;
                                                                        {
                                                                            boolean var_ignoredattributes41_field$_data__002d__tracking__002d__web__002d__interaction__002d__value = true;
                                                                            {
                                                                                boolean var_ignoredattributes41_field$_itemprop = true;
                                                                                {
                                                                                    boolean var_ignoredattributes41_field$_class = true;
                                                                                    {
                                                                                        boolean var_ignoredattributes41_field$_data__002d__tracking__002d__click__002d__event = true;
                                                                                        {
                                                                                            java.util.Map var_ignoredattributes41 = obj().with("data-tracking-click-info-value", var_ignoredattributes41_field$_data__002d__tracking__002d__click__002d__info__002d__value).with("data-cmp-clickable", var_ignoredattributes41_field$_data__002d__cmp__002d__clickable).with("data-tracking-web-interaction-value", var_ignoredattributes41_field$_data__002d__tracking__002d__web__002d__interaction__002d__value).with("itemprop", var_ignoredattributes41_field$_itemprop).with("class", var_ignoredattributes41_field$_class).with("data-tracking-click-event", var_ignoredattributes41_field$_data__002d__tracking__002d__click__002d__event);
                                                                                            if (var_attrmap33_list_coerced$ == null) {
                                                                                                var_attrmap33_list_coerced$ = renderContext.getObjectModel().toCollection(var_attrmap33);
                                                                                            }
                                                                                            long var_attrindex44 = 0;
                                                                                            for (Object var_attrname42 : var_attrmap33_list_coerced$) {
                                                                                                {
                                                                                                    Object var_attrnameescaped43 = renderContext.call("xss", var_attrname42, "attributeName");
                                                                                                    if (renderContext.getObjectModel().toBoolean(var_attrnameescaped43)) {
                                                                                                        {
                                                                                                            Object var_isignoredattr45 = var_ignoredattributes41.get(var_attrname42);
                                                                                                            if (!renderContext.getObjectModel().toBoolean(var_isignoredattr45)) {
                                                                                                                {
                                                                                                                    Object var_attrcontent46 = renderContext.getObjectModel().resolveProperty(var_attrmap33, var_attrname42);
                                                                                                                    {
                                                                                                                        Object var_attrcontentescaped47 = renderContext.call("xss", var_attrcontent46, "attribute", var_attrnameescaped43);
                                                                                                                        {
                                                                                                                            boolean var_shoulddisplayattr48 = (((null != var_attrcontentescaped47) && (!"".equals(var_attrcontentescaped47))) && ((!"".equals(var_attrcontent46)) && (!((Object)false).equals(var_attrcontent46))));
                                                                                                                            if (var_shoulddisplayattr48) {
                                                                                                                                out.write(" ");
                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrnameescaped43));
                                                                                                                                {
                                                                                                                                    boolean var_istrueattr49 = (var_attrcontent46.equals(true));
                                                                                                                                    if (!var_istrueattr49) {
                                                                                                                                        out.write("=\"");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontentescaped47));
                                                                                                                                        out.write("\"");
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                var_attrindex44++;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                var_attrmap33_list_coerced$ = null;
                                                            }
                                                            out.write(">");
                                                        }
                                                        out.write("\r\n                <span class=\"breadcrumb-text\" itemprop=\"name\">");
                                                        {
                                                            Object var_50 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navitem, "title"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_50));
                                                        }
                                                        out.write("</span>\r\n            ");
                                                        if (!renderContext.getObjectModel().toBoolean(var_unwrapcondition34)) {
                                                            out.write("</a>");
                                                        }
                                                    }
                                                    out.write("\r\n            <meta itemprop=\"position\"");
                                                    {
                                                        long var_attrvalue51 = navitemlist_field$_count;
                                                        {
                                                            Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                                                                if (var_shoulddisplayattr54) {
                                                                    out.write(" content");
                                                                    {
                                                                        boolean var_istrueattr53 = (((Object)var_attrvalue51).equals(true));
                                                                        if (!var_istrueattr53) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n        </li>\r\n    ");
                                                }
                                            }
                                        }
                                        var_index20++;
                                    }
                                    out.write("</ol>");
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar13_list_coerced$ = null;
        }
        out.write("\r\n</nav>");
    }
}
out.write("\r\n");
{
    Object var_templatevar55 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        Object var_templateoptions56_field$_isempty = (((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "items"), "size"), 0)) ? (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_breadcrumb, "items"), "size"), 0)) : renderContext.getObjectModel().resolveProperty(_global_extendmodel, "hideBreadcrumb")));
        {
            String var_templateoptions56_field$_classappend = "cmp-breadcrumb";
            {
                java.util.Map var_templateoptions56 = obj().with("isEmpty", var_templateoptions56_field$_isempty).with("classAppend", var_templateoptions56_field$_classappend);
                callUnit(out, renderContext, var_templatevar55, var_templateoptions56);
            }
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

