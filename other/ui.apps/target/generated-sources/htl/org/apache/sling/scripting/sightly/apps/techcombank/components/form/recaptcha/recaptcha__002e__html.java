/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.recaptcha;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class recaptcha__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_recaptchamodel = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
_global_recaptchamodel = renderContext.call("use", com.techcombank.core.models.RecaptchaModel.class.getName(), obj());
out.write("\r\n");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n<div class=\"recaptcha-container\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "siteKey");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-sitekey");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "secretKey");
    {
        Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
        {
            boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
            if (var_shoulddisplayattr12) {
                out.write(" data-secretkey");
                {
                    boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                    if (!var_istrueattr11) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <input type=\"hidden\" id=\"siteKey\" name=\"siteKey\"");
{
    Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "siteKey");
    {
        Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
        {
            boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
            if (var_shoulddisplayattr16) {
                out.write(" value");
                {
                    boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                    if (!var_istrueattr15) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n    <input type=\"hidden\" id=\"secretKey\" name=\"secretKey\"");
{
    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "secretKey");
    {
        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
        {
            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
            if (var_shoulddisplayattr20) {
                out.write(" value");
                {
                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                    if (!var_istrueattr19) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n    <div id=\"recaptcha\" class=\"recaptcha-element\"></div>\r\n    ");
{
    Object var_testvariable21 = renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "isRequired");
    if (renderContext.getObjectModel().toBoolean(var_testvariable21)) {
        out.write("<p id=\"captchaError\" class=\"recaptcha-error  form-error-message\">");
        {
            Object var_22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_recaptchamodel, "requiredMessage"), "text");
            out.write(renderContext.getObjectModel().toString(var_22));
        }
        out.write("</p>");
    }
}
out.write("\r\n</div>\r\n<script src=\"https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit&hl=en\" async defer></script>\r\n\r\n<script>\r\nvar recaptchaCallback = () => {\r\n    var recaptchaContainers = document.querySelectorAll(\".recaptcha-container\");\r\n\r\n    recaptchaContainers.forEach(function (container) {\r\n      var siteKey = container.getAttribute(\"data-sitekey\");\r\n      var recaptchaDiv = container.querySelectorAll(\".recaptcha-element\");\r\n\r\n      if (\r\n        siteKey !== null &&\r\n        siteKey !== \"\" &&\r\n        !container.hasAttribute(\"data-rendered\")\r\n      ) {\r\n        // Create a unique ID for each container to identify it\r\n        var containerId = \"recaptcha-\" + siteKey;\r\n\r\n        // Check if reCAPTCHA is already rendered\r\n        if (document.getElementById(containerId) === null) {\r\n          recaptchaDiv.forEach(function (e) {\r\n            e.innerHTML = \"\";\r\n\r\n            // Render reCAPTCHA\r\n            grecaptcha.render(e, {\r\n              sitekey: siteKey,\r\n              callback: function (response) {\r\n                e.nextElementSibling.style.display = 'none';\r\n                e.parentElement.classList.add(\"captcha-checked\");\r\n                recaptchaValidation(response);\r\n              },\r\n            });\r\n\r\n            container.setAttribute(\"data-rendered\", \"true\");\r\n          });\r\n        }\r\n      }\r\n    });\r\n  }\r\n\r\n  function recaptchaValidation(clickedButton, response) {\r\n    var captcha = clickedButton.parentElement.querySelector(\".recaptcha-container\"\r\n    );\r\n    if (!captcha) {\r\n        return false; // checked recaptcha-container is present or not\r\n    }\r\n    var errorElement = captcha.querySelector(\".recaptcha-error\");\r\n    var siteKeyElement = captcha.querySelector(\"#siteKey\");\r\n\r\n    if (siteKeyElement && siteKeyElement.value !== \"\") {\r\n      if (captcha.classList.contains(\"captcha-checked\")) {\r\n        // If reCAPTCHA is valid\r\n        if(captcha.closest(\"form\").classList.contains(\"booking-container\")) {\r\n          errorElement.style.display = \"none\";\t  \r\n        }else{\r\n          errorElement.classList.remove(\"form-error-message\");\r\n        }\r\n      } else {\r\n        // If reCAPTCHA is not valid\r\n        if (errorElement) {\r\n          errorElement.style.display = \"block\";\r\n          return false;\r\n        }\r\n      }\r\n    }\r\n  }\r\n</script>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

