/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.articlesrelatedpost;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class articlesrelatedpost__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Collection var_collectionvar8_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.ArticlesRelatedPostModel.class.getName(), obj());
{
    boolean var_testvariable5 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "hideStatus")));
    if (var_testvariable5) {
        out.write("\r\n    ");
        {
            Object var_testvariable6 = renderContext.getObjectModel().resolveProperty(_global_model, "relatedPost");
            if (renderContext.getObjectModel().toBoolean(var_testvariable6)) {
                out.write("<section class=\"article-related-post\">\r\n        <h2 class=\"article-related-post_title\">");
                {
                    String var_7 = (("\r\n            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "titleText"), "text"))) + "\r\n        ");
                    out.write(renderContext.getObjectModel().toString(var_7));
                }
                out.write("</h2>\r\n        <div class=\"article-related-post_content\">\r\n            ");
                {
                    Object var_collectionvar8 = renderContext.getObjectModel().resolveProperty(_global_model, "relatedPost");
                    {
                        long var_size9 = ((var_collectionvar8_list_coerced$ == null ? (var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8)) : var_collectionvar8_list_coerced$).size());
                        {
                            boolean var_notempty10 = (var_size9 > 0);
                            if (var_notempty10) {
                                {
                                    long var_end13 = var_size9;
                                    {
                                        boolean var_validstartstepend14 = (((0 < var_size9) && true) && (var_end13 > 0));
                                        if (var_validstartstepend14) {
                                            out.write("<div class=\"article-related-post_list\">");
                                            if (var_collectionvar8_list_coerced$ == null) {
                                                var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8);
                                            }
                                            long var_index15 = 0;
                                            for (Object article : var_collectionvar8_list_coerced$) {
                                                {
                                                    boolean var_traversal17 = (((var_index15 >= 0) && (var_index15 <= var_end13)) && true);
                                                    if (var_traversal17) {
                                                        out.write("\r\n                <div class=\"article-card\">\r\n                    <div class=\"article-card_container\">\r\n                        <div class=\"article-card_container_image\" aria-hidden=\"true\">\r\n                    <span style=\"box-sizing: border-box; display: block; overflow: hidden; width: initial; height: initial; background: none; opacity: 1; border: 0px; margin: 0px; padding: 0px; position: absolute; inset: 0px;\">\r\n                      <picture>\r\n                          <source media=\"(max-width: 360px)\"");
                                                        {
                                                            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(article, "mobileImage");
                                                            {
                                                                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                                    if (var_shoulddisplayattr21) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                            if (!var_istrueattr20) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                          <source media=\"(max-width: 576px)\"");
                                                        {
                                                            Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(article, "mobileImage");
                                                            {
                                                                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                                    if (var_shoulddisplayattr25) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                            if (!var_istrueattr24) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                          <source media=\"(min-width: 1080px)\"");
                                                        {
                                                            Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(article, "webImage");
                                                            {
                                                                Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                                    if (var_shoulddisplayattr29) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                            if (!var_istrueattr28) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                          <source media=\"(min-width: 1200px)\"");
                                                        {
                                                            Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(article, "webImage");
                                                            {
                                                                Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                                    if (var_shoulddisplayattr33) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                            if (!var_istrueattr32) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                          <img class=\"home-page-left__item--img\"");
                                                        {
                                                            Object var_attrvalue34 = renderContext.getObjectModel().resolveProperty(article, "webImage");
                                                            {
                                                                Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                                                                    if (var_shoulddisplayattr37) {
                                                                        out.write(" src");
                                                                        {
                                                                            boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                                                                            if (!var_istrueattr36) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(article, "altText");
                                                            {
                                                                Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                                    if (var_shoulddisplayattr41) {
                                                                        out.write(" alt");
                                                                        {
                                                                            boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                            if (!var_istrueattr40) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                      </picture>\r\n                    </span>\r\n                        </div>\r\n                        <div class=\"article-card_content\">\r\n                            <div class=\"article-card_content_top\">\r\n                                <div class=\"article-label\">\r\n                                    <span>");
                                                        {
                                                            Object var_42 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "articleCreationDate"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_42));
                                                        }
                                                        out.write("</span>\r\n                                </div>\r\n                                <div class=\"article-title\">");
                                                        {
                                                            Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "title"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_43));
                                                        }
                                                        out.write("</div>\r\n                                <p class=\"article-content\">");
                                                        {
                                                            Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "description"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_44));
                                                        }
                                                        out.write("</p>\r\n                            </div>\r\n                            <div>\r\n                                <a");
                                                        {
                                                            Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(article, "path");
                                                            {
                                                                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                                                                    if (var_shoulddisplayattr48) {
                                                                        out.write(" href");
                                                                        {
                                                                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                                                                            if (!var_istrueattr47) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue49 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "newTab"), "true")) ? "_blank" : "_self");
                                                            {
                                                                Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                                                                    if (var_shoulddisplayattr52) {
                                                                        out.write(" target");
                                                                        {
                                                                            boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                                                            if (!var_istrueattr51) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(" rel=\"noopener noreferrer\" class=\"article-viewmore\" style=\"justify-content: flex-start;\" data-tracking-click-event=\"articleView\"");
                                                        {
                                                            String var_attrcontent53 = (("{'articleName': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "title"), "attribute"))) + "'}");
                                                            out.write(" data-tracking-click-info-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            String var_attrcontent54 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n                                    <span class=\"button-text\">");
                                                        {
                                                            Object var_55 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewMore"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_55));
                                                        }
                                                        out.write("</span>\r\n                                    <div class=\"button-icon\">\r\n                                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                                    </div>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            ");
                                                    }
                                                }
                                                var_index15++;
                                            }
                                            out.write("</div>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var_collectionvar8_list_coerced$ = null;
                }
                out.write("\r\n        </div>\r\n    </section>");
            }
        }
        out.write("\r\n");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

