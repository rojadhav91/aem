/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.inspireprivilege;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class inspireprivilege__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar14_list_coerced$ = null;
Collection var_collectionvar30_list_coerced$ = null;
Collection var_collectionvar44_list_coerced$ = null;
Collection var_collectionvar60_list_coerced$ = null;
Collection var_collectionvar75_list_coerced$ = null;
Collection var_collectionvar105_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.InspirePrivilegeModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"inspire-privilege\">\r\n            <div class=\"inspire-privilege__wrapper\">\r\n                <div class=\"inspire-privilege__container\">\r\n                    <div");
            {
                String var_attrcontent1 = ("inspire-privilege__container2 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "enableSeparator")) ? "bottom-separator" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n                        <p class=\"content-item__title\">");
            {
                Object var_2 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "title"), "text");
                out.write(renderContext.getObjectModel().toString(var_2));
            }
            out.write("</p>\r\n                        <div class=\"content-item__container\">\r\n                            <div class=\"content-item__primary\">\r\n                                ");
            {
                boolean var_testvariable3 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation"), "image"));
                if (var_testvariable3) {
                    out.write("<div");
                    {
                        String var_attrcontent4 = ("content-item__picture enable-shadow-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "enableShadow"), "attribute")));
                        out.write(" class=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent4));
                        out.write("\"");
                    }
                    out.write(">\r\n                                    <picture>\r\n                                        <img");
                    {
                        Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_model, "image");
                        {
                            Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "uri");
                            {
                                boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                                if (var_shoulddisplayattr8) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                        if (!var_istrueattr7) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_model, "altText");
                        {
                            Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                            {
                                boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                                if (var_shoulddisplayattr12) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                                        if (!var_istrueattr11) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" class=\"content-item__image\"/>\r\n                                    </picture>\r\n                                </div>");
                }
            }
            out.write("\r\n                                ");
            {
                boolean var_testvariable13 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation"), "slider"));
                if (var_testvariable13) {
                    out.write("<div class=\"swiper swiper1 content-item__picture\" is-swiper=\"1\">\r\n                                    <div class=\"swiper-wrapper\">\r\n                                        ");
                    {
                        Object var_collectionvar14 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow1List");
                        {
                            long var_size15 = ((var_collectionvar14_list_coerced$ == null ? (var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14)) : var_collectionvar14_list_coerced$).size());
                            {
                                boolean var_notempty16 = (var_size15 > 0);
                                if (var_notempty16) {
                                    {
                                        long var_end19 = var_size15;
                                        {
                                            boolean var_validstartstepend20 = (((0 < var_size15) && true) && (var_end19 > 0));
                                            if (var_validstartstepend20) {
                                                if (var_collectionvar14_list_coerced$ == null) {
                                                    var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14);
                                                }
                                                long var_index21 = 0;
                                                for (Object slideimage : var_collectionvar14_list_coerced$) {
                                                    {
                                                        boolean var_traversal23 = (((var_index21 >= 0) && (var_index21 <= var_end19)) && true);
                                                        if (var_traversal23) {
                                                            out.write("<div class=\"swiper-slide\">\r\n                                            <picture>\r\n                                                <img");
                                                            {
                                                                Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(slideimage, "image");
                                                                {
                                                                    Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "uri");
                                                                    {
                                                                        boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                                                                        if (var_shoulddisplayattr27) {
                                                                            out.write(" src");
                                                                            {
                                                                                boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                                                                if (!var_istrueattr26) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" class=\"content-item__image\" alt=\"img-tcb\"/>\r\n                                            </picture>\r\n                                        </div>\n");
                                                        }
                                                    }
                                                    var_index21++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar14_list_coerced$ = null;
                    }
                    out.write("\r\n                                    </div>\r\n                                    <div class=\"swiper-button-prev\"></div>\r\n                                    <div class=\"swiper-button-next\"></div>\r\n                                </div>");
                }
            }
            out.write("\r\n                                ");
            {
                boolean var_testvariable28 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation"), "auto-slider"));
                if (var_testvariable28) {
                    out.write("<div class=\"content-item__multi-picture\">\r\n                                    ");
                    {
                        Object var_testvariable29 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow1List");
                        if (renderContext.getObjectModel().toBoolean(var_testvariable29)) {
                            out.write("\r\n                                        <div class=\"swiper swiper2 multi-picture__row\">\r\n                                            <div class=\"swiper-wrapper\">\r\n                                                ");
                            {
                                Object var_collectionvar30 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow1List");
                                {
                                    long var_size31 = ((var_collectionvar30_list_coerced$ == null ? (var_collectionvar30_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar30)) : var_collectionvar30_list_coerced$).size());
                                    {
                                        boolean var_notempty32 = (var_size31 > 0);
                                        if (var_notempty32) {
                                            {
                                                long var_end35 = var_size31;
                                                {
                                                    boolean var_validstartstepend36 = (((0 < var_size31) && true) && (var_end35 > 0));
                                                    if (var_validstartstepend36) {
                                                        if (var_collectionvar30_list_coerced$ == null) {
                                                            var_collectionvar30_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar30);
                                                        }
                                                        long var_index37 = 0;
                                                        for (Object slideimage : var_collectionvar30_list_coerced$) {
                                                            {
                                                                boolean var_traversal39 = (((var_index37 >= 0) && (var_index37 <= var_end35)) && true);
                                                                if (var_traversal39) {
                                                                    out.write("<div class=\"swiper-slide\">\r\n                                                    <picture>\r\n                                                        <img");
                                                                    {
                                                                        Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(slideimage, "image");
                                                                        {
                                                                            Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                                if (var_shoulddisplayattr43) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                                        if (!var_istrueattr42) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write(" class=\"content-item__image\" alt=\"img-tcb\"/>\r\n                                                    </picture>\r\n                                                </div>\n");
                                                                }
                                                            }
                                                            var_index37++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                var_collectionvar30_list_coerced$ = null;
                            }
                            out.write("\r\n                                                <!-- for only one card, swiper will not scroll => solution: duplicate more one card -->\r\n                                                ");
                            {
                                boolean var_testvariable51 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow1List"), "size"), 1));
                                if (var_testvariable51) {
                                    {
                                        Object var_collectionvar44 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow1List");
                                        {
                                            long var_size45 = ((var_collectionvar44_list_coerced$ == null ? (var_collectionvar44_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar44)) : var_collectionvar44_list_coerced$).size());
                                            {
                                                boolean var_notempty46 = (var_size45 > 0);
                                                if (var_notempty46) {
                                                    {
                                                        long var_end49 = var_size45;
                                                        {
                                                            boolean var_validstartstepend50 = (((0 < var_size45) && true) && (var_end49 > 0));
                                                            if (var_validstartstepend50) {
                                                                if (var_collectionvar44_list_coerced$ == null) {
                                                                    var_collectionvar44_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar44);
                                                                }
                                                                long var_index52 = 0;
                                                                for (Object slideimage : var_collectionvar44_list_coerced$) {
                                                                    {
                                                                        boolean var_traversal54 = (((var_index52 >= 0) && (var_index52 <= var_end49)) && true);
                                                                        if (var_traversal54) {
                                                                            out.write("<div class=\"swiper-slide duplicate-for-only-one-card\">\r\n                                                    <picture>\r\n                                                        <img");
                                                                            {
                                                                                Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(slideimage, "image");
                                                                                {
                                                                                    Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "uri");
                                                                                    {
                                                                                        boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                                                                                        if (var_shoulddisplayattr58) {
                                                                                            out.write(" src");
                                                                                            {
                                                                                                boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                                                                                                if (!var_istrueattr57) {
                                                                                                    out.write("=\"");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                                                                                    out.write("\"");
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            out.write(" class=\"content-item__image\" alt=\"img-tcb\"/>\r\n                                                    </picture>\r\n                                                </div>\n");
                                                                        }
                                                                    }
                                                                    var_index52++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var_collectionvar44_list_coerced$ = null;
                                    }
                                }
                            }
                            out.write("\r\n                                            </div>\r\n                                        </div>\r\n                                    ");
                        }
                    }
                    out.write("\r\n                                    ");
                    {
                        Object var_testvariable59 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow2List");
                        if (renderContext.getObjectModel().toBoolean(var_testvariable59)) {
                            out.write("\r\n                                        <div class=\"swiper swiper2 multi-picture__row\">\r\n                                            <div class=\"swiper-wrapper\">\r\n                                                ");
                            {
                                Object var_collectionvar60 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow2List");
                                {
                                    long var_size61 = ((var_collectionvar60_list_coerced$ == null ? (var_collectionvar60_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar60)) : var_collectionvar60_list_coerced$).size());
                                    {
                                        boolean var_notempty62 = (var_size61 > 0);
                                        if (var_notempty62) {
                                            {
                                                long var_end65 = var_size61;
                                                {
                                                    boolean var_validstartstepend66 = (((0 < var_size61) && true) && (var_end65 > 0));
                                                    if (var_validstartstepend66) {
                                                        if (var_collectionvar60_list_coerced$ == null) {
                                                            var_collectionvar60_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar60);
                                                        }
                                                        long var_index67 = 0;
                                                        for (Object slideimage : var_collectionvar60_list_coerced$) {
                                                            {
                                                                boolean var_traversal69 = (((var_index67 >= 0) && (var_index67 <= var_end65)) && true);
                                                                if (var_traversal69) {
                                                                    out.write("<div class=\"swiper-slide\">\r\n                                                    <picture>\r\n                                                        <img");
                                                                    {
                                                                        Object var_attrvalue70 = renderContext.getObjectModel().resolveProperty(slideimage, "image");
                                                                        {
                                                                            Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                                                                                if (var_shoulddisplayattr73) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                                                                                        if (!var_istrueattr72) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write(" class=\"content-item__image\" alt=\"img-tcb\"/>\r\n                                                    </picture>\r\n                                                </div>\n");
                                                                }
                                                            }
                                                            var_index67++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                var_collectionvar60_list_coerced$ = null;
                            }
                            out.write("\r\n                                            </div>\r\n                                        </div>\r\n                                    ");
                        }
                    }
                    out.write("\r\n                                    ");
                    {
                        Object var_testvariable74 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow3List");
                        if (renderContext.getObjectModel().toBoolean(var_testvariable74)) {
                            out.write("\r\n                                        <div class=\"swiper swiper2 multi-picture__row\">\r\n                                            <div class=\"swiper-wrapper\">\r\n                                                ");
                            {
                                Object var_collectionvar75 = renderContext.getObjectModel().resolveProperty(_global_model, "slideImageRow3List");
                                {
                                    long var_size76 = ((var_collectionvar75_list_coerced$ == null ? (var_collectionvar75_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar75)) : var_collectionvar75_list_coerced$).size());
                                    {
                                        boolean var_notempty77 = (var_size76 > 0);
                                        if (var_notempty77) {
                                            {
                                                long var_end80 = var_size76;
                                                {
                                                    boolean var_validstartstepend81 = (((0 < var_size76) && true) && (var_end80 > 0));
                                                    if (var_validstartstepend81) {
                                                        if (var_collectionvar75_list_coerced$ == null) {
                                                            var_collectionvar75_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar75);
                                                        }
                                                        long var_index82 = 0;
                                                        for (Object slideimage : var_collectionvar75_list_coerced$) {
                                                            {
                                                                boolean var_traversal84 = (((var_index82 >= 0) && (var_index82 <= var_end80)) && true);
                                                                if (var_traversal84) {
                                                                    out.write("<div class=\"swiper-slide\">\r\n                                                    <picture>\r\n                                                        <img");
                                                                    {
                                                                        Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(slideimage, "image");
                                                                        {
                                                                            Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
                                                                                if (var_shoulddisplayattr88) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                                                                                        if (!var_istrueattr87) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write(" class=\"content-item__image\" alt=\"img-tcb\"/>\r\n                                                    </picture>\r\n                                                </div>\n");
                                                                }
                                                            }
                                                            var_index82++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                var_collectionvar75_list_coerced$ = null;
                            }
                            out.write("\r\n                                            </div>\r\n                                        </div>\r\n                                    ");
                        }
                    }
                    out.write("\r\n                                </div>");
                }
            }
            out.write("\r\n                                <a class=\"info-link info-link-mobile\"");
            {
                Object var_attrvalue89 = renderContext.getObjectModel().resolveProperty(_global_model, "linkUrl");
                {
                    Object var_attrcontent90 = renderContext.call("xss", var_attrvalue89, "uri");
                    {
                        boolean var_shoulddisplayattr92 = (((null != var_attrcontent90) && (!"".equals(var_attrcontent90))) && ((!"".equals(var_attrvalue89)) && (!((Object)false).equals(var_attrvalue89))));
                        if (var_shoulddisplayattr92) {
                            out.write(" href");
                            {
                                boolean var_istrueattr91 = (var_attrvalue89.equals(true));
                                if (!var_istrueattr91) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue93 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "openInNewTab")) ? "_blank" : "_self");
                {
                    Object var_attrcontent94 = renderContext.call("xss", var_attrvalue93, "attribute");
                    {
                        boolean var_shoulddisplayattr96 = (((null != var_attrcontent94) && (!"".equals(var_attrcontent94))) && ((!"".equals(var_attrvalue93)) && (!((Object)false).equals(var_attrvalue93))));
                        if (var_shoulddisplayattr96) {
                            out.write(" target");
                            {
                                boolean var_istrueattr95 = (var_attrvalue93.equals(true));
                                if (!var_istrueattr95) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent94));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue97 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "noFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "attribute");
                    {
                        boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                        if (var_shoulddisplayattr100) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                                if (!var_istrueattr99) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent101 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "linkText"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                out.write("\"");
            }
            {
                String var_attrcontent102 = (("{'webInteractions': {'name': 'Inspire Privilege','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "webInteractionType"), "attribute"))) + "'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                out.write("\"");
            }
            out.write(">");
            {
                String var_103 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "linkText"), "text"))) + "\r\n                                    ");
                out.write(renderContext.getObjectModel().toString(var_103));
            }
            out.write("<img class=\"info-link__icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                                </a>\r\n                            </div>\r\n                            <div");
            {
                String var_attrcontent104 = ("content-item__text list-item-count-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_model, "contentList"), "size"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                out.write("\"");
            }
            out.write(">\r\n                                ");
            {
                Object var_collectionvar105 = renderContext.getObjectModel().resolveProperty(_global_model, "contentList");
                {
                    long var_size106 = ((var_collectionvar105_list_coerced$ == null ? (var_collectionvar105_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar105)) : var_collectionvar105_list_coerced$).size());
                    {
                        boolean var_notempty107 = (var_size106 > 0);
                        if (var_notempty107) {
                            {
                                long var_end110 = var_size106;
                                {
                                    boolean var_validstartstepend111 = (((0 < var_size106) && true) && (var_end110 > 0));
                                    if (var_validstartstepend111) {
                                        if (var_collectionvar105_list_coerced$ == null) {
                                            var_collectionvar105_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar105);
                                        }
                                        long var_index112 = 0;
                                        for (Object contentitem : var_collectionvar105_list_coerced$) {
                                            {
                                                boolean var_traversal114 = (((var_index112 >= 0) && (var_index112 <= var_end110)) && true);
                                                if (var_traversal114) {
                                                    out.write("<div class=\"list-item\">\r\n                                    <div class=\"list-item__image\">\r\n                                        <img");
                                                    {
                                                        Object var_attrvalue115 = renderContext.getObjectModel().resolveProperty(contentitem, "contentIcon");
                                                        {
                                                            Object var_attrcontent116 = renderContext.call("xss", var_attrvalue115, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr118 = (((null != var_attrcontent116) && (!"".equals(var_attrcontent116))) && ((!"".equals(var_attrvalue115)) && (!((Object)false).equals(var_attrvalue115))));
                                                                if (var_shoulddisplayattr118) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr117 = (var_attrvalue115.equals(true));
                                                                        if (!var_istrueattr117) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" alt/>\r\n                                    </div>\r\n                                    <div class=\"list-item__content\">\r\n                                        <div>");
                                                    {
                                                        String var_119 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(contentitem, "contentText"), "html"))) + "\r\n                                        ");
                                                        out.write(renderContext.getObjectModel().toString(var_119));
                                                    }
                                                    out.write("</div>\r\n                                    </div>\r\n                                </div>\n");
                                                }
                                            }
                                            var_index112++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar105_list_coerced$ = null;
            }
            out.write("\r\n                                <a class=\"info-link info-link-desktop\"");
            {
                Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(_global_model, "linkUrl");
                {
                    Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "uri");
                    {
                        boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                        if (var_shoulddisplayattr123) {
                            out.write(" href");
                            {
                                boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                                if (!var_istrueattr122) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue124 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "openInNewTab")) ? "_blank" : "_self");
                {
                    Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "attribute");
                    {
                        boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
                        if (var_shoulddisplayattr127) {
                            out.write(" target");
                            {
                                boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                                if (!var_istrueattr126) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue128 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "noFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent129 = renderContext.call("xss", var_attrvalue128, "attribute");
                    {
                        boolean var_shoulddisplayattr131 = (((null != var_attrcontent129) && (!"".equals(var_attrcontent129))) && ((!"".equals(var_attrvalue128)) && (!((Object)false).equals(var_attrvalue128))));
                        if (var_shoulddisplayattr131) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr130 = (var_attrvalue128.equals(true));
                                if (!var_istrueattr130) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent129));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent132 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "linkText"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent132));
                out.write("\"");
            }
            {
                String var_attrcontent133 = (("{'webInteractions': {'name': 'Inspire Privilege','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "webInteractionType"), "attribute"))) + "'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent133));
                out.write("\"");
            }
            out.write(">");
            {
                String var_134 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "linkText"), "text"))) + "\r\n                                    ");
                out.write(renderContext.getObjectModel().toString(var_134));
            }
            out.write("<img class=\"info-link__icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar135 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions136_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions136 = obj().with("isEmpty", var_templateoptions136_field$_isempty);
            callUnit(out, renderContext, var_templatevar135, var_templateoptions136);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

