/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.structure.basepage;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class basepage__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_page = null;
Object _global_pwa = null;
Object _global_head = null;
Object _global_footer = null;
Object _global_redirect = null;
Object _global_pagemodel = null;
Object _global_analyticsmodel = null;
Object _global_xfpagemodel = null;
Object _dynamic_properties = bindings.get("properties");
Object _dynamic_inheritedpageproperties = bindings.get("inheritedpageproperties");
Object _dynamic_currentpage = bindings.get("currentpage");
Object _global_datalayerenabled = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _global_isredirectpage = null;
out.write("\r\n<!DOCTYPE HTML>\r\n");
_global_page = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Page.class.getName(), obj());
_global_pwa = renderContext.call("use", com.adobe.cq.wcm.core.components.models.PWA.class.getName(), obj());
_global_head = renderContext.call("use", "head.html", obj());
_global_footer = renderContext.call("use", "footer.html", obj());
_global_redirect = renderContext.call("use", "redirect.html", obj());
out.write("<html");
{
    Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_global_page, "language");
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" lang");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <head>");
{
    Object var_templatevar4 = renderContext.getObjectModel().resolveProperty(_global_head, "head");
    {
        Object var_templateoptions5_field$_page = _global_page;
        {
            Object var_templateoptions5_field$_pwa = _global_pwa;
            {
                java.util.Map var_templateoptions5 = obj().with("page", var_templateoptions5_field$_page).with("pwa", var_templateoptions5_field$_pwa);
                callUnit(out, renderContext, var_templatevar4, var_templateoptions5);
            }
        }
    }
}
out.write("</head>\r\n    ");
_global_pagemodel = renderContext.call("use", com.techcombank.core.models.AnalyticsPageModel.class.getName(), obj());
out.write("\r\n        ");
_global_analyticsmodel = renderContext.getObjectModel().resolveProperty(_global_pagemodel, "analyticsObj");
if (renderContext.getObjectModel().toBoolean(_global_analyticsmodel)) {
}
out.write("\r\n    \r\n    ");
_global_xfpagemodel = renderContext.call("use", com.techcombank.core.models.XFPageModel.class.getName(), obj());
out.write("\r\n    \r\n    ");
{
    boolean var_testvariable6 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_page, "templateName"), "blank-page"));
    if (var_testvariable6) {
        out.write("<header class=\"xf-header global-header\">\r\n        ");
        {
            Object var_testvariable7 = renderContext.getObjectModel().resolveProperty(_global_xfpagemodel, "headerXF");
            if (renderContext.getObjectModel().toBoolean(var_testvariable7)) {
                {
                    Object var_resourcecontent8 = renderContext.call("includeResource", null, obj().with("path", renderContext.getObjectModel().resolveProperty(_global_xfpagemodel, "headerXF")).with("selectors", "content").with("wcmmode", "disabled"));
                    out.write(renderContext.getObjectModel().toString(var_resourcecontent8));
                }
            }
        }
        out.write("\r\n    </header>");
    }
}
out.write("\r\n\r\n    <body");
{
    String var_attrcontent9 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_page, "cssClassNames"), "attribute")) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "pageTheme"), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent9));
    out.write("\"");
}
{
    Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_page, "id");
    {
        Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
        {
            boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
            if (var_shoulddisplayattr13) {
                out.write(" id");
                {
                    boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                    if (!var_istrueattr12) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue14 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "siteSection")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "siteSection") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "siteSection"));
    {
        Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
        {
            boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
            if (var_shoulddisplayattr17) {
                out.write(" data-tracking-site-section");
                {
                    boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                    if (!var_istrueattr16) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue18 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "environment")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "environment") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "environment"));
    {
        Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
        {
            boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
            if (var_shoulddisplayattr21) {
                out.write(" data-tracking-site-env");
                {
                    boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                    if (!var_istrueattr20) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue22 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "businessUnit")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "businessUnit") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "businessUnit"));
    {
        Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
        {
            boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
            if (var_shoulddisplayattr25) {
                out.write(" data-tracking-business-unit");
                {
                    boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                    if (!var_istrueattr24) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue26 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "platform")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "platform") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "platform"));
    {
        Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
        {
            boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
            if (var_shoulddisplayattr29) {
                out.write(" data-tracking-platform");
                {
                    boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                    if (!var_istrueattr28) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue30 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "productName")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "productName") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "productName"));
    {
        Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
        {
            boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
            if (var_shoulddisplayattr33) {
                out.write(" data-tracking-product-name");
                {
                    boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                    if (!var_istrueattr32) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue34 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "journey")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "journey") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "journey"));
    {
        Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
        {
            boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
            if (var_shoulddisplayattr37) {
                out.write(" data-tracking-journey");
                {
                    boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                    if (!var_istrueattr36) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue38 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "country")) ? renderContext.getObjectModel().resolveProperty(_global_analyticsmodel, "country") : renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "country"));
    {
        Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
        {
            boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
            if (var_shoulddisplayattr41) {
                out.write(" data-tracking-country");
                {
                    boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                    if (!var_istrueattr40) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-tracking-event='pageLoaded'");
{
    Object var_attrvalue42 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "title");
    {
        Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "attribute");
        {
            boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
            if (var_shoulddisplayattr45) {
                out.write(" data-tracking-page-name");
                {
                    boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                    if (!var_istrueattr44) {
                        out.write("='");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                        out.write("'");
                    }
                }
            }
        }
    }
}
{
    boolean var_attrvalue46 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_page, "data")) ? true : false);
    {
        Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
        {
            boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (false != var_attrvalue46)));
            if (var_shoulddisplayattr49) {
                out.write(" data-cmp-data-layer-enabled");
                {
                    boolean var_istrueattr48 = (var_attrvalue46 == true);
                    if (!var_istrueattr48) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n        ");
_global_datalayerenabled = renderContext.getObjectModel().resolveProperty(_global_page, "data");
if (renderContext.getObjectModel().toBoolean(_global_datalayerenabled)) {
    out.write("<script>");
    {
        String var_50 = (((("\r\n          window.adobeDataLayer = window.adobeDataLayer || [];\r\n          adobeDataLayer.push({\r\n              page: JSON.parse(\"" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_page, "data"), "json"), "scriptString"))) + "\"),\r\n              event:'cmp:show',\r\n              eventInfo: {\r\n                  path: 'page.") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_page, "id"), "scriptString"))) + "'\r\n              }\r\n          });\r\n        ");
        out.write(renderContext.getObjectModel().toString(var_50));
    }
    out.write("</script>");
}
out.write("\r\n        ");
_global_isredirectpage = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_page, "redirectTarget")) ? ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview"))) : renderContext.getObjectModel().resolveProperty(_global_page, "redirectTarget")));
if (renderContext.getObjectModel().toBoolean(_global_isredirectpage)) {
    {
        Object var_templatevar51 = renderContext.getObjectModel().resolveProperty(_global_redirect, "redirect");
        {
            Object var_templateoptions52_field$_redirecttarget = renderContext.getObjectModel().resolveProperty(_global_page, "redirectTarget");
            {
                java.util.Map var_templateoptions52 = obj().with("redirectTarget", var_templateoptions52_field$_redirecttarget);
                callUnit(out, renderContext, var_templatevar51, var_templateoptions52);
            }
        }
    }
}
out.write("\r\n        ");
{
    boolean var_testvariable53 = (!renderContext.getObjectModel().toBoolean(_global_isredirectpage));
    if (var_testvariable53) {
        out.write("\r\n            ");
        {
            Object var_includedresult54 = renderContext.call("include", "body.skiptomaincontent.html", obj());
            out.write(renderContext.getObjectModel().toString(var_includedresult54));
        }
        out.write("\r\n            ");
        {
            Object var_includedresult56 = renderContext.call("include", "body.html", obj());
            out.write(renderContext.getObjectModel().toString(var_includedresult56));
        }
        out.write("\r\n            ");
        {
            Object var_templatevar58 = renderContext.getObjectModel().resolveProperty(_global_footer, "footer");
            {
                Object var_templateoptions59_field$_page = _global_page;
                {
                    Object var_templateoptions59_field$_pwa = _global_pwa;
                    {
                        java.util.Map var_templateoptions59 = obj().with("page", var_templateoptions59_field$_page).with("pwa", var_templateoptions59_field$_pwa);
                        callUnit(out, renderContext, var_templatevar58, var_templateoptions59);
                    }
                }
            }
        }
        out.write("\r\n        ");
    }
}
out.write("\r\n    </body>\r\n    ");
{
    Object var_testvariable60 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_xfpagemodel, "footerXF")) ? (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_page, "templateName"), "blank-page")) : renderContext.getObjectModel().resolveProperty(_global_xfpagemodel, "footerXF")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable60)) {
        {
            Object var_resourcecontent61 = renderContext.call("includeResource", null, obj().with("path", renderContext.getObjectModel().resolveProperty(_global_xfpagemodel, "footerXF")).with("selectors", "content").with("wcmmode", "disabled"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent61));
        }
    }
}
out.write("\r\n</html>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

