/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.structure.basepage;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class head__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_head = getProperty("head");
out.write("\r\n");
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------

/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
addSubTemplate("head", new RenderUnit() {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Sub-Template Body -------------------------------------------------------------------------

Object _global_headlibrenderer = null;
Object _global_headresources = null;
Object _dynamic_page = arguments.get("page");
Object _global_keywords = null;
Object _global_templatename = null;
Object _global_page = null;
Object _global_seopage = null;
Collection var_collectionvar14_list_coerced$ = null;
Object _global_seopagemodel = null;
Object _dynamic_properties = bindings.get("properties");
Object _global_title = null;
Object _global_metadesc = null;
Object _global_metakey = null;
Object _global_metaimg = null;
Object _global_ogtitle = null;
Object _global_ogdescription = null;
Object _global_ogtype = null;
Object _dynamic_inheritedpageproperties = bindings.get("inheritedpageproperties");
Object _global_ogimage = null;
Object _global_ogsitename = null;
Object _dynamic_pwa = arguments.get("pwa");
Object _global_clientlib = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _global_appresourcespath = null;
Collection var_collectionvar106_list_coerced$ = null;
Collection var_attrmap117_list_coerced$ = null;
_global_headlibrenderer = renderContext.call("use", "headlibs.html", obj());
_global_headresources = renderContext.call("use", "head.resources.html", obj());
out.write("\r\n    <meta charset=\"UTF-8\"/>\r\n    <title>");
{
    String var_0 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_page, "title"), "text")) + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_page, "brandSlug")) ? " | " : ""), "text"))) + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_page, "brandSlug"), "text")));
    out.write(renderContext.getObjectModel().toString(var_0));
}
out.write("</title>\r\n    ");
_global_keywords = renderContext.getObjectModel().resolveProperty(_dynamic_page, "keywords");
if (renderContext.getObjectModel().toBoolean(_global_keywords)) {
    out.write("<meta name=\"keywords\"");
    {
        Object var_attrvalue1 = _global_keywords;
        {
            Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
            {
                boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                if (var_shoulddisplayattr4) {
                    out.write(" content");
                    {
                        boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                        if (!var_istrueattr3) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>");
}
out.write("\r\n    ");
_global_templatename = renderContext.getObjectModel().resolveProperty(_dynamic_page, "templateName");
if (renderContext.getObjectModel().toBoolean(_global_templatename)) {
    out.write("<meta name=\"template\"");
    {
        Object var_attrvalue5 = _global_templatename;
        {
            Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
            {
                boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                if (var_shoulddisplayattr8) {
                    out.write(" content");
                    {
                        boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                        if (!var_istrueattr7) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>");
}
out.write("\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>\r\n    ");
{
    Object var_testvariable9 = renderContext.getObjectModel().resolveProperty(_dynamic_page, "robotsTags");
    if (renderContext.getObjectModel().toBoolean(var_testvariable9)) {
        out.write("<meta name=\"robots\"");
        {
            Object var_attrvalue10 = renderContext.call("join", renderContext.getObjectModel().resolveProperty(_dynamic_page, "robotsTags"), ", ");
            {
                Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                {
                    boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                    if (var_shoulddisplayattr13) {
                        out.write(" content");
                        {
                            boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                            if (!var_istrueattr12) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
}
out.write("\r\n    ");
_global_page = renderContext.call("use", "com.adobe.cq.wcm.core.components.models.Page", obj());
out.write("\r\n    ");
_global_seopage = renderContext.call("use", "com.techcombank.core.models.SEOPageModel", obj());
out.write("\r\n        ");
{
    Object var_collectionvar14 = renderContext.getObjectModel().resolveProperty(_global_seopage, "hrefLangList");
    {
        long var_size15 = ((var_collectionvar14_list_coerced$ == null ? (var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14)) : var_collectionvar14_list_coerced$).size());
        {
            boolean var_notempty16 = (var_size15 > 0);
            if (var_notempty16) {
                {
                    long var_end19 = var_size15;
                    {
                        boolean var_validstartstepend20 = (((0 < var_size15) && true) && (var_end19 > 0));
                        if (var_validstartstepend20) {
                            if (var_collectionvar14_list_coerced$ == null) {
                                var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14);
                            }
                            long var_index21 = 0;
                            for (Object hreflang : var_collectionvar14_list_coerced$) {
                                {
                                    boolean var_traversal23 = (((var_index21 >= 0) && (var_index21 <= var_end19)) && true);
                                    if (var_traversal23) {
                                        out.write("\r\n            <link");
                                        {
                                            Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(hreflang, "rel");
                                            {
                                                Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                                                    if (var_shoulddisplayattr27) {
                                                        out.write(" rel");
                                                        {
                                                            boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                                            if (!var_istrueattr26) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(hreflang, "hrefLang");
                                            {
                                                Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                                                    if (var_shoulddisplayattr31) {
                                                        out.write(" hreflang");
                                                        {
                                                            boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                                                            if (!var_istrueattr30) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(hreflang, "href");
                                            {
                                                Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "uri");
                                                {
                                                    boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                                    if (var_shoulddisplayattr35) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                                            if (!var_istrueattr34) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n        ");
                                    }
                                }
                                var_index21++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar14_list_coerced$ = null;
}
out.write("\r\n        ");
_global_seopagemodel = renderContext.getObjectModel().resolveProperty(_global_seopage, "seoObj");
if (renderContext.getObjectModel().toBoolean(_global_seopagemodel)) {
    out.write("\r\n            ");
_global_title = ((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "seoTitle")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "seoTitle") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageTitle")))) ? ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "seoTitle")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "seoTitle") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageTitle"))) : renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:title")));
    if (renderContext.getObjectModel().toBoolean(_global_title)) {
        out.write("<meta name=\"title\"");
        {
            Object var_attrvalue36 = _global_title;
            {
                Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                {
                    boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                    if (var_shoulddisplayattr39) {
                        out.write(" content");
                        {
                            boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                            if (!var_istrueattr38) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_metadesc = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "metaDescription")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "metaDescription") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageDescription")));
    if (renderContext.getObjectModel().toBoolean(_global_metadesc)) {
        out.write("<meta name=\"description\"");
        {
            Object var_attrvalue40 = _global_metadesc;
            {
                Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                {
                    boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                    if (var_shoulddisplayattr43) {
                        out.write(" content");
                        {
                            boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                            if (!var_istrueattr42) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_metakey = renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "metaKeyword");
    if (renderContext.getObjectModel().toBoolean(_global_metakey)) {
        out.write("<meta name=\"keywords\"");
        {
            Object var_attrvalue44 = _global_metakey;
            {
                Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                {
                    boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                    if (var_shoulddisplayattr47) {
                        out.write(" content");
                        {
                            boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                            if (!var_istrueattr46) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n\t    ");
_global_metaimg = renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "metaImage");
    if (renderContext.getObjectModel().toBoolean(_global_metaimg)) {
        out.write("<meta name=\"image\"");
        {
            Object var_attrvalue48 = _global_metaimg;
            {
                Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
                {
                    boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                    if (var_shoulddisplayattr51) {
                        out.write(" content");
                        {
                            boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                            if (!var_istrueattr50) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_ogtitle = ((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphTitle")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphTitle") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageTitle")))) ? ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphTitle")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphTitle") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageTitle"))) : renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:title")));
    if (renderContext.getObjectModel().toBoolean(_global_ogtitle)) {
        out.write("<meta property=\"og:title\"");
        {
            Object var_attrvalue52 = _global_ogtitle;
            {
                Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
                {
                    boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                    if (var_shoulddisplayattr55) {
                        out.write(" content");
                        {
                            boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                            if (!var_istrueattr54) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_ogdescription = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphDescription")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphDescription") : renderContext.getObjectModel().resolveProperty(_global_seopage, "pageDescription")));
    if (renderContext.getObjectModel().toBoolean(_global_ogdescription)) {
        out.write("<meta property=\"og:description\"");
        {
            Object var_attrvalue56 = _global_ogdescription;
            {
                Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
                {
                    boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                    if (var_shoulddisplayattr59) {
                        out.write(" content");
                        {
                            boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                            if (!var_istrueattr58) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_ogtype = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphType")) ? renderContext.getObjectModel().resolveProperty(_global_seopagemodel, "openGraphType") : "website"));
    if (renderContext.getObjectModel().toBoolean(_global_ogtype)) {
        out.write("<meta property=\"og:type\"");
        {
            Object var_attrvalue60 = _global_ogtype;
            {
                Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "attribute");
                {
                    boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                    if (var_shoulddisplayattr63) {
                        out.write(" content");
                        {
                            boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                            if (!var_istrueattr62) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_ogimage = renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "openGraphImage");
    if (renderContext.getObjectModel().toBoolean(_global_ogimage)) {
        out.write("<meta property=\"og:image\"");
        {
            Object var_attrvalue64 = _global_ogimage;
            {
                Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                {
                    boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                    if (var_shoulddisplayattr67) {
                        out.write(" content");
                        {
                            boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                            if (!var_istrueattr66) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            ");
_global_ogsitename = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "openGraphSiteName")) ? renderContext.getObjectModel().resolveProperty(_dynamic_inheritedpageproperties, "openGraphSiteName") : renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:title")));
    if (renderContext.getObjectModel().toBoolean(_global_ogsitename)) {
        out.write("<meta property=\"og:sitename\"");
        {
            Object var_attrvalue68 = _global_ogsitename;
            {
                Object var_attrcontent69 = renderContext.call("xss", var_attrvalue68, "attribute");
                {
                    boolean var_shoulddisplayattr71 = (((null != var_attrcontent69) && (!"".equals(var_attrcontent69))) && ((!"".equals(var_attrvalue68)) && (!((Object)false).equals(var_attrvalue68))));
                    if (var_shoulddisplayattr71) {
                        out.write(" content");
                        {
                            boolean var_istrueattr70 = (var_attrvalue68.equals(true));
                            if (!var_istrueattr70) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent69));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
    out.write("\r\n\r\n            <meta property=\"og:url\"");
    {
        Object var_attrvalue72 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_seopage, "pagePath"), "uri");
        {
            boolean var_shoulddisplayattr75 = ((!"".equals(var_attrvalue72)) && (!((Object)false).equals(var_attrvalue72)));
            if (var_shoulddisplayattr75) {
                out.write(" content");
                {
                    boolean var_istrueattr74 = (var_attrvalue72.equals(true));
                    if (!var_istrueattr74) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrvalue72));
                        out.write("\"");
                    }
                }
            }
        }
    }
    out.write("/>\r\n            <link rel=\"canonical\"");
    {
        Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(_global_page, "canonicalLink");
        {
            Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "uri");
            {
                boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                if (var_shoulddisplayattr79) {
                    out.write(" href");
                    {
                        boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                        if (!var_istrueattr78) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n        ");
}
out.write("\r\n    \r\n\r\n    ");
{
    Object var_testvariable80 = renderContext.getObjectModel().resolveProperty(_dynamic_pwa, "enabled");
    if (renderContext.getObjectModel().toBoolean(var_testvariable80)) {
        out.write("\r\n        <link rel=\"manifest\"");
        {
            Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_dynamic_pwa, "manifestPath");
            {
                Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "uri");
                {
                    boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
                    if (var_shoulddisplayattr84) {
                        out.write(" href");
                        {
                            boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                            if (!var_istrueattr83) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" crossorigin=\"use-credentials\"/>\r\n        <meta name=\"theme-color\"");
        {
            Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(_dynamic_pwa, "themeColor");
            {
                Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "attribute");
                {
                    boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
                    if (var_shoulddisplayattr88) {
                        out.write(" content");
                        {
                            boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                            if (!var_istrueattr87) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n        <link rel=\"apple-touch-icon\"");
        {
            Object var_attrvalue89 = renderContext.getObjectModel().resolveProperty(_dynamic_pwa, "iconPath");
            {
                Object var_attrcontent90 = renderContext.call("xss", var_attrvalue89, "uri");
                {
                    boolean var_shoulddisplayattr92 = (((null != var_attrcontent90) && (!"".equals(var_attrcontent90))) && ((!"".equals(var_attrvalue89)) && (!((Object)false).equals(var_attrvalue89))));
                    if (var_shoulddisplayattr92) {
                        out.write(" href");
                        {
                            boolean var_istrueattr91 = (var_attrvalue89.equals(true));
                            if (!var_istrueattr91) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n        ");
_global_clientlib = renderContext.call("use", "core/wcm/components/commons/v1/templates/clientlib.html", obj());
        {
            Object var_templatevar93 = renderContext.getObjectModel().resolveProperty(_global_clientlib, "css");
            {
                String var_templateoptions94_field$_categories = "core.wcm.components.page.v2.pwa";
                {
                    java.util.Map var_templateoptions94 = obj().with("categories", var_templateoptions94_field$_categories);
                    callUnit(out, renderContext, var_templatevar93, var_templateoptions94);
                }
            }
        }
        out.write("\r\n        <meta name=\"cq:sw_path\"");
        {
            Object var_attrvalue95 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_pwa, "serviceWorkerPath"), "text");
            {
                boolean var_shoulddisplayattr98 = ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95)));
                if (var_shoulddisplayattr98) {
                    out.write(" content");
                    {
                        boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                        if (!var_istrueattr97) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrvalue95));
                            out.write("\"");
                        }
                    }
                }
            }
        }
        out.write("/>\r\n    ");
    }
}
out.write("\r\n    ");
{
    Object var_includedresult99 = renderContext.call("include", "customheaderlibs.html", obj());
    out.write(renderContext.getObjectModel().toString(var_includedresult99));
}
out.write("\r\n    ");
{
    Object var_testvariable101 = (((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_page, "templateName"), "blank-page")) ? (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_page, "templateName"), "blank-page")) : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable101)) {
        {
            Object var_templatevar102 = renderContext.getObjectModel().resolveProperty(_global_headlibrenderer, "headlibs");
            {
                Object var_templateoptions103_field$_clientlibcategories = renderContext.getObjectModel().resolveProperty(_global_page, "clientLibCategories");
                {
                    Object var_templateoptions103_field$_hascloudconfigsupport = renderContext.getObjectModel().resolveProperty(_global_page, "hasCloudconfigSupport");
                    {
                        Object var_templateoptions103_field$_clientlibcategoriesjshead = renderContext.getObjectModel().resolveProperty(_global_page, "clientLibCategoriesJsHead");
                        {
                            Object var_templateoptions103_field$_staticdesignpath = renderContext.getObjectModel().resolveProperty(_global_page, "staticDesignPath");
                            {
                                Object var_templateoptions103_field$_page = _global_page;
                                {
                                    Object var_templateoptions103_field$_designpath = renderContext.getObjectModel().resolveProperty(_global_page, "designPath");
                                    {
                                        java.util.Map var_templateoptions103 = obj().with("clientLibCategories", var_templateoptions103_field$_clientlibcategories).with("hasCloudconfigSupport", var_templateoptions103_field$_hascloudconfigsupport).with("clientLibCategoriesJsHead", var_templateoptions103_field$_clientlibcategoriesjshead).with("staticDesignPath", var_templateoptions103_field$_staticdesignpath).with("page", var_templateoptions103_field$_page).with("designPath", var_templateoptions103_field$_designpath);
                                        callUnit(out, renderContext, var_templatevar102, var_templateoptions103);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
out.write("\r\n    ");
_global_appresourcespath = renderContext.getObjectModel().resolveProperty(_global_page, "appResourcesPath");
if (renderContext.getObjectModel().toBoolean(_global_appresourcespath)) {
    {
        Object var_templatevar104 = renderContext.getObjectModel().resolveProperty(_global_headresources, "favicons");
        {
            Object var_templateoptions105_field$_path = _global_appresourcespath;
            {
                java.util.Map var_templateoptions105 = obj().with("path", var_templateoptions105_field$_path);
                callUnit(out, renderContext, var_templatevar104, var_templateoptions105);
            }
        }
    }
}
out.write("\r\n    ");
{
    Object var_collectionvar106 = renderContext.getObjectModel().resolveProperty(_global_page, "htmlPageItems");
    {
        long var_size107 = ((var_collectionvar106_list_coerced$ == null ? (var_collectionvar106_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar106)) : var_collectionvar106_list_coerced$).size());
        {
            boolean var_notempty108 = (var_size107 > 0);
            if (var_notempty108) {
                {
                    long var_end111 = var_size107;
                    {
                        boolean var_validstartstepend112 = (((0 < var_size107) && true) && (var_end111 > 0));
                        if (var_validstartstepend112) {
                            if (var_collectionvar106_list_coerced$ == null) {
                                var_collectionvar106_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar106);
                            }
                            long var_index113 = 0;
                            for (Object item : var_collectionvar106_list_coerced$) {
                                {
                                    boolean var_traversal115 = (((var_index113 >= 0) && (var_index113 <= var_end111)) && true);
                                    if (var_traversal115) {
                                        {
                                            boolean var_testvariable118 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(item, "location"), "name"), "header"));
                                            if (var_testvariable118) {
                                                {
                                                    Object var_tagvar116 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(item, "element"), "name"), "text");
                                                    if (renderContext.getObjectModel().toBoolean(var_tagvar116)) {
                                                        out.write("<");
                                                        out.write(renderContext.getObjectModel().toString(var_tagvar116));
                                                    }
                                                    if (!renderContext.getObjectModel().toBoolean(var_tagvar116)) {
                                                        out.write("<script");
                                                    }
                                                    {
                                                        Object var_attrmap117 = renderContext.getObjectModel().resolveProperty(item, "attributes");
                                                        {
                                                            java.util.Map var_ignoredattributes119 = obj();
                                                            if (var_attrmap117_list_coerced$ == null) {
                                                                var_attrmap117_list_coerced$ = renderContext.getObjectModel().toCollection(var_attrmap117);
                                                            }
                                                            long var_attrindex122 = 0;
                                                            for (Object var_attrname120 : var_attrmap117_list_coerced$) {
                                                                {
                                                                    Object var_attrnameescaped121 = renderContext.call("xss", var_attrname120, "attributeName");
                                                                    if (renderContext.getObjectModel().toBoolean(var_attrnameescaped121)) {
                                                                        {
                                                                            Object var_isignoredattr123 = var_ignoredattributes119.get(var_attrname120);
                                                                            if (!renderContext.getObjectModel().toBoolean(var_isignoredattr123)) {
                                                                                {
                                                                                    Object var_attrcontent124 = renderContext.getObjectModel().resolveProperty(var_attrmap117, var_attrname120);
                                                                                    {
                                                                                        Object var_attrcontentescaped125 = renderContext.call("xss", var_attrcontent124, "attribute", var_attrnameescaped121);
                                                                                        {
                                                                                            boolean var_shoulddisplayattr126 = (((null != var_attrcontentescaped125) && (!"".equals(var_attrcontentescaped125))) && ((!"".equals(var_attrcontent124)) && (!((Object)false).equals(var_attrcontent124))));
                                                                                            if (var_shoulddisplayattr126) {
                                                                                                out.write(" ");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrnameescaped121));
                                                                                                {
                                                                                                    boolean var_istrueattr127 = (var_attrcontent124.equals(true));
                                                                                                    if (!var_istrueattr127) {
                                                                                                        out.write("=\"");
                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontentescaped125));
                                                                                                        out.write("\"");
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                var_attrindex122++;
                                                            }
                                                        }
                                                        var_attrmap117_list_coerced$ = null;
                                                    }
                                                    out.write(">");
                                                    if (renderContext.getObjectModel().toBoolean(var_tagvar116)) {
                                                        out.write("</");
                                                        out.write(renderContext.getObjectModel().toString(var_tagvar116));
                                                        out.write(">");
                                                    }
                                                    if (!renderContext.getObjectModel().toBoolean(var_tagvar116)) {
                                                        out.write("</script>");
                                                    }
                                                }
                                            }
                                        }
                                        out.write(" ");
                                    }
                                }
                                var_index113++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar106_list_coerced$ = null;
}
out.write("\r\n");


// End Of Main Sub-Template Body ------------------------------------------------------------------
    }



    {
//Sub-Sub-Templates Initialization ----------------------------------------------------------------



//End of Sub-Sub-Templates Initialization ---------------------------------------------------------
    }
    
});


//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

