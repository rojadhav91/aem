/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.listviewdocument;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class listviewdocument__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_listviewdocument = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar13_list_coerced$ = null;
Collection var_collectionvar35_list_coerced$ = null;
Collection var_collectionvar46_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
Collection var_collectionvar129_list_coerced$ = null;
_global_listviewdocument = renderContext.call("use", com.techcombank.core.models.ListViewDocumentModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "tabItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"container list-view-documents inheritance checkbox-info-table-component section__margin-medium\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "numberOfRecords");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" data-viewmore-number");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "filterPlaceholderLabel");
                {
                    Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                    {
                        boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                        if (var_shoulddisplayattr8) {
                            out.write(" data-filter-placeholder");
                            {
                                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                if (!var_istrueattr7) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <div class=\"service-fee-container\">\r\n                <!-- filter reuse -->\r\n                <div class=\"select-checkbox-filter\">\r\n                    <h2>");
            {
                Object var_9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "title"), "text");
                out.write(renderContext.getObjectModel().toString(var_9));
            }
            out.write("</h2>\r\n                    <!-- START FILTER -->\r\n                    ");
            {
                boolean var_testvariable10 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "hideFilter")));
                if (var_testvariable10) {
                    out.write("\r\n                        <div class=\"select-options\">\r\n                            <div class=\"select\">\r\n                                <img class=\"calendar-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar.svg\"/>\r\n                                <h6>");
                    {
                        Object var_11 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "yearColonLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_11));
                    }
                    out.write("</h6>\r\n                                <span></span>\r\n                                <img class=\"fa-chevron-down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                            </div>\r\n                            <div class=\"checkbox\">\r\n                                <div class=\"checkbox-list\">\r\n                                    <div class=\"type\">\r\n                                        <div class=\"year\">\r\n                                            <p>");
                    {
                        Object var_12 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "yearLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_12));
                    }
                    out.write("</p>\r\n                                            <ul>\r\n                                                ");
                    {
                        Object var_collectionvar13 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "yearList");
                        {
                            long var_size14 = ((var_collectionvar13_list_coerced$ == null ? (var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13)) : var_collectionvar13_list_coerced$).size());
                            {
                                boolean var_notempty15 = (var_size14 > 0);
                                if (var_notempty15) {
                                    {
                                        long var_end18 = var_size14;
                                        {
                                            boolean var_validstartstepend19 = (((0 < var_size14) && true) && (var_end18 > 0));
                                            if (var_validstartstepend19) {
                                                if (var_collectionvar13_list_coerced$ == null) {
                                                    var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13);
                                                }
                                                long var_index20 = 0;
                                                for (Object tab : var_collectionvar13_list_coerced$) {
                                                    {
                                                        boolean var_traversal22 = (((var_index20 >= 0) && (var_index20 <= var_end18)) && true);
                                                        if (var_traversal22) {
                                                            out.write("\r\n                                                    <li>\r\n                                                        <label>\r\n                                                            <input type=\"checkbox\"");
                                                            {
                                                                Object var_attrvalue23 = tab;
                                                                {
                                                                    Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                                                                        if (var_shoulddisplayattr26) {
                                                                            out.write(" value");
                                                                            {
                                                                                boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                                                                if (!var_istrueattr25) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write("/>\r\n                                                            <div>");
                                                            {
                                                                Object var_27 = renderContext.call("xss", tab, "text");
                                                                out.write(renderContext.getObjectModel().toString(var_27));
                                                            }
                                                            out.write("</div>\r\n                                                        </label>\r\n                                                    </li>\r\n                                                ");
                                                        }
                                                    }
                                                    var_index20++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar13_list_coerced$ = null;
                    }
                    out.write("\r\n                                            </ul>\r\n                                        </div>\r\n                                        ");
                    {
                        Object var_testvariable28 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "quarterLabel");
                        if (renderContext.getObjectModel().toBoolean(var_testvariable28)) {
                            out.write("<div class=\"quarter\">\r\n                                            <p>");
                            {
                                Object var_29 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "quarterLabel"), "text");
                                out.write(renderContext.getObjectModel().toString(var_29));
                            }
                            out.write("</p>\r\n                                            <ul>\r\n                                                <li>\r\n                                                    <label>\r\n                                                        <input type=\"checkbox\" value=\"Q1\"/>\r\n                                                        <div>Q1</div>\r\n                                                    </label>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <label>\r\n                                                        <input type=\"checkbox\" value=\"Q2\"/>\r\n                                                        <div>Q2</div>\r\n                                                    </label>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <label>\r\n                                                        <input type=\"checkbox\" value=\"Q3\"/>\r\n                                                        <div>Q3</div>\r\n                                                    </label>\r\n                                                </li>\r\n                                                <li>\r\n                                                    <label>\r\n                                                        <input type=\"checkbox\" value=\"Q4\"/>\r\n                                                        <div>Q4</div>\r\n                                                    </label>\r\n                                                </li>\r\n                                            </ul>\r\n                                        </div>");
                        }
                    }
                    out.write("\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <!-- END FILTER -->\r\n                        <div class=\"popup\"></div>\r\n                        <div class=\"popup-content\">\r\n                            <div class=\"title\"></div>\r\n                            <div class=\"content\"></div>\r\n                            <div class=\"btn\">\r\n                                <button>");
                    {
                        Object var_30 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "applyLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_30));
                    }
                    out.write("</button>\r\n                            </div>\r\n                        </div>\r\n                    ");
                }
            }
            out.write("\r\n                </div>\r\n                <div class=\"tab-content\">\r\n                    <div class=\"menu tcb-tabs\"");
            {
                Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "tabs");
                {
                    Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                    {
                        boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                        if (var_shoulddisplayattr34) {
                            out.write(" data-tabs");
                            {
                                boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                if (!var_istrueattr33) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n                    </div>\r\n                    <div class=\"list-items\">\r\n                        ");
            {
                Object var_collectionvar35 = renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "tabItems");
                {
                    long var_size36 = ((var_collectionvar35_list_coerced$ == null ? (var_collectionvar35_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar35)) : var_collectionvar35_list_coerced$).size());
                    {
                        boolean var_notempty37 = (var_size36 > 0);
                        if (var_notempty37) {
                            {
                                long var_end40 = var_size36;
                                {
                                    boolean var_validstartstepend41 = (((0 < var_size36) && true) && (var_end40 > 0));
                                    if (var_validstartstepend41) {
                                        if (var_collectionvar35_list_coerced$ == null) {
                                            var_collectionvar35_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar35);
                                        }
                                        long var_index42 = 0;
                                        for (Object tab : var_collectionvar35_list_coerced$) {
                                            {
                                                boolean tablist_field$_first = (var_index42 == 0);
                                                {
                                                    boolean var_traversal44 = (((var_index42 >= 0) && (var_index42 <= var_end40)) && true);
                                                    if (var_traversal44) {
                                                        out.write("\r\n                            <div class=\"tab\"");
                                                        {
                                                            String var_attrcontent45 = ("display: " + renderContext.getObjectModel().toString(renderContext.call("xss", (tablist_field$_first ? "block" : "none"), "html")));
                                                            out.write(" style=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n                                ");
                                                        {
                                                            Object var_collectionvar46 = renderContext.getObjectModel().resolveProperty(tab, "listViewDocumentItemsFirstLevel");
                                                            {
                                                                long var_size47 = ((var_collectionvar46_list_coerced$ == null ? (var_collectionvar46_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar46)) : var_collectionvar46_list_coerced$).size());
                                                                {
                                                                    boolean var_notempty48 = (var_size47 > 0);
                                                                    if (var_notempty48) {
                                                                        {
                                                                            long var_end51 = var_size47;
                                                                            {
                                                                                boolean var_validstartstepend52 = (((0 < var_size47) && true) && (var_end51 > 0));
                                                                                if (var_validstartstepend52) {
                                                                                    if (var_collectionvar46_list_coerced$ == null) {
                                                                                        var_collectionvar46_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar46);
                                                                                    }
                                                                                    long var_index53 = 0;
                                                                                    for (Object itemlevel1 : var_collectionvar46_list_coerced$) {
                                                                                        {
                                                                                            boolean var_traversal55 = (((var_index53 >= 0) && (var_index53 <= var_end51)) && true);
                                                                                            if (var_traversal55) {
                                                                                                out.write("\r\n                                    <div class=\"row\">\r\n                                        <div class=\"date\">\r\n                                            <span>");
                                                                                                {
                                                                                                    Object var_56 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "formattedDate"), "text");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_56));
                                                                                                }
                                                                                                out.write("</span>\r\n                                        </div>\r\n                                        <div class=\"content\">\r\n                                            <p>");
                                                                                                {
                                                                                                    Object var_57 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "documentTitle"), "text");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_57));
                                                                                                }
                                                                                                out.write("</p>\r\n                                        </div>\r\n                                        <div class=\"file-download\">\r\n                                            ");
                                                                                                {
                                                                                                    boolean var_testvariable58 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(itemlevel1, "listViewDocumentItemsSecondLevel")));
                                                                                                    if (var_testvariable58) {
                                                                                                        out.write("\r\n                                                <div class=\"item have-file-name\">\r\n                                                    <div class=\"file-name\"></div>\r\n                                                    ");
                                                                                                        {
                                                                                                            Object var_testvariable59 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(itemlevel1, "icon")) ? renderContext.getObjectModel().resolveProperty(itemlevel1, "icon") : renderContext.getObjectModel().resolveProperty(itemlevel1, "label")));
                                                                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable59)) {
                                                                                                                out.write("<div class=\"btn\">\r\n                                                        ");
                                                                                                                {
                                                                                                                    boolean var_testvariable60 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(itemlevel1, "typeOfOutput"), "seeMore"));
                                                                                                                    if (var_testvariable60) {
                                                                                                                        out.write("\r\n                                                            <input type=\"hidden\" data-tracking-click-event=\"linkClick\"");
                                                                                                                        {
                                                                                                                            String var_attrcontent61 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "label"), "attribute"))) + "'}");
                                                                                                                            out.write(" data-tracking-click-info-value=\"");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                                                                                                            out.write("\"");
                                                                                                                        }
                                                                                                                        {
                                                                                                                            String var_attrcontent62 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "webInteractionType"), "attribute"))) + "'}}");
                                                                                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                                                                            out.write("\"");
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                            <a class=\"link\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue63 = renderContext.getObjectModel().resolveProperty(itemlevel1, "typeOfOutput");
                                                                                                                            {
                                                                                                                                Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                                                                                                                                    if (var_shoulddisplayattr66) {
                                                                                                                                        out.write(" data-file-type");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                                                                                                                                            if (!var_istrueattr65) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        {
                                                                                                                            Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(itemlevel1, "documentPath");
                                                                                                                            {
                                                                                                                                Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                                                                                                                                    if (var_shoulddisplayattr70) {
                                                                                                                                        out.write(" data-file-link");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                                                                                                                            if (!var_istrueattr69) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        {
                                                                                                                            Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(itemlevel1, "documentPath");
                                                                                                                            {
                                                                                                                                Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "uri");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                                                                                                                                    if (var_shoulddisplayattr74) {
                                                                                                                                        out.write(" href");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                                                                                                                            if (!var_istrueattr73) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write(">\r\n                                                                <span>");
                                                                                                                        {
                                                                                                                            Object var_75 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "label"), "text");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_75));
                                                                                                                        }
                                                                                                                        out.write("</span>\r\n                                                                <picture>\r\n                                                                    <source media=\"(max-width: 360px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(itemlevel1, "mobileIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                                                                                                                                    if (var_shoulddisplayattr79) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                                                                                                                                            if (!var_istrueattr78) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(max-width: 576px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue80 = renderContext.getObjectModel().resolveProperty(itemlevel1, "mobileIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent81 = renderContext.call("xss", var_attrvalue80, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr83 = (((null != var_attrcontent81) && (!"".equals(var_attrcontent81))) && ((!"".equals(var_attrvalue80)) && (!((Object)false).equals(var_attrvalue80))));
                                                                                                                                    if (var_shoulddisplayattr83) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr82 = (var_attrvalue80.equals(true));
                                                                                                                                            if (!var_istrueattr82) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent81));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(min-width: 1080px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue84 = renderContext.getObjectModel().resolveProperty(itemlevel1, "webIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent85 = renderContext.call("xss", var_attrvalue84, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr87 = (((null != var_attrcontent85) && (!"".equals(var_attrcontent85))) && ((!"".equals(var_attrvalue84)) && (!((Object)false).equals(var_attrvalue84))));
                                                                                                                                    if (var_shoulddisplayattr87) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr86 = (var_attrvalue84.equals(true));
                                                                                                                                            if (!var_istrueattr86) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent85));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(min-width: 1200px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue88 = renderContext.getObjectModel().resolveProperty(itemlevel1, "webIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent89 = renderContext.call("xss", var_attrvalue88, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr91 = (((null != var_attrcontent89) && (!"".equals(var_attrcontent89))) && ((!"".equals(var_attrvalue88)) && (!((Object)false).equals(var_attrvalue88))));
                                                                                                                                    if (var_shoulddisplayattr91) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr90 = (var_attrvalue88.equals(true));
                                                                                                                                            if (!var_istrueattr90) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <img");
                                                                                                                        {
                                                                                                                            Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(itemlevel1, "icon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "uri");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                                                                                    if (var_shoulddisplayattr95) {
                                                                                                                                        out.write(" src");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                                                                                            if (!var_istrueattr94) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write(" alt=\"icon\"/>\r\n                                                                </picture>\r\n                                                            </a>\r\n                                                        ");
                                                                                                                    }
                                                                                                                }
                                                                                                                out.write("\r\n                                                        ");
                                                                                                                {
                                                                                                                    boolean var_testvariable96 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(itemlevel1, "typeOfOutput"), "seeMore"));
                                                                                                                    if (var_testvariable96) {
                                                                                                                        out.write("\r\n                                                            <a class=\"link\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue97 = renderContext.getObjectModel().resolveProperty(itemlevel1, "typeOfOutput");
                                                                                                                            {
                                                                                                                                Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                                                                                                                                    if (var_shoulddisplayattr100) {
                                                                                                                                        out.write(" data-file-type");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                                                                                                                                            if (!var_istrueattr99) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        {
                                                                                                                            Object var_attrvalue101 = renderContext.getObjectModel().resolveProperty(itemlevel1, "documentPath");
                                                                                                                            {
                                                                                                                                Object var_attrcontent102 = renderContext.call("xss", var_attrvalue101, "uri");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr104 = (((null != var_attrcontent102) && (!"".equals(var_attrcontent102))) && ((!"".equals(var_attrvalue101)) && (!((Object)false).equals(var_attrvalue101))));
                                                                                                                                    if (var_shoulddisplayattr104) {
                                                                                                                                        out.write(" href");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr103 = (var_attrvalue101.equals(true));
                                                                                                                                            if (!var_istrueattr103) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                                                        {
                                                                                                                            String var_attrcontent105 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "label"), "attribute"))) + "'}");
                                                                                                                            out.write(" data-tracking-click-info-value=\"");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                                                                                                            out.write("\"");
                                                                                                                        }
                                                                                                                        {
                                                                                                                            String var_attrcontent106 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "webInteractionType"), "attribute"))) + "'}}");
                                                                                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent106));
                                                                                                                            out.write("\"");
                                                                                                                        }
                                                                                                                        out.write(">\r\n                                                                <span>");
                                                                                                                        {
                                                                                                                            Object var_107 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemlevel1, "label"), "text");
                                                                                                                            out.write(renderContext.getObjectModel().toString(var_107));
                                                                                                                        }
                                                                                                                        out.write("</span>\r\n                                                                <picture>\r\n                                                                    <source media=\"(max-width: 360px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(itemlevel1, "mobileIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                                                                                                                                    if (var_shoulddisplayattr111) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                                                                                                                            if (!var_istrueattr110) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(max-width: 576px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(itemlevel1, "mobileIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                                                                                                                                    if (var_shoulddisplayattr115) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                                                                                                                            if (!var_istrueattr114) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(min-width: 1080px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue116 = renderContext.getObjectModel().resolveProperty(itemlevel1, "webIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent117 = renderContext.call("xss", var_attrvalue116, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr119 = (((null != var_attrcontent117) && (!"".equals(var_attrcontent117))) && ((!"".equals(var_attrvalue116)) && (!((Object)false).equals(var_attrvalue116))));
                                                                                                                                    if (var_shoulddisplayattr119) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr118 = (var_attrvalue116.equals(true));
                                                                                                                                            if (!var_istrueattr118) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <source media=\"(min-width: 1200px)\"");
                                                                                                                        {
                                                                                                                            Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(itemlevel1, "webIcon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "attribute");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                                                                                                                                    if (var_shoulddisplayattr123) {
                                                                                                                                        out.write(" srcset");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                                                                                                                                            if (!var_istrueattr122) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write("/>\r\n                                                                    <img");
                                                                                                                        {
                                                                                                                            Object var_attrvalue124 = renderContext.getObjectModel().resolveProperty(itemlevel1, "icon");
                                                                                                                            {
                                                                                                                                Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "uri");
                                                                                                                                {
                                                                                                                                    boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
                                                                                                                                    if (var_shoulddisplayattr127) {
                                                                                                                                        out.write(" src");
                                                                                                                                        {
                                                                                                                                            boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                                                                                                                                            if (!var_istrueattr126) {
                                                                                                                                                out.write("=\"");
                                                                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                                                                                                                                                out.write("\"");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                        out.write(" alt=\"icon\"/>\r\n                                                                </picture>\r\n                                                            </a>\r\n                                                        ");
                                                                                                                    }
                                                                                                                }
                                                                                                                out.write("\r\n                                                    </div>");
                                                                                                            }
                                                                                                        }
                                                                                                        out.write("\r\n                                                </div>\r\n                                            ");
                                                                                                    }
                                                                                                }
                                                                                                out.write("\r\n                                            ");
                                                                                                {
                                                                                                    Object var_testvariable128 = renderContext.getObjectModel().resolveProperty(itemlevel1, "listViewDocumentItemsSecondLevel");
                                                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable128)) {
                                                                                                        out.write("\r\n                                                ");
                                                                                                        {
                                                                                                            Object var_collectionvar129 = renderContext.getObjectModel().resolveProperty(itemlevel1, "listViewDocumentItemsSecondLevel");
                                                                                                            {
                                                                                                                long var_size130 = ((var_collectionvar129_list_coerced$ == null ? (var_collectionvar129_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar129)) : var_collectionvar129_list_coerced$).size());
                                                                                                                {
                                                                                                                    boolean var_notempty131 = (var_size130 > 0);
                                                                                                                    if (var_notempty131) {
                                                                                                                        {
                                                                                                                            long var_end134 = var_size130;
                                                                                                                            {
                                                                                                                                boolean var_validstartstepend135 = (((0 < var_size130) && true) && (var_end134 > 0));
                                                                                                                                if (var_validstartstepend135) {
                                                                                                                                    if (var_collectionvar129_list_coerced$ == null) {
                                                                                                                                        var_collectionvar129_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar129);
                                                                                                                                    }
                                                                                                                                    long var_index136 = 0;
                                                                                                                                    for (Object item : var_collectionvar129_list_coerced$) {
                                                                                                                                        {
                                                                                                                                            boolean var_traversal138 = (((var_index136 >= 0) && (var_index136 <= var_end134)) && true);
                                                                                                                                            if (var_traversal138) {
                                                                                                                                                out.write("\r\n                                                    <div class=\"item have-file-name\">\r\n                                                        <div class=\"file-name\">");
                                                                                                                                                {
                                                                                                                                                    Object var_139 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "documentTitleChild"), "text");
                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_139));
                                                                                                                                                }
                                                                                                                                                out.write("</div>\r\n                                                        ");
                                                                                                                                                {
                                                                                                                                                    Object var_testvariable140 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "icon")) ? renderContext.getObjectModel().resolveProperty(item, "icon") : renderContext.getObjectModel().resolveProperty(item, "label")));
                                                                                                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable140)) {
                                                                                                                                                        out.write("<div class=\"btn\">\r\n                                                            ");
                                                                                                                                                        {
                                                                                                                                                            boolean var_testvariable141 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "typeOfOutput"), "seeMore"));
                                                                                                                                                            if (var_testvariable141) {
                                                                                                                                                                out.write("\r\n                                                                <input type=\"hidden\" data-tracking-click-event=\"linkClick\"");
                                                                                                                                                                {
                                                                                                                                                                    String var_attrcontent142 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "label"), "attribute"))) + "'}");
                                                                                                                                                                    out.write(" data-tracking-click-info-value=\"");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent142));
                                                                                                                                                                    out.write("\"");
                                                                                                                                                                }
                                                                                                                                                                {
                                                                                                                                                                    String var_attrcontent143 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                                                                                                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent143));
                                                                                                                                                                    out.write("\"");
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                <a class=\"link\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue144 = renderContext.getObjectModel().resolveProperty(item, "typeOfOutput");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent145 = renderContext.call("xss", var_attrvalue144, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr147 = (((null != var_attrcontent145) && (!"".equals(var_attrcontent145))) && ((!"".equals(var_attrvalue144)) && (!((Object)false).equals(var_attrvalue144))));
                                                                                                                                                                            if (var_shoulddisplayattr147) {
                                                                                                                                                                                out.write(" data-file-type");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr146 = (var_attrvalue144.equals(true));
                                                                                                                                                                                    if (!var_istrueattr146) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent145));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue148 = renderContext.getObjectModel().resolveProperty(item, "url");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent149 = renderContext.call("xss", var_attrvalue148, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr151 = (((null != var_attrcontent149) && (!"".equals(var_attrcontent149))) && ((!"".equals(var_attrvalue148)) && (!((Object)false).equals(var_attrvalue148))));
                                                                                                                                                                            if (var_shoulddisplayattr151) {
                                                                                                                                                                                out.write(" data-file-link");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr150 = (var_attrvalue148.equals(true));
                                                                                                                                                                                    if (!var_istrueattr150) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent149));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue152 = renderContext.getObjectModel().resolveProperty(item, "url");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent153 = renderContext.call("xss", var_attrvalue152, "uri");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr155 = (((null != var_attrcontent153) && (!"".equals(var_attrcontent153))) && ((!"".equals(var_attrvalue152)) && (!((Object)false).equals(var_attrvalue152))));
                                                                                                                                                                            if (var_shoulddisplayattr155) {
                                                                                                                                                                                out.write(" href");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr154 = (var_attrvalue152.equals(true));
                                                                                                                                                                                    if (!var_istrueattr154) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent153));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write(">\r\n                                                                    <span>");
                                                                                                                                                                {
                                                                                                                                                                    Object var_156 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "label"), "text");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_156));
                                                                                                                                                                }
                                                                                                                                                                out.write("</span>\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue157 = renderContext.getObjectModel().resolveProperty(item, "mobileIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent158 = renderContext.call("xss", var_attrvalue157, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr160 = (((null != var_attrcontent158) && (!"".equals(var_attrcontent158))) && ((!"".equals(var_attrvalue157)) && (!((Object)false).equals(var_attrvalue157))));
                                                                                                                                                                            if (var_shoulddisplayattr160) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr159 = (var_attrvalue157.equals(true));
                                                                                                                                                                                    if (!var_istrueattr159) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent158));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue161 = renderContext.getObjectModel().resolveProperty(item, "mobileIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent162 = renderContext.call("xss", var_attrvalue161, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr164 = (((null != var_attrcontent162) && (!"".equals(var_attrcontent162))) && ((!"".equals(var_attrvalue161)) && (!((Object)false).equals(var_attrvalue161))));
                                                                                                                                                                            if (var_shoulddisplayattr164) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr163 = (var_attrvalue161.equals(true));
                                                                                                                                                                                    if (!var_istrueattr163) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent162));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue165 = renderContext.getObjectModel().resolveProperty(item, "webIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent166 = renderContext.call("xss", var_attrvalue165, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr168 = (((null != var_attrcontent166) && (!"".equals(var_attrcontent166))) && ((!"".equals(var_attrvalue165)) && (!((Object)false).equals(var_attrvalue165))));
                                                                                                                                                                            if (var_shoulddisplayattr168) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr167 = (var_attrvalue165.equals(true));
                                                                                                                                                                                    if (!var_istrueattr167) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent166));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue169 = renderContext.getObjectModel().resolveProperty(item, "webIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent170 = renderContext.call("xss", var_attrvalue169, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr172 = (((null != var_attrcontent170) && (!"".equals(var_attrcontent170))) && ((!"".equals(var_attrvalue169)) && (!((Object)false).equals(var_attrvalue169))));
                                                                                                                                                                            if (var_shoulddisplayattr172) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr171 = (var_attrvalue169.equals(true));
                                                                                                                                                                                    if (!var_istrueattr171) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent170));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <img");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue173 = renderContext.getObjectModel().resolveProperty(item, "icon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent174 = renderContext.call("xss", var_attrvalue173, "uri");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr176 = (((null != var_attrcontent174) && (!"".equals(var_attrcontent174))) && ((!"".equals(var_attrvalue173)) && (!((Object)false).equals(var_attrvalue173))));
                                                                                                                                                                            if (var_shoulddisplayattr176) {
                                                                                                                                                                                out.write(" src");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr175 = (var_attrvalue173.equals(true));
                                                                                                                                                                                    if (!var_istrueattr175) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent174));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write(" alt=\"icon\"/>\r\n                                                                    </picture>\r\n                                                                </a>\r\n                                                            ");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        out.write("\r\n                                                            ");
                                                                                                                                                        {
                                                                                                                                                            boolean var_testvariable177 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "typeOfOutput"), "seeMore"));
                                                                                                                                                            if (var_testvariable177) {
                                                                                                                                                                out.write("\r\n                                                                <a class=\"link\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue178 = renderContext.getObjectModel().resolveProperty(item, "typeOfOutput");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent179 = renderContext.call("xss", var_attrvalue178, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr181 = (((null != var_attrcontent179) && (!"".equals(var_attrcontent179))) && ((!"".equals(var_attrvalue178)) && (!((Object)false).equals(var_attrvalue178))));
                                                                                                                                                                            if (var_shoulddisplayattr181) {
                                                                                                                                                                                out.write(" data-file-type");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr180 = (var_attrvalue178.equals(true));
                                                                                                                                                                                    if (!var_istrueattr180) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent179));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue182 = renderContext.getObjectModel().resolveProperty(item, "url");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent183 = renderContext.call("xss", var_attrvalue182, "uri");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr185 = (((null != var_attrcontent183) && (!"".equals(var_attrcontent183))) && ((!"".equals(var_attrvalue182)) && (!((Object)false).equals(var_attrvalue182))));
                                                                                                                                                                            if (var_shoulddisplayattr185) {
                                                                                                                                                                                out.write(" href");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr184 = (var_attrvalue182.equals(true));
                                                                                                                                                                                    if (!var_istrueattr184) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent183));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                                                                                                {
                                                                                                                                                                    String var_attrcontent186 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "label"), "attribute"))) + "'}");
                                                                                                                                                                    out.write(" data-tracking-click-info-value=\"");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent186));
                                                                                                                                                                    out.write("\"");
                                                                                                                                                                }
                                                                                                                                                                {
                                                                                                                                                                    String var_attrcontent187 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                                                                                                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent187));
                                                                                                                                                                    out.write("\"");
                                                                                                                                                                }
                                                                                                                                                                out.write(">\r\n                                                                    <span>");
                                                                                                                                                                {
                                                                                                                                                                    Object var_188 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "label"), "text");
                                                                                                                                                                    out.write(renderContext.getObjectModel().toString(var_188));
                                                                                                                                                                }
                                                                                                                                                                out.write("</span>\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue189 = renderContext.getObjectModel().resolveProperty(item, "mobileIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent190 = renderContext.call("xss", var_attrvalue189, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr192 = (((null != var_attrcontent190) && (!"".equals(var_attrcontent190))) && ((!"".equals(var_attrvalue189)) && (!((Object)false).equals(var_attrvalue189))));
                                                                                                                                                                            if (var_shoulddisplayattr192) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr191 = (var_attrvalue189.equals(true));
                                                                                                                                                                                    if (!var_istrueattr191) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent190));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue193 = renderContext.getObjectModel().resolveProperty(item, "mobileIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent194 = renderContext.call("xss", var_attrvalue193, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr196 = (((null != var_attrcontent194) && (!"".equals(var_attrcontent194))) && ((!"".equals(var_attrvalue193)) && (!((Object)false).equals(var_attrvalue193))));
                                                                                                                                                                            if (var_shoulddisplayattr196) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr195 = (var_attrvalue193.equals(true));
                                                                                                                                                                                    if (!var_istrueattr195) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent194));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue197 = renderContext.getObjectModel().resolveProperty(item, "webIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent198 = renderContext.call("xss", var_attrvalue197, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr200 = (((null != var_attrcontent198) && (!"".equals(var_attrcontent198))) && ((!"".equals(var_attrvalue197)) && (!((Object)false).equals(var_attrvalue197))));
                                                                                                                                                                            if (var_shoulddisplayattr200) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr199 = (var_attrvalue197.equals(true));
                                                                                                                                                                                    if (!var_istrueattr199) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent198));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue201 = renderContext.getObjectModel().resolveProperty(item, "webIcon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent202 = renderContext.call("xss", var_attrvalue201, "attribute");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr204 = (((null != var_attrcontent202) && (!"".equals(var_attrcontent202))) && ((!"".equals(var_attrvalue201)) && (!((Object)false).equals(var_attrvalue201))));
                                                                                                                                                                            if (var_shoulddisplayattr204) {
                                                                                                                                                                                out.write(" srcset");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr203 = (var_attrvalue201.equals(true));
                                                                                                                                                                                    if (!var_istrueattr203) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent202));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write("/>\r\n                                                                        <img");
                                                                                                                                                                {
                                                                                                                                                                    Object var_attrvalue205 = renderContext.getObjectModel().resolveProperty(item, "icon");
                                                                                                                                                                    {
                                                                                                                                                                        Object var_attrcontent206 = renderContext.call("xss", var_attrvalue205, "uri");
                                                                                                                                                                        {
                                                                                                                                                                            boolean var_shoulddisplayattr208 = (((null != var_attrcontent206) && (!"".equals(var_attrcontent206))) && ((!"".equals(var_attrvalue205)) && (!((Object)false).equals(var_attrvalue205))));
                                                                                                                                                                            if (var_shoulddisplayattr208) {
                                                                                                                                                                                out.write(" src");
                                                                                                                                                                                {
                                                                                                                                                                                    boolean var_istrueattr207 = (var_attrvalue205.equals(true));
                                                                                                                                                                                    if (!var_istrueattr207) {
                                                                                                                                                                                        out.write("=\"");
                                                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent206));
                                                                                                                                                                                        out.write("\"");
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                out.write(" alt=\"icon\"/>\r\n                                                                    </picture>\r\n                                                                </a>\r\n                                                            ");
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        out.write("\r\n                                                        </div>");
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                out.write("\r\n                                                    </div>\r\n                                                ");
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        var_index136++;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                            var_collectionvar129_list_coerced$ = null;
                                                                                                        }
                                                                                                        out.write("\r\n                                            ");
                                                                                                    }
                                                                                                }
                                                                                                out.write("\r\n                                        </div>\r\n                                    </div>\r\n                                ");
                                                                                            }
                                                                                        }
                                                                                        var_index53++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            var_collectionvar46_list_coerced$ = null;
                                                        }
                                                        out.write("\r\n                            </div>\r\n                        ");
                                                    }
                                                }
                                            }
                                            var_index42++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar35_list_coerced$ = null;
            }
            out.write("\r\n                    </div>\r\n                    <div class=\"view-more\" style=\"display: none\">\r\n                        ");
            {
                Object var_resourcecontent209 = renderContext.call("includeResource", "button", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/button"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent209));
            }
            out.write("\r\n                    </div>\r\n                </div>\r\n                <div class=\"popup-download\" style=\"display: none\" data-default-title=\"Preview\">\r\n                    <div class=\"popup-content\">\r\n                        <div class=\"head\">\r\n                            <span class=\"title\"></span>\r\n                            <button class=\"close-btn\">\r\n                                <div class=\"button-label\">\r\n                                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-close-red.svg\"/>\r\n                                </div>\r\n                            </button>\r\n                        </div>\r\n                        <div class=\"loading\">");
            {
                String var_210 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "loadingPDF"), "text"))) + "\r\n                        ");
                out.write(renderContext.getObjectModel().toString(var_210));
            }
            out.write("</div>\r\n                        <div class=\"file-content\"></div>\r\n                        <div class=\"foot\">\r\n                            <a href=\"\" target=\"_blank\">\r\n                                <span>");
            {
                Object var_211 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listviewdocument, "download"), "text");
                out.write(renderContext.getObjectModel().toString(var_211));
            }
            out.write("</span>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-pdf-download.svg\"/>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar212 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions213_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions213 = obj().with("isEmpty", var_templateoptions213_field$_isempty);
            callUnit(out, renderContext, var_templatevar212, var_templateoptions213);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

