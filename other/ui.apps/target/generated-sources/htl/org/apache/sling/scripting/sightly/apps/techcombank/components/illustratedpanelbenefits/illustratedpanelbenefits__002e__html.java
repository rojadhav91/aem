/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.illustratedpanelbenefits;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class illustratedpanelbenefits__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.IllustratedPanelModel.class.getName(), obj());
out.write("\r\n    <div class=\"sectioncontainer\">\r\n        <div class=\"tcb-sectionContainer\">\r\n            <div class=\"tcb-content-container\">\r\n                <div class=\"illustrated-panel\">\r\n                    <div class=\"illustrated-panel-plan\">\r\n                        <div class=\"illustrated-panel-plan__description\">");
{
    String var_5 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "beforeText"), "html"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_5));
}
out.write("</div>\r\n                        ");
{
    Object var_testvariable6 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable6)) {
        out.write("<div class=\"illustrated-panel-plan__img\">\r\n                            <picture>\r\n                                <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImageMobile");
            {
                Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                {
                    boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                    if (var_shoulddisplayattr10) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                            if (!var_istrueattr9) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImageMobile");
            {
                Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                {
                    boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                    if (var_shoulddisplayattr14) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                            if (!var_istrueattr13) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImage");
            {
                Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                {
                    boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                    if (var_shoulddisplayattr18) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                            if (!var_istrueattr17) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImage");
            {
                Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                {
                    boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                    if (var_shoulddisplayattr22) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                            if (!var_istrueattr21) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <img");
        {
            Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_model, "altText");
            {
                Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                {
                    boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                    if (var_shoulddisplayattr26) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                            if (!var_istrueattr25) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-nimg=\"fixed\" decoding=\"async\"");
        {
            Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImage");
            {
                Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "uri");
                {
                    boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                    if (var_shoulddisplayattr30) {
                        out.write(" src");
                        {
                            boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                            if (!var_istrueattr29) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                            </picture>\r\n\r\n                        </div>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable31 = renderContext.getObjectModel().resolveProperty(_global_model, "afterText");
    if (renderContext.getObjectModel().toBoolean(var_testvariable31)) {
        out.write("<div class=\"illustrated-panel-plan__description\">");
        {
            String var_32 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "afterText"), "html"))) + "\r\n                        ");
            out.write(renderContext.getObjectModel().toString(var_32));
        }
        out.write("</div>");
    }
}
out.write("\r\n\r\n                        ");
{
    Object var_testvariable33 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "beforeLabel")) ? renderContext.getObjectModel().resolveProperty(_global_model, "beforeLabel") : renderContext.getObjectModel().resolveProperty(_global_model, "afterLabel")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable33)) {
        out.write("<div class=\"illustrated-panel-plan__benefitRate\">\r\n                            ");
        {
            Object var_testvariable34 = renderContext.getObjectModel().resolveProperty(_global_model, "beforeLabel");
            if (renderContext.getObjectModel().toBoolean(var_testvariable34)) {
                out.write("<div class=\"illustrated-panel-plan__beforeRate\">");
                {
                    String var_35 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "beforeLabel"), "html"))) + "\r\n                            ");
                    out.write(renderContext.getObjectModel().toString(var_35));
                }
                out.write("</div>");
            }
        }
        out.write("\r\n                            ");
        {
            Object var_testvariable36 = renderContext.getObjectModel().resolveProperty(_global_model, "afterLabel");
            if (renderContext.getObjectModel().toBoolean(var_testvariable36)) {
                out.write("<div class=\"illustrated-panel-plan__afterRate\">");
                {
                    String var_37 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "afterLabel"), "html"))) + "\r\n                            ");
                    out.write(renderContext.getObjectModel().toString(var_37));
                }
                out.write("</div>");
            }
        }
        out.write("\r\n                        </div>");
    }
}
out.write("\r\n\r\n                        <div class=\"illustrated-panel-plan__result__wrapper\">\r\n                            <div");
{
    String var_attrcontent38 = (("illustrated-panel-plan__result_" + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "bottomAlignment"), "center")) ? "center" : "left"), "attribute"))) + "__info");
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent38));
    out.write("\"");
}
out.write(">\r\n                                <div class=\"illustrated-panel-plan__result__icon\">\r\n                                    <img");
{
    Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(_global_model, "icon");
    {
        Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "uri");
        {
            boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
            if (var_shoulddisplayattr42) {
                out.write(" src");
                {
                    boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                    if (!var_istrueattr41) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(_global_model, "iconAlt");
    {
        Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
        {
            boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
            if (var_shoulddisplayattr46) {
                out.write(" alt");
                {
                    boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                    if (!var_istrueattr45) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                </div>\r\n                                <span class=\"illustrated-panel-plan__result_description\">");
{
    String var_47 = (("\r\n                                     " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bottomText"), "text"))) + "\r\n                                ");
    out.write(renderContext.getObjectModel().toString(var_47));
}
out.write("</span>\r\n                            </div>\r\n                            <div class=\"illustrated-panel-plan__result_right__info\">\r\n                                <p class=\"illustrated-panel-plan__result__title\">");
{
    Object var_48 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "valueText"), "text");
    out.write(renderContext.getObjectModel().toString(var_48));
}
out.write("</p>\r\n                                <h3 class=\"illustrated-panel-plan__result__total__money\"><span>");
{
    Object var_49 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "valueAmount"), "text");
    out.write(renderContext.getObjectModel().toString(var_49));
}
out.write("</span></h3>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

