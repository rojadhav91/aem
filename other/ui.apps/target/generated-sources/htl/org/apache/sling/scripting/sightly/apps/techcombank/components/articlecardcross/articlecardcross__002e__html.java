/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.articlecardcross;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class articlecardcross__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar2_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.ArticleCardCrossModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "articleCardCrossItemList");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div");
            {
                String var_attrcontent1 = ((((((("card-center-listcard " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation"), "attribute"))) + " card-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "theme"), "attribute"))) + "-theme display-image-in-mobile-view-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "displayImageInMobileView"), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "theme"), "grey")) ? "zoom-out-img" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n            ");
            {
                Object var_collectionvar2 = renderContext.getObjectModel().resolveProperty(_global_model, "articleCardCrossItemList");
                {
                    long var_size3 = ((var_collectionvar2_list_coerced$ == null ? (var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2)) : var_collectionvar2_list_coerced$).size());
                    {
                        boolean var_notempty4 = (var_size3 > 0);
                        if (var_notempty4) {
                            {
                                long var_end7 = var_size3;
                                {
                                    boolean var_validstartstepend8 = (((0 < var_size3) && true) && (var_end7 > 0));
                                    if (var_validstartstepend8) {
                                        if (var_collectionvar2_list_coerced$ == null) {
                                            var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2);
                                        }
                                        long var_index9 = 0;
                                        for (Object item : var_collectionvar2_list_coerced$) {
                                            {
                                                boolean var_traversal11 = (((var_index9 >= 0) && (var_index9 <= var_end7)) && true);
                                                if (var_traversal11) {
                                                    out.write("\r\n                ");
                                                    {
                                                        Object var_testvariable12 = renderContext.getObjectModel().resolveProperty(item, "ctaLink");
                                                        if (renderContext.getObjectModel().toBoolean(var_testvariable12)) {
                                                            out.write("<a");
                                                            {
                                                                Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(item, "ctaLink");
                                                                {
                                                                    Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "uri");
                                                                    {
                                                                        boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                                                                        if (var_shoulddisplayattr16) {
                                                                            out.write(" href");
                                                                            {
                                                                                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                                                                                if (!var_istrueattr15) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            {
                                                                String var_attrvalue17 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "_self");
                                                                {
                                                                    Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                                        if (var_shoulddisplayattr20) {
                                                                            out.write(" target");
                                                                            {
                                                                                boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                                                if (!var_istrueattr19) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            {
                                                                String var_attrvalue21 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                                {
                                                                    Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                        if (var_shoulddisplayattr24) {
                                                                            out.write(" rel");
                                                                            {
                                                                                boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                                if (!var_istrueattr23) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" class=\"card-center-carditem\" data-tracking-click-event=\"linkClick\"");
                                                            {
                                                                String var_attrcontent25 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "attribute"))) + "'}");
                                                                out.write(" data-tracking-click-info-value=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                out.write("\"");
                                                            }
                                                            {
                                                                String var_attrcontent26 = (("{'webInteractions': {'name': 'Article Card Cross','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                                out.write(" data-tracking-web-interaction-value=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                out.write("\"");
                                                            }
                                                            out.write(">\r\n                    <article>\r\n                        <div class=\"cardcenter-carditem-header\">\r\n                            <div class=\"cardcenter-carditem-headerimg\">\r\n                                <span>\r\n                                    ");
                                                            {
                                                                Object var_testvariable27 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                if (renderContext.getObjectModel().toBoolean(var_testvariable27)) {
                                                                    out.write("<picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                    {
                                                                        Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                                                                                if (var_shoulddisplayattr31) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                                                                                        if (!var_istrueattr30) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                    {
                                                                        Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                                                                if (var_shoulddisplayattr35) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                                                                        if (!var_istrueattr34) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                    {
                                                                        Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                                                                if (var_shoulddisplayattr39) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                                                                        if (!var_istrueattr38) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                    {
                                                                        Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                                if (var_shoulddisplayattr43) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                                        if (!var_istrueattr42) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <img");
                                                                    {
                                                                        Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(item, "imageAlt");
                                                                        {
                                                                            Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                                                                if (var_shoulddisplayattr47) {
                                                                                    out.write(" alt");
                                                                                    {
                                                                                        boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                                                                        if (!var_istrueattr46) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    {
                                                                        Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                                                                if (var_shoulddisplayattr51) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                                                                        if (!var_istrueattr50) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                    </picture>");
                                                                }
                                                            }
                                                            out.write("\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"cardcenter-carditem-content\">\r\n                            <h3>");
                                                            {
                                                                Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_52));
                                                            }
                                                            out.write("</h3>\r\n                            <div>");
                                                            {
                                                                String var_53 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "html"))) + "\r\n                            ");
                                                                out.write(renderContext.getObjectModel().toString(var_53));
                                                            }
                                                            out.write("</div>\r\n                        </div>\r\n                    </article>\r\n                </a>");
                                                        }
                                                    }
                                                    out.write("\r\n                ");
                                                    {
                                                        boolean var_testvariable54 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "ctaLink")));
                                                        if (var_testvariable54) {
                                                            out.write("<div class=\"card-center-carditem\">\r\n                    <article>\r\n                        <div class=\"cardcenter-carditem-header\">\r\n                            <div class=\"cardcenter-carditem-headerimg\">\r\n                                <span>\r\n                                    ");
                                                            {
                                                                Object var_testvariable55 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                if (renderContext.getObjectModel().toBoolean(var_testvariable55)) {
                                                                    out.write("<picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                    {
                                                                        Object var_attrvalue56 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                                                                                if (var_shoulddisplayattr59) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                                                                                        if (!var_istrueattr58) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                    {
                                                                        Object var_attrvalue60 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                                                                                if (var_shoulddisplayattr63) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                                                                                        if (!var_istrueattr62) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                    {
                                                                        Object var_attrvalue64 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                                                                                if (var_shoulddisplayattr67) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                                                                                        if (!var_istrueattr66) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                    {
                                                                        Object var_attrvalue68 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent69 = renderContext.call("xss", var_attrvalue68, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr71 = (((null != var_attrcontent69) && (!"".equals(var_attrcontent69))) && ((!"".equals(var_attrvalue68)) && (!((Object)false).equals(var_attrvalue68))));
                                                                                if (var_shoulddisplayattr71) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr70 = (var_attrvalue68.equals(true));
                                                                                        if (!var_istrueattr70) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent69));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <img");
                                                                    {
                                                                        Object var_attrvalue72 = renderContext.getObjectModel().resolveProperty(item, "imageAlt");
                                                                        {
                                                                            Object var_attrcontent73 = renderContext.call("xss", var_attrvalue72, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr75 = (((null != var_attrcontent73) && (!"".equals(var_attrcontent73))) && ((!"".equals(var_attrvalue72)) && (!((Object)false).equals(var_attrvalue72))));
                                                                                if (var_shoulddisplayattr75) {
                                                                                    out.write(" alt");
                                                                                    {
                                                                                        boolean var_istrueattr74 = (var_attrvalue72.equals(true));
                                                                                        if (!var_istrueattr74) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent73));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    {
                                                                        Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                                                                                if (var_shoulddisplayattr79) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                                                                                        if (!var_istrueattr78) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                    </picture>");
                                                                }
                                                            }
                                                            out.write("\r\n                                </span>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"cardcenter-carditem-content\">\r\n                            <h3>");
                                                            {
                                                                Object var_80 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_80));
                                                            }
                                                            out.write("</h3>\r\n                            <div>");
                                                            {
                                                                String var_81 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "html"))) + "\r\n                            ");
                                                                out.write(renderContext.getObjectModel().toString(var_81));
                                                            }
                                                            out.write("</div>\r\n                        </div>\r\n                    </article>\r\n                </div>");
                                                        }
                                                    }
                                                    out.write("\r\n            ");
                                                }
                                            }
                                            var_index9++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar2_list_coerced$ = null;
            }
            out.write("\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar82 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions83_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions83 = obj().with("isEmpty", var_templateoptions83_field$_isempty);
            callUnit(out, renderContext, var_templatevar82, var_templateoptions83);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

