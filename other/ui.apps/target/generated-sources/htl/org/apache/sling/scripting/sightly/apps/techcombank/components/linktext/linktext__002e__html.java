/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.linktext;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class linktext__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _global_linktext = null;
Object _global_hascontent = null;
Collection var_collectionvar3_list_coerced$ = null;
Object _global_processedurl = null;
Collection var_collectionvar66_list_coerced$ = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_linktext = renderContext.call("use", com.techcombank.core.models.LinkTextModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_linktext, "linkTextItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    <div class=\"link-text link-text-component\">\r\n        <!-- Left image link text -->\r\n        ");
    {
        boolean var_testvariable0 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_linktext, "imagePosition"), "left"));
        if (var_testvariable0) {
            out.write("<div class=\"link-text__container\">\r\n            ");
            {
                Object var_testvariable1 = renderContext.getObjectModel().resolveProperty(_global_linktext, "calculatePosition");
                if (renderContext.getObjectModel().toBoolean(var_testvariable1)) {
                    out.write("<div class=\"ticket-list\">\r\n                <div class=\"ticket-list-wrapper leftType\"");
                    {
                        String var_attrcontent2 = (("grid-template-columns: repeat(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_linktext, "calculateColumnsNumber"), "styleString"))) + ", 1fr);");
                        out.write(" style=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                        out.write("\"");
                    }
                    out.write(">\r\n                    ");
                    {
                        Object var_collectionvar3 = renderContext.getObjectModel().resolveProperty(_global_linktext, "linkTextItems");
                        {
                            long var_size4 = ((var_collectionvar3_list_coerced$ == null ? (var_collectionvar3_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar3)) : var_collectionvar3_list_coerced$).size());
                            {
                                boolean var_notempty5 = (var_size4 > 0);
                                if (var_notempty5) {
                                    {
                                        long var_end8 = var_size4;
                                        {
                                            boolean var_validstartstepend9 = (((0 < var_size4) && true) && (var_end8 > 0));
                                            if (var_validstartstepend9) {
                                                if (var_collectionvar3_list_coerced$ == null) {
                                                    var_collectionvar3_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar3);
                                                }
                                                long var_index10 = 0;
                                                for (Object item : var_collectionvar3_list_coerced$) {
                                                    {
                                                        long itemlist_field$_index = var_index10;
                                                        {
                                                            boolean var_traversal12 = (((var_index10 >= 0) && (var_index10 <= var_end8)) && true);
                                                            if (var_traversal12) {
                                                                out.write("<div class=\"ticket-item\"");
                                                                {
                                                                    String var_attrvalue13 = ((itemlist_field$_index >= 3) ? "hidden" : "");
                                                                    {
                                                                        Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                                                                            if (var_shoulddisplayattr16) {
                                                                                out.write(" hiddenInMobile");
                                                                                {
                                                                                    boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                                                                                    if (!var_istrueattr15) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrvalue17 = ((itemlist_field$_index >= 12) ? "hidden" : "");
                                                                    {
                                                                        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                                            if (var_shoulddisplayattr20) {
                                                                                out.write(" hiddenInDesktop");
                                                                                {
                                                                                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                                                    if (!var_istrueattr19) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrcontent21 = (((("grid-row-start:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "row"), "styleString"))) + ";grid-column-start:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "column"), "styleString"))) + ";");
                                                                    out.write(" style=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">\r\n                        ");
_global_processedurl = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                                out.write("\r\n                            ");
                                                                {
                                                                    boolean var_testvariable22 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "linkUrlType"), "tele"));
                                                                    if (var_testvariable22) {
                                                                        out.write("\r\n                                ");
_global_processedurl = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {"tel:", _global_processedurl})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_processedurl)) {
                                                                        }
                                                                        out.write("\r\n                            ");
                                                                    }
                                                                }
                                                                out.write("\r\n                            ");
                                                                {
                                                                    boolean var_testvariable23 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "linkUrlType"), "mail"));
                                                                    if (var_testvariable23) {
                                                                        out.write("\r\n                                ");
_global_processedurl = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {"mailto:", _global_processedurl})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_processedurl)) {
                                                                        }
                                                                        out.write("\r\n                            ");
                                                                    }
                                                                }
                                                                out.write("\r\n                        \r\n                        <a");
                                                                {
                                                                    Object var_attrvalue24 = _global_processedurl;
                                                                    {
                                                                        Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                                                                            if (var_shoulddisplayattr27) {
                                                                                out.write(" href");
                                                                                {
                                                                                    boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                                                                    if (!var_istrueattr26) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" class=\"link_component-link\"");
                                                                {
                                                                    String var_attrvalue28 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "");
                                                                    {
                                                                        Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                                                                            if (var_shoulddisplayattr31) {
                                                                                out.write(" target");
                                                                                {
                                                                                    boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                                                                                    if (!var_istrueattr30) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrvalue32 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                                    {
                                                                        Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                                                            if (var_shoulddisplayattr35) {
                                                                                out.write(" rel");
                                                                                {
                                                                                    boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                                                                    if (!var_istrueattr34) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">\r\n                            <div class=\"ticket_wrapper leftType\">\r\n                                <span class=\"ticket_icon leftType\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                {
                                                                    Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                                    {
                                                                        Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                                                            if (var_shoulddisplayattr39) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                                                                    if (!var_istrueattr38) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                {
                                                                    Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                                    {
                                                                        Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                            if (var_shoulddisplayattr43) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                                    if (!var_istrueattr42) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                {
                                                                    Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                    {
                                                                        Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                                                            if (var_shoulddisplayattr47) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                                                                    if (!var_istrueattr46) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                {
                                                                    Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                    {
                                                                        Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                                                            if (var_shoulddisplayattr51) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                                                                    if (!var_istrueattr50) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                        <img");
                                                                {
                                                                    Object var_attrvalue52 = renderContext.getObjectModel().resolveProperty(item, "iconImageAltText");
                                                                    {
                                                                        Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                                                                            if (var_shoulddisplayattr55) {
                                                                                out.write(" alt");
                                                                                {
                                                                                    boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                                                                    if (!var_istrueattr54) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" class=\"leftType__icon-img\"");
                                                                {
                                                                    Object var_attrvalue56 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                    {
                                                                        Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                                                                            if (var_shoulddisplayattr59) {
                                                                                out.write(" src");
                                                                                {
                                                                                    boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                                                                                    if (!var_istrueattr58) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                    </picture>\r\n                                </span>\r\n                                <div class=\"ticket-content\">\r\n                                    <p class=\"ticket-title\">");
                                                                {
                                                                    Object var_60 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "titleText"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_60));
                                                                }
                                                                out.write("</p>\r\n                                    <p class=\"ticket-description\">");
                                                                {
                                                                    Object var_61 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "description"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_61));
                                                                }
                                                                out.write("</p>\r\n                                </div>\r\n                                ");
                                                                {
                                                                    Object var_testvariable62 = renderContext.getObjectModel().resolveProperty(_global_linktext, "arrow");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable62)) {
                                                                        out.write("<div class=\"icon-ext\">\r\n                                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                                </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                            </div>\r\n                        </a>\r\n                    </div>\n");
                                                            }
                                                        }
                                                    }
                                                    var_index10++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar3_list_coerced$ = null;
                    }
                    out.write("\r\n                </div>\r\n            </div>");
                }
            }
            out.write("\r\n        </div>");
        }
    }
    out.write("\r\n        <!-- Top image link text -->\r\n        ");
    {
        boolean var_testvariable63 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_linktext, "imagePosition"), "top"));
        if (var_testvariable63) {
            out.write("<div class=\"link-text__container\">\r\n            ");
            {
                Object var_testvariable64 = renderContext.getObjectModel().resolveProperty(_global_linktext, "calculatePosition");
                if (renderContext.getObjectModel().toBoolean(var_testvariable64)) {
                    out.write("<div class=\"ticket-list\">\r\n                <div class=\"ticket-list-wrapper topType\"");
                    {
                        String var_attrcontent65 = (("grid-template-columns: repeat(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_linktext, "calculateColumnsNumber"), "styleString"))) + ", 1fr);");
                        out.write(" style=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                        out.write("\"");
                    }
                    out.write(">\r\n                    ");
                    {
                        Object var_collectionvar66 = renderContext.getObjectModel().resolveProperty(_global_linktext, "linkTextItems");
                        {
                            long var_size67 = ((var_collectionvar66_list_coerced$ == null ? (var_collectionvar66_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar66)) : var_collectionvar66_list_coerced$).size());
                            {
                                boolean var_notempty68 = (var_size67 > 0);
                                if (var_notempty68) {
                                    {
                                        long var_end71 = var_size67;
                                        {
                                            boolean var_validstartstepend72 = (((0 < var_size67) && true) && (var_end71 > 0));
                                            if (var_validstartstepend72) {
                                                if (var_collectionvar66_list_coerced$ == null) {
                                                    var_collectionvar66_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar66);
                                                }
                                                long var_index73 = 0;
                                                for (Object item : var_collectionvar66_list_coerced$) {
                                                    {
                                                        long itemlist_field$_index = var_index73;
                                                        {
                                                            boolean var_traversal75 = (((var_index73 >= 0) && (var_index73 <= var_end71)) && true);
                                                            if (var_traversal75) {
                                                                out.write("<div class=\"ticket-item\"");
                                                                {
                                                                    String var_attrvalue76 = ((itemlist_field$_index >= 3) ? "hidden" : "");
                                                                    {
                                                                        Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                                                                            if (var_shoulddisplayattr79) {
                                                                                out.write(" hiddenInMobile");
                                                                                {
                                                                                    boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                                                                                    if (!var_istrueattr78) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrvalue80 = ((itemlist_field$_index >= 12) ? "hidden" : "");
                                                                    {
                                                                        Object var_attrcontent81 = renderContext.call("xss", var_attrvalue80, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr83 = (((null != var_attrcontent81) && (!"".equals(var_attrcontent81))) && ((!"".equals(var_attrvalue80)) && (!((Object)false).equals(var_attrvalue80))));
                                                                            if (var_shoulddisplayattr83) {
                                                                                out.write(" hiddenInDesktop");
                                                                                {
                                                                                    boolean var_istrueattr82 = (var_attrvalue80.equals(true));
                                                                                    if (!var_istrueattr82) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent81));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrcontent84 = (((("grid-row-start:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "row"), "styleString"))) + ";grid-column-start:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "column"), "styleString"))) + ";");
                                                                    out.write(" style=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">\r\n                        ");
_global_processedurl = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                                out.write("\r\n                            ");
                                                                {
                                                                    boolean var_testvariable85 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "linkUrlType"), "tele"));
                                                                    if (var_testvariable85) {
                                                                        out.write("\r\n                                ");
_global_processedurl = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {"tel:", _global_processedurl})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_processedurl)) {
                                                                        }
                                                                        out.write("\r\n                            ");
                                                                    }
                                                                }
                                                                out.write("\r\n                            ");
                                                                {
                                                                    boolean var_testvariable86 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "linkUrlType"), "mail"));
                                                                    if (var_testvariable86) {
                                                                        out.write("\r\n                                ");
_global_processedurl = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {"mailto:", _global_processedurl})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_processedurl)) {
                                                                        }
                                                                        out.write("\r\n                            ");
                                                                    }
                                                                }
                                                                out.write("\r\n                        \r\n                        <a");
                                                                {
                                                                    Object var_attrvalue87 = _global_processedurl;
                                                                    {
                                                                        Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                                                                            if (var_shoulddisplayattr90) {
                                                                                out.write(" href");
                                                                                {
                                                                                    boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                                                                                    if (!var_istrueattr89) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" class=\"contact-us__item\"");
                                                                {
                                                                    String var_attrvalue91 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "");
                                                                    {
                                                                        Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                                                                            if (var_shoulddisplayattr94) {
                                                                                out.write(" target");
                                                                                {
                                                                                    boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                                                                                    if (!var_istrueattr93) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    String var_attrvalue95 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                                    {
                                                                        Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
                                                                            if (var_shoulddisplayattr98) {
                                                                                out.write(" rel");
                                                                                {
                                                                                    boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                                                                                    if (!var_istrueattr97) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">\r\n                            <div class=\"contact-us__item__wrapper\">\r\n                                ");
                                                                {
                                                                    Object var_testvariable99 = renderContext.getObjectModel().resolveProperty(item, "iconImage");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable99)) {
                                                                        out.write("<div class=\"contact-us__item__icon\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                        {
                                                                            Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                                            {
                                                                                Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                                                    if (var_shoulddisplayattr103) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                                            if (!var_istrueattr102) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                        {
                                                                            Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                                            {
                                                                                Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                                                                                    if (var_shoulddisplayattr107) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                                                                            if (!var_istrueattr106) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                        {
                                                                            Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                            {
                                                                                Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                                                                                    if (var_shoulddisplayattr111) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                                                                            if (!var_istrueattr110) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                        {
                                                                            Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                            {
                                                                                Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                                                                                    if (var_shoulddisplayattr115) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                                                                            if (!var_istrueattr114) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                        <img");
                                                                        {
                                                                            Object var_attrvalue116 = renderContext.getObjectModel().resolveProperty(item, "iconImageAltText");
                                                                            {
                                                                                Object var_attrcontent117 = renderContext.call("xss", var_attrvalue116, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr119 = (((null != var_attrcontent117) && (!"".equals(var_attrcontent117))) && ((!"".equals(var_attrvalue116)) && (!((Object)false).equals(var_attrvalue116))));
                                                                                    if (var_shoulddisplayattr119) {
                                                                                        out.write(" alt");
                                                                                        {
                                                                                            boolean var_istrueattr118 = (var_attrvalue116.equals(true));
                                                                                            if (!var_istrueattr118) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write(" class=\"contact-us__icon-img\"");
                                                                        {
                                                                            Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                                            {
                                                                                Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "uri");
                                                                                {
                                                                                    boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                                                                                    if (var_shoulddisplayattr123) {
                                                                                        out.write(" src");
                                                                                        {
                                                                                            boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                                                                                            if (!var_istrueattr122) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                    </picture>\r\n                                </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                ");
                                                                {
                                                                    boolean var_testvariable124 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "iconImage")));
                                                                    if (var_testvariable124) {
                                                                        out.write("<div class=\"contact-us__item__placeholder\"></div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                <div class=\"contact-us__item__content__wrapper\">\r\n                                    <div class=\"contact-us__item__content__title\">\r\n                                        <p class=\"contact-us__item__content__title_text\">");
                                                                {
                                                                    Object var_125 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "titleText"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_125));
                                                                }
                                                                out.write("</p>\r\n                                        ");
                                                                {
                                                                    Object var_testvariable126 = renderContext.getObjectModel().resolveProperty(_global_linktext, "arrow");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable126)) {
                                                                        out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                    </div>\r\n                                    <div class=\"contact-us__item__content__description\">\r\n                                        <p>");
                                                                {
                                                                    Object var_127 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "description"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_127));
                                                                }
                                                                out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </a>\r\n                    </div>\n");
                                                            }
                                                        }
                                                    }
                                                    var_index73++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar66_list_coerced$ = null;
                    }
                    out.write("\r\n                </div>\r\n            </div>");
                }
            }
            out.write("\r\n        </div>");
        }
    }
    out.write("\r\n    </div>\r\n    <div class=\"link-text-btn\">\r\n        <button type=\"button\" class=\"title link-text-button__link\">\r\n          <span lass=\"link-text-view-more__title\">");
    {
        Object var_128 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_linktext, "viewMoreLabel"), "text");
        out.write(renderContext.getObjectModel().toString(var_128));
    }
    out.write("</span>\r\n          <div class=\"link-text-view-more__icon\">\r\n            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\"/>\r\n          </div>\r\n        </button>\r\n    </div>\r\n");
}
out.write("\r\n");
{
    Object var_templatevar129 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions130_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions130 = obj().with("isEmpty", var_templateoptions130_field$_isempty);
            callUnit(out, renderContext, var_templatevar129, var_templateoptions130);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

