/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.atomic.spacer;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class spacer__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_spacer = null;
Object _global_template = null;
Object _global_hascontent = null;
_global_spacer = renderContext.call("use", com.techcombank.core.models.SpacerModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_spacer, "mobilePadding")) ? renderContext.getObjectModel().resolveProperty(_global_spacer, "mobilePadding") : renderContext.getObjectModel().resolveProperty(_global_spacer, "desktopPadding")));
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"spacer-sm\"");
            {
                String var_attrcontent1 = (("height:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_spacer, "mobilePadding"), "styleString"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write("></div>\r\n        <div class=\"spacer-lg\"");
            {
                String var_attrcontent2 = (("height:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_spacer, "desktopPadding"), "styleString"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                out.write("\"");
            }
            out.write("></div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar3 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions4_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions4 = obj().with("isEmpty", var_templateoptions4_field$_isempty);
            callUnit(out, renderContext, var_templatevar3, var_templateoptions4);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

