/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.articleTagCloud;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class articleTagCloud__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_article = null;
Collection var_collectionvar8_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_article = renderContext.call("use", com.techcombank.core.models.ArticleTagCloudModel.class.getName(), obj());
{
    boolean var_testvariable5 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_article, "hideStatus")));
    if (var_testvariable5) {
        out.write("\r\n    ");
        {
            Object var_testvariable6 = renderContext.getObjectModel().resolveProperty(_global_article, "articleList");
            if (renderContext.getObjectModel().toBoolean(var_testvariable6)) {
                out.write("<section class=\"article-tag-cloud\">\r\n        <div class=\"article-tag-cloud_heading\">\r\n            <div class=\"article-tag-cloud_icon\">\r\n                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-tag.svg\"/>\r\n            </div>\r\n            <div class=\"article-tag-cloud_title\">");
                {
                    String var_7 = (("\r\n                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_article, "tagLabel"), "text"))) + ":\r\n            ");
                    out.write(renderContext.getObjectModel().toString(var_7));
                }
                out.write("</div>\r\n        </div>\r\n    ");
                {
                    Object var_collectionvar8 = renderContext.getObjectModel().resolveProperty(_global_article, "articleList");
                    {
                        long var_size9 = ((var_collectionvar8_list_coerced$ == null ? (var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8)) : var_collectionvar8_list_coerced$).size());
                        {
                            boolean var_notempty10 = (var_size9 > 0);
                            if (var_notempty10) {
                                {
                                    long var_end13 = var_size9;
                                    {
                                        boolean var_validstartstepend14 = (((0 < var_size9) && true) && (var_end13 > 0));
                                        if (var_validstartstepend14) {
                                            out.write("<div class=\"article-tag-cloud_content\">");
                                            if (var_collectionvar8_list_coerced$ == null) {
                                                var_collectionvar8_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar8);
                                            }
                                            long var_index15 = 0;
                                            for (Object article : var_collectionvar8_list_coerced$) {
                                                {
                                                    boolean var_traversal17 = (((var_index15 >= 0) && (var_index15 <= var_end13)) && true);
                                                    if (var_traversal17) {
                                                        out.write("\r\n        <a");
                                                        {
                                                            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(article, "path");
                                                            {
                                                                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                                    if (var_shoulddisplayattr21) {
                                                                        out.write(" href");
                                                                        {
                                                                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                            if (!var_istrueattr20) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue22 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(article, "openInNewTab"), "true")) ? "_blank" : "_self");
                                                            {
                                                                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                                    if (var_shoulddisplayattr25) {
                                                                        out.write(" target");
                                                                        {
                                                                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                            if (!var_istrueattr24) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(" x-cq-linkchecker=\"valid\" class=\"article-tag-cloud_item\" data-tracking-click-event=\"linkCLick\"");
                                                        {
                                                            String var_attrcontent26 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            String var_attrcontent27 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "title"), "attribute"))) + "'}");
                                                            out.write(" data-tracking-click-info-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n            <span class=\"article-tag-cloud_item_title\">");
                                                        {
                                                            Object var_28 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(article, "title"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_28));
                                                        }
                                                        out.write("</span>\r\n        </a>\r\n    ");
                                                    }
                                                }
                                                var_index15++;
                                            }
                                            out.write("</div>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var_collectionvar8_list_coerced$ = null;
                }
                out.write("\r\n    </section>");
            }
        }
        out.write("\r\n");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

