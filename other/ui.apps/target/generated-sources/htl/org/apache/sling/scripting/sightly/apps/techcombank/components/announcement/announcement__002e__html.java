/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.announcement;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class announcement__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_announcement = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n\t<p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_announcement = renderContext.call("use", com.techcombank.core.models.AnnouncementModel.class.getName(), obj());
out.write("\r\n");
{
    Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_global_announcement, "announcementText");
    if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
        out.write("<div class=\"announcement\">\r\n\t<div class=\"announcement__container\">\r\n\t\t<div class=\"content\">\r\n\t\t\t<div class=\"main-content\">\r\n\t\t\t\t<div class=\"title\">\r\n\t\t\t\t\t<picture>\r\n\t\t\t\t\t\t<source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconPathMobile");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t\t\t<source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconPathMobile");
            {
                Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                {
                    boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                    if (var_shoulddisplayattr13) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                            if (!var_istrueattr12) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t\t\t<source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconPath");
            {
                Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                {
                    boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                    if (var_shoulddisplayattr17) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                            if (!var_istrueattr16) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t\t\t<source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconPath");
            {
                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                {
                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                    if (var_shoulddisplayattr21) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                            if (!var_istrueattr20) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t\t\t<img");
        {
            Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconAltText");
            {
                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                {
                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                    if (var_shoulddisplayattr25) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                            if (!var_istrueattr24) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-nimg=\"fixed\" decoding=\"async\"");
        {
            Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_global_announcement, "iconPath");
            {
                Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "uri");
                {
                    boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                    if (var_shoulddisplayattr29) {
                        out.write(" src");
                        {
                            boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                            if (!var_istrueattr28) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t\t</picture>\r\n\t\t\t\t\t<h3>");
        {
            Object var_30 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_announcement, "announcementText"), "text");
            out.write(renderContext.getObjectModel().toString(var_30));
        }
        out.write("</h3>\r\n\t\t\t\t</div>\r\n\t\t\t\t<h2>");
        {
            Object var_31 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_announcement, "titleText"), "text");
            out.write(renderContext.getObjectModel().toString(var_31));
        }
        out.write("</h2>");
        {
            String var_32 = (("\r\n\t\t\t\t" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_announcement, "titleDescription"), "html"))) + "\r\n\t\t\t");
            out.write(renderContext.getObjectModel().toString(var_32));
        }
        out.write("</div>\r\n\t\t\t");
        {
            Object var_testvariable33 = renderContext.getObjectModel().resolveProperty(_global_announcement, "ctaRequired");
            if (renderContext.getObjectModel().toBoolean(var_testvariable33)) {
                out.write("\r\n\t\t\t\t");
                {
                    Object var_resourcecontent34 = renderContext.call("includeResource", "button", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/button"));
                    out.write(renderContext.getObjectModel().toString(var_resourcecontent34));
                }
                out.write("\r\n\t\t\t");
            }
        }
        out.write("\r\n\t\t</div>\r\n\t</div>\r\n</div>");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

