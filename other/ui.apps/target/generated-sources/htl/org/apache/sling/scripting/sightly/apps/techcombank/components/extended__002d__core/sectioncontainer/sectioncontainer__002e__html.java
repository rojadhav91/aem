/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.extended__002d__core.sectioncontainer;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class sectioncontainer__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_sectioncontainer = null;
Object _dynamic_resource = bindings.get("resource");
_global_sectioncontainer = renderContext.call("use", com.techcombank.core.models.SectionContainerModel.class.getName(), obj());
out.write("\r\n\r\n<div");
{
    String var_attrcontent0 = ("tcb-sectionContainer " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "textColor")) ? "section-font-color" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent0));
    out.write("\"");
}
{
    String var_attrcontent1 = ("color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "textColor"), "styleToken")));
    out.write(" style=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent1));
    out.write("\"");
}
out.write(">\r\n    <!--No small image then mobile will display large image.  -->\r\n    ");
{
    boolean var_testvariable2 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "containerAction"), "ll-ll:sl-ll"));
    if (var_testvariable2) {
        out.write("\r\n        <div");
        {
            String var_attrcontent3 = ("tcb-bgImage " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundImageRounded")) ? "rounded" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent3));
            out.write("\"");
        }
        {
            String var_attrcontent4 = (((((((((("background-image: url(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "webImagePath"), "html"))) + "); padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px; padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent4));
            out.write("\"");
        }
        out.write(">\r\n            <div class=\"tcb-content-container\">\r\n                ");
        {
            Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
                out.write("<h2 class=\"tcb-title\">");
                {
                    Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_6));
                }
                out.write("</h2>");
            }
        }
        out.write("\r\n                ");
        {
            Object var_resourcecontent7 = renderContext.call("includeResource", _dynamic_resource, obj().with("resourceType", "wcm/foundation/components/responsivegrid"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent7));
        }
        out.write("\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n\r\n    <!--large image not authored then desktop will display background color\r\n        and small image authored will display for mobile.-->\r\n    ");
{
    boolean var_testvariable8 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "containerAction"), "ll-bgColor:sl-sl"));
    if (var_testvariable8) {
        out.write("\r\n        <div class=\"tcb-largeBgColor\"");
        {
            String var_attrcontent9 = ((" background-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundColor"), "html"))) + ";");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent9));
            out.write("\"");
        }
        out.write(">\r\n            <div");
        {
            String var_attrcontent10 = ("tcb-bgImage tcb-onlySmallImage " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundImageRounded")) ? "rounded" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent10));
            out.write("\"");
        }
        {
            String var_attrcontent11 = (((((((((("background-image: url(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "mobileImagePath"), "html"))) + "); padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px; padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent11));
            out.write("\"");
        }
        out.write(">\r\n                <div class=\"tcb-content-container\">\r\n                    ");
        {
            Object var_testvariable12 = renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable12)) {
                out.write("<h2 class=\"tcb-title\">");
                {
                    Object var_13 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_13));
                }
                out.write("</h2>");
            }
        }
        out.write("\r\n                    ");
        {
            Object var_resourcecontent14 = renderContext.call("includeResource", _dynamic_resource, obj().with("resourceType", "wcm/foundation/components/responsivegrid"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent14));
        }
        out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n    <!--If both images are authored then large image will be displayed for desktop and small for mobile.-->\r\n    ");
{
    boolean var_testvariable15 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "containerAction"), "ll-ll:sl-sl"));
    if (var_testvariable15) {
        out.write("\r\n        <div");
        {
            String var_attrcontent16 = ("tcb-bgImage tcb-largeImage " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundImageRounded")) ? "rounded" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
            out.write("\"");
        }
        {
            String var_attrcontent17 = ((((((((((" background-image: url(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "webImagePath"), "html"))) + "); padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px; padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent17));
            out.write("\"");
        }
        out.write(">\r\n            <div");
        {
            String var_attrcontent18 = ("tcb-bgImage tcb-smallImage " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundImageRounded")) ? "rounded" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent18));
            out.write("\"");
        }
        {
            String var_attrcontent19 = (((((((((("background-image: url(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "mobileImagePath"), "html"))) + "); padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px;  padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent19));
            out.write("\"");
        }
        out.write(">\r\n                <div class=\"tcb-content-container\">\r\n                    ");
        {
            Object var_testvariable20 = renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable20)) {
                out.write("<h2 class=\"tcb-title\">");
                {
                    Object var_21 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_21));
                }
                out.write("</h2>");
            }
        }
        out.write("\r\n                    ");
        {
            Object var_resourcecontent22 = renderContext.call("includeResource", _dynamic_resource, obj().with("resourceType", "wcm/foundation/components/responsivegrid"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent22));
        }
        out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n\r\n    <!--If no images are authored both desktop and mobile will display Background color.-->\r\n    ");
{
    boolean var_testvariable23 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "containerAction"), "bgColor"));
    if (var_testvariable23) {
        out.write("\r\n        <div class=\"tcb-bgColor\"");
        {
            String var_attrcontent24 = ((((((((((" background-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "backgroundColor"), "html"))) + "; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px; padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent24));
            out.write("\"");
        }
        out.write(">\r\n            <div class=\"tcb-content-container\">\r\n                ");
        {
            Object var_testvariable25 = renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable25)) {
                out.write("<h2 class=\"tcb-title\">");
                {
                    Object var_26 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_26));
                }
                out.write("</h2>");
            }
        }
        out.write("\r\n                ");
        {
            Object var_resourcecontent27 = renderContext.call("includeResource", _dynamic_resource, obj().with("resourceType", "wcm/foundation/components/responsivegrid"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent27));
        }
        out.write("\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n\r\n    ");
{
    boolean var_testvariable28 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "containerAction")));
    if (var_testvariable28) {
        out.write("\r\n        <div class=\"tcb-content-container\"");
        {
            String var_attrcontent29 = (((((((("padding-left:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "rightPadding"), "html"))) + "px; padding-top:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "topPadding"), "html"))) + "px; padding-bottom: ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "bottomPadding"), "html"))) + "px;");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent29));
            out.write("\"");
        }
        out.write(">\r\n            ");
        {
            Object var_testvariable30 = renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable30)) {
                out.write("<h2 class=\"tcb-title\">");
                {
                    Object var_31 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_sectioncontainer, "titleText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_31));
                }
                out.write("</h2>");
            }
        }
        out.write("\r\n            ");
        {
            Object var_resourcecontent32 = renderContext.call("includeResource", _dynamic_resource, obj().with("resourceType", "wcm/foundation/components/responsivegrid"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent32));
        }
        out.write("\r\n        </div>\r\n    ");
    }
}
out.write("\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

