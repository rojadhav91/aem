/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.termsconditions;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class termsconditions__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.TermsAndConditionsModel.class.getName(), obj());
out.write("\r\n<div class=\"checkbox section width-100\"");
{
    Object var_attrvalue5 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "required")) ? renderContext.getObjectModel().resolveProperty(_global_model, "requiredMessage") : "");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-required");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <div");
{
    String var_attrcontent9 = (("cmp-form-options__field--term " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "analytics")) ? "analytics-form-field" : ""), "attribute"))) + " ");
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent9));
    out.write("\"");
}
out.write(">\r\n        <div class=\"checkbox-wrapper\">\r\n            <div class=\"checkbox-item\">\r\n                <label");
{
    Object var_attrvalue10 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "fieldId"), "html");
    {
        boolean var_shoulddisplayattr13 = ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10)));
        if (var_shoulddisplayattr13) {
            out.write(" for");
            {
                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                if (!var_istrueattr12) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue10));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(" class=\"checkbox-label\">\r\n                    <input");
{
    Object var_attrvalue14 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "fieldName"), "html");
    {
        boolean var_shoulddisplayattr17 = ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14)));
        if (var_shoulddisplayattr17) {
            out.write(" name");
            {
                boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                if (!var_istrueattr16) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue14));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(" type=\"checkbox\"");
{
    Object var_attrvalue18 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "fieldValue"), "html");
    {
        boolean var_shoulddisplayattr21 = ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18)));
        if (var_shoulddisplayattr21) {
            out.write(" value");
            {
                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                if (!var_istrueattr20) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue18));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n                    <div>");
{
    String var_22 = (("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "fieldTitle"), "html"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_22));
}
out.write("</div>\r\n                </label>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

