/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.personnelpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class personnelpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
_global_model = renderContext.call("use", com.techcombank.core.models.PersonnelPanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "personnelName");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n  ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n    <div class=\"personal-section pad-wrapper\"");
            {
                String var_attrcontent1 = ("--right-panel-background-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "rightPanelBackgroundColor"), "styleString")));
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n      <div class=\"personal-container\">\r\n        <div class=\"personal-panel\">\r\n          <div class=\"personal-info\">\r\n            <div class=\"personal-info-container\">\r\n              <div class=\"personal-info-content\">\r\n                <div class=\"personal-info-description\">");
            {
                String var_2 = (("\r\n                  " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "personnelDescription"), "html"))) + "\r\n                ");
                out.write(renderContext.getObjectModel().toString(var_2));
            }
            out.write("</div>\r\n                <h3 class=\"personal-info-name\">");
            {
                Object var_3 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "personnelName"), "text");
                out.write(renderContext.getObjectModel().toString(var_3));
            }
            out.write("</h3>\r\n                <div class=\"personal-info-position\">");
            {
                Object var_4 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "personnelPosition"), "text");
                out.write(renderContext.getObjectModel().toString(var_4));
            }
            out.write("</div>\r\n                <div class=\"personal-link-button\">\r\n                  <a");
            {
                Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_model, "panelButtonLinkUrl");
                {
                    Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "uri");
                    {
                        boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                        if (var_shoulddisplayattr8) {
                            out.write(" href");
                            {
                                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                if (!var_istrueattr7) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue9 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "openInNewTab")) ? "_blank" : "_self");
                {
                    Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                    {
                        boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                        if (var_shoulddisplayattr12) {
                            out.write(" target");
                            {
                                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                                if (!var_istrueattr11) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue13 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "noFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
                    {
                        boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                        if (var_shoulddisplayattr16) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                                if (!var_istrueattr15) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" class=\"personal-button\" style=\"justify-content: flex-start\" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent17 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "panelButtonText"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                out.write("\"");
            }
            {
                String var_attrcontent18 = (("{'webInteractions': {'name': 'Personnel panel','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "webInteractionType"), "attribute"))) + "'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                out.write("\"");
            }
            out.write(">\r\n                    <span class=\"bio-button-text\">");
            {
                Object var_19 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "panelButtonText"), "text");
                out.write(renderContext.getObjectModel().toString(var_19));
            }
            out.write("</span>\r\n                  </a>\r\n                </div>\r\n              </div>\r\n              <span class=\"left-Square\"></span>\r\n            </div>\r\n          </div>\r\n          <div class=\"empty-col-bio\"></div>\r\n          <div class=\"personal-image\">\r\n            <div class=\"personal-image-container\">\r\n              <img class=\"image-personal\"");
            {
                Object var_attrvalue20 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAltText");
                {
                    Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
                    {
                        boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (!((Object)false).equals(var_attrvalue20))));
                        if (var_shoulddisplayattr23) {
                            out.write(" alt");
                            {
                                boolean var_istrueattr22 = (var_attrvalue20.equals(true));
                                if (!var_istrueattr22) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(_global_model, "personnelImage");
                {
                    Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "uri");
                    {
                        boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                        if (var_shoulddisplayattr27) {
                            out.write(" src");
                            {
                                boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                if (!var_istrueattr26) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" decoding=\"async\" data-nimg=\"intrinsic\"/>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar28 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions29_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions29 = obj().with("isEmpty", var_templateoptions29_field$_isempty);
            callUnit(out, renderContext, var_templatevar28, var_templateoptions29);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

