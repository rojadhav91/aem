/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.options;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class options__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_options = null;
Object _dynamic_properties = bindings.get("properties");
Collection var_collectionvar14_list_coerced$ = null;
Collection var_collectionvar68_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_options = renderContext.call("use", com.adobe.cq.wcm.core.components.models.form.Options.class.getName(), obj());
out.write("\r\n    ");
{
    boolean var_testvariable5 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "checkbox")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "radio")));
    if (var_testvariable5) {
        out.write("\r\n        <div");
        {
            String var_attrcontent6 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "attribute")) + " section ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "analytics")) ? "analytics-form-field" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
            out.write("\"");
        }
        {
            Object var_attrvalue7 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRequired")) ? renderContext.getObjectModel().resolveProperty(_dynamic_properties, "requiredMsg") : "");
            {
                Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                {
                    boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                    if (var_shoulddisplayattr10) {
                        out.write(" data-required");
                        {
                            boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                            if (!var_istrueattr9) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n            <div");
        {
            String var_attrcontent11 = ("cmp-form-options__field--" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent11));
            out.write("\"");
        }
        out.write(">\r\n                <label>");
        {
            String var_12 = ((" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_options, "title"), "text"))) + " ");
            out.write(renderContext.getObjectModel().toString(var_12));
        }
        {
            Object var_testvariable13 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRequired")) ? renderContext.getObjectModel().resolveProperty(_global_options, "title") : renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRequired")));
            if (renderContext.getObjectModel().toBoolean(var_testvariable13)) {
                out.write("<span>*</span>");
            }
        }
        out.write("</label>\r\n                ");
        {
            Object var_collectionvar14 = renderContext.getObjectModel().resolveProperty(_global_options, "items");
            {
                long var_size15 = ((var_collectionvar14_list_coerced$ == null ? (var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14)) : var_collectionvar14_list_coerced$).size());
                {
                    boolean var_notempty16 = (var_size15 > 0);
                    if (var_notempty16) {
                        {
                            long var_end19 = var_size15;
                            {
                                boolean var_validstartstepend20 = (((0 < var_size15) && true) && (var_end19 > 0));
                                if (var_validstartstepend20) {
                                    out.write("<div");
                                    {
                                        String var_attrcontent21 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "attribute")) + "-wrapper ") + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "radio")) ? "one-column small" : ""), "attribute")));
                                        out.write(" class=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                        out.write("\"");
                                    }
                                    out.write(">");
                                    if (var_collectionvar14_list_coerced$ == null) {
                                        var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14);
                                    }
                                    long var_index22 = 0;
                                    for (Object optionitem : var_collectionvar14_list_coerced$) {
                                        {
                                            boolean var_traversal24 = (((var_index22 >= 0) && (var_index22 <= var_end19)) && true);
                                            if (var_traversal24) {
                                                out.write("\r\n                    <div");
                                                {
                                                    String var_attrcontent25 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "attribute")) + "-item");
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                        <label for=\"\"");
                                                {
                                                    String var_attrcontent26 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "attribute")) + "-label");
                                                    out.write(" class=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                    out.write("\"");
                                                }
                                                out.write(">\r\n                            <input");
                                                {
                                                    Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_options, "name");
                                                    {
                                                        Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                                                            if (var_shoulddisplayattr34) {
                                                                out.write(" name");
                                                                {
                                                                    boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                                                    if (!var_istrueattr33) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value");
                                                    {
                                                        Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                                                            if (var_shoulddisplayattr38) {
                                                                out.write(" type");
                                                                {
                                                                    boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                                                    if (!var_istrueattr37) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(optionitem, "value");
                                                    {
                                                        Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                                                            if (var_shoulddisplayattr42) {
                                                                out.write(" value");
                                                                {
                                                                    boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                                                    if (!var_istrueattr41) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(optionitem, "disabled");
                                                    {
                                                        Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                            if (var_shoulddisplayattr46) {
                                                                out.write(" disabled");
                                                                {
                                                                    boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                                    if (!var_istrueattr45) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue_checked27 = renderContext.getObjectModel().resolveProperty(optionitem, "selected");
                                                    {
                                                        Object var_attrvalueescaped_checked28 = renderContext.call("xss", var_attrvalue_checked27, "attribute", "checked");
                                                        {
                                                            boolean var_shoulddisplayattr_checked30 = (((null != var_attrvalueescaped_checked28) && (!"".equals(var_attrvalueescaped_checked28))) && ((!"".equals(var_attrvalue_checked27)) && (!((Object)false).equals(var_attrvalue_checked27))));
                                                            if (var_shoulddisplayattr_checked30) {
                                                                out.write(" checked");
                                                                {
                                                                    boolean var_istruevalue_checked29 = (var_attrvalue_checked27.equals(true));
                                                                    if (!var_istruevalue_checked29) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrvalueescaped_checked28));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                            <div><p>");
                                                {
                                                    Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(optionitem, "text"), "html");
                                                    out.write(renderContext.getObjectModel().toString(var_47));
                                                }
                                                out.write("</p></div>\r\n                        </label>\r\n                    </div>\r\n                ");
                                            }
                                        }
                                        var_index22++;
                                    }
                                    out.write("</div>");
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar14_list_coerced$ = null;
        }
        out.write("\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n\r\n    ");
{
    boolean var_testvariable48 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "drop-down")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_options, "type"), "value"), "multi-drop-down")));
    if (var_testvariable48) {
        out.write("\r\n        <div");
        {
            String var_attrcontent49 = ("dropdown section " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "analytics")) ? "analytics-form-field" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent49));
            out.write("\"");
        }
        {
            Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(_global_options, "helpMessage");
            {
                Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
                {
                    boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                    if (var_shoulddisplayattr53) {
                        out.write(" data-type");
                        {
                            boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                            if (!var_istrueattr52) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue54 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRequired")) ? renderContext.getObjectModel().resolveProperty(_dynamic_properties, "requiredMsg") : "");
            {
                Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "attribute");
                {
                    boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                    if (var_shoulddisplayattr57) {
                        out.write(" data-required");
                        {
                            boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                            if (!var_istrueattr56) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n            <div class=\"cmp-form-options__drop-down\">\r\n                <label>");
        {
            Object var_58 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_options, "title"), "text");
            out.write(renderContext.getObjectModel().toString(var_58));
        }
        {
            Object var_testvariable59 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRequired");
            if (renderContext.getObjectModel().toBoolean(var_testvariable59)) {
                out.write("<span> *</span>");
            }
        }
        out.write("</label>\r\n                <div class=\"dropdown-wrapper\"");
        {
            Object var_attrvalue60 = renderContext.getObjectModel().resolveProperty(_global_options, "helpMessage");
            {
                Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "attribute");
                {
                    boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                    if (var_shoulddisplayattr63) {
                        out.write(" data-placeholder");
                        {
                            boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                            if (!var_istrueattr62) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-selected=\"\">\r\n                    <div class=\"dropdown-inputbase\" role=\"button\">\r\n                        <input class=\"dropdown-input\"");
        {
            Object var_attrvalue64 = renderContext.getObjectModel().resolveProperty(_global_options, "name");
            {
                Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                {
                    boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                    if (var_shoulddisplayattr67) {
                        out.write(" name");
                        {
                            boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                            if (!var_istrueattr66) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" aria-hidden=\"true\" tabindex=\"-1\" value=\"\"/>\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\" alt=\"\"/>\r\n                    </div>\r\n                    <div class=\"dropdown-backdrop\" hidden></div>\r\n                    ");
        {
            Object var_collectionvar68 = renderContext.getObjectModel().resolveProperty(_global_options, "items");
            {
                long var_size69 = ((var_collectionvar68_list_coerced$ == null ? (var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68)) : var_collectionvar68_list_coerced$).size());
                {
                    boolean var_notempty70 = (var_size69 > 0);
                    if (var_notempty70) {
                        {
                            long var_end73 = var_size69;
                            {
                                boolean var_validstartstepend74 = (((0 < var_size69) && true) && (var_end73 > 0));
                                if (var_validstartstepend74) {
                                    out.write("<ul class=\"list-dropdown\" hidden>");
                                    if (var_collectionvar68_list_coerced$ == null) {
                                        var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68);
                                    }
                                    long var_index75 = 0;
                                    for (Object optionitem : var_collectionvar68_list_coerced$) {
                                        {
                                            boolean var_traversal77 = (((var_index75 >= 0) && (var_index75 <= var_end73)) && true);
                                            if (var_traversal77) {
                                                out.write("\r\n                        <li class=\"dropdown-item\" role=\"option\"");
                                                {
                                                    Object var_attrvalue78 = renderContext.getObjectModel().resolveProperty(optionitem, "value");
                                                    {
                                                        Object var_attrcontent79 = renderContext.call("xss", var_attrvalue78, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr81 = (((null != var_attrcontent79) && (!"".equals(var_attrcontent79))) && ((!"".equals(var_attrvalue78)) && (!((Object)false).equals(var_attrvalue78))));
                                                            if (var_shoulddisplayattr81) {
                                                                out.write(" data-value");
                                                                {
                                                                    boolean var_istrueattr80 = (var_attrvalue78.equals(true));
                                                                    if (!var_istrueattr80) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent79));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue82 = renderContext.getObjectModel().resolveProperty(optionitem, "selected");
                                                    {
                                                        Object var_attrcontent83 = renderContext.call("xss", var_attrvalue82, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr85 = (((null != var_attrcontent83) && (!"".equals(var_attrcontent83))) && ((!"".equals(var_attrvalue82)) && (!((Object)false).equals(var_attrvalue82))));
                                                            if (var_shoulddisplayattr85) {
                                                                out.write(" data-selected");
                                                                {
                                                                    boolean var_istrueattr84 = (var_attrvalue82.equals(true));
                                                                    if (!var_istrueattr84) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent83));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue86 = renderContext.getObjectModel().resolveProperty(optionitem, "disabled");
                                                    {
                                                        Object var_attrcontent87 = renderContext.call("xss", var_attrvalue86, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr89 = (((null != var_attrcontent87) && (!"".equals(var_attrcontent87))) && ((!"".equals(var_attrvalue86)) && (!((Object)false).equals(var_attrvalue86))));
                                                            if (var_shoulddisplayattr89) {
                                                                out.write(" disabled");
                                                                {
                                                                    boolean var_istrueattr88 = (var_attrvalue86.equals(true));
                                                                    if (!var_istrueattr88) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent87));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">\r\n                            <span>");
                                                {
                                                    Object var_90 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(optionitem, "text"), "html");
                                                    out.write(renderContext.getObjectModel().toString(var_90));
                                                }
                                                out.write("</span>\r\n                        </li>\r\n                    ");
                                            }
                                        }
                                        var_index75++;
                                    }
                                    out.write("</ul>");
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar68_list_coerced$ = null;
        }
        out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
    }
}
out.write("\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

