/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.atmbranchmap;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class atmbranchmap__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_atmbranchmapmodel = null;
Object _dynamic_currentpage = bindings.get("currentpage");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n   <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_atmbranchmapmodel = renderContext.call("use", com.techcombank.core.models.AtmBranchMapModel.class.getName(), obj());
out.write("\r\n   ");
{
    boolean var_testvariable5 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable5) {
        out.write("   \r\n      <section class=\"atm-map-component\" data-api=\"/graphql/execute.json/techcombank/\"");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" data-api-branch");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-api-branch-suffix=\"\"");
        {
            Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "googleMapValue");
            {
                Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                {
                    boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                    if (var_shoulddisplayattr13) {
                        out.write(" data-map-key");
                        {
                            boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                            if (!var_istrueattr12) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "noOptionMessage");
            {
                Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                {
                    boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                    if (var_shoulddisplayattr17) {
                        out.write(" data-no-option");
                        {
                            boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                            if (!var_istrueattr16) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "resultMessage");
            {
                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                {
                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                    if (var_shoulddisplayattr21) {
                        out.write(" data-result-message");
                        {
                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                            if (!var_istrueattr20) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "districtLabel");
            {
                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                {
                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                    if (var_shoulddisplayattr25) {
                        out.write(" data-district-place-holder");
                        {
                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                            if (!var_istrueattr24) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchLabel");
            {
                Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                {
                    boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                    if (var_shoulddisplayattr29) {
                        out.write(" data-branch-label");
                        {
                            boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                            if (!var_istrueattr28) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n         <div class=\"branches-root-item\">\r\n            <div class=\"branches-atm-locate\">\r\n               <div class=\"branches-radio-secondary\">\r\n                  <div class=\"branches-radio-item\">\r\n                     <label class=\"branches-radio-container\" data-tracking-click-event=\"branchLocator\"");
        {
            String var_attrcontent30 = (((("{'customerType' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalLabel"), "attribute"))) + "','branchSearchType' : '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmLabel"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent30));
            out.write("\"");
        }
        {
            String var_attrcontent31 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent31));
            out.write("\"");
        }
        out.write(">\r\n                        <input checked name=\"youAre\" type=\"radio\" value=\"11\"/>\r\n                        <span class=\"branches-radio-cycle\"></span> \r\n                        <span class=\"branches-radio-checkmark\">\r\n                           <span>");
        {
            Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_32));
        }
        out.write("</span>\r\n                           <span>\r\n                              <picture>\r\n                                 <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconMobileImagePath");
            {
                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                {
                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                    if (var_shoulddisplayattr36) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                            if (!var_istrueattr35) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconMobileImagePath");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconWebImagePath");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconWebImagePath");
            {
                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                {
                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                    if (var_shoulddisplayattr48) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                            if (!var_istrueattr47) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <img");
        {
            Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconAlt");
            {
                Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                {
                    boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                    if (var_shoulddisplayattr52) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                            if (!var_istrueattr51) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalIconWebImagePath");
            {
                Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "uri");
                {
                    boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                    if (var_shoulddisplayattr56) {
                        out.write(" src");
                        {
                            boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                            if (!var_istrueattr55) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/> \r\n                              </picture>\r\n                           </span>\r\n                        </span>\r\n                     </label>\r\n                     <label class=\"branches-radio-container\" data-tracking-click-event=\"branchLocator\"");
        {
            String var_attrcontent57 = (((("{'customerType' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessLabel"), "attribute"))) + "','branchSearchType' : '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmLabel"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent57));
            out.write("\"");
        }
        {
            String var_attrcontent58 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent58));
            out.write("\"");
        }
        out.write(">\r\n                        <input name=\"youAre\" type=\"radio\" value=\"12\"/> <span class=\"branches-radio-cycle\"></span> \r\n                        <span class=\"branches-radio-checkmark\">\r\n                           <span>");
        {
            Object var_59 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_59));
        }
        out.write("</span>\r\n                           <span>\r\n                              <picture>\r\n                                 <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue60 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconMobileImagePath");
            {
                Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "attribute");
                {
                    boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                    if (var_shoulddisplayattr63) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                            if (!var_istrueattr62) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue64 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconMobileImagePath");
            {
                Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                {
                    boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                    if (var_shoulddisplayattr67) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                            if (!var_istrueattr66) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue68 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconWebImagePath");
            {
                Object var_attrcontent69 = renderContext.call("xss", var_attrvalue68, "attribute");
                {
                    boolean var_shoulddisplayattr71 = (((null != var_attrcontent69) && (!"".equals(var_attrcontent69))) && ((!"".equals(var_attrvalue68)) && (!((Object)false).equals(var_attrvalue68))));
                    if (var_shoulddisplayattr71) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr70 = (var_attrvalue68.equals(true));
                            if (!var_istrueattr70) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent69));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue72 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconWebImagePath");
            {
                Object var_attrcontent73 = renderContext.call("xss", var_attrvalue72, "attribute");
                {
                    boolean var_shoulddisplayattr75 = (((null != var_attrcontent73) && (!"".equals(var_attrcontent73))) && ((!"".equals(var_attrvalue72)) && (!((Object)false).equals(var_attrvalue72))));
                    if (var_shoulddisplayattr75) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr74 = (var_attrvalue72.equals(true));
                            if (!var_istrueattr74) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent73));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <img");
        {
            Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconAlt");
            {
                Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "attribute");
                {
                    boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                    if (var_shoulddisplayattr79) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                            if (!var_istrueattr78) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue80 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "businessIconWebImagePath");
            {
                Object var_attrcontent81 = renderContext.call("xss", var_attrvalue80, "uri");
                {
                    boolean var_shoulddisplayattr83 = (((null != var_attrcontent81) && (!"".equals(var_attrcontent81))) && ((!"".equals(var_attrvalue80)) && (!((Object)false).equals(var_attrvalue80))));
                    if (var_shoulddisplayattr83) {
                        out.write(" src");
                        {
                            boolean var_istrueattr82 = (var_attrvalue80.equals(true));
                            if (!var_istrueattr82) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent81));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/> \r\n                              </picture>\r\n                           </span>\r\n                        </span>\r\n                     </label>\r\n                  </div>\r\n               </div>\r\n            </div>\r\n            <div class=\"head-title\">\r\n               <h2>");
        {
            Object var_84 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "title"), "text");
            out.write(renderContext.getObjectModel().toString(var_84));
        }
        out.write("</h2>\r\n            </div>\r\n         </div>\r\n         <div class=\"atm-map-component__container\">\r\n            <div class=\"atm-map_container\">\r\n               <div class=\"search-container\">\r\n                  <div class=\"search-section\">\r\n                     <div class=\"search-input\">\r\n                        <div class=\"search\">\r\n                           <div class=\"search-input_icon\">\r\n                              <picture>\r\n                                 <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconMobileImagePath");
            {
                Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "attribute");
                {
                    boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
                    if (var_shoulddisplayattr88) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                            if (!var_istrueattr87) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue89 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconMobileImagePath");
            {
                Object var_attrcontent90 = renderContext.call("xss", var_attrvalue89, "attribute");
                {
                    boolean var_shoulddisplayattr92 = (((null != var_attrcontent90) && (!"".equals(var_attrcontent90))) && ((!"".equals(var_attrvalue89)) && (!((Object)false).equals(var_attrvalue89))));
                    if (var_shoulddisplayattr92) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr91 = (var_attrvalue89.equals(true));
                            if (!var_istrueattr91) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue93 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconWebImagePath");
            {
                Object var_attrcontent94 = renderContext.call("xss", var_attrvalue93, "attribute");
                {
                    boolean var_shoulddisplayattr96 = (((null != var_attrcontent94) && (!"".equals(var_attrcontent94))) && ((!"".equals(var_attrvalue93)) && (!((Object)false).equals(var_attrvalue93))));
                    if (var_shoulddisplayattr96) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr95 = (var_attrvalue93.equals(true));
                            if (!var_istrueattr95) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent94));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue97 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconWebImagePath");
            {
                Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "attribute");
                {
                    boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                    if (var_shoulddisplayattr100) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                            if (!var_istrueattr99) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 <img");
        {
            Object var_attrvalue101 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconAlt");
            {
                Object var_attrcontent102 = renderContext.call("xss", var_attrvalue101, "attribute");
                {
                    boolean var_shoulddisplayattr104 = (((null != var_attrcontent102) && (!"".equals(var_attrcontent102))) && ((!"".equals(var_attrvalue101)) && (!((Object)false).equals(var_attrvalue101))));
                    if (var_shoulddisplayattr104) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr103 = (var_attrvalue101.equals(true));
                            if (!var_istrueattr103) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue105 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchIconWebImagePath");
            {
                Object var_attrcontent106 = renderContext.call("xss", var_attrvalue105, "uri");
                {
                    boolean var_shoulddisplayattr108 = (((null != var_attrcontent106) && (!"".equals(var_attrcontent106))) && ((!"".equals(var_attrvalue105)) && (!((Object)false).equals(var_attrvalue105))));
                    if (var_shoulddisplayattr108) {
                        out.write(" src");
                        {
                            boolean var_istrueattr107 = (var_attrvalue105.equals(true));
                            if (!var_istrueattr107) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent106));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                              </picture>\r\n                           </div>\r\n                           <input aria-autocomplete=\"list\" aria-invalid=\"false\" autocapitalize=\"none\" autocomplete=\"off\" id=\"test\"");
        {
            Object var_attrvalue109 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "searchPlaceHolder");
            {
                Object var_attrcontent110 = renderContext.call("xss", var_attrvalue109, "attribute");
                {
                    boolean var_shoulddisplayattr112 = (((null != var_attrcontent110) && (!"".equals(var_attrcontent110))) && ((!"".equals(var_attrvalue109)) && (!((Object)false).equals(var_attrvalue109))));
                    if (var_shoulddisplayattr112) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr111 = (var_attrvalue109.equals(true));
                            if (!var_istrueattr111) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent110));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" spellcheck=\"false\" type=\"text\" value=\"\"/>\r\n                        </div>\r\n                        <div id=\"atm-map-autocomplete\"></div>\r\n                     </div>\r\n                     <div class=\"search-filter\">\r\n                        <div class=\"city filter\">\r\n                           <div class=\"option\">\r\n                              <input aria-hidden=\"true\" name=\"cities\"");
        {
            Object var_attrvalue113 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "selectDropdown");
            {
                Object var_attrcontent114 = renderContext.call("xss", var_attrvalue113, "attribute");
                {
                    boolean var_shoulddisplayattr116 = (((null != var_attrcontent114) && (!"".equals(var_attrcontent114))) && ((!"".equals(var_attrvalue113)) && (!((Object)false).equals(var_attrvalue113))));
                    if (var_shoulddisplayattr116) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr115 = (var_attrvalue113.equals(true));
                            if (!var_istrueattr115) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent114));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" tabindex=\"-1\"/> <img alt=\"arrow down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\"/>\r\n                              <div class=\"dropdown-backdrop\"></div>\r\n                           </div>\r\n                           <div class=\"select-options\">\r\n                              <ul>\r\n                                 <li value=\"all\">");
        {
            Object var_117 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "selectDropdown"), "text");
            out.write(renderContext.getObjectModel().toString(var_117));
        }
        out.write("</li>\r\n                                 <li value=\"nearest\">");
        {
            Object var_118 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchDropdown"), "text");
            out.write(renderContext.getObjectModel().toString(var_118));
        }
        out.write("</li>\r\n                              </ul>\r\n                           </div>\r\n                        </div>\r\n                        <div class=\"district filter disable\">\r\n                           <div class=\"option\">\r\n                              <input aria-hidden=\"true\" name=\"districts\"");
        {
            Object var_attrvalue119 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "districtLabel");
            {
                Object var_attrcontent120 = renderContext.call("xss", var_attrvalue119, "attribute");
                {
                    boolean var_shoulddisplayattr122 = (((null != var_attrcontent120) && (!"".equals(var_attrcontent120))) && ((!"".equals(var_attrvalue119)) && (!((Object)false).equals(var_attrvalue119))));
                    if (var_shoulddisplayattr122) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr121 = (var_attrvalue119.equals(true));
                            if (!var_istrueattr121) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent120));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" tabindex=\"-1\"/>\r\n                              <img alt=\"arrow down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\"/>\r\n                              <div class=\"dropdown-backdrop\"></div>\r\n                           </div>\r\n                           <div class=\"select-options\">\r\n                              <ul>\r\n                                 <li value=\"all\">");
        {
            Object var_123 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "selectDropdown"), "text");
            out.write(renderContext.getObjectModel().toString(var_123));
        }
        out.write("</li>\r\n                              </ul>\r\n                           </div>\r\n                        </div>\r\n                     </div>\r\n                     <div class=\"search-type\">\r\n                        <div class=\"items\">\r\n                           <button class=\"atm-type type clicked\" data-tracking-click-event=\"branchLocator\"");
        {
            String var_attrcontent124 = (((("{'customerType' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalLabel"), "attribute"))) + "','branchSearchType' : '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmLabel"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent124));
            out.write("\"");
        }
        {
            String var_attrcontent125 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent125));
            out.write("\"");
        }
        out.write(">\r\n                              <div class=\"atm-icon icon\">                                                                \r\n                                 <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue126 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconMobileImagePath");
            {
                Object var_attrcontent127 = renderContext.call("xss", var_attrvalue126, "attribute");
                {
                    boolean var_shoulddisplayattr129 = (((null != var_attrcontent127) && (!"".equals(var_attrcontent127))) && ((!"".equals(var_attrvalue126)) && (!((Object)false).equals(var_attrvalue126))));
                    if (var_shoulddisplayattr129) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr128 = (var_attrvalue126.equals(true));
                            if (!var_istrueattr128) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent127));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue130 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconMobileImagePath");
            {
                Object var_attrcontent131 = renderContext.call("xss", var_attrvalue130, "attribute");
                {
                    boolean var_shoulddisplayattr133 = (((null != var_attrcontent131) && (!"".equals(var_attrcontent131))) && ((!"".equals(var_attrvalue130)) && (!((Object)false).equals(var_attrvalue130))));
                    if (var_shoulddisplayattr133) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr132 = (var_attrvalue130.equals(true));
                            if (!var_istrueattr132) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent131));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue134 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconWebImagePath");
            {
                Object var_attrcontent135 = renderContext.call("xss", var_attrvalue134, "attribute");
                {
                    boolean var_shoulddisplayattr137 = (((null != var_attrcontent135) && (!"".equals(var_attrcontent135))) && ((!"".equals(var_attrvalue134)) && (!((Object)false).equals(var_attrvalue134))));
                    if (var_shoulddisplayattr137) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr136 = (var_attrvalue134.equals(true));
                            if (!var_istrueattr136) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent135));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue138 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconWebImagePath");
            {
                Object var_attrcontent139 = renderContext.call("xss", var_attrvalue138, "attribute");
                {
                    boolean var_shoulddisplayattr141 = (((null != var_attrcontent139) && (!"".equals(var_attrcontent139))) && ((!"".equals(var_attrvalue138)) && (!((Object)false).equals(var_attrvalue138))));
                    if (var_shoulddisplayattr141) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr140 = (var_attrvalue138.equals(true));
                            if (!var_istrueattr140) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent139));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <img");
        {
            Object var_attrvalue142 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconAlt");
            {
                Object var_attrcontent143 = renderContext.call("xss", var_attrvalue142, "attribute");
                {
                    boolean var_shoulddisplayattr145 = (((null != var_attrcontent143) && (!"".equals(var_attrcontent143))) && ((!"".equals(var_attrvalue142)) && (!((Object)false).equals(var_attrvalue142))));
                    if (var_shoulddisplayattr145) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr144 = (var_attrvalue142.equals(true));
                            if (!var_istrueattr144) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent143));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"hide\"");
        {
            Object var_attrvalue146 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmIconWebImagePath");
            {
                Object var_attrcontent147 = renderContext.call("xss", var_attrvalue146, "uri");
                {
                    boolean var_shoulddisplayattr149 = (((null != var_attrcontent147) && (!"".equals(var_attrcontent147))) && ((!"".equals(var_attrvalue146)) && (!((Object)false).equals(var_attrvalue146))));
                    if (var_shoulddisplayattr149) {
                        out.write(" src");
                        {
                            boolean var_istrueattr148 = (var_attrvalue146.equals(true));
                            if (!var_istrueattr148) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent147));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 </picture>\r\n                              </div>\r\n                              <span>");
        {
            Object var_150 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "atmLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_150));
        }
        out.write("</span>\r\n                           </button>\r\n                           <button class=\"cdm-type type\" data-tracking-click-event=\"branchLocator\"");
        {
            String var_attrcontent151 = (((("{'customerType' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalLabel"), "attribute"))) + "','branchSearchType' : '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmLabel"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent151));
            out.write("\"");
        }
        {
            String var_attrcontent152 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent152));
            out.write("\"");
        }
        out.write(">\r\n                              <div class=\"cdm-icon icon\">                                                               \r\n                                 <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue153 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconMobileImagePath");
            {
                Object var_attrcontent154 = renderContext.call("xss", var_attrvalue153, "attribute");
                {
                    boolean var_shoulddisplayattr156 = (((null != var_attrcontent154) && (!"".equals(var_attrcontent154))) && ((!"".equals(var_attrvalue153)) && (!((Object)false).equals(var_attrvalue153))));
                    if (var_shoulddisplayattr156) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr155 = (var_attrvalue153.equals(true));
                            if (!var_istrueattr155) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent154));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue157 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconMobileImagePath");
            {
                Object var_attrcontent158 = renderContext.call("xss", var_attrvalue157, "attribute");
                {
                    boolean var_shoulddisplayattr160 = (((null != var_attrcontent158) && (!"".equals(var_attrcontent158))) && ((!"".equals(var_attrvalue157)) && (!((Object)false).equals(var_attrvalue157))));
                    if (var_shoulddisplayattr160) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr159 = (var_attrvalue157.equals(true));
                            if (!var_istrueattr159) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent158));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue161 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconWebImagePath");
            {
                Object var_attrcontent162 = renderContext.call("xss", var_attrvalue161, "attribute");
                {
                    boolean var_shoulddisplayattr164 = (((null != var_attrcontent162) && (!"".equals(var_attrcontent162))) && ((!"".equals(var_attrvalue161)) && (!((Object)false).equals(var_attrvalue161))));
                    if (var_shoulddisplayattr164) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr163 = (var_attrvalue161.equals(true));
                            if (!var_istrueattr163) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent162));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue165 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconWebImagePath");
            {
                Object var_attrcontent166 = renderContext.call("xss", var_attrvalue165, "attribute");
                {
                    boolean var_shoulddisplayattr168 = (((null != var_attrcontent166) && (!"".equals(var_attrcontent166))) && ((!"".equals(var_attrvalue165)) && (!((Object)false).equals(var_attrvalue165))));
                    if (var_shoulddisplayattr168) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr167 = (var_attrvalue165.equals(true));
                            if (!var_istrueattr167) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent166));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <img");
        {
            Object var_attrvalue169 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconAlt");
            {
                Object var_attrcontent170 = renderContext.call("xss", var_attrvalue169, "attribute");
                {
                    boolean var_shoulddisplayattr172 = (((null != var_attrcontent170) && (!"".equals(var_attrcontent170))) && ((!"".equals(var_attrvalue169)) && (!((Object)false).equals(var_attrvalue169))));
                    if (var_shoulddisplayattr172) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr171 = (var_attrvalue169.equals(true));
                            if (!var_istrueattr171) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent170));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"hide\"");
        {
            Object var_attrvalue173 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmIconWebImagePath");
            {
                Object var_attrcontent174 = renderContext.call("xss", var_attrvalue173, "uri");
                {
                    boolean var_shoulddisplayattr176 = (((null != var_attrcontent174) && (!"".equals(var_attrcontent174))) && ((!"".equals(var_attrvalue173)) && (!((Object)false).equals(var_attrvalue173))));
                    if (var_shoulddisplayattr176) {
                        out.write(" src");
                        {
                            boolean var_istrueattr175 = (var_attrvalue173.equals(true));
                            if (!var_istrueattr175) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent174));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 </picture>\r\n                              </div>\r\n                              <span>");
        {
            Object var_177 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "cdmLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_177));
        }
        out.write("</span>\r\n                           </button>\r\n                           <button class=\"branch-type type\" data-tracking-click-event=\"branchLocator\"");
        {
            String var_attrcontent178 = (((("{'customerType' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "personalLabel"), "attribute"))) + "','branchSearchType' : '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchLabel"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent178));
            out.write("\"");
        }
        {
            String var_attrcontent179 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent179));
            out.write("\"");
        }
        out.write(">\r\n                              <div class=\"branch-icon icon\">                                                               \r\n                                 <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue180 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconMobileImagePath");
            {
                Object var_attrcontent181 = renderContext.call("xss", var_attrvalue180, "attribute");
                {
                    boolean var_shoulddisplayattr183 = (((null != var_attrcontent181) && (!"".equals(var_attrcontent181))) && ((!"".equals(var_attrvalue180)) && (!((Object)false).equals(var_attrvalue180))));
                    if (var_shoulddisplayattr183) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr182 = (var_attrvalue180.equals(true));
                            if (!var_istrueattr182) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent181));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue184 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconMobileImagePath");
            {
                Object var_attrcontent185 = renderContext.call("xss", var_attrvalue184, "attribute");
                {
                    boolean var_shoulddisplayattr187 = (((null != var_attrcontent185) && (!"".equals(var_attrcontent185))) && ((!"".equals(var_attrvalue184)) && (!((Object)false).equals(var_attrvalue184))));
                    if (var_shoulddisplayattr187) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr186 = (var_attrvalue184.equals(true));
                            if (!var_istrueattr186) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent185));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue188 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconWebImagePath");
            {
                Object var_attrcontent189 = renderContext.call("xss", var_attrvalue188, "attribute");
                {
                    boolean var_shoulddisplayattr191 = (((null != var_attrcontent189) && (!"".equals(var_attrcontent189))) && ((!"".equals(var_attrvalue188)) && (!((Object)false).equals(var_attrvalue188))));
                    if (var_shoulddisplayattr191) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr190 = (var_attrvalue188.equals(true));
                            if (!var_istrueattr190) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent189));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue192 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconWebImagePath");
            {
                Object var_attrcontent193 = renderContext.call("xss", var_attrvalue192, "attribute");
                {
                    boolean var_shoulddisplayattr195 = (((null != var_attrcontent193) && (!"".equals(var_attrcontent193))) && ((!"".equals(var_attrvalue192)) && (!((Object)false).equals(var_attrvalue192))));
                    if (var_shoulddisplayattr195) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr194 = (var_attrvalue192.equals(true));
                            if (!var_istrueattr194) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent193));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                    <img");
        {
            Object var_attrvalue196 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconAlt");
            {
                Object var_attrcontent197 = renderContext.call("xss", var_attrvalue196, "attribute");
                {
                    boolean var_shoulddisplayattr199 = (((null != var_attrcontent197) && (!"".equals(var_attrcontent197))) && ((!"".equals(var_attrvalue196)) && (!((Object)false).equals(var_attrvalue196))));
                    if (var_shoulddisplayattr199) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr198 = (var_attrvalue196.equals(true));
                            if (!var_istrueattr198) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent197));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"hide\"");
        {
            Object var_attrvalue200 = renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchIconWebImagePath");
            {
                Object var_attrcontent201 = renderContext.call("xss", var_attrvalue200, "uri");
                {
                    boolean var_shoulddisplayattr203 = (((null != var_attrcontent201) && (!"".equals(var_attrcontent201))) && ((!"".equals(var_attrvalue200)) && (!((Object)false).equals(var_attrvalue200))));
                    if (var_shoulddisplayattr203) {
                        out.write(" src");
                        {
                            boolean var_istrueattr202 = (var_attrvalue200.equals(true));
                            if (!var_istrueattr202) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent201));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                 </picture>\r\n                              </div>\r\n                              <span>");
        {
            Object var_204 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "branchLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_204));
        }
        out.write("</span>\r\n                           </button>\r\n                        </div>\r\n                     </div>\r\n                  </div>\r\n                  <div class=\"search-result\">\r\n                     <div class=\"result-message\"></div>\r\n                     <div class=\"results\">\r\n                        <div class=\"show-list\">\r\n                           <div class=\"atm-list\"></div>\r\n                           <div class=\"cdm-list\"></div>\r\n                           <div class=\"branch-list\"></div>\r\n                        </div>\r\n                        <div class=\"detail-item\">\r\n                           <div class=\"above\">\r\n                              <div class=\"comeback\">\r\n                                 <img alt src=\"https://d1kndcit1zrj97.cloudfront.net/uploads/left_arrow_1e3443f554.svg?w=1920&q=75\"/>\r\n                                 <span>");
        {
            Object var_205 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "backLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_205));
        }
        out.write("</span>\r\n                              </div>\r\n                              <div class=\"atm\">\r\n                                 <div>\r\n                                    <img alt src=\"\"/>\r\n                                 </div>\r\n                                 <p class=\"name\"></p>\r\n                              </div>\r\n                           </div>\r\n                           <div class=\"under\">\r\n                              <p>");
        {
            Object var_206 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "addressLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_206));
        }
        out.write("</p>\r\n                              <p class=\"address\"></p>\r\n                              <a class=\"direction\" href=\"#\" target=\"_blank\">");
        {
            Object var_207 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "directionLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_207));
        }
        out.write("</a>\r\n                              <div class=\"expand_cards_phone\">\r\n                                 <p>");
        {
            Object var_208 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_atmbranchmapmodel, "hotlineLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_208));
        }
        out.write("</p>\r\n                                 <a class=\"contact-number\" href=\"#\"></a>\r\n                              </div>\r\n                              ");
    }
}
out.write("\r\n                              ");
{
    Object var_resourcecontent209 = renderContext.call("includeResource", "button", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/button"));
    out.write(renderContext.getObjectModel().toString(var_resourcecontent209));
}
out.write("                            \r\n                             ");
{
    boolean var_testvariable210 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable210) {
        out.write("\r\n                           </div>\r\n                        </div>\r\n                     </div>\r\n                  </div>\r\n               </div>\r\n               <div class=\"map-container\">\r\n                  <iframe allowfullscreen frameborder=\"0\" id=\"atm-map\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\" title=\"tcb-map\"></iframe>\r\n               </div>\r\n            </div>\r\n         </div>\r\n      </section>   \r\n      ");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

