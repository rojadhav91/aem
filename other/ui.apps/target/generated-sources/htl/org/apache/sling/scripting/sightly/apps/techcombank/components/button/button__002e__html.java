/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.button;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class button__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_buttonmodel = null;
Object _global_type = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n\t<p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_buttonmodel = renderContext.call("use", com.techcombank.core.models.ButtonModel.class.getName(), obj());
out.write("\r\n    ");
_global_type = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "default")));
if (renderContext.getObjectModel().toBoolean(_global_type)) {
    out.write("\r\n\t");
    {
        Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType");
        if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
            out.write("<a");
            {
                Object var_attrvalue6 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaID") : "");
                {
                    Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                    {
                        boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                        if (var_shoulddisplayattr9) {
                            out.write(" id");
                            {
                                boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                                if (!var_istrueattr8) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue10 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaModelId") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaLink"));
                {
                    Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "uri");
                    {
                        boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                        if (var_shoulddisplayattr13) {
                            out.write(" href");
                            {
                                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                                if (!var_istrueattr12) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue14 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaTarget") : "");
                {
                    Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                    {
                        boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                        if (var_shoulddisplayattr17) {
                            out.write(" target");
                            {
                                boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                                if (!var_istrueattr16) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue18 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaNoFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                    {
                        boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                        if (var_shoulddisplayattr21) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                if (!var_istrueattr20) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent22 = ((("cta-button " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "link")) ? "cta-button--link" : ""), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? "qr-button" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                out.write("\"");
            }
            out.write(" data-tracking-click-event=\"cta\"");
            {
                String var_attrcontent23 = (((("{'" + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "contactRegisterCTA")) ? "cta" : "linkClick"), "attribute"))) + "' : 'Button | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaLabel"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                out.write("\"");
            }
            {
                Object var_attrvalue24 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaModalInteractionNameType") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "interactionNameType"));
                {
                    Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                    {
                        boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                        if (var_shoulddisplayattr27) {
                            out.write(" data-tracking-web-interaction-value");
                            {
                                boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                if (!var_istrueattr26) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent28 = (((("background-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaColor"), "styleToken"))) + ";color:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "fontColor"), "styleToken"))) + ";");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                out.write("\"");
            }
            out.write(">\r\n\t\t");
        }
    }
}
out.write("\r\n\t\t");
_global_type = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "link"));
if (renderContext.getObjectModel().toBoolean(_global_type)) {
    out.write("\r\n\t");
    {
        Object var_testvariable29 = renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType");
        if (renderContext.getObjectModel().toBoolean(var_testvariable29)) {
            out.write("<a");
            {
                Object var_attrvalue30 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaID") : "");
                {
                    Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                    {
                        boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                        if (var_shoulddisplayattr33) {
                            out.write(" id");
                            {
                                boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                if (!var_istrueattr32) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue34 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaModelId") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaLink"));
                {
                    Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "uri");
                    {
                        boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                        if (var_shoulddisplayattr37) {
                            out.write(" href");
                            {
                                boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                                if (!var_istrueattr36) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue38 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaTarget") : "");
                {
                    Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
                    {
                        boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                        if (var_shoulddisplayattr41) {
                            out.write(" target");
                            {
                                boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                if (!var_istrueattr40) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue42 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaNoFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "attribute");
                    {
                        boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
                        if (var_shoulddisplayattr45) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                                if (!var_istrueattr44) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent46 = ((("cta-button " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "link")) ? "cta-button--link" : ""), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? "qr-button" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                out.write("\"");
            }
            out.write(" data-tracking-click-event=\"cta\"");
            {
                String var_attrcontent47 = (((("{'" + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "contactRegisterCTA")) ? "cta" : "linkClick"), "attribute"))) + "' : 'Button | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaLabel"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                out.write("\"");
            }
            {
                Object var_attrvalue48 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaType"), "modal")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaModalInteractionNameType") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "interactionNameType"));
                {
                    Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
                    {
                        boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                        if (var_shoulddisplayattr51) {
                            out.write(" data-tracking-web-interaction-value");
                            {
                                boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                if (!var_istrueattr50) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n\t\t");
        }
    }
}
out.write("\r\n\t\t<span class=\"cmp-button__text\">");
{
    String var_52 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaLabel"), "text")) + " ");
    out.write(renderContext.getObjectModel().toString(var_52));
}
out.write("</span>\r\n\t\t");
{
    Object var_testvariable53 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIcon")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIcon") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRImage")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable53)) {
        out.write("\r\n\t\t\t<picture>\r\n\t\t\t\t<source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue54 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRMobileImagePath") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIconMobileImagePath"));
            {
                Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "attribute");
                {
                    boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                    if (var_shoulddisplayattr57) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                            if (!var_istrueattr56) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t<source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue58 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRMobileImagePath") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIconMobileImagePath"));
            {
                Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                {
                    boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                    if (var_shoulddisplayattr61) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                            if (!var_istrueattr60) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t<source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue62 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRWebImagePath") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIconWebImagePath"));
            {
                Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
                {
                    boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                    if (var_shoulddisplayattr65) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                            if (!var_istrueattr64) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t<source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue66 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRWebImagePath") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIconWebImagePath"));
            {
                Object var_attrcontent67 = renderContext.call("xss", var_attrvalue66, "attribute");
                {
                    boolean var_shoulddisplayattr69 = (((null != var_attrcontent67) && (!"".equals(var_attrcontent67))) && ((!"".equals(var_attrvalue66)) && (!((Object)false).equals(var_attrvalue66))));
                    if (var_shoulddisplayattr69) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr68 = (var_attrvalue66.equals(true));
                            if (!var_istrueattr68) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent67));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t\t\t<img");
        {
            Object var_attrvalue70 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRWebImagePath") : renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaIconWebImagePath"));
            {
                Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "uri");
                {
                    boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                    if (var_shoulddisplayattr73) {
                        out.write(" src");
                        {
                            boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                            if (!var_istrueattr72) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue74 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQR")) ? renderContext.getObjectModel().resolveProperty(_global_buttonmodel, "ctaQRImageAltText") : "");
            {
                Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "attribute");
                {
                    boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                    if (var_shoulddisplayattr77) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                            if (!var_istrueattr76) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cmp-button__icon\" aria-hidden=\"true\"/>\r\n\t\t\t</picture>\r\n\t\t");
    }
}
out.write("\r\n\t</a>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

