/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.atomic.columnContainer;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class columnContainer__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_columncontainermodel = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _global_fifty = null;
Object _global_thirty = null;
_global_columncontainermodel = renderContext.call("use", com.techcombank.core.models.ColumnContainerModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
}
out.write("\r\n");
{
    Object var_testvariable0 = _global_hascontent;
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <div class=\"column-wrapper\">\r\n        <div");
        {
            String var_attrcontent1 = ("column-container " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "flipView")) ? "column-reverse" : " "), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent1));
            out.write("\"");
        }
        out.write(">\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "5050"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent2 = ("column-item col-w50 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                out.write("\"");
            }
            {
                String var_attrcontent3 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent4 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent4));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w50\"");
            {
                String var_attrcontent5 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent6 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent6));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "6040"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent7 = ("column-item col-w60 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                out.write("\"");
            }
            {
                String var_attrcontent8 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent9 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent9));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w40\"");
            {
                String var_attrcontent10 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent11 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent11));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "4060"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent12 = ("column-item col-w40 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                out.write("\"");
            }
            {
                String var_attrcontent13 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent14 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent14));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w60\"");
            {
                String var_attrcontent15 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent16 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent16));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "3070"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent17 = ("column-item col-w30 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                out.write("\"");
            }
            {
                String var_attrcontent18 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent19 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent19));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w70\"");
            {
                String var_attrcontent20 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent21 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent21));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "7030"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent22 = ("column-item col-w70 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                out.write("\"");
            }
            {
                String var_attrcontent23 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent24 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent24));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w30\"");
            {
                String var_attrcontent25 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent26 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent26));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_thirty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "3col"));
        if (renderContext.getObjectModel().toBoolean(_global_thirty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent27 = ("column-item col-w33 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                out.write("\"");
            }
            {
                String var_attrcontent28 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent29 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent29));
            }
            out.write("\r\n                </div>\r\n                <div");
            {
                String var_attrcontent30 = ("column-item col-w33 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                out.write("\"");
            }
            {
                String var_attrcontent31 = (((((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent32 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent32));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w33\"");
            {
                String var_attrcontent33 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent34 = renderContext.call("includeResource", null, obj().with("path", "column-3").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent34));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n            ");
_global_fifty = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "width"), "5col"));
        if (renderContext.getObjectModel().toBoolean(_global_fifty)) {
            out.write("\r\n                <div");
            {
                String var_attrcontent35 = ("column-item col-w20 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                out.write("\"");
            }
            {
                String var_attrcontent36 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent37 = renderContext.call("includeResource", null, obj().with("path", "column-1").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent37));
            }
            out.write("\r\n                </div>\r\n                <div");
            {
                String var_attrcontent38 = ("column-item col-w20 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                out.write("\"");
            }
            {
                String var_attrcontent39 = (((((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent40 = renderContext.call("includeResource", null, obj().with("path", "column-2").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent40));
            }
            out.write("\r\n                </div>\r\n                <div");
            {
                String var_attrcontent41 = ("column-item col-w20 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                out.write("\"");
            }
            {
                String var_attrcontent42 = (((((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent43 = renderContext.call("includeResource", null, obj().with("path", "column-3").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent43));
            }
            out.write("\r\n                </div>\r\n                <div");
            {
                String var_attrcontent44 = ("column-item col-w20 " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "activateVerticalDivider")) ? "divider--vertical-right" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                out.write("\"");
            }
            {
                String var_attrcontent45 = (((((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px; padding-right:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "rightPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent46 = renderContext.call("includeResource", null, obj().with("path", "column-4").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent46));
            }
            out.write("\r\n                </div>\r\n                <div class=\"column-item col-w20\"");
            {
                String var_attrcontent47 = (((((("padding-top:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "topPadding"), "html"))) + "px; padding-bottom:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "bottomPadding"), "html"))) + "px; padding-left:") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_columncontainermodel, "leftPadding"), "html"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                out.write("\"");
            }
            out.write(">\r\n                    ");
            {
                Object var_resourcecontent48 = renderContext.call("includeResource", null, obj().with("path", "column-5").with("resourceType", "foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent48));
            }
            out.write("\r\n                </div>\r\n            ");
        }
        out.write("\r\n        </div>\r\n    </div>\r\n");
    }
}
out.write("\r\n");
{
    Object var_templatevar49 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions50_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions50 = obj().with("isEmpty", var_templateoptions50_field$_isempty);
            callUnit(out, renderContext, var_templatevar49, var_templateoptions50);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

