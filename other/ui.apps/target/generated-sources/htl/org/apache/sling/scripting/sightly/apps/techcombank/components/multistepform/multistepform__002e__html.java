/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.multistepform;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class multistepform__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar5_list_coerced$ = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
_global_model = renderContext.call("use", com.techcombank.core.models.MultiStepFormModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "numberOfSteps");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n  ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n    <div class=\"multi-step-form\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_model, "formId");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" id");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n      ");
            {
                Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_model, "stepIndexes");
                {
                    long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
                    {
                        boolean var_notempty7 = (var_size6 > 0);
                        if (var_notempty7) {
                            {
                                long var_end10 = var_size6;
                                {
                                    boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                                    if (var_validstartstepend11) {
                                        if (var_collectionvar5_list_coerced$ == null) {
                                            var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                                        }
                                        long var_index12 = 0;
                                        for (Object stepindex : var_collectionvar5_list_coerced$) {
                                            {
                                                long stepindexlist_field$_index = var_index12;
                                                {
                                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                                    if (var_traversal14) {
                                                        out.write("\r\n        <div");
                                                        {
                                                            String var_attrcontent15 = ("multi-step-form__step " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean((((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(stepindex, 1)) ? (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(stepindex, 1)) : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")))) ? (((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(stepindex, 1)) ? (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(stepindex, 1)) : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")))) ? "active" : ""), "attribute")));
                                                            out.write(" class=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            String var_attrcontent16 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "formId"), "attribute")) + "-step-") + renderContext.getObjectModel().toString(renderContext.call("xss", stepindex, "attribute")));
                                                            out.write(" id=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n          ");
                                                        {
                                                            Object var_testvariable17 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")));
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable17)) {
                                                                out.write("<p>");
                                                                {
                                                                    String var_18 = ("Step " + renderContext.getObjectModel().toString(renderContext.call("xss", stepindex, "text")));
                                                                    out.write(renderContext.getObjectModel().toString(var_18));
                                                                }
                                                                out.write("</p>");
                                                            }
                                                        }
                                                        out.write("\r\n          ");
                                                        {
                                                            Object var_resourcecontent19 = renderContext.call("includeResource", "step-body", obj().with("resourceType", "wcm/foundation/components/parsys"));
                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent19));
                                                        }
                                                        out.write("\r\n          ");
                                                        {
                                                            Object var_resourcecontent20 = renderContext.call("includeResource", stepindexlist_field$_index, obj().with("resourceType", "wcm/foundation/components/parsys"));
                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent20));
                                                        }
                                                        out.write("\r\n        </div>\r\n      ");
                                                    }
                                                }
                                            }
                                            var_index12++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar5_list_coerced$ = null;
            }
            out.write("\r\n    </div>\r\n  ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar21 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions22_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions22 = obj().with("isEmpty", var_templateoptions22_field$_isempty);
            callUnit(out, renderContext, var_templatevar21, var_templateoptions22);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

