/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.tablecompare;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class tablecompare__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_tablecompare = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar2_list_coerced$ = null;
Collection var_collectionvar17_list_coerced$ = null;
Collection var_collectionvar52_list_coerced$ = null;
_global_tablecompare = renderContext.call("use", com.techcombank.core.models.TableCompareModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_tablecompare, "tableCompareColumnHeaderList");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n      <section class=\"container table-compare-component\">\r\n        <div class=\"table-compare-container\">\r\n          <div class=\"table-compare-table\">\r\n            <div class=\"table-compare-content-col\">\r\n              <div class=\"table-compare-item-col first-col header\">\r\n                <div class=\"table-compare-item-row first-row\" row-index=\"-1\">\r\n                  <span>");
            {
                Object var_1 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_tablecompare, "columnHeaderTitle"), "html");
                out.write(renderContext.getObjectModel().toString(var_1));
            }
            out.write("</span>\r\n                </div>\r\n                ");
            {
                Object var_collectionvar2 = renderContext.getObjectModel().resolveProperty(_global_tablecompare, "tableCompareColumnHeaderList");
                {
                    long var_size3 = ((var_collectionvar2_list_coerced$ == null ? (var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2)) : var_collectionvar2_list_coerced$).size());
                    {
                        boolean var_notempty4 = (var_size3 > 0);
                        if (var_notempty4) {
                            {
                                long var_end7 = var_size3;
                                {
                                    boolean var_validstartstepend8 = (((0 < var_size3) && true) && (var_end7 > 0));
                                    if (var_validstartstepend8) {
                                        if (var_collectionvar2_list_coerced$ == null) {
                                            var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2);
                                        }
                                        long var_index9 = 0;
                                        for (Object item : var_collectionvar2_list_coerced$) {
                                            {
                                                long itemlist_field$_index = var_index9;
                                                {
                                                    boolean var_traversal11 = (((var_index9 >= 0) && (var_index9 <= var_end7)) && true);
                                                    if (var_traversal11) {
                                                        out.write("<div class=\"table-compare-item-row first-col-row\"");
                                                        {
                                                            long var_attrvalue12 = itemlist_field$_index;
                                                            {
                                                                Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                                                                    if (var_shoulddisplayattr15) {
                                                                        out.write(" row-index");
                                                                        {
                                                                            boolean var_istrueattr14 = (((Object)var_attrvalue12).equals(true));
                                                                            if (!var_istrueattr14) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(">\r\n                  <p>");
                                                        {
                                                            Object var_16 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "columnHeader"), "html");
                                                            out.write(renderContext.getObjectModel().toString(var_16));
                                                        }
                                                        out.write("</p>\r\n                </div>\n");
                                                    }
                                                }
                                            }
                                            var_index9++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar2_list_coerced$ = null;
            }
            out.write("\r\n              </div>\r\n              ");
            {
                Object var_collectionvar17 = renderContext.getObjectModel().resolveProperty(_global_tablecompare, "tableCompareCardList");
                {
                    long var_size18 = ((var_collectionvar17_list_coerced$ == null ? (var_collectionvar17_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar17)) : var_collectionvar17_list_coerced$).size());
                    {
                        boolean var_notempty19 = (var_size18 > 0);
                        if (var_notempty19) {
                            {
                                long var_end22 = var_size18;
                                {
                                    boolean var_validstartstepend23 = (((0 < var_size18) && true) && (var_end22 > 0));
                                    if (var_validstartstepend23) {
                                        if (var_collectionvar17_list_coerced$ == null) {
                                            var_collectionvar17_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar17);
                                        }
                                        long var_index24 = 0;
                                        for (Object card : var_collectionvar17_list_coerced$) {
                                            {
                                                boolean var_traversal26 = (((var_index24 >= 0) && (var_index24 <= var_end22)) && true);
                                                if (var_traversal26) {
                                                    out.write("<div class=\"table-compare-item-col\">\r\n                <div class=\"table-compare-item-row first-row header\" row-index=\"-1\">\r\n                  <picture>\r\n                    <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(card, "mobileCardTitleImagePath");
                                                        {
                                                            Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                                                                if (var_shoulddisplayattr30) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                                                        if (!var_istrueattr29) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(card, "mobileCardTitleImagePath");
                                                        {
                                                            Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                                                                if (var_shoulddisplayattr34) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                                                        if (!var_istrueattr33) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(card, "webCardTitleImagePath");
                                                        {
                                                            Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                                                                if (var_shoulddisplayattr38) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                                                        if (!var_istrueattr37) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(card, "webCardTitleImagePath");
                                                        {
                                                            Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                                                                if (var_shoulddisplayattr42) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                                                        if (!var_istrueattr41) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <img");
                                                    {
                                                        Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(card, "altText");
                                                        {
                                                            Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                                if (var_shoulddisplayattr46) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                                        if (!var_istrueattr45) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(card, "webCardTitleImagePath");
                                                        {
                                                            Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                                                                if (var_shoulddisplayattr50) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                                                        if (!var_istrueattr49) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                  </picture>\r\n                  <span class=\"first-row-heading-text\">");
                                                    {
                                                        Object var_51 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(card, "cardTitle"), "html");
                                                        out.write(renderContext.getObjectModel().toString(var_51));
                                                    }
                                                    out.write("</span>\r\n                </div>\r\n                ");
                                                    {
                                                        Object var_collectionvar52 = renderContext.getObjectModel().resolveProperty(card, "tableCompareRowList");
                                                        {
                                                            long var_size53 = ((var_collectionvar52_list_coerced$ == null ? (var_collectionvar52_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar52)) : var_collectionvar52_list_coerced$).size());
                                                            {
                                                                boolean var_notempty54 = (var_size53 > 0);
                                                                if (var_notempty54) {
                                                                    {
                                                                        long var_end57 = var_size53;
                                                                        {
                                                                            boolean var_validstartstepend58 = (((0 < var_size53) && true) && (var_end57 > 0));
                                                                            if (var_validstartstepend58) {
                                                                                if (var_collectionvar52_list_coerced$ == null) {
                                                                                    var_collectionvar52_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar52);
                                                                                }
                                                                                long var_index59 = 0;
                                                                                for (Object row : var_collectionvar52_list_coerced$) {
                                                                                    {
                                                                                        long rowlist_field$_index = var_index59;
                                                                                        {
                                                                                            boolean var_traversal61 = (((var_index59 >= 0) && (var_index59 <= var_end57)) && true);
                                                                                            if (var_traversal61) {
                                                                                                out.write("\r\n                  <div class=\"item-background\"");
                                                                                                {
                                                                                                    String var_attrcontent62 = (("background-color:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(card, "cardBackgroundColour"), "styleString"))) + ";");
                                                                                                    out.write(" style=\"");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                                                    out.write("\"");
                                                                                                }
                                                                                                out.write(">\r\n                    <div class=\"table-compare-item-row\"");
                                                                                                {
                                                                                                    long var_attrvalue63 = rowlist_field$_index;
                                                                                                    {
                                                                                                        Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
                                                                                                        {
                                                                                                            boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                                                                                                            if (var_shoulddisplayattr66) {
                                                                                                                out.write(" row-index");
                                                                                                                {
                                                                                                                    boolean var_istrueattr65 = (((Object)var_attrvalue63).equals(true));
                                                                                                                    if (!var_istrueattr65) {
                                                                                                                        out.write("=\"");
                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                                                                                                        out.write("\"");
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                out.write(">                       \r\n                      <p class=\"moved-title mobile-left\"></p>");
                                                                                                {
                                                                                                    String var_67 = (("\r\n                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(row, "rowValue"), "html"))) + "\r\n                    ");
                                                                                                    out.write(renderContext.getObjectModel().toString(var_67));
                                                                                                }
                                                                                                out.write("</div>\r\n                  </div>\r\n                ");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    var_index59++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar52_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n              </div>\n");
                                                }
                                            }
                                            var_index24++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar17_list_coerced$ = null;
            }
            out.write("\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar68 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions69_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions69 = obj().with("isEmpty", var_templateoptions69_field$_isempty);
            callUnit(out, renderContext, var_templatevar68, var_templateoptions69);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

