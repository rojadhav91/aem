/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.complexpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class complexpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_complexpanel = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_complexpanel = renderContext.call("use", com.techcombank.core.models.ComplexPanelModel.class.getName(), obj());
out.write("\r\n    <section class=\"container\">\r\n        <div class=\"complex-panel\">\r\n            <div class=\"complex-panel__body-content\">\r\n                <div class=\"body-content__container\">\r\n                    <div class=\"content-item__wrapper\">\r\n                        <div class=\"content-item__panel\">\r\n                            <div class=\"content-item__panel-text\">\r\n                                <h2 class=\"complex-title\">");
{
    Object var_5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_complexpanel, "panelTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_5));
}
out.write("</h2>\r\n                                <div class=\"panel-text__information\">\r\n                                    <div class=\"complex-information\">");
{
    String var_6 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_complexpanel, "description"), "html"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_6));
}
out.write("</div>\r\n                                    <p class=\"complex-date\">");
{
    Object var_7 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_complexpanel, "dateText"), "text");
    out.write(renderContext.getObjectModel().toString(var_7));
}
out.write("</p>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"content-item__card-image\">\r\n                                <div class=\"card-image__container\">\r\n                                    <div class=\"card-image__left-site\">\r\n                                        <div class=\"card-image__two-card\">\r\n                                            ");
{
    Object var_testvariable8 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "topImage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable8)) {
        out.write("<div class=\"card-image__image\">\r\n                                                <img");
        {
            Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "topAltText");
            {
                Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                {
                    boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                    if (var_shoulddisplayattr12) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                            if (!var_istrueattr11) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "topImage");
            {
                Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "uri");
                {
                    boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                    if (var_shoulddisplayattr16) {
                        out.write(" src");
                        {
                            boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                            if (!var_istrueattr15) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\" srcset=\"\"/>\r\n                                            </div>");
    }
}
out.write("\r\n                                            ");
{
    Object var_testvariable17 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "bottomImage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable17)) {
        out.write("<div class=\"card-image__image\">\r\n                                                <img");
        {
            Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "bottomAltText");
            {
                Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                {
                    boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                    if (var_shoulddisplayattr21) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                            if (!var_istrueattr20) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "bottomImage");
            {
                Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "uri");
                {
                    boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                    if (var_shoulddisplayattr25) {
                        out.write(" src");
                        {
                            boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                            if (!var_istrueattr24) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\" srcset=\"\"/>\r\n                                            </div>");
    }
}
out.write("\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"card-image__right-site\">\r\n                                        <div class=\"card-image__one-card\">\r\n                                            ");
{
    Object var_testvariable26 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "rightImage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable26)) {
        out.write("<div class=\"card-image__image\">\r\n                                                <img");
        {
            Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "rightAltText");
            {
                Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
                {
                    boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                    if (var_shoulddisplayattr30) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                            if (!var_istrueattr29) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_complexpanel, "rightImage");
            {
                Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "uri");
                {
                    boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                    if (var_shoulddisplayattr34) {
                        out.write(" src");
                        {
                            boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                            if (!var_istrueattr33) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\" srcset=\"\"/>\r\n                                            </div>");
    }
}
out.write("\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

