/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.tabs;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class tabs__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_tabs = null;
Object _global_tabscontrol = null;
Object _global_templates = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _global_lang = null;
Object _global_items = null;
Collection var_collectionvar13_list_coerced$ = null;
Object _global_isactive = null;
Collection var_collectionvar35_list_coerced$ = null;
Object _global_tabslist = null;
Collection var_collectionvar51_list_coerced$ = null;
Collection var_collectionvar68_list_coerced$ = null;
Object _dynamic_resource = bindings.get("resource");
Collection var_collectionvar89_list_coerced$ = null;
Collection var_collectionvar110_list_coerced$ = null;
Object _dynamic_tab = bindings.get("tab");
Collection var_collectionvar140_list_coerced$ = null;
Collection var_collectionvar159_list_coerced$ = null;
Collection var_collectionvar180_list_coerced$ = null;
Collection var_collectionvar199_list_coerced$ = null;
Collection var_collectionvar215_list_coerced$ = null;
_global_tabs = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Tabs.class.getName(), obj());
_global_tabscontrol = renderContext.call("use", com.techcombank.core.models.TabsModel.class.getName(), obj());
_global_templates = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
out.write("<div");
{
    Object var_attrvalue0 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "tabs" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" data-panelcontainer");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue4 = renderContext.getObjectModel().resolveProperty(_global_tabs, "id");
    {
        Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
        {
            boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
            if (var_shoulddisplayattr7) {
                out.write(" id");
                {
                    boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                    if (!var_istrueattr6) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" class=\"cmp-tabs\" data-cmp-is=\"tabs\"");
{
    Object var_attrvalue8 = renderContext.call("i18n", ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Please drag Tab components here" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))), obj().with("i18n", null));
    {
        Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
        {
            boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
            if (var_shoulddisplayattr11) {
                out.write(" data-placeholder-text");
                {
                    boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                    if (!var_istrueattr10) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n  <!-- Component Structure -->\r\n  ");
_global_lang = renderContext.getObjectModel().resolveProperty(_global_tabscontrol, "getLanguageTitle");
if (renderContext.getObjectModel().toBoolean(_global_lang)) {
    out.write("<section class=\"tabs-control-component\">\r\n    <!-- Vertical in desktop, horizontal in tablet/mobile case -->\r\n    ");
    {
        boolean var_testvariable12 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_tabscontrol, "tabLabelAlignment"), "vertical"));
        if (var_testvariable12) {
            out.write("\r\n      <!-- Vertical in desktop -->\r\n      <div class=\"tabs-vertical\">\r\n        <section class=\"container tab-vertical-features-component\">\r\n          <div class=\"tab-vertical-features-component__container\">\r\n            <div class=\"tab-content\">\r\n              ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            {
                Object var_testvariable20 = ((renderContext.getObjectModel().toBoolean(_global_items) ? (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0))) : _global_items));
                if (renderContext.getObjectModel().toBoolean(var_testvariable20)) {
                    {
                        Object var_collectionvar13 = _global_items;
                        {
                            long var_size14 = ((var_collectionvar13_list_coerced$ == null ? (var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13)) : var_collectionvar13_list_coerced$).size());
                            {
                                boolean var_notempty15 = (var_size14 > 0);
                                if (var_notempty15) {
                                    {
                                        long var_end18 = var_size14;
                                        {
                                            boolean var_validstartstepend19 = (((0 < var_size14) && true) && (var_end18 > 0));
                                            if (var_validstartstepend19) {
                                                out.write("<div class=\"menu cmp-tabs__tablist\" role=\"tablist\"");
                                                {
                                                    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_tabs, "accessibilityLabel");
                                                    {
                                                        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                            if (var_shoulddisplayattr24) {
                                                                out.write(" aria-label");
                                                                {
                                                                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                    if (!var_istrueattr23) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" aria-multiselectable=\"false\">");
                                                if (var_collectionvar13_list_coerced$ == null) {
                                                    var_collectionvar13_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar13);
                                                }
                                                long var_index25 = 0;
                                                for (Object tab : var_collectionvar13_list_coerced$) {
                                                    {
                                                        boolean var_traversal27 = (((var_index25 >= 0) && (var_index25 <= var_end18)) && true);
                                                        if (var_traversal27) {
                                                            out.write("\r\n                ");
_global_isactive = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(tab, "name"), renderContext.getObjectModel().resolveProperty(_global_tabs, "activeItem")));
                                                            if (renderContext.getObjectModel().toBoolean(_global_isactive)) {
                                                            }
                                                            out.write("\r\n                <button class=\"menu-item\" role=\"tab\"");
                                                            {
                                                                String var_attrcontent28 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tab, "id"), "attribute")) + "-tab");
                                                                out.write(" id=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                                                out.write("\"");
                                                            }
                                                            {
                                                                String var_attrcontent29 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tab, "id"), "attribute")) + "-tabpanel");
                                                                out.write(" aria-controls=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                out.write("\"");
                                                            }
                                                            {
                                                                String var_attrvalue30 = (renderContext.getObjectModel().toBoolean(_global_isactive) ? "0" : "-1");
                                                                {
                                                                    Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                                        if (var_shoulddisplayattr33) {
                                                                            out.write(" tabindex");
                                                                            {
                                                                                boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                                if (!var_istrueattr32) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" data-cmp-hook-tabs=\"tab\">\r\n                    <div class=\"vertical-tabs-button-text\">");
                                                            {
                                                                Object var_34 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tab, "title"), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_34));
                                                            }
                                                            out.write("</div>\r\n                </button>\r\n              ");
                                                        }
                                                    }
                                                    var_index25++;
                                                }
                                                out.write("</div>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar13_list_coerced$ = null;
                    }
                }
            }
            out.write("\r\n              <div class=\"list-items\">\r\n                ");
            {
                Object var_collectionvar35 = _global_items;
                {
                    long var_size36 = ((var_collectionvar35_list_coerced$ == null ? (var_collectionvar35_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar35)) : var_collectionvar35_list_coerced$).size());
                    {
                        boolean var_notempty37 = (var_size36 > 0);
                        if (var_notempty37) {
                            {
                                long var_end40 = var_size36;
                                {
                                    boolean var_validstartstepend41 = (((0 < var_size36) && true) && (var_end40 > 0));
                                    if (var_validstartstepend41) {
                                        if (var_collectionvar35_list_coerced$ == null) {
                                            var_collectionvar35_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar35);
                                        }
                                        long var_index42 = 0;
                                        for (Object item : var_collectionvar35_list_coerced$) {
                                            {
                                                boolean var_traversal44 = (((var_index42 >= 0) && (var_index42 <= var_end40)) && true);
                                                if (var_traversal44) {
                                                    out.write("<div class=\"tab\">\r\n                  <div");
                                                    {
                                                        Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(item, "id");
                                                        {
                                                            Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                                                                if (var_shoulddisplayattr48) {
                                                                    out.write(" id");
                                                                    {
                                                                        boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                                                                        if (!var_istrueattr47) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" role=\"tabpanel\"");
                                                    {
                                                        String var_attrcontent49 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-tab");
                                                        out.write(" aria-labelledby=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                        out.write("\"");
                                                    }
                                                    out.write(" tabindex=\"0\">");
                                                    {
                                                        Object var_resourcecontent50 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                        out.write(renderContext.getObjectModel().toString(var_resourcecontent50));
                                                    }
                                                    out.write("</div>\r\n                </div>\n");
                                                }
                                            }
                                            var_index42++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar35_list_coerced$ = null;
            }
            out.write("\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </section>     \r\n      </div>\r\n      <!-- Horizontal in tablet/mobile -->\r\n      <div class=\"tabs-horizontal\">\r\n        <section class=\"tabs-control-wrapper\">\r\n          ");
_global_tabslist = "";
            out.write("\r\n            ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            {
                Object var_testvariable58 = ((renderContext.getObjectModel().toBoolean(_global_items) ? (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0))) : _global_items));
                if (renderContext.getObjectModel().toBoolean(var_testvariable58)) {
                    {
                        Object var_collectionvar51 = _global_items;
                        {
                            long var_size52 = ((var_collectionvar51_list_coerced$ == null ? (var_collectionvar51_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar51)) : var_collectionvar51_list_coerced$).size());
                            {
                                boolean var_notempty53 = (var_size52 > 0);
                                if (var_notempty53) {
                                    {
                                        long var_end56 = var_size52;
                                        {
                                            boolean var_validstartstepend57 = (((0 < var_size52) && true) && (var_end56 > 0));
                                            if (var_validstartstepend57) {
                                                if (var_collectionvar51_list_coerced$ == null) {
                                                    var_collectionvar51_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar51);
                                                }
                                                long var_index59 = 0;
                                                for (Object item : var_collectionvar51_list_coerced$) {
                                                    {
                                                        boolean itemlist_field$_last = (var_index59 == (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.SUB.eval(var_size52, 1)).longValue()));
                                                        {
                                                            boolean var_traversal61 = (((var_index59 >= 0) && (var_index59 <= var_end56)) && true);
                                                            if (var_traversal61) {
                                                                out.write("\r\n              ");
                                                                {
                                                                    boolean var_testvariable62 = (!itemlist_field$_last);
                                                                    if (var_testvariable62) {
                                                                        out.write("<div>\r\n                ");
_global_tabslist = renderContext.call("format", "{0}{1}{2}", obj().with("format", (new Object[] {_global_tabslist, renderContext.getObjectModel().resolveProperty(item, "title"), ";"})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_tabslist)) {
                                                                        }
                                                                        out.write("\r\n              </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n              ");
                                                                {
                                                                    boolean var_testvariable63 = itemlist_field$_last;
                                                                    if (var_testvariable63) {
                                                                        out.write("<div>\r\n                ");
_global_tabslist = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {_global_tabslist, renderContext.getObjectModel().resolveProperty(item, "title")})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_tabslist)) {
                                                                        }
                                                                        out.write("\r\n              </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n            ");
                                                            }
                                                        }
                                                    }
                                                    var_index59++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar51_list_coerced$ = null;
                    }
                }
            }
            out.write("\r\n          \r\n          <div class=\"tcb-tabs\"");
            {
                Object var_attrvalue64 = _global_tabslist;
                {
                    Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                    {
                        boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                        if (var_shoulddisplayattr67) {
                            out.write(" data-tabs");
                            {
                                boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                                if (!var_istrueattr66) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("></div>\r\n          <div class=\"tab-item_list tcb-tab-content\">\r\n              <div class=\"tab-items\">\r\n                ");
            {
                Object var_collectionvar68 = _global_items;
                {
                    long var_size69 = ((var_collectionvar68_list_coerced$ == null ? (var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68)) : var_collectionvar68_list_coerced$).size());
                    {
                        boolean var_notempty70 = (var_size69 > 0);
                        if (var_notempty70) {
                            {
                                long var_end73 = var_size69;
                                {
                                    boolean var_validstartstepend74 = (((0 < var_size69) && true) && (var_end73 > 0));
                                    if (var_validstartstepend74) {
                                        if (var_collectionvar68_list_coerced$ == null) {
                                            var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68);
                                        }
                                        long var_index75 = 0;
                                        for (Object item : var_collectionvar68_list_coerced$) {
                                            {
                                                boolean var_traversal77 = (((var_index75 >= 0) && (var_index75 <= var_end73)) && true);
                                                if (var_traversal77) {
                                                    out.write("<div");
                                                    {
                                                        Object var_attrvalue78 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), "hashCode");
                                                        {
                                                            Object var_attrcontent79 = renderContext.call("xss", var_attrvalue78, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr81 = (((null != var_attrcontent79) && (!"".equals(var_attrcontent79))) && ((!"".equals(var_attrvalue78)) && (!((Object)false).equals(var_attrvalue78))));
                                                                if (var_shoulddisplayattr81) {
                                                                    out.write(" tabs-control-index");
                                                                    {
                                                                        boolean var_istrueattr80 = (var_attrvalue78.equals(true));
                                                                        if (!var_istrueattr80) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent79));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" role=\"tabpanel\"");
                                                    {
                                                        String var_attrcontent82 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-tab");
                                                        out.write(" aria-labelledby=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                                                        out.write("\"");
                                                    }
                                                    out.write(" class=\"tab-item\"");
                                                    {
                                                        Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(item, "title");
                                                        {
                                                            Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                                                                if (var_shoulddisplayattr86) {
                                                                    out.write(" data-year");
                                                                    {
                                                                        boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                                                                        if (!var_istrueattr85) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        Object var_resourcecontent87 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                        out.write(renderContext.getObjectModel().toString(var_resourcecontent87));
                                                    }
                                                    out.write("</div>\n");
                                                }
                                            }
                                            var_index75++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar68_list_coerced$ = null;
            }
            out.write("\r\n              </div>\r\n          </div>\r\n        </section>\r\n      </div>\r\n    ");
        }
    }
    out.write("\r\n    <!-- Always horizontal case -->\r\n    ");
    {
        boolean var_testvariable88 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_tabscontrol, "tabLabelAlignment"), "horizontal"));
        if (var_testvariable88) {
            out.write("\r\n      <section class=\"tabs-control-wrapper\">\r\n        ");
_global_tabslist = "";
            out.write("\r\n          ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            {
                Object var_testvariable96 = ((renderContext.getObjectModel().toBoolean(_global_items) ? (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0))) : _global_items));
                if (renderContext.getObjectModel().toBoolean(var_testvariable96)) {
                    {
                        Object var_collectionvar89 = _global_items;
                        {
                            long var_size90 = ((var_collectionvar89_list_coerced$ == null ? (var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89)) : var_collectionvar89_list_coerced$).size());
                            {
                                boolean var_notempty91 = (var_size90 > 0);
                                if (var_notempty91) {
                                    {
                                        long var_end94 = var_size90;
                                        {
                                            boolean var_validstartstepend95 = (((0 < var_size90) && true) && (var_end94 > 0));
                                            if (var_validstartstepend95) {
                                                if (var_collectionvar89_list_coerced$ == null) {
                                                    var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89);
                                                }
                                                long var_index97 = 0;
                                                for (Object item : var_collectionvar89_list_coerced$) {
                                                    {
                                                        boolean itemlist_field$_last = (var_index97 == (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.SUB.eval(var_size90, 1)).longValue()));
                                                        {
                                                            boolean var_traversal99 = (((var_index97 >= 0) && (var_index97 <= var_end94)) && true);
                                                            if (var_traversal99) {
                                                                out.write("\r\n            ");
                                                                {
                                                                    boolean var_testvariable100 = (!itemlist_field$_last);
                                                                    if (var_testvariable100) {
                                                                        out.write("<div>\r\n              ");
_global_tabslist = renderContext.call("format", "{0}{1}{2}", obj().with("format", (new Object[] {_global_tabslist, renderContext.getObjectModel().resolveProperty(item, "title"), ";"})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_tabslist)) {
                                                                        }
                                                                        out.write("\r\n            </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n            ");
                                                                {
                                                                    boolean var_testvariable101 = itemlist_field$_last;
                                                                    if (var_testvariable101) {
                                                                        out.write("<div>\r\n              ");
_global_tabslist = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {_global_tabslist, renderContext.getObjectModel().resolveProperty(item, "title")})));
                                                                        if (renderContext.getObjectModel().toBoolean(_global_tabslist)) {
                                                                        }
                                                                        out.write("\r\n            </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n          ");
                                                            }
                                                        }
                                                    }
                                                    var_index97++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar89_list_coerced$ = null;
                    }
                }
            }
            out.write("\r\n        \r\n        <div class=\"tcb-tabs\"");
            {
                Object var_attrvalue102 = _global_tabslist;
                {
                    Object var_attrcontent103 = renderContext.call("xss", var_attrvalue102, "attribute");
                    {
                        boolean var_shoulddisplayattr105 = (((null != var_attrcontent103) && (!"".equals(var_attrcontent103))) && ((!"".equals(var_attrvalue102)) && (!((Object)false).equals(var_attrvalue102))));
                        if (var_shoulddisplayattr105) {
                            out.write(" data-tabs");
                            {
                                boolean var_istrueattr104 = (var_attrvalue102.equals(true));
                                if (!var_istrueattr104) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent103));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("></div>\r\n        <div class=\"tab-item_list tcb-tab-content\">\r\n            <div");
            {
                String var_attrvalue106 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "tab-items-author" : "tab-items");
                {
                    Object var_attrcontent107 = renderContext.call("xss", var_attrvalue106, "attribute");
                    {
                        boolean var_shoulddisplayattr109 = (((null != var_attrcontent107) && (!"".equals(var_attrcontent107))) && ((!"".equals(var_attrvalue106)) && (!((Object)false).equals(var_attrvalue106))));
                        if (var_shoulddisplayattr109) {
                            out.write(" class");
                            {
                                boolean var_istrueattr108 = (var_attrvalue106.equals(true));
                                if (!var_istrueattr108) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent107));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n                ");
            {
                Object var_collectionvar110 = _global_items;
                {
                    long var_size111 = ((var_collectionvar110_list_coerced$ == null ? (var_collectionvar110_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar110)) : var_collectionvar110_list_coerced$).size());
                    {
                        boolean var_notempty112 = (var_size111 > 0);
                        if (var_notempty112) {
                            {
                                long var_end115 = var_size111;
                                {
                                    boolean var_validstartstepend116 = (((0 < var_size111) && true) && (var_end115 > 0));
                                    if (var_validstartstepend116) {
                                        if (var_collectionvar110_list_coerced$ == null) {
                                            var_collectionvar110_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar110);
                                        }
                                        long var_index117 = 0;
                                        for (Object item : var_collectionvar110_list_coerced$) {
                                            {
                                                boolean var_traversal119 = (((var_index117 >= 0) && (var_index117 <= var_end115)) && true);
                                                if (var_traversal119) {
                                                    out.write("<div");
                                                    {
                                                        Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), "hashCode");
                                                        {
                                                            Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                                                                if (var_shoulddisplayattr123) {
                                                                    out.write(" tabs-control-index");
                                                                    {
                                                                        boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                                                                        if (!var_istrueattr122) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" role=\"tabpanel\"");
                                                    {
                                                        String var_attrcontent124 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-tab");
                                                        out.write(" aria-labelledby=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent124));
                                                        out.write("\"");
                                                    }
                                                    out.write(" class=\"tab-item\"");
                                                    {
                                                        Object var_attrvalue125 = renderContext.getObjectModel().resolveProperty(item, "title");
                                                        {
                                                            Object var_attrcontent126 = renderContext.call("xss", var_attrvalue125, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr128 = (((null != var_attrcontent126) && (!"".equals(var_attrcontent126))) && ((!"".equals(var_attrvalue125)) && (!((Object)false).equals(var_attrvalue125))));
                                                                if (var_shoulddisplayattr128) {
                                                                    out.write(" data-year");
                                                                    {
                                                                        boolean var_istrueattr127 = (var_attrvalue125.equals(true));
                                                                        if (!var_istrueattr127) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent126));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        Object var_resourcecontent129 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                        out.write(renderContext.getObjectModel().toString(var_resourcecontent129));
                                                    }
                                                    out.write("</div>\n");
                                                }
                                            }
                                            var_index117++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar110_list_coerced$ = null;
            }
            out.write("          \r\n            </div>      \r\n        </div>\r\n      </section>\r\n    ");
        }
    }
    out.write("\r\n    <!-- Inspire case -->\r\n    ");
    {
        boolean var_testvariable130 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_tabscontrol, "tabLabelAlignment"), "inspire"));
        if (var_testvariable130) {
            out.write("\r\n      <section class=\"container inspire-exclusive\"");
            {
                Object var_attrvalue131 = renderContext.getObjectModel().resolveProperty(_global_tabscontrol, "getLanguageTitle");
                {
                    Object var_attrcontent132 = renderContext.call("xss", var_attrvalue131, "attribute");
                    {
                        boolean var_shoulddisplayattr134 = (((null != var_attrcontent132) && (!"".equals(var_attrcontent132))) && ((!"".equals(var_attrvalue131)) && (!((Object)false).equals(var_attrvalue131))));
                        if (var_shoulddisplayattr134) {
                            out.write(" lang");
                            {
                                boolean var_istrueattr133 = (var_attrvalue131.equals(true));
                                if (!var_istrueattr133) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent132));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n        <div class=\"inspire-exclusive__wrapper\">\r\n          <div class=\"inspire-exclusive__container\" responsive-type=\"desktop\">\r\n            ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            {
                Object var_testvariable135 = ((renderContext.getObjectModel().toBoolean(_global_items) ? (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0))) : _global_items));
                if (renderContext.getObjectModel().toBoolean(var_testvariable135)) {
                    out.write("<div class=\"inspire-exclusive__tab-menu cmp-tabs__tablist\" role=\"tablist\"");
                    {
                        Object var_attrvalue136 = renderContext.getObjectModel().resolveProperty(_global_tabs, "accessibilityLabel");
                        {
                            Object var_attrcontent137 = renderContext.call("xss", var_attrvalue136, "attribute");
                            {
                                boolean var_shoulddisplayattr139 = (((null != var_attrcontent137) && (!"".equals(var_attrcontent137))) && ((!"".equals(var_attrvalue136)) && (!((Object)false).equals(var_attrvalue136))));
                                if (var_shoulddisplayattr139) {
                                    out.write(" aria-label");
                                    {
                                        boolean var_istrueattr138 = (var_attrvalue136.equals(true));
                                        if (!var_istrueattr138) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent137));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" aria-multiselectable=\"false\">\r\n                ");
_global_isactive = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_tab, "name"), renderContext.getObjectModel().resolveProperty(_global_tabs, "activeItem")));
                    if (renderContext.getObjectModel().toBoolean(_global_isactive)) {
                    }
                    out.write("\r\n              ");
                    {
                        Object var_collectionvar140 = _global_items;
                        {
                            long var_size141 = ((var_collectionvar140_list_coerced$ == null ? (var_collectionvar140_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar140)) : var_collectionvar140_list_coerced$).size());
                            {
                                boolean var_notempty142 = (var_size141 > 0);
                                if (var_notempty142) {
                                    {
                                        long var_end145 = var_size141;
                                        {
                                            boolean var_validstartstepend146 = (((0 < var_size141) && true) && (var_end145 > 0));
                                            if (var_validstartstepend146) {
                                                if (var_collectionvar140_list_coerced$ == null) {
                                                    var_collectionvar140_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar140);
                                                }
                                                long var_index147 = 0;
                                                for (Object item : var_collectionvar140_list_coerced$) {
                                                    {
                                                        long itemlist_field$_index = var_index147;
                                                        {
                                                            boolean var_traversal149 = (((var_index147 >= 0) && (var_index147 <= var_end145)) && true);
                                                            if (var_traversal149) {
                                                                out.write("<div class=\"tab-menu-item\"");
                                                                {
                                                                    long var_attrvalue150 = itemlist_field$_index;
                                                                    {
                                                                        Object var_attrcontent151 = renderContext.call("xss", var_attrvalue150, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr153 = (((null != var_attrcontent151) && (!"".equals(var_attrcontent151))) && ((!"".equals(var_attrvalue150)) && (!((Object)false).equals(var_attrvalue150))));
                                                                            if (var_shoulddisplayattr153) {
                                                                                out.write(" color-tab");
                                                                                {
                                                                                    boolean var_istrueattr152 = (((Object)var_attrvalue150).equals(true));
                                                                                    if (!var_istrueattr152) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent151));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">\r\n                <h3");
                                                                {
                                                                    long var_attrvalue154 = itemlist_field$_index;
                                                                    {
                                                                        Object var_attrcontent155 = renderContext.call("xss", var_attrvalue154, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr157 = (((null != var_attrcontent155) && (!"".equals(var_attrcontent155))) && ((!"".equals(var_attrvalue154)) && (!((Object)false).equals(var_attrvalue154))));
                                                                            if (var_shoulddisplayattr157) {
                                                                                out.write(" color-title");
                                                                                {
                                                                                    boolean var_istrueattr156 = (((Object)var_attrvalue154).equals(true));
                                                                                    if (!var_istrueattr156) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent155));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">");
                                                                {
                                                                    Object var_158 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "title"), "html");
                                                                    out.write(renderContext.getObjectModel().toString(var_158));
                                                                }
                                                                out.write("</h3>\r\n              </div>\n");
                                                            }
                                                        }
                                                    }
                                                    var_index147++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar140_list_coerced$ = null;
                    }
                    out.write("\r\n            </div>");
                }
            }
            out.write("\r\n            ");
            {
                Object var_collectionvar159 = _global_items;
                {
                    long var_size160 = ((var_collectionvar159_list_coerced$ == null ? (var_collectionvar159_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar159)) : var_collectionvar159_list_coerced$).size());
                    {
                        boolean var_notempty161 = (var_size160 > 0);
                        if (var_notempty161) {
                            {
                                long var_end164 = var_size160;
                                {
                                    boolean var_validstartstepend165 = (((0 < var_size160) && true) && (var_end164 > 0));
                                    if (var_validstartstepend165) {
                                        if (var_collectionvar159_list_coerced$ == null) {
                                            var_collectionvar159_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar159);
                                        }
                                        long var_index166 = 0;
                                        for (Object item : var_collectionvar159_list_coerced$) {
                                            {
                                                boolean var_traversal168 = (((var_index166 >= 0) && (var_index166 <= var_end164)) && true);
                                                if (var_traversal168) {
                                                    out.write("<div");
                                                    {
                                                        Object var_attrvalue169 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), "hashCode");
                                                        {
                                                            Object var_attrcontent170 = renderContext.call("xss", var_attrvalue169, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr172 = (((null != var_attrcontent170) && (!"".equals(var_attrcontent170))) && ((!"".equals(var_attrvalue169)) && (!((Object)false).equals(var_attrvalue169))));
                                                                if (var_shoulddisplayattr172) {
                                                                    out.write(" tabs-control-index");
                                                                    {
                                                                        boolean var_istrueattr171 = (var_attrvalue169.equals(true));
                                                                        if (!var_istrueattr171) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent170));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" role=\"tabpanel\"");
                                                    {
                                                        String var_attrcontent173 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-tab");
                                                        out.write(" aria-labelledby=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent173));
                                                        out.write("\"");
                                                    }
                                                    out.write(" class=\"inspire-exclusive__tab-list\">");
                                                    {
                                                        Object var_resourcecontent174 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                        out.write(renderContext.getObjectModel().toString(var_resourcecontent174));
                                                    }
                                                    out.write("</div>\n");
                                                }
                                            }
                                            var_index166++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar159_list_coerced$ = null;
            }
            out.write("\r\n          </div>\r\n        </div>\r\n        <div class=\"inspire-exclusive__container\" responsive-type=\"mobile\">\r\n          <div class=\"inspire-exclusive__hidden-content\">\r\n            ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            {
                Object var_testvariable175 = ((renderContext.getObjectModel().toBoolean(_global_items) ? (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0))) : _global_items));
                if (renderContext.getObjectModel().toBoolean(var_testvariable175)) {
                    out.write("<div class=\"inspire-exclusive__tab-menu cmp-tabs__tablist\" role=\"tablist\"");
                    {
                        Object var_attrvalue176 = renderContext.getObjectModel().resolveProperty(_global_tabs, "accessibilityLabel");
                        {
                            Object var_attrcontent177 = renderContext.call("xss", var_attrvalue176, "attribute");
                            {
                                boolean var_shoulddisplayattr179 = (((null != var_attrcontent177) && (!"".equals(var_attrcontent177))) && ((!"".equals(var_attrvalue176)) && (!((Object)false).equals(var_attrvalue176))));
                                if (var_shoulddisplayattr179) {
                                    out.write(" aria-label");
                                    {
                                        boolean var_istrueattr178 = (var_attrvalue176.equals(true));
                                        if (!var_istrueattr178) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent177));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" aria-multiselectable=\"false\">\r\n                ");
_global_isactive = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_tab, "name"), renderContext.getObjectModel().resolveProperty(_global_tabs, "activeItem")));
                    if (renderContext.getObjectModel().toBoolean(_global_isactive)) {
                    }
                    out.write("\r\n              ");
                    {
                        Object var_collectionvar180 = _global_items;
                        {
                            long var_size181 = ((var_collectionvar180_list_coerced$ == null ? (var_collectionvar180_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar180)) : var_collectionvar180_list_coerced$).size());
                            {
                                boolean var_notempty182 = (var_size181 > 0);
                                if (var_notempty182) {
                                    {
                                        long var_end185 = var_size181;
                                        {
                                            boolean var_validstartstepend186 = (((0 < var_size181) && true) && (var_end185 > 0));
                                            if (var_validstartstepend186) {
                                                if (var_collectionvar180_list_coerced$ == null) {
                                                    var_collectionvar180_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar180);
                                                }
                                                long var_index187 = 0;
                                                for (Object item : var_collectionvar180_list_coerced$) {
                                                    {
                                                        long itemlist_field$_index = var_index187;
                                                        {
                                                            boolean var_traversal189 = (((var_index187 >= 0) && (var_index187 <= var_end185)) && true);
                                                            if (var_traversal189) {
                                                                out.write("<div class=\"tab-menu-item\"");
                                                                {
                                                                    long var_attrvalue190 = itemlist_field$_index;
                                                                    {
                                                                        Object var_attrcontent191 = renderContext.call("xss", var_attrvalue190, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr193 = (((null != var_attrcontent191) && (!"".equals(var_attrcontent191))) && ((!"".equals(var_attrvalue190)) && (!((Object)false).equals(var_attrvalue190))));
                                                                            if (var_shoulddisplayattr193) {
                                                                                out.write(" color-tab");
                                                                                {
                                                                                    boolean var_istrueattr192 = (((Object)var_attrvalue190).equals(true));
                                                                                    if (!var_istrueattr192) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent191));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">\r\n                <h3");
                                                                {
                                                                    long var_attrvalue194 = itemlist_field$_index;
                                                                    {
                                                                        Object var_attrcontent195 = renderContext.call("xss", var_attrvalue194, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr197 = (((null != var_attrcontent195) && (!"".equals(var_attrcontent195))) && ((!"".equals(var_attrvalue194)) && (!((Object)false).equals(var_attrvalue194))));
                                                                            if (var_shoulddisplayattr197) {
                                                                                out.write(" color-title");
                                                                                {
                                                                                    boolean var_istrueattr196 = (((Object)var_attrvalue194).equals(true));
                                                                                    if (!var_istrueattr196) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent195));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(">");
                                                                {
                                                                    Object var_198 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "title"), "html");
                                                                    out.write(renderContext.getObjectModel().toString(var_198));
                                                                }
                                                                out.write("</h3>\r\n              </div>\n");
                                                            }
                                                        }
                                                    }
                                                    var_index187++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar180_list_coerced$ = null;
                    }
                    out.write("\r\n            </div>");
                }
            }
            out.write("\r\n            ");
            {
                Object var_collectionvar199 = _global_items;
                {
                    long var_size200 = ((var_collectionvar199_list_coerced$ == null ? (var_collectionvar199_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar199)) : var_collectionvar199_list_coerced$).size());
                    {
                        boolean var_notempty201 = (var_size200 > 0);
                        if (var_notempty201) {
                            {
                                long var_end204 = var_size200;
                                {
                                    boolean var_validstartstepend205 = (((0 < var_size200) && true) && (var_end204 > 0));
                                    if (var_validstartstepend205) {
                                        if (var_collectionvar199_list_coerced$ == null) {
                                            var_collectionvar199_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar199);
                                        }
                                        long var_index206 = 0;
                                        for (Object item : var_collectionvar199_list_coerced$) {
                                            {
                                                boolean var_traversal208 = (((var_index206 >= 0) && (var_index206 <= var_end204)) && true);
                                                if (var_traversal208) {
                                                    out.write("<div");
                                                    {
                                                        Object var_attrvalue209 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), "hashCode");
                                                        {
                                                            Object var_attrcontent210 = renderContext.call("xss", var_attrvalue209, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr212 = (((null != var_attrcontent210) && (!"".equals(var_attrcontent210))) && ((!"".equals(var_attrvalue209)) && (!((Object)false).equals(var_attrvalue209))));
                                                                if (var_shoulddisplayattr212) {
                                                                    out.write(" tabs-control-index");
                                                                    {
                                                                        boolean var_istrueattr211 = (var_attrvalue209.equals(true));
                                                                        if (!var_istrueattr211) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent210));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" role=\"tabpanel\"");
                                                    {
                                                        String var_attrcontent213 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-tab");
                                                        out.write(" aria-labelledby=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent213));
                                                        out.write("\"");
                                                    }
                                                    out.write(" class=\"inspire-exclusive__tab-list\">");
                                                    {
                                                        Object var_resourcecontent214 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                        out.write(renderContext.getObjectModel().toString(var_resourcecontent214));
                                                    }
                                                    out.write("</div>\n");
                                                }
                                            }
                                            var_index206++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar199_list_coerced$ = null;
            }
            out.write("\r\n          </div>\r\n          <div class=\"inspire-exclusive__tab-menu-mobile\">\r\n            ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_tabs, "children")) ? renderContext.getObjectModel().resolveProperty(_global_tabs, "children") : renderContext.getObjectModel().resolveProperty(_global_tabs, "items")));
            out.write("\r\n              ");
            {
                Object var_collectionvar215 = _global_items;
                {
                    long var_size216 = ((var_collectionvar215_list_coerced$ == null ? (var_collectionvar215_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar215)) : var_collectionvar215_list_coerced$).size());
                    {
                        boolean var_notempty217 = (var_size216 > 0);
                        if (var_notempty217) {
                            {
                                long var_end220 = var_size216;
                                {
                                    boolean var_validstartstepend221 = (((0 < var_size216) && true) && (var_end220 > 0));
                                    if (var_validstartstepend221) {
                                        if (var_collectionvar215_list_coerced$ == null) {
                                            var_collectionvar215_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar215);
                                        }
                                        long var_index222 = 0;
                                        for (Object item : var_collectionvar215_list_coerced$) {
                                            {
                                                long itemlist_field$_index = var_index222;
                                                {
                                                    boolean var_traversal224 = (((var_index222 >= 0) && (var_index222 <= var_end220)) && true);
                                                    if (var_traversal224) {
                                                        out.write("<div class=\"tab-menu-mobile-item\"");
                                                        {
                                                            long var_attrvalue225 = itemlist_field$_index;
                                                            {
                                                                Object var_attrcontent226 = renderContext.call("xss", var_attrvalue225, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr228 = (((null != var_attrcontent226) && (!"".equals(var_attrcontent226))) && ((!"".equals(var_attrvalue225)) && (!((Object)false).equals(var_attrvalue225))));
                                                                    if (var_shoulddisplayattr228) {
                                                                        out.write(" color-tab");
                                                                        {
                                                                            boolean var_istrueattr227 = (((Object)var_attrvalue225).equals(true));
                                                                            if (!var_istrueattr227) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent226));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(">\r\n                <div class=\"tab-menu-mobile-content\">\r\n                  <h3></h3>\r\n                  <div class=\"dropdown-title-icon\">\r\n                    ");
                                                        {
                                                            boolean var_testvariable229 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_global_lang, "Vietnamese"));
                                                            if (var_testvariable229) {
                                                                out.write("<p>Xem th\u00EAm</p>");
                                                            }
                                                        }
                                                        out.write("\r\n                    ");
                                                        {
                                                            boolean var_testvariable230 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_global_lang, "Vietnamese"));
                                                            if (var_testvariable230) {
                                                                out.write("<p>View more</p>");
                                                            }
                                                        }
                                                        out.write("\r\n                    <svg width=\"16\" height=\"10\" viewBox=\"0 0 16 10\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                      <path d=\"M14.12 -6.17205e-07L8 6.18084L1.88 \r\n                        -8.21774e-08L8.31755e-08 1.90283L8 \r\n                        10L16 1.90283L14.12 -6.17205e-07Z\" fill=\"#ED1B24\"></path>\r\n                    </svg>\r\n                  </div>\r\n                </div>\r\n                <div class=\"content-wrapper tab-list-mobile\"></div>\r\n                <div class=\"tab-menu-collapse-icon\">\r\n                  <div class=\"collapse-icon-content\">\r\n                    ");
                                                        {
                                                            boolean var_testvariable231 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_global_lang, "Vietnamese"));
                                                            if (var_testvariable231) {
                                                                out.write("<p>Thu g\u1ECDn</p>");
                                                            }
                                                        }
                                                        out.write("\r\n                    ");
                                                        {
                                                            boolean var_testvariable232 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_global_lang, "Vietnamese"));
                                                            if (var_testvariable232) {
                                                                out.write("<p>View less</p>");
                                                            }
                                                        }
                                                        out.write("\r\n                    <svg width=\"16\" height=\"10\" viewBox=\"0 0 16 10\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n                      <path d=\"M14.12 -6.17205e-07L8 6.18084L1.88 \r\n                      -8.21774e-08L8.31755e-08 1.90283L8 10L16 \r\n                      1.90283L14.12 -6.17205e-07Z\" fill=\"#ED1B24\"></path>\r\n                    </svg>\r\n                  </div>\r\n                </div>\r\n              </div>\n");
                                                    }
                                                }
                                            }
                                            var_index222++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar215_list_coerced$ = null;
            }
            out.write("\r\n            \r\n          </div>\r\n        </div>\r\n      </section>\r\n    ");
        }
    }
    out.write("\r\n  </section>");
}
out.write("\r\n  <!-- Parsys -->\r\n  ");
{
    Object var_testvariable233 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable233)) {
        {
            Object var_resourcecontent234 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), obj().with("cssClassName", "new section aem-Grid-newComponent").with("decorationTagName", "div").with("appendPath", "/*").with("resourceType", "wcm/foundation/components/parsys/newpar"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent234));
        }
    }
}
out.write("\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

