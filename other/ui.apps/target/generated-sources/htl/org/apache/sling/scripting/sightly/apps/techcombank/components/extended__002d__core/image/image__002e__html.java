/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.extended__002d__core.image;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class image__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_image = null;
Object _global_component = null;
Object _global_templates = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_resource = bindings.get("resource");
Object _global_usemap = null;
Object _dynamic_properties = bindings.get("properties");
Collection var_collectionvar73_list_coerced$ = null;
out.write("\r\n");
_global_image = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Image.class.getName(), obj());
_global_component = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Component.class.getName(), obj());
_global_templates = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_global_image, "src");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("<div data-cmp-is=\"image\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_image, "lazyEnabled");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-cmp-lazy");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_image, "lazyThreshold");
            {
                Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                {
                    boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                    if (var_shoulddisplayattr8) {
                        out.write(" data-cmp-lazythreshold");
                        {
                            boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                            if (!var_istrueattr7) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue9 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "srcUriTemplate")) ? renderContext.getObjectModel().resolveProperty(_global_image, "srcUriTemplate") : renderContext.getObjectModel().resolveProperty(_global_image, "src"));
            {
                Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                {
                    boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                    if (var_shoulddisplayattr12) {
                        out.write(" data-cmp-src");
                        {
                            boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                            if (!var_istrueattr11) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_image, "widths");
            {
                Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
                {
                    boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                    if (var_shoulddisplayattr16) {
                        out.write(" data-cmp-widths");
                        {
                            boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                            if (!var_istrueattr15) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_image, "dmImage");
            {
                Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                {
                    boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                    if (var_shoulddisplayattr20) {
                        out.write(" data-cmp-dmimage");
                        {
                            boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                            if (!var_istrueattr19) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_image, "smartCropRendition");
            {
                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                {
                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                    if (var_shoulddisplayattr24) {
                        out.write(" data-cmp-smartcroprendition");
                        {
                            boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                            if (!var_istrueattr23) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(_global_image, "fileReference");
            {
                Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                {
                    boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                    if (var_shoulddisplayattr28) {
                        out.write(" data-asset");
                        {
                            boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                            if (!var_istrueattr27) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(_global_image, "uuid");
            {
                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                {
                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                    if (var_shoulddisplayattr32) {
                        out.write(" data-asset-id");
                        {
                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                            if (!var_istrueattr31) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue33 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "title")) ? renderContext.getObjectModel().resolveProperty(_global_image, "title") : renderContext.getObjectModel().resolveProperty(_global_image, "alt")));
            {
                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                {
                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                    if (var_shoulddisplayattr36) {
                        out.write(" data-title");
                        {
                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                            if (!var_istrueattr35) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_component, "id");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" id");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_image, "data"), "json");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" data-cmp-data-layer");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrcontent45 = ("cmp-image" + renderContext.getObjectModel().toString(renderContext.call("xss", ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "disabled"))) ? " cq-dd-image" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
            out.write("\"");
        }
        out.write(" itemscope itemtype=\"http://schema.org/ImageObject\">\r\n    ");
        {
            boolean var_unwrapcondition46 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "link")));
            if (!var_unwrapcondition46) {
                out.write("<a class=\"cmp-image__link\"");
                {
                    Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(_global_image, "link");
                    {
                        Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "uri");
                        {
                            boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                            if (var_shoulddisplayattr50) {
                                out.write(" href");
                                {
                                    boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                    if (!var_istrueattr49) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    boolean var_attrvalue51 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "data")) ? true : false);
                    {
                        Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                        {
                            boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (false != var_attrvalue51)));
                            if (var_shoulddisplayattr54) {
                                out.write(" data-cmp-clickable");
                                {
                                    boolean var_istrueattr53 = (var_attrvalue51 == true);
                                    if (!var_istrueattr53) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write(" data-cmp-hook-image=\"link\">");
            }
            out.write("\r\n        ");
            {
                boolean var_unwrapcondition55 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_image, "smartCropRendition"), "SmartCrop:Auto")) && (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "lazyEnabled"))) && (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_image, "widths"), "length"), 1))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "areas")))));
                if (!var_unwrapcondition55) {
                    out.write("<noscript data-cmp-hook-image=\"noscript\">");
                }
                out.write("\r\n            ");
_global_usemap = renderContext.call("format", "{0}{1}", obj().with("format", (new Object[] {"#", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path")})));
                if (renderContext.getObjectModel().toBoolean(_global_usemap)) {
                }
                out.write("\r\n            <img");
                {
                    Object var_attrvalue60 = renderContext.getObjectModel().resolveProperty(_global_image, "src");
                    {
                        Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "uri");
                        {
                            boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                            if (var_shoulddisplayattr63) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                                    if (!var_istrueattr62) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    String var_attrcontent64 = ("cmp-image__image " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "isRounded")) ? "rounded" : ""), "attribute")));
                    out.write(" class=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                    out.write("\"");
                }
                out.write(" itemprop=\"contentUrl\" data-cmp-hook-image=\"image\"");
                {
                    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "alt");
                    {
                        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                        {
                            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                            if (var_shoulddisplayattr68) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                                    if (!var_istrueattr67) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue69 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle")) ? renderContext.getObjectModel().resolveProperty(_global_image, "title") : renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle")));
                    {
                        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
                        {
                            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
                            if (var_shoulddisplayattr72) {
                                out.write(" title");
                                {
                                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                                    if (!var_istrueattr71) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue_usemap56 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "areas")) ? _global_usemap : "");
                    {
                        Object var_attrvalueescaped_usemap57 = renderContext.call("xss", var_attrvalue_usemap56, "attribute", "usemap");
                        {
                            boolean var_shoulddisplayattr_usemap59 = (((null != var_attrvalueescaped_usemap57) && (!"".equals(var_attrvalueescaped_usemap57))) && ((!"".equals(var_attrvalue_usemap56)) && (!((Object)false).equals(var_attrvalue_usemap56))));
                            if (var_shoulddisplayattr_usemap59) {
                                out.write(" usemap");
                                {
                                    boolean var_istruevalue_usemap58 = (var_attrvalue_usemap56.equals(true));
                                    if (!var_istruevalue_usemap58) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrvalueescaped_usemap57));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n\r\n            ");
                {
                    Object var_testvariable80 = renderContext.getObjectModel().resolveProperty(_global_image, "areas");
                    if (renderContext.getObjectModel().toBoolean(var_testvariable80)) {
                        {
                            Object var_collectionvar73 = renderContext.getObjectModel().resolveProperty(_global_image, "areas");
                            {
                                long var_size74 = ((var_collectionvar73_list_coerced$ == null ? (var_collectionvar73_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar73)) : var_collectionvar73_list_coerced$).size());
                                {
                                    boolean var_notempty75 = (var_size74 > 0);
                                    if (var_notempty75) {
                                        {
                                            long var_end78 = var_size74;
                                            {
                                                boolean var_validstartstepend79 = (((0 < var_size74) && true) && (var_end78 > 0));
                                                if (var_validstartstepend79) {
                                                    out.write("<map");
                                                    {
                                                        Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path");
                                                        {
                                                            Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
                                                                if (var_shoulddisplayattr84) {
                                                                    out.write(" name");
                                                                    {
                                                                        boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                                                                        if (!var_istrueattr83) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-cmp-hook-image=\"map\">");
                                                    if (var_collectionvar73_list_coerced$ == null) {
                                                        var_collectionvar73_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar73);
                                                    }
                                                    long var_index85 = 0;
                                                    for (Object area : var_collectionvar73_list_coerced$) {
                                                        {
                                                            boolean var_traversal87 = (((var_index85 >= 0) && (var_index85 <= var_end78)) && true);
                                                            if (var_traversal87) {
                                                                out.write("\r\n                <area");
                                                                {
                                                                    Object var_attrvalue88 = renderContext.getObjectModel().resolveProperty(area, "shape");
                                                                    {
                                                                        Object var_attrcontent89 = renderContext.call("xss", var_attrvalue88, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr91 = (((null != var_attrcontent89) && (!"".equals(var_attrcontent89))) && ((!"".equals(var_attrvalue88)) && (!((Object)false).equals(var_attrvalue88))));
                                                                            if (var_shoulddisplayattr91) {
                                                                                out.write(" shape");
                                                                                {
                                                                                    boolean var_istrueattr90 = (var_attrvalue88.equals(true));
                                                                                    if (!var_istrueattr90) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(area, "coordinates");
                                                                    {
                                                                        Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                            if (var_shoulddisplayattr95) {
                                                                                out.write(" coords");
                                                                                {
                                                                                    boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                                    if (!var_istrueattr94) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(area, "href");
                                                                    {
                                                                        Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                                            if (var_shoulddisplayattr99) {
                                                                                out.write(" href");
                                                                                {
                                                                                    boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                                                    if (!var_istrueattr98) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(area, "target");
                                                                    {
                                                                        Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                                            if (var_shoulddisplayattr103) {
                                                                                out.write(" target");
                                                                                {
                                                                                    boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                                    if (!var_istrueattr102) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(area, "alt");
                                                                    {
                                                                        Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                                                                            if (var_shoulddisplayattr107) {
                                                                                out.write(" alt");
                                                                                {
                                                                                    boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                                                                    if (!var_istrueattr106) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" data-cmp-hook-image=\"area\"");
                                                                {
                                                                    Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(area, "relativeCoordinates");
                                                                    {
                                                                        Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                                                                            if (var_shoulddisplayattr111) {
                                                                                out.write(" data-cmp-relcoords");
                                                                                {
                                                                                    boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                                                                    if (!var_istrueattr110) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n            ");
                                                            }
                                                        }
                                                        var_index85++;
                                                    }
                                                    out.write("</map>");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var_collectionvar73_list_coerced$ = null;
                        }
                    }
                }
                out.write("\r\n        ");
                if (!var_unwrapcondition55) {
                    out.write("</noscript>");
                }
            }
            out.write("\r\n    ");
            if (!var_unwrapcondition46) {
                out.write("</a>");
            }
        }
        out.write("\r\n    ");
        {
            Object var_testvariable112 = (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle"))) ? renderContext.getObjectModel().resolveProperty(_global_image, "title") : (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle")))));
            if (renderContext.getObjectModel().toBoolean(var_testvariable112)) {
                out.write("<span class=\"cmp-image__title\" itemprop=\"caption\">");
                {
                    Object var_113 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_image, "title"), "text");
                    out.write(renderContext.getObjectModel().toString(var_113));
                }
                out.write("</span>");
            }
        }
        out.write("\r\n    ");
        {
            Object var_testvariable114 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle")) ? renderContext.getObjectModel().resolveProperty(_global_image, "title") : renderContext.getObjectModel().resolveProperty(_global_image, "displayPopupTitle")));
            if (renderContext.getObjectModel().toBoolean(var_testvariable114)) {
                out.write("<meta itemprop=\"caption\"");
                {
                    Object var_attrvalue115 = renderContext.getObjectModel().resolveProperty(_global_image, "title");
                    {
                        Object var_attrcontent116 = renderContext.call("xss", var_attrvalue115, "attribute");
                        {
                            boolean var_shoulddisplayattr118 = (((null != var_attrcontent116) && (!"".equals(var_attrcontent116))) && ((!"".equals(var_attrvalue115)) && (!((Object)false).equals(var_attrvalue115))));
                            if (var_shoulddisplayattr118) {
                                out.write(" content");
                                {
                                    boolean var_istrueattr117 = (var_attrvalue115.equals(true));
                                    if (!var_istrueattr117) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>");
            }
        }
        out.write("\r\n</div>");
    }
}
out.write("\r\n");
{
    Object var_templatevar119 = renderContext.getObjectModel().resolveProperty(_global_templates, "placeholder");
    {
        boolean var_templateoptions120_field$_isempty = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_image, "src")));
        {
            String var_templateoptions120_field$_classappend = "cmp-image cq-dd-image";
            {
                java.util.Map var_templateoptions120 = obj().with("isEmpty", var_templateoptions120_field$_isempty).with("classAppend", var_templateoptions120_field$_classappend);
                callUnit(out, renderContext, var_templatevar119, var_templateoptions120);
            }
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

