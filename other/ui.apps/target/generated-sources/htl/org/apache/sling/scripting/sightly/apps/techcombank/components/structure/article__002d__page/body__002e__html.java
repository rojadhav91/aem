/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.structure.article__002d__page;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class body__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_templatedcontainer = null;
Collection var_collectionvar0_list_coerced$ = null;
Collection var_collectionvar10_list_coerced$ = null;
Object _dynamic_properties = bindings.get("properties");
Object _dynamic_wcmmode = bindings.get("wcmmode");
out.write("\r\n<section class=\"container press-article\">\r\n    ");
_global_templatedcontainer = renderContext.call("use", com.day.cq.wcm.foundation.TemplatedContainer.class.getName(), obj());
{
    Object var_collectionvar0 = renderContext.getObjectModel().resolveProperty(_global_templatedcontainer, "structureResources");
    {
        long var_size1 = ((var_collectionvar0_list_coerced$ == null ? (var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0)) : var_collectionvar0_list_coerced$).size());
        {
            boolean var_notempty2 = (var_size1 > 0);
            if (var_notempty2) {
                {
                    long var_end5 = var_size1;
                    {
                        boolean var_validstartstepend6 = (((0 < var_size1) && true) && (var_end5 > 0));
                        if (var_validstartstepend6) {
                            if (var_collectionvar0_list_coerced$ == null) {
                                var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0);
                            }
                            long var_index7 = 0;
                            for (Object child : var_collectionvar0_list_coerced$) {
                                {
                                    boolean var_traversal9 = (((var_index7 >= 0) && (var_index7 <= var_end5)) && true);
                                    if (var_traversal9) {
                                        out.write("\r\n        ");
                                        {
                                            Object var_collectionvar10 = renderContext.getObjectModel().resolveProperty(child, "children");
                                            {
                                                long var_size11 = ((var_collectionvar10_list_coerced$ == null ? (var_collectionvar10_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar10)) : var_collectionvar10_list_coerced$).size());
                                                {
                                                    boolean var_notempty12 = (var_size11 > 0);
                                                    if (var_notempty12) {
                                                        {
                                                            long var_end15 = var_size11;
                                                            {
                                                                boolean var_validstartstepend16 = (((0 < var_size11) && true) && (var_end15 > 0));
                                                                if (var_validstartstepend16) {
                                                                    if (var_collectionvar10_list_coerced$ == null) {
                                                                        var_collectionvar10_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar10);
                                                                    }
                                                                    long var_index17 = 0;
                                                                    for (Object childelements : var_collectionvar10_list_coerced$) {
                                                                        {
                                                                            boolean var_traversal19 = (((var_index17 >= 0) && (var_index17 <= var_end15)) && true);
                                                                            if (var_traversal19) {
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable20 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "header"));
                                                                                    if (var_testvariable20) {
                                                                                        out.write("\r\n                ");
                                                                                        {
                                                                                            Object var_resourcecontent21 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent21));
                                                                                        }
                                                                                        out.write("\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable22 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "breadcrumb"));
                                                                                    if (var_testvariable22) {
                                                                                        out.write("\r\n                <div class=\"article-content--wrapper\">\r\n                    ");
                                                                                        {
                                                                                            Object var_resourcecontent23 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("decoration", "true").with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent23));
                                                                                        }
                                                                                        out.write("\r\n                </div>\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable24 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "articleandevents"));
                                                                                    if (var_testvariable24) {
                                                                                        out.write("\r\n                ");
                                                                                        {
                                                                                            Object var_resourcecontent25 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent25));
                                                                                        }
                                                                                        out.write("\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable26 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "container"));
                                                                                    if (var_testvariable26) {
                                                                                        out.write("\r\n                <div class=\"article-content--wrapper\">\r\n                    <div class=\"content-wrapper\">\r\n                    \t<div class=\"wrapper\">\r\n                        \t<div class=\"article-content\">\r\n                                ");
                                                                                        {
                                                                                            boolean var_testvariable27 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "hideTableOfContent")));
                                                                                            if (var_testvariable27) {
                                                                                                {
                                                                                                    Object var_resourcecontent28 = renderContext.call("includeResource", "articletableofcontent", obj().with("decoration", "true").with("resourceType", "techcombank/components/articletableofcontent"));
                                                                                                    out.write(renderContext.getObjectModel().toString(var_resourcecontent28));
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n                                ");
                                                                                        {
                                                                                            boolean var_testvariable29 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "container"));
                                                                                            if (var_testvariable29) {
                                                                                                out.write("\r\n                                    <div class=\"article-text\">\r\n                                        ");
                                                                                                {
                                                                                                    Object var_resourcecontent30 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                                    out.write(renderContext.getObjectModel().toString(var_resourcecontent30));
                                                                                                }
                                                                                                out.write("\r\n                                    </div>\r\n                                ");
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable31 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "sectioncontainer"));
                                                                                    if (var_testvariable31) {
                                                                                        out.write("\r\n                ");
                                                                                        {
                                                                                            Object var_testvariable32 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(childelements, "listChildren")) ? renderContext.getObjectModel().resolveProperty(childelements, "listChildren") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
                                                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable32)) {
                                                                                                out.write("\r\n                    ");
                                                                                                {
                                                                                                    Object var_resourcecontent33 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                                    out.write(renderContext.getObjectModel().toString(var_resourcecontent33));
                                                                                                }
                                                                                                out.write("\r\n                ");
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable34 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "articletagcloud"));
                                                                                    if (var_testvariable34) {
                                                                                        out.write("\r\n                ");
                                                                                        {
                                                                                            boolean var_testvariable35 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "hideArticleTagCloud")));
                                                                                            if (var_testvariable35) {
                                                                                                out.write("<div class=\"article-content__tag-cloud\">\r\n                    <div class=\"article-content__tag-cloud--wrapper\">\r\n                        <div class=\"content-body\">\r\n                            ");
                                                                                                {
                                                                                                    Object var_resourcecontent36 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                                    out.write(renderContext.getObjectModel().toString(var_resourcecontent36));
                                                                                                }
                                                                                                out.write("\r\n                        </div>\r\n                    </div>\r\n                </div>");
                                                                                            }
                                                                                        }
                                                                                        out.write("\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable37 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "experiencefragment"));
                                                                                    if (var_testvariable37) {
                                                                                        out.write("\r\n                <div class=\"article-content__social-share\">\r\n                    <div class=\"article-content__social-share--wrapper\">\r\n                        <div class=\"content-body\">\r\n                            ");
                                                                                        {
                                                                                            Object var_resourcecontent38 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent38));
                                                                                        }
                                                                                        out.write("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable39 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "articlerelatedpost"));
                                                                                    if (var_testvariable39) {
                                                                                        out.write("\r\n                <div class=\"article-footer--wrapper tcb-container\">\r\n                    ");
                                                                                        {
                                                                                            Object var_resourcecontent40 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent40));
                                                                                        }
                                                                                        out.write("\r\n                </div>\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n            ");
                                                                                {
                                                                                    boolean var_testvariable41 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(childelements, "name"), "footer"));
                                                                                    if (var_testvariable41) {
                                                                                        out.write("\r\n                <div>");
                                                                                        {
                                                                                            Object var_resourcecontent42 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(childelements, "path"), obj().with("resourceType", renderContext.getObjectModel().resolveProperty(childelements, "resourceType")));
                                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent42));
                                                                                        }
                                                                                        out.write("</div>\r\n            ");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n        ");
                                                                            }
                                                                        }
                                                                        var_index17++;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            var_collectionvar10_list_coerced$ = null;
                                        }
                                        out.write("\r\n    ");
                                    }
                                }
                                var_index7++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar0_list_coerced$ = null;
}
out.write("\r\n</section>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

