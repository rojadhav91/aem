/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.listeventtile;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class listeventtile__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_listeventitem = null;
Collection var_collectionvar5_list_coerced$ = null;
Object _dynamic_tems = bindings.get("tems");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_listeventitem = renderContext.call("use", com.techcombank.core.models.ListEventTileModel.class.getName(), obj());
out.write("\r\n    <section class=\"container list-event-tile\">\r\n        <div class=\"list-event-tile__container\">\r\n            <div class=\"list-event-tile__list-item\">\r\n                ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_listeventitem, "eventTileItem");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object eventitems : var_collectionvar5_list_coerced$) {
                                {
                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                    if (var_traversal14) {
                                        out.write("\r\n\t\t\t\t");
                                        {
                                            boolean var_testvariable15 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(eventitems, "linkText")));
                                            if (var_testvariable15) {
                                                out.write("\r\n\t\t\t\t\r\n                    <a data-tracking-click-event=\"linkClick\"");
                                                {
                                                    Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(eventitems, "linkInteraction");
                                                    {
                                                        Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                                                            if (var_shoulddisplayattr19) {
                                                                out.write(" data-tracking-web-interaction-value");
                                                                {
                                                                    boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                                                                    if (!var_istrueattr18) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrcontent20 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "cardTitle"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                    out.write("\"");
                                                }
                                                {
                                                    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(eventitems, "linkUrl");
                                                    {
                                                        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                            if (var_shoulddisplayattr24) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                    if (!var_istrueattr23) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(eventitems, "openInNewTab");
                                                    {
                                                        Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                            if (var_shoulddisplayattr28) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                    if (!var_istrueattr27) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(eventitems, "noFollow");
                                                    {
                                                        Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                            if (var_shoulddisplayattr32) {
                                                                out.write(" rel");
                                                                {
                                                                    boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                    if (!var_istrueattr31) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">\r\n\t\t\t\t\t");
                                            }
                                        }
                                        out.write("\r\n                        <div class=\"list-event-tile__item\">\r\n                            <div class=\"list-event-tile__item-container\">\r\n                                <div class=\"list-event-tile__item-img\">\r\n                                    <span>\r\n                                        <img");
                                        {
                                            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(eventitems, "cardImage");
                                            {
                                                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "uri");
                                                {
                                                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                    if (var_shoulddisplayattr36) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                            if (!var_istrueattr35) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(eventitems, "cardImageAltText");
                                            {
                                                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                    if (var_shoulddisplayattr40) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                            if (!var_istrueattr39) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                    </span>\r\n                                </div>\r\n                                <div class=\"list-event-tile__item-body\">\r\n                                    <div class=\"list-event-tile__item-content\">\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\r\n                                        ");
                                        {
                                            Object var_testvariable41 = (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(eventitems, "linkText"))) ? renderContext.getObjectModel().resolveProperty(eventitems, "cardLabelText") : (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(eventitems, "linkText")))));
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable41)) {
                                                out.write("<div class=\"list-event-tile__item-financial-advice\">\r\n\t\t\t\t\t\t\t\t\t\r\n                                            <span>");
                                                {
                                                    Object var_42 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "cardLabelText"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_42));
                                                }
                                                out.write("</span> \t\r\n                                      \t\t\t\t\t\t\t\t\t\t\r\n                                        </div>");
                                            }
                                        }
                                        out.write("\r\n\r\n                                       ");
                                        {
                                            Object var_testvariable43 = renderContext.getObjectModel().resolveProperty(eventitems, "linkText");
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable43)) {
                                                out.write("<div class=\"list-event-tile__item-financial-advice\">\t\t\t\t\t\t\t\t\t\t\r\n                                            <span>");
                                                {
                                                    Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "cardLabelText"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_44));
                                                }
                                                out.write("</span>\r\n                                            <span>");
                                                {
                                                    Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "eventDate"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_45));
                                                }
                                                out.write("</span>\r\n\t\t\t\t\t\t\t\t\t\t\r\n                                        </div>");
                                            }
                                        }
                                        out.write("\r\n\t\t\t\t\t\t\t\r\n\r\n                                        <div class=\"list-event-tile__item-title\">");
                                        {
                                            Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "cardTitle"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_46));
                                        }
                                        out.write("</div>\r\n                                        <div class=\"list-event-tile__item-description display-webkit-box\">\r\n                                            <span>");
                                        {
                                            String var_47 = (("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "cardDescription"), "html"))) + "\r\n                                            ");
                                            out.write(renderContext.getObjectModel().toString(var_47));
                                        }
                                        out.write("</span>\r\n                                            <span class=\"link-icon\">\r\n                                                <img");
                                        {
                                            Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(eventitems, "linkIcon");
                                            {
                                                Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "uri");
                                                {
                                                    boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                                    if (var_shoulddisplayattr51) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                                            if (!var_istrueattr50) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue52 = renderContext.getObjectModel().resolveProperty(eventitems, "linkIconAltText");
                                            {
                                                Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                                                    if (var_shoulddisplayattr55) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                                            if (!var_istrueattr54) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                            </span>\r\n                                        </div>\r\n                                    </div>\r\n                                    ");
                                        {
                                            Object var_testvariable56 = renderContext.getObjectModel().resolveProperty(eventitems, "linkText");
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable56)) {
                                                out.write("<div class=\"list-event-tile__item-action\">\r\n                                        <a");
                                                {
                                                    Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(eventitems, "linkUrl");
                                                    {
                                                        Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                                                            if (var_shoulddisplayattr60) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                                                                    if (!var_istrueattr59) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(eventitems, "openInNewTab");
                                                    {
                                                        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                                                            if (var_shoulddisplayattr64) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                                                    if (!var_istrueattr63) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_dynamic_tems, "noFollow");
                                                    {
                                                        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                                                            if (var_shoulddisplayattr68) {
                                                                out.write(" rel");
                                                                {
                                                                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                                                                    if (!var_istrueattr67) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"list-event-tile__item-action-link\" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(eventitems, "linkInteraction");
                                                    {
                                                        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
                                                            if (var_shoulddisplayattr72) {
                                                                out.write(" data-tracking-web-interaction-value");
                                                                {
                                                                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                                                                    if (!var_istrueattr71) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrcontent73 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "linkText"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent73));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_74 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(eventitems, "linkText"), "text"))) + "\r\n                                            ");
                                                    out.write(renderContext.getObjectModel().toString(var_74));
                                                }
                                                out.write("<img");
                                                {
                                                    Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(eventitems, "linkIcon");
                                                    {
                                                        Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                                                            if (var_shoulddisplayattr78) {
                                                                out.write(" src");
                                                                {
                                                                    boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                                                                    if (!var_istrueattr77) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(eventitems, "linkIconAltText");
                                                    {
                                                        Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
                                                            if (var_shoulddisplayattr82) {
                                                                out.write(" alt");
                                                                {
                                                                    boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                                                                    if (!var_istrueattr81) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        </a>\r\n                                    </div>");
                                            }
                                        }
                                        out.write("\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                   ");
                                        {
                                            boolean var_testvariable83 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(eventitems, "linkText")));
                                            if (var_testvariable83) {
                                                out.write("\r\n\t\t\t\t\r\n                    </a>\r\n\t\t\t\t\t");
                                            }
                                        }
                                        out.write("\r\n                ");
                                    }
                                }
                                var_index12++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

