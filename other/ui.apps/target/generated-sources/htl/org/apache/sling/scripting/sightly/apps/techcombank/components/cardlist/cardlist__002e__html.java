/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.cardlist;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class cardlist__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Collection var_collectionvar87_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.CardListingModel.class.getName(), obj());
out.write("\r\n<div class=\"sectioncontainer tcb-full-width credit-card-listing-section\"");
{
    Object var_attrvalue5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardCFRootPath"), "html");
    {
        boolean var_shoulddisplayattr8 = ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5)));
        if (var_shoulddisplayattr8) {
            out.write(" data-cfPath");
            {
                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                if (!var_istrueattr7) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue5));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardTags"), "html");
    {
        boolean var_shoulddisplayattr12 = ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9)));
        if (var_shoulddisplayattr12) {
            out.write(" data-cardoptions");
            {
                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                if (!var_istrueattr11) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue9));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue13 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "nudgeTags"), "html");
    {
        boolean var_shoulddisplayattr16 = ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13)));
        if (var_shoulddisplayattr16) {
            out.write(" data-nudgeoptions");
            {
                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                if (!var_istrueattr15) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue13));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_model, "regCtaMobile");
    {
        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
        {
            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
            if (var_shoulddisplayattr20) {
                out.write(" data-registerLink");
                {
                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                    if (!var_istrueattr19) {
                        out.write("='");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                        out.write("'");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
    {
        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
        {
            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
            if (var_shoulddisplayattr24) {
                out.write(" data-component-name");
                {
                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                    if (!var_istrueattr23) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <div class=\"tcb-sectionContainer\">\r\n        <div class=\"tcb-content-container\">\r\n            <div class=\"credit-card-listing\">\r\n                <div class=\"credit-card-listing__content\">\r\n                    <div class=\"content-wrapper credit-card-listing__container\">\r\n                        <h6 class=\"credit-card-listing__title\">");
{
    Object var_25 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardTypeLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_25));
}
out.write("</h6>\r\n                        <div class=\"credit-card-listing__items\">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"list-card-product__container content-wrapper\">\r\n                    <div class=\"list-card-product__grid-container\">\r\n\r\n                        <div class=\"card-product-banner\">\r\n                            <div class=\"card-product-banner__container\">\r\n                                <div class=\"card-product-banner__wrapper\">\r\n                                    <a class=\"card-product-banner__link\"");
{
    Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_global_model, "bannerLink");
    {
        Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "uri");
        {
            boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
            if (var_shoulddisplayattr29) {
                out.write(" href");
                {
                    boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                    if (!var_istrueattr28) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(_global_model, "bannerTarget");
    {
        Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
        {
            boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
            if (var_shoulddisplayattr33) {
                out.write(" target");
                {
                    boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                    if (!var_istrueattr32) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue34 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "bannerFollow")) ? "nofollow" : "");
    {
        Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
        {
            boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
            if (var_shoulddisplayattr37) {
                out.write(" rel");
                {
                    boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                    if (!var_istrueattr36) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                        <div aria-hidden=\"true\" class=\"card-product-banner__content\">\r\n                                            <img");
{
    Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAlt");
    {
        Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
        {
            boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
            if (var_shoulddisplayattr41) {
                out.write(" alt");
                {
                    boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                    if (!var_istrueattr40) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" class=\"card-product-banner__img\"");
{
    Object var_attrvalue42 = renderContext.getObjectModel().resolveProperty(_global_model, "bannerImg");
    {
        Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "uri");
        {
            boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
            if (var_shoulddisplayattr45) {
                out.write(" src");
                {
                    boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                    if (!var_istrueattr44) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <div class=\"card-product-banner__text\">\r\n                                                <div class=\"card-product-banner__text__title\">");
{
    Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bannerTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_46));
}
out.write("</div>\r\n                                                <div class=\"card-product-banner__text__description\">");
{
    String var_47 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bannerDesc"), "html"))) + "\r\n                                                ");
    out.write(renderContext.getObjectModel().toString(var_47));
}
out.write("</div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </a>\r\n                                    <div class=\"card-product-banner__close\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n                <div class=\"container compare-choosing hidden\">\r\n                    <div class=\"compare-choosing__sticky-card\">\r\n                        <div class=\"content-wrapper compare-choosing__container\">\r\n                            <div class=\"compare-choosing__list-card\">\r\n                                <div class=\"list-card__item\">\r\n                                    <div class=\"list-card__item-image\">\r\n                  <span>\r\n                    <img alt=\"\" data-nimg=\"fixed\" decoding=\"async\" src=\"\"/>\r\n                  </span>\r\n                                    </div>\r\n                                    <span aria-hidden=\"true\" class=\"remove-button\">\r\n                  <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_white.svg\"/>\r\n                </span>\r\n                                    <div aria-hidden=\"true\" class=\"add-panel\">\r\n                                        <img alt=\"\" class=\"icon-svg-add\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg\"/>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"list-card__item\">\r\n                                    <div class=\"list-card__item-image\">\r\n                  <span>\r\n                    <img alt=\"\" data-nimg=\"fixed\" decoding=\"async\" src=\"\"/>\r\n                  </span>\r\n                                    </div>\r\n                                    <span aria-hidden=\"true\" class=\"remove-button\">\r\n                  <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_white.svg\"/>\r\n                </span>\r\n                                    <div aria-hidden=\"true\" class=\"add-panel\">\r\n                                        <img alt=\"\" class=\"icon-svg-add\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg\"/>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"list-card__item\">\r\n                                    <div class=\"list-card__item-image\">\r\n                  <span>\r\n                    <img alt=\"\" data-nimg=\"fixed\" decoding=\"async\" src=\"\"/>\r\n                  </span>\r\n                                    </div>\r\n                                    <span aria-hidden=\"true\" class=\"remove-button\">\r\n                  <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_white.svg\"/>\r\n                </span>\r\n                                    <div aria-hidden=\"true\" class=\"add-panel\">\r\n                                        <img alt=\"\" class=\"icon-svg-add\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg\"/>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"compare-button\">\r\n                                <a class=\"compare-button__link\" disabled=\"\"");
{
    Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(_global_model, "ctaCompLink");
    {
        Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "uri");
        {
            boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
            if (var_shoulddisplayattr51) {
                out.write(" href");
                {
                    boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                    if (!var_istrueattr50) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue52 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "ctaCompFollow")) ? "noopener noreferrer" : "");
    {
        Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
        {
            boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
            if (var_shoulddisplayattr55) {
                out.write(" rel");
                {
                    boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                    if (!var_istrueattr54) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue56 = renderContext.getObjectModel().resolveProperty(_global_model, "ctaCompTarget");
    {
        Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
        {
            boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
            if (var_shoulddisplayattr59) {
                out.write(" target");
                {
                    boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                    if (!var_istrueattr58) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_60 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "ctaCompLabel"), "text"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_60));
}
out.write("<div class=\"compare-button__link-icon\">\r\n                                        <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg?w=1920&q=75\"/>\r\n                                    </div>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"popup\">\r\n                    <div class=\"popup-content\">\r\n                        <img alt=\"\" class=\"close\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_red.svg\"/>\r\n                        <div class=\"popup-register_image\">\r\n                            <picture>\r\n                                <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(_global_model, "regImgMobile");
    {
        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "attribute");
        {
            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
            if (var_shoulddisplayattr64) {
                out.write(" srcset");
                {
                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                    if (!var_istrueattr63) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_global_model, "regImgMobile");
    {
        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
        {
            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
            if (var_shoulddisplayattr68) {
                out.write(" srcset");
                {
                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                    if (!var_istrueattr67) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
        {
            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
            if (var_shoulddisplayattr72) {
                out.write(" srcset");
                {
                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                    if (!var_istrueattr71) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue73 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent74 = renderContext.call("xss", var_attrvalue73, "attribute");
        {
            boolean var_shoulddisplayattr76 = (((null != var_attrcontent74) && (!"".equals(var_attrcontent74))) && ((!"".equals(var_attrvalue73)) && (!((Object)false).equals(var_attrvalue73))));
            if (var_shoulddisplayattr76) {
                out.write(" srcset");
                {
                    boolean var_istrueattr75 = (var_attrvalue73.equals(true));
                    if (!var_istrueattr75) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent74));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <img");
{
    Object var_attrvalue77 = renderContext.getObjectModel().resolveProperty(_global_model, "imageRegAlt");
    {
        Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "attribute");
        {
            boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
            if (var_shoulddisplayattr80) {
                out.write(" alt");
                {
                    boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                    if (!var_istrueattr79) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"responsive\" sizes=\"100vw\" decoding=\"async\"");
{
    Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "uri");
        {
            boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
            if (var_shoulddisplayattr84) {
                out.write(" src");
                {
                    boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                    if (!var_istrueattr83) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                            </picture>\r\n                        </div>\r\n                        <div class=\"popup-register_content\">\r\n                            <div class=\"popup-register_contentTop\">\r\n                                <h4 class=\"popup-register_title\">");
{
    Object var_85 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "regTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_85));
}
out.write("</h4>\r\n                                <div class=\"popup-register_desc\">\r\n                                    <div class=\"popup-register_text\">\r\n                                        <p>");
{
    Object var_86 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "regSubTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_86));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"popup-register_steps\">\r\n                                    ");
{
    Object var_collectionvar87 = renderContext.getObjectModel().resolveProperty(_global_model, "steps");
    {
        long var_size88 = ((var_collectionvar87_list_coerced$ == null ? (var_collectionvar87_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar87)) : var_collectionvar87_list_coerced$).size());
        {
            boolean var_notempty89 = (var_size88 > 0);
            if (var_notempty89) {
                {
                    long var_end92 = var_size88;
                    {
                        boolean var_validstartstepend93 = (((0 < var_size88) && true) && (var_end92 > 0));
                        if (var_validstartstepend93) {
                            if (var_collectionvar87_list_coerced$ == null) {
                                var_collectionvar87_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar87);
                            }
                            long var_index94 = 0;
                            for (Object item : var_collectionvar87_list_coerced$) {
                                {
                                    long itemlist_field$_count = (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.ADD.eval(var_index94, 1)).longValue());
                                    {
                                        boolean var_traversal96 = (((var_index94 >= 0) && (var_index94 <= var_end92)) && true);
                                        if (var_traversal96) {
                                            out.write("\r\n                                        <div class=\"popup-register_item popup-register_step\">\r\n                                            <h6><span>");
                                            {
                                                String var_97 = (renderContext.getObjectModel().toString(renderContext.call("xss", itemlist_field$_count, "text")) + ".");
                                                out.write(renderContext.getObjectModel().toString(var_97));
                                            }
                                            out.write("</span>");
                                            {
                                                Object var_98 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "stepTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_98));
                                            }
                                            out.write("</h6>\r\n                                            <div class=\"popup-register_titleDescOuter\">\r\n                                                <div class=\"popup-register_titleDesc\">\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
                                            {
                                                Object var_attrvalue99 = renderContext.getObjectModel().resolveProperty(item, "stepImgMobile");
                                                {
                                                    Object var_attrcontent100 = renderContext.call("xss", var_attrvalue99, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr102 = (((null != var_attrcontent100) && (!"".equals(var_attrcontent100))) && ((!"".equals(var_attrvalue99)) && (!((Object)false).equals(var_attrvalue99))));
                                                        if (var_shoulddisplayattr102) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr101 = (var_attrvalue99.equals(true));
                                                                if (!var_istrueattr101) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent100));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
                                            {
                                                Object var_attrvalue103 = renderContext.getObjectModel().resolveProperty(item, "stepImgMobile");
                                                {
                                                    Object var_attrcontent104 = renderContext.call("xss", var_attrvalue103, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr106 = (((null != var_attrcontent104) && (!"".equals(var_attrcontent104))) && ((!"".equals(var_attrvalue103)) && (!((Object)false).equals(var_attrvalue103))));
                                                        if (var_shoulddisplayattr106) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr105 = (var_attrvalue103.equals(true));
                                                                if (!var_istrueattr105) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
                                            {
                                                Object var_attrvalue107 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent108 = renderContext.call("xss", var_attrvalue107, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr110 = (((null != var_attrcontent108) && (!"".equals(var_attrcontent108))) && ((!"".equals(var_attrvalue107)) && (!((Object)false).equals(var_attrvalue107))));
                                                        if (var_shoulddisplayattr110) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr109 = (var_attrvalue107.equals(true));
                                                                if (!var_istrueattr109) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent108));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
                                            {
                                                Object var_attrvalue111 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent112 = renderContext.call("xss", var_attrvalue111, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr114 = (((null != var_attrcontent112) && (!"".equals(var_attrcontent112))) && ((!"".equals(var_attrvalue111)) && (!((Object)false).equals(var_attrvalue111))));
                                                        if (var_shoulddisplayattr114) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr113 = (var_attrvalue111.equals(true));
                                                                if (!var_istrueattr113) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent112));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                        <img");
                                            {
                                                Object var_attrvalue115 = renderContext.getObjectModel().resolveProperty(item, "imageStpAlt");
                                                {
                                                    Object var_attrcontent116 = renderContext.call("xss", var_attrvalue115, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr118 = (((null != var_attrcontent116) && (!"".equals(var_attrcontent116))) && ((!"".equals(var_attrvalue115)) && (!((Object)false).equals(var_attrvalue115))));
                                                        if (var_shoulddisplayattr118) {
                                                            out.write(" alt");
                                                            {
                                                                boolean var_istrueattr117 = (var_attrvalue115.equals(true));
                                                                if (!var_istrueattr117) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(" data-nimg=\"intrinsic\" decoding=\"async\"");
                                            {
                                                Object var_attrvalue119 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent120 = renderContext.call("xss", var_attrvalue119, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr122 = (((null != var_attrcontent120) && (!"".equals(var_attrcontent120))) && ((!"".equals(var_attrvalue119)) && (!((Object)false).equals(var_attrvalue119))));
                                                        if (var_shoulddisplayattr122) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr121 = (var_attrvalue119.equals(true));
                                                                if (!var_istrueattr121) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent120));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                    </picture>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    ");
                                        }
                                    }
                                }
                                var_index94++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar87_list_coerced$ = null;
}
out.write("\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"popup-register_contentBottom\">\r\n                                <p class=\"popup-register_title\">");
{
    Object var_123 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bottomText"), "text");
    out.write(renderContext.getObjectModel().toString(var_123));
}
out.write("</p>\r\n                                <div class=\"popup-register_linkGroup\">\r\n                                    <div class=\"popup-register_linkItem\">\r\n                                        <a");
{
    Object var_attrvalue124 = renderContext.getObjectModel().resolveProperty(_global_model, "bottomLink");
    {
        Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "uri");
        {
            boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
            if (var_shoulddisplayattr127) {
                out.write(" href");
                {
                    boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                    if (!var_istrueattr126) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue128 = renderContext.getObjectModel().resolveProperty(_global_model, "ctaBtmTarget");
    {
        Object var_attrcontent129 = renderContext.call("xss", var_attrvalue128, "attribute");
        {
            boolean var_shoulddisplayattr131 = (((null != var_attrcontent129) && (!"".equals(var_attrcontent129))) && ((!"".equals(var_attrvalue128)) && (!((Object)false).equals(var_attrvalue128))));
            if (var_shoulddisplayattr131) {
                out.write(" target");
                {
                    boolean var_istrueattr130 = (var_attrvalue128.equals(true));
                    if (!var_istrueattr130) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent129));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue132 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "ctaBtmFollow")) ? "noopener noreferrer" : "");
    {
        Object var_attrcontent133 = renderContext.call("xss", var_attrvalue132, "attribute");
        {
            boolean var_shoulddisplayattr135 = (((null != var_attrcontent133) && (!"".equals(var_attrcontent133))) && ((!"".equals(var_attrvalue132)) && (!((Object)false).equals(var_attrvalue132))));
            if (var_shoulddisplayattr135) {
                out.write(" rel");
                {
                    boolean var_istrueattr134 = (var_attrvalue132.equals(true));
                    if (!var_istrueattr134) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent133));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                            <span class=\"link-text\">");
{
    Object var_136 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bottomLinkLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_136));
}
out.write("</span>\r\n                                            <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg?w=1920&q=75\"/>\r\n                                        </a>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

