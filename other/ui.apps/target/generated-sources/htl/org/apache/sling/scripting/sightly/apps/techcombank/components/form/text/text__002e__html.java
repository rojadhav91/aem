/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.text;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class text__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_text = null;
Object _dynamic_properties = bindings.get("properties");
Object _global_usetextarea = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n\r\n\r\n");
_global_text = renderContext.call("use", com.adobe.cq.wcm.core.components.models.form.Text.class.getName(), obj());
out.write("<div");
{
    String var_attrcontent5 = ("cmp-form-text " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "analytics")) ? "analytics-form-field" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
out.write(">\r\n\r\n    <label");
{
    Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_text, "id");
    {
        Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
        {
            boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
            if (var_shoulddisplayattr9) {
                out.write(" for");
                {
                    boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                    if (!var_istrueattr8) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue10 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "date")) ? "text-input-label" : "");
    {
        Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
        {
            boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
            if (var_shoulddisplayattr13) {
                out.write(" class");
                {
                    boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                    if (!var_istrueattr12) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    Object var_14 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_text, "title"), "text");
    out.write(renderContext.getObjectModel().toString(var_14));
}
{
    Object var_testvariable15 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "required");
    if (renderContext.getObjectModel().toBoolean(var_testvariable15)) {
        out.write("<span> *</span>");
    }
}
out.write("</label>\r\n\r\n    ");
{
    boolean var_testvariable16 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "date"));
    if (var_testvariable16) {
        out.write("<div class=\"datepicker\">\r\n        <div");
        {
            String var_attrvalue17 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_text, "readOnly")) ? "" : "datepicker-section");
            {
                Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                {
                    boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                    if (var_shoulddisplayattr20) {
                        out.write(" class");
                        {
                            boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                            if (!var_istrueattr19) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n            <input type=\"text\" data-type=\"date\"");
        {
            Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_text, "name");
            {
                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                {
                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                    if (var_shoulddisplayattr24) {
                        out.write(" name");
                        {
                            boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                            if (!var_istrueattr23) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"datepicker-input\" data-change-year=\"true\"");
        {
            Object var_attrvalue25 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "year"), "attribute");
            {
                boolean var_shoulddisplayattr28 = ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25)));
                if (var_shoulddisplayattr28) {
                    out.write(" data-year-title");
                    {
                        boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                        if (!var_istrueattr27) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrvalue25));
                            out.write("\"");
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(_global_text, "readOnly");
            {
                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                {
                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                    if (var_shoulddisplayattr32) {
                        out.write(" readonly");
                        {
                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                            if (!var_istrueattr31) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(_global_text, "requiredMessage");
            {
                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                {
                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                    if (var_shoulddisplayattr36) {
                        out.write(" data-required");
                        {
                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                            if (!var_istrueattr35) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_text, "placeholder");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n            ");
        {
            boolean var_testvariable41 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_text, "readOnly")));
            if (var_testvariable41) {
                out.write("<div class=\"date-icon\">\r\n                <div>\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar.svg\"/>\r\n                </div>\r\n            </div>");
            }
        }
        out.write("\r\n        </div>\r\n    </div>");
    }
}
out.write("\r\n    ");
_global_usetextarea = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "textarea"));
if (renderContext.getObjectModel().toBoolean(_global_usetextarea)) {
    out.write("<textarea class=\"cmp-form-text__textarea\" data-cmp-hook-form-text=\"input\"");
    {
        Object var_attrvalue46 = renderContext.getObjectModel().resolveProperty(_global_text, "id");
        {
            Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
            {
                boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (!((Object)false).equals(var_attrvalue46))));
                if (var_shoulddisplayattr49) {
                    out.write(" id");
                    {
                        boolean var_istrueattr48 = (var_attrvalue46.equals(true));
                        if (!var_istrueattr48) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(_global_text, "placeholder");
        {
            Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
            {
                boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                if (var_shoulddisplayattr53) {
                    out.write(" placeholder");
                    {
                        boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                        if (!var_istrueattr52) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue54 = renderContext.getObjectModel().resolveProperty(_global_text, "name");
        {
            Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "attribute");
            {
                boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                if (var_shoulddisplayattr57) {
                    out.write(" name");
                    {
                        boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                        if (!var_istrueattr56) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(_global_text, "readOnly");
        {
            Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
            {
                boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                if (var_shoulddisplayattr61) {
                    out.write(" readonly");
                    {
                        boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                        if (!var_istrueattr60) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue62 = renderContext.getObjectModel().resolveProperty(_global_text, "requiredMessage");
        {
            Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
            {
                boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                if (var_shoulddisplayattr65) {
                    out.write(" data-required");
                    {
                        boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                        if (!var_istrueattr64) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue66 = renderContext.getObjectModel().resolveProperty(_global_text, "required");
        {
            Object var_attrcontent67 = renderContext.call("xss", var_attrvalue66, "attribute");
            {
                boolean var_shoulddisplayattr69 = (((null != var_attrcontent67) && (!"".equals(var_attrcontent67))) && ((!"".equals(var_attrvalue66)) && (!((Object)false).equals(var_attrvalue66))));
                if (var_shoulddisplayattr69) {
                    out.write(" required");
                    {
                        boolean var_istrueattr68 = (var_attrvalue66.equals(true));
                        if (!var_istrueattr68) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent67));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue70 = renderContext.getObjectModel().resolveProperty(_global_text, "rows");
        {
            Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "attribute");
            {
                boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                if (var_shoulddisplayattr73) {
                    out.write(" rows");
                    {
                        boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                        if (!var_istrueattr72) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue_aria_describedby42 = renderContext.call("join", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_text, "helpMessage")) ? (new Object[] {renderContext.getObjectModel().resolveProperty(_global_text, "id"), "helpMessage"}) : ""), "-");
        {
            Object var_attrvalueescaped_aria_describedby43 = renderContext.call("xss", var_attrvalue_aria_describedby42, "attribute", "aria-describedby");
            {
                boolean var_shoulddisplayattr_aria_describedby45 = (((null != var_attrvalueescaped_aria_describedby43) && (!"".equals(var_attrvalueescaped_aria_describedby43))) && ((!"".equals(var_attrvalue_aria_describedby42)) && (!((Object)false).equals(var_attrvalue_aria_describedby42))));
                if (var_shoulddisplayattr_aria_describedby45) {
                    out.write(" aria-describedby");
                    {
                        boolean var_istruevalue_aria_describedby44 = (var_attrvalue_aria_describedby42.equals(true));
                        if (!var_istruevalue_aria_describedby44) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrvalueescaped_aria_describedby43));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("></textarea>");
}
out.write("\r\n\r\n\r\n    ");
{
    boolean var_testvariable82 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "textarea")) && (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "date")));
    if (var_testvariable82) {
        out.write("<input");
        {
            Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type");
            {
                Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
                {
                    boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                    if (var_shoulddisplayattr86) {
                        out.write(" data-type");
                        {
                            boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                            if (!var_istrueattr85) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cmp-form-text__text\" data-cmp-hook-form-text=\"input\"");
        {
            Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(_global_text, "id");
            {
                Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "attribute");
                {
                    boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                    if (var_shoulddisplayattr90) {
                        out.write(" id");
                        {
                            boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                            if (!var_istrueattr89) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue91 = renderContext.getObjectModel().resolveProperty(_global_text, "name");
            {
                Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
                {
                    boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                    if (var_shoulddisplayattr94) {
                        out.write(" name");
                        {
                            boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                            if (!var_istrueattr93) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue95 = renderContext.getObjectModel().resolveProperty(_global_text, "readOnly");
            {
                Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "attribute");
                {
                    boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
                    if (var_shoulddisplayattr98) {
                        out.write(" readonly");
                        {
                            boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                            if (!var_istrueattr97) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue99 = ((!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "email")) ? renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type") : "text");
            {
                Object var_attrcontent100 = renderContext.call("xss", var_attrvalue99, "attribute");
                {
                    boolean var_shoulddisplayattr102 = (((null != var_attrcontent100) && (!"".equals(var_attrcontent100))) && ((!"".equals(var_attrvalue99)) && (!((Object)false).equals(var_attrvalue99))));
                    if (var_shoulddisplayattr102) {
                        out.write(" type");
                        {
                            boolean var_istrueattr101 = (var_attrvalue99.equals(true));
                            if (!var_istrueattr101) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent100));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue103 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "usePlaceholder")) ? renderContext.getObjectModel().resolveProperty(_global_text, "helpMessage") : "");
            {
                Object var_attrcontent104 = renderContext.call("xss", var_attrvalue103, "attribute");
                {
                    boolean var_shoulddisplayattr106 = (((null != var_attrcontent104) && (!"".equals(var_attrcontent104))) && ((!"".equals(var_attrvalue103)) && (!((Object)false).equals(var_attrvalue103))));
                    if (var_shoulddisplayattr106) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr105 = (var_attrvalue103.equals(true));
                            if (!var_istrueattr105) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue107 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "email")) ? renderContext.getObjectModel().resolveProperty(_global_text, "constraintMessage") : "");
            {
                Object var_attrcontent108 = renderContext.call("xss", var_attrvalue107, "attribute");
                {
                    boolean var_shoulddisplayattr110 = (((null != var_attrcontent108) && (!"".equals(var_attrcontent108))) && ((!"".equals(var_attrvalue107)) && (!((Object)false).equals(var_attrvalue107))));
                    if (var_shoulddisplayattr110) {
                        out.write(" data-format");
                        {
                            boolean var_istrueattr109 = (var_attrvalue107.equals(true));
                            if (!var_istrueattr109) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent108));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            boolean var_attrvalue_data_is_email_type78 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "type"), "email"));
            {
                Object var_attrvalueescaped_data_is_email_type79 = renderContext.call("xss", var_attrvalue_data_is_email_type78, "attribute", "data-is-email-type");
                {
                    boolean var_shoulddisplayattr_data_is_email_type81 = (((null != var_attrvalueescaped_data_is_email_type79) && (!"".equals(var_attrvalueescaped_data_is_email_type79))) && ((!"".equals(var_attrvalue_data_is_email_type78)) && (false != var_attrvalue_data_is_email_type78)));
                    if (var_shoulddisplayattr_data_is_email_type81) {
                        out.write(" data-is-email-type");
                        {
                            boolean var_istruevalue_data_is_email_type80 = (var_attrvalue_data_is_email_type78 == true);
                            if (!var_istruevalue_data_is_email_type80) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrvalueescaped_data_is_email_type79));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue_data_required74 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "required")) ? renderContext.getObjectModel().resolveProperty(_global_text, "requiredMessage") : "");
            {
                Object var_attrvalueescaped_data_required75 = renderContext.call("xss", var_attrvalue_data_required74, "attribute", "data-required");
                {
                    boolean var_shoulddisplayattr_data_required77 = (((null != var_attrvalueescaped_data_required75) && (!"".equals(var_attrvalueescaped_data_required75))) && ((!"".equals(var_attrvalue_data_required74)) && (!((Object)false).equals(var_attrvalue_data_required74))));
                    if (var_shoulddisplayattr_data_required77) {
                        out.write(" data-required");
                        {
                            boolean var_istruevalue_data_required76 = (var_attrvalue_data_required74.equals(true));
                            if (!var_istruevalue_data_required76) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrvalueescaped_data_required75));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
}
out.write("\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

