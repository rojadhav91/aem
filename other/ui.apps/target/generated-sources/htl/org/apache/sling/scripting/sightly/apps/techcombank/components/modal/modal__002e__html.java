/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.modal;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class modal__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_modal = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
_global_modal = renderContext.call("use", com.techcombank.core.models.ModalModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_modal, "id");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div");
            {
                String var_attrcontent1 = ((renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "popup-editable" : "popup modal-hidden"), "attribute")) + " popup-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_modal, "size"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            {
                Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_modal, "id");
                {
                    Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                    {
                        boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                        if (var_shoulddisplayattr5) {
                            out.write(" id");
                            {
                                boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                if (!var_istrueattr4) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <div class=\"popup__container\">\r\n                <div class=\"popup__container-item\">\r\n                    <div class=\"popup__header\">\r\n                        <span class=\"popup__header-title\">");
            {
                Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_modal, "title"), "text");
                out.write(renderContext.getObjectModel().toString(var_6));
            }
            out.write("</span>\r\n                        <span class=\"close popup__close-icon\">\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/gray-close.svg\"/>\r\n                        </span>\r\n                    </div>\r\n                    <div class=\"popup__content\"");
            {
                String var_attrcontent7 = (("padding:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_modal, "padding"), "styleString"))) + "px;");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                out.write("\"");
            }
            out.write(">\r\n                        <span class=\"close popup__close-icon\">\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_red.svg\"/>\r\n                        </span>\r\n                        ");
            {
                Object var_resourcecontent8 = renderContext.call("includeResource", "modal-body", obj().with("resourceType", "wcm/foundation/components/parsys"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent8));
            }
            out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar9 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions10_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions10 = obj().with("isEmpty", var_templateoptions10_field$_isempty);
            callUnit(out, renderContext, var_templatevar9, var_templateoptions10);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

