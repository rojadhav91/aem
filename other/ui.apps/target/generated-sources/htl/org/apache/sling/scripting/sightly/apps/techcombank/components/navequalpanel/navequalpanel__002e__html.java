/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.navequalpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class navequalpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_navequalpanelmodel = null;
Collection var_collectionvar5_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n  <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_navequalpanelmodel = renderContext.call("use", com.techcombank.core.models.NavEqualPanelModel.class.getName(), obj());
out.write("\r\n  <div class=\"nav-equal-panel\">\r\n    <div class=\"with-cta-container\">\r\n      <div class=\"nav-equal__content\">\r\n        <div class=\"with-cta-root\">\r\n          <div class=\"with-cta-grid\">\r\n            <div class=\"with-cta-innergrid\">\r\n              ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_navequalpanelmodel, "navPanelSection");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object navpanelsectionitem : var_collectionvar5_list_coerced$) {
                                {
                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                    if (var_traversal14) {
                                        out.write("\r\n                <div class=\"with-cta-grid-item\">\r\n                  <div class=\"with-cta-event-card\">\r\n                    <div class=\"with-cta-event-wrapper\">");
                                        {
                                            String var_15 = (("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "description"), "html"))) + "\r\n                      ");
                                            out.write(renderContext.getObjectModel().toString(var_15));
                                        }
                                        {
                                            Object var_testvariable16 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneLabel");
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable16)) {
                                                out.write("\r\n                        <div class=\"with-cta-event-btn\">\r\n                          <div class=\"with-cta-event-nav\">\r\n                            <a");
                                                {
                                                    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneLinkURL");
                                                    {
                                                        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                            if (var_shoulddisplayattr20) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                                    if (!var_istrueattr19) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneTarget");
                                                    {
                                                        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                            if (var_shoulddisplayattr24) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                    if (!var_istrueattr23) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrvalue25 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneNoFollow")) ? "nofollow" : "");
                                                    {
                                                        Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                            if (var_shoulddisplayattr28) {
                                                                out.write(" rel");
                                                                {
                                                                    boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                    if (!var_istrueattr27) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent29 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneLabel"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                    out.write("\"");
                                                }
                                                {
                                                    Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneLinkURLType");
                                                    {
                                                        Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                            if (var_shoulddisplayattr33) {
                                                                out.write(" data-tracking-web-interaction-value");
                                                                {
                                                                    boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                    if (!var_istrueattr32) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">\r\n                              <span>");
                                                {
                                                    Object var_34 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneLabel"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_34));
                                                }
                                                out.write("</span>\r\n                               ");
                                                {
                                                    Object var_testvariable35 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIcon");
                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable35)) {
                                                        out.write("\r\n                              <div class=\"with-cta-event-svg\">\r\n                                 <picture>\r\n                  <source media=\"(max-width: 360px)\"");
                                                        {
                                                            Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIconMobileImagePath");
                                                            {
                                                                Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                                                    if (var_shoulddisplayattr39) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                                                            if (!var_istrueattr38) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(max-width: 576px)\"");
                                                        {
                                                            Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIconMobileImagePath");
                                                            {
                                                                Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                    if (var_shoulddisplayattr43) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                            if (!var_istrueattr42) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(min-width: 1080px)\"");
                                                        {
                                                            Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIconWebImagePath");
                                                            {
                                                                Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                                                    if (var_shoulddisplayattr47) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                                                            if (!var_istrueattr46) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(min-width: 1200px)\"");
                                                        {
                                                            Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIconWebImagePath");
                                                            {
                                                                Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                                                    if (var_shoulddisplayattr51) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                                                            if (!var_istrueattr50) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <img");
                                                        {
                                                            Object var_attrvalue52 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneIconWebImagePath");
                                                            {
                                                                Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                                                                    if (var_shoulddisplayattr55) {
                                                                        out.write(" src");
                                                                        {
                                                                            boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                                                            if (!var_istrueattr54) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            Object var_attrvalue56 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneAltText");
                                                            {
                                                                Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                                                                    if (var_shoulddisplayattr59) {
                                                                        out.write(" alt");
                                                                        {
                                                                            boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                                                                            if (!var_istrueattr58) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                 </picture>\r\n                              </div>\r\n                              ");
                                                    }
                                                }
                                                out.write("\r\n                            </a>\r\n                          </div>\r\n                        </div>\r\n                      ");
                                            }
                                        }
                                        out.write("\r\n                      ");
                                        {
                                            Object var_testvariable60 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoLabel");
                                            if (renderContext.getObjectModel().toBoolean(var_testvariable60)) {
                                                out.write("\r\n                        <div class=\"with-cta-event-btn\">\r\n                          <div class=\"with-cta-event-nav\">\r\n                            <a");
                                                {
                                                    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoLinkURL");
                                                    {
                                                        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                                                            if (var_shoulddisplayattr64) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                                                    if (!var_istrueattr63) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoTarget");
                                                    {
                                                        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                                                            if (var_shoulddisplayattr68) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                                                                    if (!var_istrueattr67) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrvalue69 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeOneNoFollow")) ? "nofollow" : "");
                                                    {
                                                        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
                                                            if (var_shoulddisplayattr72) {
                                                                out.write(" rel");
                                                                {
                                                                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                                                                    if (!var_istrueattr71) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent73 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoLabel"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent73));
                                                    out.write("\"");
                                                }
                                                {
                                                    Object var_attrvalue74 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoLinkURLType");
                                                    {
                                                        Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                                                            if (var_shoulddisplayattr77) {
                                                                out.write(" data-tracking-web-interaction-value");
                                                                {
                                                                    boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                                                                    if (!var_istrueattr76) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">\r\n                              <span>");
                                                {
                                                    Object var_78 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoLabel"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_78));
                                                }
                                                out.write("</span>\r\n                              ");
                                                {
                                                    Object var_testvariable79 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIcon");
                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable79)) {
                                                        out.write("\r\n                              <div class=\"with-cta-event-svg\">\r\n                                 <picture>\r\n                  <source media=\"(max-width: 360px)\"");
                                                        {
                                                            Object var_attrvalue80 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIconMobileImagePath");
                                                            {
                                                                Object var_attrcontent81 = renderContext.call("xss", var_attrvalue80, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr83 = (((null != var_attrcontent81) && (!"".equals(var_attrcontent81))) && ((!"".equals(var_attrvalue80)) && (!((Object)false).equals(var_attrvalue80))));
                                                                    if (var_shoulddisplayattr83) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr82 = (var_attrvalue80.equals(true));
                                                                            if (!var_istrueattr82) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent81));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(max-width: 576px)\"");
                                                        {
                                                            Object var_attrvalue84 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIconMobileImagePath");
                                                            {
                                                                Object var_attrcontent85 = renderContext.call("xss", var_attrvalue84, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr87 = (((null != var_attrcontent85) && (!"".equals(var_attrcontent85))) && ((!"".equals(var_attrvalue84)) && (!((Object)false).equals(var_attrvalue84))));
                                                                    if (var_shoulddisplayattr87) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr86 = (var_attrvalue84.equals(true));
                                                                            if (!var_istrueattr86) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent85));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(min-width: 1080px)\"");
                                                        {
                                                            Object var_attrvalue88 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIconWebImagePath");
                                                            {
                                                                Object var_attrcontent89 = renderContext.call("xss", var_attrvalue88, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr91 = (((null != var_attrcontent89) && (!"".equals(var_attrcontent89))) && ((!"".equals(var_attrvalue88)) && (!((Object)false).equals(var_attrvalue88))));
                                                                    if (var_shoulddisplayattr91) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr90 = (var_attrvalue88.equals(true));
                                                                            if (!var_istrueattr90) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <source media=\"(min-width: 1200px)\"");
                                                        {
                                                            Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIconWebImagePath");
                                                            {
                                                                Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                    if (var_shoulddisplayattr95) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                            if (!var_istrueattr94) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                  <img");
                                                        {
                                                            Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoIconWebImagePath");
                                                            {
                                                                Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                                    if (var_shoulddisplayattr99) {
                                                                        out.write(" src");
                                                                        {
                                                                            boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                                            if (!var_istrueattr98) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(navpanelsectionitem, "ctaTypeTwoAltText");
                                                            {
                                                                Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                                    if (var_shoulddisplayattr103) {
                                                                        out.write(" alt");
                                                                        {
                                                                            boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                            if (!var_istrueattr102) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                 </picture>\r\n                              </div>\r\n                              ");
                                                    }
                                                }
                                                out.write("\r\n                            </a>\r\n                          </div>\r\n                        </div>\r\n                      ");
                                            }
                                        }
                                        out.write("\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              ");
                                    }
                                }
                                var_index12++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

