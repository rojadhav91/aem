/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.devicelookstepscarousel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class devicelookstepscarousel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar6_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.DeviceLookStepsCarouselModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "numberOfCardTilesToDisplay");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div");
            {
                String var_attrcontent1 = ("device-look-carousel layout-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "layout"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            {
                Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_model, "numberOfCardTilesToDisplay");
                {
                    Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                    {
                        boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                        if (var_shoulddisplayattr5) {
                            out.write(" slides-to-show-num");
                            {
                                boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                if (!var_istrueattr4) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <div class=\"device-look-carousel__container\">\r\n                <div class=\"device-look-carousel-wrapper\">\r\n                    <div>\r\n                        <div class=\"device-look-carousel__list-item\">\r\n                            ");
            {
                Object var_collectionvar6 = renderContext.getObjectModel().resolveProperty(_global_model, "listStepItems");
                {
                    long var_size7 = ((var_collectionvar6_list_coerced$ == null ? (var_collectionvar6_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar6)) : var_collectionvar6_list_coerced$).size());
                    {
                        boolean var_notempty8 = (var_size7 > 0);
                        if (var_notempty8) {
                            {
                                long var_end11 = var_size7;
                                {
                                    boolean var_validstartstepend12 = (((0 < var_size7) && true) && (var_end11 > 0));
                                    if (var_validstartstepend12) {
                                        if (var_collectionvar6_list_coerced$ == null) {
                                            var_collectionvar6_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar6);
                                        }
                                        long var_index13 = 0;
                                        for (Object item : var_collectionvar6_list_coerced$) {
                                            {
                                                boolean var_traversal15 = (((var_index13 >= 0) && (var_index13 <= var_end11)) && true);
                                                if (var_traversal15) {
                                                    out.write("<div class=\"device-look-carousel__item\">\r\n                                ");
                                                    {
                                                        Object var_testvariable16 = ((((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "layout"), "device-image-step-text")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "layout"), "image-step-text"))) ? renderContext.getObjectModel().resolveProperty(_global_model, "isHaveStepImage") : ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "layout"), "device-image-step-text")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "layout"), "image-step-text")))));
                                                        if (renderContext.getObjectModel().toBoolean(var_testvariable16)) {
                                                            out.write("<div class=\"device-look-carousel__item-image-wrapper\">\r\n                                    ");
                                                            {
                                                                Object var_testvariable17 = renderContext.getObjectModel().resolveProperty(item, "image");
                                                                if (renderContext.getObjectModel().toBoolean(var_testvariable17)) {
                                                                    out.write("<picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                    {
                                                                        Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                                                if (var_shoulddisplayattr21) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                                        if (!var_istrueattr20) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                    {
                                                                        Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                        {
                                                                            Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                                                if (var_shoulddisplayattr25) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                                        if (!var_istrueattr24) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                    {
                                                                        Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                                                if (var_shoulddisplayattr29) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                                        if (!var_istrueattr28) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                    {
                                                                        Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                                                if (var_shoulddisplayattr33) {
                                                                                    out.write(" srcset");
                                                                                    {
                                                                                        boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                                        if (!var_istrueattr32) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                        <img");
                                                                    {
                                                                        Object var_attrvalue34 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                                        {
                                                                            Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
                                                                            {
                                                                                boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                                                                                if (var_shoulddisplayattr37) {
                                                                                    out.write(" alt");
                                                                                    {
                                                                                        boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                                                                                        if (!var_istrueattr36) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write(" class=\"device-look-carousel__item-image\"");
                                                                    {
                                                                        Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                        {
                                                                            Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "uri");
                                                                            {
                                                                                boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                                                if (var_shoulddisplayattr41) {
                                                                                    out.write(" src");
                                                                                    {
                                                                                        boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                                        if (!var_istrueattr40) {
                                                                                            out.write("=\"");
                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                                            out.write("\"");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    out.write("/>\r\n                                    </picture>");
                                                                }
                                                            }
                                                            out.write("\r\n                                    ");
                                                            {
                                                                boolean var_testvariable42 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "image")));
                                                                if (var_testvariable42) {
                                                                    out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/1x1.png\" class=\"device-look-carousel__item-blank-image\"/>");
                                                                }
                                                            }
                                                            out.write("\r\n                                </div>");
                                                        }
                                                    }
                                                    out.write("\r\n                                <div class=\"device-look-carousel__item-body\">\r\n                                    <p class=\"device-look-carousel__item-number\">");
                                                    {
                                                        Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "stepNumber"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_43));
                                                    }
                                                    out.write("</p>\r\n                                    <div class=\"device-look-carousel__item-content\">");
                                                    {
                                                        String var_44 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "description"), "html"))) + "\r\n                                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_44));
                                                    }
                                                    out.write("</div>\r\n                                </div>\r\n                            </div>\n");
                                                }
                                            }
                                            var_index13++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar6_list_coerced$ = null;
            }
            out.write("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar45 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions46_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions46 = obj().with("isEmpty", var_templateoptions46_field$_isempty);
            callUnit(out, renderContext, var_templatevar45, var_templateoptions46);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

