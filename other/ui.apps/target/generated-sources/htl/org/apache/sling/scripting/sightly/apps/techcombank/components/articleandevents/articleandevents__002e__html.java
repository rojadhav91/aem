/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.articleandevents;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class articleandevents__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_eventmodel = null;
Object _global_articleimagepath = null;
out.write("\r\n");
_global_eventmodel = renderContext.call("use", com.techcombank.core.models.ArticleAndEventsModel.class.getName(), obj());
out.write("\r\n<div itemscope itemtype=\"https://schema.org/NewsArticle\" class=\"press-article\">\r\n   ");
_global_articleimagepath = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath"), ""));
if (renderContext.getObjectModel().toBoolean(_global_articleimagepath)) {
    out.write("\r\n      <div class=\"banner-image\">\r\n\t<picture>\r\n\t    <source media=\"(max-width: 360px)\"");
    {
        Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath");
        {
            Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
            {
                boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
                if (var_shoulddisplayattr3) {
                    out.write(" srcset");
                    {
                        boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                        if (!var_istrueattr2) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n\t    <source media=\"(max-width: 576px)\"");
    {
        Object var_attrvalue4 = renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath");
        {
            Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
            {
                boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
                if (var_shoulddisplayattr7) {
                    out.write(" srcset");
                    {
                        boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                        if (!var_istrueattr6) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n\t    <source media=\"(min-width: 1080px)\"");
    {
        Object var_attrvalue8 = renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath");
        {
            Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
            {
                boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
                if (var_shoulddisplayattr11) {
                    out.write(" srcset");
                    {
                        boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                        if (!var_istrueattr10) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n\t    <source media=\"(min-width: 1200px)\"");
    {
        Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath");
        {
            Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
            {
                boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                if (var_shoulddisplayattr15) {
                    out.write(" srcset");
                    {
                        boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                        if (!var_istrueattr14) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n\t    <img itemprop=\"image\"");
    {
        Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleImagePath");
        {
            Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "uri");
            {
                boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                if (var_shoulddisplayattr19) {
                    out.write(" src");
                    {
                        boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                        if (!var_istrueattr18) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n\t</picture>\r\n      </div>\r\n   ");
}
out.write("\r\n   ");
{
    boolean var_testvariable20 = (!renderContext.getObjectModel().toBoolean(_global_articleimagepath));
    if (var_testvariable20) {
        out.write("\r\n      ");
        {
            Object var_resourcecontent21 = renderContext.call("includeResource", "mastheadsimplebanner", obj().with("resourceType", "techcombank/components/mastheadsimplebanner"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent21));
        }
        out.write("\r\n   ");
    }
}
out.write("\r\n   <section class=\"article-header__container\">\r\n       <div class=\"article-header__container--wrapper\">\r\n           <div class=\"article-header-content\">\r\n               <div class=\"article-header-body\">\r\n                   <h3 itemprop=\"headline\" class=\"article-header-body--title\">");
{
    String var_22 = (("\r\n                       " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleTitle"), "text"))) + "\r\n                   ");
    out.write(renderContext.getObjectModel().toString(var_22));
}
out.write("</h3>\r\n                   <p itemprop=\"description\" class=\"article-header-body--subTitle\">");
{
    String var_23 = (("\r\n                       " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleDescription"), "text"))) + "\r\n                   ");
    out.write(renderContext.getObjectModel().toString(var_23));
}
out.write("</p>\r\n                   <div itemprop=\"dateModified\" class=\"article-header-body--date\">");
{
    Object var_24 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_eventmodel, "articleCreationDate"), "text");
    out.write(renderContext.getObjectModel().toString(var_24));
}
out.write("</div>\r\n               </div>\r\n           </div>\r\n       </div>\r\n   </section>\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

