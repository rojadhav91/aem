/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.downloadcontentblock;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class downloadcontentblock__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_downloadcontentblockmodel = null;
Object _global_descriptionpositionabove = null;
_global_downloadcontentblockmodel = renderContext.call("use", com.techcombank.core.models.DownloadContentBlockModel.class.getName(), obj());
out.write("\r\n<section");
{
    String var_attrcontent0 = ((("tcb-teaser " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "imagePosition"), "right")) ? "tcb-teaser--reverse" : ""), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "imagePath")) ? "" : "tcb-teaser--mobile-large"), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent0));
    out.write("\"");
}
out.write(">\r\n    <div class=\"tcb-teaser_content tcb-teaser--larger-height\">\r\n        <div class=\"tcb-teaser_body\">\r\n            <div");
{
    String var_attrcontent1 = (("tcb-teaser_column tcb-teaser_column-left tcb-teaser_app-preview " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "imageAlignment"), "unsafe"))) + "-align small-image");
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent1));
    out.write("\"");
}
out.write(">\r\n\t\t");
{
    Object var_testvariable2 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "imagePath");
    if (renderContext.getObjectModel().toBoolean(var_testvariable2)) {
        out.write("\r\n\t\t<div class=\"app-preview-image\">\t\t   \r\n\t\t    <picture>\r\n\t\t       <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue3 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "mobileImagePath");
            {
                Object var_attrcontent4 = renderContext.call("xss", var_attrvalue3, "attribute");
                {
                    boolean var_shoulddisplayattr6 = (((null != var_attrcontent4) && (!"".equals(var_attrcontent4))) && ((!"".equals(var_attrvalue3)) && (!((Object)false).equals(var_attrvalue3))));
                    if (var_shoulddisplayattr6) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr5 = (var_attrvalue3.equals(true));
                            if (!var_istrueattr5) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent4));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t       <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "mobileImagePath");
            {
                Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                {
                    boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                    if (var_shoulddisplayattr10) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                            if (!var_istrueattr9) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t       <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "webImagePath");
            {
                Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                {
                    boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                    if (var_shoulddisplayattr14) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                            if (!var_istrueattr13) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t       <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "webImagePath");
            {
                Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                {
                    boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                    if (var_shoulddisplayattr18) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                            if (!var_istrueattr17) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n\t\t       <img");
        {
            Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "webImagePath");
            {
                Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "uri");
                {
                    boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                    if (var_shoulddisplayattr22) {
                        out.write(" src");
                        {
                            boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                            if (!var_istrueattr21) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "imageAltText");
            {
                Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                {
                    boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                    if (var_shoulddisplayattr26) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                            if (!var_istrueattr25) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/> \r\n\t\t    </picture>\t\t   \r\n\t        </div>\r\n\t     ");
    }
}
out.write("\r\n\t    </div>\r\n\r\n            <div");
{
    String var_attrcontent27 = ("tcb-teaser_column tcb-teaser_column-right " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "textAlignment"), "center")) ? "tcb-teaser_align-center" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent27));
    out.write("\"");
}
out.write(">\r\n\r\n                <h2 class=\"tcb-teaser_title\"");
{
    Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "id");
    {
        Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
        {
            boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
            if (var_shoulddisplayattr31) {
                out.write(" id");
                {
                    boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                    if (!var_istrueattr30) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "title"), "text");
    out.write(renderContext.getObjectModel().toString(var_32));
}
out.write("</h2>\r\n                ");
_global_descriptionpositionabove = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "descriptionPosition"), "above"));
if (renderContext.getObjectModel().toBoolean(_global_descriptionpositionabove)) {
    out.write("\r\n                    <div class=\"tcb-teaser_description\">\r\n                        <p>");
    {
        Object var_33 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "description"), "html");
        out.write(renderContext.getObjectModel().toString(var_33));
    }
    out.write("</p>\r\n                    </div>\r\n                ");
}
out.write("\r\n                <div class=\"tcb-teaser_download-app-row\">\r\n                    ");
{
    Object var_testvariable34 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "showScanner");
    if (renderContext.getObjectModel().toBoolean(var_testvariable34)) {
        out.write("\r\n                        <div class=\"tcb-download-app-block revert-on-mobile desktop-only\">\r\n                            <span class=\"tcb-scanner--text\">");
        {
            Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerText"), "text");
            out.write(renderContext.getObjectModel().toString(var_35));
        }
        out.write("</span>\r\n                              ");
        {
            Object var_testvariable36 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerImage");
            if (renderContext.getObjectModel().toBoolean(var_testvariable36)) {
                out.write("\r\n                                <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerMobileImagePath");
                    {
                        Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                        {
                            boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                            if (var_shoulddisplayattr40) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                    if (!var_istrueattr39) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerMobileImagePath");
                    {
                        Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                        {
                            boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                            if (var_shoulddisplayattr44) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                    if (!var_istrueattr43) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerWebImagePath");
                    {
                        Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                        {
                            boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                            if (var_shoulddisplayattr48) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                                    if (!var_istrueattr47) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerWebImagePath");
                    {
                        Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                        {
                            boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                            if (var_shoulddisplayattr52) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                    if (!var_istrueattr51) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n            \t\t\t    <img class=\"tcb-download-app-block_qr-code\"");
                {
                    Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerWebImagePath");
                    {
                        Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "uri");
                        {
                            boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                            if (var_shoulddisplayattr56) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                                    if (!var_istrueattr55) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                  </picture>\r\n                             ");
            }
        }
        out.write("                            \r\n                            <span class=\"app-button-arrow tcb-button--arrow material-symbols-outlined\">\r\n                                arrow_forward</span>\r\n                        </div>\r\n                        <a");
        {
            Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerLink");
            {
                Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "uri");
                {
                    boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                    if (var_shoulddisplayattr60) {
                        out.write(" href");
                        {
                            boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                            if (!var_istrueattr59) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"tcb-download-app-block revert-on-mobile mobile-only\" data-tracking-click-event=\"linkClick\"");
        {
            String var_attrcontent61 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerText"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent61));
            out.write("\"");
        }
        {
            Object var_attrvalue62 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerLinkWebInteractionValue");
            {
                Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
                {
                    boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                    if (var_shoulddisplayattr65) {
                        out.write(" data-tracking-web-interaction-value");
                        {
                            boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                            if (!var_istrueattr64) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            <span class=\"tcb-scanner--text\">");
        {
            Object var_66 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "scannerText"), "text");
            out.write(renderContext.getObjectModel().toString(var_66));
        }
        out.write("</span>\r\n                            <span class=\"app-button-arrow tcb-button--arrow material-symbols-outlined\">\r\n                                arrow_forward</span>\r\n                        </a>\r\n                    ");
    }
}
out.write("\r\n                </div>\r\n                ");
{
    boolean var_testvariable67 = (!renderContext.getObjectModel().toBoolean(_global_descriptionpositionabove));
    if (var_testvariable67) {
        out.write("\r\n                    <div class=\"tcb-teaser_description\">\r\n                        <p>");
        {
            Object var_68 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "description"), "html");
            out.write(renderContext.getObjectModel().toString(var_68));
        }
        out.write("</p>\r\n                    </div>\r\n                ");
    }
}
out.write("\r\n\t         ");
{
    Object var_testvariable69 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkText")) ? renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkUrl") : renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkText")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable69)) {
        out.write("\r\n\t             <a class=\"tcb-button tcb-button--link\"");
        {
            Object var_attrvalue70 = renderContext.call("uriManipulation", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkUrl"), obj().with("extension", "html"));
            {
                Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "uri");
                {
                    boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                    if (var_shoulddisplayattr73) {
                        out.write(" href");
                        {
                            boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                            if (!var_istrueattr72) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue74 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "openInNewTabLink");
            {
                Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "attribute");
                {
                    boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                    if (var_shoulddisplayattr77) {
                        out.write(" target");
                        {
                            boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                            if (!var_istrueattr76) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue78 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "noFollowLink");
            {
                Object var_attrcontent79 = renderContext.call("xss", var_attrvalue78, "attribute");
                {
                    boolean var_shoulddisplayattr81 = (((null != var_attrcontent79) && (!"".equals(var_attrcontent79))) && ((!"".equals(var_attrvalue78)) && (!((Object)false).equals(var_attrvalue78))));
                    if (var_shoulddisplayattr81) {
                        out.write(" rel");
                        {
                            boolean var_istrueattr80 = (var_attrvalue78.equals(true));
                            if (!var_istrueattr80) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent79));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-tracking-click-event=\"linkClick\"");
        {
            String var_attrcontent82 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkText"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent82));
            out.write("\"");
        }
        {
            Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkUrlWebInteractionValue");
            {
                Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
                {
                    boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                    if (var_shoulddisplayattr86) {
                        out.write(" data-tracking-web-interaction-value");
                        {
                            boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                            if (!var_istrueattr85) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                         <div class=\"tcb-button--title\">");
        {
            Object var_87 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_downloadcontentblockmodel, "linkText"), "text");
            out.write(renderContext.getObjectModel().toString(var_87));
        }
        out.write("</div>\r\n                         <span class=\"tcb-button--arrow material-symbols-outlined\">arrow_forward</span>\r\n                     </a>\r\n               ");
    }
}
out.write("\r\n                ");
{
    Object var_resourcecontent88 = renderContext.call("includeResource", "button", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/button"));
    out.write(renderContext.getObjectModel().toString(var_resourcecontent88));
}
out.write("\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

