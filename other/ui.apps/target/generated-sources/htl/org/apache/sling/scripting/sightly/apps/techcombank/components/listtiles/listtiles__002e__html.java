/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.listtiles;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class listtiles__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_listtiles = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar2_list_coerced$ = null;
_global_listtiles = renderContext.call("use", com.techcombank.core.models.ListTilesModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_listtiles, "displayLayout");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div");
            {
                String var_attrcontent1 = ("list__items list-tile " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_listtiles, "containerClasses"), renderContext.getObjectModel().resolveProperty(_global_listtiles, "displayLayout")), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n            ");
            {
                Object var_collectionvar2 = renderContext.getObjectModel().resolveProperty(_global_listtiles, "listTilesItems");
                {
                    long var_size3 = ((var_collectionvar2_list_coerced$ == null ? (var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2)) : var_collectionvar2_list_coerced$).size());
                    {
                        boolean var_notempty4 = (var_size3 > 0);
                        if (var_notempty4) {
                            {
                                long var_end7 = var_size3;
                                {
                                    boolean var_validstartstepend8 = (((0 < var_size3) && true) && (var_end7 > 0));
                                    if (var_validstartstepend8) {
                                        if (var_collectionvar2_list_coerced$ == null) {
                                            var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2);
                                        }
                                        long var_index9 = 0;
                                        for (Object item : var_collectionvar2_list_coerced$) {
                                            {
                                                long itemlist_field$_index = var_index9;
                                                {
                                                    boolean var_traversal11 = (((var_index9 >= 0) && (var_index9 <= var_end7)) && true);
                                                    if (var_traversal11) {
                                                        out.write("<div");
                                                        {
                                                            String var_attrcontent12 = ((("list__item list-tile__tile-item " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_listtiles, "tileClasses"), renderContext.getObjectModel().resolveProperty(_global_listtiles, "displayLayout")), itemlist_field$_index), "attribute"))) + " list-tile__tile-item__theme-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "theme"), "attribute")));
                                                            out.write(" class=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n                <a class=\"list-tile__tile-link\"");
                                                        {
                                                            Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                            {
                                                                Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                                                                    if (var_shoulddisplayattr16) {
                                                                        out.write(" href");
                                                                        {
                                                                            boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                                                                            if (!var_istrueattr15) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue17 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "_self");
                                                            {
                                                                Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                                    if (var_shoulddisplayattr20) {
                                                                        out.write(" target");
                                                                        {
                                                                            boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                                            if (!var_istrueattr19) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue21 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                            {
                                                                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                    if (var_shoulddisplayattr24) {
                                                                        out.write(" rel");
                                                                        {
                                                                            boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                            if (!var_istrueattr23) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(">\r\n                    <article class=\"list-tile__hero\">\r\n                        <picture>\r\n                            <source media=\"(max-width: 360px)\"");
                                                        {
                                                            Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(item, "mobileBackgroundImagePath");
                                                            {
                                                                Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                    if (var_shoulddisplayattr28) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                            if (!var_istrueattr27) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
                                                        {
                                                            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(item, "mobileBackgroundImagePath");
                                                            {
                                                                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                    if (var_shoulddisplayattr32) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                            if (!var_istrueattr31) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
                                                        {
                                                            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(item, "webBackgroundImagePath");
                                                            {
                                                                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                                    if (var_shoulddisplayattr36) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                                            if (!var_istrueattr35) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
                                                        {
                                                            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(item, "webBackgroundImagePath");
                                                            {
                                                                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                                    if (var_shoulddisplayattr40) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                                            if (!var_istrueattr39) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                            <img alt=\"image\" class=\"list-tile__img\"");
                                                        {
                                                            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(item, "webBackgroundImagePath");
                                                            {
                                                                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                                                                    if (var_shoulddisplayattr44) {
                                                                        out.write(" src");
                                                                        {
                                                                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                                                            if (!var_istrueattr43) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                        </picture>\r\n                        <div class=\"list-tile__content\">\r\n                            <div class=\"list-tile__content-inner\">\r\n                                ");
                                                        {
                                                            Object var_testvariable45 = renderContext.getObjectModel().resolveProperty(item, "nudgeText");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable45)) {
                                                                out.write("<div class=\"list-tile__card-label\">\r\n                                    <p>");
                                                                {
                                                                    Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "nudgeText"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_46));
                                                                }
                                                                out.write("</p>\r\n                                </div>");
                                                            }
                                                        }
                                                        out.write("\r\n                                <div class=\"list-tile__card-body\">\r\n                                    <div class=\"list-tile__card-content\">\r\n                                        <h3>");
                                                        {
                                                            Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "textTitle"), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_47));
                                                        }
                                                        out.write("</h3>\r\n                                        ");
                                                        {
                                                            Object var_testvariable48 = renderContext.getObjectModel().resolveProperty(item, "description");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
                                                                out.write("<div");
                                                                {
                                                                    String var_attrcontent49 = ("list-tile__card-description " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkUrl")) ? (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText"))) : renderContext.getObjectModel().resolveProperty(item, "linkUrl")))) ? "have-arrow-sticky" : ""), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">");
                                                                {
                                                                    String var_50 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "description"), "html"))) + "\r\n                                        ");
                                                                    out.write(renderContext.getObjectModel().toString(var_50));
                                                                }
                                                                out.write("</div>");
                                                            }
                                                        }
                                                        out.write("\r\n                                        ");
                                                        {
                                                            Object var_testvariable51 = renderContext.getObjectModel().resolveProperty(item, "linkText");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable51)) {
                                                                out.write("<div class=\"list-tile__card-navtext\">\r\n                                            <span class=\"list-tile__nav-text\">");
                                                                {
                                                                    Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkText"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_52));
                                                                }
                                                                out.write("</span>\r\n                                            ");
                                                                {
                                                                    Object var_testvariable53 = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable53)) {
                                                                        out.write("<span class=\"list-tile__nav-icon\">\r\n                                                <span class=\"list-tile__nav-inner\">\r\n                                                    <span class=\"list-tile__card-img\"></span>\r\n                                                </span>\r\n                                            </span>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                            </span>\r\n                                        </div>");
                                                            }
                                                        }
                                                        out.write("\r\n                                        ");
                                                        {
                                                            Object var_testvariable54 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkUrl")) ? (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText"))) : renderContext.getObjectModel().resolveProperty(item, "linkUrl")));
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable54)) {
                                                                out.write("<span class=\"list-tile__nav-icon right-corner\">\r\n                                            <span class=\"list-tile__nav-inner\">\r\n                                                <span class=\"list-tile__card-img\"></span>\r\n                                            </span>\r\n                                        </span>");
                                                            }
                                                        }
                                                        out.write("\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </article>\r\n                </a>\r\n            </div>\n");
                                                    }
                                                }
                                            }
                                            var_index9++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar2_list_coerced$ = null;
            }
            out.write("\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar55 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions56_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions56 = obj().with("isEmpty", var_templateoptions56_field$_isempty);
            callUnit(out, renderContext, var_templatevar55, var_templateoptions56);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

