/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.creditrating;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class creditrating__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _global_creditrating = null;
Object _global_hascontent = null;
Collection var_collectionvar9_list_coerced$ = null;
Collection var_collectionvar24_list_coerced$ = null;
Collection var_collectionvar34_list_coerced$ = null;
Collection var_collectionvar49_list_coerced$ = null;
Collection var_collectionvar89_list_coerced$ = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_creditrating = renderContext.call("use", com.techcombank.core.models.CreditRatingModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_creditrating, "creditRatingReportList");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"credit-ranking-component\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_creditrating, "getLanguageTitle");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" lang");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <div class=\"select-filter\">\r\n                <div>");
            {
                Object var_resourcecontent5 = renderContext.call("includeResource", "title", obj().with("resourceType", "core/wcm/components/title/v3/title"));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent5));
            }
            out.write("</div>\r\n                <div class=\"select-options\">\r\n                  <div class=\"select\">\r\n                    <div class=\"material-symbols-outlined\">calendar_today</div>\r\n                    <h6>");
            {
                Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_creditrating, "filterYearLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_6));
            }
            out.write("</h6>\r\n                    <span>");
            {
                Object var_7 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_creditrating, "filterSelectLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_7));
            }
            out.write("</span>\r\n                    <img class=\"credit-rating-chevron-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                  </div>\r\n                  <div class=\"options\">\r\n                    <div>\r\n                      <ul class=\"filter-options\">\r\n                        <li class=\"selected option\" value=\"select-all\">");
            {
                Object var_8 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_creditrating, "filterAllLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_8));
            }
            out.write("</li>\r\n                        ");
            {
                Object var_collectionvar9 = renderContext.getObjectModel().resolveProperty(_global_creditrating, "yearForFilterList");
                {
                    long var_size10 = ((var_collectionvar9_list_coerced$ == null ? (var_collectionvar9_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar9)) : var_collectionvar9_list_coerced$).size());
                    {
                        boolean var_notempty11 = (var_size10 > 0);
                        if (var_notempty11) {
                            {
                                long var_end14 = var_size10;
                                {
                                    boolean var_validstartstepend15 = (((0 < var_size10) && true) && (var_end14 > 0));
                                    if (var_validstartstepend15) {
                                        if (var_collectionvar9_list_coerced$ == null) {
                                            var_collectionvar9_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar9);
                                        }
                                        long var_index16 = 0;
                                        for (Object yearforfilter : var_collectionvar9_list_coerced$) {
                                            {
                                                boolean var_traversal18 = (((var_index16 >= 0) && (var_index16 <= var_end14)) && true);
                                                if (var_traversal18) {
                                                    out.write("<li class=\"option\"");
                                                    {
                                                        Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(yearforfilter, "yearForFilter");
                                                        {
                                                            Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                                if (var_shoulddisplayattr22) {
                                                                    out.write(" value");
                                                                    {
                                                                        boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                                        if (!var_istrueattr21) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        String var_23 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(yearforfilter, "yearForFilter"), "text"))) + "\r\n                        ");
                                                        out.write(renderContext.getObjectModel().toString(var_23));
                                                    }
                                                    out.write("</li>\n");
                                                }
                                            }
                                            var_index16++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar9_list_coerced$ = null;
            }
            out.write("\r\n                      </ul>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            <div class=\"credits-list\">\r\n                ");
            {
                Object var_collectionvar24 = renderContext.getObjectModel().resolveProperty(_global_creditrating, "creditRatingReportList");
                {
                    long var_size25 = ((var_collectionvar24_list_coerced$ == null ? (var_collectionvar24_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar24)) : var_collectionvar24_list_coerced$).size());
                    {
                        boolean var_notempty26 = (var_size25 > 0);
                        if (var_notempty26) {
                            {
                                long var_end29 = var_size25;
                                {
                                    boolean var_validstartstepend30 = (((0 < var_size25) && true) && (var_end29 > 0));
                                    if (var_validstartstepend30) {
                                        if (var_collectionvar24_list_coerced$ == null) {
                                            var_collectionvar24_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar24);
                                        }
                                        long var_index31 = 0;
                                        for (Object creditratingreport : var_collectionvar24_list_coerced$) {
                                            {
                                                boolean var_traversal33 = (((var_index31 >= 0) && (var_index31 <= var_end29)) && true);
                                                if (var_traversal33) {
                                                    out.write("<div class=\"credit\">\r\n                    <div class=\"menu-container\">\r\n                        <div class=\"menu\">\r\n                            ");
                                                    {
                                                        Object var_collectionvar34 = renderContext.getObjectModel().resolveProperty(creditratingreport, "creditRatingTabList");
                                                        {
                                                            long var_size35 = ((var_collectionvar34_list_coerced$ == null ? (var_collectionvar34_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar34)) : var_collectionvar34_list_coerced$).size());
                                                            {
                                                                boolean var_notempty36 = (var_size35 > 0);
                                                                if (var_notempty36) {
                                                                    {
                                                                        long var_end39 = var_size35;
                                                                        {
                                                                            boolean var_validstartstepend40 = (((0 < var_size35) && true) && (var_end39 > 0));
                                                                            if (var_validstartstepend40) {
                                                                                if (var_collectionvar34_list_coerced$ == null) {
                                                                                    var_collectionvar34_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar34);
                                                                                }
                                                                                long var_index41 = 0;
                                                                                for (Object creditratingtab : var_collectionvar34_list_coerced$) {
                                                                                    {
                                                                                        boolean var_traversal43 = (((var_index41 >= 0) && (var_index41 <= var_end39)) && true);
                                                                                        if (var_traversal43) {
                                                                                            out.write("<button class=\"menu-item\"");
                                                                                            {
                                                                                                Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(creditratingtab, "formattedDate");
                                                                                                {
                                                                                                    Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                                                                                        if (var_shoulddisplayattr47) {
                                                                                                            out.write(" tabLabel");
                                                                                                            {
                                                                                                                boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                                                                                                if (!var_istrueattr46) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(">\r\n                                <span class=\"menu-item-tab-label\">");
                                                                                            {
                                                                                                Object var_48 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(creditratingtab, "formattedDate"), "text");
                                                                                                out.write(renderContext.getObjectModel().toString(var_48));
                                                                                            }
                                                                                            out.write("</span>\r\n                            </button>\n");
                                                                                        }
                                                                                    }
                                                                                    var_index41++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar34_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"list-items\">\r\n                        ");
                                                    {
                                                        Object var_collectionvar49 = renderContext.getObjectModel().resolveProperty(creditratingreport, "creditRatingTabList");
                                                        {
                                                            long var_size50 = ((var_collectionvar49_list_coerced$ == null ? (var_collectionvar49_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar49)) : var_collectionvar49_list_coerced$).size());
                                                            {
                                                                boolean var_notempty51 = (var_size50 > 0);
                                                                if (var_notempty51) {
                                                                    {
                                                                        long var_end54 = var_size50;
                                                                        {
                                                                            boolean var_validstartstepend55 = (((0 < var_size50) && true) && (var_end54 > 0));
                                                                            if (var_validstartstepend55) {
                                                                                if (var_collectionvar49_list_coerced$ == null) {
                                                                                    var_collectionvar49_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar49);
                                                                                }
                                                                                long var_index56 = 0;
                                                                                for (Object creditratingtab : var_collectionvar49_list_coerced$) {
                                                                                    {
                                                                                        boolean var_traversal58 = (((var_index56 >= 0) && (var_index56 <= var_end54)) && true);
                                                                                        if (var_traversal58) {
                                                                                            out.write("<div class=\"tab\"");
                                                                                            {
                                                                                                Object var_attrvalue59 = renderContext.getObjectModel().resolveProperty(creditratingtab, "formattedDate");
                                                                                                {
                                                                                                    Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
                                                                                                        if (var_shoulddisplayattr62) {
                                                                                                            out.write(" tabLabel");
                                                                                                            {
                                                                                                                boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                                                                                                                if (!var_istrueattr61) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(">\r\n                            <div class=\"content white-bg\">\r\n                                <div class=\"banner\">\r\n                                <div class=\"image\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                                                            {
                                                                                                Object var_attrvalue63 = renderContext.getObjectModel().resolveProperty(creditratingreport, "mobileCreditRatingLogoImagePath");
                                                                                                {
                                                                                                    Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                                                                                                        if (var_shoulddisplayattr66) {
                                                                                                            out.write(" srcset");
                                                                                                            {
                                                                                                                boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                                                                                                                if (!var_istrueattr65) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                                                            {
                                                                                                Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(creditratingreport, "mobileCreditRatingLogoImagePath");
                                                                                                {
                                                                                                    Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                                                                                                        if (var_shoulddisplayattr70) {
                                                                                                            out.write(" srcset");
                                                                                                            {
                                                                                                                boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                                                                                                if (!var_istrueattr69) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                                                            {
                                                                                                Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(creditratingreport, "webCreditRatingLogoImagePath");
                                                                                                {
                                                                                                    Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                                                                                                        if (var_shoulddisplayattr74) {
                                                                                                            out.write(" srcset");
                                                                                                            {
                                                                                                                boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                                                                                                if (!var_istrueattr73) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                                                            {
                                                                                                Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(creditratingreport, "webCreditRatingLogoImagePath");
                                                                                                {
                                                                                                    Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                                                                                                        if (var_shoulddisplayattr78) {
                                                                                                            out.write(" srcset");
                                                                                                            {
                                                                                                                boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                                                                                                                if (!var_istrueattr77) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write("/>\r\n                                        <img");
                                                                                            {
                                                                                                Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(creditratingreport, "logoImageAltText");
                                                                                                {
                                                                                                    Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
                                                                                                        if (var_shoulddisplayattr82) {
                                                                                                            out.write(" alt");
                                                                                                            {
                                                                                                                boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                                                                                                                if (!var_istrueattr81) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            {
                                                                                                Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(creditratingreport, "webCreditRatingLogoImagePath");
                                                                                                {
                                                                                                    Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "uri");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                                                                                                        if (var_shoulddisplayattr86) {
                                                                                                            out.write(" src");
                                                                                                            {
                                                                                                                boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                                                                                                                if (!var_istrueattr85) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write("/>\r\n                                    </picture>\r\n                                </div>\r\n                                </div>\r\n                                <div class=\"table\">\r\n                                <table>\r\n                                    <thead>\r\n                                    <tr>\r\n                                        <th>");
                                                                                            {
                                                                                                Object var_87 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(creditratingtab, "categoryHeader"), "text");
                                                                                                out.write(renderContext.getObjectModel().toString(var_87));
                                                                                            }
                                                                                            out.write("</th>\r\n                                        <th>");
                                                                                            {
                                                                                                Object var_88 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(creditratingtab, "ratingHeader"), "text");
                                                                                                out.write(renderContext.getObjectModel().toString(var_88));
                                                                                            }
                                                                                            out.write("</th>\r\n                                    </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                    ");
                                                                                            {
                                                                                                Object var_collectionvar89 = renderContext.getObjectModel().resolveProperty(creditratingtab, "creditRatingDataList");
                                                                                                {
                                                                                                    long var_size90 = ((var_collectionvar89_list_coerced$ == null ? (var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89)) : var_collectionvar89_list_coerced$).size());
                                                                                                    {
                                                                                                        boolean var_notempty91 = (var_size90 > 0);
                                                                                                        if (var_notempty91) {
                                                                                                            {
                                                                                                                long var_end94 = var_size90;
                                                                                                                {
                                                                                                                    boolean var_validstartstepend95 = (((0 < var_size90) && true) && (var_end94 > 0));
                                                                                                                    if (var_validstartstepend95) {
                                                                                                                        if (var_collectionvar89_list_coerced$ == null) {
                                                                                                                            var_collectionvar89_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar89);
                                                                                                                        }
                                                                                                                        long var_index96 = 0;
                                                                                                                        for (Object creditratingdata : var_collectionvar89_list_coerced$) {
                                                                                                                            {
                                                                                                                                boolean var_traversal98 = (((var_index96 >= 0) && (var_index96 <= var_end94)) && true);
                                                                                                                                if (var_traversal98) {
                                                                                                                                    out.write("<tr>\r\n                                        <td>");
                                                                                                                                    {
                                                                                                                                        Object var_99 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(creditratingdata, "categoryData"), "text");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_99));
                                                                                                                                    }
                                                                                                                                    out.write("</td>\r\n                                        <td>");
                                                                                                                                    {
                                                                                                                                        Object var_100 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(creditratingdata, "ratingData"), "text");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_100));
                                                                                                                                    }
                                                                                                                                    out.write("</td>\r\n                                    </tr>\n");
                                                                                                                                }
                                                                                                                            }
                                                                                                                            var_index96++;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                var_collectionvar89_list_coerced$ = null;
                                                                                            }
                                                                                            out.write("\r\n                                    </tbody>\r\n                                </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\n");
                                                                                        }
                                                                                    }
                                                                                    var_index56++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar49_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n                    </div>\r\n                </div>\n");
                                                }
                                            }
                                            var_index31++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar24_list_coerced$ = null;
            }
            out.write("\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar101 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions102_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions102 = obj().with("isEmpty", var_templateoptions102_field$_isempty);
            callUnit(out, renderContext, var_templatevar101, var_templateoptions102);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

