/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.filterpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class filterpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_filterpanel = null;
Collection var_collectionvar7_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_filterpanel = renderContext.call("use", com.techcombank.core.models.FilterPanel.class.getName(), obj());
out.write("\r\n    <div class=\"filter-panel\">\r\n        <div class=\"filter-panel__content\">\r\n            <div class=\"content-wrapper filter-panel__container\">\r\n                <h6 class=\"filter-panel__title\">");
{
    Object var_5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_filterpanel, "categoryLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_5));
}
out.write("</h6>\r\n                <div class=\"filter-panel__items\">\r\n                    <div class=\"filter__item\">\r\n                        <button type=\"button\" class=\"filter-panel__button filter-selected\" data-id=\"all\">");
{
    String var_6 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_filterpanel, "viewAll"), "text"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_6));
}
out.write("</button>\r\n                    </div>\r\n                    ");
{
    Object var_collectionvar7 = renderContext.getObjectModel().resolveProperty(_global_filterpanel, "tags");
    {
        long var_size8 = ((var_collectionvar7_list_coerced$ == null ? (var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7)) : var_collectionvar7_list_coerced$).size());
        {
            boolean var_notempty9 = (var_size8 > 0);
            if (var_notempty9) {
                {
                    long var_end12 = var_size8;
                    {
                        boolean var_validstartstepend13 = (((0 < var_size8) && true) && (var_end12 > 0));
                        if (var_validstartstepend13) {
                            if (var_collectionvar7_list_coerced$ == null) {
                                var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7);
                            }
                            long var_index14 = 0;
                            for (Object item : var_collectionvar7_list_coerced$) {
                                {
                                    boolean var_traversal16 = (((var_index14 >= 0) && (var_index14 <= var_end12)) && true);
                                    if (var_traversal16) {
                                        out.write("\r\n                        <div class=\"filter__item\">\r\n                            <button type=\"button\" class=\"filter-panel__button\"");
                                        {
                                            Object var_attrvalue17 = item;
                                            {
                                                Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                    if (var_shoulddisplayattr20) {
                                                        out.write(" data-id");
                                                        {
                                                            boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                            if (!var_istrueattr19) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">");
                                        {
                                            String var_21 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", item, "text"))) + "\r\n                            ");
                                            out.write(renderContext.getObjectModel().toString(var_21));
                                        }
                                        out.write("</button>\r\n                        </div>\r\n                    ");
                                    }
                                }
                                var_index14++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar7_list_coerced$ = null;
}
out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n        ");
{
    Object var_resourcecontent22 = renderContext.call("includeResource", null, obj().with("path", "filterpanelparsys").with("resourceType", "foundation/components/parsys"));
    out.write(renderContext.getObjectModel().toString(var_resourcecontent22));
}
out.write("\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

