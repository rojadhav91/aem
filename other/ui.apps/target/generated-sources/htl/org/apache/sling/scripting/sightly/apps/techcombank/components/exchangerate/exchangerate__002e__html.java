/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.exchangerate;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class exchangerate__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_exchangeratemodel = null;
Object _global_page = null;
Object _dynamic_currentpage = bindings.get("currentpage");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_exchangeratemodel = renderContext.call("use", com.techcombank.core.models.ExchangeRateModel.class.getName(), obj());
_global_page = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Page.class.getName(), obj());
out.write("\r\n    <section class=\"container exchange-rate width-25-75\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-url");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n        <div class=\"exchange-rate__header\">\r\n            <div class=\"title-cmp\">\r\n                <div class=\"title-cmp__title\">");
{
    Object var_9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "exchangeRateTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_9));
}
out.write("</div>\r\n            </div>\r\n            <div class=\"exchange-rate__header-select analytics-active-link\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent10 = (("{'calculatorName':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "', 'calculatorField':'TimeStamp' }");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
    out.write("\"");
}
out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Calculators','type': 'other'}}\">\r\n                <div class=\"header-select__data date-time-wrapper__input-extra\">\r\n                    <div class=\"header-select__data-input calendar_real_estate\">\r\n                        <div class=\"data-input__prefix\">\r\n                            <div class=\"data-input__calendar-icon\">\r\n                                <picture>\r\n                                    <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
        {
            boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
            if (var_shoulddisplayattr14) {
                out.write(" srcset");
                {
                    boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                    if (!var_istrueattr13) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
        {
            boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
            if (var_shoulddisplayattr18) {
                out.write(" srcset");
                {
                    boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                    if (!var_istrueattr17) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
        {
            boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
            if (var_shoulddisplayattr22) {
                out.write(" srcset");
                {
                    boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                    if (!var_istrueattr21) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
        {
            boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
            if (var_shoulddisplayattr26) {
                out.write(" srcset");
                {
                    boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                    if (!var_istrueattr25) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    <img");
{
    Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconAlt");
    {
        Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
        {
            boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
            if (var_shoulddisplayattr30) {
                out.write(" alt");
                {
                    boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                    if (!var_istrueattr29) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "uri");
        {
            boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
            if (var_shoulddisplayattr34) {
                out.write(" src");
                {
                    boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                    if (!var_istrueattr33) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                </picture>\r\n                            </div>\r\n                            <p>");
{
    Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_35));
}
out.write("</p>\r\n                        </div>\r\n                        <div class=\"data-input__suffix\">\r\n                            <p class=\"calendar__input-field\"></p>\r\n                            <span class=\"data-input__arrow-icon\">\r\n                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\"/>\r\n              </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"calendar-popup\">\r\n                        <div class=\"month-line\">\r\n                            <span class=\"month-data\"></span>\r\n                            <div>\r\n                                <img class=\"prev-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-left-icon.svg\"/>\r\n                                <img class=\"next-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-right-icon.svg\"/>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"date-line\">\r\n                        </div>\r\n                        <div class=\"day-tbl\">\r\n                            <div class=\"week line1\"></div>\r\n                            <div class=\"week line2\"></div>\r\n                            <div class=\"week line3\"></div>\r\n                            <div class=\"week line4\"></div>\r\n                            <div class=\"week line5\"></div>\r\n                            <div class=\"week line6\"></div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"header-select__data time-select\"");
{
    Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconMobileImagePath");
    {
        Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
        {
            boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
            if (var_shoulddisplayattr39) {
                out.write(" icon-mobile-path");
                {
                    boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                    if (!var_istrueattr38) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconWebImagePath");
    {
        Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
        {
            boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
            if (var_shoulddisplayattr43) {
                out.write(" icon-web-path");
                {
                    boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                    if (!var_istrueattr42) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconAlt");
    {
        Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
        {
            boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
            if (var_shoulddisplayattr47) {
                out.write(" icon-alt");
                {
                    boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                    if (!var_istrueattr46) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeLabel");
    {
        Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
        {
            boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
            if (var_shoulddisplayattr51) {
                out.write(" title");
                {
                    boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                    if (!var_istrueattr50) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <p class=\"exchange-rate__empty-label\">");
{
    Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "errorMessage"), "text");
    out.write(renderContext.getObjectModel().toString(var_52));
}
out.write("</p>\r\n        <div class=\"table-content-container hidden\">\r\n            <div class=\"exchange-rate__table\">\r\n                <div class=\"exchange-rate__table-outer\">\r\n                    <div class=\"exchange-rate__table-content\">\r\n                        <div class=\"exchange-rate__table-records table-header\">\r\n                            <div class=\"table__first-column up-par first-row first-column\">\r\n                                <p>");
{
    Object var_53 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "currencyLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_53));
}
out.write("</p>\r\n                            </div>\r\n                            <div class=\"table-records__data\">\r\n                                <div class=\"group-header\">\r\n                                    <div class=\"table-records__data-content up-par first-row\">\r\n                                        <p>");
{
    Object var_54 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "purchaseLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_54));
}
out.write("</p>\r\n                                    </div>\r\n                                    <div class=\"table-records__data-content row-divided first-row\">\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_55 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "cashLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_55));
}
out.write("</p>\r\n                                        </div>\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_56 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "transferLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_56));
}
out.write("</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"group-header\">\r\n                                    <div class=\"table-records__data-content up-par first-row last-column\">\r\n                                        <p>");
{
    Object var_57 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "sellLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_57));
}
out.write("</p>\r\n                                    </div>\r\n                                    <div class=\"table-records__data-content row-divided first-row last-column\">\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_58 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "cashLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_58));
}
out.write("</p>\r\n                                        </div>\r\n                                        <div class=\"data-content__item last-column\">\r\n                                            <p>");
{
    Object var_59 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "transferLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_59));
}
out.write("</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate-table-content\">\r\n                        </div>\r\n                        <!-- Table note italic -->\r\n                        <div class=\"table-note\">\r\n                            <div class=\"exchange-rate__table-note\" note=\"<p>T\u1EF7 gi\u00E1 \u0111\u01B0\u1EE3c c\u1EADp nh\u1EADt l\u00FAc %updatedDate %updatedTime v\u00E0 ch\u1EC9 mang t\u00EDnh ch\u1EA5t tham kh\u1EA3o</p><p>Ngu\u1ED3n t\u1EEB Bloomberg</p>\">\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- Fixing Rate view more button -->\r\n            <div class=\"content-wrapper exchange-rate__view-more\">\r\n                <!-- hidden -->\r\n                <button type=\"button\" class=\"title button__link\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent60 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "seeMoreLabel"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent60));
    out.write("\"");
}
{
    String var_attrcontent61 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent61));
    out.write("\"");
}
out.write(">\r\n                    <span>");
{
    Object var_62 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "seeMoreLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_62));
}
out.write("</span>\r\n                    <div class=\"view-more__icon\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\"/>\r\n                    </div>\r\n                </button>\r\n            </div>\r\n        </div>\r\n        <!-- Fixing Rate view more popup -->\r\n        <div class=\"exchange-rate__popup\" style=\"opacity: 0; transform: scale(0.75, 0.5625); transition: opacity 461ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, transform 307ms cubic-bezier(0.4, 0, 0.2, 1) 154ms; visibility: hidden;\">\r\n            <div class=\"content-wrapper exchange-rate__popup-wrapper\">\r\n                <div class=\"exchange-rate__header analytics-active-link\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent63 = (("{'calculatorName':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "', 'calculatorField':'TimeStamp' }");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent63));
    out.write("\"");
}
out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Calculators','type': 'other'}}\">\r\n                    <div class=\"title-cmp\">\r\n                        <div class=\"title-cmp__title\">");
{
    Object var_64 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "exchangeRateTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_64));
}
out.write("</div>\r\n                    </div>\r\n                    <div class=\"exchange-rate__header-select\">\r\n                        <div class=\"header-select__data date-time-wrapper__input-extra\">\r\n                            <div class=\"header-select__data-input calendar_real_estate\">\r\n                                <div class=\"data-input__prefix\">\r\n                                    <div class=\"data-input__calendar-icon\">\r\n                                        <picture>\r\n                                            <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
        {
            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
            if (var_shoulddisplayattr68) {
                out.write(" srcset");
                {
                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                    if (!var_istrueattr67) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconMobileImagePath");
    {
        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
        {
            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
            if (var_shoulddisplayattr72) {
                out.write(" srcset");
                {
                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                    if (!var_istrueattr71) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue73 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent74 = renderContext.call("xss", var_attrvalue73, "attribute");
        {
            boolean var_shoulddisplayattr76 = (((null != var_attrcontent74) && (!"".equals(var_attrcontent74))) && ((!"".equals(var_attrvalue73)) && (!((Object)false).equals(var_attrvalue73))));
            if (var_shoulddisplayattr76) {
                out.write(" srcset");
                {
                    boolean var_istrueattr75 = (var_attrvalue73.equals(true));
                    if (!var_istrueattr75) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent74));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue77 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "attribute");
        {
            boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
            if (var_shoulddisplayattr80) {
                out.write(" srcset");
                {
                    boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                    if (!var_istrueattr79) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <img");
{
    Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconAlt");
    {
        Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "attribute");
        {
            boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
            if (var_shoulddisplayattr84) {
                out.write(" alt");
                {
                    boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                    if (!var_istrueattr83) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayIconWebImagePath");
    {
        Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "uri");
        {
            boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
            if (var_shoulddisplayattr88) {
                out.write(" src");
                {
                    boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                    if (!var_istrueattr87) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        </picture>\r\n                                    </div>\r\n                                    <p>");
{
    Object var_89 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "dayLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_89));
}
out.write("</p>\r\n                                </div>\r\n                                <div class=\"data-input__suffix\">\r\n                                    <p class=\"calendar__input-field\"></p>\r\n                                    <span class=\"data-input__arrow-icon\">\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg\"/>\r\n                  </span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"calendar-popup\">\r\n                                <div class=\"month-line\">\r\n                                    <span class=\"month-data\"></span>\r\n                                    <div>\r\n                                        <img class=\"prev-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-left-icon.svg\"/>\r\n                                        <img class=\"next-month\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-right-icon.svg\"/>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"date-line\">\r\n                                </div>\r\n                                <div class=\"day-tbl\">\r\n                                    <div class=\"week line1\"></div>\r\n                                    <div class=\"week line2\"></div>\r\n                                    <div class=\"week line3\"></div>\r\n                                    <div class=\"week line4\"></div>\r\n                                    <div class=\"week line5\"></div>\r\n                                    <div class=\"week line6\"></div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"header-select__data time-select\"");
{
    Object var_attrvalue90 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconMobileImagePath");
    {
        Object var_attrcontent91 = renderContext.call("xss", var_attrvalue90, "attribute");
        {
            boolean var_shoulddisplayattr93 = (((null != var_attrcontent91) && (!"".equals(var_attrcontent91))) && ((!"".equals(var_attrvalue90)) && (!((Object)false).equals(var_attrvalue90))));
            if (var_shoulddisplayattr93) {
                out.write(" icon-mobile-path");
                {
                    boolean var_istrueattr92 = (var_attrvalue90.equals(true));
                    if (!var_istrueattr92) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent91));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue94 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconWebImagePath");
    {
        Object var_attrcontent95 = renderContext.call("xss", var_attrvalue94, "attribute");
        {
            boolean var_shoulddisplayattr97 = (((null != var_attrcontent95) && (!"".equals(var_attrcontent95))) && ((!"".equals(var_attrvalue94)) && (!((Object)false).equals(var_attrvalue94))));
            if (var_shoulddisplayattr97) {
                out.write(" icon-web-path");
                {
                    boolean var_istrueattr96 = (var_attrvalue94.equals(true));
                    if (!var_istrueattr96) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent95));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue98 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeIconAlt");
    {
        Object var_attrcontent99 = renderContext.call("xss", var_attrvalue98, "attribute");
        {
            boolean var_shoulddisplayattr101 = (((null != var_attrcontent99) && (!"".equals(var_attrcontent99))) && ((!"".equals(var_attrvalue98)) && (!((Object)false).equals(var_attrvalue98))));
            if (var_shoulddisplayattr101) {
                out.write(" icon-alt");
                {
                    boolean var_istrueattr100 = (var_attrvalue98.equals(true));
                    if (!var_istrueattr100) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent99));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue102 = renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "timeLabel");
    {
        Object var_attrcontent103 = renderContext.call("xss", var_attrvalue102, "attribute");
        {
            boolean var_shoulddisplayattr105 = (((null != var_attrcontent103) && (!"".equals(var_attrcontent103))) && ((!"".equals(var_attrvalue102)) && (!((Object)false).equals(var_attrvalue102))));
            if (var_shoulddisplayattr105) {
                out.write(" title");
                {
                    boolean var_istrueattr104 = (var_attrvalue102.equals(true));
                    if (!var_istrueattr104) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent103));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <span aria-hidden=\"true\" class=\"popup__close-button\"></span>\r\n                <p class=\"exchange-rate__empty-label\">");
{
    Object var_106 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "errorMessage"), "text");
    out.write(renderContext.getObjectModel().toString(var_106));
}
out.write("</p>\r\n                <div class=\"table-content-container exchange-rate__table\">\r\n                    <div class=\"exchange-rate__table-content\">\r\n                        <div class=\"exchange-rate__table-records table-header\">\r\n                            <div class=\"table__first-column up-par first-row first-column\">\r\n                                <p>");
{
    Object var_107 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "currencyLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_107));
}
out.write("</p>\r\n                            </div>\r\n                            <div class=\"table-records__data\">\r\n                                <div class=\"group-header\">\r\n                                    <div class=\"table-records__data-content up-par first-row\">\r\n                                        <p>");
{
    Object var_108 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "purchaseLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_108));
}
out.write("</p>\r\n                                    </div>\r\n                                    <div class=\"table-records__data-content row-divided first-row\">\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_109 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "cashLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_109));
}
out.write("</p>\r\n                                        </div>\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_110 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "transferLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_110));
}
out.write("</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"group-header\">\r\n                                    <div class=\"table-records__data-content up-par first-row last-column\">\r\n                                        <p>");
{
    Object var_111 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "sellLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_111));
}
out.write("</p>\r\n                                    </div>\r\n                                    <div class=\"table-records__data-content row-divided first-row last-column\">\r\n                                        <div class=\"data-content__item\">\r\n                                            <p>");
{
    Object var_112 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "cashLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_112));
}
out.write("</p>\r\n                                        </div>\r\n                                        <div class=\"data-content__item last-column\">\r\n                                            <p>");
{
    Object var_113 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "transferLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_113));
}
out.write("</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate-table-content\">\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column one-col\">\r\n                                <p><strong>");
{
    Object var_114 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "centralLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_114));
}
out.write("</strong><span id=\"central-rate\"></span></p>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column one-col\">\r\n                                <p><strong>");
{
    Object var_115 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "floorLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_115));
}
out.write("</strong><span id=\"floor-rate\"></span></p>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column one-col\">\r\n                                <p><strong>");
{
    Object var_116 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "ceilingLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_116));
}
out.write("</strong><span id=\"ceiling-rate\"></span></p>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column left-pos strong-par first-column\">\r\n                                <p>");
{
    Object var_117 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "usdLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_117));
}
out.write("</p>\r\n                            </div>\r\n                            <div class=\"table-records__data width-33-66 strong-par\">\r\n                                <div class=\"table-records__data-content\">\r\n                                </div>\r\n                                <div class=\"table-records__data-content strong-par last-column\">\r\n                                    <p>");
{
    Object var_118 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "refLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_118));
}
out.write("</p>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column left-pos strong-par first-column\">\r\n                                <p>");
{
    Object var_119 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "termLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_119));
}
out.write("</p>\r\n                            </div>\r\n                            <div class=\"table-records__data width-66-33 strong-par\">\r\n                                <div class=\"table-records__data-content row-divided\">\r\n                                    <div class=\"data-content__item\"></div>\r\n                                    <div class=\"data-content__item\">\r\n                                        <p>");
{
    Object var_120 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "purchaseLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_120));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"table-records__data-content last-column\">\r\n                                    <p>");
{
    Object var_121 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "sellLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_121));
}
out.write("</p>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div id=\"tenor-rate-content\">\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records first-border\">\r\n                            <div class=\"table__first-column left-pos strong-par first-column\">\r\n                            </div>\r\n                            <div class=\"table-records__data width-66-33 no-border strong-par\">\r\n                                <div class=\"table-records__data-content row-divided\">\r\n                                    <div class=\"data-content__item\"></div>\r\n                                    <div class=\"data-content__item\">\r\n                                        <p>");
{
    Object var_122 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "refLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_122));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"table-records__data-content last-column\">\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"exchange-rate__table-records\">\r\n                            <div class=\"table__first-column left-pos strong-par first-column\">\r\n                                <p>");
{
    Object var_123 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "goldLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_123));
}
out.write("</p>\r\n                            </div>\r\n                            <div class=\"table-records__data width-66-33\">\r\n                                <div class=\"table-records__data-content row-divided\">\r\n                                    <div class=\"data-content__item\">\r\n                                        <p id=\"gold-bid-rate\"></p>\r\n                                    </div>\r\n                                    <div class=\"data-content__item\"></div>\r\n                                </div>\r\n                                <div class=\"table-records__data-content last-column\">\r\n                                    <p id=\"gold-ask-rate\"></p>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div id=\"tenorint-rate\">\r\n                            <div id=\"tenorint-rate-content\">\r\n                            </div>\r\n                        </div>\r\n                        <!-- Table note italic -->\r\n                        <div class=\"table-note\">\r\n                            <div class=\"exchange-rate__table-note\"");
{
    Object var_attrvalue124 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "summaryDesc"), "html");
    {
        boolean var_shoulddisplayattr127 = ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124)));
        if (var_shoulddisplayattr127) {
            out.write(" note");
            {
                boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                if (!var_istrueattr126) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue124));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(">\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-content-container popup__download-button\" data-tracking-click-event=\"download\"");
{
    String var_attrcontent128 = (((("{'download':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_page, "title"), "attribute"))) + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_page, "brandSlug")) ? "_" : ""), "attribute"))) + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_page, "brandSlug"), "attribute"))) + "' }");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent128));
    out.write("\"");
}
{
    String var_attrcontent129 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'Download'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent129));
    out.write("\"");
}
out.write(">\r\n                    <div class=\"download-button__wrapper\">\r\n                        <button class=\"title\" type=\"button\">\r\n                            <span>");
{
    Object var_130 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "ctaLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_130));
}
out.write("</span>\r\n                            <div class=\"download-button__icon\">\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/download-icon.svg\"/>\r\n                            </div>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"exchange-rate__footer\"><h6 class=\"exchange-rate__footer__info\">");
{
    String var_131 = ("\r\n                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "footerLabelOne"), "text")));
    out.write(renderContext.getObjectModel().toString(var_131));
}
out.write("</h6>\r\n                <p>");
{
    Object var_132 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "footerLabelTwo"), "html");
    out.write(renderContext.getObjectModel().toString(var_132));
}
out.write("</p>\r\n                <p>");
{
    Object var_133 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_exchangeratemodel, "footerLabelThree"), "text");
    out.write(renderContext.getObjectModel().toString(var_133));
}
out.write("</p></div>\r\n        </div>\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

