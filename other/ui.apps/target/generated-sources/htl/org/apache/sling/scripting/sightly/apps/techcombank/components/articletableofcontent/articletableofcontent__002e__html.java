/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.articletableofcontent;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class articletableofcontent__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Object _global_jsonmap = null;
Collection var_collectionvar7_list_coerced$ = null;
Collection var_collectionvar22_list_coerced$ = null;
Collection var_collectionvar32_list_coerced$ = null;
Object _global_h3title = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.TableOfContents.class.getName(), obj());
{
    boolean var_testvariable5 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "hideStatus")));
    if (var_testvariable5) {
        out.write("\r\n\t");
_global_jsonmap = renderContext.getObjectModel().resolveProperty(_global_model, "jsonMap");
        if (renderContext.getObjectModel().toBoolean(_global_jsonmap)) {
            out.write("\r\n\t\t<div class=\"table-content\">\r\n\t\t\t<header class=\"table-content__header\">\r\n\t\t\t\t<div class=\"table-content__content\">\r\n\t\t\t\t\t<div class=\"table-content__label\" aria-hidden=\"true\">\r\n\t\t\t\t\t\t<p class=\"table-content__labelText\">");
            {
                Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "title"), "text");
                out.write(renderContext.getObjectModel().toString(var_6));
            }
            out.write("</p>\r\n\t\t\t\t\t\t<img class=\"table-content__icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\" alt=\"\"/>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"table-content__entered\">\r\n\t\t\t\t\t\t<div class=\"table-content__entered-wrapper\">\r\n\t\t\t\t\t\t\t<div class=\"table-content__entered-inner\">\r\n\t\t\t\t\t\t\t\t<ul class=\"table-content__tableList\">\r\n\t\t\t\t\t\t\t\t\t");
            {
                Object var_collectionvar7 = _global_jsonmap;
                {
                    long var_size8 = ((var_collectionvar7_list_coerced$ == null ? (var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7)) : var_collectionvar7_list_coerced$).size());
                    {
                        boolean var_notempty9 = (var_size8 > 0);
                        if (var_notempty9) {
                            {
                                long var_end12 = var_size8;
                                {
                                    boolean var_validstartstepend13 = (((0 < var_size8) && true) && (var_end12 > 0));
                                    if (var_validstartstepend13) {
                                        if (var_collectionvar7_list_coerced$ == null) {
                                            var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7);
                                        }
                                        long var_index14 = 0;
                                        for (Object key : var_collectionvar7_list_coerced$) {
                                            {
                                                boolean var_traversal16 = (((var_index14 >= 0) && (var_index14 <= var_end12)) && true);
                                                if (var_traversal16) {
                                                    out.write("\r\n\t\t\t\t\t\t\t\t\t\t<li class=\"table-content__tableItem\">\r\n\t\t\t\t\t\t\t\t\t\t\t<a");
                                                    {
                                                        String var_attrcontent17 = ("#" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_jsonmap, key), "id"), "uri")));
                                                        out.write(" href=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                        out.write("\"");
                                                    }
                                                    out.write(" data-tracking-click-event=\"linkClick\" class=\"table-content__tableItem-text\"");
                                                    {
                                                        String var_attrcontent18 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_jsonmap, key), "value"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent19 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                        out.write("\"");
                                                    }
                                                    out.write(">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<span>");
                                                    {
                                                        Object var_20 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_jsonmap, key), "value"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_20));
                                                    }
                                                    out.write("</span>\r\n\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t");
                                                    {
                                                        Object var_testvariable21 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_jsonmap, key), "h3");
                                                        if (renderContext.getObjectModel().toBoolean(var_testvariable21)) {
                                                            out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t");
                                                            {
                                                                Object var_collectionvar22 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_jsonmap, key), "h3");
                                                                {
                                                                    long var_size23 = ((var_collectionvar22_list_coerced$ == null ? (var_collectionvar22_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar22)) : var_collectionvar22_list_coerced$).size());
                                                                    {
                                                                        boolean var_notempty24 = (var_size23 > 0);
                                                                        if (var_notempty24) {
                                                                            {
                                                                                long var_end27 = var_size23;
                                                                                {
                                                                                    boolean var_validstartstepend28 = (((0 < var_size23) && true) && (var_end27 > 0));
                                                                                    if (var_validstartstepend28) {
                                                                                        out.write("<ul class=\"table-content__listChild\">");
                                                                                        if (var_collectionvar22_list_coerced$ == null) {
                                                                                            var_collectionvar22_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar22);
                                                                                        }
                                                                                        long var_index29 = 0;
                                                                                        for (Object h3key : var_collectionvar22_list_coerced$) {
                                                                                            {
                                                                                                boolean var_traversal31 = (((var_index29 >= 0) && (var_index29 <= var_end27)) && true);
                                                                                                if (var_traversal31) {
                                                                                                    out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t");
                                                                                                    {
                                                                                                        Object var_collectionvar32 = h3key;
                                                                                                        {
                                                                                                            long var_size33 = ((var_collectionvar32_list_coerced$ == null ? (var_collectionvar32_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar32)) : var_collectionvar32_list_coerced$).size());
                                                                                                            {
                                                                                                                boolean var_notempty34 = (var_size33 > 0);
                                                                                                                if (var_notempty34) {
                                                                                                                    {
                                                                                                                        long var_end37 = var_size33;
                                                                                                                        {
                                                                                                                            boolean var_validstartstepend38 = (((0 < var_size33) && true) && (var_end37 > 0));
                                                                                                                            if (var_validstartstepend38) {
                                                                                                                                if (var_collectionvar32_list_coerced$ == null) {
                                                                                                                                    var_collectionvar32_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar32);
                                                                                                                                }
                                                                                                                                long var_index39 = 0;
                                                                                                                                for (Object itemh3list : var_collectionvar32_list_coerced$) {
                                                                                                                                    {
                                                                                                                                        boolean var_traversal41 = (((var_index39 >= 0) && (var_index39 <= var_end37)) && true);
                                                                                                                                        if (var_traversal41) {
                                                                                                                                            out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
                                                                                                                                            {
                                                                                                                                                Object var_testvariable42 = itemh3list;
                                                                                                                                                if (renderContext.getObjectModel().toBoolean(var_testvariable42)) {
                                                                                                                                                    out.write("<li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t");
_global_h3title = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(h3key, itemh3list), "value");
                                                                                                                                                    out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a");
                                                                                                                                                    {
                                                                                                                                                        String var_attrcontent43 = ("#" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(h3key, itemh3list), "id"), "uri")));
                                                                                                                                                        out.write(" href=\"");
                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                                                                                                                                        out.write("\"");
                                                                                                                                                    }
                                                                                                                                                    out.write(" class=\"table-content__tableItem-text\" data-tracking-click-event=\"linkClick\"");
                                                                                                                                                    {
                                                                                                                                                        String var_attrcontent44 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", _global_h3title, "attribute"))) + "'}");
                                                                                                                                                        out.write(" data-tracking-click-info-value=\"");
                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                                                                                                        out.write("\"");
                                                                                                                                                    }
                                                                                                                                                    {
                                                                                                                                                        String var_attrcontent45 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                                                                                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                                                                                                        out.write("\"");
                                                                                                                                                    }
                                                                                                                                                    out.write(">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">");
                                                                                                                                                    {
                                                                                                                                                        Object var_46 = renderContext.call("xss", _global_h3title, "text");
                                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_46));
                                                                                                                                                    }
                                                                                                                                                    out.write("</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>");
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t");
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    var_index39++;
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        var_collectionvar32_list_coerced$ = null;
                                                                                                    }
                                                                                                    out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t\t");
                                                                                                }
                                                                                            }
                                                                                            var_index29++;
                                                                                        }
                                                                                        out.write("</ul>");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                var_collectionvar22_list_coerced$ = null;
                                                            }
                                                            out.write("\r\n\t\t\t\t\t\t\t\t\t\t\t");
                                                        }
                                                    }
                                                    out.write("\r\n\t\t\t\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t\t\t");
                                                }
                                            }
                                            var_index14++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar7_list_coerced$ = null;
            }
            out.write("\r\n\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</header>\r\n\t\t</div>\r\n\t");
        }
        out.write("\r\n");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

