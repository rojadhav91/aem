/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.columnpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class columnpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Object _global_template = null;
Collection var_collectionvar5_list_coerced$ = null;
Collection var_collectionvar25_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n   <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.ColumnPanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
out.write("\r\n\r\n<div class=\"column-panel-component\">\r\n   <div class=\"column-panel-component__container\">\r\n      ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_model, "cardLists");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            out.write("<div class=\"column-panels\">");
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object cards : var_collectionvar5_list_coerced$) {
                                {
                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                    if (var_traversal14) {
                                        out.write("\r\n         <div class=\"item-panel\">\r\n            <div class=\"image\">\r\n               <img");
                                        {
                                            Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(cards, "iconImage");
                                            {
                                                Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "uri");
                                                {
                                                    boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                                    if (var_shoulddisplayattr18) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                                            if (!var_istrueattr17) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(cards, "iconImageAltText");
                                            {
                                                Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                    if (var_shoulddisplayattr22) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                            if (!var_istrueattr21) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n               <p class=\"image-title\">");
                                        {
                                            String var_23 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(cards, "categoryTitle"), "text")) + "\r\n                  ");
                                            out.write(renderContext.getObjectModel().toString(var_23));
                                        }
                                        out.write("<span class=\"tooltiptext\">");
                                        {
                                            String var_24 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(cards, "categoryTitle"), "text")) + "\r\n                  ");
                                            out.write(renderContext.getObjectModel().toString(var_24));
                                        }
                                        out.write("</span>\r\n               </p>\r\n            </div>\r\n            ");
                                        {
                                            Object var_collectionvar25 = renderContext.getObjectModel().resolveProperty(cards, "cardItems");
                                            {
                                                long var_size26 = ((var_collectionvar25_list_coerced$ == null ? (var_collectionvar25_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar25)) : var_collectionvar25_list_coerced$).size());
                                                {
                                                    boolean var_notempty27 = (var_size26 > 0);
                                                    if (var_notempty27) {
                                                        {
                                                            long var_end30 = var_size26;
                                                            {
                                                                boolean var_validstartstepend31 = (((0 < var_size26) && true) && (var_end30 > 0));
                                                                if (var_validstartstepend31) {
                                                                    out.write("<div class=\"list-option\">");
                                                                    if (var_collectionvar25_list_coerced$ == null) {
                                                                        var_collectionvar25_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar25);
                                                                    }
                                                                    long var_index32 = 0;
                                                                    for (Object item : var_collectionvar25_list_coerced$) {
                                                                        {
                                                                            boolean var_traversal34 = (((var_index32 >= 0) && (var_index32 <= var_end30)) && true);
                                                                            if (var_traversal34) {
                                                                                out.write("\r\n               <a class=\"option\"");
                                                                                {
                                                                                    Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(item, "cardLinkUrl");
                                                                                    {
                                                                                        Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "uri");
                                                                                        {
                                                                                            boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                                                                                            if (var_shoulddisplayattr38) {
                                                                                                out.write(" href");
                                                                                                {
                                                                                                    boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                                                                                    if (!var_istrueattr37) {
                                                                                                        out.write("=\"");
                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                                                        out.write("\"");
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                {
                                                                                    Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(item, "cardTarget");
                                                                                    {
                                                                                        Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
                                                                                        {
                                                                                            boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                                                                                            if (var_shoulddisplayattr42) {
                                                                                                out.write(" target");
                                                                                                {
                                                                                                    boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                                                                                    if (!var_istrueattr41) {
                                                                                                        out.write("=\"");
                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                                                        out.write("\"");
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                {
                                                                                    String var_attrvalue43 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "cardNoFollow")) ? "nofollow" : "");
                                                                                    {
                                                                                        Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                                                                                        {
                                                                                            boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                                                            if (var_shoulddisplayattr46) {
                                                                                                out.write(" rel");
                                                                                                {
                                                                                                    boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                                                                    if (!var_istrueattr45) {
                                                                                                        out.write("=\"");
                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                                                        out.write("\"");
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                {
                                                                                    String var_attrcontent47 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "attribute"))) + "'}");
                                                                                    out.write(" data-tracking-click-info-value=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                                                                                    out.write("\"");
                                                                                }
                                                                                {
                                                                                    String var_attrcontent48 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                                                                    out.write("\"");
                                                                                }
                                                                                out.write(">\r\n                  <div class=\"content\">\r\n                     <p class=\"content-title\">");
                                                                                {
                                                                                    String var_49 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "text")));
                                                                                    out.write(renderContext.getObjectModel().toString(var_49));
                                                                                }
                                                                                out.write("</p>\r\n                     ");
                                                                                {
                                                                                    Object var_testvariable50 = renderContext.getObjectModel().resolveProperty(item, "cardDescription");
                                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable50)) {
                                                                                        out.write("<p class=\"content-description\">");
                                                                                        {
                                                                                            String var_51 = (("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "html"))) + "\r\n                        ");
                                                                                            out.write(renderContext.getObjectModel().toString(var_51));
                                                                                        }
                                                                                        out.write("<span class=\"tooltiptext\">");
                                                                                        {
                                                                                            Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "text");
                                                                                            out.write(renderContext.getObjectModel().toString(var_52));
                                                                                        }
                                                                                        out.write("</span>\r\n                     </p>");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n                  </div>\r\n                  ");
                                                                                {
                                                                                    Object var_testvariable53 = renderContext.getObjectModel().resolveProperty(item, "cardArrow");
                                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable53)) {
                                                                                        out.write("<div class=\"arrow\">\r\n                     <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg\" alt=\"arrow\"/>\r\n                  </div>");
                                                                                    }
                                                                                }
                                                                                out.write("\r\n               </a>\r\n            ");
                                                                            }
                                                                        }
                                                                        var_index32++;
                                                                    }
                                                                    out.write("</div>");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            var_collectionvar25_list_coerced$ = null;
                                        }
                                        out.write("\r\n         </div>\r\n      ");
                                    }
                                }
                                var_index12++;
                            }
                            out.write("</div>");
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n      ");
{
    Object var_testvariable54 = renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabelText");
    if (renderContext.getObjectModel().toBoolean(var_testvariable54)) {
        out.write("<div class=\"read-more\"");
        {
            Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabelText");
            {
                Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                {
                    boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                    if (var_shoulddisplayattr58) {
                        out.write(" data-viewmore");
                        {
                            boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                            if (!var_istrueattr57) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue59 = renderContext.getObjectModel().resolveProperty(_global_model, "viewLessLabelText");
            {
                Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
                {
                    boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
                    if (var_shoulddisplayattr62) {
                        out.write(" data-viewless");
                        {
                            boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                            if (!var_istrueattr61) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n         <button>");
        {
            Object var_63 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabelText"), "text");
            out.write(renderContext.getObjectModel().toString(var_63));
        }
        out.write("</button>\r\n         <div class=\"expand\">\r\n            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/Mediamodifier-Design-down.svg\"/>\r\n         </div>\r\n      </div>");
    }
}
out.write("\r\n   </div>\r\n\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

