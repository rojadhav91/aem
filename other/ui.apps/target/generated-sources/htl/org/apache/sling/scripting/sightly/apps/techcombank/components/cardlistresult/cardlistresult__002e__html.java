/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.cardlistresult;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class cardlistresult__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Collection var_collectionvar83_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.CardListingResultModel.class.getName(), obj());
out.write("\r\n<section class=\"container section__margin-medium\">\r\n    <div class=\"credit-card-comparison-result\"");
{
    Object var_attrvalue5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardCFRootPath"), "html");
    {
        boolean var_shoulddisplayattr8 = ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5)));
        if (var_shoulddisplayattr8) {
            out.write(" data-cfPath");
            {
                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                if (!var_istrueattr7) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue5));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "listingPage"), "html");
    {
        boolean var_shoulddisplayattr12 = ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9)));
        if (var_shoulddisplayattr12) {
            out.write(" data-addCardUrl");
            {
                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                if (!var_istrueattr11) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue9));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue13 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "forLabel"), "html");
    {
        boolean var_shoulddisplayattr16 = ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13)));
        if (var_shoulddisplayattr16) {
            out.write(" data-forLabel");
            {
                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                if (!var_istrueattr15) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue13));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue17 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "offersLabel"), "html");
    {
        boolean var_shoulddisplayattr20 = ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17)));
        if (var_shoulddisplayattr20) {
            out.write(" data-offersLabel");
            {
                boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                if (!var_istrueattr19) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue17));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue21 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "utilitiesLabel"), "html");
    {
        boolean var_shoulddisplayattr24 = ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21)));
        if (var_shoulddisplayattr24) {
            out.write(" data-utilitiesLabel");
            {
                boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                if (!var_istrueattr23) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue21));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue25 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "conditionsLabel"), "html");
    {
        boolean var_shoulddisplayattr28 = ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25)));
        if (var_shoulddisplayattr28) {
            out.write(" data-conditionsLabel");
            {
                boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                if (!var_istrueattr27) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue25));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue29 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "ratesLabel"), "html");
    {
        boolean var_shoulddisplayattr32 = ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29)));
        if (var_shoulddisplayattr32) {
            out.write(" data-ratesLabel");
            {
                boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                if (!var_istrueattr31) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue29));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue33 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "outstandingLabel"), "html");
    {
        boolean var_shoulddisplayattr36 = ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33)));
        if (var_shoulddisplayattr36) {
            out.write(" data-outstandingLabel");
            {
                boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                if (!var_istrueattr35) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue33));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue37 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardOffersLabel"), "html");
    {
        boolean var_shoulddisplayattr40 = ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37)));
        if (var_shoulddisplayattr40) {
            out.write(" data-cardOffersLabel");
            {
                boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                if (!var_istrueattr39) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue37));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue41 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "feesLabel"), "html");
    {
        boolean var_shoulddisplayattr44 = ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41)));
        if (var_shoulddisplayattr44) {
            out.write(" data-feesLabel");
            {
                boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                if (!var_istrueattr43) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue41));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "nudgeTags"), "html");
    {
        boolean var_shoulddisplayattr48 = ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45)));
        if (var_shoulddisplayattr48) {
            out.write(" data-nudgeoptions");
            {
                boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                if (!var_istrueattr47) {
                    out.write("='");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue45));
                    out.write("'");
                }
            }
        }
    }
}
{
    Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_global_model, "regCtaMobile");
    {
        Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
        {
            boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
            if (var_shoulddisplayattr52) {
                out.write(" data-applynowLink");
                {
                    boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                    if (!var_istrueattr51) {
                        out.write("='");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                        out.write("'");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
    {
        Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
        {
            boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
            if (var_shoulddisplayattr56) {
                out.write(" data-component-name");
                {
                    boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                    if (!var_istrueattr55) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n        <div class=\"content-wrapper\">\r\n            <div class=\"compare-product\">\r\n                <div class=\"compare-product__container\">\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"popup\">\r\n            <div class=\"popup-content\">\r\n                <img alt=\"\" class=\"close\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_red.svg\"/>\r\n                <div class=\"popup-register_image\">\r\n                    <picture>\r\n                        <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(_global_model, "regImgMobile");
    {
        Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "attribute");
        {
            boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
            if (var_shoulddisplayattr60) {
                out.write(" srcset");
                {
                    boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                    if (!var_istrueattr59) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(_global_model, "regImgMobile");
    {
        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "attribute");
        {
            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
            if (var_shoulddisplayattr64) {
                out.write(" srcset");
                {
                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                    if (!var_istrueattr63) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
        {
            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
            if (var_shoulddisplayattr68) {
                out.write(" srcset");
                {
                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                    if (!var_istrueattr67) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
        {
            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
            if (var_shoulddisplayattr72) {
                out.write(" srcset");
                {
                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                    if (!var_istrueattr71) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        <img");
{
    Object var_attrvalue73 = renderContext.getObjectModel().resolveProperty(_global_model, "imageRegAlt");
    {
        Object var_attrcontent74 = renderContext.call("xss", var_attrvalue73, "attribute");
        {
            boolean var_shoulddisplayattr76 = (((null != var_attrcontent74) && (!"".equals(var_attrcontent74))) && ((!"".equals(var_attrvalue73)) && (!((Object)false).equals(var_attrvalue73))));
            if (var_shoulddisplayattr76) {
                out.write(" alt");
                {
                    boolean var_istrueattr75 = (var_attrvalue73.equals(true));
                    if (!var_istrueattr75) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent74));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"responsive\" sizes=\"100vw\" decoding=\"async\"");
{
    Object var_attrvalue77 = renderContext.getObjectModel().resolveProperty(_global_model, "regImg");
    {
        Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "uri");
        {
            boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
            if (var_shoulddisplayattr80) {
                out.write(" src");
                {
                    boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                    if (!var_istrueattr79) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                    </picture>\r\n                </div>\r\n                <div class=\"popup-register_content\">\r\n                    <div class=\"popup-register_contentTop\">\r\n                        <h4 class=\"popup-register_title\">");
{
    Object var_81 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "regTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_81));
}
out.write("</h4>\r\n                        <div class=\"popup-register_desc\">\r\n                            <div class=\"popup-register_text\">\r\n                                <p>");
{
    Object var_82 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "regSubTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_82));
}
out.write("</p>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"popup-register_steps\">\r\n                            ");
{
    Object var_collectionvar83 = renderContext.getObjectModel().resolveProperty(_global_model, "steps");
    {
        long var_size84 = ((var_collectionvar83_list_coerced$ == null ? (var_collectionvar83_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar83)) : var_collectionvar83_list_coerced$).size());
        {
            boolean var_notempty85 = (var_size84 > 0);
            if (var_notempty85) {
                {
                    long var_end88 = var_size84;
                    {
                        boolean var_validstartstepend89 = (((0 < var_size84) && true) && (var_end88 > 0));
                        if (var_validstartstepend89) {
                            if (var_collectionvar83_list_coerced$ == null) {
                                var_collectionvar83_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar83);
                            }
                            long var_index90 = 0;
                            for (Object item : var_collectionvar83_list_coerced$) {
                                {
                                    long itemlist_field$_count = (renderContext.getObjectModel().toNumber(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.ADD.eval(var_index90, 1)).longValue());
                                    {
                                        boolean var_traversal92 = (((var_index90 >= 0) && (var_index90 <= var_end88)) && true);
                                        if (var_traversal92) {
                                            out.write("\r\n                                <div class=\"popup-register_item popup-register_step\">\r\n                                    <h6><span>");
                                            {
                                                String var_93 = (renderContext.getObjectModel().toString(renderContext.call("xss", itemlist_field$_count, "text")) + ".");
                                                out.write(renderContext.getObjectModel().toString(var_93));
                                            }
                                            out.write("</span>");
                                            {
                                                Object var_94 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "stepTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_94));
                                            }
                                            out.write("</h6>\r\n                                    <div class=\"popup-register_titleDescOuter\">\r\n                                        <div class=\"popup-register_titleDesc\">\r\n                                            <picture>\r\n                                                <source media=\"(max-width: 360px)\"");
                                            {
                                                Object var_attrvalue95 = renderContext.getObjectModel().resolveProperty(item, "stepImgMobile");
                                                {
                                                    Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
                                                        if (var_shoulddisplayattr98) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                                                                if (!var_istrueattr97) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                <source media=\"(max-width: 576px)\"");
                                            {
                                                Object var_attrvalue99 = renderContext.getObjectModel().resolveProperty(item, "stepImgMobile");
                                                {
                                                    Object var_attrcontent100 = renderContext.call("xss", var_attrvalue99, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr102 = (((null != var_attrcontent100) && (!"".equals(var_attrcontent100))) && ((!"".equals(var_attrvalue99)) && (!((Object)false).equals(var_attrvalue99))));
                                                        if (var_shoulddisplayattr102) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr101 = (var_attrvalue99.equals(true));
                                                                if (!var_istrueattr101) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent100));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                <source media=\"(min-width: 1080px)\"");
                                            {
                                                Object var_attrvalue103 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent104 = renderContext.call("xss", var_attrvalue103, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr106 = (((null != var_attrcontent104) && (!"".equals(var_attrcontent104))) && ((!"".equals(var_attrvalue103)) && (!((Object)false).equals(var_attrvalue103))));
                                                        if (var_shoulddisplayattr106) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr105 = (var_attrvalue103.equals(true));
                                                                if (!var_istrueattr105) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                <source media=\"(min-width: 1200px)\"");
                                            {
                                                Object var_attrvalue107 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent108 = renderContext.call("xss", var_attrvalue107, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr110 = (((null != var_attrcontent108) && (!"".equals(var_attrcontent108))) && ((!"".equals(var_attrvalue107)) && (!((Object)false).equals(var_attrvalue107))));
                                                        if (var_shoulddisplayattr110) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr109 = (var_attrvalue107.equals(true));
                                                                if (!var_istrueattr109) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent108));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                                <img");
                                            {
                                                Object var_attrvalue111 = renderContext.getObjectModel().resolveProperty(item, "imageStpAlt");
                                                {
                                                    Object var_attrcontent112 = renderContext.call("xss", var_attrvalue111, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr114 = (((null != var_attrcontent112) && (!"".equals(var_attrcontent112))) && ((!"".equals(var_attrvalue111)) && (!((Object)false).equals(var_attrvalue111))));
                                                        if (var_shoulddisplayattr114) {
                                                            out.write(" alt");
                                                            {
                                                                boolean var_istrueattr113 = (var_attrvalue111.equals(true));
                                                                if (!var_istrueattr113) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent112));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(" data-nimg=\"intrinsic\" decoding=\"async\"");
                                            {
                                                Object var_attrvalue115 = renderContext.getObjectModel().resolveProperty(item, "stepImg");
                                                {
                                                    Object var_attrcontent116 = renderContext.call("xss", var_attrvalue115, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr118 = (((null != var_attrcontent116) && (!"".equals(var_attrcontent116))) && ((!"".equals(var_attrvalue115)) && (!((Object)false).equals(var_attrvalue115))));
                                                        if (var_shoulddisplayattr118) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr117 = (var_attrvalue115.equals(true));
                                                                if (!var_istrueattr117) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                            </picture>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            ");
                                        }
                                    }
                                }
                                var_index90++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar83_list_coerced$ = null;
}
out.write("\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"popup-register_contentBottom\">\r\n                        <p class=\"popup-register_title\">");
{
    Object var_119 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bottomText"), "text");
    out.write(renderContext.getObjectModel().toString(var_119));
}
out.write("</p>\r\n                        <div class=\"popup-register_linkGroup\">\r\n                            <div class=\"popup-register_linkItem\">\r\n                                <a");
{
    Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(_global_model, "bottomLink");
    {
        Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "uri");
        {
            boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
            if (var_shoulddisplayattr123) {
                out.write(" href");
                {
                    boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                    if (!var_istrueattr122) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue124 = renderContext.getObjectModel().resolveProperty(_global_model, "ctaBtmTarget");
    {
        Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "attribute");
        {
            boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
            if (var_shoulddisplayattr127) {
                out.write(" target");
                {
                    boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                    if (!var_istrueattr126) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue128 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "ctaBtmFollow")) ? "noopener noreferrer" : "");
    {
        Object var_attrcontent129 = renderContext.call("xss", var_attrvalue128, "attribute");
        {
            boolean var_shoulddisplayattr131 = (((null != var_attrcontent129) && (!"".equals(var_attrcontent129))) && ((!"".equals(var_attrvalue128)) && (!((Object)false).equals(var_attrvalue128))));
            if (var_shoulddisplayattr131) {
                out.write(" rel");
                {
                    boolean var_istrueattr130 = (var_attrvalue128.equals(true));
                    if (!var_istrueattr130) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent129));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                    <span class=\"link-text\">");
{
    Object var_132 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "bottomLinkLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_132));
}
out.write("</span>\r\n                                    <img alt=\"\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg?w=1920&q=75\"/>\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

