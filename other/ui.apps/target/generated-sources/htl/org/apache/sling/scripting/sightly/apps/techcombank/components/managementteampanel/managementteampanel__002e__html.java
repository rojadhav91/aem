/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.managementteampanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class managementteampanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_managementteampanelmodel = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar1_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
_global_managementteampanelmodel = renderContext.call("use", com.techcombank.core.models.ManagementPanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgImage");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"management-team-panel tab-horizontal-report\">\r\n            <div class=\"tab-horizontal-report__container\">\r\n                <div class=\"tab-horizontal-report__tab-content\">\r\n                    <div class=\"tab-horizontal-report__tab-content-card\" data-tab-content=\"directory\">\r\n                        <div class=\"tab-horizontal-bio__list-card\">\r\n                                ");
            {
                Object var_collectionvar1 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "profileItem");
                {
                    long var_size2 = ((var_collectionvar1_list_coerced$ == null ? (var_collectionvar1_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar1)) : var_collectionvar1_list_coerced$).size());
                    {
                        boolean var_notempty3 = (var_size2 > 0);
                        if (var_notempty3) {
                            {
                                long var_end6 = var_size2;
                                {
                                    boolean var_validstartstepend7 = (((0 < var_size2) && true) && (var_end6 > 0));
                                    if (var_validstartstepend7) {
                                        if (var_collectionvar1_list_coerced$ == null) {
                                            var_collectionvar1_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar1);
                                        }
                                        long var_index8 = 0;
                                        for (Object panel : var_collectionvar1_list_coerced$) {
                                            {
                                                boolean var_traversal10 = (((var_index8 >= 0) && (var_index8 <= var_end6)) && true);
                                                if (var_traversal10) {
                                                    out.write("\r\n                                <div class=\"tab-horizontal-bio__card\">\r\n                                    <div class=\"tab-horizontal-bio__card-container\">\r\n                                        <picture>\r\n                                            <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(panel, "profileImageMobile");
                                                        {
                                                            Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                                                                if (var_shoulddisplayattr14) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                                                                        if (!var_istrueattr13) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                                            <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(panel, "profileImageMobile");
                                                        {
                                                            Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                                                if (var_shoulddisplayattr18) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                                                        if (!var_istrueattr17) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                                            <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(panel, "profileImage");
                                                        {
                                                            Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                                if (var_shoulddisplayattr22) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                                        if (!var_istrueattr21) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                                            <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(panel, "profileImage");
                                                        {
                                                            Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                                                                if (var_shoulddisplayattr26) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                                                        if (!var_istrueattr25) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                                            <img");
                                                    {
                                                        Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(panel, "profileImage");
                                                        {
                                                            Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                                                                if (var_shoulddisplayattr30) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                                                        if (!var_istrueattr29) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" alt=\"Profile image\"/>\r\n                                        </picture>\r\n\r\n                                        <div class=\"tab-horizontal-bio__card-content\">\r\n                                            <div class=\"tab-horizontal-bio__card-content-role\">");
                                                    {
                                                        String var_31 = (("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(panel, "designation"), "text"))) + "\r\n                                            ");
                                                        out.write(renderContext.getObjectModel().toString(var_31));
                                                    }
                                                    out.write("</div>\r\n                                            <div class=\"tab-horizontal-bio__card-content-title\">");
                                                    {
                                                        String var_32 = (("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(panel, "name"), "text"))) + "\r\n                                            ");
                                                        out.write(renderContext.getObjectModel().toString(var_32));
                                                    }
                                                    out.write("</div>\r\n                                            <div class=\"tab-horizontal-bio__card-content-description\">");
                                                    {
                                                        String var_33 = (("\r\n                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(panel, "description"), "html"))) + "\r\n                                            ");
                                                        out.write(renderContext.getObjectModel().toString(var_33));
                                                    }
                                                    out.write("</div>\r\n                                            <div class=\"tab-horizontal-bio__card-action\">\r\n                                                <button class=\"tab-horizontal-bio__card-action-btn\">");
                                                    {
                                                        String var_34 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "cardText"), "text"))) + "\r\n                                                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_34));
                                                    }
                                                    out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg\" style=\"width:16px; height: 17px;\" alt=\"close\"/>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            ");
                                                }
                                            }
                                            var_index8++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar1_list_coerced$ = null;
            }
            out.write("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"bio-popup\">\r\n                <div class=\"bio-popup-content\">\r\n                    <picture class=\"background\">\r\n                        <source media=\"(max-width: 360px)\"");
            {
                Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgMobile");
                {
                    Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                    {
                        boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                        if (var_shoulddisplayattr38) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                if (!var_istrueattr37) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                        <source media=\"(max-width: 576px)\"");
            {
                Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgMobile");
                {
                    Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
                    {
                        boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                        if (var_shoulddisplayattr42) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                if (!var_istrueattr41) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                        <source media=\"(min-width: 1080px)\"");
            {
                Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgImage");
                {
                    Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                    {
                        boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                        if (var_shoulddisplayattr46) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                if (!var_istrueattr45) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                        <source media=\"(min-width: 1200px)\"");
            {
                Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgImage");
                {
                    Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
                    {
                        boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                        if (var_shoulddisplayattr50) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                if (!var_istrueattr49) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                        <img");
            {
                Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "panelBgImage");
                {
                    Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "uri");
                    {
                        boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                        if (var_shoulddisplayattr54) {
                            out.write(" src");
                            {
                                boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                                if (!var_istrueattr53) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" alt=\"Panel background image\"/>\r\n                    </picture>\r\n                    <div class=\"personal-info\">\r\n                        <div class=\"info\">\r\n                            <h4 class=\"position\"></h4>\r\n                            <h3 class=\"name\"></h3>\r\n                            <p class=\"detail-info\"></p>\r\n                        </div>\r\n                        <div class=\"avatar\">\r\n                            <img src=\"\" alt=\"\"/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"close-btn\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_white.svg?w=1920&q=75\" alt=\"close\"/>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"management-team-panel__load-more\">\r\n                <button type=\"button\" class=\"load-more__button\" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent55 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                out.write("\"");
            }
            {
                String var_attrcontent56 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "viewMore"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                out.write("\"");
            }
            out.write(">\r\n                    <span>");
            {
                String var_57 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_managementteampanelmodel, "viewMore"), "text")));
                out.write(renderContext.getObjectModel().toString(var_57));
            }
            out.write("</span>\r\n                    <div class=\"load-more__button-icon\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg?w=1920&q=75\" alt=\"Red arrow\"/>\r\n                    </div>\r\n                </button>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar58 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions59_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions59 = obj().with("isEmpty", var_templateoptions59_field$_isempty);
            callUnit(out, renderContext, var_templatevar58, var_templateoptions59);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

