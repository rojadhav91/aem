/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.extended__002d__core.embed;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class embed__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_clientlib = null;
Object _global_embed = null;
Object _global_commonstemplates = null;
Object _global_hascontent = null;
Object _global_youtubeembed = null;
Object _dynamic_properties = bindings.get("properties");
Object _global_processortemplate = null;
Object _dynamic_resource = bindings.get("resource");
out.write("\r\n");
_global_clientlib = renderContext.call("use", "/libs/granite/sightly/templates/clientlib.html", obj());
out.write("\r\n    ");
{
    Object var_templatevar1 = renderContext.getObjectModel().resolveProperty(_global_clientlib, "js");
    {
        String var_templateoptions2_field$_categories = "clientlibs.embed";
        {
            java.util.Map var_templateoptions2 = obj().with("categories", var_templateoptions2_field$_categories);
            callUnit(out, renderContext, var_templatevar1, var_templateoptions2);
        }
    }
}
out.write("\r\n    </meta>\r\n\r\n<input id=\"input-analytics-start\" type=\"hidden\" data-tracking-click-event=\"videoStart\" data-tracking-click-info-value=\"{'videoName':'videoTitle'}\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Embed','type': 'Exit'}}\"/>\r\n<input id=\"input-analytics-complete\" type=\"hidden\" data-tracking-click-event=\"videoComplete\" data-tracking-click-info-value=\"{'videoName':'videoTitle'}\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Embed','type': 'Exit'}}\"/>\r\n");
_global_embed = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Embed.class.getName(), obj());
_global_commonstemplates = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = ((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_embed, "result")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor") : renderContext.getObjectModel().resolveProperty(_global_embed, "result")))) ? ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_embed, "result")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor") : renderContext.getObjectModel().resolveProperty(_global_embed, "result"))) : renderContext.getObjectModel().resolveProperty(_global_embed, "html")))) ? ((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_embed, "result")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor") : renderContext.getObjectModel().resolveProperty(_global_embed, "result")))) ? ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_embed, "result")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor") : renderContext.getObjectModel().resolveProperty(_global_embed, "result"))) : renderContext.getObjectModel().resolveProperty(_global_embed, "html"))) : renderContext.getObjectModel().resolveProperty(_global_embed, "embeddableResourceType")));
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div");
    {
        Object var_attrvalue3 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "data"), "json");
        {
            Object var_attrcontent4 = renderContext.call("xss", var_attrvalue3, "attribute");
            {
                boolean var_shoulddisplayattr6 = (((null != var_attrcontent4) && (!"".equals(var_attrcontent4))) && ((!"".equals(var_attrvalue3)) && (!((Object)false).equals(var_attrvalue3))));
                if (var_shoulddisplayattr6) {
                    out.write(" data-cmp-data-layer");
                    {
                        boolean var_istrueattr5 = (var_attrvalue3.equals(true));
                        if (!var_istrueattr5) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent4));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_embed, "id");
        {
            Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
            {
                boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                if (var_shoulddisplayattr10) {
                    out.write(" id");
                    {
                        boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                        if (!var_istrueattr9) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(" class=\"cmp-custom-embed\">\r\n    ");
_global_youtubeembed = renderContext.call("use", com.techcombank.core.models.ExtendedEmbed.class.getName(), obj());
    out.write("\r\n        <input type=\"hidden\" id=\"embed_video_id\"");
    {
        Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_youtubeembed, "youtubeVideoId");
        {
            Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
            {
                boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                if (var_shoulddisplayattr14) {
                    out.write(" value");
                    {
                        boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                        if (!var_istrueattr13) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n        <input type=\"hidden\" id=\"embed_video_width\"");
    {
        Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_youtubeembed, "youtubeWidth");
        {
            Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
            {
                boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                if (var_shoulddisplayattr18) {
                    out.write(" value");
                    {
                        boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                        if (!var_istrueattr17) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n        <input type=\"hidden\" id=\"embed_video_height\"");
    {
        Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_youtubeembed, "youtubeHeight");
        {
            Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
            {
                boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                if (var_shoulddisplayattr22) {
                    out.write(" value");
                    {
                        boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                        if (!var_istrueattr21) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n        <input type=\"hidden\" id=\"embed_video_aspect_ratio\"");
    {
        Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_youtubeembed, "youtubeAspectRatio");
        {
            Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
            {
                boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                if (var_shoulddisplayattr26) {
                    out.write(" value");
                    {
                        boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                        if (!var_istrueattr25) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    \r\n    <input type=\"hidden\" id=\"embed_id\"");
    {
        Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_embed, "id");
        {
            Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
            {
                boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                if (var_shoulddisplayattr30) {
                    out.write(" value");
                    {
                        boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                        if (!var_istrueattr29) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    <input type=\"hidden\" id=\"embed_url\"");
    {
        Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_global_embed, "url");
        {
            Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
            {
                boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                if (var_shoulddisplayattr34) {
                    out.write(" value");
                    {
                        boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                        if (!var_istrueattr33) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    <input type=\"hidden\" id=\"embed_html\"");
    {
        Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(_global_embed, "html");
        {
            Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
            {
                boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                if (var_shoulddisplayattr38) {
                    out.write(" value");
                    {
                        boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                        if (!var_istrueattr37) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    <input type=\"hidden\" id=\"embed_type\"");
    {
        Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(_global_embed, "type");
        {
            Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
            {
                boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                if (var_shoulddisplayattr42) {
                    out.write(" value");
                    {
                        boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                        if (!var_istrueattr41) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    <input type=\"hidden\" class=\"embed_video_layout\"");
    {
        Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "layout");
        {
            Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
            {
                boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                if (var_shoulddisplayattr46) {
                    out.write(" value");
                    {
                        boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                        if (!var_istrueattr45) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("/>\r\n    ");
    {
        Object var_testvariable47 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_embed, "result")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor") : renderContext.getObjectModel().resolveProperty(_global_embed, "result")));
        if (renderContext.getObjectModel().toBoolean(var_testvariable47)) {
_global_processortemplate = renderContext.call("use", (("processors/" + renderContext.getObjectModel().toString(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "processor"))) + ".html"), obj());
            {
                Object var_templatevar48 = renderContext.getObjectModel().resolveProperty(_global_processortemplate, "render");
                {
                    Object var_templateoptions49_field$_options = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_embed, "result"), "options");
                    {
                        java.util.Map var_templateoptions49 = obj().with("options", var_templateoptions49_field$_options);
                        callUnit(out, renderContext, var_templatevar48, var_templateoptions49);
                    }
                }
            }
        }
    }
    out.write("\r\n    ");
    {
        Object var_testvariable50 = renderContext.getObjectModel().resolveProperty(_global_embed, "html");
        if (renderContext.getObjectModel().toBoolean(var_testvariable50)) {
            {
                Object var_51 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_embed, "html"), "html");
                out.write(renderContext.getObjectModel().toString(var_51));
            }
        }
    }
    out.write("\r\n    ");
    {
        Object var_testvariable52 = renderContext.getObjectModel().resolveProperty(_global_embed, "embeddableResourceType");
        if (renderContext.getObjectModel().toBoolean(var_testvariable52)) {
            {
                Object var_resourcecontent53 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), obj().with("wcmmode", "disabled").with("resourceType", renderContext.getObjectModel().resolveProperty(_global_embed, "embeddableResourceType")));
                out.write(renderContext.getObjectModel().toString(var_resourcecontent53));
            }
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n\r\n");
{
    Object var_templatevar54 = renderContext.getObjectModel().resolveProperty(_global_commonstemplates, "placeholder");
    {
        boolean var_templateoptions55_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions55 = obj().with("isEmpty", var_templateoptions55_field$_isempty);
            callUnit(out, renderContext, var_templatevar54, var_templateoptions55);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

