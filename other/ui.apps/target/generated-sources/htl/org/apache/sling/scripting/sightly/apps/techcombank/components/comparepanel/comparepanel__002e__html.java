/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.comparepanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class comparepanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar2_list_coerced$ = null;
Collection var_collectionvar23_list_coerced$ = null;
Collection var_collectionvar33_list_coerced$ = null;
Collection var_collectionvar58_list_coerced$ = null;
Collection var_collectionvar68_list_coerced$ = null;
Collection var_collectionvar78_list_coerced$ = null;
Collection var_collectionvar217_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.ComparePanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "tagFolder");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"loan-list\">\r\n            <div class=\"table-content\">\r\n                <div class=\"table-root display-flex\">\r\n                    <div class=\"table-grid\">\r\n                        <div class=\"table-item-upper position-relative\">\r\n                            <div class=\"table-upper__select-list\">\r\n                                <div class=\"select-list__panel\">\r\n                                    <div class=\"select-list__dropdown\">\r\n                                        <div aria-hidden=\"true\" class=\"select-list__dropdown-display\">\r\n                                            <div class=\"select-list__dropdown-label\">\r\n                                                <span class=\"dropdown-label__prefix-label\">");
            {
                Object var_1 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "customerTypePlaceholderLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_1));
            }
            out.write("</span>\r\n                                                <span class=\"dropdown-label__place-holder dropdown-label-customer-type\"></span>\r\n                                            </div>\r\n                                            <img class=\"select-list__arrow-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                                        </div>\r\n                                        <div class=\"select-list__dropdown-list\">\r\n                                            <ul class=\"select-list__dropdown-items\">\r\n                                                <li></li>\r\n                                                ");
            {
                Object var_collectionvar2 = renderContext.getObjectModel().resolveProperty(_global_model, "customerTypeTags");
                {
                    long var_size3 = ((var_collectionvar2_list_coerced$ == null ? (var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2)) : var_collectionvar2_list_coerced$).size());
                    {
                        boolean var_notempty4 = (var_size3 > 0);
                        if (var_notempty4) {
                            {
                                long var_end7 = var_size3;
                                {
                                    boolean var_validstartstepend8 = (((0 < var_size3) && true) && (var_end7 > 0));
                                    if (var_validstartstepend8) {
                                        if (var_collectionvar2_list_coerced$ == null) {
                                            var_collectionvar2_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar2);
                                        }
                                        long var_index9 = 0;
                                        for (Object customertypetag : var_collectionvar2_list_coerced$) {
                                            {
                                                boolean var_traversal11 = (((var_index9 >= 0) && (var_index9 <= var_end7)) && true);
                                                if (var_traversal11) {
                                                    out.write("\r\n                                                    <li class=\"customer-type-select-item\"");
                                                    {
                                                        Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(customertypetag, "tagId");
                                                        {
                                                            Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                                                                if (var_shoulddisplayattr15) {
                                                                    out.write(" data-tag-id");
                                                                    {
                                                                        boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                                                                        if (!var_istrueattr14) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(customertypetag, "tagPath");
                                                        {
                                                            Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                                                                if (var_shoulddisplayattr19) {
                                                                    out.write(" data-tag-path");
                                                                    {
                                                                        boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                                                                        if (!var_istrueattr18) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-tracking-click-event=\"articleFilter\"");
                                                    {
                                                        String var_attrcontent20 = (("{'articleFilter' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(customertypetag, "tagTitle"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                        out.write("\"");
                                                    }
                                                    out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Compare Panel','type': 'other'}}\">");
                                                    {
                                                        String var_21 = (("\r\n                                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(customertypetag, "tagTitle"), "text"))) + "\r\n                                                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_21));
                                                    }
                                                    out.write("</li>\r\n                                                ");
                                                }
                                            }
                                            var_index9++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar2_list_coerced$ = null;
            }
            out.write("\r\n                                            </ul>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"select-list__dropdown\">\r\n                                        <div aria-hidden=\"true\" class=\"select-list__dropdown-display\">\r\n                                            <div class=\"select-list__dropdown-label\">\r\n                                                <span class=\"dropdown-label__prefix-label\">");
            {
                Object var_22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "houseTypePlaceholderLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_22));
            }
            out.write("</span>\r\n                                                <span class=\"dropdown-label__place-holder dropdown-label-house-type\"></span>\r\n                                            </div>\r\n                                            <img class=\"select-list__arrow-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                                        </div>\r\n                                        <div class=\"select-list__dropdown-list\">\r\n                                            <ul class=\"select-list__dropdown-items\">\r\n                                                <li></li>\r\n                                                ");
            {
                Object var_collectionvar23 = renderContext.getObjectModel().resolveProperty(_global_model, "customerTypeTags");
                {
                    long var_size24 = ((var_collectionvar23_list_coerced$ == null ? (var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23)) : var_collectionvar23_list_coerced$).size());
                    {
                        boolean var_notempty25 = (var_size24 > 0);
                        if (var_notempty25) {
                            {
                                long var_end28 = var_size24;
                                {
                                    boolean var_validstartstepend29 = (((0 < var_size24) && true) && (var_end28 > 0));
                                    if (var_validstartstepend29) {
                                        if (var_collectionvar23_list_coerced$ == null) {
                                            var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23);
                                        }
                                        long var_index30 = 0;
                                        for (Object customertypetag : var_collectionvar23_list_coerced$) {
                                            {
                                                boolean var_traversal32 = (((var_index30 >= 0) && (var_index30 <= var_end28)) && true);
                                                if (var_traversal32) {
                                                    out.write("\r\n                                                    ");
                                                    {
                                                        Object var_collectionvar33 = renderContext.getObjectModel().resolveProperty(customertypetag, "childTags");
                                                        {
                                                            long var_size34 = ((var_collectionvar33_list_coerced$ == null ? (var_collectionvar33_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar33)) : var_collectionvar33_list_coerced$).size());
                                                            {
                                                                boolean var_notempty35 = (var_size34 > 0);
                                                                if (var_notempty35) {
                                                                    {
                                                                        long var_end38 = var_size34;
                                                                        {
                                                                            boolean var_validstartstepend39 = (((0 < var_size34) && true) && (var_end38 > 0));
                                                                            if (var_validstartstepend39) {
                                                                                if (var_collectionvar33_list_coerced$ == null) {
                                                                                    var_collectionvar33_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar33);
                                                                                }
                                                                                long var_index40 = 0;
                                                                                for (Object housetypetag : var_collectionvar33_list_coerced$) {
                                                                                    {
                                                                                        boolean var_traversal42 = (((var_index40 >= 0) && (var_index40 <= var_end38)) && true);
                                                                                        if (var_traversal42) {
                                                                                            out.write("\r\n                                                        <li class=\"house-type-select-item deactivate\"");
                                                                                            {
                                                                                                Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(housetypetag, "tagId");
                                                                                                {
                                                                                                    Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                                                                        if (var_shoulddisplayattr46) {
                                                                                                            out.write(" data-tag-id");
                                                                                                            {
                                                                                                                boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                                                                                if (!var_istrueattr45) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            {
                                                                                                Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(housetypetag, "tagPath");
                                                                                                {
                                                                                                    Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                                                                                                        if (var_shoulddisplayattr50) {
                                                                                                            out.write(" data-tag-path");
                                                                                                            {
                                                                                                                boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                                                                                                if (!var_istrueattr49) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            {
                                                                                                Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(customertypetag, "tagId");
                                                                                                {
                                                                                                    Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                                                                                                        if (var_shoulddisplayattr54) {
                                                                                                            out.write(" data-customer-type");
                                                                                                            {
                                                                                                                boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                                                                                                                if (!var_istrueattr53) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(" data-tracking-click-event=\"articleFilter\"");
                                                                                            {
                                                                                                String var_attrcontent55 = (("{'articleFilter' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(housetypetag, "tagTitle"), "attribute"))) + "'}");
                                                                                                out.write(" data-tracking-click-info-value=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Compare Panel','type': 'other'}}\">");
                                                                                            {
                                                                                                String var_56 = (("\r\n                                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(housetypetag, "tagTitle"), "text"))) + "\r\n                                                        ");
                                                                                                out.write(renderContext.getObjectModel().toString(var_56));
                                                                                            }
                                                                                            out.write("</li>\r\n                                                    ");
                                                                                        }
                                                                                    }
                                                                                    var_index40++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar33_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n                                                ");
                                                }
                                            }
                                            var_index30++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar23_list_coerced$ = null;
            }
            out.write("\r\n                                            </ul>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"select-list__dropdown\">\r\n                                        <div aria-hidden=\"true\" class=\"select-list__dropdown-display\">\r\n                                            <div class=\"select-list__dropdown-label\">\r\n                                                <span class=\"dropdown-label__prefix-label\">");
            {
                Object var_57 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "investorTypePlaceholderLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_57));
            }
            out.write("</span>\r\n                                                <span class=\"dropdown-label__place-holder dropdown-label-investor-type\"></span>\r\n                                            </div>\r\n                                            <img class=\"select-list__arrow-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                                        </div>\r\n                                        <div class=\"select-list__dropdown-list\">\r\n                                            <ul class=\"select-list__dropdown-items\">\r\n                                                <li></li>\r\n                                                ");
            {
                Object var_collectionvar58 = renderContext.getObjectModel().resolveProperty(_global_model, "customerTypeTags");
                {
                    long var_size59 = ((var_collectionvar58_list_coerced$ == null ? (var_collectionvar58_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar58)) : var_collectionvar58_list_coerced$).size());
                    {
                        boolean var_notempty60 = (var_size59 > 0);
                        if (var_notempty60) {
                            {
                                long var_end63 = var_size59;
                                {
                                    boolean var_validstartstepend64 = (((0 < var_size59) && true) && (var_end63 > 0));
                                    if (var_validstartstepend64) {
                                        if (var_collectionvar58_list_coerced$ == null) {
                                            var_collectionvar58_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar58);
                                        }
                                        long var_index65 = 0;
                                        for (Object customertypetag : var_collectionvar58_list_coerced$) {
                                            {
                                                boolean var_traversal67 = (((var_index65 >= 0) && (var_index65 <= var_end63)) && true);
                                                if (var_traversal67) {
                                                    out.write("\r\n                                                    ");
                                                    {
                                                        Object var_collectionvar68 = renderContext.getObjectModel().resolveProperty(customertypetag, "childTags");
                                                        {
                                                            long var_size69 = ((var_collectionvar68_list_coerced$ == null ? (var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68)) : var_collectionvar68_list_coerced$).size());
                                                            {
                                                                boolean var_notempty70 = (var_size69 > 0);
                                                                if (var_notempty70) {
                                                                    {
                                                                        long var_end73 = var_size69;
                                                                        {
                                                                            boolean var_validstartstepend74 = (((0 < var_size69) && true) && (var_end73 > 0));
                                                                            if (var_validstartstepend74) {
                                                                                if (var_collectionvar68_list_coerced$ == null) {
                                                                                    var_collectionvar68_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar68);
                                                                                }
                                                                                long var_index75 = 0;
                                                                                for (Object housetypetag : var_collectionvar68_list_coerced$) {
                                                                                    {
                                                                                        boolean var_traversal77 = (((var_index75 >= 0) && (var_index75 <= var_end73)) && true);
                                                                                        if (var_traversal77) {
                                                                                            out.write("\r\n                                                        ");
                                                                                            {
                                                                                                Object var_collectionvar78 = renderContext.getObjectModel().resolveProperty(housetypetag, "childTags");
                                                                                                {
                                                                                                    long var_size79 = ((var_collectionvar78_list_coerced$ == null ? (var_collectionvar78_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar78)) : var_collectionvar78_list_coerced$).size());
                                                                                                    {
                                                                                                        boolean var_notempty80 = (var_size79 > 0);
                                                                                                        if (var_notempty80) {
                                                                                                            {
                                                                                                                long var_end83 = var_size79;
                                                                                                                {
                                                                                                                    boolean var_validstartstepend84 = (((0 < var_size79) && true) && (var_end83 > 0));
                                                                                                                    if (var_validstartstepend84) {
                                                                                                                        if (var_collectionvar78_list_coerced$ == null) {
                                                                                                                            var_collectionvar78_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar78);
                                                                                                                        }
                                                                                                                        long var_index85 = 0;
                                                                                                                        for (Object investortypetag : var_collectionvar78_list_coerced$) {
                                                                                                                            {
                                                                                                                                boolean var_traversal87 = (((var_index85 >= 0) && (var_index85 <= var_end83)) && true);
                                                                                                                                if (var_traversal87) {
                                                                                                                                    out.write("\r\n                                                            <li class=\"investor-type-select-item deactivate\"");
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue88 = renderContext.getObjectModel().resolveProperty(investortypetag, "tagId");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent89 = renderContext.call("xss", var_attrvalue88, "attribute");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr91 = (((null != var_attrcontent89) && (!"".equals(var_attrcontent89))) && ((!"".equals(var_attrvalue88)) && (!((Object)false).equals(var_attrvalue88))));
                                                                                                                                                if (var_shoulddisplayattr91) {
                                                                                                                                                    out.write(" data-tag-id");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr90 = (var_attrvalue88.equals(true));
                                                                                                                                                        if (!var_istrueattr90) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(investortypetag, "tagPath");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                                                                                                if (var_shoulddisplayattr95) {
                                                                                                                                                    out.write(" data-tag-path");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                                                                                                        if (!var_istrueattr94) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(customertypetag, "tagId");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                                                                                                                if (var_shoulddisplayattr99) {
                                                                                                                                                    out.write(" data-customer-type");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                                                                                                                        if (!var_istrueattr98) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(housetypetag, "tagId");
                                                                                                                                        {
                                                                                                                                            Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                                                                                                                                            {
                                                                                                                                                boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                                                                                                                if (var_shoulddisplayattr103) {
                                                                                                                                                    out.write(" data-house-type");
                                                                                                                                                    {
                                                                                                                                                        boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                                                                                                        if (!var_istrueattr102) {
                                                                                                                                                            out.write("=\"");
                                                                                                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                                                                                                            out.write("\"");
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    out.write(" data-tracking-click-event=\"articleFilter\"");
                                                                                                                                    {
                                                                                                                                        String var_attrcontent104 = (("{'articleFilter' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(investortypetag, "tagTitle"), "attribute"))) + "'}");
                                                                                                                                        out.write(" data-tracking-click-info-value=\"");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                                                                                                                                        out.write("\"");
                                                                                                                                    }
                                                                                                                                    out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Compare Panel','type': 'other'}}\">");
                                                                                                                                    {
                                                                                                                                        String var_105 = (("\r\n                                                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(investortypetag, "tagTitle"), "text"))) + "\r\n                                                            ");
                                                                                                                                        out.write(renderContext.getObjectModel().toString(var_105));
                                                                                                                                    }
                                                                                                                                    out.write("</li>\r\n                                                        ");
                                                                                                                                }
                                                                                                                            }
                                                                                                                            var_index85++;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                var_collectionvar78_list_coerced$ = null;
                                                                                            }
                                                                                            out.write("\r\n                                                    ");
                                                                                        }
                                                                                    }
                                                                                    var_index75++;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar68_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n                                                ");
                                                }
                                            }
                                            var_index65++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar58_list_coerced$ = null;
            }
            out.write("\r\n                                            </ul>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"select-list__dropdown\">\r\n                                        <button type=\"button\" class=\"select-list__submit\" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent106 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewResultCtaLabel"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent106));
                out.write("\"");
            }
            out.write(" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Compare Panel','type': 'other'}}\">\r\n                                            <span>");
            {
                Object var_107 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewResultCtaLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_107));
            }
            out.write("</span>\r\n                                            <div class=\"select-list__submit-icon\">\r\n                                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                                            </div>\r\n                                        </button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <span>\r\n                                <img src=\"https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=1920&q=75\" srcset=\"\r\n                          https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=360&q=75   360w,\r\n                          https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=576&q=75   576w,\r\n                          https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=1080&q=75 1080w,\r\n                          https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=1200&q=75 1200w,\r\n                          https://d1kndcit1zrj97.cloudfront.net/uploads/bg_header_desktop_32b37c1342.png?w=1920&q=75 1920w\r\n                        \" alt sizes=\"100vw\"/>\r\n                            </span>\r\n                        </div>\r\n                        <div class=\"table-item-below position-relative width-100\">\r\n                            <table class=\"table-item-body\">\r\n                                <tbody class=\"table-body-root\">\r\n                                    <tr class=\"table-body-row table-header\">\r\n                                        <th class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                            <div class=\"table-label-content\">\r\n                                                <p>");
            {
                Object var_108 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "projectNameLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_108));
            }
            out.write("</p>\r\n                                            </div>\r\n                                        </th>\r\n                                        <th class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                            <div class=\"table-label-content\">\r\n                                                <p>");
            {
                Object var_109 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "loanRateOnCapitalNeedLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_109));
            }
            out.write("</p>\r\n                                                <div class=\"table-label-content-icon\">\r\n                                                    <div class=\"content-icon__container\">\r\n                                                        <div class=\"label__text\">\r\n                                                            <div class=\"icon-info\">\r\n                                                                ");
            {
                Object var_testvariable110 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanRateOnCapitalNeed");
                if (renderContext.getObjectModel().toBoolean(var_testvariable110)) {
                    out.write("<span class=\"icon\">\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue111 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent112 = renderContext.call("xss", var_attrvalue111, "attribute");
                            {
                                boolean var_shoulddisplayattr114 = (((null != var_attrcontent112) && (!"".equals(var_attrcontent112))) && ((!"".equals(var_attrvalue111)) && (!((Object)false).equals(var_attrvalue111))));
                                if (var_shoulddisplayattr114) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr113 = (var_attrvalue111.equals(true));
                                        if (!var_istrueattr113) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent112));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue115 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent116 = renderContext.call("xss", var_attrvalue115, "attribute");
                            {
                                boolean var_shoulddisplayattr118 = (((null != var_attrcontent116) && (!"".equals(var_attrcontent116))) && ((!"".equals(var_attrvalue115)) && (!((Object)false).equals(var_attrvalue115))));
                                if (var_shoulddisplayattr118) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr117 = (var_attrvalue115.equals(true));
                                        if (!var_istrueattr117) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue119 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent120 = renderContext.call("xss", var_attrvalue119, "attribute");
                            {
                                boolean var_shoulddisplayattr122 = (((null != var_attrcontent120) && (!"".equals(var_attrcontent120))) && ((!"".equals(var_attrvalue119)) && (!((Object)false).equals(var_attrvalue119))));
                                if (var_shoulddisplayattr122) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr121 = (var_attrvalue119.equals(true));
                                        if (!var_istrueattr121) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent120));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue123 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent124 = renderContext.call("xss", var_attrvalue123, "attribute");
                            {
                                boolean var_shoulddisplayattr126 = (((null != var_attrcontent124) && (!"".equals(var_attrcontent124))) && ((!"".equals(var_attrvalue123)) && (!((Object)false).equals(var_attrvalue123))));
                                if (var_shoulddisplayattr126) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr125 = (var_attrvalue123.equals(true));
                                        if (!var_istrueattr125) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent124));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <img");
                    {
                        Object var_attrvalue127 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                        {
                            Object var_attrcontent128 = renderContext.call("xss", var_attrvalue127, "attribute");
                            {
                                boolean var_shoulddisplayattr130 = (((null != var_attrcontent128) && (!"".equals(var_attrcontent128))) && ((!"".equals(var_attrvalue127)) && (!((Object)false).equals(var_attrvalue127))));
                                if (var_shoulddisplayattr130) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr129 = (var_attrvalue127.equals(true));
                                        if (!var_istrueattr129) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent128));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue131 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent132 = renderContext.call("xss", var_attrvalue131, "uri");
                            {
                                boolean var_shoulddisplayattr134 = (((null != var_attrcontent132) && (!"".equals(var_attrcontent132))) && ((!"".equals(var_attrvalue131)) && (!((Object)false).equals(var_attrvalue131))));
                                if (var_shoulddisplayattr134) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr133 = (var_attrvalue131.equals(true));
                                        if (!var_istrueattr133) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent132));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                                                                    </picture>\r\n                                                                </span>");
                }
            }
            out.write("\r\n                                                                <div class=\"tooltiptext\">");
            {
                Object var_135 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanRateOnCapitalNeed"), "text");
                out.write(renderContext.getObjectModel().toString(var_135));
            }
            out.write("</div>\r\n                                                            </div>\r\n                                                        </div>    \r\n                                                    </div>\r\n                                                </i>\r\n                                            </div>\r\n                                        </th>\r\n                                        <th class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                            <div class=\"table-label-content\">\r\n                                                <p>");
            {
                Object var_136 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "loanRateOnCollateralValueLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_136));
            }
            out.write("</p>\r\n                                                <div class=\"table-label-content-icon\">\r\n                                                    <div class=\"content-icon__container\">\r\n                                                        <div class=\"label__text\">\r\n                                                            <div class=\"icon-info\">\r\n                                                                ");
            {
                Object var_testvariable137 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanRateOnCollateralValueLabel");
                if (renderContext.getObjectModel().toBoolean(var_testvariable137)) {
                    out.write("<span class=\"icon\">\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue138 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent139 = renderContext.call("xss", var_attrvalue138, "attribute");
                            {
                                boolean var_shoulddisplayattr141 = (((null != var_attrcontent139) && (!"".equals(var_attrcontent139))) && ((!"".equals(var_attrvalue138)) && (!((Object)false).equals(var_attrvalue138))));
                                if (var_shoulddisplayattr141) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr140 = (var_attrvalue138.equals(true));
                                        if (!var_istrueattr140) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent139));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue142 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent143 = renderContext.call("xss", var_attrvalue142, "attribute");
                            {
                                boolean var_shoulddisplayattr145 = (((null != var_attrcontent143) && (!"".equals(var_attrcontent143))) && ((!"".equals(var_attrvalue142)) && (!((Object)false).equals(var_attrvalue142))));
                                if (var_shoulddisplayattr145) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr144 = (var_attrvalue142.equals(true));
                                        if (!var_istrueattr144) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent143));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue146 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent147 = renderContext.call("xss", var_attrvalue146, "attribute");
                            {
                                boolean var_shoulddisplayattr149 = (((null != var_attrcontent147) && (!"".equals(var_attrcontent147))) && ((!"".equals(var_attrvalue146)) && (!((Object)false).equals(var_attrvalue146))));
                                if (var_shoulddisplayattr149) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr148 = (var_attrvalue146.equals(true));
                                        if (!var_istrueattr148) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent147));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue150 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent151 = renderContext.call("xss", var_attrvalue150, "attribute");
                            {
                                boolean var_shoulddisplayattr153 = (((null != var_attrcontent151) && (!"".equals(var_attrcontent151))) && ((!"".equals(var_attrvalue150)) && (!((Object)false).equals(var_attrvalue150))));
                                if (var_shoulddisplayattr153) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr152 = (var_attrvalue150.equals(true));
                                        if (!var_istrueattr152) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent151));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <img");
                    {
                        Object var_attrvalue154 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                        {
                            Object var_attrcontent155 = renderContext.call("xss", var_attrvalue154, "attribute");
                            {
                                boolean var_shoulddisplayattr157 = (((null != var_attrcontent155) && (!"".equals(var_attrcontent155))) && ((!"".equals(var_attrvalue154)) && (!((Object)false).equals(var_attrvalue154))));
                                if (var_shoulddisplayattr157) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr156 = (var_attrvalue154.equals(true));
                                        if (!var_istrueattr156) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent155));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue158 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent159 = renderContext.call("xss", var_attrvalue158, "uri");
                            {
                                boolean var_shoulddisplayattr161 = (((null != var_attrcontent159) && (!"".equals(var_attrcontent159))) && ((!"".equals(var_attrvalue158)) && (!((Object)false).equals(var_attrvalue158))));
                                if (var_shoulddisplayattr161) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr160 = (var_attrvalue158.equals(true));
                                        if (!var_istrueattr160) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent159));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                                                                    </picture>\r\n                                                                </span>");
                }
            }
            out.write("\r\n                                                                <div class=\"tooltiptext\">");
            {
                Object var_162 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanRateOnCollateralValueLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_162));
            }
            out.write("</div>\r\n                                                            </div>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </i>\r\n                                            </div>\r\n                                        </th>\r\n                                        <th class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                            <div class=\"table-label-content\">\r\n                                                <p>");
            {
                Object var_163 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "loanTermLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_163));
            }
            out.write("</p>\r\n                                                <div class=\"table-label-content-icon\">\r\n                                                    <div class=\"content-icon__container\">\r\n                                                        <div class=\"label__text\">\r\n                                                            <div class=\"icon-info\">\r\n                                                                ");
            {
                Object var_testvariable164 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanTermLabel");
                if (renderContext.getObjectModel().toBoolean(var_testvariable164)) {
                    out.write("<span class=\"icon\">\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue165 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent166 = renderContext.call("xss", var_attrvalue165, "attribute");
                            {
                                boolean var_shoulddisplayattr168 = (((null != var_attrcontent166) && (!"".equals(var_attrcontent166))) && ((!"".equals(var_attrvalue165)) && (!((Object)false).equals(var_attrvalue165))));
                                if (var_shoulddisplayattr168) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr167 = (var_attrvalue165.equals(true));
                                        if (!var_istrueattr167) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent166));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue169 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent170 = renderContext.call("xss", var_attrvalue169, "attribute");
                            {
                                boolean var_shoulddisplayattr172 = (((null != var_attrcontent170) && (!"".equals(var_attrcontent170))) && ((!"".equals(var_attrvalue169)) && (!((Object)false).equals(var_attrvalue169))));
                                if (var_shoulddisplayattr172) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr171 = (var_attrvalue169.equals(true));
                                        if (!var_istrueattr171) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent170));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue173 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent174 = renderContext.call("xss", var_attrvalue173, "attribute");
                            {
                                boolean var_shoulddisplayattr176 = (((null != var_attrcontent174) && (!"".equals(var_attrcontent174))) && ((!"".equals(var_attrvalue173)) && (!((Object)false).equals(var_attrvalue173))));
                                if (var_shoulddisplayattr176) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr175 = (var_attrvalue173.equals(true));
                                        if (!var_istrueattr175) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent174));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue177 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent178 = renderContext.call("xss", var_attrvalue177, "attribute");
                            {
                                boolean var_shoulddisplayattr180 = (((null != var_attrcontent178) && (!"".equals(var_attrcontent178))) && ((!"".equals(var_attrvalue177)) && (!((Object)false).equals(var_attrvalue177))));
                                if (var_shoulddisplayattr180) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr179 = (var_attrvalue177.equals(true));
                                        if (!var_istrueattr179) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent178));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <img");
                    {
                        Object var_attrvalue181 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                        {
                            Object var_attrcontent182 = renderContext.call("xss", var_attrvalue181, "attribute");
                            {
                                boolean var_shoulddisplayattr184 = (((null != var_attrcontent182) && (!"".equals(var_attrcontent182))) && ((!"".equals(var_attrvalue181)) && (!((Object)false).equals(var_attrvalue181))));
                                if (var_shoulddisplayattr184) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr183 = (var_attrvalue181.equals(true));
                                        if (!var_istrueattr183) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent182));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue185 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent186 = renderContext.call("xss", var_attrvalue185, "uri");
                            {
                                boolean var_shoulddisplayattr188 = (((null != var_attrcontent186) && (!"".equals(var_attrcontent186))) && ((!"".equals(var_attrvalue185)) && (!((Object)false).equals(var_attrvalue185))));
                                if (var_shoulddisplayattr188) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr187 = (var_attrvalue185.equals(true));
                                        if (!var_istrueattr187) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent186));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                                                                    </picture>\r\n                                                                </span>");
                }
            }
            out.write("\r\n                                                                <div class=\"tooltiptext\">");
            {
                Object var_189 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "toolTipLoanTermLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_189));
            }
            out.write("</div>\r\n                                                            </div>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </i>\r\n                                            </div>\r\n                                        </th>\r\n                                        <th class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                            <div class=\"table-label-content\">\r\n                                                <p>");
            {
                Object var_190 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "collateralLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_190));
            }
            out.write("</p>\r\n                                                <div class=\"table-label-content-icon\">\r\n                                                    <div class=\"content-icon__container\">\r\n                                                        <div class=\"label__text\">\r\n                                                            <div class=\"icon-info\">\r\n                                                                ");
            {
                Object var_testvariable191 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipCollateral");
                if (renderContext.getObjectModel().toBoolean(var_testvariable191)) {
                    out.write("<span class=\"icon\">\r\n                                                                    <picture>\r\n                                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue192 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent193 = renderContext.call("xss", var_attrvalue192, "attribute");
                            {
                                boolean var_shoulddisplayattr195 = (((null != var_attrcontent193) && (!"".equals(var_attrcontent193))) && ((!"".equals(var_attrvalue192)) && (!((Object)false).equals(var_attrvalue192))));
                                if (var_shoulddisplayattr195) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr194 = (var_attrvalue192.equals(true));
                                        if (!var_istrueattr194) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent193));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue196 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileTooltipIconPath");
                        {
                            Object var_attrcontent197 = renderContext.call("xss", var_attrvalue196, "attribute");
                            {
                                boolean var_shoulddisplayattr199 = (((null != var_attrcontent197) && (!"".equals(var_attrcontent197))) && ((!"".equals(var_attrvalue196)) && (!((Object)false).equals(var_attrvalue196))));
                                if (var_shoulddisplayattr199) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr198 = (var_attrvalue196.equals(true));
                                        if (!var_istrueattr198) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent197));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue200 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent201 = renderContext.call("xss", var_attrvalue200, "attribute");
                            {
                                boolean var_shoulddisplayattr203 = (((null != var_attrcontent201) && (!"".equals(var_attrcontent201))) && ((!"".equals(var_attrvalue200)) && (!((Object)false).equals(var_attrvalue200))));
                                if (var_shoulddisplayattr203) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr202 = (var_attrvalue200.equals(true));
                                        if (!var_istrueattr202) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent201));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue204 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent205 = renderContext.call("xss", var_attrvalue204, "attribute");
                            {
                                boolean var_shoulddisplayattr207 = (((null != var_attrcontent205) && (!"".equals(var_attrcontent205))) && ((!"".equals(var_attrvalue204)) && (!((Object)false).equals(var_attrvalue204))));
                                if (var_shoulddisplayattr207) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr206 = (var_attrvalue204.equals(true));
                                        if (!var_istrueattr206) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent205));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                                        <img");
                    {
                        Object var_attrvalue208 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                        {
                            Object var_attrcontent209 = renderContext.call("xss", var_attrvalue208, "attribute");
                            {
                                boolean var_shoulddisplayattr211 = (((null != var_attrcontent209) && (!"".equals(var_attrcontent209))) && ((!"".equals(var_attrvalue208)) && (!((Object)false).equals(var_attrvalue208))));
                                if (var_shoulddisplayattr211) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr210 = (var_attrvalue208.equals(true));
                                        if (!var_istrueattr210) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent209));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue212 = renderContext.getObjectModel().resolveProperty(_global_model, "webTooltipIconPath");
                        {
                            Object var_attrcontent213 = renderContext.call("xss", var_attrvalue212, "uri");
                            {
                                boolean var_shoulddisplayattr215 = (((null != var_attrcontent213) && (!"".equals(var_attrcontent213))) && ((!"".equals(var_attrvalue212)) && (!((Object)false).equals(var_attrvalue212))));
                                if (var_shoulddisplayattr215) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr214 = (var_attrvalue212.equals(true));
                                        if (!var_istrueattr214) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent213));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                                                                    </picture>\r\n                                                                </span>");
                }
            }
            out.write("\r\n                                                                <div class=\"tooltiptext\">");
            {
                Object var_216 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "toolTipCollateral"), "text");
                out.write(renderContext.getObjectModel().toString(var_216));
            }
            out.write("</div>\r\n                                                            </div>\r\n                                                        </div>\r\n                                                    </div>\r\n                                                </i>\r\n                                            </div>\r\n                                        </th>\r\n                                    </tr>\r\n                                    ");
            {
                Object var_collectionvar217 = renderContext.getObjectModel().resolveProperty(_global_model, "dataItems");
                {
                    long var_size218 = ((var_collectionvar217_list_coerced$ == null ? (var_collectionvar217_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar217)) : var_collectionvar217_list_coerced$).size());
                    {
                        boolean var_notempty219 = (var_size218 > 0);
                        if (var_notempty219) {
                            {
                                long var_end222 = var_size218;
                                {
                                    boolean var_validstartstepend223 = (((0 < var_size218) && true) && (var_end222 > 0));
                                    if (var_validstartstepend223) {
                                        if (var_collectionvar217_list_coerced$ == null) {
                                            var_collectionvar217_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar217);
                                        }
                                        long var_index224 = 0;
                                        for (Object item : var_collectionvar217_list_coerced$) {
                                            {
                                                boolean var_traversal226 = (((var_index224 >= 0) && (var_index224 <= var_end222)) && true);
                                                if (var_traversal226) {
                                                    out.write("\r\n                                        <tr class=\"table-body-row table-data deactivate\"");
                                                    {
                                                        Object var_attrvalue227 = renderContext.getObjectModel().resolveProperty(item, "tag");
                                                        {
                                                            Object var_attrcontent228 = renderContext.call("xss", var_attrvalue227, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr230 = (((null != var_attrcontent228) && (!"".equals(var_attrcontent228))) && ((!"".equals(var_attrvalue227)) && (!((Object)false).equals(var_attrvalue227))));
                                                                if (var_shoulddisplayattr230) {
                                                                    out.write(" data-tag-path");
                                                                    {
                                                                        boolean var_istrueattr229 = (var_attrvalue227.equals(true));
                                                                        if (!var_istrueattr229) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent228));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">\r\n                                            <td class=\"table-body-cell table-cell-label\" role=\"cell\" scope=\"row\">\r\n                                                <div class=\"table-label-content\">");
                                                    {
                                                        String var_231 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "projectName"), "text"))) + "\r\n                                                ");
                                                        out.write(renderContext.getObjectModel().toString(var_231));
                                                    }
                                                    out.write("</div>\r\n                                            </td>\r\n                                            <td class=\"table-body-cell table-cell-content\" role=\"cell\" scope=\"row\">\r\n                                                <div class=\"table-cell-innercontent\">");
                                                    {
                                                        String var_232 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "loanRateOnCapitalNeed"), "text"))) + "\r\n                                                ");
                                                        out.write(renderContext.getObjectModel().toString(var_232));
                                                    }
                                                    out.write("</div>\r\n                                            </td>\r\n                                            <td class=\"table-body-cell table-cell-content\" role=\"cell\" scope=\"row\">\r\n                                                <div class=\"table-cell-innercontent\">");
                                                    {
                                                        String var_233 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "loanRateOnCollateralValue"), "text"))) + "\r\n                                                ");
                                                        out.write(renderContext.getObjectModel().toString(var_233));
                                                    }
                                                    out.write("</div>\r\n                                            </td>\r\n                                            <td class=\"table-body-cell table-cell-content\" role=\"cell\" scope=\"row\">\r\n                                                <div class=\"table-cell-innercontent\">");
                                                    {
                                                        Object var_234 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "loanTerm"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_234));
                                                    }
                                                    out.write("</div>\r\n                                            </td>\r\n                                            <td class=\"table-body-cell table-cell-content\" role=\"cell\" scope=\"row\">\r\n                                                <div class=\"table-cell-innercontent\">");
                                                    {
                                                        String var_235 = (("\r\n                                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "collateral"), "text"))) + "\r\n                                                ");
                                                        out.write(renderContext.getObjectModel().toString(var_235));
                                                    }
                                                    out.write("</div>\r\n                                            </td>\r\n                                        </tr>\r\n                                    ");
                                                }
                                            }
                                            var_index224++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar217_list_coerced$ = null;
            }
            out.write("\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar236 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions237_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions237 = obj().with("isEmpty", var_templateoptions237_field$_isempty);
            callUnit(out, renderContext, var_templatevar236, var_templateoptions237);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

