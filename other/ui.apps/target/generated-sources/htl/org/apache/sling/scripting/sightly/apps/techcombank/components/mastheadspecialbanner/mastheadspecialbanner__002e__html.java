/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.mastheadspecialbanner;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class mastheadspecialbanner__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_specialbannermodel = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n  <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_specialbannermodel = renderContext.call("use", com.techcombank.core.models.MastHeadSpecialBannerModel.class.getName(), obj());
out.write("\r\n  <section class=\"tcb-hero-product\">\r\n    <picture class=\"tcb-hero-product_background\">\r\n      <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerMobileImage");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" srcset");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerMobileImage");
    {
        Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
        {
            boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
            if (var_shoulddisplayattr12) {
                out.write(" srcset");
                {
                    boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                    if (!var_istrueattr11) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerImage");
    {
        Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
        {
            boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
            if (var_shoulddisplayattr16) {
                out.write(" srcset");
                {
                    boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                    if (!var_istrueattr15) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerImage");
    {
        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
        {
            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
            if (var_shoulddisplayattr20) {
                out.write(" srcset");
                {
                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                    if (!var_istrueattr19) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <img class=\"tcb-hero-product_background-image\"");
{
    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerImage");
    {
        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "uri");
        {
            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
            if (var_shoulddisplayattr24) {
                out.write(" src");
                {
                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                    if (!var_istrueattr23) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bgBannerImgAlt");
    {
        Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
        {
            boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
            if (var_shoulddisplayattr28) {
                out.write(" alt");
                {
                    boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                    if (!var_istrueattr27) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n    </picture>\r\n      <div class=\"hero-product\">\r\n        <div class=\"hero-product__wrapper\">\r\n          <div class=\"hero-product__wrapper-item\">\r\n            <div class=\"hero-product__wrapper-image hero-product__on-large-screen\">\r\n              <img");
{
    Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerImage");
    {
        Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "uri");
        {
            boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
            if (var_shoulddisplayattr32) {
                out.write(" src");
                {
                    boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                    if (!var_istrueattr31) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerImgAlt");
    {
        Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
        {
            boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
            if (var_shoulddisplayattr36) {
                out.write(" alt");
                {
                    boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                    if (!var_istrueattr35) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n            </div>\r\n          </div>\r\n          <div class=\"hero-product__wrapper-item\">\r\n            <div class=\"hero-product__wrapper-content\">\r\n              <label>");
{
    Object var_37 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "highlightedtext"), "text");
    out.write(renderContext.getObjectModel().toString(var_37));
}
out.write("</label>\r\n              <h1>");
{
    Object var_38 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_38));
}
out.write("</h1>\r\n              <h3>");
{
    Object var_39 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerSubTitle"), "text");
    out.write(renderContext.getObjectModel().toString(var_39));
}
out.write("</h3>\r\n              <div class=\"hero-product__wrapper-image hero-product__on-small-screen\">\r\n                <img");
{
    Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerImage");
    {
        Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "uri");
        {
            boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
            if (var_shoulddisplayattr43) {
                out.write(" src");
                {
                    boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                    if (!var_istrueattr42) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerImgAlt");
    {
        Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
        {
            boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
            if (var_shoulddisplayattr47) {
                out.write(" alt");
                {
                    boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                    if (!var_istrueattr46) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n              </div>\r\n              ");
{
    Object var_testvariable48 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerDesc");
    if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
        out.write("\r\n              <div class=\"hero-product__wrapper-title\">");
        {
            String var_49 = (("\r\n                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "bannerDesc"), "html"))) + "\r\n              ");
            out.write(renderContext.getObjectModel().toString(var_49));
        }
        out.write("</div>\r\n              ");
    }
}
out.write("\r\n              <div class=\"hero-product__wrapper-button\">\r\n                ");
{
    Object var_testvariable50 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonLabel");
    if (renderContext.getObjectModel().toBoolean(var_testvariable50)) {
        out.write("\r\n                  <a");
        {
            Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonlinkURL");
            {
                Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "uri");
                {
                    boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                    if (var_shoulddisplayattr54) {
                        out.write(" href");
                        {
                            boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                            if (!var_istrueattr53) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonTarget");
            {
                Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                {
                    boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                    if (var_shoulddisplayattr58) {
                        out.write(" target");
                        {
                            boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                            if (!var_istrueattr57) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrvalue59 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonNoFollow")) ? "nofollow" : "");
            {
                Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
                {
                    boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
                    if (var_shoulddisplayattr62) {
                        out.write(" rel");
                        {
                            boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                            if (!var_istrueattr61) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrvalue63 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "registerContactCta")) ? "cta" : "linkClick");
            {
                Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
                {
                    boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                    if (var_shoulddisplayattr66) {
                        out.write(" data-tracking-click-event");
                        {
                            boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                            if (!var_istrueattr65) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue67 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "registerContactCta")) ? renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "registerContactClickInfoValue") : renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "defaultClickInfoValue"));
            {
                Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                {
                    boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                    if (var_shoulddisplayattr70) {
                        out.write(" data-tracking-click-info-value");
                        {
                            boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                            if (!var_istrueattr69) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "webInteractionValue");
            {
                Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                {
                    boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                    if (var_shoulddisplayattr74) {
                        out.write(" data-tracking-web-interaction-value");
                        {
                            boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                            if (!var_istrueattr73) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                    <button>\r\n                      <span>");
        {
            Object var_75 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_75));
        }
        out.write("</span>\r\n                      ");
        {
            boolean var_testvariable76 = (!org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "hideButtonArrow"), true));
            if (var_testvariable76) {
                out.write("<img class=\"forward-arrow\"");
                {
                    Object var_attrvalue77 = renderContext.getObjectModel().resolveProperty(_global_specialbannermodel, "buttonLabel");
                    {
                        Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "attribute");
                        {
                            boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
                            if (var_shoulddisplayattr80) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                                    if (!var_istrueattr79) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write(" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>");
            }
        }
        out.write("\r\n                    </button>\r\n                  </a>\r\n                ");
    }
}
out.write("\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n  </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

