/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.socialshare;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class socialshare__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Object _global_hascontent = null;
Collection var_collectionvar15_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.SocialShareInfoModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "socialShareItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div");
    {
        Object var_attrvalue5 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Social Share" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
        {
            Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
            {
                boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                if (var_shoulddisplayattr8) {
                    out.write(" data-panelcontainer");
                    {
                        boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                        if (!var_istrueattr7) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue9 = renderContext.call("i18n", ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Please drag Social Share Item Component" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))), obj().with("i18n", null));
        {
            Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
            {
                boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                if (var_shoulddisplayattr12) {
                    out.write(" data-placeholder-text");
                    {
                        boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                        if (!var_istrueattr11) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(">\r\n    ");
    {
        Object var_testvariable13 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable13)) {
            out.write("\r\n        <div class=\"content-social-share\">\r\n            <label class=\"social-label\">");
            {
                String var_14 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "socialShareTitle"), "text")) + ":");
                out.write(renderContext.getObjectModel().toString(var_14));
            }
            out.write("</label>\r\n            ");
            {
                Object var_collectionvar15 = renderContext.getObjectModel().resolveProperty(_global_model, "socialShareItems");
                {
                    long var_size16 = ((var_collectionvar15_list_coerced$ == null ? (var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15)) : var_collectionvar15_list_coerced$).size());
                    {
                        boolean var_notempty17 = (var_size16 > 0);
                        if (var_notempty17) {
                            {
                                long var_end20 = var_size16;
                                {
                                    boolean var_validstartstepend21 = (((0 < var_size16) && true) && (var_end20 > 0));
                                    if (var_validstartstepend21) {
                                        out.write("<div class=\"social-content\">");
                                        if (var_collectionvar15_list_coerced$ == null) {
                                            var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15);
                                        }
                                        long var_index22 = 0;
                                        for (Object item : var_collectionvar15_list_coerced$) {
                                            {
                                                boolean var_traversal24 = (((var_index22 >= 0) && (var_index22 <= var_end20)) && true);
                                                if (var_traversal24) {
                                                    out.write("\r\n                <a class=\"content-social-share-icon-wrapper\"");
                                                    {
                                                        Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(item, "socialShareLink");
                                                        {
                                                            Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                if (var_shoulddisplayattr28) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                        if (!var_istrueattr27) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(item, "socialShareTarget");
                                                        {
                                                            Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                if (var_shoulddisplayattr32) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                        if (!var_istrueattr31) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue33 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "socialShareNoFollow")) ? "nofollow" : "");
                                                        {
                                                            Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                                if (var_shoulddisplayattr36) {
                                                                    out.write(" rel");
                                                                    {
                                                                        boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                                        if (!var_istrueattr35) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent37 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "socialShareAltText"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(item, "webInteractionValue");
                                                        {
                                                            Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                                if (var_shoulddisplayattr41) {
                                                                    out.write(" data-tracking-web-interaction-value");
                                                                    {
                                                                        boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                        if (!var_istrueattr40) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">\r\n                    <picture>\r\n                        <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue42 = renderContext.getObjectModel().resolveProperty(item, "socialShareIconMobile");
                                                        {
                                                            Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
                                                                if (var_shoulddisplayattr45) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                                                                        if (!var_istrueattr44) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                        <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue46 = renderContext.getObjectModel().resolveProperty(item, "socialShareIconMobile");
                                                        {
                                                            Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (!((Object)false).equals(var_attrvalue46))));
                                                                if (var_shoulddisplayattr49) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr48 = (var_attrvalue46.equals(true));
                                                                        if (!var_istrueattr48) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                        <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(item, "socialShareIcon");
                                                        {
                                                            Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                                                                if (var_shoulddisplayattr53) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                                                                        if (!var_istrueattr52) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                        <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue54 = renderContext.getObjectModel().resolveProperty(item, "socialShareIcon");
                                                        {
                                                            Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                                                                if (var_shoulddisplayattr57) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                                                                        if (!var_istrueattr56) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                        <img class=\"content-social-share-icon\"");
                                                    {
                                                        Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(item, "socialShareAltText");
                                                        {
                                                            Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                                                                if (var_shoulddisplayattr61) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                                                                        if (!var_istrueattr60) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-nimg=\"fixed\" decoding=\"async\"");
                                                    {
                                                        Object var_attrvalue62 = renderContext.getObjectModel().resolveProperty(item, "socialShareIcon");
                                                        {
                                                            Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                                                                if (var_shoulddisplayattr65) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                                                                        if (!var_istrueattr64) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    </picture>\r\n                </a>\r\n            ");
                                                }
                                            }
                                            var_index22++;
                                        }
                                        out.write("</div>");
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar15_list_coerced$ = null;
            }
            out.write("\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

