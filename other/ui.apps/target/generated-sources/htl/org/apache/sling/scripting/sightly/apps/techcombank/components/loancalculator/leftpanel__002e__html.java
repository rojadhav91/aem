/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.loancalculator;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class leftpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_leftpanel = getProperty("leftpanel");
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------

/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
addSubTemplate("leftpanel", new RenderUnit() {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Sub-Template Body -------------------------------------------------------------------------

Object _dynamic_calculator = arguments.get("calculator");
out.write("\r\n    <div class=\"panel-inputs\">\r\n        <div class=\"input-items\">\r\n            <div class=\"item__label\">\r\n                <div class=\"label__text\"");
{
    Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelToolTip");
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" title");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_4 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_4));
}
out.write("<div class=\"icon-info\">\r\n                        ");
{
    Object var_testvariable5 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable5)) {
        out.write("<span");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelToolTip");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" title");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            ");
        {
            Object var_testvariable10 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable10)) {
                out.write("<picture>\r\n                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                        {
                            boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                            if (var_shoulddisplayattr14) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                                    if (!var_istrueattr13) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                        {
                            boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                            if (var_shoulddisplayattr18) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                    if (!var_istrueattr17) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                        {
                            boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                            if (var_shoulddisplayattr22) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                    if (!var_istrueattr21) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                        {
                            boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                            if (var_shoulddisplayattr26) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                    if (!var_istrueattr25) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <img");
                {
                    Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "uri");
                        {
                            boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                            if (var_shoulddisplayattr30) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                    if (!var_istrueattr29) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipAlt");
                    {
                        Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                        {
                            boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                            if (var_shoulddisplayattr34) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                    if (!var_istrueattr33) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                            </picture>");
            }
        }
        out.write("\r\n                        </span>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable35 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable35)) {
        out.write("<div class=\"tooltiptext\">");
        {
            Object var_36 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelToolTip"), "text");
            out.write(renderContext.getObjectModel().toString(var_36));
        }
        out.write("</div>");
    }
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__input-fields\">\r\n                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                    <input aria-invalid=\"false\" type=\"text\" maxlength=\"255\" title=\"\" id=\"bds_value\" name=\"totalPrice\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                    <div class=\"date-time-wrapper__input-extra\">\r\n                        <p class=\"currency__place-holder\">");
{
    Object var_37 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_37));
}
out.write("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__label\">\r\n                <div class=\"label__text\"");
{
    Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountToolTip");
    {
        Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
        {
            boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
            if (var_shoulddisplayattr41) {
                out.write(" title");
                {
                    boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                    if (!var_istrueattr40) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_42 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_42));
}
out.write("<div class=\"icon-info\">\r\n                        ");
{
    Object var_testvariable43 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable43)) {
        out.write("<span");
        {
            Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountToolTip");
            {
                Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                {
                    boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                    if (var_shoulddisplayattr47) {
                        out.write(" title");
                        {
                            boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                            if (!var_istrueattr46) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            ");
        {
            Object var_testvariable48 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
                out.write("<picture>\r\n                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                        {
                            boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                            if (var_shoulddisplayattr52) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                    if (!var_istrueattr51) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
                        {
                            boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                            if (var_shoulddisplayattr56) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                                    if (!var_istrueattr55) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "attribute");
                        {
                            boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                            if (var_shoulddisplayattr60) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                                    if (!var_istrueattr59) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "attribute");
                        {
                            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                            if (var_shoulddisplayattr64) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                    if (!var_istrueattr63) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <img");
                {
                    Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "uri");
                        {
                            boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                            if (var_shoulddisplayattr68) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                                    if (!var_istrueattr67) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipAlt");
                    {
                        Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
                        {
                            boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
                            if (var_shoulddisplayattr72) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                                    if (!var_istrueattr71) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                            </picture>");
            }
        }
        out.write("\r\n                        </span>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable73 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable73)) {
        out.write("<div class=\"tooltiptext\">");
        {
            Object var_74 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountToolTip"), "text");
            out.write(renderContext.getObjectModel().toString(var_74));
        }
        out.write("</div>");
    }
}
out.write("\r\n                    </div>\r\n                </div>\r\n                <div class=\"label__des loan-realestate__des card-article-content-des\">");
{
    String var_75 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanDescription"), "text"))) + "\r\n                ");
    out.write(renderContext.getObjectModel().toString(var_75));
}
out.write("</div>\r\n            </div>\r\n            <div class=\"item__input-fields\">\r\n                <div class=\"tcb-input-range\"");
{
    Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountSlider");
    {
        Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "attribute");
        {
            boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
            if (var_shoulddisplayattr79) {
                out.write(" data-value");
                {
                    boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                    if (!var_istrueattr78) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-min=\"0\" data-max=\"100\" data-use-label=\"true\" data-use-value-popover=\"true\" data-unit=\"%\" disabled></div>\r\n                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                    <input aria-invalid=\"false\" type=\"text\" maxlength=\"255\" id=\"bds_percent\" name=\"selectedPrice\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                    <div class=\"date-time-wrapper__input-extra\">\r\n                        <p class=\"currency__place-holder\">");
{
    Object var_80 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_80));
}
out.write("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__label\">\r\n                <div class=\"label__text\"");
{
    Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermToolTip");
    {
        Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "attribute");
        {
            boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
            if (var_shoulddisplayattr84) {
                out.write(" title");
                {
                    boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                    if (!var_istrueattr83) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_85 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_85));
}
out.write("<div class=\"icon-info\">\r\n                        ");
{
    Object var_testvariable86 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable86)) {
        out.write("<span");
        {
            Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermToolTip");
            {
                Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "attribute");
                {
                    boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                    if (var_shoulddisplayattr90) {
                        out.write(" title");
                        {
                            boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                            if (!var_istrueattr89) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            ");
        {
            Object var_testvariable91 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable91)) {
                out.write("<picture>\r\n                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                        {
                            boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                            if (var_shoulddisplayattr95) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                    if (!var_istrueattr94) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                        {
                            boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                            if (var_shoulddisplayattr99) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                    if (!var_istrueattr98) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                        {
                            boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                            if (var_shoulddisplayattr103) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                    if (!var_istrueattr102) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                        {
                            boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                            if (var_shoulddisplayattr107) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                    if (!var_istrueattr106) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <img");
                {
                    Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "uri");
                        {
                            boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                            if (var_shoulddisplayattr111) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                    if (!var_istrueattr110) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipAlt");
                    {
                        Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                        {
                            boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                            if (var_shoulddisplayattr115) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                    if (!var_istrueattr114) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                            </picture>");
            }
        }
        out.write("\r\n                        </span>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable116 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable116)) {
        out.write("<div class=\"tooltiptext\">");
        {
            String var_117 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermToolTip"), "text"))) + "\r\n                        ");
            out.write(renderContext.getObjectModel().toString(var_117));
        }
        out.write("</div>");
    }
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__input-fields\">\r\n                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                    <input name=\"loanMonth\" aria-invalid=\"false\" type=\"text\" maxlength=\"3\" max=\"240\" class=\"date-time-wrapper__input month__input-field\" value=\"\"/>\r\n                    <div class=\"date-time-wrapper__input-extra\">\r\n                        <p class=\"currency__place-holder\">");
{
    Object var_118 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_118));
}
out.write("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__label\">\r\n                <div class=\"label__text\"");
{
    Object var_attrvalue119 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateToolTip");
    {
        Object var_attrcontent120 = renderContext.call("xss", var_attrvalue119, "attribute");
        {
            boolean var_shoulddisplayattr122 = (((null != var_attrcontent120) && (!"".equals(var_attrcontent120))) && ((!"".equals(var_attrvalue119)) && (!((Object)false).equals(var_attrvalue119))));
            if (var_shoulddisplayattr122) {
                out.write(" title");
                {
                    boolean var_istrueattr121 = (var_attrvalue119.equals(true));
                    if (!var_istrueattr121) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent120));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_123 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_123));
}
out.write("<div class=\"icon-info\">\r\n                        ");
{
    Object var_testvariable124 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable124)) {
        out.write("<span");
        {
            Object var_attrvalue125 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateToolTip");
            {
                Object var_attrcontent126 = renderContext.call("xss", var_attrvalue125, "attribute");
                {
                    boolean var_shoulddisplayattr128 = (((null != var_attrcontent126) && (!"".equals(var_attrcontent126))) && ((!"".equals(var_attrvalue125)) && (!((Object)false).equals(var_attrvalue125))));
                    if (var_shoulddisplayattr128) {
                        out.write(" title");
                        {
                            boolean var_istrueattr127 = (var_attrvalue125.equals(true));
                            if (!var_istrueattr127) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent126));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            ");
        {
            Object var_testvariable129 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable129)) {
                out.write("<picture>\r\n                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue130 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent131 = renderContext.call("xss", var_attrvalue130, "attribute");
                        {
                            boolean var_shoulddisplayattr133 = (((null != var_attrcontent131) && (!"".equals(var_attrcontent131))) && ((!"".equals(var_attrvalue130)) && (!((Object)false).equals(var_attrvalue130))));
                            if (var_shoulddisplayattr133) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr132 = (var_attrvalue130.equals(true));
                                    if (!var_istrueattr132) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent131));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue134 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent135 = renderContext.call("xss", var_attrvalue134, "attribute");
                        {
                            boolean var_shoulddisplayattr137 = (((null != var_attrcontent135) && (!"".equals(var_attrcontent135))) && ((!"".equals(var_attrvalue134)) && (!((Object)false).equals(var_attrvalue134))));
                            if (var_shoulddisplayattr137) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr136 = (var_attrvalue134.equals(true));
                                    if (!var_istrueattr136) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent135));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue138 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent139 = renderContext.call("xss", var_attrvalue138, "attribute");
                        {
                            boolean var_shoulddisplayattr141 = (((null != var_attrcontent139) && (!"".equals(var_attrcontent139))) && ((!"".equals(var_attrvalue138)) && (!((Object)false).equals(var_attrvalue138))));
                            if (var_shoulddisplayattr141) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr140 = (var_attrvalue138.equals(true));
                                    if (!var_istrueattr140) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent139));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue142 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent143 = renderContext.call("xss", var_attrvalue142, "attribute");
                        {
                            boolean var_shoulddisplayattr145 = (((null != var_attrcontent143) && (!"".equals(var_attrcontent143))) && ((!"".equals(var_attrvalue142)) && (!((Object)false).equals(var_attrvalue142))));
                            if (var_shoulddisplayattr145) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr144 = (var_attrvalue142.equals(true));
                                    if (!var_istrueattr144) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent143));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <img");
                {
                    Object var_attrvalue146 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent147 = renderContext.call("xss", var_attrvalue146, "uri");
                        {
                            boolean var_shoulddisplayattr149 = (((null != var_attrcontent147) && (!"".equals(var_attrcontent147))) && ((!"".equals(var_attrvalue146)) && (!((Object)false).equals(var_attrvalue146))));
                            if (var_shoulddisplayattr149) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr148 = (var_attrvalue146.equals(true));
                                    if (!var_istrueattr148) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent147));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue150 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipAlt");
                    {
                        Object var_attrcontent151 = renderContext.call("xss", var_attrvalue150, "attribute");
                        {
                            boolean var_shoulddisplayattr153 = (((null != var_attrcontent151) && (!"".equals(var_attrcontent151))) && ((!"".equals(var_attrvalue150)) && (!((Object)false).equals(var_attrvalue150))));
                            if (var_shoulddisplayattr153) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr152 = (var_attrvalue150.equals(true));
                                    if (!var_istrueattr152) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent151));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                            </picture>");
            }
        }
        out.write("\r\n                        </span>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable154 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable154)) {
        out.write("<div class=\"tooltiptext\">");
        {
            String var_155 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateToolTip"), "text"))) + "\r\n                        ");
            out.write(renderContext.getObjectModel().toString(var_155));
        }
        out.write("</div>");
    }
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__input-fields\">\r\n                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                    <input aria-invalid=\"false\" type=\"text\" id=\"profit\" name=\"interestRate\" class=\"money__input-field percent__input-field\" value=\"\"/>\r\n                    <div class=\"date-time-wrapper__input-extra\">\r\n                        <p class=\"currency__place-holder\">");
{
    Object var_156 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_156));
}
out.write("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__label\">\r\n                <div class=\"label__text\"");
{
    Object var_attrvalue157 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateToolTip");
    {
        Object var_attrcontent158 = renderContext.call("xss", var_attrvalue157, "attribute");
        {
            boolean var_shoulddisplayattr160 = (((null != var_attrcontent158) && (!"".equals(var_attrcontent158))) && ((!"".equals(var_attrvalue157)) && (!((Object)false).equals(var_attrvalue157))));
            if (var_shoulddisplayattr160) {
                out.write(" title");
                {
                    boolean var_istrueattr159 = (var_attrvalue157.equals(true));
                    if (!var_istrueattr159) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent158));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_161 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_161));
}
out.write("<div class=\"icon-info\">\r\n                        ");
{
    Object var_testvariable162 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable162)) {
        out.write("<span");
        {
            Object var_attrvalue163 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateToolTip");
            {
                Object var_attrcontent164 = renderContext.call("xss", var_attrvalue163, "attribute");
                {
                    boolean var_shoulddisplayattr166 = (((null != var_attrcontent164) && (!"".equals(var_attrcontent164))) && ((!"".equals(var_attrvalue163)) && (!((Object)false).equals(var_attrvalue163))));
                    if (var_shoulddisplayattr166) {
                        out.write(" title");
                        {
                            boolean var_istrueattr165 = (var_attrvalue163.equals(true));
                            if (!var_istrueattr165) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent164));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                            ");
        {
            Object var_testvariable167 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable167)) {
                out.write("<picture>\r\n                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue168 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent169 = renderContext.call("xss", var_attrvalue168, "attribute");
                        {
                            boolean var_shoulddisplayattr171 = (((null != var_attrcontent169) && (!"".equals(var_attrcontent169))) && ((!"".equals(var_attrvalue168)) && (!((Object)false).equals(var_attrvalue168))));
                            if (var_shoulddisplayattr171) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr170 = (var_attrvalue168.equals(true));
                                    if (!var_istrueattr170) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent169));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue172 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIconMobile");
                    {
                        Object var_attrcontent173 = renderContext.call("xss", var_attrvalue172, "attribute");
                        {
                            boolean var_shoulddisplayattr175 = (((null != var_attrcontent173) && (!"".equals(var_attrcontent173))) && ((!"".equals(var_attrvalue172)) && (!((Object)false).equals(var_attrvalue172))));
                            if (var_shoulddisplayattr175) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr174 = (var_attrvalue172.equals(true));
                                    if (!var_istrueattr174) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent173));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue176 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent177 = renderContext.call("xss", var_attrvalue176, "attribute");
                        {
                            boolean var_shoulddisplayattr179 = (((null != var_attrcontent177) && (!"".equals(var_attrcontent177))) && ((!"".equals(var_attrvalue176)) && (!((Object)false).equals(var_attrvalue176))));
                            if (var_shoulddisplayattr179) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr178 = (var_attrvalue176.equals(true));
                                    if (!var_istrueattr178) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent177));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue180 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent181 = renderContext.call("xss", var_attrvalue180, "attribute");
                        {
                            boolean var_shoulddisplayattr183 = (((null != var_attrcontent181) && (!"".equals(var_attrcontent181))) && ((!"".equals(var_attrvalue180)) && (!((Object)false).equals(var_attrvalue180))));
                            if (var_shoulddisplayattr183) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr182 = (var_attrvalue180.equals(true));
                                    if (!var_istrueattr182) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent181));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                <img");
                {
                    Object var_attrvalue184 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipIcon");
                    {
                        Object var_attrcontent185 = renderContext.call("xss", var_attrvalue184, "uri");
                        {
                            boolean var_shoulddisplayattr187 = (((null != var_attrcontent185) && (!"".equals(var_attrcontent185))) && ((!"".equals(var_attrvalue184)) && (!((Object)false).equals(var_attrvalue184))));
                            if (var_shoulddisplayattr187) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr186 = (var_attrvalue184.equals(true));
                                    if (!var_istrueattr186) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent185));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue188 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "tooltipAlt");
                    {
                        Object var_attrcontent189 = renderContext.call("xss", var_attrvalue188, "attribute");
                        {
                            boolean var_shoulddisplayattr191 = (((null != var_attrcontent189) && (!"".equals(var_attrcontent189))) && ((!"".equals(var_attrvalue188)) && (!((Object)false).equals(var_attrvalue188))));
                            if (var_shoulddisplayattr191) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr190 = (var_attrvalue188.equals(true));
                                    if (!var_istrueattr190) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent189));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                            </picture>");
            }
        }
        out.write("\r\n                        </span>");
    }
}
out.write("\r\n                        ");
{
    Object var_testvariable192 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable192)) {
        out.write("<div class=\"tooltiptext\">");
        {
            String var_193 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateToolTip"), "text"))) + "\r\n                        ");
            out.write(renderContext.getObjectModel().toString(var_193));
        }
        out.write("</div>");
    }
}
out.write("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"item__input-fields\">\r\n                <div class=\"input-field__currency-field calendar_real_estate loan-realestate__input\" inputmode=\"numeric\" data-acceptAllDate=\"true\">\r\n                    <input name=\"startDate\" aria-invalid=\"false\" type=\"text\" readonly class=\"date-time-wrapper__input calendar__input-field\"");
{
    Object var_attrvalue194 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDatePicker");
    {
        Object var_attrcontent195 = renderContext.call("xss", var_attrvalue194, "attribute");
        {
            boolean var_shoulddisplayattr197 = (((null != var_attrcontent195) && (!"".equals(var_attrcontent195))) && ((!"".equals(var_attrvalue194)) && (!((Object)false).equals(var_attrvalue194))));
            if (var_shoulddisplayattr197) {
                out.write(" value");
                {
                    boolean var_istrueattr196 = (var_attrvalue194.equals(true));
                    if (!var_istrueattr196) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent195));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                    <div class=\"date-time-wrapper__input-extra\">\r\n                        <p class=\"currency__place-holder loan-realestate__icon monthLabel\"");
{
    Object var_attrvalue198 = renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "decreasingMonthTextLoanTerm");
    {
        Object var_attrcontent199 = renderContext.call("xss", var_attrvalue198, "attribute");
        {
            boolean var_shoulddisplayattr201 = (((null != var_attrcontent199) && (!"".equals(var_attrcontent199))) && ((!"".equals(var_attrvalue198)) && (!((Object)false).equals(var_attrvalue198))));
            if (var_shoulddisplayattr201) {
                out.write(" data-month-label");
                {
                    boolean var_istrueattr200 = (var_attrvalue198.equals(true));
                    if (!var_istrueattr200) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent199));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                            <img class=\"material-symbols-outlined loan-realestate__icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar.svg\"/>\r\n                        </p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"calendar-popup\">\r\n                    <div class=\"month-line\">\r\n                        <span class=\"month-data\">");
{
    Object var_202 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "months"), "text");
    out.write(renderContext.getObjectModel().toString(var_202));
}
out.write("</span>\r\n                        <div>\r\n                            <img class=\"prev-month nav-action\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-left-icon.svg\"/>\r\n                            <img class=\"next-month nav-action\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar-right-icon.svg\"/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"date-line\"></div>\r\n                    <div class=\"day-tbl\">\r\n                        <div class=\"week line1\"></div>\r\n                        <div class=\"week line2\"></div>\r\n                        <div class=\"week line3\"></div>\r\n                        <div class=\"week line4\"></div>\r\n                        <div class=\"week line5\"></div>\r\n                        <div class=\"week line6\"></div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");


// End Of Main Sub-Template Body ------------------------------------------------------------------
    }



    {
//Sub-Sub-Templates Initialization ----------------------------------------------------------------



//End of Sub-Sub-Templates Initialization ---------------------------------------------------------
    }
    
});


//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

