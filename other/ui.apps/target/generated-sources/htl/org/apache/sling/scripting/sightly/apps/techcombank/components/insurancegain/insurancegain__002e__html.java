/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.insurancegain;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class insurancegain__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_insurance = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n\r\n");
_global_insurance = renderContext.call("use", com.techcombank.core.models.InsuranceGain.class.getName(), obj());
out.write("\r\n    <section class=\"container \">\r\n        <div class=\"insurance-gain\"");
{
    Object var_attrvalue5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "genericList"), "scriptComment");
    {
        boolean var_shoulddisplayattr8 = ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5)));
        if (var_shoulddisplayattr8) {
            out.write(" data-insurance-gain");
            {
                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                if (!var_istrueattr7) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue5));
                    out.write("\"");
                }
            }
        }
    }
}
{
    Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_insurance, "currencyText");
    {
        Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
        {
            boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
            if (var_shoulddisplayattr12) {
                out.write(" data-currency-input");
                {
                    boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                    if (!var_istrueattr11) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_insurance, "errorDate");
    {
        Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
        {
            boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
            if (var_shoulddisplayattr16) {
                out.write(" data-invalid-date");
                {
                    boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                    if (!var_istrueattr15) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_insurance, "errorAge");
    {
        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
        {
            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
            if (var_shoulddisplayattr20) {
                out.write(" data-invalid-age");
                {
                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                    if (!var_istrueattr19) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n            <div class=\"insurance-gain__container\">\r\n                <div class=\"insurance-gain__panel\">\r\n                    <div class=\"panel-inputs\">\r\n                        <div class=\"input-items\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent21 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent21));
    out.write("\"");
}
out.write(" data-tracking-click-info-value=\"{'calculatorName':'Insurance Gain' ,'calculatorFields': ''}\">\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\">");
{
    Object var_22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "dob"), "text");
    out.write(renderContext.getObjectModel().toString(var_22));
}
out.write("</div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"input-fields__date-time-wrapper\">\r\n                                    <div class=\"date-time-wrapper__input-field\" inputmode=\"numeric\">\r\n                                        <input aria-invalid=\"false\" name=\"day\" placeholder=\"DD\" type=\"text\" maxlength=\"2\" class=\"date-time-wrapper__input\" value=\"\"/>\r\n                                    </div>\r\n                                    <span class=\"date-time-wrapper__separator\">-</span>\r\n                                    <div class=\"date-time-wrapper__input-field\" inputmode=\"numeric\">\r\n                                        <input aria-invalid=\"false\" name=\"month\" placeholder=\"MM\" type=\"text\" maxlength=\"2\" class=\"date-time-wrapper__input\" value=\"\"/>\r\n                                    </div>\r\n                                    <span class=\"date-time-wrapper__separator\">-</span>\r\n                                    <div class=\"date-time-wrapper__input-field\" inputmode=\"numeric\">\r\n                                        <input aria-invalid=\"false\" name=\"year\" placeholder=\"YYYY\" type=\"text\" maxlength=\"4\" class=\"date-time-wrapper__input\" value=\"\"/>\r\n                                    </div>\r\n                                </div>\r\n                                <p class=\"input-fields__error-msg\"></p>\r\n                            </div>\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\">");
{
    Object var_23 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "genderLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_23));
}
out.write("</div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"input-fields__drop-down\">\r\n                                    <div class=\"drop-down__container\">\r\n                                        <div class=\"drop-down__controls\">\r\n                                            <div class=\"drop-down__select\">");
{
    Object var_24 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "male"), "text");
    out.write(renderContext.getObjectModel().toString(var_24));
}
out.write("</div>\r\n                                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                                        </div>\r\n                                        <div class=\"select-option\">\r\n                                            <div class=\"option gender selected\" value=\"0\">");
{
    Object var_25 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "male"), "text");
    out.write(renderContext.getObjectModel().toString(var_25));
}
out.write("</div>\r\n                                            <div class=\"option gender\" value=\"1\">");
{
    Object var_26 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "female"), "text");
    out.write(renderContext.getObjectModel().toString(var_26));
}
out.write("</div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\">");
{
    String var_27 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "amount"), "text"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_27));
}
out.write("<div class=\"icon-info\">\r\n                                        ");
{
    Object var_testvariable28 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIcon");
    if (renderContext.getObjectModel().toBoolean(var_testvariable28)) {
        out.write("<span class=\"icon\">\r\n                                            <picture>\r\n                                                <source media=\"(min-width:768px)\"");
        {
            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIconMobile");
            {
                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                {
                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                    if (var_shoulddisplayattr32) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                            if (!var_istrueattr31) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <source media=\"(min-width:280px)\"");
        {
            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIconMobile");
            {
                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                {
                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                    if (var_shoulddisplayattr36) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                            if (!var_istrueattr35) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIconMobile");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIconMobile");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIcon");
            {
                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                {
                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                    if (var_shoulddisplayattr48) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                            if (!var_istrueattr47) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIcon");
            {
                Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                {
                    boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                    if (var_shoulddisplayattr52) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                            if (!var_istrueattr51) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                                <img");
        {
            Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipAltText");
            {
                Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
                {
                    boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                    if (var_shoulddisplayattr56) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                            if (!var_istrueattr55) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipIcon");
            {
                Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "uri");
                {
                    boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                    if (var_shoulddisplayattr60) {
                        out.write(" src");
                        {
                            boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                            if (!var_istrueattr59) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                                            </picture>\r\n                                        </span>");
    }
}
out.write("\r\n                                        <div class=\"tooltiptext\">");
{
    Object var_61 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "toolTipAltText"), "text");
    out.write(renderContext.getObjectModel().toString(var_61));
}
out.write("</div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"input-field__currency-field currency-suffix\" inputmode=\"numeric\">\r\n                                    ");
{
    boolean var_testvariable62 = (((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "type"), "legacy")) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "type"), "quyen"))) || (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "type"), "superLink")));
    if (var_testvariable62) {
        out.write("\r\n                                        <input aria-invalid=\"false\" name=\"moneyValue\" maxlength=\"25\" type=\"text\" id=\"insuranceSL\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                                    ");
    }
}
out.write("\r\n                                    ");
{
    boolean var_testvariable63 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "type"), "anGia"));
    if (var_testvariable63) {
        out.write("\r\n                                        <input aria-invalid=\"false\" name=\"moneyValue\" maxlength=\"25\" type=\"text\" id=\"insuranceAG\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                                    ");
    }
}
out.write("\r\n                                    ");
{
    boolean var_testvariable64 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "type"), "bach"));
    if (var_testvariable64) {
        out.write("\r\n                                        <input aria-invalid=\"false\" name=\"moneyValue\" maxlength=\"25\" type=\"text\" id=\"insuranceBL\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                                    ");
    }
}
out.write("\r\n                                    <div class=\"date-time-wrapper__input-extra\">\r\n                                        <p class=\"currency__place-holder\">");
{
    Object var_65 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "amountInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_65));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"panel-info\">\r\n                        <span>\r\n                            ");
{
    Object var_testvariable66 = renderContext.getObjectModel().resolveProperty(_global_insurance, "background");
    if (renderContext.getObjectModel().toBoolean(var_testvariable66)) {
        out.write("<picture>\r\n                                <source media=\"(min-width:768px)\"");
        {
            Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(_global_insurance, "backgroundMobile");
            {
                Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                {
                    boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                    if (var_shoulddisplayattr70) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                            if (!var_istrueattr69) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(min-width:280px)\"");
        {
            Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(_global_insurance, "backgroundMobile");
            {
                Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                {
                    boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                    if (var_shoulddisplayattr74) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                            if (!var_istrueattr73) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(_global_insurance, "backgroundMobile");
            {
                Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "attribute");
                {
                    boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                    if (var_shoulddisplayattr78) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                            if (!var_istrueattr77) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(_global_insurance, "backgroundMobile");
            {
                Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
                {
                    boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
                    if (var_shoulddisplayattr82) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                            if (!var_istrueattr81) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(_global_insurance, "background");
            {
                Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
                {
                    boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                    if (var_shoulddisplayattr86) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                            if (!var_istrueattr85) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(_global_insurance, "background");
            {
                Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "attribute");
                {
                    boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                    if (var_shoulddisplayattr90) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                            if (!var_istrueattr89) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                <img");
        {
            Object var_attrvalue91 = renderContext.getObjectModel().resolveProperty(_global_insurance, "alt");
            {
                Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
                {
                    boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                    if (var_shoulddisplayattr94) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                            if (!var_istrueattr93) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"InsuranceGainPanel_background__v_Ipd\"");
        {
            Object var_attrvalue95 = renderContext.getObjectModel().resolveProperty(_global_insurance, "background");
            {
                Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "uri");
                {
                    boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
                    if (var_shoulddisplayattr98) {
                        out.write(" src");
                        {
                            boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                            if (!var_istrueattr97) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" sizes=\"100vw\"/>\r\n                            </picture>");
    }
}
out.write("\r\n                        </span>\r\n                        <div class=\"panel-info__content\">\r\n                            <div class=\"panel-info__content-text\">\r\n                                <div class=\"info-content-text__icon\">\r\n                                    <span>\r\n                                        ");
{
    Object var_testvariable99 = renderContext.getObjectModel().resolveProperty(_global_insurance, "icon");
    if (renderContext.getObjectModel().toBoolean(var_testvariable99)) {
        out.write("<picture>\r\n                                            <source media=\"(min-width:768px)\"");
        {
            Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(_global_insurance, "iconMobile");
            {
                Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                {
                    boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                    if (var_shoulddisplayattr103) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                            if (!var_istrueattr102) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width:280px)\"");
        {
            Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(_global_insurance, "iconMobile");
            {
                Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                {
                    boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                    if (var_shoulddisplayattr107) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                            if (!var_istrueattr106) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(_global_insurance, "iconMobile");
            {
                Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                {
                    boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                    if (var_shoulddisplayattr111) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                            if (!var_istrueattr110) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(_global_insurance, "iconMobile");
            {
                Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                {
                    boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                    if (var_shoulddisplayattr115) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                            if (!var_istrueattr114) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue116 = renderContext.getObjectModel().resolveProperty(_global_insurance, "icon");
            {
                Object var_attrcontent117 = renderContext.call("xss", var_attrvalue116, "attribute");
                {
                    boolean var_shoulddisplayattr119 = (((null != var_attrcontent117) && (!"".equals(var_attrcontent117))) && ((!"".equals(var_attrvalue116)) && (!((Object)false).equals(var_attrvalue116))));
                    if (var_shoulddisplayattr119) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr118 = (var_attrvalue116.equals(true));
                            if (!var_istrueattr118) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(_global_insurance, "icon");
            {
                Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "attribute");
                {
                    boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                    if (var_shoulddisplayattr123) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                            if (!var_istrueattr122) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <img");
        {
            Object var_attrvalue124 = renderContext.getObjectModel().resolveProperty(_global_insurance, "altIcon");
            {
                Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "attribute");
                {
                    boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
                    if (var_shoulddisplayattr127) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                            if (!var_istrueattr126) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue128 = renderContext.getObjectModel().resolveProperty(_global_insurance, "icon");
            {
                Object var_attrcontent129 = renderContext.call("xss", var_attrvalue128, "uri");
                {
                    boolean var_shoulddisplayattr131 = (((null != var_attrcontent129) && (!"".equals(var_attrcontent129))) && ((!"".equals(var_attrvalue128)) && (!((Object)false).equals(var_attrvalue128))));
                    if (var_shoulddisplayattr131) {
                        out.write(" src");
                        {
                            boolean var_istrueattr130 = (var_attrvalue128.equals(true));
                            if (!var_istrueattr130) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent129));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\"/>\r\n                                        </picture>");
    }
}
out.write("\r\n                                    </span>\r\n                                </div>\r\n                                <div class=\"info-content-text__label\">\r\n                                    <p>");
{
    Object var_132 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "interest"), "text");
    out.write(renderContext.getObjectModel().toString(var_132));
}
out.write("</p>\r\n                                    <h3 class=\"insurance-fee\">");
{
    String var_133 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "currencyText"), "text")));
    out.write(renderContext.getObjectModel().toString(var_133));
}
out.write("</h3>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"panel-info__content-button\">\r\n                                <div class=\"cta-button--light\">\r\n                                    <a class=\"cta-button\"");
{
    Object var_attrvalue134 = renderContext.getObjectModel().resolveProperty(_global_insurance, "link");
    {
        Object var_attrcontent135 = renderContext.call("xss", var_attrvalue134, "uri");
        {
            boolean var_shoulddisplayattr137 = (((null != var_attrcontent135) && (!"".equals(var_attrcontent135))) && ((!"".equals(var_attrvalue134)) && (!((Object)false).equals(var_attrvalue134))));
            if (var_shoulddisplayattr137) {
                out.write(" href");
                {
                    boolean var_istrueattr136 = (var_attrvalue134.equals(true));
                    if (!var_istrueattr136) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent135));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue138 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "openInNewTab"), "true")) ? "_blank" : "_self");
    {
        Object var_attrcontent139 = renderContext.call("xss", var_attrvalue138, "attribute");
        {
            boolean var_shoulddisplayattr141 = (((null != var_attrcontent139) && (!"".equals(var_attrcontent139))) && ((!"".equals(var_attrvalue138)) && (!((Object)false).equals(var_attrvalue138))));
            if (var_shoulddisplayattr141) {
                out.write(" target");
                {
                    boolean var_istrueattr140 = (var_attrvalue138.equals(true));
                    if (!var_istrueattr140) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent139));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrvalue142 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "nofollow"), "true")) ? "noffolow" : "");
    {
        Object var_attrcontent143 = renderContext.call("xss", var_attrvalue142, "attribute");
        {
            boolean var_shoulddisplayattr145 = (((null != var_attrcontent143) && (!"".equals(var_attrcontent143))) && ((!"".equals(var_attrvalue142)) && (!((Object)false).equals(var_attrvalue142))));
            if (var_shoulddisplayattr145) {
                out.write(" rel");
                {
                    boolean var_istrueattr144 = (var_attrvalue142.equals(true));
                    if (!var_istrueattr144) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent143));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-tracking-click-event=\"contactUs\"");
{
    String var_attrcontent146 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "interationType"), "attribute"))) + "'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent146));
    out.write("\"");
}
{
    String var_attrcontent147 = (((("{'" + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_insurance, "cta"), "true")) ? "contactUs" : "linkclick"), "attribute"))) + "':'") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "button"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent147));
    out.write("\"");
}
out.write(">\r\n\r\n                                        <span class=\"cmp-button__text\">");
{
    Object var_148 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_insurance, "button"), "text");
    out.write(renderContext.getObjectModel().toString(var_148));
}
out.write("</span>\r\n                                        ");
{
    Object var_testvariable149 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable149)) {
        out.write("<picture>\r\n                                            <source media=\"(min-width:768px)\"");
        {
            Object var_attrvalue150 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImageMobile");
            {
                Object var_attrcontent151 = renderContext.call("xss", var_attrvalue150, "attribute");
                {
                    boolean var_shoulddisplayattr153 = (((null != var_attrcontent151) && (!"".equals(var_attrcontent151))) && ((!"".equals(var_attrvalue150)) && (!((Object)false).equals(var_attrvalue150))));
                    if (var_shoulddisplayattr153) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr152 = (var_attrvalue150.equals(true));
                            if (!var_istrueattr152) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent151));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width:280px)\"");
        {
            Object var_attrvalue154 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImageMobile");
            {
                Object var_attrcontent155 = renderContext.call("xss", var_attrvalue154, "attribute");
                {
                    boolean var_shoulddisplayattr157 = (((null != var_attrcontent155) && (!"".equals(var_attrcontent155))) && ((!"".equals(var_attrvalue154)) && (!((Object)false).equals(var_attrvalue154))));
                    if (var_shoulddisplayattr157) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr156 = (var_attrvalue154.equals(true));
                            if (!var_istrueattr156) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent155));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue158 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImageMobile");
            {
                Object var_attrcontent159 = renderContext.call("xss", var_attrvalue158, "attribute");
                {
                    boolean var_shoulddisplayattr161 = (((null != var_attrcontent159) && (!"".equals(var_attrcontent159))) && ((!"".equals(var_attrvalue158)) && (!((Object)false).equals(var_attrvalue158))));
                    if (var_shoulddisplayattr161) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr160 = (var_attrvalue158.equals(true));
                            if (!var_istrueattr160) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent159));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue162 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImageMobile");
            {
                Object var_attrcontent163 = renderContext.call("xss", var_attrvalue162, "attribute");
                {
                    boolean var_shoulddisplayattr165 = (((null != var_attrcontent163) && (!"".equals(var_attrcontent163))) && ((!"".equals(var_attrvalue162)) && (!((Object)false).equals(var_attrvalue162))));
                    if (var_shoulddisplayattr165) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr164 = (var_attrvalue162.equals(true));
                            if (!var_istrueattr164) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent163));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue166 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImage");
            {
                Object var_attrcontent167 = renderContext.call("xss", var_attrvalue166, "attribute");
                {
                    boolean var_shoulddisplayattr169 = (((null != var_attrcontent167) && (!"".equals(var_attrcontent167))) && ((!"".equals(var_attrvalue166)) && (!((Object)false).equals(var_attrvalue166))));
                    if (var_shoulddisplayattr169) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr168 = (var_attrvalue166.equals(true));
                            if (!var_istrueattr168) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent167));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue170 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImage");
            {
                Object var_attrcontent171 = renderContext.call("xss", var_attrvalue170, "attribute");
                {
                    boolean var_shoulddisplayattr173 = (((null != var_attrcontent171) && (!"".equals(var_attrcontent171))) && ((!"".equals(var_attrvalue170)) && (!((Object)false).equals(var_attrvalue170))));
                    if (var_shoulddisplayattr173) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr172 = (var_attrvalue170.equals(true));
                            if (!var_istrueattr172) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent171));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                                            <img");
        {
            Object var_attrvalue174 = renderContext.getObjectModel().resolveProperty(_global_insurance, "altIcon");
            {
                Object var_attrcontent175 = renderContext.call("xss", var_attrvalue174, "attribute");
                {
                    boolean var_shoulddisplayattr177 = (((null != var_attrcontent175) && (!"".equals(var_attrcontent175))) && ((!"".equals(var_attrvalue174)) && (!((Object)false).equals(var_attrvalue174))));
                    if (var_shoulddisplayattr177) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr176 = (var_attrvalue174.equals(true));
                            if (!var_istrueattr176) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent175));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue178 = renderContext.getObjectModel().resolveProperty(_global_insurance, "buttonImage");
            {
                Object var_attrcontent179 = renderContext.call("xss", var_attrvalue178, "uri");
                {
                    boolean var_shoulddisplayattr181 = (((null != var_attrcontent179) && (!"".equals(var_attrcontent179))) && ((!"".equals(var_attrvalue178)) && (!((Object)false).equals(var_attrvalue178))));
                    if (var_shoulddisplayattr181) {
                        out.write(" src");
                        {
                            boolean var_istrueattr180 = (var_attrvalue178.equals(true));
                            if (!var_istrueattr180) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent179));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" class=\"cmp-button__icon\"/>\r\n                                        </picture>");
    }
}
out.write("\r\n                                    </a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

