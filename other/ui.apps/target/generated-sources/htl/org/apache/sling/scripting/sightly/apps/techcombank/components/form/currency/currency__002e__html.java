/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.currency;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class currency__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_properties = bindings.get("properties");
Object _global_editmode = null;
Object _dynamic_component = bindings.get("component");
Object _global_text = null;
_global_editmode = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr"), "title") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
if (renderContext.getObjectModel().toBoolean(_global_editmode)) {
    out.write("\r\n    <p class=\"cq-placeholder\"");
    {
        Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
        {
            Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
            {
                boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
                if (var_shoulddisplayattr3) {
                    out.write(" data-emptytext");
                    {
                        boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                        if (!var_istrueattr2) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write("></p>\r\n");
}
out.write("\r\n");
{
    boolean var_testvariable4 = (!renderContext.getObjectModel().toBoolean(_global_editmode));
    if (var_testvariable4) {
        out.write("\r\n    ");
_global_text = renderContext.call("use", com.adobe.cq.wcm.core.components.models.form.Text.class.getName(), obj());
        out.write("<div");
        {
            String var_attrcontent5 = ("cmp-form-text " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "analytics")) ? "analytics-form-field" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent5));
            out.write("\"");
        }
        out.write(">\r\n        <label");
        {
            Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_text, "id");
            {
                Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                {
                    boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                    if (var_shoulddisplayattr9) {
                        out.write(" for");
                        {
                            boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                            if (!var_istrueattr8) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">");
        {
            String var_10 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_text, "title"), "text")) + " ");
            out.write(renderContext.getObjectModel().toString(var_10));
        }
        out.write("<span>");
        {
            Object var_testvariable11 = renderContext.getObjectModel().resolveProperty(_global_text, "required");
            if (renderContext.getObjectModel().toBoolean(var_testvariable11)) {
                out.write(" *");
            }
        }
        out.write("</span></label>\r\n        <input");
        {
            Object var_attrvalue12 = renderContext.call("join", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_text, "helpMessage")) ? (new Object[] {renderContext.getObjectModel().resolveProperty(_global_text, "id"), "helpMessage"}) : ""), "-");
            {
                Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                {
                    boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                    if (var_shoulddisplayattr15) {
                        out.write(" aria-describedby");
                        {
                            boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                            if (!var_istrueattr14) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" data-type=\"currency\"");
        {
            Object var_attrvalue16 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "required")) ? renderContext.getObjectModel().resolveProperty(_global_text, "requiredMessage") : "");
            {
                Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                {
                    boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                    if (var_shoulddisplayattr19) {
                        out.write(" data-required");
                        {
                            boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                            if (!var_istrueattr18) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cmp-form-text__text\" data-cmp-hook-form-text=\"input\"");
        {
            Object var_attrvalue20 = renderContext.getObjectModel().resolveProperty(_global_text, "id");
            {
                Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
                {
                    boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (!((Object)false).equals(var_attrvalue20))));
                    if (var_shoulddisplayattr23) {
                        out.write(" id");
                        {
                            boolean var_istrueattr22 = (var_attrvalue20.equals(true));
                            if (!var_istrueattr22) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(_global_text, "name");
            {
                Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                {
                    boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                    if (var_shoulddisplayattr27) {
                        out.write(" name");
                        {
                            boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                            if (!var_istrueattr26) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue28 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_properties, "usePlaceholder")) ? renderContext.getObjectModel().resolveProperty(_global_text, "helpMessage") : "");
            {
                Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                {
                    boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                    if (var_shoulddisplayattr31) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                            if (!var_istrueattr30) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"currency\"/>\r\n    </div>\r\n");
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

