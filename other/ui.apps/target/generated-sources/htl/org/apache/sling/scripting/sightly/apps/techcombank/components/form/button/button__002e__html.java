/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.button;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class button__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _dynamic_properties = bindings.get("properties");
Object _global_hascontent = null;
Object _global_button = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "jcr:title");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n");
_global_button = renderContext.call("use", com.adobe.cq.wcm.core.components.models.form.Button.class.getName(), obj());
    out.write("<button");
    {
        Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_global_button, "id");
        {
            Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
            {
                boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
                if (var_shoulddisplayattr3) {
                    out.write(" id");
                    {
                        boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                        if (!var_istrueattr2) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(" class=\"cmp-form-button\"");
    {
        Object var_attrvalue4 = renderContext.getObjectModel().resolveProperty(_global_button, "name");
        {
            Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
            {
                boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
                if (var_shoulddisplayattr7) {
                    out.write(" name");
                    {
                        boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                        if (!var_istrueattr6) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue8 = renderContext.getObjectModel().resolveProperty(_global_button, "value");
        {
            Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
            {
                boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
                if (var_shoulddisplayattr11) {
                    out.write(" value");
                    {
                        boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                        if (!var_istrueattr10) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(">");
    {
        String var_12 = (("\r\n        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_button, "title"), "text"))) + "\r\n        ");
        out.write(renderContext.getObjectModel().toString(var_12));
    }
    {
        Object var_testvariable13 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "showIcon");
        if (renderContext.getObjectModel().toBoolean(var_testvariable13)) {
            out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\" alt=\"\" class=\"cmp-form-button__icon\" aria-hidden=\"true\"/>");
        }
    }
    out.write("\r\n</button>\r\n");
}
out.write("\r\n");
{
    Object var_templatevar14 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions15_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions15 = obj().with("isEmpty", var_templateoptions15_field$_isempty);
            callUnit(out, renderContext, var_templatevar14, var_templateoptions15);
        }
    }
}
out.write("\r\n\r\n\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

