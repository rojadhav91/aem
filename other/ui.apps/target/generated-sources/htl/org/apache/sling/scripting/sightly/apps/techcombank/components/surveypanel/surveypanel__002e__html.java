/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.surveypanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class surveypanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_surveypanel = null;
Object _global_template = null;
Object _global_hascontent = null;
_global_surveypanel = renderContext.call("use", com.techcombank.core.models.SurveyPanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = _global_surveypanel;
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n      ");
            {
                Object var_testvariable1 = renderContext.getObjectModel().resolveProperty(_global_surveypanel, "setWebInteractionType");
                if (renderContext.getObjectModel().toBoolean(var_testvariable1)) {
                    out.write("<div class=\"survey-panel-data-container\"");
                    {
                        Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_surveypanel, "getJson");
                        {
                            Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                            {
                                boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                                if (var_shoulddisplayattr5) {
                                    out.write(" surveyPanel");
                                    {
                                        boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                        if (!var_istrueattr4) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("></div>");
                }
            }
            out.write("\r\n        <div class=\"survey-panel survey-panel__container\">\r\n          <!-- Question and Answer element-->\r\n          <div class=\"survey-panel__QA\">\r\n            <!-- Panel Question -->\r\n            <div class=\"survey-panel__question-wrapper\">\r\n              <div class=\"survey-panel__question\">\r\n                <label class=\"survey-panel__question-label\"></label>\r\n                <h4 class=\"survey-panel__question-title\"></h4>\r\n                <div class=\"question-radio-container\"></div>\r\n      \r\n                <!-- Button submit Survey -->\r\n                <div class=\"survey-panel__button-wrapper btn-next-question\">\r\n                  <a href=\"#\" class=\"survey-panel__question-button\">\r\n                    <span class=\"button-text\">");
            {
                Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_surveypanel, "nextCta"), "text");
                out.write(renderContext.getObjectModel().toString(var_6));
            }
            out.write("</span>\r\n                    <div class=\"button-icon\">\r\n                      <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                    </div>\r\n                  </a>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <!-- Panel Process -->\r\n            <div class=\"survey-panel__process-wrapper\">\r\n              <div class=\"survey-panel__process-image\"></div>\r\n              <div class=\"survey-panel__card-image\" hidden>\r\n                <img/>\r\n              </div>\r\n              <div class=\"survey-panel__button-wrapper btn-learn-more\" hidden>\r\n                <a class=\"survey-panel__question-button btn-learn-more\" href=\"#\"");
            {
                String var_attrvalue7 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_surveypanel, "openInNewTab")) ? "_blank" : "_self");
                {
                    Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                    {
                        boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                        if (var_shoulddisplayattr10) {
                            out.write(" target");
                            {
                                boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                                if (!var_istrueattr9) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue11 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_surveypanel, "noFollow")) ? "nofollow" : "");
                {
                    Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                    {
                        boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                        if (var_shoulddisplayattr14) {
                            out.write(" rel");
                            {
                                boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                                if (!var_istrueattr13) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n                  <span class=\"button-text\">");
            {
                Object var_15 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_surveypanel, "learnMoreCta"), "text");
                out.write(renderContext.getObjectModel().toString(var_15));
            }
            out.write("</span>\r\n                  <div class=\"button-icon\" style=\"margin-left: 0px\">\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                  </div>\r\n                </a>\r\n              </div>\r\n              <div class=\"survey-panel__progress-bar\">\r\n                <div class=\"linear-progress-bar\"></div>\r\n              </div>\r\n              <h4 class=\"survey-panel__process-label\">");
            {
                Object var_16 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_surveypanel, "q1ProgressBarDescriptionText"), "text");
                out.write(renderContext.getObjectModel().toString(var_16));
            }
            out.write("</h4>\r\n            </div>\r\n          </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar17 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions18_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions18 = obj().with("isEmpty", var_templateoptions18_field$_isempty);
            callUnit(out, renderContext, var_templatevar17, var_templateoptions18);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

