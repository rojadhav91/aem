/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.primarysearch;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class primarysearch__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar15_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.PrimarySearchModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = _global_model;
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n  ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n    <!-- icon primary search -->\r\n    <a class=\"search-primary-btn\" href=\"#primarySearchModal\">\r\n      <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/search-primary-icon.svg\"/>\r\n    </a>\r\n\r\n    <!-- primary search modal -->\r\n    <div class=\"popup modal-hidden\" id=\"primarySearchModal\" style=\"align-items: normal !important\">\r\n      <div class=\"popup__container search-primary-modal\">\r\n        <div class=\"popup__container-item\">\r\n          <div class=\"search-primary-container\">\r\n            <div class=\"input-container\">\r\n              <div class=\"icon\">\r\n                <img");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_model, "inputSearchIcon");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "uri");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" src");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n              </div>\r\n              <div class=\"icon close-icon\">\r\n                <div class=\"close-icon-wrapper\">\r\n                  <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_red.svg\"/>\r\n                </div>\r\n              </div>\r\n              <input aria-invalid=\"false\" autocomplete=\"off\"");
            {
                Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_model, "inputPlaceHolder");
                {
                    Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                    {
                        boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                        if (var_shoulddisplayattr8) {
                            out.write(" placeholder");
                            {
                                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                if (!var_istrueattr7) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" type=\"text\" aria-autocomplete=\"list\" autocapitalize=\"none\" spellcheck=\"false\" value=\"\" class=\"ui-autocomplete-input search-input\"/>\r\n            </div>\r\n            <div class=\"search-box_title\">");
            {
                Object var_9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "historyLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_9));
            }
            out.write("</div>\r\n            <div class=\"key-word-list\"");
            {
                Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_model, "resultSlug");
                {
                    Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                    {
                        boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                        if (var_shoulddisplayattr13) {
                            out.write(" data-primary-search-result-slug");
                            {
                                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                                if (!var_istrueattr12) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("></div>\r\n            <div class=\"link_title\">");
            {
                Object var_14 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "usefulLinksTitle"), "text");
                out.write(renderContext.getObjectModel().toString(var_14));
            }
            out.write("</div>\r\n            <div class=\"card-list\">\r\n              ");
            {
                Object var_collectionvar15 = renderContext.getObjectModel().resolveProperty(_global_model, "usefulLinksItemList");
                {
                    long var_size16 = ((var_collectionvar15_list_coerced$ == null ? (var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15)) : var_collectionvar15_list_coerced$).size());
                    {
                        boolean var_notempty17 = (var_size16 > 0);
                        if (var_notempty17) {
                            {
                                long var_end20 = var_size16;
                                {
                                    boolean var_validstartstepend21 = (((0 < var_size16) && true) && (var_end20 > 0));
                                    if (var_validstartstepend21) {
                                        if (var_collectionvar15_list_coerced$ == null) {
                                            var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15);
                                        }
                                        long var_index22 = 0;
                                        for (Object card : var_collectionvar15_list_coerced$) {
                                            {
                                                boolean var_traversal24 = (((var_index22 >= 0) && (var_index22 <= var_end20)) && true);
                                                if (var_traversal24) {
                                                    out.write("\r\n                <div class=\"card-container\">\r\n                  <a");
                                                    {
                                                        String var_attrcontent25 = ("https://techcombank.com/" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(card, "slug"), "uri")));
                                                        out.write(" href=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                        out.write("\"");
                                                    }
                                                    out.write(" x-cq-linkchecker=\"skip\">\r\n                    <div>\r\n                      <img alt=\"card-search-popup\"");
                                                    {
                                                        Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(card, "banner");
                                                        {
                                                            Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                                if (var_shoulddisplayattr29) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                        if (!var_istrueattr28) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    </div>\r\n                    <div class=\"card-label\">");
                                                    {
                                                        Object var_30 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(card, "title"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_30));
                                                    }
                                                    out.write("</div>\r\n                  </a>\r\n                </div>\r\n              ");
                                                }
                                            }
                                            var_index22++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar15_list_coerced$ = null;
            }
            out.write("\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar31 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions32_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions32 = obj().with("isEmpty", var_templateoptions32_field$_isempty);
            callUnit(out, renderContext, var_templatevar31, var_templateoptions32);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

