/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.imagecardslide;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class imagecardslide__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_card = null;
Object _global_template = null;
Object _global_hascontent = null;
_global_card = renderContext.call("use", com.techcombank.core.models.ImageCardSlideModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_card, "cardImage")) ? renderContext.getObjectModel().resolveProperty(_global_card, "cardImage") : renderContext.getObjectModel().resolveProperty(_global_card, "cardTitle")));
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"slide-show__container-item\">\r\n            <div class=\"card-item\"");
            {
                String var_attrcontent1 = ("--bg-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "cardBackgroundColour"), "styleString")));
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n                <div class=\"card-item__slide-show\">\r\n                    <div class=\"card-image__over-height\">\r\n                        <picture>\r\n                            <source media=\"(max-width: 360px)\"");
            {
                Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_card, "mobileCardImagePath");
                {
                    Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                    {
                        boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                        if (var_shoulddisplayattr5) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                if (!var_istrueattr4) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
            {
                Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_card, "mobileCardImagePath");
                {
                    Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                    {
                        boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                        if (var_shoulddisplayattr9) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                                if (!var_istrueattr8) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
            {
                Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_card, "webCardImagePath");
                {
                    Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                    {
                        boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                        if (var_shoulddisplayattr13) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                                if (!var_istrueattr12) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
            {
                Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_card, "webCardImagePath");
                {
                    Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                    {
                        boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                        if (var_shoulddisplayattr17) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                                if (!var_istrueattr16) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                            <img");
            {
                Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_card, "cardImageAltText");
                {
                    Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                    {
                        boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                        if (var_shoulddisplayattr21) {
                            out.write(" alt");
                            {
                                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                if (!var_istrueattr20) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_card, "webCardImagePath");
                {
                    Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "uri");
                    {
                        boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                        if (var_shoulddisplayattr25) {
                            out.write(" src");
                            {
                                boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                if (!var_istrueattr24) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" decoding=\"async\" data-nimg=\"intrinsic\"/>\r\n                          </picture>\r\n                    </div>\r\n                    <div");
            {
                String var_attrcontent26 = ("card-image__content-text card-image__content-text-align-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "textAlignmentOnCard"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                out.write("\"");
            }
            out.write(">\r\n                        <h5>");
            {
                Object var_27 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "cardTitle"), "text");
                out.write(renderContext.getObjectModel().toString(var_27));
            }
            out.write("</h5>\r\n                        <p>");
            {
                Object var_28 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "cardDescription"), "text");
                out.write(renderContext.getObjectModel().toString(var_28));
            }
            out.write("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar29 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions30_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions30 = obj().with("isEmpty", var_templateoptions30_field$_isempty);
            callUnit(out, renderContext, var_templatevar29, var_templateoptions30);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

