/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.staticgridpanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class staticgridpanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _global_staticgridpanel = null;
Object _global_hascontent = null;
Collection var_collectionvar3_list_coerced$ = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_staticgridpanel = renderContext.call("use", com.techcombank.core.models.StaticGridPanelModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "layout");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    <section class=\"container\">\r\n        ");
    {
        Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "heading");
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("<div class=\"SectionHeader_title\">");
            {
                Object var_1 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "heading"), "html");
                out.write(renderContext.getObjectModel().toString(var_1));
            }
            out.write("</div>");
        }
    }
    out.write("\r\n        <div");
    {
        String var_attrcontent2 = (("static-grid-panel " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "layout"), "attribute"))) + "__container");
        out.write(" class=\"");
        out.write(renderContext.getObjectModel().toString(var_attrcontent2));
        out.write("\"");
    }
    out.write(">\r\n            <div class=\"static-grid__container\">\r\n                ");
    {
        Object var_collectionvar3 = renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "staticGridPanelItems");
        {
            long var_size4 = ((var_collectionvar3_list_coerced$ == null ? (var_collectionvar3_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar3)) : var_collectionvar3_list_coerced$).size());
            {
                boolean var_notempty5 = (var_size4 > 0);
                if (var_notempty5) {
                    {
                        long var_end8 = var_size4;
                        {
                            boolean var_validstartstepend9 = (((0 < var_size4) && true) && (var_end8 > 0));
                            if (var_validstartstepend9) {
                                out.write("<div class=\"list__item__wrapper\">");
                                if (var_collectionvar3_list_coerced$ == null) {
                                    var_collectionvar3_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar3);
                                }
                                long var_index10 = 0;
                                for (Object item : var_collectionvar3_list_coerced$) {
                                    {
                                        boolean var_traversal12 = (((var_index10 >= 0) && (var_index10 <= var_end8)) && true);
                                        if (var_traversal12) {
                                            out.write("\r\n                    <div");
                                            {
                                                String var_attrcontent13 = (("static-grid-item static-grid-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "layout"), "attribute"))) + "-item");
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                                out.write("\"");
                                            }
                                            out.write(">\r\n                        <div");
                                            {
                                                String var_attrcontent14 = ("icon " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_staticgridpanel, "iconImageAlignment"), "attribute")));
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                                out.write("\"");
                                            }
                                            out.write(">\r\n                            <picture");
                                            {
                                                String var_attrvalue15 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "webIconImagePath")) ? "" : "no-img");
                                                {
                                                    Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                                        if (var_shoulddisplayattr18) {
                                                            out.write(" class");
                                                            {
                                                                boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                                                if (!var_istrueattr17) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(">\r\n                                <source media=\"(max-width: 360px)\"");
                                            {
                                                Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                {
                                                    Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                        if (var_shoulddisplayattr22) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                                if (!var_istrueattr21) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                                            {
                                                Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(item, "mobileIconImagePath");
                                                {
                                                    Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                                                        if (var_shoulddisplayattr26) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                                                if (!var_istrueattr25) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                                            {
                                                Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                {
                                                    Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                                                        if (var_shoulddisplayattr30) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                                                if (!var_istrueattr29) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                                            {
                                                Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                {
                                                    Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                                                        if (var_shoulddisplayattr34) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                                                if (!var_istrueattr33) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                                <img");
                                            {
                                                Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(item, "iconImageAltText");
                                                {
                                                    Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                                                        if (var_shoulddisplayattr38) {
                                                            out.write(" alt");
                                                            {
                                                                boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                                                if (!var_istrueattr37) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            {
                                                Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(item, "webIconImagePath");
                                                {
                                                    Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                                                        if (var_shoulddisplayattr42) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                                                if (!var_istrueattr41) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                            </picture>\r\n                        </div>\r\n                        <div class=\"content\">\r\n                            <div class=\"title\">");
                                            {
                                                Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "iconTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_43));
                                            }
                                            out.write("</div>\r\n                            <div class=\"description\">");
                                            {
                                                Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "description"), "html");
                                                out.write(renderContext.getObjectModel().toString(var_44));
                                            }
                                            out.write("</div>\r\n                        </div>\r\n                    </div>\r\n                ");
                                        }
                                    }
                                    var_index10++;
                                }
                                out.write("</div>");
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar3_list_coerced$ = null;
    }
    out.write("\r\n            </div>\r\n        </div>\r\n    </section>\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar45 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions46_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions46 = obj().with("isEmpty", var_templateoptions46_field$_isempty);
            callUnit(out, renderContext, var_templatevar45, var_templateoptions46);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

