/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.formsurvey;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class formsurvey__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar7_list_coerced$ = null;
Collection var_collectionvar34_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.FormSurveyModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "surveyType");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"container section__margin-medium c-cash-form-survey\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_model, "surveyType");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" data-survey-type");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <!--SINGLE CHOICE-->\r\n            ");
            {
                boolean var_testvariable5 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "surveyType"), "single"));
                if (var_testvariable5) {
                    out.write("\r\n                <div class=\"form-survey__single-choice\">\r\n                    <div class=\"single-choice__container\">\r\n                        <!--QUESTION-->\r\n                        <div class=\"single-choice__container--question\">\r\n                            <span class=\"question--title\">");
                    {
                        Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "title"), "text");
                        out.write(renderContext.getObjectModel().toString(var_6));
                    }
                    out.write("</span>\r\n                        </div>\r\n                        <!--ANSWER-->\r\n                        <div class=\"single-choice__container--answer\">\r\n                            ");
                    {
                        Object var_collectionvar7 = renderContext.getObjectModel().resolveProperty(_global_model, "formSurveyItemList");
                        {
                            long var_size8 = ((var_collectionvar7_list_coerced$ == null ? (var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7)) : var_collectionvar7_list_coerced$).size());
                            {
                                boolean var_notempty9 = (var_size8 > 0);
                                if (var_notempty9) {
                                    {
                                        long var_end12 = var_size8;
                                        {
                                            boolean var_validstartstepend13 = (((0 < var_size8) && true) && (var_end12 > 0));
                                            if (var_validstartstepend13) {
                                                if (var_collectionvar7_list_coerced$ == null) {
                                                    var_collectionvar7_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar7);
                                                }
                                                long var_index14 = 0;
                                                for (Object answer : var_collectionvar7_list_coerced$) {
                                                    {
                                                        boolean var_traversal16 = (((var_index14 >= 0) && (var_index14 <= var_end12)) && true);
                                                        if (var_traversal16) {
                                                            out.write("<label");
                                                            {
                                                                String var_attrcontent17 = ("answer__item checkbox-item__wrapper " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "answerDisplayDirection"), "attribute")));
                                                                out.write(" class=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                                out.write("\"");
                                                            }
                                                            out.write(">\r\n                                <input class=\"answer__item--input input__radio\"");
                                                            {
                                                                Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_model, "name");
                                                                {
                                                                    Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                                        if (var_shoulddisplayattr21) {
                                                                            out.write(" name");
                                                                            {
                                                                                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                                if (!var_istrueattr20) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" type=\"radio\"");
                                                            {
                                                                Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(answer, "value");
                                                                {
                                                                    Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                                        if (var_shoulddisplayattr25) {
                                                                            out.write(" value");
                                                                            {
                                                                                boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                                if (!var_istrueattr24) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write("/>\r\n                                <span class=\"answer__item--text\">");
                                                            {
                                                                Object var_26 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(answer, "title"), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_26));
                                                            }
                                                            out.write("</span>\r\n                            </label>\n");
                                                        }
                                                    }
                                                    var_index14++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar7_list_coerced$ = null;
                    }
                    out.write("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            ");
                }
            }
            out.write("\r\n\r\n            <!--MULTIPLE CHOICE-->\r\n            ");
            {
                boolean var_testvariable27 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "surveyType"), "multi"));
                if (var_testvariable27) {
                    out.write("\r\n                <div class=\"form-survey__multiple-choice\">\r\n                    <div class=\"multiple-choice__container\">\r\n                        <!--QUESTION-->\r\n                        <div class=\"multiple-choice__container--question\">\r\n                            <span class=\"question--title\">");
                    {
                        String var_28 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "title"), "text")) + " ");
                        out.write(renderContext.getObjectModel().toString(var_28));
                    }
                    out.write("<span class=\"question--caption\">");
                    {
                        Object var_29 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "caption"), "text");
                        out.write(renderContext.getObjectModel().toString(var_29));
                    }
                    out.write("</span></span>\r\n                        </div>\r\n                        <!--ANSWER-->\r\n                        <div class=\"multiple-choice__container--answer\"");
                    {
                        Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(_global_model, "minimumNumberOfAnswers");
                        {
                            Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                            {
                                boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                if (var_shoulddisplayattr33) {
                                    out.write(" data-minimum-of-answers");
                                    {
                                        boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                        if (!var_istrueattr32) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(">\r\n                            ");
                    {
                        Object var_collectionvar34 = renderContext.getObjectModel().resolveProperty(_global_model, "formSurveyItemList");
                        {
                            long var_size35 = ((var_collectionvar34_list_coerced$ == null ? (var_collectionvar34_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar34)) : var_collectionvar34_list_coerced$).size());
                            {
                                boolean var_notempty36 = (var_size35 > 0);
                                if (var_notempty36) {
                                    {
                                        long var_end39 = var_size35;
                                        {
                                            boolean var_validstartstepend40 = (((0 < var_size35) && true) && (var_end39 > 0));
                                            if (var_validstartstepend40) {
                                                if (var_collectionvar34_list_coerced$ == null) {
                                                    var_collectionvar34_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar34);
                                                }
                                                long var_index41 = 0;
                                                for (Object answer : var_collectionvar34_list_coerced$) {
                                                    {
                                                        boolean var_traversal43 = (((var_index41 >= 0) && (var_index41 <= var_end39)) && true);
                                                        if (var_traversal43) {
                                                            out.write("<label");
                                                            {
                                                                String var_attrcontent44 = ("answer__item checkbox-item__wrapper " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "answerDisplayDirection"), "attribute")));
                                                                out.write(" class=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                out.write("\"");
                                                            }
                                                            out.write(">\r\n                                <input class=\"answer__item--input input__checkbox\"");
                                                            {
                                                                Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_model, "name");
                                                                {
                                                                    Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                                                                        if (var_shoulddisplayattr48) {
                                                                            out.write(" name");
                                                                            {
                                                                                boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                                                                                if (!var_istrueattr47) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            {
                                                                Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(answer, "value");
                                                                {
                                                                    Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                                                                        if (var_shoulddisplayattr52) {
                                                                            out.write(" value");
                                                                            {
                                                                                boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                                                                if (!var_istrueattr51) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" type=\"checkbox\"/>\r\n                                <span class=\"answer__item--text\">");
                                                            {
                                                                Object var_53 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(answer, "title"), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_53));
                                                            }
                                                            out.write("</span>\r\n                            </label>\n");
                                                        }
                                                    }
                                                    var_index41++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar34_list_coerced$ = null;
                    }
                    out.write("\r\n                        </div>\r\n                        <!--ANSWER ALERT-->\r\n                        <div class=\"multiple-choice__container--answer-alert\">");
                    {
                        Object var_54 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "minimumNumberOfAnswersAlert"), "text");
                        out.write(renderContext.getObjectModel().toString(var_54));
                    }
                    out.write("</div>\r\n                    </div>\r\n                </div>\r\n            ");
                }
            }
            out.write("\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar55 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions56_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions56 = obj().with("isEmpty", var_templateoptions56_field$_isempty);
            callUnit(out, renderContext, var_templatevar55, var_templateoptions56);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

