/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.accordion;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class accordion__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_accordion = null;
Object _global_tcbaccordion = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _global_items = null;
Object _global_hascontent = null;
Object _dynamic_item = bindings.get("item");
Collection var_collectionvar24_list_coerced$ = null;
Object _dynamic_resource = bindings.get("resource");
_global_accordion = renderContext.call("use", com.adobe.cq.wcm.core.components.models.Accordion.class.getName(), obj());
_global_tcbaccordion = renderContext.call("use", com.techcombank.core.models.AccordionModel.class.getName(), obj());
out.write("<div");
{
    Object var_attrvalue0 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "accordion" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" data-panelcontainer");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue4 = renderContext.getObjectModel().resolveProperty(_global_accordion, "id");
    {
        Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
        {
            boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
            if (var_shoulddisplayattr7) {
                out.write(" id");
                {
                    boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                    if (!var_istrueattr6) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-cmp-is=\"accordion\"");
{
    Object var_attrvalue8 = renderContext.getObjectModel().resolveProperty(_global_accordion, "singleExpansion");
    {
        Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
        {
            boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
            if (var_shoulddisplayattr11) {
                out.write(" data-cmp-single-expansion");
                {
                    boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                    if (!var_istrueattr10) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue12 = renderContext.call("i18n", ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Please drag Accordion item components here" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))), obj().with("i18n", null));
    {
        Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
        {
            boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
            if (var_shoulddisplayattr15) {
                out.write(" data-placeholder-text");
                {
                    boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                    if (!var_istrueattr14) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <section class=\"accordion-component\">\r\n        <!-- content-wrapper 1440px -->\r\n        ");
_global_items = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_accordion, "children")) ? renderContext.getObjectModel().resolveProperty(_global_accordion, "children") : renderContext.getObjectModel().resolveProperty(_global_accordion, "items")));
_global_hascontent = (!(org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.leq(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 0)));
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div class=\"card-product-feature__container\" data-cmp-hook-accordion=\"item\"");
    {
        Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_dynamic_item, "id");
        {
            Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
            {
                boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                if (var_shoulddisplayattr19) {
                    out.write(" id");
                    {
                        boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                        if (!var_istrueattr18) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        boolean var_attrvalue20 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.inOp(renderContext.getObjectModel().resolveProperty(_dynamic_item, "name"), renderContext.getObjectModel().resolveProperty(_global_accordion, "expandedItems") instanceof String ? renderContext.getObjectModel().resolveProperty(_global_accordion, "expandedItems") : renderContext.getObjectModel().toCollection(renderContext.getObjectModel().resolveProperty(_global_accordion, "expandedItems"))));
        {
            Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
            {
                boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (false != var_attrvalue20)));
                if (var_shoulddisplayattr23) {
                    out.write(" data-cmp-expanded");
                    {
                        boolean var_istrueattr22 = (var_attrvalue20 == true);
                        if (!var_istrueattr22) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(">\r\n          <div class=\"card-product-feature__item__container\">\r\n            ");
    {
        Object var_collectionvar24 = _global_items;
        {
            long var_size25 = ((var_collectionvar24_list_coerced$ == null ? (var_collectionvar24_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar24)) : var_collectionvar24_list_coerced$).size());
            {
                boolean var_notempty26 = (var_size25 > 0);
                if (var_notempty26) {
                    {
                        long var_end29 = var_size25;
                        {
                            boolean var_validstartstepend30 = (((0 < var_size25) && true) && (var_end29 > 0));
                            if (var_validstartstepend30) {
                                if (var_collectionvar24_list_coerced$ == null) {
                                    var_collectionvar24_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar24);
                                }
                                long var_index31 = 0;
                                for (Object item : var_collectionvar24_list_coerced$) {
                                    {
                                        long itemlist_field$_index = var_index31;
                                        {
                                            boolean var_traversal33 = (((var_index31 >= 0) && (var_index31 <= var_end29)) && true);
                                            if (var_traversal33) {
                                                out.write("<div class=\"card-product-feature__item\">\r\n              <div class=\"card-product-feature__item__wrapper\">\r\n                <div class=\"card-product-feature__item__img__wrapper\">\r\n                  <picture>\r\n                    <source media=\"(max-width: 360px)\"");
                                                {
                                                    Object var_attrvalue34 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "mobileIconSrcOff");
                                                    {
                                                        Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                                                            if (var_shoulddisplayattr37) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                                                                    if (!var_istrueattr36) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(max-width: 576px)\"");
                                                {
                                                    Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "mobileIconSrcOff");
                                                    {
                                                        Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                            if (var_shoulddisplayattr41) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                    if (!var_istrueattr40) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(min-width: 1080px)\"");
                                                {
                                                    Object var_attrvalue42 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOff");
                                                    {
                                                        Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
                                                            if (var_shoulddisplayattr45) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                                                                    if (!var_istrueattr44) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(min-width: 1200px)\"");
                                                {
                                                    Object var_attrvalue46 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOff");
                                                    {
                                                        Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (!((Object)false).equals(var_attrvalue46))));
                                                            if (var_shoulddisplayattr49) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr48 = (var_attrvalue46.equals(true));
                                                                    if (!var_istrueattr48) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <img");
                                                {
                                                    Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                    {
                                                        Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                                                            if (var_shoulddisplayattr53) {
                                                                out.write(" alt");
                                                                {
                                                                    boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                                                                    if (!var_istrueattr52) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"card-product-feature__item__img__off\"");
                                                {
                                                    Object var_attrvalue54 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOff");
                                                    {
                                                        Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                                                            if (var_shoulddisplayattr57) {
                                                                out.write(" src");
                                                                {
                                                                    boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                                                                    if (!var_istrueattr56) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                  </picture>\r\n                  <picture>\r\n                    <source media=\"(max-width: 360px)\"");
                                                {
                                                    Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "mobileIconSrcOn");
                                                    {
                                                        Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                                                            if (var_shoulddisplayattr61) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                                                                    if (!var_istrueattr60) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(max-width: 576px)\"");
                                                {
                                                    Object var_attrvalue62 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "mobileIconSrcOn");
                                                    {
                                                        Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                                                            if (var_shoulddisplayattr65) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                                                                    if (!var_istrueattr64) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(min-width: 1080px)\"");
                                                {
                                                    Object var_attrvalue66 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOn");
                                                    {
                                                        Object var_attrcontent67 = renderContext.call("xss", var_attrvalue66, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr69 = (((null != var_attrcontent67) && (!"".equals(var_attrcontent67))) && ((!"".equals(var_attrvalue66)) && (!((Object)false).equals(var_attrvalue66))));
                                                            if (var_shoulddisplayattr69) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr68 = (var_attrvalue66.equals(true));
                                                                    if (!var_istrueattr68) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent67));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <source media=\"(min-width: 1200px)\"");
                                                {
                                                    Object var_attrvalue70 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOn");
                                                    {
                                                        Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                                                            if (var_shoulddisplayattr73) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                                                                    if (!var_istrueattr72) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                    <img");
                                                {
                                                    Object var_attrvalue74 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                    {
                                                        Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                                                            if (var_shoulddisplayattr77) {
                                                                out.write(" alt");
                                                                {
                                                                    boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                                                                    if (!var_istrueattr76) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"card-product-feature__item__img__on\"");
                                                {
                                                    Object var_attrvalue78 = renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_tcbaccordion, "iconImages"), itemlist_field$_index), "webIconSrcOn");
                                                    {
                                                        Object var_attrcontent79 = renderContext.call("xss", var_attrvalue78, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr81 = (((null != var_attrcontent79) && (!"".equals(var_attrcontent79))) && ((!"".equals(var_attrvalue78)) && (!((Object)false).equals(var_attrvalue78))));
                                                            if (var_shoulddisplayattr81) {
                                                                out.write(" src");
                                                                {
                                                                    boolean var_istrueattr80 = (var_attrvalue78.equals(true));
                                                                    if (!var_istrueattr80) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent79));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                  </picture>\r\n                </div>\r\n                <div class=\"card-product-feature__item__content__wrapper\">\r\n                  <div class=\"card-product-feature__item__content__title\">");
                                                {
                                                    Object var_82 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "title"), "text");
                                                    out.write(renderContext.getObjectModel().toString(var_82));
                                                }
                                                out.write("</div>\r\n                  <div class=\"card-product-feature__item__content__icon\">\r\n                    <div class=\"card-product-feature__item__content__icon__on\">\r\n                      <span class=\"add popup__add-icon material-symbols-outlined\">add</span>\r\n                    </div>\r\n                    <div class=\"card-product-feature__item__content__icon__off\">\r\n                      <span class=\"remove popup__remove-icon material-symbols-outlined\">remove</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div data-cmp-hook-accordion=\"panel\"");
                                                {
                                                    String var_attrcontent83 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-panel");
                                                    out.write(" id=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent83));
                                                    out.write("\"");
                                                }
                                                out.write(" class=\"card-product-feature__item__description\" role=\"region\"");
                                                {
                                                    String var_attrcontent84 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "id"), "attribute")) + "-button");
                                                    out.write(" aria-labelledby=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    Object var_resourcecontent85 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(item, "resource"), obj().with("decorationTagName", "div"));
                                                    out.write(renderContext.getObjectModel().toString(var_resourcecontent85));
                                                }
                                                out.write("</div>\r\n            </div>\n");
                                            }
                                        }
                                    }
                                    var_index31++;
                                }
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar24_list_coerced$ = null;
    }
    out.write("\r\n          </div>\r\n        </div>");
}
out.write("\r\n    </section>\r\n    ");
{
    Object var_testvariable86 = ((renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")))) ? (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.lt(renderContext.getObjectModel().resolveProperty(_global_items, "size"), 1)) : ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")))));
    if (renderContext.getObjectModel().toBoolean(var_testvariable86)) {
        {
            Object var_resourcecontent87 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), obj().with("cssClassName", "new section aem-Grid-newComponent").with("decorationTagName", "div").with("appendPath", "/*").with("resourceType", "wcm/foundation/components/parsys/newpar"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent87));
        }
    }
}
out.write("\r\n</div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

