/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.appointmentbooking;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class appointmentbooking__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_appointmentbookingmodel = null;
Object _dynamic_currentpage = bindings.get("currentpage");
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n\r\n");
_global_appointmentbookingmodel = renderContext.call("use", com.techcombank.core.models.AppointmentBookingModel.class.getName(), obj());
out.write("\r\n    <div");
{
    String var_attrcontent5 = ("booking " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableTabs")) ? "booking-visit" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
{
    Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "todayLabel");
    {
        Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
        {
            boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
            if (var_shoulddisplayattr9) {
                out.write(" data-today-label");
                {
                    boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                    if (!var_istrueattr8) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "tomorrowLabel");
    {
        Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
        {
            boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
            if (var_shoulddisplayattr13) {
                out.write(" data-tomorrow-label");
                {
                    boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                    if (!var_istrueattr12) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
        {
            boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
            if (var_shoulddisplayattr17) {
                out.write(" data-api-branch");
                {
                    boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                    if (!var_istrueattr16) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-api-branch-suffix=\"\"");
{
    Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
        {
            boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
            if (var_shoulddisplayattr21) {
                out.write(" data-api-service");
                {
                    boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                    if (!var_istrueattr20) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
        {
            boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
            if (var_shoulddisplayattr25) {
                out.write(" data-api-booking-slot");
                {
                    boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                    if (!var_istrueattr24) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path");
    {
        Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
        {
            boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
            if (var_shoulddisplayattr29) {
                out.write(" data-api-booking-confirm");
                {
                    boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                    if (!var_istrueattr28) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-serviceId=\"79\">\r\n            <input hidden name=\"selectedBranch\" value=\"\"/>\r\n            <input hidden name=\"selectedService\" value=\"\"/>\r\n        <div class=\"booking-container-wrapper\">\r\n            ");
{
    Object var_testvariable30 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableTabs");
    if (renderContext.getObjectModel().toBoolean(var_testvariable30)) {
        out.write("\r\n                <div");
        {
            String var_attrcontent31 = ("client booking-container hidden " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableTabs")) ? (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) : renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableTabs")))) ? "active" : "hidden"), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent31));
            out.write("\"");
        }
        out.write(">\r\n                    <div class=\"type\">\r\n                        <p>");
        {
            Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "identifierLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_32));
        }
        out.write("</p>\r\n                        <div class=\"client-list\">\r\n                            <div class=\"item\">\r\n                                <span class=\"personal\">");
        {
            Object var_33 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "personalLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_33));
        }
        out.write("</span>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg\"/>\r\n                            </div>\r\n                            <div class=\"item\">\r\n                                <span class=\"bussiness\">");
        {
            Object var_34 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "businessLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_34));
        }
        out.write("</span>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg\"/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"service booking-container hidden\">\r\n                    <div class=\"type\">\r\n                        <p>");
        {
            Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "actionLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_35));
        }
        out.write("</p>\r\n                        <div class=\"service-list\">\r\n                            <div class=\"item\">\r\n                                <span class=\"\"></span>\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg\"/>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"branch booking-container hidden\">\r\n                    <div class=\"name\">\r\n                        <p>");
        {
            Object var_36 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "branchLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_36));
        }
        out.write("</p>\r\n                        <div class=\"search-filter\">\r\n                            <div class=\"city filter\">\r\n                                <div class=\"option\">\r\n                                    <div class=\"dropdown-text\">\r\n                                        <span class=\"dropdown-label\">");
        {
            Object var_37 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "cityLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_37));
        }
        out.write("</span>\r\n                                        <span class=\"dropdown-body\"></span>\r\n                                    </div>\r\n                                    <img alt=\"arrow down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\"/>\r\n                                </div>\r\n                                <div class=\"select-options\">\r\n                                    <ul>\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"district filter\">\r\n                                <div class=\"option\">\r\n                                    <div class=\"dropdown-text\">\r\n                                        <span class=\"dropdown-label\">");
        {
            Object var_38 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "districtLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_38));
        }
        out.write("</span>\r\n                                        <span class=\"dropdown-body\"></span>\r\n                                    </div>\r\n                                    <img alt=\"arrow down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-icon-down-red.svg\"/>\r\n                                </div>\r\n                                <div class=\"select-options\">\r\n                                    <ul>\r\n                                    </ul>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"address\">\r\n                        <p>");
        {
            Object var_39 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "branchListLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_39));
        }
        out.write("</p>\r\n                        <div class=\"address-list\"></div>\r\n                    </div>\r\n                </div>\r\n            ");
    }
}
out.write("\r\n            ");
{
    boolean var_testvariable40 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable40) {
        out.write("\r\n            <div");
        {
            String var_attrcontent41 = ("schedule booking-container " + renderContext.getObjectModel().toString(renderContext.call("xss", (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableTabs"))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")))) ? "active" : "hidden"), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
            out.write("\"");
        }
        out.write(">\r\n                <div class=\"date\">\r\n                    <div class=\"today\"></div>\r\n                    <div class=\"tomorrow\"></div>\r\n                </div>\r\n                <div class=\"time\">\r\n                    <p>");
        {
            Object var_42 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "timeSlotLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_42));
        }
        out.write("</p>\r\n                    <div class=\"time-list\"></div>\r\n                </div>\r\n            </div>\r\n            ");
    }
}
out.write("\r\n            <form");
{
    String var_attrcontent43 = ("form booking-container " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "active" : "hidden"), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent43));
    out.write("\"");
}
out.write(">\r\n            \t");
{
    boolean var_testvariable44 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable44) {
        out.write("\r\n                <h3>");
        {
            Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "title"), "text");
            out.write(renderContext.getObjectModel().toString(var_45));
        }
        out.write("</h3>\r\n                <div class=\"input-form\">\r\n                    <div class=\"input taxt-code\">\r\n                        <label for=\"name\">");
        {
            Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "accTaxLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_46));
        }
        out.write("<span>*</span></label><br/>\r\n                        <input autocomplete=\"off\" id=\"taxCode\" name=\"taxCode\"");
        {
            Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "accTaxPlaceHolder");
            {
                Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
                {
                    boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                    if (var_shoulddisplayattr50) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                            if (!var_istrueattr49) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"text\"/><br/>\r\n                        <p class=\"form-error-message\" id=\"taxCode-error\">");
        {
            Object var_51 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "accTaxRequired"), "text");
            out.write(renderContext.getObjectModel().toString(var_51));
        }
        out.write("</p>\r\n                    </div>\r\n                    <div class=\"input\">\r\n                        <label for=\"name\">");
        {
            Object var_52 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "nameLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_52));
        }
        out.write("</label><br/>\r\n                        <input autocomplete=\"off\" id=\"name\" name=\"name\"");
        {
            Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "namePlaceHolder");
            {
                Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
                {
                    boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                    if (var_shoulddisplayattr56) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                            if (!var_istrueattr55) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"text\"/><br/>\r\n                    </div>\r\n                    <div class=\"input\">\r\n                        <label for=\"email\">");
        {
            Object var_57 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "emailLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_57));
        }
        out.write("<span>*</span></label><br/>\r\n                        <input autocomplete=\"off\" id=\"emailAddress\" name=\"emailAddress\"");
        {
            Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "emailPlaceHolder");
            {
                Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                {
                    boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                    if (var_shoulddisplayattr61) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                            if (!var_istrueattr60) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"text\"/><br/>\r\n                        <p class=\"form-error-message\" id=\"emailAddress-error-required\">");
        {
            String var_62 = ("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "emailRequired"), "text")));
            out.write(renderContext.getObjectModel().toString(var_62));
        }
        out.write("</p>\r\n                        <p class=\"form-error-message\" id=\"emailAddress-error-invalid\">");
        {
            String var_63 = ("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "emailInvalid"), "text")));
            out.write(renderContext.getObjectModel().toString(var_63));
        }
        out.write("</p>\r\n                    </div>\r\n                    <div class=\"input\">\r\n                        <label for=\"phone\">");
        {
            Object var_64 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "phoneLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_64));
        }
        out.write("<span>*</span></label><br/>\r\n                        <input autocomplete=\"off\" id=\"phone\" maxlength=\"10\" name=\"phone\"");
        {
            Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "phonePlaceHolder");
            {
                Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                {
                    boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                    if (var_shoulddisplayattr68) {
                        out.write(" placeholder");
                        {
                            boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                            if (!var_istrueattr67) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" type=\"text\"/><br/>\r\n                        <p class=\"form-error-message\" id=\"phone-error\">");
        {
            Object var_69 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "phoneRequired"), "text");
            out.write(renderContext.getObjectModel().toString(var_69));
        }
        out.write("</p>\r\n                        <p class=\"form-error-message\" id=\"phone-error-invalid\" style=\"display: none;\">");
        {
            Object var_70 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "phoneInvalid"), "text");
            out.write(renderContext.getObjectModel().toString(var_70));
        }
        out.write("</p>\r\n                    </div>\r\n                </div>\r\n                ");
    }
}
out.write("\r\n                ");
{
    Object var_testvariable71 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "enableRecaptcha");
    if (renderContext.getObjectModel().toBoolean(var_testvariable71)) {
        out.write("\r\n                    <div class=\"captcha\">\r\n                        ");
        {
            Object var_resourcecontent72 = renderContext.call("includeResource", "recaptcha", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/form/recaptcha"));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent72));
        }
        out.write("\r\n                    </div>\r\n                ");
    }
}
out.write("\r\n                ");
{
    boolean var_testvariable73 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
    if (var_testvariable73) {
        out.write("\r\n                <input class=\"btn-submit\" type=\"submit\"");
        {
            Object var_attrvalue74 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "ctaLabel");
            {
                Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "attribute");
                {
                    boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                    if (var_shoulddisplayattr77) {
                        out.write(" value");
                        {
                            boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                            if (!var_istrueattr76) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                       <!--book a visit analytics : start -->\r\n                <input type=\"hidden\" id=\"book-visit-analytics\" data-tracking-click-event=\"bookVisit\" data-tracking-form-info-value=\"{'hashedEmail': 'emailID'}\" data-tracking-form-info-key=\"formInfo\"");
        {
            String var_attrcontent78 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'Other'}}");
            out.write(" data-tracking-web-interaction-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent78));
            out.write("\"");
        }
        out.write("/>\r\n                <!--book a visit analytics : end -->\r\n                ");
    }
}
out.write("\r\n            </form>\r\n            <div class=\"confirm-appointment hidden\">\r\n                <div class=\"confirm-content-wrapper\">\r\n                    <div class=\"comfirm-img-wrapper\">\r\n                     <span>\r\n                        <picture>\r\n                                 <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconMobileImagePath");
    {
        Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
        {
            boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
            if (var_shoulddisplayattr82) {
                out.write(" srcset");
                {
                    boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                    if (!var_istrueattr81) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                 <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconMobileImagePath");
    {
        Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
        {
            boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
            if (var_shoulddisplayattr86) {
                out.write(" srcset");
                {
                    boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                    if (!var_istrueattr85) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                 <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconWebImagePath");
    {
        Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "attribute");
        {
            boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
            if (var_shoulddisplayattr90) {
                out.write(" srcset");
                {
                    boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                    if (!var_istrueattr89) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                 <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue91 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconWebImagePath");
    {
        Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
        {
            boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
            if (var_shoulddisplayattr94) {
                out.write(" srcset");
                {
                    boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                    if (!var_istrueattr93) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                 <img");
{
    Object var_attrvalue95 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconAlt");
    {
        Object var_attrcontent96 = renderContext.call("xss", var_attrvalue95, "attribute");
        {
            boolean var_shoulddisplayattr98 = (((null != var_attrcontent96) && (!"".equals(var_attrcontent96))) && ((!"".equals(var_attrvalue95)) && (!((Object)false).equals(var_attrvalue95))));
            if (var_shoulddisplayattr98) {
                out.write(" alt");
                {
                    boolean var_istrueattr97 = (var_attrvalue95.equals(true));
                    if (!var_istrueattr97) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent96));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue99 = renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationIconWebImagePath");
    {
        Object var_attrcontent100 = renderContext.call("xss", var_attrvalue99, "uri");
        {
            boolean var_shoulddisplayattr102 = (((null != var_attrcontent100) && (!"".equals(var_attrcontent100))) && ((!"".equals(var_attrvalue99)) && (!((Object)false).equals(var_attrvalue99))));
            if (var_shoulddisplayattr102) {
                out.write(" src");
                {
                    boolean var_istrueattr101 = (var_attrvalue99.equals(true));
                    if (!var_istrueattr101) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent100));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                              </picture>\r\n                        <noscript></noscript>\r\n                     </span>\r\n                    </div>\r\n                    <h6 class=\"confirm-label\">");
{
    Object var_103 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "confirmationMessage"), "text");
    out.write(renderContext.getObjectModel().toString(var_103));
}
out.write("</h6>\r\n                    <h6 class=\"confirm-label date-appointment\">");
{
    Object var_104 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "bookingMessage"), "text");
    out.write(renderContext.getObjectModel().toString(var_104));
}
out.write("</h6>\r\n                    <h6 class=\"confirm-label ticket-number\">");
{
    String var_105 = ("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "ticketNumberLabel"), "text")));
    out.write(renderContext.getObjectModel().toString(var_105));
}
out.write("</h6>\r\n                    <h4 class=\"confirm-ticket-number\"></h4>\r\n                    <p class=\"confirm-note\">");
{
    Object var_106 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_appointmentbookingmodel, "ticketNotes"), "text");
    out.write(renderContext.getObjectModel().toString(var_106));
}
out.write("</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"loading\">\r\n                <div></div>\r\n                <div></div>\r\n                <div></div>\r\n                <div></div>\r\n            </div>\r\n            <div class=\"loading_backdrop\"></div>\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

