/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.milestonepanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class milestonepanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_milestonepanelmodel = null;
Collection var_collectionvar5_list_coerced$ = null;
Collection var_collectionvar21_list_coerced$ = null;
Collection var_collectionvar33_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n  <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_milestonepanelmodel = renderContext.call("use", com.techcombank.core.models.MilestonePanelModel.class.getName(), obj());
out.write("\r\n    <div class=\"milestone\">\r\n        <div class=\"milestone__bg-image\">\r\n            <div class=\"milestone__body\">\r\n                <div class=\"body-wrapper__time-line\">\r\n                    <div class=\"time-line__left-content\">\r\n                        <div class=\"left-content__wrapper\">\r\n                            <div class=\"left-content__year-slider\">\r\n                                <div class=\"year-slider__slick\">\r\n                                    ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_milestonepanelmodel, "timelines");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object timelines : var_collectionvar5_list_coerced$) {
                                {
                                    boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                    if (var_traversal14) {
                                        out.write("\r\n                                        <div class=\"year-slider__slick-item\" tabindex=\"-1\" data-tracking-click-event=\"carouselClick\"");
                                        {
                                            String var_attrcontent15 = (("{'carouselName': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(timelines, "timelineYear"), "attribute"))) + "'}");
                                            out.write(" data-tracking-click-info-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                            out.write("\"");
                                        }
                                        {
                                            Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_global_milestonepanelmodel, "webInteractionValue");
                                            {
                                                Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                                                    if (var_shoulddisplayattr19) {
                                                        out.write(" data-tracking-web-interaction-value");
                                                        {
                                                            boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                                                            if (!var_istrueattr18) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">\r\n                                            <h3>");
                                        {
                                            Object var_20 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(timelines, "timelineYear"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_20));
                                        }
                                        out.write("</h3>\r\n                                        </div>\r\n                                    ");
                                    }
                                }
                                var_index12++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"left-content__content-slider\">\r\n                                <div class=\"content-slider__slick\">\r\n                                    ");
{
    Object var_collectionvar21 = renderContext.getObjectModel().resolveProperty(_global_milestonepanelmodel, "timelines");
    {
        long var_size22 = ((var_collectionvar21_list_coerced$ == null ? (var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21)) : var_collectionvar21_list_coerced$).size());
        {
            boolean var_notempty23 = (var_size22 > 0);
            if (var_notempty23) {
                {
                    long var_end26 = var_size22;
                    {
                        boolean var_validstartstepend27 = (((0 < var_size22) && true) && (var_end26 > 0));
                        if (var_validstartstepend27) {
                            if (var_collectionvar21_list_coerced$ == null) {
                                var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21);
                            }
                            long var_index28 = 0;
                            for (Object timelines : var_collectionvar21_list_coerced$) {
                                {
                                    boolean var_traversal30 = (((var_index28 >= 0) && (var_index28 <= var_end26)) && true);
                                    if (var_traversal30) {
                                        out.write("\r\n                                        <div class=\"content-slider__item\" tabindex=\"-1\">\r\n                                            <h3>");
                                        {
                                            Object var_31 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(timelines, "timelineSliderTitle"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_31));
                                        }
                                        out.write("</h3>\r\n                                            <div class=\"content-slider__item-text\">");
                                        {
                                            Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(timelines, "timelineSliderDescriptionText"), "html");
                                            out.write(renderContext.getObjectModel().toString(var_32));
                                        }
                                        out.write("</div>\r\n                                        </div>\r\n                                    ");
                                    }
                                }
                                var_index28++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar21_list_coerced$ = null;
}
out.write("\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"time-line__right-content\"></div>\r\n                </div>\r\n            </div>\r\n            <div class=\"milestone__image-card\">\r\n                <div class=\"image-card__left-content\"></div>\r\n                <div class=\"image-card__right-content\">\r\n                    ");
{
    Object var_collectionvar33 = renderContext.getObjectModel().resolveProperty(_global_milestonepanelmodel, "timelines");
    {
        long var_size34 = ((var_collectionvar33_list_coerced$ == null ? (var_collectionvar33_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar33)) : var_collectionvar33_list_coerced$).size());
        {
            boolean var_notempty35 = (var_size34 > 0);
            if (var_notempty35) {
                {
                    long var_end38 = var_size34;
                    {
                        boolean var_validstartstepend39 = (((0 < var_size34) && true) && (var_end38 > 0));
                        if (var_validstartstepend39) {
                            if (var_collectionvar33_list_coerced$ == null) {
                                var_collectionvar33_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar33);
                            }
                            long var_index40 = 0;
                            for (Object timelines : var_collectionvar33_list_coerced$) {
                                {
                                    boolean var_traversal42 = (((var_index40 >= 0) && (var_index40 <= var_end38)) && true);
                                    if (var_traversal42) {
                                        out.write("\r\n                        <div class=\"content-slider__image\">\r\n                            <picture>\r\n                                <source media=\"(max-width: 360px)\"");
                                        {
                                            Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(timelines, "mobileImagePath");
                                            {
                                                Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                    if (var_shoulddisplayattr46) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                            if (!var_istrueattr45) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                                        {
                                            Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(timelines, "mobileImagePath");
                                            {
                                                Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                                                    if (var_shoulddisplayattr50) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                                            if (!var_istrueattr49) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                                        {
                                            Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(timelines, "webImagePath");
                                            {
                                                Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                                                    if (var_shoulddisplayattr54) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                                                            if (!var_istrueattr53) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                                        {
                                            Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(timelines, "webImagePath");
                                            {
                                                Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                                                    if (var_shoulddisplayattr58) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                                                            if (!var_istrueattr57) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                <img");
                                        {
                                            Object var_attrvalue59 = renderContext.getObjectModel().resolveProperty(timelines, "timelineImageAltText");
                                            {
                                                Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
                                                    if (var_shoulddisplayattr62) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                                                            if (!var_istrueattr61) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(" data-nimg=\"intrinsic\" decoding=\"async\"");
                                        {
                                            Object var_attrvalue63 = renderContext.getObjectModel().resolveProperty(timelines, "webImagePath");
                                            {
                                                Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "uri");
                                                {
                                                    boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                                                    if (var_shoulddisplayattr66) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                                                            if (!var_istrueattr65) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                            </picture>\r\n                        </div>\r\n                    ");
                                    }
                                }
                                var_index40++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar33_list_coerced$ = null;
}
out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

