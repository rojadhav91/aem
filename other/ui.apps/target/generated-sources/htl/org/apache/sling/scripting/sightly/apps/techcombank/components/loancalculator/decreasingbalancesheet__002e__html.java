/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.loancalculator;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class decreasingbalancesheet__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_decreasingbalancesheet = getProperty("decreasingbalancesheet");
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------

/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
addSubTemplate("decreasingbalancesheet", new RenderUnit() {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Sub-Template Body -------------------------------------------------------------------------

Object _dynamic_index = arguments.get("index");
Object _dynamic_component = bindings.get("component");
Object _dynamic_calculator = arguments.get("calculator");
Object _dynamic_calculationtype = arguments.get("calculationtype");
out.write("\r\n    <div");
{
    String var_attrcontent0 = ("panel-info " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_dynamic_index, 0)) ? "active-panel" : ""), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent0));
    out.write("\"");
}
{
    Object var_attrvalue1 = _dynamic_index;
    {
        Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
        {
            boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
            if (var_shoulddisplayattr4) {
                out.write(" data-id");
                {
                    boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                    if (!var_istrueattr3) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue5 = renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(_dynamic_index, 0)) ? "" : "display: none;"), "styleString");
    {
        boolean var_shoulddisplayattr8 = ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5)));
        if (var_shoulddisplayattr8) {
            out.write(" style");
            {
                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                if (!var_istrueattr7) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue5));
                    out.write("\"");
                }
            }
        }
    }
}
out.write(">\r\n    <input type=\"hidden\" class=\"calulator-analytics\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent9 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent9));
    out.write("\"");
}
{
    String var_attrcontent10 = (((((((((("{'calculatorName':'Decreasing balance sheet',\r\n     'calculatorFields': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "valueLabelText"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanAmountLabelText"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "loanTermLabelText"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "interestRateLabelText"), "attribute"))) + " |  ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculator, "disbursementDateLabelText"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
    out.write("\"");
}
out.write("/>\r\n        <span>\r\n            ");
{
    Object var_testvariable11 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBg");
    if (renderContext.getObjectModel().toBoolean(var_testvariable11)) {
        out.write("<picture>\r\n                <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBgMobile");
            {
                Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                {
                    boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                    if (var_shoulddisplayattr15) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                            if (!var_istrueattr14) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBgMobile");
            {
                Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                {
                    boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                    if (var_shoulddisplayattr19) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                            if (!var_istrueattr18) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue20 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBg");
            {
                Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
                {
                    boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (!((Object)false).equals(var_attrvalue20))));
                    if (var_shoulddisplayattr23) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr22 = (var_attrvalue20.equals(true));
                            if (!var_istrueattr22) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBg");
            {
                Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                {
                    boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                    if (var_shoulddisplayattr27) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                            if (!var_istrueattr26) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                <img decoding=\"async\" data-nimg=\"fill\" class=\"InsuranceGainPanel_background__v_Ipd\" sizes=\"100vw\"");
        {
            Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingBg");
            {
                Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "uri");
                {
                    boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                    if (var_shoulddisplayattr31) {
                        out.write(" src");
                        {
                            boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                            if (!var_istrueattr30) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingImgAltText");
            {
                Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "attribute");
                {
                    boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                    if (var_shoulddisplayattr35) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                            if (!var_istrueattr34) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n            </picture>");
    }
}
out.write("\r\n        </span>\r\n        <div class=\"panel-info__content\">\r\n            <div class=\"panel-info__content-text\">\r\n                <div class=\"info-content-text__icon\">\r\n                    <span>\r\n                        ");
{
    Object var_testvariable36 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIcon");
    if (renderContext.getObjectModel().toBoolean(var_testvariable36)) {
        out.write("<picture>\r\n                            <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIconMobile");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIconMobile");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIcon");
            {
                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                {
                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                    if (var_shoulddisplayattr48) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                            if (!var_istrueattr47) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIcon");
            {
                Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                {
                    boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                    if (var_shoulddisplayattr52) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                            if (!var_istrueattr51) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                            <img decoding=\"async\" data-nimg=\"fill\"");
        {
            Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCardIcon");
            {
                Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "uri");
                {
                    boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                    if (var_shoulddisplayattr56) {
                        out.write(" src");
                        {
                            boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                            if (!var_istrueattr55) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingImgAltText");
            {
                Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "attribute");
                {
                    boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                    if (var_shoulddisplayattr60) {
                        out.write(" alt");
                        {
                            boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                            if (!var_istrueattr59) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                        </picture>");
    }
}
out.write("\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"panel-info__content-text\">\r\n                <div class=\"info-content-text__label\">\r\n                    <div class=\"info-title light-gray\">");
{
    String var_61 = (("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingMonthlyLabelText"), "text"))) + "\r\n                    ");
    out.write(renderContext.getObjectModel().toString(var_61));
}
out.write("</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"panel-info__content-text payment-detail\">\r\n                <div class=\"info-content-text__label\">\r\n                    <div class=\"dark-gray\">");
{
    Object var_62 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingFromLabelText"), "text");
    out.write(renderContext.getObjectModel().toString(var_62));
}
out.write("</div>\r\n                    <h3 id=\"max-value\" class=\"monthly-pay\">");
{
    String var_63 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCurrencyText"), "text")));
    out.write(renderContext.getObjectModel().toString(var_63));
}
out.write("</h3>\r\n                </div>\r\n                <div class=\"info-content-text__label\">\r\n                    <div class=\"dark-gray\">");
{
    Object var_64 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingToLabelText"), "text");
    out.write(renderContext.getObjectModel().toString(var_64));
}
out.write("</div>\r\n                    <h3 id=\"min-value\" class=\"monthly-pay to\">");
{
    String var_65 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCurrencyText"), "text")));
    out.write(renderContext.getObjectModel().toString(var_65));
}
out.write("</h3>\r\n                </div>\r\n            </div>\r\n            <div class=\"panel-info__content-text\">\r\n                <div class=\"info-content-text__label\">\r\n                    <div class=\"info-title light-gray\">");
{
    Object var_66 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingTotalIntLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_66));
}
out.write("</div>\r\n                    <h3 id=\"total-value\" class=\"total-pay\">");
{
    String var_67 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingCurrencyText"), "text")));
    out.write(renderContext.getObjectModel().toString(var_67));
}
out.write("</h3>\r\n                </div>\r\n            </div>\r\n            <div class=\"panel-info__content-button\">\r\n                <a href=\"#\" class=\"content-button__link loan-calc\">");
{
    String var_68 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_calculationtype, "decreasingLinkTextLabel"), "text"))) + "\r\n                ");
    out.write(renderContext.getObjectModel().toString(var_68));
}
out.write("</a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");


// End Of Main Sub-Template Body ------------------------------------------------------------------
    }



    {
//Sub-Sub-Templates Initialization ----------------------------------------------------------------



//End of Sub-Sub-Templates Initialization ---------------------------------------------------------
    }
    
});


//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

