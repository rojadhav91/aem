/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.mastheadextend;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class mastheadextend__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _global_mastheadextend = null;
Object _global_hascontent = null;
Collection var_collectionvar11_list_coerced$ = null;
Collection var_collectionvar70_list_coerced$ = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_mastheadextend = renderContext.call("use", com.techcombank.core.models.MastheadExtendModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "phoneScreenshotImage");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n      <div class=\"masthead-extend-component\">\r\n        <section");
            {
                String var_attrvalue1 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "applyGradientOverBackgroundColor")) ? "masthead-extend gradient" : "masthead-extend");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" class");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent5 = (("background:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "bannerTopBackgroundColor"), "styleString"))) + ";");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                out.write("\"");
            }
            out.write(">\r\n          <div class=\"masthead-extend__banner\">\r\n            <div class=\"banner-center__container\">\r\n              <div class=\"banner-center__container-title\">\r\n                  <div class=\"container-title__title-arrow\">\r\n                      <div>");
            {
                Object var_6 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "title"), "html");
                out.write(renderContext.getObjectModel().toString(var_6));
            }
            out.write("</div>\r\n                  </div>\r\n              </div>\r\n              <div class=\"banner-center__container-desc\">\r\n                <p>");
            {
                Object var_7 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "description"), "html");
                out.write(renderContext.getObjectModel().toString(var_7));
            }
            out.write("</p>\r\n              </div>\r\n              ");
            {
                Object var_testvariable8 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "ctaButtonRequired");
                if (renderContext.getObjectModel().toBoolean(var_testvariable8)) {
                    out.write("<div class=\"banner-center__container-button\">\r\n                  <div class=\"btn--wrapper\">");
                    {
                        Object var_resourcecontent9 = renderContext.call("includeResource", "button", obj().with("decorationTagName", "div").with("resourceType", "techcombank/components/button"));
                        out.write(renderContext.getObjectModel().toString(var_resourcecontent9));
                    }
                    out.write("</div>\r\n              </div>");
                }
            }
            out.write("\r\n            </div>\r\n          </div>\r\n          <div class=\"masthead-extend__card-gallery\"");
            {
                String var_attrcontent10 = (("background:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "bannerBottomBackgroundColor"), "styleString"))) + ";");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                out.write("\"");
            }
            out.write(">\r\n            <div class=\"card-gallery__image-gallery\">\r\n              ");
            {
                Object var_collectionvar11 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "mastheadExtendImageList");
                {
                    long var_size12 = ((var_collectionvar11_list_coerced$ == null ? (var_collectionvar11_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar11)) : var_collectionvar11_list_coerced$).size());
                    {
                        boolean var_notempty13 = (var_size12 > 0);
                        if (var_notempty13) {
                            {
                                long var_end16 = var_size12;
                                {
                                    boolean var_validstartstepend17 = (((0 < var_size12) && true) && (var_end16 > 0));
                                    if (var_validstartstepend17) {
                                        out.write("<div class=\"image-gallery__container\">");
                                        if (var_collectionvar11_list_coerced$ == null) {
                                            var_collectionvar11_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar11);
                                        }
                                        long var_index18 = 0;
                                        for (Object image : var_collectionvar11_list_coerced$) {
                                            {
                                                long imagelist_field$_index = var_index18;
                                                {
                                                    boolean var_traversal20 = (((var_index18 >= 0) && (var_index18 <= var_end16)) && true);
                                                    if (var_traversal20) {
                                                        out.write("\r\n                <div class=\"image-gallery__content-item\">\r\n                  <div class=\"content-item__img\">\r\n                    <span>\r\n                      <picture>\r\n                        <source media=\"(max-width: 360px)\"");
                                                        {
                                                            Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(image, "mobileImagePath");
                                                            {
                                                                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                    if (var_shoulddisplayattr24) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                            if (!var_istrueattr23) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                        <source media=\"(max-width: 576px)\"");
                                                        {
                                                            Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(image, "mobileImagePath");
                                                            {
                                                                Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                    if (var_shoulddisplayattr28) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                            if (!var_istrueattr27) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                        <source media=\"(min-width: 1080px)\"");
                                                        {
                                                            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(image, "webImagePath");
                                                            {
                                                                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                    if (var_shoulddisplayattr32) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                            if (!var_istrueattr31) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                        <source media=\"(min-width: 1200px)\"");
                                                        {
                                                            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(image, "webImagePath");
                                                            {
                                                                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                                    if (var_shoulddisplayattr36) {
                                                                        out.write(" srcset");
                                                                        {
                                                                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                                            if (!var_istrueattr35) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                        <img");
                                                        {
                                                            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(image, "imageAltText");
                                                            {
                                                                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                                    if (var_shoulddisplayattr40) {
                                                                        out.write(" alt");
                                                                        {
                                                                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                                            if (!var_istrueattr39) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(image, "webImagePath");
                                                            {
                                                                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                                                                    if (var_shoulddisplayattr44) {
                                                                        out.write(" src");
                                                                        {
                                                                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                                                            if (!var_istrueattr43) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write("/>\r\n                      </picture>\r\n                    </span>\r\n                  </div>\r\n                </div>\r\n                ");
                                                        {
                                                            boolean var_testvariable45 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(imagelist_field$_index, 1));
                                                            if (var_testvariable45) {
                                                                out.write("<div class=\"image-gallery__content-item image-gallery__center\">\r\n                  <div class=\"content-item__img\">\r\n                    <span>\r\n                      <picture>\r\n                        <source media=\"(max-width: 360px)\"");
                                                                {
                                                                    Object var_attrvalue46 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "mobilePhoneScreenShotImagePath");
                                                                    {
                                                                        Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (!((Object)false).equals(var_attrvalue46))));
                                                                            if (var_shoulddisplayattr49) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr48 = (var_attrvalue46.equals(true));
                                                                                    if (!var_istrueattr48) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                        <source media=\"(max-width: 576px)\"");
                                                                {
                                                                    Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "mobilePhoneScreenShotImagePath");
                                                                    {
                                                                        Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
                                                                            if (var_shoulddisplayattr53) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                                                                                    if (!var_istrueattr52) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                        <source media=\"(min-width: 1080px)\"");
                                                                {
                                                                    Object var_attrvalue54 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "webPhoneScreenShotImagePath");
                                                                    {
                                                                        Object var_attrcontent55 = renderContext.call("xss", var_attrvalue54, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr57 = (((null != var_attrcontent55) && (!"".equals(var_attrcontent55))) && ((!"".equals(var_attrvalue54)) && (!((Object)false).equals(var_attrvalue54))));
                                                                            if (var_shoulddisplayattr57) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr56 = (var_attrvalue54.equals(true));
                                                                                    if (!var_istrueattr56) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                        <source media=\"(min-width: 1200px)\"");
                                                                {
                                                                    Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "webPhoneScreenShotImagePath");
                                                                    {
                                                                        Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                                                                            if (var_shoulddisplayattr61) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                                                                                    if (!var_istrueattr60) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                        <img");
                                                                {
                                                                    Object var_attrvalue62 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "phoneScreenshotImageAltText");
                                                                    {
                                                                        Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                                                                            if (var_shoulddisplayattr65) {
                                                                                out.write(" alt");
                                                                                {
                                                                                    boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                                                                                    if (!var_istrueattr64) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue66 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "webPhoneScreenShotImagePath");
                                                                    {
                                                                        Object var_attrcontent67 = renderContext.call("xss", var_attrvalue66, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr69 = (((null != var_attrcontent67) && (!"".equals(var_attrcontent67))) && ((!"".equals(var_attrvalue66)) && (!((Object)false).equals(var_attrvalue66))));
                                                                            if (var_shoulddisplayattr69) {
                                                                                out.write(" src");
                                                                                {
                                                                                    boolean var_istrueattr68 = (var_attrvalue66.equals(true));
                                                                                    if (!var_istrueattr68) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent67));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                      </picture>\r\n                    </span>\r\n                  </div>\r\n                </div>");
                                                            }
                                                        }
                                                        out.write("\r\n              ");
                                                    }
                                                }
                                            }
                                            var_index18++;
                                        }
                                        out.write("</div>");
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar11_list_coerced$ = null;
            }
            out.write("\r\n            </div>\r\n            ");
            {
                Object var_collectionvar70 = renderContext.getObjectModel().resolveProperty(_global_mastheadextend, "mastheadExtendCardArticleImageList");
                {
                    long var_size71 = ((var_collectionvar70_list_coerced$ == null ? (var_collectionvar70_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar70)) : var_collectionvar70_list_coerced$).size());
                    {
                        boolean var_notempty72 = (var_size71 > 0);
                        if (var_notempty72) {
                            {
                                long var_end75 = var_size71;
                                {
                                    boolean var_validstartstepend76 = (((0 < var_size71) && true) && (var_end75 > 0));
                                    if (var_validstartstepend76) {
                                        out.write("<div class=\"card-gallery__aticle\">");
                                        if (var_collectionvar70_list_coerced$ == null) {
                                            var_collectionvar70_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar70);
                                        }
                                        long var_index77 = 0;
                                        for (Object articleimage : var_collectionvar70_list_coerced$) {
                                            {
                                                boolean var_traversal79 = (((var_index77 >= 0) && (var_index77 <= var_end75)) && true);
                                                if (var_traversal79) {
                                                    out.write("\r\n              <div class=\"card-gallery__aticle-item\">\r\n                <div class=\"aticle-item__transparent\">\r\n                  <div class=\"aticle-item__transparent-content\">\r\n                    <div class=\"transparent-content__img\">\r\n                      <span>\r\n                          <picture>\r\n                            <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue80 = renderContext.getObjectModel().resolveProperty(articleimage, "mobileCardArticleImagePath");
                                                        {
                                                            Object var_attrcontent81 = renderContext.call("xss", var_attrvalue80, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr83 = (((null != var_attrcontent81) && (!"".equals(var_attrcontent81))) && ((!"".equals(var_attrvalue80)) && (!((Object)false).equals(var_attrvalue80))));
                                                                if (var_shoulddisplayattr83) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr82 = (var_attrvalue80.equals(true));
                                                                        if (!var_istrueattr82) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent81));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue84 = renderContext.getObjectModel().resolveProperty(articleimage, "mobileCardArticleImagePath");
                                                        {
                                                            Object var_attrcontent85 = renderContext.call("xss", var_attrvalue84, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr87 = (((null != var_attrcontent85) && (!"".equals(var_attrcontent85))) && ((!"".equals(var_attrvalue84)) && (!((Object)false).equals(var_attrvalue84))));
                                                                if (var_shoulddisplayattr87) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr86 = (var_attrvalue84.equals(true));
                                                                        if (!var_istrueattr86) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent85));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue88 = renderContext.getObjectModel().resolveProperty(articleimage, "webCardArticleImagePath");
                                                        {
                                                            Object var_attrcontent89 = renderContext.call("xss", var_attrvalue88, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr91 = (((null != var_attrcontent89) && (!"".equals(var_attrcontent89))) && ((!"".equals(var_attrvalue88)) && (!((Object)false).equals(var_attrvalue88))));
                                                                if (var_shoulddisplayattr91) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr90 = (var_attrvalue88.equals(true));
                                                                        if (!var_istrueattr90) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(articleimage, "webCardArticleImagePath");
                                                        {
                                                            Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                if (var_shoulddisplayattr95) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                        if (!var_istrueattr94) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <img");
                                                    {
                                                        Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(articleimage, "cardArticleImageAltText");
                                                        {
                                                            Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                                if (var_shoulddisplayattr99) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                                        if (!var_istrueattr98) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(articleimage, "webCardArticleImagePath");
                                                        {
                                                            Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                                if (var_shoulddisplayattr103) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                                        if (!var_istrueattr102) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                          </picture>\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"transparent-content__text\">");
                                                    {
                                                        String var_104 = (("\r\n                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(articleimage, "cardArticleTitle"), "html"))) + "\r\n                      ");
                                                        out.write(renderContext.getObjectModel().toString(var_104));
                                                    }
                                                    out.write("<div class=\"transparent-content__text-body\">");
                                                    {
                                                        String var_105 = (("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(articleimage, "cardArticleDescrition"), "html"))) + "\r\n                      ");
                                                        out.write(renderContext.getObjectModel().toString(var_105));
                                                    }
                                                    out.write("</div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            ");
                                                }
                                            }
                                            var_index77++;
                                        }
                                        out.write("</div>");
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar70_list_coerced$ = null;
            }
            out.write("\r\n          </div>\r\n        </section>\r\n      </div>\r\n        \r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar106 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions107_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions107 = obj().with("isEmpty", var_templateoptions107_field$_isempty);
            callUnit(out, renderContext, var_templatevar106, var_templateoptions107);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

