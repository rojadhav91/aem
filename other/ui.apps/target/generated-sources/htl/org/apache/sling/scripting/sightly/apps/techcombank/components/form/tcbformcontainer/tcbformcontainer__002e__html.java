/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.form.tcbformcontainer;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class tcbformcontainer__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_container = null;
Object _global_grid = null;
Object _dynamic_properties = bindings.get("properties");
Collection var_collectionvar29_list_coerced$ = null;
Object _dynamic_resource = bindings.get("resource");
Collection var_collectionvar53_list_coerced$ = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
out.write("<div class=\"tcb-form-wrapper\">\r\n     ");
_global_container = renderContext.call("use", com.adobe.cq.wcm.core.components.models.form.Container.class.getName(), obj());
_global_grid = renderContext.call("use", com.day.cq.wcm.foundation.model.responsivegrid.ResponsiveGrid.class.getName(), obj());
out.write("<form");
{
    Object var_attrvalue0 = renderContext.getObjectModel().resolveProperty(_global_container, "method");
    {
        Object var_attrcontent1 = renderContext.call("xss", var_attrvalue0, "attribute");
        {
            boolean var_shoulddisplayattr3 = (((null != var_attrcontent1) && (!"".equals(var_attrcontent1))) && ((!"".equals(var_attrvalue0)) && (!((Object)false).equals(var_attrvalue0))));
            if (var_shoulddisplayattr3) {
                out.write(" method");
                {
                    boolean var_istrueattr2 = (var_attrvalue0.equals(true));
                    if (!var_istrueattr2) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue4 = renderContext.call("uriManipulation", renderContext.getObjectModel().resolveProperty(_global_container, "action"), obj().with("addSelectors", "forms"));
    {
        Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "uri");
        {
            boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
            if (var_shoulddisplayattr7) {
                out.write(" action");
                {
                    boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                    if (!var_istrueattr6) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue8 = renderContext.getObjectModel().resolveProperty(_global_container, "id");
    {
        Object var_attrcontent9 = renderContext.call("xss", var_attrvalue8, "attribute");
        {
            boolean var_shoulddisplayattr11 = (((null != var_attrcontent9) && (!"".equals(var_attrcontent9))) && ((!"".equals(var_attrvalue8)) && (!((Object)false).equals(var_attrvalue8))));
            if (var_shoulddisplayattr11) {
                out.write(" id");
                {
                    boolean var_istrueattr10 = (var_attrvalue8.equals(true));
                    if (!var_istrueattr10) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent9));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(_global_container, "name");
    {
        Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
        {
            boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
            if (var_shoulddisplayattr15) {
                out.write(" name");
                {
                    boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                    if (!var_istrueattr14) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(_global_container, "enctype");
    {
        Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
        {
            boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
            if (var_shoulddisplayattr19) {
                out.write(" enctype");
                {
                    boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                    if (!var_istrueattr18) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrcontent20 = (("cmp-form " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_grid, "cssClass"), "attribute"))) + " test");
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent20));
    out.write("\"");
}
{
    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "successModelId");
    {
        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
        {
            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
            if (var_shoulddisplayattr24) {
                out.write(" data-success-modalId");
                {
                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                    if (!var_istrueattr23) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "failureModelId");
    {
        Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
        {
            boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
            if (var_shoulddisplayattr28) {
                out.write(" data-failure-modalId");
                {
                    boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                    if (!var_istrueattr27) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-tracking-click-event=\"formStart\" data-tracking-click-info-value=\"{'formName': 'formTitle'}\" data-tracking-click-info-key=\"formInfo\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'send form','type': 'other'}}\">\r\n     ");
{
    Object var_testvariable36 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "errorMessageInfo");
    if (renderContext.getObjectModel().toBoolean(var_testvariable36)) {
        {
            Object var_collectionvar29 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "errorMessageInfo");
            {
                long var_size30 = ((var_collectionvar29_list_coerced$ == null ? (var_collectionvar29_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar29)) : var_collectionvar29_list_coerced$).size());
                {
                    boolean var_notempty31 = (var_size30 > 0);
                    if (var_notempty31) {
                        {
                            long var_end34 = var_size30;
                            {
                                boolean var_validstartstepend35 = (((0 < var_size30) && true) && (var_end34 > 0));
                                if (var_validstartstepend35) {
                                    out.write("<div class=\"cmp-form-error\">");
                                    if (var_collectionvar29_list_coerced$ == null) {
                                        var_collectionvar29_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar29);
                                    }
                                    long var_index37 = 0;
                                    for (Object item : var_collectionvar29_list_coerced$) {
                                        {
                                            boolean var_traversal39 = (((var_index37 >= 0) && (var_index37 <= var_end34)) && true);
                                            if (var_traversal39) {
                                                out.write("\r\n \r\n     ");
                                            }
                                        }
                                        var_index37++;
                                    }
                                    out.write("</div>");
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar29_list_coerced$ = null;
        }
    }
}
out.write("\r\n     <input type=\"hidden\" name=\":formstart\"");
{
    Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path");
    {
        Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
        {
            boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
            if (var_shoulddisplayattr43) {
                out.write(" value");
                {
                    boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                    if (!var_istrueattr42) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n     <input type=\"hidden\" name=\"_charset_\" value=\"utf-8\"/>\r\n     <input type=\"hidden\" name=\"formtype\"");
{
    Object var_attrvalue44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "formType"), "text");
    {
        boolean var_shoulddisplayattr47 = ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44)));
        if (var_shoulddisplayattr47) {
            out.write(" value");
            {
                boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                if (!var_istrueattr46) {
                    out.write("=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrvalue44));
                    out.write("\"");
                }
            }
        }
    }
}
out.write("/>\r\n \r\n \r\n     ");
{
    Object var_testvariable48 = renderContext.getObjectModel().resolveProperty(_dynamic_properties, "redirectPage");
    if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
        out.write("<input type=\"hidden\" name=\":redirectPage\"");
        {
            Object var_attrvalue49 = renderContext.call("uriManipulation", renderContext.getObjectModel().resolveProperty(_dynamic_properties, "redirectPage"), obj().with("extension", "html"));
            {
                Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                {
                    boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                    if (var_shoulddisplayattr52) {
                        out.write(" value");
                        {
                            boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                            if (!var_istrueattr51) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>");
    }
}
out.write("  \r\n \r\n     ");
{
    Object var_collectionvar53 = renderContext.getObjectModel().resolveProperty(_global_grid, "paragraphs");
    {
        long var_size54 = ((var_collectionvar53_list_coerced$ == null ? (var_collectionvar53_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar53)) : var_collectionvar53_list_coerced$).size());
        {
            boolean var_notempty55 = (var_size54 > 0);
            if (var_notempty55) {
                {
                    long var_end58 = var_size54;
                    {
                        boolean var_validstartstepend59 = (((0 < var_size54) && true) && (var_end58 > 0));
                        if (var_validstartstepend59) {
                            if (var_collectionvar53_list_coerced$ == null) {
                                var_collectionvar53_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar53);
                            }
                            long var_index60 = 0;
                            for (Object paragraph : var_collectionvar53_list_coerced$) {
                                {
                                    boolean var_traversal62 = (((var_index60 >= 0) && (var_index60 <= var_end58)) && true);
                                    if (var_traversal62) {
                                        {
                                            Object var_resourcecontent63 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(paragraph, "path"), obj().with("cssClassName", renderContext.getObjectModel().resolveProperty(paragraph, "cssClass")).with("decorationTagName", "div").with("resourceType", renderContext.getObjectModel().resolveProperty(paragraph, "resourceType")));
                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent63));
                                        }
                                    }
                                }
                                var_index60++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar53_list_coerced$ = null;
}
out.write("\r\n     ");
{
    Object var_testvariable64 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit") : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "preview")));
    if (renderContext.getObjectModel().toBoolean(var_testvariable64)) {
        {
            Object var_resourcecontent65 = renderContext.call("includeResource", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), obj().with("cssClassName", "new section aem-Grid-newComponent").with("decorationTagName", "div").with("appendPath", "/*").with("resourceType", renderContext.getObjectModel().resolveProperty(_global_container, "resourceTypeForDropArea")));
            out.write(renderContext.getObjectModel().toString(var_resourcecontent65));
        }
    }
}
out.write("\r\n      </form>\r\n      <input type=\"hidden\" id=\"form-submit-analytics\" data-tracking-click-event=\"formComplete\" data-tracking-click-info-value=\"{'formName': 'formTitle','formDetails':'formDetailsVal','hashedEmail':'hashedEmailVal','branchLocation':'branchLocationVal'}\" data-tracking-click-info-key=\"formInfo\" data-tracking-user-info-key=\"userInfo\" data-tracking-user-info-value=\"{'status': 'not logged-in','customerID':'','idfa':'','gaid':''}\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Send the application','type': 'Other'},'webPageDetail':{'isErrorPage': 'n'}}\"/>\r\n     <input type=\"hidden\" id=\"form-error-analytics\" data-tracking-click-event=\"error\" data-tracking-click-info-value=\"{'errorType': 'Form error','errorMessage':'errorMessageVal'}\" data-tracking-click-info-key=\"errorInfo\" data-tracking-form-info-value=\"{'formName': 'Form name'}\" data-tracking-form-info-key=\"formInfo\" data-tracking-user-info-key=\"userInfo\" data-tracking-user-info-value=\"{'status': 'not logged-in','customerID':'','idfa':'','gaid':''}\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'error','type': 'Other'},'webPageDetail':{'isErrorPage': 'n'}}\"/>\r\n </div>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

