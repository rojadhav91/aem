/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.tabeventcard;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class tabeventcard__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Object _global_variation = null;
Collection var_collectionvar5_list_coerced$ = null;
Collection var_collectionvar21_list_coerced$ = null;
Object _global_awardvar = null;
Collection var_collectionvar57_list_coerced$ = null;
Collection var_collectionvar71_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.TabEventCardModel.class.getName(), obj());
out.write("\r\n    ");
_global_variation = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "variationType"), "regular"));
if (renderContext.getObjectModel().toBoolean(_global_variation)) {
    out.write("\r\n        <div class=\"tabs-event-card\">\r\n            <div class=\"tcb-tab-content_container\">\r\n                ");
    {
        Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_model, "tabEventList");
        {
            long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
            {
                boolean var_notempty7 = (var_size6 > 0);
                if (var_notempty7) {
                    {
                        long var_end10 = var_size6;
                        {
                            boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                            if (var_validstartstepend11) {
                                if (var_collectionvar5_list_coerced$ == null) {
                                    var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                                }
                                long var_index12 = 0;
                                for (Object regularlist : var_collectionvar5_list_coerced$) {
                                    {
                                        boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                        if (var_traversal14) {
                                            out.write("\r\n                    <div class=\"news-card\">\r\n                        <picture class=\"news-card_cover\">\r\n                            <img class=\"news-card_cover-image\"");
                                            {
                                                Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(regularlist, "imageForEvent");
                                                {
                                                    Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                                        if (var_shoulddisplayattr18) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                                                if (!var_istrueattr17) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(" alt=\"Logo\"/>\r\n                        </picture>\r\n                        <div class=\"news-card_month\"><span>");
                                            {
                                                Object var_19 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventDate"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_19));
                                            }
                                            out.write("</span><small>");
                                            {
                                                Object var_20 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventMonth"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_20));
                                            }
                                            out.write("</small></div>\r\n                        <div class=\"news-card_info\">\r\n                            <div class=\"news-card_type\">\r\n                                ");
                                            {
                                                Object var_collectionvar21 = renderContext.getObjectModel().resolveProperty(regularlist, "eventTag");
                                                {
                                                    long var_size22 = ((var_collectionvar21_list_coerced$ == null ? (var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21)) : var_collectionvar21_list_coerced$).size());
                                                    {
                                                        boolean var_notempty23 = (var_size22 > 0);
                                                        if (var_notempty23) {
                                                            {
                                                                long var_end26 = var_size22;
                                                                {
                                                                    boolean var_validstartstepend27 = (((0 < var_size22) && true) && (var_end26 > 0));
                                                                    if (var_validstartstepend27) {
                                                                        if (var_collectionvar21_list_coerced$ == null) {
                                                                            var_collectionvar21_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar21);
                                                                        }
                                                                        long var_index28 = 0;
                                                                        for (Object taglist : var_collectionvar21_list_coerced$) {
                                                                            {
                                                                                boolean var_traversal30 = (((var_index28 >= 0) && (var_index28 <= var_end26)) && true);
                                                                                if (var_traversal30) {
                                                                                    out.write("\r\n                                    <span class=\"news-card_type-inner\">");
                                                                                    {
                                                                                        Object var_31 = renderContext.call("xss", taglist, "text");
                                                                                        out.write(renderContext.getObjectModel().toString(var_31));
                                                                                    }
                                                                                    out.write("</span>\r\n                                ");
                                                                                }
                                                                            }
                                                                            var_index28++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                var_collectionvar21_list_coerced$ = null;
                                            }
                                            out.write("\r\n                                <span class=\"news-card_time\">");
                                            {
                                                Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventTime"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_32));
                                            }
                                            out.write("</span>\r\n                            </div>\r\n                            <div class=\"news-card_title\">");
                                            {
                                                String var_33 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventHeading"), "text"))) + "\r\n                            ");
                                                out.write(renderContext.getObjectModel().toString(var_33));
                                            }
                                            out.write("</div>\r\n                            <div class=\"news-card_description\">");
                                            {
                                                String var_34 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventDesription"), "html"))) + "\r\n                            ");
                                                out.write(renderContext.getObjectModel().toString(var_34));
                                            }
                                            out.write("</div>\r\n                            ");
                                            {
                                                Object var_testvariable35 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(regularlist, "ctaButtonLabel")) ? renderContext.getObjectModel().resolveProperty(regularlist, "ctaButtonLink") : renderContext.getObjectModel().resolveProperty(regularlist, "ctaButtonLabel")));
                                                if (renderContext.getObjectModel().toBoolean(var_testvariable35)) {
                                                    out.write("\r\n                                <div class=\"news-card_actions\">\r\n                                    <a class=\"tcb-button tcb-button--link\" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent36 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "linkUrl"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent37 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "eventHeading"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(regularlist, "ctaButtonLink");
                                                        {
                                                            Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                                if (var_shoulddisplayattr41) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                        if (!var_istrueattr40) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue42 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(regularlist, "ctaTarget"), "true")) ? "_blank" : "_self");
                                                        {
                                                            Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
                                                                if (var_shoulddisplayattr45) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                                                                        if (!var_istrueattr44) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        String var_46 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(regularlist, "ctaButtonLabel"), "text"))) + "\r\n                                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_46));
                                                    }
                                                    out.write("</a>\r\n                                </div>\r\n                            ");
                                                }
                                            }
                                            out.write("\r\n                        </div>\r\n                    </div>\r\n                ");
                                        }
                                    }
                                    var_index12++;
                                }
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar5_list_coerced$ = null;
    }
    out.write("\r\n            </div>\r\n            ");
    {
        Object var_testvariable47 = renderContext.getObjectModel().resolveProperty(_global_model, "viewAll");
        if (renderContext.getObjectModel().toBoolean(var_testvariable47)) {
            out.write("<div class=\"read-more\">\r\n                <a");
            {
                Object var_attrvalue48 = renderContext.call("uriManipulation", renderContext.getObjectModel().resolveProperty(_global_model, "link"), obj().with("extension", "html"));
                {
                    Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "uri");
                    {
                        boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                        if (var_shoulddisplayattr51) {
                            out.write(" href");
                            {
                                boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                if (!var_istrueattr50) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue52 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "newTab"), "true")) ? "_blank" : "_self");
                {
                    Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
                    {
                        boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                        if (var_shoulddisplayattr55) {
                            out.write(" target");
                            {
                                boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                if (!var_istrueattr54) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">");
            {
                Object var_56 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewAll"), "text");
                out.write(renderContext.getObjectModel().toString(var_56));
            }
            out.write("</a>\r\n                <div class=\"expand\">\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                </div>\r\n            </div>");
        }
    }
    out.write("\r\n        </div>\r\n    ");
}
out.write("\r\n    ");
_global_awardvar = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "variationType"), "awards"));
if (renderContext.getObjectModel().toBoolean(_global_awardvar)) {
    out.write("\r\n        <div class=\"tabs-event-card\">\r\n            <div class=\"tcb-tab-content_container\">\r\n                ");
    {
        Object var_collectionvar57 = renderContext.getObjectModel().resolveProperty(_global_model, "tabEventList");
        {
            long var_size58 = ((var_collectionvar57_list_coerced$ == null ? (var_collectionvar57_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar57)) : var_collectionvar57_list_coerced$).size());
            {
                boolean var_notempty59 = (var_size58 > 0);
                if (var_notempty59) {
                    {
                        long var_end62 = var_size58;
                        {
                            boolean var_validstartstepend63 = (((0 < var_size58) && true) && (var_end62 > 0));
                            if (var_validstartstepend63) {
                                if (var_collectionvar57_list_coerced$ == null) {
                                    var_collectionvar57_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar57);
                                }
                                long var_index64 = 0;
                                for (Object awardlist : var_collectionvar57_list_coerced$) {
                                    {
                                        boolean var_traversal66 = (((var_index64 >= 0) && (var_index64 <= var_end62)) && true);
                                        if (var_traversal66) {
                                            out.write("\r\n                    <div class=\"news-card\">\r\n                        <picture class=\"news-card_cover\">\r\n                            <img class=\"news-card_cover-image\"");
                                            {
                                                Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(awardlist, "imageForEvent");
                                                {
                                                    Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                                                        if (var_shoulddisplayattr70) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                                                if (!var_istrueattr69) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write(" alt=\"Logo\"/>\r\n                        </picture>\r\n                        <div class=\"news-card_info\">\r\n                            <div class=\"news-card_type\">\r\n                                ");
                                            {
                                                Object var_collectionvar71 = renderContext.getObjectModel().resolveProperty(awardlist, "eventTag");
                                                {
                                                    long var_size72 = ((var_collectionvar71_list_coerced$ == null ? (var_collectionvar71_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar71)) : var_collectionvar71_list_coerced$).size());
                                                    {
                                                        boolean var_notempty73 = (var_size72 > 0);
                                                        if (var_notempty73) {
                                                            {
                                                                long var_end76 = var_size72;
                                                                {
                                                                    boolean var_validstartstepend77 = (((0 < var_size72) && true) && (var_end76 > 0));
                                                                    if (var_validstartstepend77) {
                                                                        if (var_collectionvar71_list_coerced$ == null) {
                                                                            var_collectionvar71_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar71);
                                                                        }
                                                                        long var_index78 = 0;
                                                                        for (Object taglist : var_collectionvar71_list_coerced$) {
                                                                            {
                                                                                boolean var_traversal80 = (((var_index78 >= 0) && (var_index78 <= var_end76)) && true);
                                                                                if (var_traversal80) {
                                                                                    out.write("\r\n                                    <span class=\"news-card_type-inner\">");
                                                                                    {
                                                                                        Object var_81 = renderContext.call("xss", taglist, "text");
                                                                                        out.write(renderContext.getObjectModel().toString(var_81));
                                                                                    }
                                                                                    out.write("</span>\r\n                                ");
                                                                                }
                                                                            }
                                                                            var_index78++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                var_collectionvar71_list_coerced$ = null;
                                            }
                                            out.write("\r\n                            </div>\r\n                            <div class=\"news-card_title\">");
                                            {
                                                String var_82 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "eventHeading"), "text"))) + "\r\n                            ");
                                                out.write(renderContext.getObjectModel().toString(var_82));
                                            }
                                            out.write("</div>\r\n                            <div class=\"news-card_description\">\r\n                                <p>");
                                            {
                                                String var_83 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "awardLabelText"), "text")) + ": ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "eventSubHeading"), "text")));
                                                out.write(renderContext.getObjectModel().toString(var_83));
                                            }
                                            out.write("</p>\r\n                            </div>\r\n                            ");
                                            {
                                                Object var_testvariable84 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(awardlist, "ctaButtonLabel")) ? renderContext.getObjectModel().resolveProperty(awardlist, "ctaButtonLink") : renderContext.getObjectModel().resolveProperty(awardlist, "ctaButtonLabel")));
                                                if (renderContext.getObjectModel().toBoolean(var_testvariable84)) {
                                                    out.write("\r\n                                <div class=\"news-card_actions\">\r\n                                    <a class=\"tcb-button tcb-button--link\" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent85 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "linkUrl"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent85));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent86 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "eventHeading"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(awardlist, "ctaButtonLink");
                                                        {
                                                            Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                                                                if (var_shoulddisplayattr90) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                                                                        if (!var_istrueattr89) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue91 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(awardlist, "ctaTarget"), "true")) ? "_blank" : "_self");
                                                        {
                                                            Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                                                                if (var_shoulddisplayattr94) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                                                                        if (!var_istrueattr93) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        String var_95 = (("\r\n                                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(awardlist, "ctaButtonLabel"), "text"))) + "\r\n                                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_95));
                                                    }
                                                    out.write("</a>\r\n                                </div>\r\n                            ");
                                                }
                                            }
                                            out.write("\r\n                        </div>\r\n                    </div>\r\n                ");
                                        }
                                    }
                                    var_index64++;
                                }
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar57_list_coerced$ = null;
    }
    out.write("\r\n            </div>\r\n            ");
    {
        Object var_testvariable96 = renderContext.getObjectModel().resolveProperty(_global_model, "viewAll");
        if (renderContext.getObjectModel().toBoolean(var_testvariable96)) {
            out.write("<div class=\"read-more\">\r\n                <a");
            {
                Object var_attrvalue97 = renderContext.call("uriManipulation", renderContext.getObjectModel().resolveProperty(_global_model, "link"), obj().with("extension", "html"));
                {
                    Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "uri");
                    {
                        boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                        if (var_shoulddisplayattr100) {
                            out.write(" href");
                            {
                                boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                                if (!var_istrueattr99) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrvalue101 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_model, "newTab"), "true")) ? "_blank" : "_self");
                {
                    Object var_attrcontent102 = renderContext.call("xss", var_attrvalue101, "attribute");
                    {
                        boolean var_shoulddisplayattr104 = (((null != var_attrcontent102) && (!"".equals(var_attrcontent102))) && ((!"".equals(var_attrvalue101)) && (!((Object)false).equals(var_attrvalue101))));
                        if (var_shoulddisplayattr104) {
                            out.write(" target");
                            {
                                boolean var_istrueattr103 = (var_attrvalue101.equals(true));
                                if (!var_istrueattr103) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">");
            {
                Object var_105 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewAll"), "text");
                out.write(renderContext.getObjectModel().toString(var_105));
            }
            out.write("</a>\r\n                <div class=\"expand\">\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                </div>\r\n            </div>");
        }
    }
    out.write("\r\n        </div>\r\n    ");
}
out.write("\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

