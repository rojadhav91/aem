/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.debttabspanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class debttabspanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.DebtPanel.class.getName(), obj());
out.write("\r\n    <section class=\"container insurance-calculation save-calculation \">\r\n        <div class=\"insurance-calculation__container\">\r\n            <div class=\"insurance-calculation__content\">\r\n                <div class=\"insurance-calculation__panel\">\r\n                    <div class=\"panel-inputs\">\r\n                        <div class=\"input-items\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent5 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
out.write(" data-tracking-click-info-value=\"{'calculatorName':'Debt Panel', 'calculatorFields':'' }\">\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\"");
{
    Object var_attrvalue6 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool")) ? renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool") : "");
    {
        Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
        {
            boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
            if (var_shoulddisplayattr9) {
                out.write(" title");
                {
                    boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                    if (!var_istrueattr8) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_10 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountLabel"), "text"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_10));
}
out.write("<div class=\"icon-info\">\r\n                                        ");
{
    Object var_testvariable11 = renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool");
    if (renderContext.getObjectModel().toBoolean(var_testvariable11)) {
        out.write("<span");
        {
            Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool");
            {
                Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                {
                    boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                    if (var_shoulddisplayattr15) {
                        out.write(" title");
                        {
                            boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                            if (!var_istrueattr14) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                                            ");
        {
            Object var_testvariable16 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable16)) {
                out.write("<picture>\r\n                                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                        {
                            boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                            if (var_shoulddisplayattr20) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                    if (!var_istrueattr19) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                        {
                            boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                            if (var_shoulddisplayattr24) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                    if (!var_istrueattr23) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                        {
                            boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                            if (var_shoulddisplayattr28) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                    if (!var_istrueattr27) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                        {
                            boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                            if (var_shoulddisplayattr32) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                    if (!var_istrueattr31) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <img");
                {
                    Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "uri");
                        {
                            boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                            if (var_shoulddisplayattr36) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                    if (!var_istrueattr35) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipAltText");
                    {
                        Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                        {
                            boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                            if (var_shoulddisplayattr40) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                    if (!var_istrueattr39) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                </picture>");
            }
        }
        out.write("\r\n                                        </span>");
    }
}
out.write("\r\n                                        ");
{
    Object var_testvariable41 = renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool");
    if (renderContext.getObjectModel().toBoolean(var_testvariable41)) {
        out.write("<div class=\"tooltiptext\">");
        {
            String var_42 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountTool"), "text"))) + "\r\n                                        ");
            out.write(renderContext.getObjectModel().toString(var_42));
        }
        out.write("</div>");
    }
}
out.write("\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                                    <input aria-invalid=\"false\" name=\"depositMoney\" type=\"text\" maxlength=\"20\" title=\"\" class=\"date-time-wrapper__input money__input-field\" value=\"\"/>\r\n                                    <div class=\"date-time-wrapper__input-extra\">\r\n                                        <p class=\"currency__place-holder\">");
{
    Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "depositAmountInput"), "text");
    out.write(renderContext.getObjectModel().toString(var_43));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\">");
{
    String var_44 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "interestRate"), "text"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_44));
}
out.write("<div class=\"icon-info\">\r\n                                        ");
{
    Object var_testvariable45 = renderContext.getObjectModel().resolveProperty(_global_model, "interestRateTool");
    if (renderContext.getObjectModel().toBoolean(var_testvariable45)) {
        out.write("<span");
        {
            Object var_attrvalue46 = renderContext.getObjectModel().resolveProperty(_global_model, "interestRateTool");
            {
                Object var_attrcontent47 = renderContext.call("xss", var_attrvalue46, "attribute");
                {
                    boolean var_shoulddisplayattr49 = (((null != var_attrcontent47) && (!"".equals(var_attrcontent47))) && ((!"".equals(var_attrvalue46)) && (!((Object)false).equals(var_attrvalue46))));
                    if (var_shoulddisplayattr49) {
                        out.write(" title");
                        {
                            boolean var_istrueattr48 = (var_attrvalue46.equals(true));
                            if (!var_istrueattr48) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent47));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                                            ");
        {
            Object var_testvariable50 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable50)) {
                out.write("<picture>\r\n                                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                        {
                            boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                            if (var_shoulddisplayattr54) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                                    if (!var_istrueattr53) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                        {
                            boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                            if (var_shoulddisplayattr58) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                                    if (!var_istrueattr57) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue59 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
                        {
                            boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
                            if (var_shoulddisplayattr62) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                                    if (!var_istrueattr61) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue63 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
                        {
                            boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
                            if (var_shoulddisplayattr66) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                                    if (!var_istrueattr65) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <img");
                {
                    Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "uri");
                        {
                            boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                            if (var_shoulddisplayattr70) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                    if (!var_istrueattr69) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipAltText");
                    {
                        Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                        {
                            boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                            if (var_shoulddisplayattr74) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                    if (!var_istrueattr73) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                            </picture>");
            }
        }
        out.write("\r\n                                        </span>");
    }
}
out.write("\r\n                                        ");
{
    Object var_testvariable75 = renderContext.getObjectModel().resolveProperty(_global_model, "interestRateTool");
    if (renderContext.getObjectModel().toBoolean(var_testvariable75)) {
        out.write("<div class=\"tooltiptext\">");
        {
            String var_76 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "interestRateTool"), "text"))) + "\r\n                                        ");
            out.write(renderContext.getObjectModel().toString(var_76));
        }
        out.write("</div>");
    }
}
out.write("\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"tcb-input--save-calc\" data-value=\"0\"");
{
    Object var_attrvalue77 = renderContext.getObjectModel().resolveProperty(_global_model, "interestRateSlider");
    {
        Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "attribute");
        {
            boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
            if (var_shoulddisplayattr80) {
                out.write(" data-min");
                {
                    boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                    if (!var_istrueattr79) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-max=\"10\" data-use-label=\"true\" data-use-value-popover=\"true\" data-unit=\"%\"></div>\r\n                            </div>\r\n                            <div class=\"item__label\">\r\n                                <div class=\"label__text\"");
{
    Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_global_model, "termToolTip");
    {
        Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "attribute");
        {
            boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
            if (var_shoulddisplayattr84) {
                out.write(" title");
                {
                    boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                    if (!var_istrueattr83) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">");
{
    String var_85 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "termLabelText"), "text"))) + "\r\n                                    ");
    out.write(renderContext.getObjectModel().toString(var_85));
}
out.write("<div class=\"icon-info\">\r\n                                        ");
{
    Object var_testvariable86 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
    if (renderContext.getObjectModel().toBoolean(var_testvariable86)) {
        out.write("<span");
        {
            Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(_global_model, "termToolTip");
            {
                Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "attribute");
                {
                    boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                    if (var_shoulddisplayattr90) {
                        out.write(" title");
                        {
                            boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                            if (!var_istrueattr89) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                                            ");
        {
            Object var_testvariable91 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
            if (renderContext.getObjectModel().toBoolean(var_testvariable91)) {
                out.write("<picture>\r\n                                                <source media=\"(max-width: 360px)\"");
                {
                    Object var_attrvalue92 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                        {
                            boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                            if (var_shoulddisplayattr95) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                    if (!var_istrueattr94) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(max-width: 576px)\"");
                {
                    Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIconMobile");
                    {
                        Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                        {
                            boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                            if (var_shoulddisplayattr99) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                    if (!var_istrueattr98) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1080px)\"");
                {
                    Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                        {
                            boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                            if (var_shoulddisplayattr103) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                    if (!var_istrueattr102) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <source media=\"(min-width: 1200px)\"");
                {
                    Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                        {
                            boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                            if (var_shoulddisplayattr107) {
                                out.write(" srcset");
                                {
                                    boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                    if (!var_istrueattr106) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                                <img");
                {
                    Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipIcon");
                    {
                        Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "uri");
                        {
                            boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                            if (var_shoulddisplayattr111) {
                                out.write(" src");
                                {
                                    boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                    if (!var_istrueattr110) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(_global_model, "toolTipAltText");
                    {
                        Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                        {
                            boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                            if (var_shoulddisplayattr115) {
                                out.write(" alt");
                                {
                                    boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                    if (!var_istrueattr114) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                out.write("/>\r\n                                            </picture>");
            }
        }
        out.write("\r\n                                        </span>");
    }
}
out.write("\r\n                                        ");
{
    Object var_testvariable116 = renderContext.getObjectModel().resolveProperty(_global_model, "termToolTip");
    if (renderContext.getObjectModel().toBoolean(var_testvariable116)) {
        out.write("<div class=\"tooltiptext\">");
        {
            Object var_117 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "termToolTip"), "text");
            out.write(renderContext.getObjectModel().toString(var_117));
        }
        out.write("</div>");
    }
}
out.write("\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"item__input-fields\">\r\n                                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                                    <input aria-invalid=\"false\" name=\"depositMonth\" type=\"text\" maxlength=\"3\" min=\"0\" class=\"date-time-wrapper__input month__input-field\" value=\"\"/>\r\n                                    <div class=\"date-time-wrapper__input-extra\">\r\n                                        <p class=\"currency__place-holder\">");
{
    Object var_118 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "monthText"), "text");
    out.write(renderContext.getObjectModel().toString(var_118));
}
out.write("</p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"panel-info\">\r\n                        <span>\r\n                            <picture>\r\n                                <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue119 = renderContext.getObjectModel().resolveProperty(_global_model, "outputPanelBgMobile");
    {
        Object var_attrcontent120 = renderContext.call("xss", var_attrvalue119, "attribute");
        {
            boolean var_shoulddisplayattr122 = (((null != var_attrcontent120) && (!"".equals(var_attrcontent120))) && ((!"".equals(var_attrvalue119)) && (!((Object)false).equals(var_attrvalue119))));
            if (var_shoulddisplayattr122) {
                out.write(" srcset");
                {
                    boolean var_istrueattr121 = (var_attrvalue119.equals(true));
                    if (!var_istrueattr121) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent120));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue123 = renderContext.getObjectModel().resolveProperty(_global_model, "outputPanelBgMobile");
    {
        Object var_attrcontent124 = renderContext.call("xss", var_attrvalue123, "attribute");
        {
            boolean var_shoulddisplayattr126 = (((null != var_attrcontent124) && (!"".equals(var_attrcontent124))) && ((!"".equals(var_attrvalue123)) && (!((Object)false).equals(var_attrvalue123))));
            if (var_shoulddisplayattr126) {
                out.write(" srcset");
                {
                    boolean var_istrueattr125 = (var_attrvalue123.equals(true));
                    if (!var_istrueattr125) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent124));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue127 = renderContext.getObjectModel().resolveProperty(_global_model, "outputPanelBg");
    {
        Object var_attrcontent128 = renderContext.call("xss", var_attrvalue127, "attribute");
        {
            boolean var_shoulddisplayattr130 = (((null != var_attrcontent128) && (!"".equals(var_attrcontent128))) && ((!"".equals(var_attrvalue127)) && (!((Object)false).equals(var_attrvalue127))));
            if (var_shoulddisplayattr130) {
                out.write(" srcset");
                {
                    boolean var_istrueattr129 = (var_attrvalue127.equals(true));
                    if (!var_istrueattr129) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent128));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue131 = renderContext.getObjectModel().resolveProperty(_global_model, "outputPanelBg");
    {
        Object var_attrcontent132 = renderContext.call("xss", var_attrvalue131, "attribute");
        {
            boolean var_shoulddisplayattr134 = (((null != var_attrcontent132) && (!"".equals(var_attrcontent132))) && ((!"".equals(var_attrvalue131)) && (!((Object)false).equals(var_attrvalue131))));
            if (var_shoulddisplayattr134) {
                out.write(" srcset");
                {
                    boolean var_istrueattr133 = (var_attrvalue131.equals(true));
                    if (!var_istrueattr133) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent132));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                <img");
{
    Object var_attrvalue135 = renderContext.getObjectModel().resolveProperty(_global_model, "outputPanelBg");
    {
        Object var_attrcontent136 = renderContext.call("xss", var_attrvalue135, "uri");
        {
            boolean var_shoulddisplayattr138 = (((null != var_attrcontent136) && (!"".equals(var_attrcontent136))) && ((!"".equals(var_attrvalue135)) && (!((Object)false).equals(var_attrvalue135))));
            if (var_shoulddisplayattr138) {
                out.write(" src");
                {
                    boolean var_istrueattr137 = (var_attrvalue135.equals(true));
                    if (!var_istrueattr137) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent136));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue139 = renderContext.getObjectModel().resolveProperty(_global_model, "bgImageAltText");
    {
        Object var_attrcontent140 = renderContext.call("xss", var_attrvalue139, "attribute");
        {
            boolean var_shoulddisplayattr142 = (((null != var_attrcontent140) && (!"".equals(var_attrcontent140))) && ((!"".equals(var_attrvalue139)) && (!((Object)false).equals(var_attrvalue139))));
            if (var_shoulddisplayattr142) {
                out.write(" alt");
                {
                    boolean var_istrueattr141 = (var_attrvalue139.equals(true));
                    if (!var_istrueattr141) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent140));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                            </picture>\r\n                        </span>\r\n                        <div class=\"panel-info__content\">\r\n                            <div class=\"panel-info__content-text\">\r\n                                <div class=\"info-content-text__icon\">\r\n                                    <span>\r\n                                        <picture>\r\n                                            <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue143 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImageMobile");
    {
        Object var_attrcontent144 = renderContext.call("xss", var_attrvalue143, "attribute");
        {
            boolean var_shoulddisplayattr146 = (((null != var_attrcontent144) && (!"".equals(var_attrcontent144))) && ((!"".equals(var_attrvalue143)) && (!((Object)false).equals(var_attrvalue143))));
            if (var_shoulddisplayattr146) {
                out.write(" srcset");
                {
                    boolean var_istrueattr145 = (var_attrvalue143.equals(true));
                    if (!var_istrueattr145) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent144));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue147 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImageMobile");
    {
        Object var_attrcontent148 = renderContext.call("xss", var_attrvalue147, "attribute");
        {
            boolean var_shoulddisplayattr150 = (((null != var_attrcontent148) && (!"".equals(var_attrcontent148))) && ((!"".equals(var_attrvalue147)) && (!((Object)false).equals(var_attrvalue147))));
            if (var_shoulddisplayattr150) {
                out.write(" srcset");
                {
                    boolean var_istrueattr149 = (var_attrvalue147.equals(true));
                    if (!var_istrueattr149) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent148));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue151 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImage");
    {
        Object var_attrcontent152 = renderContext.call("xss", var_attrvalue151, "attribute");
        {
            boolean var_shoulddisplayattr154 = (((null != var_attrcontent152) && (!"".equals(var_attrcontent152))) && ((!"".equals(var_attrvalue151)) && (!((Object)false).equals(var_attrvalue151))));
            if (var_shoulddisplayattr154) {
                out.write(" srcset");
                {
                    boolean var_istrueattr153 = (var_attrvalue151.equals(true));
                    if (!var_istrueattr153) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent152));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue155 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImage");
    {
        Object var_attrcontent156 = renderContext.call("xss", var_attrvalue155, "attribute");
        {
            boolean var_shoulddisplayattr158 = (((null != var_attrcontent156) && (!"".equals(var_attrcontent156))) && ((!"".equals(var_attrvalue155)) && (!((Object)false).equals(var_attrvalue155))));
            if (var_shoulddisplayattr158) {
                out.write(" srcset");
                {
                    boolean var_istrueattr157 = (var_attrvalue155.equals(true));
                    if (!var_istrueattr157) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent156));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                            <img");
{
    Object var_attrvalue159 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImage");
    {
        Object var_attrcontent160 = renderContext.call("xss", var_attrvalue159, "uri");
        {
            boolean var_shoulddisplayattr162 = (((null != var_attrcontent160) && (!"".equals(var_attrcontent160))) && ((!"".equals(var_attrvalue159)) && (!((Object)false).equals(var_attrvalue159))));
            if (var_shoulddisplayattr162) {
                out.write(" src");
                {
                    boolean var_istrueattr161 = (var_attrvalue159.equals(true));
                    if (!var_istrueattr161) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent160));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue163 = renderContext.getObjectModel().resolveProperty(_global_model, "iconImageAlt");
    {
        Object var_attrcontent164 = renderContext.call("xss", var_attrvalue163, "attribute");
        {
            boolean var_shoulddisplayattr166 = (((null != var_attrcontent164) && (!"".equals(var_attrcontent164))) && ((!"".equals(var_attrvalue163)) && (!((Object)false).equals(var_attrvalue163))));
            if (var_shoulddisplayattr166) {
                out.write(" alt");
                {
                    boolean var_istrueattr165 = (var_attrvalue163.equals(true));
                    if (!var_istrueattr165) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent164));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" decoding=\"async\" data-nimg=\"fixed\"/>\r\n                                        </picture>\r\n                                    </span>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"panel-info__content-text\">\r\n                                <div class=\"info-content-text__label\">\r\n                                    <div class=\"dark-gray\">");
{
    Object var_167 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "interestPaidLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_167));
}
out.write("</div>\r\n                                    <h3 id=\"profit-value--save-calc\">");
{
    String var_168 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "interestPaidOutputAmount"), "text")));
    out.write(renderContext.getObjectModel().toString(var_168));
}
out.write("</h3>\r\n                                </div>\r\n                                <div class=\"info-content-text__label\">\r\n                                    <div class=\"dark-gray\">");
{
    Object var_169 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "totalReceivedLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_169));
}
out.write("</div>\r\n                                    <h3 id=\"total-value--save-calc\">");
{
    String var_170 = ("0 " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "totalReceivedOutputAmount"), "text")));
    out.write(renderContext.getObjectModel().toString(var_170));
}
out.write("</h3>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

