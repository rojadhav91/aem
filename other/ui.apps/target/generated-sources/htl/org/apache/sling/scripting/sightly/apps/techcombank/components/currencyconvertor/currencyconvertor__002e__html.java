/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.currencyconvertor;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class currencyconvertor__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_currencyconvertormodel = null;
Object _dynamic_currentpage = bindings.get("currentpage");
Collection var_collectionvar12_list_coerced$ = null;
Collection var_collectionvar29_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n  <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_currencyconvertormodel = renderContext.call("use", com.techcombank.core.models.CurrencyConvertorModel.class.getName(), obj());
out.write("\r\n<section class=\"container\">\r\n    <div class=\"currency-converter\"");
{
    String var_attrcontent5 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_currentpage, "path"), "attribute")) + "/_jcr_content.exchange-rates.integration.json");
    out.write(" data-url=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
out.write(">\r\n        <div class=\"priority-exchange__container\">\r\n                <div class=\"priority-exchange__panel\">\r\n                    <div class=\"panel-item\">                       \r\n                        <div class=\"analytics-active-link panel-item__exchange-section\" data-tracking-click-event=\"calculator\"");
{
    String var_attrcontent6 = (("{'CalculatorName': 'Currency Exchange','CalculatorFields': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "calculatorField"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
    out.write("\"");
}
{
    Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "interactionNameType");
    {
        Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
        {
            boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
            if (var_shoulddisplayattr10) {
                out.write(" data-tracking-web-interaction-value");
                {
                    boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                    if (!var_istrueattr9) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                            <div class=\"exchange-section__transaction-type-selector\">\r\n                                <h3 class=\"exchange-section__label\">");
{
    Object var_11 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "transaction"), "text");
    out.write(renderContext.getObjectModel().toString(var_11));
}
out.write("</h3>\r\n                                <div class=\"exchange-section__tab-wrapper\">                                \r\n                                    <div class=\"exchange-section__tabs analytics-transaction-tab\">\r\n                                        ");
{
    Object var_collectionvar12 = renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "transactionTypeTab");
    {
        long var_size13 = ((var_collectionvar12_list_coerced$ == null ? (var_collectionvar12_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar12)) : var_collectionvar12_list_coerced$).size());
        {
            boolean var_notempty14 = (var_size13 > 0);
            if (var_notempty14) {
                {
                    long var_end17 = var_size13;
                    {
                        boolean var_validstartstepend18 = (((0 < var_size13) && true) && (var_end17 > 0));
                        if (var_validstartstepend18) {
                            if (var_collectionvar12_list_coerced$ == null) {
                                var_collectionvar12_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar12);
                            }
                            long var_index19 = 0;
                            for (Object transactiontabitem : var_collectionvar12_list_coerced$) {
                                {
                                    boolean transactiontabitemlist_field$_first = (var_index19 == 0);
                                    {
                                        boolean var_traversal21 = (((var_index19 >= 0) && (var_index19 <= var_end17)) && true);
                                        if (var_traversal21) {
                                            out.write("\r\n                                        \t<span");
                                            {
                                                Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(transactiontabitem, "transactionType");
                                                {
                                                    Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                        if (var_shoulddisplayattr25) {
                                                            out.write(" data-type");
                                                            {
                                                                boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                if (!var_istrueattr24) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            {
                                                String var_attrcontent26 = ("exchange-section__tabs-item " + renderContext.getObjectModel().toString(renderContext.call("xss", (transactiontabitemlist_field$_first ? "tab-active" : ""), "attribute")));
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                out.write("\"");
                                            }
                                            out.write(" aria-hidden=\"true\">");
                                            {
                                                Object var_27 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(transactiontabitem, "transactionLabel"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_27));
                                            }
                                            out.write("</span>\r\n                                        ");
                                        }
                                    }
                                }
                                var_index19++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar12_list_coerced$ = null;
}
out.write(" \r\n                                    </div>  \t\t\t\t\t\t\t\t \r\n                                </div>\r\n                            </div>\r\n                            <div class=\"exchange-section__currency-selector\">\r\n                                <div class=\"currency-selector__currency-item\">\r\n                                    <h3 class=\"exchange-section__label\">");
{
    Object var_28 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "foreignCurrency"), "text");
    out.write(renderContext.getObjectModel().toString(var_28));
}
out.write("</h3>\r\n                                    <div class=\"currency-item__inner\">\r\n                                        <div class=\"exchange-section__tabs scroll-currency-converter\">\r\n                                            <div class=\"exchange-section__tabs-scroller scroller analytics-currency-tab\">\r\n                                             ");
{
    Object var_collectionvar29 = renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "sourceCurrencyTab");
    {
        long var_size30 = ((var_collectionvar29_list_coerced$ == null ? (var_collectionvar29_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar29)) : var_collectionvar29_list_coerced$).size());
        {
            boolean var_notempty31 = (var_size30 > 0);
            if (var_notempty31) {
                {
                    long var_end34 = var_size30;
                    {
                        boolean var_validstartstepend35 = (((0 < var_size30) && true) && (var_end34 > 0));
                        if (var_validstartstepend35) {
                            if (var_collectionvar29_list_coerced$ == null) {
                                var_collectionvar29_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar29);
                            }
                            long var_index36 = 0;
                            for (Object sourcecurrencytabitem : var_collectionvar29_list_coerced$) {
                                {
                                    boolean sourcecurrencytabitemlist_field$_first = (var_index36 == 0);
                                    {
                                        boolean var_traversal38 = (((var_index36 >= 0) && (var_index36 <= var_end34)) && true);
                                        if (var_traversal38) {
                                            out.write("\r\n                                                 <div data-index=\"0\" tabindex=\"-1\" aria-hidden=\"true\">\r\n                               \t\t\t\t\t\t <span");
                                            {
                                                String var_attrcontent39 = ("exchange-section__tabs-item " + renderContext.getObjectModel().toString(renderContext.call("xss", (sourcecurrencytabitemlist_field$_first ? "tab-active" : ""), "attribute")));
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                out.write("\"");
                                            }
                                            out.write(" aria-hidden=\"true\" tabindex=\"-1\">");
                                            {
                                                Object var_40 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(sourcecurrencytabitem, "sourceCurrency"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_40));
                                            }
                                            out.write("</span>\r\n                                                </div>\r\n  \t\t\t\t\t\t\t\t\t\t\t ");
                                        }
                                    }
                                }
                                var_index36++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar29_list_coerced$ = null;
}
out.write("\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"exchange-section__input\">\r\n                                            <div class=\"item__input-fields\">\r\n                                                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                                                    <input aria-invalid=\"false\" type=\"text\"");
{
    Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "sourceCurrencyPlaceholder");
    {
        Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
        {
            boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
            if (var_shoulddisplayattr44) {
                out.write(" placeholder");
                {
                    boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                    if (!var_istrueattr43) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" maxlength=\"25\" name=\"totalPrice\" class=\"date-time-wrapper__input money__input-field\" value=\"\" pattern=\"[0-9]\"/>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"currency-selector__currency-item\">\r\n                                    <h3 class=\"exchange-section__label\">");
{
    Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "exchangeInfo"), "text");
    out.write(renderContext.getObjectModel().toString(var_45));
}
out.write("</h3>\r\n                                    <div class=\"currency-item__inner\">\r\n                                        <div class=\"exchange-section__tabs\">\r\n                                            <div class=\"exchange-section__tabs-scroller single-item\">\r\n                            <span class=\"exchange-section__tabs-item tab-active\" aria-hidden=\"true\" tabindex=\"-1\">");
{
    String var_46 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "targetCurrency"), "text")) + "\r\n                            ");
    out.write(renderContext.getObjectModel().toString(var_46));
}
out.write("</span>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"exchange-section__input\">\r\n                                            <div class=\"item__input-fields\">\r\n                                                <div class=\"input-field__currency-field\" inputmode=\"numeric\">\r\n                                                    <input aria-invalid=\"false\" type=\"text\"");
{
    Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "targetCurrencyPlaceholder");
    {
        Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
        {
            boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
            if (var_shoulddisplayattr50) {
                out.write(" placeholder");
                {
                    boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                    if (!var_istrueattr49) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" maxlength=\"25\" name=\"tranferPrice\" class=\"money__output-field\" value=\"\" pattern=\"[0-9]\" readonly/>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"exchange-section__note\">");
{
    String var_51 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "currencyChangeNote"), "text"))) + "\r\n                            ");
    out.write(renderContext.getObjectModel().toString(var_51));
}
out.write("</div>\r\n                            <div class=\"exchange-section__error\">");
{
    String var_52 = (("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_currencyconvertormodel, "errorNote"), "text"))) + "\r\n                            ");
    out.write(renderContext.getObjectModel().toString(var_52));
}
out.write("</div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

