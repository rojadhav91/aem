/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.footer;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class footer__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_footervalues = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar26_list_coerced$ = null;
Collection var_collectionvar45_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
Collection var_collectionvar67_list_coerced$ = null;
_global_footervalues = renderContext.call("use", com.techcombank.core.models.FooterModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logo");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"scroll-to-top\">\r\n            <div class=\"scroll-to-top__icon\">\r\n            </div>\r\n            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/scroll-top-icon.svg\"/>\r\n        </section>\r\n        <footer class=\"footer-container\">\r\n            <div class=\"content-wrapper\">\r\n                <div class=\"footer-wrapper\">\r\n                    <div class=\"footer-head\">\r\n                        <div class=\"footer-logo\">\r\n                        \t<picture>\r\n\t\t\t\t    <source media=\"(max-width: 360px)\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoMobileImagePath");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n\t\t\t\t    <source media=\"(max-width: 576px)\"");
            {
                Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoMobileImagePath");
                {
                    Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                    {
                        boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                        if (var_shoulddisplayattr8) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                if (!var_istrueattr7) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n\t\t\t\t    <source media=\"(min-width: 1080px)\"");
            {
                Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoWebImagePath");
                {
                    Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                    {
                        boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                        if (var_shoulddisplayattr12) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                                if (!var_istrueattr11) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n\t\t\t\t    <source media=\"(min-width: 1200px)\"");
            {
                Object var_attrvalue13 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoWebImagePath");
                {
                    Object var_attrcontent14 = renderContext.call("xss", var_attrvalue13, "attribute");
                    {
                        boolean var_shoulddisplayattr16 = (((null != var_attrcontent14) && (!"".equals(var_attrcontent14))) && ((!"".equals(var_attrvalue13)) && (!((Object)false).equals(var_attrvalue13))));
                        if (var_shoulddisplayattr16) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr15 = (var_attrvalue13.equals(true));
                                if (!var_istrueattr15) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent14));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n\t\t\t\t    <img");
            {
                Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoWebImagePath");
                {
                    Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "uri");
                    {
                        boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                        if (var_shoulddisplayattr20) {
                            out.write(" src");
                            {
                                boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                if (!var_istrueattr19) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "logoAltText");
                {
                    Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                    {
                        boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                        if (var_shoulddisplayattr24) {
                            out.write(" alt");
                            {
                                boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                if (!var_istrueattr23) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n\t\t\t\t</picture>\r\n\t\t\t</div>\r\n                        <div class=\"footer-expand\">\r\n                            <p>");
            {
                Object var_25 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "rightButtonText"), "text");
                out.write(renderContext.getObjectModel().toString(var_25));
            }
            out.write("</p>\r\n                            <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"footer-links\">\r\n                        <div class=\"footer-links__list\" id=\"footer-links__list\">\r\n                            ");
            {
                Object var_collectionvar26 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "menuList");
                {
                    long var_size27 = ((var_collectionvar26_list_coerced$ == null ? (var_collectionvar26_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar26)) : var_collectionvar26_list_coerced$).size());
                    {
                        boolean var_notempty28 = (var_size27 > 0);
                        if (var_notempty28) {
                            {
                                long var_end31 = var_size27;
                                {
                                    boolean var_validstartstepend32 = (((0 < var_size27) && true) && (var_end31 > 0));
                                    if (var_validstartstepend32) {
                                        if (var_collectionvar26_list_coerced$ == null) {
                                            var_collectionvar26_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar26);
                                        }
                                        long var_index33 = 0;
                                        for (Object itemfooter : var_collectionvar26_list_coerced$) {
                                            {
                                                boolean var_traversal35 = (((var_index33 >= 0) && (var_index33 <= var_end31)) && true);
                                                if (var_traversal35) {
                                                    out.write("<div class=\"footer-links__item\">\r\n                                <h3>\r\n                                    <a");
                                                    {
                                                        String var_attrvalue36 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(itemfooter, "openInNewTab"), "true")) ? "_blank" : "");
                                                        {
                                                            Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                                                if (var_shoulddisplayattr39) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                                                        if (!var_istrueattr38) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(itemfooter, "footerMenuUrl");
                                                        {
                                                            Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                if (var_shoulddisplayattr43) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                        if (!var_istrueattr42) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        Object var_44 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(itemfooter, "footerMenuHeader"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_44));
                                                    }
                                                    out.write("</a></h3>\r\n                                ");
                                                    {
                                                        Object var_collectionvar45 = renderContext.getObjectModel().resolveProperty(itemfooter, "subMenuList");
                                                        {
                                                            long var_size46 = ((var_collectionvar45_list_coerced$ == null ? (var_collectionvar45_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar45)) : var_collectionvar45_list_coerced$).size());
                                                            {
                                                                boolean var_notempty47 = (var_size46 > 0);
                                                                if (var_notempty47) {
                                                                    {
                                                                        long var_end50 = var_size46;
                                                                        {
                                                                            boolean var_validstartstepend51 = (((0 < var_size46) && true) && (var_end50 > 0));
                                                                            if (var_validstartstepend51) {
                                                                                out.write("<ul>");
                                                                                if (var_collectionvar45_list_coerced$ == null) {
                                                                                    var_collectionvar45_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar45);
                                                                                }
                                                                                long var_index52 = 0;
                                                                                for (Object subitem : var_collectionvar45_list_coerced$) {
                                                                                    {
                                                                                        boolean var_traversal54 = (((var_index52 >= 0) && (var_index52 <= var_end50)) && true);
                                                                                        if (var_traversal54) {
                                                                                            out.write("\r\n                                    <li>\r\n                                        <a");
                                                                                            {
                                                                                                String var_attrvalue55 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(subitem, "openInNewTab"), "true")) ? "_blank" : "");
                                                                                                {
                                                                                                    Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
                                                                                                        if (var_shoulddisplayattr58) {
                                                                                                            out.write(" target");
                                                                                                            {
                                                                                                                boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                                                                                                                if (!var_istrueattr57) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(" data-tracking-click-event=\"linkClick\"");
                                                                                            {
                                                                                                String var_attrcontent59 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(subitem, "subMenuText"), "attribute"))) + "'}");
                                                                                                out.write(" data-tracking-click-info-value=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            {
                                                                                                String var_attrcontent60 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(subitem, "isSubMenuUrlExternal"), "attribute"))) + "'}}");
                                                                                                out.write(" data-tracking-web-interaction-value=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                                                                                out.write("\"");
                                                                                            }
                                                                                            {
                                                                                                Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(subitem, "subMenuUrl");
                                                                                                {
                                                                                                    Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "uri");
                                                                                                    {
                                                                                                        boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                                                                                                        if (var_shoulddisplayattr64) {
                                                                                                            out.write(" href");
                                                                                                            {
                                                                                                                boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                                                                                                if (!var_istrueattr63) {
                                                                                                                    out.write("=\"");
                                                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                                                                    out.write("\"");
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            out.write(">");
                                                                                            {
                                                                                                Object var_65 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(subitem, "subMenuText"), "text");
                                                                                                out.write(renderContext.getObjectModel().toString(var_65));
                                                                                            }
                                                                                            out.write("</a>\r\n                                    </li>\r\n                                ");
                                                                                        }
                                                                                    }
                                                                                    var_index52++;
                                                                                }
                                                                                out.write("</ul>");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        var_collectionvar45_list_coerced$ = null;
                                                    }
                                                    out.write("\r\n                            </div>\n");
                                                }
                                            }
                                            var_index33++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar26_list_coerced$ = null;
            }
            out.write("\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"footer-links__social-container\">\r\n                        <div class=\"footer-links__social-text\">\r\n                            <p>");
            {
                Object var_66 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "socialMediaText"), "text");
                out.write(renderContext.getObjectModel().toString(var_66));
            }
            out.write("</p>\r\n                        </div>\r\n                        <div class=\"footer-links__social-media\">\r\n                            ");
            {
                Object var_collectionvar67 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "socialMediaList");
                {
                    long var_size68 = ((var_collectionvar67_list_coerced$ == null ? (var_collectionvar67_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar67)) : var_collectionvar67_list_coerced$).size());
                    {
                        boolean var_notempty69 = (var_size68 > 0);
                        if (var_notempty69) {
                            {
                                long var_end72 = var_size68;
                                {
                                    boolean var_validstartstepend73 = (((0 < var_size68) && true) && (var_end72 > 0));
                                    if (var_validstartstepend73) {
                                        if (var_collectionvar67_list_coerced$ == null) {
                                            var_collectionvar67_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar67);
                                        }
                                        long var_index74 = 0;
                                        for (Object submenu : var_collectionvar67_list_coerced$) {
                                            {
                                                boolean var_traversal76 = (((var_index74 >= 0) && (var_index74 <= var_end72)) && true);
                                                if (var_traversal76) {
                                                    out.write("\r\n                                <div class=\"footer-links__social-item\">\r\n                                    <a");
                                                    {
                                                        String var_attrvalue77 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(submenu, "openInNewTab"), "true")) ? "_blank" : "");
                                                        {
                                                            Object var_attrcontent78 = renderContext.call("xss", var_attrvalue77, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr80 = (((null != var_attrcontent78) && (!"".equals(var_attrcontent78))) && ((!"".equals(var_attrvalue77)) && (!((Object)false).equals(var_attrvalue77))));
                                                                if (var_shoulddisplayattr80) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr79 = (var_attrvalue77.equals(true));
                                                                        if (!var_istrueattr79) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent78));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(submenu, "socialUrl");
                                                        {
                                                            Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
                                                                if (var_shoulddisplayattr84) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                                                                        if (!var_istrueattr83) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">\r\n                                         <picture>\r\n\t\t\t\t\t\t<source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(submenu, "socialMobileImagePath");
                                                        {
                                                            Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
                                                                if (var_shoulddisplayattr88) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                                                                        if (!var_istrueattr87) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n\t\t\t\t\t\t<source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue89 = renderContext.getObjectModel().resolveProperty(submenu, "socialMobileImagePath");
                                                        {
                                                            Object var_attrcontent90 = renderContext.call("xss", var_attrvalue89, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr92 = (((null != var_attrcontent90) && (!"".equals(var_attrcontent90))) && ((!"".equals(var_attrvalue89)) && (!((Object)false).equals(var_attrvalue89))));
                                                                if (var_shoulddisplayattr92) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr91 = (var_attrvalue89.equals(true));
                                                                        if (!var_istrueattr91) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n\t\t\t\t\t\t<source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue93 = renderContext.getObjectModel().resolveProperty(submenu, "socialWebImagePath");
                                                        {
                                                            Object var_attrcontent94 = renderContext.call("xss", var_attrvalue93, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr96 = (((null != var_attrcontent94) && (!"".equals(var_attrcontent94))) && ((!"".equals(var_attrvalue93)) && (!((Object)false).equals(var_attrvalue93))));
                                                                if (var_shoulddisplayattr96) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr95 = (var_attrvalue93.equals(true));
                                                                        if (!var_istrueattr95) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent94));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n\t\t\t\t\t\t<source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue97 = renderContext.getObjectModel().resolveProperty(submenu, "socialWebImagePath");
                                                        {
                                                            Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                                                                if (var_shoulddisplayattr100) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                                                                        if (!var_istrueattr99) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n\t\t\t\t\t \t<img target=\"_blank\" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent101 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(submenu, "socialText"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent102 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(submenu, "externalLink"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        Object var_attrvalue103 = renderContext.getObjectModel().resolveProperty(submenu, "socialWebImagePath");
                                                        {
                                                            Object var_attrcontent104 = renderContext.call("xss", var_attrvalue103, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr106 = (((null != var_attrcontent104) && (!"".equals(var_attrcontent104))) && ((!"".equals(var_attrvalue103)) && (!((Object)false).equals(var_attrvalue103))));
                                                                if (var_shoulddisplayattr106) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr105 = (var_attrvalue103.equals(true));
                                                                        if (!var_istrueattr105) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent104));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue107 = renderContext.getObjectModel().resolveProperty(submenu, "socialText");
                                                        {
                                                            Object var_attrcontent108 = renderContext.call("xss", var_attrvalue107, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr110 = (((null != var_attrcontent108) && (!"".equals(var_attrcontent108))) && ((!"".equals(var_attrvalue107)) && (!((Object)false).equals(var_attrvalue107))));
                                                                if (var_shoulddisplayattr110) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr109 = (var_attrvalue107.equals(true));
                                                                        if (!var_istrueattr109) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent108));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n\t\t\t\t\t</picture> \r\n                                    </a>\r\n                                </div>\r\n                            ");
                                                }
                                            }
                                            var_index74++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar67_list_coerced$ = null;
            }
            out.write("\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"footer-info\">\r\n                        <p class=\"copyright\">");
            {
                Object var_111 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "copyrightText"), "text");
                out.write(renderContext.getObjectModel().toString(var_111));
            }
            out.write("</p>\r\n                        <a");
            {
                String var_attrvalue112 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_footervalues, "openInNewTabTerms"), "true")) ? "_blank" : "");
                {
                    Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                    {
                        boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                        if (var_shoulddisplayattr115) {
                            out.write(" target");
                            {
                                boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                if (!var_istrueattr114) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent116 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "termsAndCondition"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent116));
                out.write("\"");
            }
            {
                String var_attrcontent117 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "isTermsAndConditionExternal"), "attribute"))) + "'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                out.write("\"");
            }
            {
                Object var_attrvalue118 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "termsAndConditionUrl");
                {
                    Object var_attrcontent119 = renderContext.call("xss", var_attrvalue118, "uri");
                    {
                        boolean var_shoulddisplayattr121 = (((null != var_attrcontent119) && (!"".equals(var_attrcontent119))) && ((!"".equals(var_attrvalue118)) && (!((Object)false).equals(var_attrvalue118))));
                        if (var_shoulddisplayattr121) {
                            out.write(" href");
                            {
                                boolean var_istrueattr120 = (var_attrvalue118.equals(true));
                                if (!var_istrueattr120) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent119));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">");
            {
                String var_122 = ("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "termsAndCondition"), "text")));
                out.write(renderContext.getObjectModel().toString(var_122));
            }
            out.write("</a>\r\n                        <a");
            {
                String var_attrvalue123 = ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_footervalues, "openInNewTabPrivacy"), "true")) ? "_blank" : "");
                {
                    Object var_attrcontent124 = renderContext.call("xss", var_attrvalue123, "attribute");
                    {
                        boolean var_shoulddisplayattr126 = (((null != var_attrcontent124) && (!"".equals(var_attrcontent124))) && ((!"".equals(var_attrvalue123)) && (!((Object)false).equals(var_attrvalue123))));
                        if (var_shoulddisplayattr126) {
                            out.write(" target");
                            {
                                boolean var_istrueattr125 = (var_attrvalue123.equals(true));
                                if (!var_istrueattr125) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent124));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(" data-tracking-click-event=\"linkClick\"");
            {
                String var_attrcontent127 = (((("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + " | ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "privacyStatement"), "attribute"))) + "'}");
                out.write(" data-tracking-click-info-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent127));
                out.write("\"");
            }
            {
                String var_attrcontent128 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "isPrivacyStatementExternal"), "attribute"))) + "'}}");
                out.write(" data-tracking-web-interaction-value=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent128));
                out.write("\"");
            }
            {
                Object var_attrvalue129 = renderContext.getObjectModel().resolveProperty(_global_footervalues, "privacyStatementLinkUrl");
                {
                    Object var_attrcontent130 = renderContext.call("xss", var_attrvalue129, "uri");
                    {
                        boolean var_shoulddisplayattr132 = (((null != var_attrcontent130) && (!"".equals(var_attrcontent130))) && ((!"".equals(var_attrvalue129)) && (!((Object)false).equals(var_attrvalue129))));
                        if (var_shoulddisplayattr132) {
                            out.write(" href");
                            {
                                boolean var_istrueattr131 = (var_attrvalue129.equals(true));
                                if (!var_istrueattr131) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent130));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">");
            {
                String var_133 = ("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "privacyStatement"), "text")));
                out.write(renderContext.getObjectModel().toString(var_133));
            }
            out.write("</a>");
            {
                String var_134 = (((("\r\n                        " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "personalBanking"), "html"))) + "\r\n                        ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_footervalues, "businessBanking"), "html"))) + "\r\n                    ");
                out.write(renderContext.getObjectModel().toString(var_134));
            }
            out.write("</div>\r\n                </div>\r\n            </div>\r\n        </footer>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar135 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions136_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions136 = obj().with("isEmpty", var_templateoptions136_field$_isempty);
            callUnit(out, renderContext, var_templatevar135, var_templateoptions136);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

