/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.mastheadsimplebanner;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class mastheadsimplebanner__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_simplebannermodel = null;
Collection var_collectionvar51_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n  <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_simplebannermodel = renderContext.call("use", com.techcombank.core.models.MastHeadSimpleBannerModel.class.getName(), obj());
out.write("\r\n  <section");
{
    String var_attrcontent5 = ((((("tcb-hero-banner " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonLabel")) ? "tcb-hero-banner--content-reverse" : ""), "attribute"))) + " type-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bannerType"), "attribute"))) + " align-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "textAlignment"), "attribute")));
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
out.write(">\r\n    <picture class=\"tcb-hero-banner_background\">\r\n      <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerMobileImage");
    {
        Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
        {
            boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
            if (var_shoulddisplayattr9) {
                out.write(" srcset");
                {
                    boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                    if (!var_istrueattr8) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerMobileImage");
    {
        Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
        {
            boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
            if (var_shoulddisplayattr13) {
                out.write(" srcset");
                {
                    boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                    if (!var_istrueattr12) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerImage");
    {
        Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
        {
            boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
            if (var_shoulddisplayattr17) {
                out.write(" srcset");
                {
                    boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                    if (!var_istrueattr16) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerImage");
    {
        Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
        {
            boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
            if (var_shoulddisplayattr21) {
                out.write(" srcset");
                {
                    boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                    if (!var_istrueattr20) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n      <img class=\"tcb-hero-banner_background-image\"");
{
    Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerImage");
    {
        Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "uri");
        {
            boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
            if (var_shoulddisplayattr25) {
                out.write(" src");
                {
                    boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                    if (!var_istrueattr24) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bgBannerImgAlt");
    {
        Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
        {
            boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
            if (var_shoulddisplayattr29) {
                out.write(" alt");
                {
                    boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                    if (!var_istrueattr28) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n    </picture>\r\n    <div id=\"sizing\" class=\"tcb-hero-banner_content tcb-hero-banner_content--medium tcb-hero-banner_content--use-breadcrumb\r\n              tcb-hero-banner_content--use-menu\">\r\n      <div");
{
    String var_attrcontent30 = (("tcb-hero-banner_body " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "tabSection")) ? "tcb-hero-banner--remove-alignment" : ""), "attribute"))) + " tcb-container");
    out.write(" class=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent30));
    out.write("\"");
}
out.write(">\r\n\r\n        <div id=\"alignment\" class=\"tcb-hero-banner_customize-content tcb-hero-banner_customize-content--left-center\">\r\n          <div class=\"external-component full-width\">\r\n            ");
{
    Object var_testvariable31 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "highlightedtext");
    if (renderContext.getObjectModel().toBoolean(var_testvariable31)) {
        out.write("<label class=\"tcb-hero-banner_tag\">");
        {
            Object var_32 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "highlightedtext"), "text");
            out.write(renderContext.getObjectModel().toString(var_32));
        }
        out.write("</label>");
    }
}
out.write("\r\n            <h1 class=\"tcb-hero-banner_hero-title\">");
{
    String var_33 = ("\r\n              " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bannerTitle"), "html")));
    out.write(renderContext.getObjectModel().toString(var_33));
}
out.write("</h1>\r\n            ");
{
    Object var_testvariable34 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bannerDesc");
    if (renderContext.getObjectModel().toBoolean(var_testvariable34)) {
        out.write("<div class=\"tcb-hero-banner_description\">");
        {
            String var_35 = (("\r\n              " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "bannerDesc"), "html"))) + "\r\n            ");
            out.write(renderContext.getObjectModel().toString(var_35));
        }
        out.write("</div>");
    }
}
out.write("\r\n            ");
{
    Object var_testvariable36 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonLabel");
    if (renderContext.getObjectModel().toBoolean(var_testvariable36)) {
        out.write("\r\n              <a class=\"tcb-button tcb-button--light-black tcb-button--hover-lightgray\"");
        {
            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonlinkURL");
            {
                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "uri");
                {
                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                    if (var_shoulddisplayattr40) {
                        out.write(" href");
                        {
                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                            if (!var_istrueattr39) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonTarget");
            {
                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                {
                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                    if (var_shoulddisplayattr44) {
                        out.write(" target");
                        {
                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                            if (!var_istrueattr43) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrvalue45 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonNoFollow")) ? "nofollow" : "");
            {
                Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                {
                    boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                    if (var_shoulddisplayattr48) {
                        out.write(" rel");
                        {
                            boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                            if (!var_istrueattr47) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                <span class=\"white-space\">");
        {
            Object var_49 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "buttonLabel"), "text");
            out.write(renderContext.getObjectModel().toString(var_49));
        }
        out.write("</span>\r\n                <span class=\"tcb-arrow material-symbols-outlined\"> arrow_forward </span>\r\n              </a>\r\n            ");
    }
}
out.write("\r\n            ");
{
    Object var_testvariable50 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "displaySearchBox");
    if (renderContext.getObjectModel().toBoolean(var_testvariable50)) {
        out.write("<div class=\"tcb-hero-banner_looking-for-wrapper\">\r\n              <div class=\"looking-for\">\r\n                <span class=\"looking-for_title\">B\u1EA1n \u0111ang t\u00ECm ki\u1EBFm</span>\r\n                <div class=\"looking-for_input\">\r\n                  <div class=\"wrapper\">\r\n                    <input placeholder=\"Nhu c\u1EA7u c\u1EE7a b\u1EA1n\" name=\"search-document\" trigger-search-to=\"list-view-document\"/>\r\n                    <div class=\"icon\">\r\n                      <span>\r\n                        <img src=\"https://media.techcombank.com/uploads/search-cfd04bece9-647a770675.svg\" alt=\"search icon\"/>\r\n                      </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"looking-for_input-options\">\r\n                    <div class=\"looking-for_input-options-roots\">\r\n                      <div class=\"looking-for_input-options-wrapper\">\r\n                        <div class=\"looking-for_input-options-wrapper-inner\">\r\n                          <div class=\"looking-for_input-options-content\">\r\n                            <span class=\"looking-for_input-options-quick-access\">Quick access</span>\r\n                            <a href=\"#option1\">Option 1</a>\r\n                            <a href=\"#option2\">Option 2</a>\r\n                            <a href=\"#option3\">Option 3</a>\r\n                            <a href=\"#option4\">Option 4</a>\r\n                            <a href=\"#option5\">Option 5</a>\r\n                            <a href=\"#option6\">Option 6</a>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"looking-for_submit\">\r\n                  <button trigger-search-to=\"list-view-document\">\r\n                    <span>Truy c\u1EADp ngay</span>\r\n                    <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg\" alt=\"arrow\"/>\r\n                  </button>\r\n                </div>\r\n              </div>\r\n            </div>");
    }
}
out.write("\r\n          </div>\r\n        </div>\r\n\r\n        <!-- Page menu -->\r\n        <div class=\"tcb-hero-banner_bottom-content\">\r\n          <div class=\"tcb-hero-banner_control tcb-hero-banner_control--prev\"><span class=\"material-symbols-outlined\">\r\n              chevron_left\r\n            </span></div>\r\n          <ul class=\"page-menu\">\r\n            ");
{
    Object var_collectionvar51 = renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "tabSection");
    {
        long var_size52 = ((var_collectionvar51_list_coerced$ == null ? (var_collectionvar51_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar51)) : var_collectionvar51_list_coerced$).size());
        {
            boolean var_notempty53 = (var_size52 > 0);
            if (var_notempty53) {
                {
                    long var_end56 = var_size52;
                    {
                        boolean var_validstartstepend57 = (((0 < var_size52) && true) && (var_end56 > 0));
                        if (var_validstartstepend57) {
                            if (var_collectionvar51_list_coerced$ == null) {
                                var_collectionvar51_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar51);
                            }
                            long var_index58 = 0;
                            for (Object tabsectionitem : var_collectionvar51_list_coerced$) {
                                {
                                    boolean var_traversal60 = (((var_index58 >= 0) && (var_index58 <= var_end56)) && true);
                                    if (var_traversal60) {
                                        out.write("\r\n              <li>\r\n                <a");
                                        {
                                            Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabLinkURL");
                                            {
                                                Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "uri");
                                                {
                                                    boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                                                    if (var_shoulddisplayattr64) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                                            if (!var_istrueattr63) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue65 = renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabTarget");
                                            {
                                                Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                                                    if (var_shoulddisplayattr68) {
                                                        out.write(" target");
                                                        {
                                                            boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                                                            if (!var_istrueattr67) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            String var_attrcontent69 = ("page-menu_item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabLinkURL"), renderContext.getObjectModel().resolveProperty(_global_simplebannermodel, "currentActivePage"))) ? "page-menu_item--active" : ""), "attribute")));
                                            out.write(" class=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent69));
                                            out.write("\"");
                                        }
                                        {
                                            String var_attrvalue70 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabNoFollow")) ? "nofollow" : "");
                                            {
                                                Object var_attrcontent71 = renderContext.call("xss", var_attrvalue70, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr73 = (((null != var_attrcontent71) && (!"".equals(var_attrcontent71))) && ((!"".equals(var_attrvalue70)) && (!((Object)false).equals(var_attrvalue70))));
                                                    if (var_shoulddisplayattr73) {
                                                        out.write(" rel");
                                                        {
                                                            boolean var_istrueattr72 = (var_attrvalue70.equals(true));
                                                            if (!var_istrueattr72) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent71));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">\r\n                  <img aria-hidden=\"true\"");
                                        {
                                            Object var_attrvalue74 = renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabIcon");
                                            {
                                                Object var_attrcontent75 = renderContext.call("xss", var_attrvalue74, "uri");
                                                {
                                                    boolean var_shoulddisplayattr77 = (((null != var_attrcontent75) && (!"".equals(var_attrcontent75))) && ((!"".equals(var_attrvalue74)) && (!((Object)false).equals(var_attrvalue74))));
                                                    if (var_shoulddisplayattr77) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr76 = (var_attrvalue74.equals(true));
                                                            if (!var_istrueattr76) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent75));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue78 = renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabIconAlt");
                                            {
                                                Object var_attrcontent79 = renderContext.call("xss", var_attrvalue78, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr81 = (((null != var_attrcontent79) && (!"".equals(var_attrcontent79))) && ((!"".equals(var_attrvalue78)) && (!((Object)false).equals(var_attrvalue78))));
                                                    if (var_shoulddisplayattr81) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr80 = (var_attrvalue78.equals(true));
                                                            if (!var_istrueattr80) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent79));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                  <span>");
                                        {
                                            Object var_82 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabsectionitem, "tabName"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_82));
                                        }
                                        out.write("</span>\r\n                </a>\r\n              </li>\r\n            ");
                                    }
                                }
                                var_index58++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar51_list_coerced$ = null;
}
out.write("\r\n          </ul>\r\n          <div class=\"tcb-hero-banner_control tcb-hero-banner_control--next\"><span class=\"material-symbols-outlined\">\r\n              chevron_right\r\n            </span></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

