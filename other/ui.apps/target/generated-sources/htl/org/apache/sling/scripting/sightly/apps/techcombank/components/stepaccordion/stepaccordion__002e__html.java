/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.stepaccordion;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class stepaccordion__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_stepaccordion = null;
Collection var_collectionvar5_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_stepaccordion = renderContext.call("use", com.techcombank.core.models.StepAccordionModel.class.getName(), obj());
out.write("\r\n    <section class=\"accordion-inspire-component\">\r\n        ");
{
    Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_stepaccordion, "stepAccordionItemList");
    {
        long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
        {
            boolean var_notempty7 = (var_size6 > 0);
            if (var_notempty7) {
                {
                    long var_end10 = var_size6;
                    {
                        boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                        if (var_validstartstepend11) {
                            out.write("<div class=\"accordion-inspire-container\">");
                            if (var_collectionvar5_list_coerced$ == null) {
                                var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                            }
                            long var_index12 = 0;
                            for (Object item : var_collectionvar5_list_coerced$) {
                                {
                                    long itemlist_field$_index = var_index12;
                                    {
                                        boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                        if (var_traversal14) {
                                            out.write("\r\n            <div");
                                            {
                                                String var_attrcontent15 = (("item " + renderContext.getObjectModel().toString(renderContext.call("xss", ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) ? "hide" : "show"), "attribute"))) + " ");
                                                out.write(" class=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                                out.write("\"");
                                            }
                                            out.write(">\r\n                <div class=\"item-title\" data-tracking-click-event=\"faq\"");
                                            {
                                                String var_attrcontent16 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
                                                out.write(" data-tracking-web-interaction-value=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                out.write("\"");
                                            }
                                            {
                                                String var_attrcontent17 = (("{'accordionTitle': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "accordionItemTitle"), "attribute"))) + "'}");
                                                out.write(" data-tracking-click-info-value=\"");
                                                out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                out.write("\"");
                                            }
                                            out.write(" data-tracking-click-info-key=\"offer\">\r\n                    <h3 class=\"item-title-text\">");
                                            {
                                                Object var_18 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "accordionItemTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_18));
                                            }
                                            out.write("</h3>\r\n                    <div class=\"expand-icon\">\r\n                        <img class=\"add-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg\"/>\r\n                        <img class=\"sub-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/sub-icon.svg\"/>\r\n                    </div>\r\n                </div>\r\n                <div class=\"item-description\">\r\n                    ");
                                            {
                                                Object var_resourcecontent19 = renderContext.call("includeResource", itemlist_field$_index, obj().with("resourceType", "wcm/foundation/components/parsys"));
                                                out.write(renderContext.getObjectModel().toString(var_resourcecontent19));
                                            }
                                            out.write("\r\n                </div>\r\n            </div>\r\n        ");
                                        }
                                    }
                                }
                                var_index12++;
                            }
                            out.write("</div>");
                        }
                    }
                }
            }
        }
    }
    var_collectionvar5_list_coerced$ = null;
}
out.write("\r\n    </section>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

