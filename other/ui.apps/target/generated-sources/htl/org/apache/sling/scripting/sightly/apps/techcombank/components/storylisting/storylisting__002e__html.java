/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.storylisting;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class storylisting__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_card = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _dynamic_resource = bindings.get("resource");
Collection var_collectionvar23_list_coerced$ = null;
Collection var_collectionvar48_list_coerced$ = null;
Collection var_collectionvar81_list_coerced$ = null;
Collection var_collectionvar103_list_coerced$ = null;
_global_card = renderContext.call("use", com.techcombank.core.models.StoryListingModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryTags");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"container news-filter\"");
            {
                Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_global_card, "ctaLabel");
                {
                    Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                    {
                        boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                        if (var_shoulddisplayattr4) {
                            out.write(" data-ctalabel");
                            {
                                boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                                if (!var_istrueattr3) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_card, "result");
                {
                    Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
                    {
                        boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                        if (var_shoulddisplayattr8) {
                            out.write(" data-limit");
                            {
                                boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                                if (!var_istrueattr7) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue9 = renderContext.getObjectModel().resolveProperty(_global_card, "image");
                {
                    Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
                    {
                        boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                        if (var_shoulddisplayattr12) {
                            out.write(" data-defaultImage");
                            {
                                boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                                if (!var_istrueattr11) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent13 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_resource, "path"), "attribute")) + ".storylisting.json");
                out.write(" data-url=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                out.write("\"");
            }
            {
                Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(_global_card, "newTab");
                {
                    Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                    {
                        boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                        if (var_shoulddisplayattr17) {
                            out.write(" data-newtab");
                            {
                                boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                                if (!var_istrueattr16) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(_global_card, "nofollow");
                {
                    Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                    {
                        boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                        if (var_shoulddisplayattr21) {
                            out.write(" data-nofollow");
                            {
                                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                if (!var_istrueattr20) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n            <div class=\"category-filter\" data-tracking-click-event=\"articleFilter\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Article Filter ','type': 'other'}}\" data-tracking-click-info-value=\"{'articleFilter':'article-filter'}\">\r\n                <div class=\"container\">\r\n                    <h2 class=\"category-title\">");
            {
                Object var_22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_22));
            }
            out.write("</h2>\r\n                    ");
            {
                Object var_collectionvar23 = renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryTags");
                {
                    long var_size24 = ((var_collectionvar23_list_coerced$ == null ? (var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23)) : var_collectionvar23_list_coerced$).size());
                    {
                        boolean var_notempty25 = (var_size24 > 0);
                        if (var_notempty25) {
                            {
                                long var_end28 = var_size24;
                                {
                                    boolean var_validstartstepend29 = (((0 < var_size24) && true) && (var_end28 > 0));
                                    if (var_validstartstepend29) {
                                        out.write("<div class=\"slide-button\">");
                                        if (var_collectionvar23_list_coerced$ == null) {
                                            var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23);
                                        }
                                        long var_index30 = 0;
                                        for (Object articlecategory : var_collectionvar23_list_coerced$) {
                                            {
                                                boolean var_traversal32 = (((var_index30 >= 0) && (var_index30 <= var_end28)) && true);
                                                if (var_traversal32) {
                                                    out.write("\r\n                        <button class=\"btn\"");
                                                    {
                                                        Object var_attrvalue33 = articlecategory;
                                                        {
                                                            Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                                if (var_shoulddisplayattr36) {
                                                                    out.write(" id");
                                                                    {
                                                                        boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                                        if (!var_istrueattr35) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue37 = articlecategory;
                                                        {
                                                            Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                                if (var_shoulddisplayattr40) {
                                                                    out.write(" data-articleCategory");
                                                                    {
                                                                        boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                                        if (!var_istrueattr39) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue41 = articlecategory;
                                                        {
                                                            Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                                                                if (var_shoulddisplayattr44) {
                                                                    out.write(" name");
                                                                    {
                                                                        boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                                                        if (!var_istrueattr43) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(">");
                                                    {
                                                        Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryTags"), articlecategory), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_45));
                                                    }
                                                    out.write("</button>\r\n                    ");
                                                }
                                            }
                                            var_index30++;
                                        }
                                        out.write("</div>");
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar23_list_coerced$ = null;
            }
            out.write("\r\n                </div>\r\n            </div>\r\n            <div id=\"sticky\" class=\"sticky-filter\" style=\"display: none\">\r\n                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/filter.svg\"/>\r\n            </div>\r\n            <div class=\"content-wrapper news-filter__wrapper\">\r\n                <!-- Side bar Offer filter -->\r\n                ");
            {
                Object var_testvariable46 = renderContext.getObjectModel().resolveProperty(_global_card, "articleTags");
                if (renderContext.getObjectModel().toBoolean(var_testvariable46)) {
                    out.write("<div class=\"news_filter-group\">\r\n                    <!-- Filter title -->\r\n                    <div class=\"offer-filter__title filter--border-bottom\">\r\n                        <h6>");
                    {
                        Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "articleTagLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_47));
                    }
                    out.write("</h6>\r\n                    </div>\r\n                    <!-- Checkbox list item -->\r\n                    ");
                    {
                        Object var_collectionvar48 = renderContext.getObjectModel().resolveProperty(_global_card, "articleTags");
                        {
                            long var_size49 = ((var_collectionvar48_list_coerced$ == null ? (var_collectionvar48_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar48)) : var_collectionvar48_list_coerced$).size());
                            {
                                boolean var_notempty50 = (var_size49 > 0);
                                if (var_notempty50) {
                                    {
                                        long var_end53 = var_size49;
                                        {
                                            boolean var_validstartstepend54 = (((0 < var_size49) && true) && (var_end53 > 0));
                                            if (var_validstartstepend54) {
                                                out.write("<div class=\"offer-filter__checkbox\" id=\"newsTypes\">");
                                                if (var_collectionvar48_list_coerced$ == null) {
                                                    var_collectionvar48_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar48);
                                                }
                                                long var_index55 = 0;
                                                for (Object articletag : var_collectionvar48_list_coerced$) {
                                                    {
                                                        boolean var_traversal57 = (((var_index55 >= 0) && (var_index55 <= var_end53)) && true);
                                                        if (var_traversal57) {
                                                            out.write("\r\n                        <div class=\"offer-filter__checkbox-item\">\r\n                            <label class=\"checkbox-item__wrapper\">\r\n                                <input id=\"allTypePost\" class=\"input__checkbox\" type=\"checkbox\"");
                                                            {
                                                                Object var_attrvalue58 = articletag;
                                                                {
                                                                    Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                                                                        if (var_shoulddisplayattr61) {
                                                                            out.write(" name");
                                                                            {
                                                                                boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                                                                                if (!var_istrueattr60) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            {
                                                                Object var_attrvalue62 = articletag;
                                                                {
                                                                    Object var_attrcontent63 = renderContext.call("xss", var_attrvalue62, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr65 = (((null != var_attrcontent63) && (!"".equals(var_attrcontent63))) && ((!"".equals(var_attrvalue62)) && (!((Object)false).equals(var_attrvalue62))));
                                                                        if (var_shoulddisplayattr65) {
                                                                            out.write(" data-articletag");
                                                                            {
                                                                                boolean var_istrueattr64 = (var_attrvalue62.equals(true));
                                                                                if (!var_istrueattr64) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write("/>\r\n                                <div>");
                                                            {
                                                                Object var_66 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_card, "articleTags"), articletag), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_66));
                                                            }
                                                            out.write("</div>\r\n                            </label>\r\n                        </div>\r\n                    ");
                                                        }
                                                    }
                                                    var_index55++;
                                                }
                                                out.write("</div>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar48_list_coerced$ = null;
                    }
                    out.write("\r\n                </div>");
                }
            }
            out.write("\r\n                <div class=\"filter-container\">\r\n                    <div class=\"news_open-filter-button\"");
            {
                Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(_global_card, "filter");
                {
                    Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                    {
                        boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                        if (var_shoulddisplayattr70) {
                            out.write(" data-title");
                            {
                                boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                if (!var_istrueattr69) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(_global_card, "filter");
                {
                    Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                    {
                        boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                        if (var_shoulddisplayattr74) {
                            out.write(" data-action");
                            {
                                boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                if (!var_istrueattr73) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n                        <button class=\"btn-open-filter\" data-toogle=\"modal\" data-target=\"open-modal\">\r\n                            <span>");
            {
                Object var_75 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "filter"), "text");
                out.write(renderContext.getObjectModel().toString(var_75));
            }
            out.write("</span>\r\n                            <div class=\"btn-open-filter__icon\">\r\n                                <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/filter.svg\"/>\r\n                            </div>\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- Offer cards container -->\r\n                <div");
            {
                String var_attrcontent76 = ("offer-cards__container " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_card, "articleTags")) ? "" : "full-width"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                out.write("\"");
            }
            out.write(">\r\n                    <div class=\"news-list\">\r\n\r\n\r\n                    </div>\r\n                    <div class=\"not-found\">\r\n                        <div class=\"description\">");
            {
                String var_77 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "errorMsg"), "text"))) + "\r\n                        ");
                out.write(renderContext.getObjectModel().toString(var_77));
            }
            out.write("</div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"information-filter__load-more\">\r\n                <button type=\"button\" class=\"load-more__button\" data-tracking-click-event=\"articleView\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'View more - Article Filter','type': 'other'}}\" data-tracking-click-info-value=\"{'articleFilter':'Article Filters'}\">\r\n                    <span>");
            {
                String var_78 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "viewMore"), "text")));
                out.write(renderContext.getObjectModel().toString(var_78));
            }
            out.write("</span>\r\n                    <div class=\"load-more__button-icon\">\r\n                        <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-down-red.png\"/>\r\n                    </div>\r\n                </button>\r\n            </div>\r\n        </section>\r\n        <!-- filter modal for Mobile -->\r\n        <div class=\"dialog container news-filter\" style=\"display: none\">\r\n            <div class=\"category-filter-mobi\">\r\n                <div>\r\n                    <div class=\"dialog-title\">\r\n                        <span class=\"title\">");
            {
                Object var_79 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "allFilter"), "text");
                out.write(renderContext.getObjectModel().toString(var_79));
            }
            out.write("</span>\r\n                        <span class=\"btn-close\" style=\"float: right\">\r\n              <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_red.svg\"/>\r\n            </span>\r\n                    </div>\r\n                    <div class=\"dialog-content\">\r\n                        <div>\r\n                            <h2 class=\"category-title\">");
            {
                Object var_80 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryLabel"), "text");
                out.write(renderContext.getObjectModel().toString(var_80));
            }
            out.write("</h2>\r\n                            ");
            {
                Object var_collectionvar81 = renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryTags");
                {
                    long var_size82 = ((var_collectionvar81_list_coerced$ == null ? (var_collectionvar81_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar81)) : var_collectionvar81_list_coerced$).size());
                    {
                        boolean var_notempty83 = (var_size82 > 0);
                        if (var_notempty83) {
                            {
                                long var_end86 = var_size82;
                                {
                                    boolean var_validstartstepend87 = (((0 < var_size82) && true) && (var_end86 > 0));
                                    if (var_validstartstepend87) {
                                        out.write("<div class=\"slide-button\">");
                                        if (var_collectionvar81_list_coerced$ == null) {
                                            var_collectionvar81_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar81);
                                        }
                                        long var_index88 = 0;
                                        for (Object articlecategory : var_collectionvar81_list_coerced$) {
                                            {
                                                long articlecategorylist_field$_index = var_index88;
                                                {
                                                    boolean var_traversal90 = (((var_index88 >= 0) && (var_index88 <= var_end86)) && true);
                                                    if (var_traversal90) {
                                                        out.write("\r\n                                <button class=\"btn\"");
                                                        {
                                                            String var_attrcontent91 = ((renderContext.getObjectModel().toString(renderContext.call("xss", articlecategory, "attribute")) + "-") + renderContext.getObjectModel().toString(renderContext.call("xss", articlecategorylist_field$_index, "attribute")));
                                                            out.write(" id=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent91));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            Object var_attrvalue92 = articlecategory;
                                                            {
                                                                Object var_attrcontent93 = renderContext.call("xss", var_attrvalue92, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr95 = (((null != var_attrcontent93) && (!"".equals(var_attrcontent93))) && ((!"".equals(var_attrvalue92)) && (!((Object)false).equals(var_attrvalue92))));
                                                                    if (var_shoulddisplayattr95) {
                                                                        out.write(" data-articleCategory");
                                                                        {
                                                                            boolean var_istrueattr94 = (var_attrvalue92.equals(true));
                                                                            if (!var_istrueattr94) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent93));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            Object var_attrvalue96 = articlecategory;
                                                            {
                                                                Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                                    if (var_shoulddisplayattr99) {
                                                                        out.write(" name");
                                                                        {
                                                                            boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                                            if (!var_istrueattr98) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(">");
                                                        {
                                                            Object var_100 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_card, "articleCategoryTags"), articlecategory), "text");
                                                            out.write(renderContext.getObjectModel().toString(var_100));
                                                        }
                                                        out.write("</button>\r\n                            ");
                                                    }
                                                }
                                            }
                                            var_index88++;
                                        }
                                        out.write("</div>");
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar81_list_coerced$ = null;
            }
            out.write("\r\n                            <div>\r\n                                <!-- Filter title -->\r\n                                ");
            {
                Object var_testvariable101 = renderContext.getObjectModel().resolveProperty(_global_card, "articleTags");
                if (renderContext.getObjectModel().toBoolean(var_testvariable101)) {
                    out.write("<div class=\"offer-filter__title filter--border-bottom\">\r\n                                    <h6>");
                    {
                        Object var_102 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "articleTagLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_102));
                    }
                    out.write("</h6>\r\n                                </div>");
                }
            }
            out.write("\r\n                                <!-- Checkbox list item -->\r\n                                ");
            {
                Object var_testvariable110 = renderContext.getObjectModel().resolveProperty(_global_card, "articleTags");
                if (renderContext.getObjectModel().toBoolean(var_testvariable110)) {
                    {
                        Object var_collectionvar103 = renderContext.getObjectModel().resolveProperty(_global_card, "articleTags");
                        {
                            long var_size104 = ((var_collectionvar103_list_coerced$ == null ? (var_collectionvar103_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar103)) : var_collectionvar103_list_coerced$).size());
                            {
                                boolean var_notempty105 = (var_size104 > 0);
                                if (var_notempty105) {
                                    {
                                        long var_end108 = var_size104;
                                        {
                                            boolean var_validstartstepend109 = (((0 < var_size104) && true) && (var_end108 > 0));
                                            if (var_validstartstepend109) {
                                                out.write("<div class=\"offer-filter__checkbox\" id=\"newsTypes\">");
                                                if (var_collectionvar103_list_coerced$ == null) {
                                                    var_collectionvar103_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar103);
                                                }
                                                long var_index111 = 0;
                                                for (Object articletag : var_collectionvar103_list_coerced$) {
                                                    {
                                                        boolean var_traversal113 = (((var_index111 >= 0) && (var_index111 <= var_end108)) && true);
                                                        if (var_traversal113) {
                                                            out.write("\r\n                                    <div class=\"offer-filter__checkbox-item\">\r\n                                        <label class=\"checkbox-item__wrapper\">\r\n                                            <input id=\"allTypePost\" class=\"input__checkbox\" type=\"checkbox\"");
                                                            {
                                                                Object var_attrvalue114 = articletag;
                                                                {
                                                                    Object var_attrcontent115 = renderContext.call("xss", var_attrvalue114, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr117 = (((null != var_attrcontent115) && (!"".equals(var_attrcontent115))) && ((!"".equals(var_attrvalue114)) && (!((Object)false).equals(var_attrvalue114))));
                                                                        if (var_shoulddisplayattr117) {
                                                                            out.write(" name");
                                                                            {
                                                                                boolean var_istrueattr116 = (var_attrvalue114.equals(true));
                                                                                if (!var_istrueattr116) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent115));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            {
                                                                Object var_attrvalue118 = articletag;
                                                                {
                                                                    Object var_attrcontent119 = renderContext.call("xss", var_attrvalue118, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr121 = (((null != var_attrcontent119) && (!"".equals(var_attrcontent119))) && ((!"".equals(var_attrvalue118)) && (!((Object)false).equals(var_attrvalue118))));
                                                                        if (var_shoulddisplayattr121) {
                                                                            out.write(" data-articletag");
                                                                            {
                                                                                boolean var_istrueattr120 = (var_attrvalue118.equals(true));
                                                                                if (!var_istrueattr120) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent119));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write("/>\r\n                                            <div>");
                                                            {
                                                                Object var_122 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(renderContext.getObjectModel().resolveProperty(_global_card, "articleTags"), articletag), "text");
                                                                out.write(renderContext.getObjectModel().toString(var_122));
                                                            }
                                                            out.write("</div>\r\n                                        </label>\r\n                                    </div>\r\n                                ");
                                                        }
                                                    }
                                                    var_index111++;
                                                }
                                                out.write("</div>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar103_list_coerced$ = null;
                    }
                }
            }
            out.write("\r\n                                <div>\r\n                                    <button class=\"btn-close\" data-tracking-click-event=\"articleView\" data-tracking-web-interaction-value=\"{'webInteractions': {'name': 'Apply - Article Filter','type': 'other'}}\" data-tracking-click-info-value=\"{'articleFilter':'Article Filters'}\">");
            {
                String var_123 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_card, "apply"), "text")));
                out.write(renderContext.getObjectModel().toString(var_123));
            }
            out.write("</button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar124 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions125_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions125 = obj().with("isEmpty", var_templateoptions125_field$_isempty);
            callUnit(out, renderContext, var_templatevar124, var_templateoptions125);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

