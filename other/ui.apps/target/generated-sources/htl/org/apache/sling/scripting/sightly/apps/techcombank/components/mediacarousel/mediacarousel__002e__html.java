/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.mediacarousel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class mediacarousel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar1_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.MediaCarouselModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "mediaCarouselItemList");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n  ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n    <div class=\"inspire-slider-component\">\r\n      <div class=\"inspire-slider-container\">\r\n        <div class=\"swiper inspire-slider-swiper\">\r\n          <div class=\"carousel-items swiper-wrapper\">\r\n            ");
            {
                Object var_collectionvar1 = renderContext.getObjectModel().resolveProperty(_global_model, "mediaCarouselItemList");
                {
                    long var_size2 = ((var_collectionvar1_list_coerced$ == null ? (var_collectionvar1_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar1)) : var_collectionvar1_list_coerced$).size());
                    {
                        boolean var_notempty3 = (var_size2 > 0);
                        if (var_notempty3) {
                            {
                                long var_end6 = var_size2;
                                {
                                    boolean var_validstartstepend7 = (((0 < var_size2) && true) && (var_end6 > 0));
                                    if (var_validstartstepend7) {
                                        if (var_collectionvar1_list_coerced$ == null) {
                                            var_collectionvar1_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar1);
                                        }
                                        long var_index8 = 0;
                                        for (Object item : var_collectionvar1_list_coerced$) {
                                            {
                                                boolean var_traversal10 = (((var_index8 >= 0) && (var_index8 <= var_end6)) && true);
                                                if (var_traversal10) {
                                                    out.write("\r\n              <div");
                                                    {
                                                        String var_attrcontent11 = ("item swiper-slide " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "mediaType"), "attribute")));
                                                        out.write(" class=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                                        out.write("\"");
                                                    }
                                                    out.write(">\r\n                <div class=\"item-img\">\r\n                  <picture>\r\n                    <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue12 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                        {
                                                            Object var_attrcontent13 = renderContext.call("xss", var_attrvalue12, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr15 = (((null != var_attrcontent13) && (!"".equals(var_attrcontent13))) && ((!"".equals(var_attrvalue12)) && (!((Object)false).equals(var_attrvalue12))));
                                                                if (var_shoulddisplayattr15) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr14 = (var_attrvalue12.equals(true));
                                                                        if (!var_istrueattr14) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent13));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue16 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                        {
                                                            Object var_attrcontent17 = renderContext.call("xss", var_attrvalue16, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr19 = (((null != var_attrcontent17) && (!"".equals(var_attrcontent17))) && ((!"".equals(var_attrvalue16)) && (!((Object)false).equals(var_attrvalue16))));
                                                                if (var_shoulddisplayattr19) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr18 = (var_attrvalue16.equals(true));
                                                                        if (!var_istrueattr18) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent17));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue20 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                        {
                                                            Object var_attrcontent21 = renderContext.call("xss", var_attrvalue20, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr23 = (((null != var_attrcontent21) && (!"".equals(var_attrcontent21))) && ((!"".equals(var_attrvalue20)) && (!((Object)false).equals(var_attrvalue20))));
                                                                if (var_shoulddisplayattr23) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr22 = (var_attrvalue20.equals(true));
                                                                        if (!var_istrueattr22) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent21));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                        {
                                                            Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                                                                if (var_shoulddisplayattr27) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                                                        if (!var_istrueattr26) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                    <img");
                                                    {
                                                        Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                        {
                                                            Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                                                                if (var_shoulddisplayattr31) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                                                                        if (!var_istrueattr30) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                        {
                                                            Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                                                if (var_shoulddisplayattr35) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                                                        if (!var_istrueattr34) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                  </picture>\r\n                  ");
                                                    {
                                                        boolean var_testvariable36 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "mediaType"), "video"));
                                                        if (var_testvariable36) {
                                                            out.write("<div class=\"btn-video\"");
                                                            {
                                                                Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(item, "videoLinkUrl");
                                                                {
                                                                    Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "attribute");
                                                                    {
                                                                        boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                                        if (var_shoulddisplayattr40) {
                                                                            out.write(" video-url");
                                                                            {
                                                                                boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                                                if (!var_istrueattr39) {
                                                                                    out.write("=\"");
                                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                                    out.write("\"");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            out.write(" data-tracking-click-event=\"videoStart\"");
                                                            {
                                                                String var_attrcontent41 = (("{'videoUrl' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "videoLinkUrl"), "attribute"))) + "'}");
                                                                out.write(" data-tracking-click-info-value=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                out.write("\"");
                                                            }
                                                            {
                                                                String var_attrcontent42 = (("{'webInteractions': {'name': 'Media Carousel','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "videoLinkWebInteractionType"), "attribute"))) + "'}}");
                                                                out.write(" data-tracking-web-interaction-value=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                out.write("\"");
                                                            }
                                                            out.write(">\r\n                    <img class=\"btn-video-img\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/video-play-button.svg\"/>\r\n                  </div>");
                                                        }
                                                    }
                                                    out.write("\r\n                </div>\r\n                <div class=\"item-content\">\r\n                  <div class=\"heading\">\r\n                    <p>");
                                                    {
                                                        String var_43 = (("\r\n                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "carouselTitleText1"), "text"))) + "\r\n                      ");
                                                        out.write(renderContext.getObjectModel().toString(var_43));
                                                    }
                                                    {
                                                        Object var_testvariable44 = renderContext.getObjectModel().resolveProperty(item, "carouselTitleText2");
                                                        if (renderContext.getObjectModel().toBoolean(var_testvariable44)) {
                                                            out.write("<br/>");
                                                        }
                                                    }
                                                    {
                                                        String var_45 = (("\r\n                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "carouselTitleText2"), "text"))) + "\r\n                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_45));
                                                    }
                                                    out.write("</p>\r\n                    <p class=\"text-smallText\">");
                                                    {
                                                        Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "subTitleText"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_46));
                                                    }
                                                    out.write("</p>\r\n                  </div>\r\n                  <div class=\"description\">\r\n                    <p>");
                                                    {
                                                        String var_47 = (("\r\n                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "descriptionText"), "text"))) + "\r\n                    ");
                                                        out.write(renderContext.getObjectModel().toString(var_47));
                                                    }
                                                    out.write("</p>\r\n                  </div>\r\n                  <div class=\"btn\">\r\n                    <a class=\"primary-tracking\"");
                                                    {
                                                        Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                        {
                                                            Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                                                if (var_shoulddisplayattr51) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                                                        if (!var_istrueattr50) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue52 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "_self");
                                                        {
                                                            Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                                                                if (var_shoulddisplayattr55) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                                                        if (!var_istrueattr54) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue56 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                        {
                                                            Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                                                                if (var_shoulddisplayattr59) {
                                                                    out.write(" rel");
                                                                    {
                                                                        boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                                                                        if (!var_istrueattr58) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-tracking-click-event=\"carouselClick\"");
                                                    {
                                                        String var_attrcontent60 = (("{'carouselName' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkText"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent61 = (("{'webInteractions': {'name': 'Media Carousel','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkUrlWebInteractionType"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                                        out.write("\"");
                                                    }
                                                    out.write(">\r\n                      <span>");
                                                    {
                                                        Object var_62 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkText"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_62));
                                                    }
                                                    out.write("</span>\r\n                      <img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\"/>\r\n                    </a>\r\n                    <a class=\"secondary-tracking hidden-tracking\" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent63 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkText"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent63));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent64 = (("{'webInteractions': {'name': 'Media Carousel','type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkUrlWebInteractionType"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                                                        out.write("\"");
                                                    }
                                                    out.write(">\r\n                    </a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            ");
                                                }
                                            }
                                            var_index8++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar1_list_coerced$ = null;
            }
            out.write("\r\n          </div>\r\n          <div class=\"swiper-button-next swiper-button hide\"></div>\r\n          <div class=\"swiper-button-prev swiper-button hide\"></div>\r\n        </div>\r\n        <div class=\"slider-popup\">\r\n          <div class=\"slider-popup-content\">\r\n            <img class=\"close-video\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon_close_grey.svg\"/>\r\n            <iframe src=\"\" frameborder=\"0\"></iframe>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar65 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions66_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions66 = obj().with("isEmpty", var_templateoptions66_field$_isempty);
            callUnit(out, renderContext, var_templatevar65, var_templateoptions66);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

