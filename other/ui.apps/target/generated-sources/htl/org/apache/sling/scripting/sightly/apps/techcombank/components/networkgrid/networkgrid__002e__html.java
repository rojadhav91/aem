/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.networkgrid;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class networkgrid__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_template = null;
Object _global_networkgrid = null;
Object _global_hascontent = null;
Collection var_collectionvar0_list_coerced$ = null;
Collection var_collectionvar37_list_coerced$ = null;
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_networkgrid = renderContext.call("use", com.techcombank.core.models.NetworkGridModel.class.getName(), obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_networkgrid, "networkGridItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    <section class=\"network-grid-component\">\r\n        <!-- network-grid.html -->\r\n        <div class=\"network-grid-component__container\">\r\n            <div class=\"network-grid\">\r\n                <div class=\"slideshow\">\r\n                    <div class=\"item-list\">\r\n                        ");
    {
        Object var_collectionvar0 = renderContext.getObjectModel().resolveProperty(_global_networkgrid, "networkGridItems");
        {
            long var_size1 = ((var_collectionvar0_list_coerced$ == null ? (var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0)) : var_collectionvar0_list_coerced$).size());
            {
                boolean var_notempty2 = (var_size1 > 0);
                if (var_notempty2) {
                    {
                        long var_end5 = var_size1;
                        {
                            boolean var_validstartstepend6 = (((0 < var_size1) && true) && (var_end5 > 0));
                            if (var_validstartstepend6) {
                                if (var_collectionvar0_list_coerced$ == null) {
                                    var_collectionvar0_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar0);
                                }
                                long var_index7 = 0;
                                for (Object item : var_collectionvar0_list_coerced$) {
                                    {
                                        boolean var_traversal9 = (((var_index7 >= 0) && (var_index7 <= var_end5)) && true);
                                        if (var_traversal9) {
                                            out.write("<div class=\"item\">\r\n                        <picture>\r\n                            <source media=\"(max-width: 360px)\"");
                                            {
                                                Object var_attrvalue10 = renderContext.getObjectModel().resolveProperty(item, "mobileGridDisplayImagePath");
                                                {
                                                    Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                                                        if (var_shoulddisplayattr13) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                                                                if (!var_istrueattr12) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
                                            {
                                                Object var_attrvalue14 = renderContext.getObjectModel().resolveProperty(item, "mobileGridDisplayImagePath");
                                                {
                                                    Object var_attrcontent15 = renderContext.call("xss", var_attrvalue14, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr17 = (((null != var_attrcontent15) && (!"".equals(var_attrcontent15))) && ((!"".equals(var_attrvalue14)) && (!((Object)false).equals(var_attrvalue14))));
                                                        if (var_shoulddisplayattr17) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr16 = (var_attrvalue14.equals(true));
                                                                if (!var_istrueattr16) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
                                            {
                                                Object var_attrvalue18 = renderContext.getObjectModel().resolveProperty(item, "webGridDisplayImagePath");
                                                {
                                                    Object var_attrcontent19 = renderContext.call("xss", var_attrvalue18, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr21 = (((null != var_attrcontent19) && (!"".equals(var_attrcontent19))) && ((!"".equals(var_attrvalue18)) && (!((Object)false).equals(var_attrvalue18))));
                                                        if (var_shoulddisplayattr21) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr20 = (var_attrvalue18.equals(true));
                                                                if (!var_istrueattr20) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent19));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
                                            {
                                                Object var_attrvalue22 = renderContext.getObjectModel().resolveProperty(item, "webGridDisplayImagePath");
                                                {
                                                    Object var_attrcontent23 = renderContext.call("xss", var_attrvalue22, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr25 = (((null != var_attrcontent23) && (!"".equals(var_attrcontent23))) && ((!"".equals(var_attrvalue22)) && (!((Object)false).equals(var_attrvalue22))));
                                                        if (var_shoulddisplayattr25) {
                                                            out.write(" srcset");
                                                            {
                                                                boolean var_istrueattr24 = (var_attrvalue22.equals(true));
                                                                if (!var_istrueattr24) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent23));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                            <img");
                                            {
                                                Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(item, "displayImageAltText");
                                                {
                                                    Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "attribute");
                                                    {
                                                        boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                        if (var_shoulddisplayattr29) {
                                                            out.write(" alt");
                                                            {
                                                                boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                if (!var_istrueattr28) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            {
                                                Object var_attrvalue30 = renderContext.getObjectModel().resolveProperty(item, "webGridDisplayImagePath");
                                                {
                                                    Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "uri");
                                                    {
                                                        boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                        if (var_shoulddisplayattr33) {
                                                            out.write(" src");
                                                            {
                                                                boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                if (!var_istrueattr32) {
                                                                    out.write("=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                    out.write("\"");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            out.write("/>\r\n                        </picture>\r\n                        <div class=\"content\">\r\n                            <span class=\"tag\">");
                                            {
                                                Object var_34 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridImageTag"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_34));
                                            }
                                            out.write("</span>\r\n                            <h3 class=\"title\">");
                                            {
                                                Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridImageTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_35));
                                            }
                                            out.write("</h3>\r\n                            <p class=\"description\">");
                                            {
                                                Object var_36 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridImageDescription"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_36));
                                            }
                                            out.write("</p>\r\n                        </div>\r\n                        </div>\n");
                                        }
                                    }
                                    var_index7++;
                                }
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar0_list_coerced$ = null;
    }
    out.write("\r\n                    </div>\r\n                </div>\r\n                <div class=\"panel-list\"> \r\n                    ");
    {
        Object var_collectionvar37 = renderContext.getObjectModel().resolveProperty(_global_networkgrid, "networkGridItems");
        {
            long var_size38 = ((var_collectionvar37_list_coerced$ == null ? (var_collectionvar37_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar37)) : var_collectionvar37_list_coerced$).size());
            {
                boolean var_notempty39 = (var_size38 > 0);
                if (var_notempty39) {
                    {
                        long var_end42 = var_size38;
                        {
                            boolean var_validstartstepend43 = (((0 < var_size38) && true) && (var_end42 > 0));
                            if (var_validstartstepend43) {
                                if (var_collectionvar37_list_coerced$ == null) {
                                    var_collectionvar37_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar37);
                                }
                                long var_index44 = 0;
                                for (Object item : var_collectionvar37_list_coerced$) {
                                    {
                                        boolean var_traversal46 = (((var_index44 >= 0) && (var_index44 <= var_end42)) && true);
                                        if (var_traversal46) {
                                            out.write("<div class=\"panel\">\r\n                        <div class=\"number\">\r\n                            <span>");
                                            {
                                                Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridPanelListNumber"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_47));
                                            }
                                            out.write("</span>\r\n                        </div>\r\n                        <div class=\"content\">\r\n                            <h3 class=\"title\">");
                                            {
                                                Object var_48 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridPanelListTitleText"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_48));
                                            }
                                            out.write("</h3>\r\n                            <p>");
                                            {
                                                Object var_49 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "gridPanelListSubTitle"), "text");
                                                out.write(renderContext.getObjectModel().toString(var_49));
                                            }
                                            out.write("</p>\r\n                        </div>\r\n                    </div>\n");
                                        }
                                    }
                                    var_index44++;
                                }
                            }
                        }
                    }
                }
            }
        }
        var_collectionvar37_list_coerced$ = null;
    }
    out.write("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section> \r\n");
}
out.write("\r\n");
{
    Object var_templatevar50 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions51_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions51 = obj().with("isEmpty", var_templateoptions51_field$_isempty);
            callUnit(out, renderContext, var_templatevar50, var_templateoptions51);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

