/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.homepageblock;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class homepageblock__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_homepageblockmodel = null;
Object _global_template = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _global_hascontent = null;
Collection var_collectionvar5_list_coerced$ = null;
Collection var_collectionvar75_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
_global_homepageblockmodel = renderContext.call("use", com.techcombank.core.models.HomePageBlockModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) ? (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) : renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "image")));
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
}
out.write("\r\n");
{
    Object var_testvariable0 = _global_hascontent;
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <section");
        {
            String var_attrcontent1 = ("home-page " + renderContext.getObjectModel().toString(renderContext.call("xss", ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) ? "full-height" : ""), "attribute")));
            out.write(" class=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent1));
            out.write("\"");
        }
        {
            String var_attrcontent2 = (("background-image: url(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "image"), "styleString"))) + ");");
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent2));
            out.write("\"");
        }
        out.write(">\r\n        <div class=\"home-page-list\">\r\n            <div class=\"home-page-left__wrap\"");
        {
            String var_attrcontent3 = ("background-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "bgColor"), "styleString")));
            out.write(" style=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent3));
            out.write("\"");
        }
        out.write(">\r\n                <div class=\"home-page-left__content\">\r\n                    <div class=\"home-page-left__top-container\">\r\n                        <h1>");
        {
            Object var_4 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "title"), "text");
            out.write(renderContext.getObjectModel().toString(var_4));
        }
        out.write("</h1>\r\n                        <div class=\"home-page-left__contact\">\r\n                            ");
        {
            Object var_collectionvar5 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "navigation");
            {
                long var_size6 = ((var_collectionvar5_list_coerced$ == null ? (var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5)) : var_collectionvar5_list_coerced$).size());
                {
                    boolean var_notempty7 = (var_size6 > 0);
                    if (var_notempty7) {
                        {
                            long var_end10 = var_size6;
                            {
                                boolean var_validstartstepend11 = (((0 < var_size6) && true) && (var_end10 > 0));
                                if (var_validstartstepend11) {
                                    if (var_collectionvar5_list_coerced$ == null) {
                                        var_collectionvar5_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar5);
                                    }
                                    long var_index12 = 0;
                                    for (Object navigationlist : var_collectionvar5_list_coerced$) {
                                        {
                                            boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end10)) && true);
                                            if (var_traversal14) {
                                                out.write("\r\n                                <a");
                                                {
                                                    String var_attrvalue15 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navigationlist, "nofollow")) ? "nofollow" : "");
                                                    {
                                                        Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                                            if (var_shoulddisplayattr18) {
                                                                out.write(" rel");
                                                                {
                                                                    boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                                                    if (!var_istrueattr17) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(" class=\"home-page-left__item\"");
                                                {
                                                    Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(navigationlist, "homeLink");
                                                    {
                                                        Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                                            if (var_shoulddisplayattr22) {
                                                                out.write(" href");
                                                                {
                                                                    boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                                                    if (!var_istrueattr21) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrvalue23 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(navigationlist, "openInNewTabNavigation")) ? "_blank" : "_self");
                                                    {
                                                        Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                                                            if (var_shoulddisplayattr26) {
                                                                out.write(" target");
                                                                {
                                                                    boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                                                    if (!var_istrueattr25) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write(">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
                                                {
                                                    Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(navigationlist, "iconMobileImagePath");
                                                    {
                                                        Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                                                            if (var_shoulddisplayattr30) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                                                    if (!var_istrueattr29) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
                                                {
                                                    Object var_attrvalue31 = renderContext.getObjectModel().resolveProperty(navigationlist, "iconMobileImagePath");
                                                    {
                                                        Object var_attrcontent32 = renderContext.call("xss", var_attrvalue31, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr34 = (((null != var_attrcontent32) && (!"".equals(var_attrcontent32))) && ((!"".equals(var_attrvalue31)) && (!((Object)false).equals(var_attrvalue31))));
                                                            if (var_shoulddisplayattr34) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr33 = (var_attrvalue31.equals(true));
                                                                    if (!var_istrueattr33) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent32));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
                                                {
                                                    Object var_attrvalue35 = renderContext.getObjectModel().resolveProperty(navigationlist, "iconWebImagePath");
                                                    {
                                                        Object var_attrcontent36 = renderContext.call("xss", var_attrvalue35, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr38 = (((null != var_attrcontent36) && (!"".equals(var_attrcontent36))) && ((!"".equals(var_attrvalue35)) && (!((Object)false).equals(var_attrvalue35))));
                                                            if (var_shoulddisplayattr38) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr37 = (var_attrvalue35.equals(true));
                                                                    if (!var_istrueattr37) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent36));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
                                                {
                                                    Object var_attrvalue39 = renderContext.getObjectModel().resolveProperty(navigationlist, "iconWebImagePath");
                                                    {
                                                        Object var_attrcontent40 = renderContext.call("xss", var_attrvalue39, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr42 = (((null != var_attrcontent40) && (!"".equals(var_attrcontent40))) && ((!"".equals(var_attrvalue39)) && (!((Object)false).equals(var_attrvalue39))));
                                                            if (var_shoulddisplayattr42) {
                                                                out.write(" srcset");
                                                                {
                                                                    boolean var_istrueattr41 = (var_attrvalue39.equals(true));
                                                                    if (!var_istrueattr41) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent40));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                        <img class=\"home-page-left__item--img\"");
                                                {
                                                    Object var_attrvalue43 = renderContext.getObjectModel().resolveProperty(navigationlist, "iconWebImagePath");
                                                    {
                                                        Object var_attrcontent44 = renderContext.call("xss", var_attrvalue43, "uri");
                                                        {
                                                            boolean var_shoulddisplayattr46 = (((null != var_attrcontent44) && (!"".equals(var_attrcontent44))) && ((!"".equals(var_attrvalue43)) && (!((Object)false).equals(var_attrvalue43))));
                                                            if (var_shoulddisplayattr46) {
                                                                out.write(" src");
                                                                {
                                                                    boolean var_istrueattr45 = (var_attrvalue43.equals(true));
                                                                    if (!var_istrueattr45) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent44));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    Object var_attrvalue47 = renderContext.getObjectModel().resolveProperty(navigationlist, "text");
                                                    {
                                                        Object var_attrcontent48 = renderContext.call("xss", var_attrvalue47, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr50 = (((null != var_attrcontent48) && (!"".equals(var_attrcontent48))) && ((!"".equals(var_attrvalue47)) && (!((Object)false).equals(var_attrvalue47))));
                                                            if (var_shoulddisplayattr50) {
                                                                out.write(" alt");
                                                                {
                                                                    boolean var_istrueattr49 = (var_attrvalue47.equals(true));
                                                                    if (!var_istrueattr49) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent48));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                out.write("/>\r\n                                    </picture>\r\n                                    <div class=\"home-page-left__menu-item\">\r\n                                        <div class=\"home-page-left__text\" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    Object var_attrvalue51 = renderContext.getObjectModel().resolveProperty(navigationlist, "interaction");
                                                    {
                                                        Object var_attrcontent52 = renderContext.call("xss", var_attrvalue51, "attribute");
                                                        {
                                                            boolean var_shoulddisplayattr54 = (((null != var_attrcontent52) && (!"".equals(var_attrcontent52))) && ((!"".equals(var_attrvalue51)) && (!((Object)false).equals(var_attrvalue51))));
                                                            if (var_shoulddisplayattr54) {
                                                                out.write(" data-tracking-web-interaction-value");
                                                                {
                                                                    boolean var_istrueattr53 = (var_attrvalue51.equals(true));
                                                                    if (!var_istrueattr53) {
                                                                        out.write("=\"");
                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent52));
                                                                        out.write("\"");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                {
                                                    String var_attrcontent55 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navigationlist, "text"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent55));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_56 = (("\r\n                                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(navigationlist, "text"), "text"))) + "\r\n                                        ");
                                                    out.write(renderContext.getObjectModel().toString(var_56));
                                                }
                                                out.write("</div>\r\n                                    </div>\r\n                                </a>\r\n                            ");
                                            }
                                        }
                                        var_index12++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar5_list_coerced$ = null;
        }
        out.write("\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"home-page-left__bottom-container\">\r\n                        <div class=\"home-page-left__contact--block\">\r\n                            <div class=\"home-page-left__top-panel\"></div>\r\n                            <div class=\"\">\r\n                                <a");
        {
            String var_attrvalue57 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "nofollowContact")) ? "nofollow" : "");
            {
                Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "attribute");
                {
                    boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                    if (var_shoulddisplayattr60) {
                        out.write(" rel");
                        {
                            boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                            if (!var_istrueattr59) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"\"");
        {
            Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "linkUrl");
            {
                Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "uri");
                {
                    boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                    if (var_shoulddisplayattr64) {
                        out.write(" href");
                        {
                            boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                            if (!var_istrueattr63) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrvalue65 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "openInNewTabContact")) ? "_blank" : "_self");
            {
                Object var_attrcontent66 = renderContext.call("xss", var_attrvalue65, "attribute");
                {
                    boolean var_shoulddisplayattr68 = (((null != var_attrcontent66) && (!"".equals(var_attrcontent66))) && ((!"".equals(var_attrvalue65)) && (!((Object)false).equals(var_attrvalue65))));
                    if (var_shoulddisplayattr68) {
                        out.write(" target");
                        {
                            boolean var_istrueattr67 = (var_attrvalue65.equals(true));
                            if (!var_istrueattr67) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent66));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                                    <div class=\"home-page-left__menu-item\" data-tracking-click-event=\"linkClick\"");
        {
            Object var_attrvalue69 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "linkInteraction");
            {
                Object var_attrcontent70 = renderContext.call("xss", var_attrvalue69, "attribute");
                {
                    boolean var_shoulddisplayattr72 = (((null != var_attrcontent70) && (!"".equals(var_attrcontent70))) && ((!"".equals(var_attrvalue69)) && (!((Object)false).equals(var_attrvalue69))));
                    if (var_shoulddisplayattr72) {
                        out.write(" data-tracking-web-interaction-value");
                        {
                            boolean var_istrueattr71 = (var_attrvalue69.equals(true));
                            if (!var_istrueattr71) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent70));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrcontent73 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "label"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent73));
            out.write("\"");
        }
        out.write(">\r\n                                        <div class=\"home-page-left__text\">");
        {
            String var_74 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "label"), "text")));
            out.write(renderContext.getObjectModel().toString(var_74));
        }
        out.write("</div>\r\n                                    </div>\r\n                                    ");
        {
            Object var_collectionvar75 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "contact");
            {
                long var_size76 = ((var_collectionvar75_list_coerced$ == null ? (var_collectionvar75_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar75)) : var_collectionvar75_list_coerced$).size());
                {
                    boolean var_notempty77 = (var_size76 > 0);
                    if (var_notempty77) {
                        {
                            long var_end80 = var_size76;
                            {
                                boolean var_validstartstepend81 = (((0 < var_size76) && true) && (var_end80 > 0));
                                if (var_validstartstepend81) {
                                    if (var_collectionvar75_list_coerced$ == null) {
                                        var_collectionvar75_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar75);
                                    }
                                    long var_index82 = 0;
                                    for (Object rightlist : var_collectionvar75_list_coerced$) {
                                        {
                                            boolean var_traversal84 = (((var_index82 >= 0) && (var_index82 <= var_end80)) && true);
                                            if (var_traversal84) {
                                                out.write("\r\n                                        <div class=\"home-page-left__bottom--content\">\r\n                                            <a");
                                                {
                                                    String var_attrcontent85 = ("tel:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightlist, "number"), "uri")));
                                                    out.write(" href=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent85));
                                                    out.write("\"");
                                                }
                                                out.write(" data-tracking-click-event=\"linkClick\"");
                                                {
                                                    String var_attrcontent86 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'exit'}}");
                                                    out.write(" data-tracking-web-interaction-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                                    out.write("\"");
                                                }
                                                {
                                                    String var_attrcontent87 = (("{'linkClick':'tel:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightlist, "number"), "attribute"))) + "'}");
                                                    out.write(" data-tracking-click-info-value=\"");
                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent87));
                                                    out.write("\"");
                                                }
                                                out.write(">");
                                                {
                                                    String var_88 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightlist, "contactText"), "text")) + ":\r\n                                                ");
                                                    out.write(renderContext.getObjectModel().toString(var_88));
                                                }
                                                out.write("<span>");
                                                {
                                                    String var_89 = (" " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(rightlist, "number"), "text")));
                                                    out.write(renderContext.getObjectModel().toString(var_89));
                                                }
                                                out.write("</span></a>\r\n                                        </div>\r\n                                    ");
                                            }
                                        }
                                        var_index82++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var_collectionvar75_list_coerced$ = null;
        }
        out.write("\r\n                                </a>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"home-page-left__contact--block\">\r\n                            <div class=\"home-page-left__top-panel\"></div>\r\n                            <a");
        {
            String var_attrvalue90 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "nofollow")) ? "nofollow" : "");
            {
                Object var_attrcontent91 = renderContext.call("xss", var_attrvalue90, "attribute");
                {
                    boolean var_shoulddisplayattr93 = (((null != var_attrcontent91) && (!"".equals(var_attrcontent91))) && ((!"".equals(var_attrvalue90)) && (!((Object)false).equals(var_attrvalue90))));
                    if (var_shoulddisplayattr93) {
                        out.write(" rel");
                        {
                            boolean var_istrueattr92 = (var_attrvalue90.equals(true));
                            if (!var_istrueattr92) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent91));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"home-page-left__link\"");
        {
            Object var_attrvalue94 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "buttonUrl");
            {
                Object var_attrcontent95 = renderContext.call("xss", var_attrvalue94, "uri");
                {
                    boolean var_shoulddisplayattr97 = (((null != var_attrcontent95) && (!"".equals(var_attrcontent95))) && ((!"".equals(var_attrvalue94)) && (!((Object)false).equals(var_attrvalue94))));
                    if (var_shoulddisplayattr97) {
                        out.write(" href");
                        {
                            boolean var_istrueattr96 = (var_attrvalue94.equals(true));
                            if (!var_istrueattr96) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent95));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrvalue98 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "openInNewTab")) ? "_blank" : "_self");
            {
                Object var_attrcontent99 = renderContext.call("xss", var_attrvalue98, "attribute");
                {
                    boolean var_shoulddisplayattr101 = (((null != var_attrcontent99) && (!"".equals(var_attrcontent99))) && ((!"".equals(var_attrvalue98)) && (!((Object)false).equals(var_attrvalue98))));
                    if (var_shoulddisplayattr101) {
                        out.write(" target");
                        {
                            boolean var_istrueattr100 = (var_attrvalue98.equals(true));
                            if (!var_istrueattr100) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent99));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(">\r\n                                <div class=\"home-page-left__menu-item\" data-tracking-click-event=\"linkClick\"");
        {
            Object var_attrvalue102 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "buttonInteraction");
            {
                Object var_attrcontent103 = renderContext.call("xss", var_attrvalue102, "attribute");
                {
                    boolean var_shoulddisplayattr105 = (((null != var_attrcontent103) && (!"".equals(var_attrcontent103))) && ((!"".equals(var_attrvalue102)) && (!((Object)false).equals(var_attrvalue102))));
                    if (var_shoulddisplayattr105) {
                        out.write(" data-tracking-web-interaction-value");
                        {
                            boolean var_istrueattr104 = (var_attrvalue102.equals(true));
                            if (!var_istrueattr104) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent103));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        {
            String var_attrcontent106 = (("{'linkClick':'" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "buttonText"), "attribute"))) + "'}");
            out.write(" data-tracking-click-info-value=\"");
            out.write(renderContext.getObjectModel().toString(var_attrcontent106));
            out.write("\"");
        }
        out.write(">\r\n                                    <div class=\"home-page-left__text\">");
        {
            Object var_107 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "buttonText"), "text");
            out.write(renderContext.getObjectModel().toString(var_107));
        }
        out.write("</div>\r\n                                </div>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"home-page-right__wrap\">\r\n                <picture>\r\n                    <source media=\"(min-width:768px)\"");
        {
            Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "imageMobilePath");
            {
                Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                {
                    boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                    if (var_shoulddisplayattr111) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                            if (!var_istrueattr110) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <source media=\"(min-width:280px)\"");
        {
            Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "imageMobilePath");
            {
                Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                {
                    boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                    if (var_shoulddisplayattr115) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                            if (!var_istrueattr114) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <source media=\"(max-width: 360px)\"");
        {
            Object var_attrvalue116 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "imageMobilePath");
            {
                Object var_attrcontent117 = renderContext.call("xss", var_attrvalue116, "attribute");
                {
                    boolean var_shoulddisplayattr119 = (((null != var_attrcontent117) && (!"".equals(var_attrcontent117))) && ((!"".equals(var_attrvalue116)) && (!((Object)false).equals(var_attrvalue116))));
                    if (var_shoulddisplayattr119) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr118 = (var_attrvalue116.equals(true));
                            if (!var_istrueattr118) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <source media=\"(max-width: 576px)\"");
        {
            Object var_attrvalue120 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "imageMobilePath");
            {
                Object var_attrcontent121 = renderContext.call("xss", var_attrvalue120, "attribute");
                {
                    boolean var_shoulddisplayattr123 = (((null != var_attrcontent121) && (!"".equals(var_attrcontent121))) && ((!"".equals(var_attrvalue120)) && (!((Object)false).equals(var_attrvalue120))));
                    if (var_shoulddisplayattr123) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr122 = (var_attrvalue120.equals(true));
                            if (!var_istrueattr122) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent121));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <source media=\"(min-width: 1080px)\"");
        {
            Object var_attrvalue124 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "webImagePath");
            {
                Object var_attrcontent125 = renderContext.call("xss", var_attrvalue124, "attribute");
                {
                    boolean var_shoulddisplayattr127 = (((null != var_attrcontent125) && (!"".equals(var_attrcontent125))) && ((!"".equals(var_attrvalue124)) && (!((Object)false).equals(var_attrvalue124))));
                    if (var_shoulddisplayattr127) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr126 = (var_attrvalue124.equals(true));
                            if (!var_istrueattr126) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent125));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <source media=\"(min-width: 1200px)\"");
        {
            Object var_attrvalue128 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "webImagePath");
            {
                Object var_attrcontent129 = renderContext.call("xss", var_attrvalue128, "attribute");
                {
                    boolean var_shoulddisplayattr131 = (((null != var_attrcontent129) && (!"".equals(var_attrcontent129))) && ((!"".equals(var_attrvalue128)) && (!((Object)false).equals(var_attrvalue128))));
                    if (var_shoulddisplayattr131) {
                        out.write(" srcset");
                        {
                            boolean var_istrueattr130 = (var_attrvalue128.equals(true));
                            if (!var_istrueattr130) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent129));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("/>\r\n                    <img alt=\"\"");
        {
            Object var_attrvalue132 = renderContext.getObjectModel().resolveProperty(_global_homepageblockmodel, "webImagePath");
            {
                Object var_attrcontent133 = renderContext.call("xss", var_attrvalue132, "uri");
                {
                    boolean var_shoulddisplayattr135 = (((null != var_attrcontent133) && (!"".equals(var_attrcontent133))) && ((!"".equals(var_attrvalue132)) && (!((Object)false).equals(var_attrvalue132))));
                    if (var_shoulddisplayattr135) {
                        out.write(" src");
                        {
                            boolean var_istrueattr134 = (var_attrvalue132.equals(true));
                            if (!var_istrueattr134) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent133));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" decoding=\"async\" data-nimg=\"fill\" class=\"home-page-right__img home-page-right__img--mobile\"/>\r\n                </picture>\r\n            </div>\r\n        </div>\r\n    </section>\r\n");
    }
}
out.write("\r\n");
{
    Object var_templatevar136 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions137_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions137 = obj().with("isEmpty", var_templateoptions137_field$_isempty);
            callUnit(out, renderContext, var_templatevar136, var_templateoptions137);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

