/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.atomic.divider;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class divider__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_divider = null;
Object _global_template = null;
_global_divider = renderContext.call("use", com.techcombank.core.models.DividerModel.class.getName(), obj());
out.write("   \r\n    <div>\r\n        ");
{
    boolean var_testvariable0 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_divider, "dividerStyle"), "Dotted"));
    if (var_testvariable0) {
        out.write("\r\n            ");
        {
            Object var_testvariable1 = renderContext.getObjectModel().resolveProperty(_global_divider, "labelText");
            if (renderContext.getObjectModel().toBoolean(var_testvariable1)) {
                out.write("\r\n                <span class=\"divider--dotted\">");
                {
                    Object var_2 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_divider, "labelText"), "text");
                    out.write(renderContext.getObjectModel().toString(var_2));
                }
                out.write("</span>\r\n            ");
            }
        }
        out.write("\r\n            ");
        {
            boolean var_testvariable3 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_divider, "labelText")));
            if (var_testvariable3) {
                out.write("\r\n                <span class=\"divider--dotted no-content\"></span>\r\n            ");
            }
        }
        out.write("\r\n        ");
    }
}
out.write("\r\n        ");
{
    boolean var_testvariable4 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(_global_divider, "dividerStyle"), "Solid"));
    if (var_testvariable4) {
        out.write("\r\n            <hr/>\r\n        ");
    }
}
out.write("\r\n    </div>\r\n    ");
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_divider = renderContext.getObjectModel().resolveProperty(_global_divider, "dividerStyle");
if (renderContext.getObjectModel().toBoolean(_global_divider)) {
}
out.write("\r\n    ");
{
    Object var_templatevar5 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions6_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_divider));
        {
            java.util.Map var_templateoptions6 = obj().with("isEmpty", var_templateoptions6_field$_isempty);
            callUnit(out, renderContext, var_templatevar5, var_templateoptions6);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

