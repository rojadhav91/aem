/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.extended__002d__core.title;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class title__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_title = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _dynamic_wcmmode = bindings.get("wcmmode");
_global_title = renderContext.call("use", com.techcombank.core.models.ExtendedTitle.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_title, "text");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"title-component container\">\r\n            <div");
            {
                String var_attrcontent1 = ((((("title-cmp align-" + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_title, "alignment")) ? renderContext.getObjectModel().resolveProperty(_global_title, "alignment") : ""), "html"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) ? "title-cmp--row" : ""), "attribute"))) + " tcb-header-display ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_title, "separatorStyle")) ? "title-separator" : "title-cmp--row"), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            {
                String var_attrcontent2 = (("color:" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_title, "fontColorStyle"), "styleToken"))) + ";");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                out.write("\"");
            }
            out.write(">\r\n                ");
            {
                Object var_tagvar3 = renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_title, "type")) ? renderContext.getObjectModel().resolveProperty(_global_title, "type") : ""), "elementName");
                if (renderContext.getObjectModel().toBoolean(var_tagvar3)) {
                    out.write("<");
                    out.write(renderContext.getObjectModel().toString(var_tagvar3));
                }
                if (!renderContext.getObjectModel().toBoolean(var_tagvar3)) {
                    out.write("<h2");
                }
                {
                    Object var_attrvalue4 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_title, "id")) ? renderContext.getObjectModel().resolveProperty(_global_title, "id") : "");
                    {
                        Object var_attrcontent5 = renderContext.call("xss", var_attrvalue4, "attribute");
                        {
                            boolean var_shoulddisplayattr7 = (((null != var_attrcontent5) && (!"".equals(var_attrcontent5))) && ((!"".equals(var_attrvalue4)) && (!((Object)false).equals(var_attrvalue4))));
                            if (var_shoulddisplayattr7) {
                                out.write(" id");
                                {
                                    boolean var_istrueattr6 = (var_attrvalue4.equals(true));
                                    if (!var_istrueattr6) {
                                        out.write("=\"");
                                        out.write(renderContext.getObjectModel().toString(var_attrcontent5));
                                        out.write("\"");
                                    }
                                }
                            }
                        }
                    }
                }
                {
                    String var_attrcontent8 = ("tcb-title " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_title, "id")) ? "scroll-margin-top-header" : ""), "attribute")));
                    out.write(" class=\"");
                    out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                    out.write("\"");
                }
                out.write(">");
                {
                    String var_9 = (("\r\n                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_title, "text"), "html"))) + "\r\n                ");
                    out.write(renderContext.getObjectModel().toString(var_9));
                }
                if (renderContext.getObjectModel().toBoolean(var_tagvar3)) {
                    out.write("</");
                    out.write(renderContext.getObjectModel().toString(var_tagvar3));
                    out.write(">");
                }
                if (!renderContext.getObjectModel().toBoolean(var_tagvar3)) {
                    out.write("</h2>");
                }
            }
            out.write("\r\n                <div");
            {
                String var_attrvalue10 = ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))) ? "title-cmp--parsys" : "");
                {
                    Object var_attrcontent11 = renderContext.call("xss", var_attrvalue10, "attribute");
                    {
                        boolean var_shoulddisplayattr13 = (((null != var_attrcontent11) && (!"".equals(var_attrcontent11))) && ((!"".equals(var_attrvalue10)) && (!((Object)false).equals(var_attrvalue10))));
                        if (var_shoulddisplayattr13) {
                            out.write(" class");
                            {
                                boolean var_istrueattr12 = (var_attrvalue10.equals(true));
                                if (!var_istrueattr12) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n                    ");
            {
                Object var_testvariable14 = renderContext.getObjectModel().resolveProperty(_global_title, "separatorStyle");
                if (renderContext.getObjectModel().toBoolean(var_testvariable14)) {
                    out.write("\r\n                        ");
                    {
                        Object var_resourcecontent15 = renderContext.call("includeResource", null, obj().with("path", "title-title.id").with("resourceType", "foundation/components/parsys"));
                        out.write(renderContext.getObjectModel().toString(var_resourcecontent15));
                    }
                    out.write("\r\n                    ");
                }
            }
            out.write("\r\n                </div>\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n");
}
out.write("\r\n");
{
    Object var_templatevar16 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions17_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions17 = obj().with("isEmpty", var_templateoptions17_field$_isempty);
            callUnit(out, renderContext, var_templatevar16, var_templateoptions17);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

