/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.quickaccess;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class quickaccess__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_quickaccessmodel = null;
Collection var_collectionvar23_list_coerced$ = null;
Collection var_collectionvar79_list_coerced$ = null;
Collection var_collectionvar156_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p class=\"cq-placeholder\"");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write("></p>\r\n");
    }
}
out.write("\r\n");
_global_quickaccessmodel = renderContext.call("use", com.techcombank.core.models.QuickAccessModel.class.getName(), obj());
out.write("\r\n    <div class=\"quick-access\">\r\n        <div class=\"quick-access__table-desktop\">\r\n            <div class=\"quick-access-wrapper\">\r\n                <div class=\"table-header\">\r\n                    <div class=\"table-header__items\">\r\n                        <button aria-selected=\"true\" class=\"header__button\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent5 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneHeading"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent5));
    out.write("\"");
}
{
    String var_attrcontent6 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent6));
    out.write("\"");
}
out.write(" role=\"tab\" tabindex=\"0\" type=\"button\">");
{
    String var_7 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneHeading"), "text"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_7));
}
out.write("</button>\r\n                        <button aria-selected=\"false\" class=\"header__button\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent8 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoHeading"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent8));
    out.write("\"");
}
{
    String var_attrcontent9 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent9));
    out.write("\"");
}
out.write(" role=\"tab\" tabindex=\"-1\" type=\"button\">");
{
    String var_10 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoHeading"), "text"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_10));
}
out.write("</button>\r\n                        <button aria-selected=\"false\" class=\"header__button\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent11 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeHeading"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
    out.write("\"");
}
{
    String var_attrcontent12 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': 'other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent12));
    out.write("\"");
}
out.write(" role=\"tab\" tabindex=\"-1\" type=\"button\">");
{
    String var_13 = (("\r\n                            " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeHeading"), "text"))) + "\r\n                        ");
    out.write(renderContext.getObjectModel().toString(var_13));
}
out.write("</button>\r\n                    </div>\r\n                </div>\r\n                <div class=\"table-records\">\r\n                    <div class=\"table-records__inner\">\r\n                        <div class=\"table-records__inner-top\">\r\n                            <div class=\"table-records__inner-top-header\">\r\n                                <div class=\"header__title\"></div>\r\n                                <div class=\"header__title\">\r\n                                    <p>\r\n                                        <strong>");
{
    Object var_14 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingOne"), "text");
    out.write(renderContext.getObjectModel().toString(var_14));
}
out.write("</strong>\r\n                                    </p>\r\n                                    ");
{
    Object var_testvariable15 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColOne");
    if (renderContext.getObjectModel().toBoolean(var_testvariable15)) {
        out.write("\r\n                                        <p>");
        {
            Object var_16 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColOne"), "text");
            out.write(renderContext.getObjectModel().toString(var_16));
        }
        out.write("</p>\r\n                                    ");
    }
}
out.write("\r\n                                </div>\r\n                                <div class=\"header__title\">\r\n                                    <p>\r\n                                        <strong>");
{
    Object var_17 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingTwo"), "text");
    out.write(renderContext.getObjectModel().toString(var_17));
}
out.write("</strong>\r\n                                    </p>\r\n                                    ");
{
    Object var_testvariable18 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColTwo");
    if (renderContext.getObjectModel().toBoolean(var_testvariable18)) {
        out.write("\r\n                                        <p>");
        {
            Object var_19 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColTwo"), "text");
            out.write(renderContext.getObjectModel().toString(var_19));
        }
        out.write("</p>\r\n                                    ");
    }
}
out.write("\r\n                                </div>\r\n                                <div class=\"header__title\">\r\n                                    <p>\r\n                                        <strong>");
{
    Object var_20 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingThree"), "text");
    out.write(renderContext.getObjectModel().toString(var_20));
}
out.write("</strong>\r\n                                    </p>\r\n                                    ");
{
    Object var_testvariable21 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColThree");
    if (renderContext.getObjectModel().toBoolean(var_testvariable21)) {
        out.write("\r\n                                        <p>");
        {
            Object var_22 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "subbHeadingColThree"), "text");
            out.write(renderContext.getObjectModel().toString(var_22));
        }
        out.write("</p>\r\n                                    ");
    }
}
out.write("\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"table-records__inner-top-body\">\r\n                                ");
{
    Object var_collectionvar23 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "exchangeRateList");
    {
        long var_size24 = ((var_collectionvar23_list_coerced$ == null ? (var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23)) : var_collectionvar23_list_coerced$).size());
        {
            boolean var_notempty25 = (var_size24 > 0);
            if (var_notempty25) {
                {
                    long var_end28 = var_size24;
                    {
                        boolean var_validstartstepend29 = (((0 < var_size24) && true) && (var_end28 > 0));
                        if (var_validstartstepend29) {
                            if (var_collectionvar23_list_coerced$ == null) {
                                var_collectionvar23_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar23);
                            }
                            long var_index30 = 0;
                            for (Object exchangerateitem : var_collectionvar23_list_coerced$) {
                                {
                                    boolean var_traversal32 = (((var_index30 >= 0) && (var_index30 <= var_end28)) && true);
                                    if (var_traversal32) {
                                        out.write("\r\n                                    <a class=\"body__item\" href=\"/\">\r\n                                        <div class=\"body__item-flag\">\r\n                                            <img alt=\"\" data-nimg=\"fixed\" decoding=\"async\"");
                                        {
                                            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(exchangerateitem, "flag");
                                            {
                                                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "uri");
                                                {
                                                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                    if (var_shoulddisplayattr36) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                            if (!var_istrueattr35) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            String var_attrcontent37 = ((renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "flag"), "attribute")) + ",\r\n                                                ") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "flag"), "attribute")));
                                            out.write(" srcset=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                            out.write("\"");
                                        }
                                        out.write("/>\r\n                                            <p>");
                                        {
                                            Object var_38 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "label"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_38));
                                        }
                                        out.write("</p>\r\n                                        </div>\r\n                                        <p class=\"body__item-currency-value\">\r\n                                            <span>");
                                        {
                                            Object var_39 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "bidRateCK"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_39));
                                        }
                                        out.write("</span>\r\n                                        </p>\r\n                                        <p class=\"body__item-currency-value\">\r\n                                            <span>");
                                        {
                                            Object var_40 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "bidRateTM"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_40));
                                        }
                                        out.write("</span>\r\n                                        </p>\r\n                                        <p class=\"body__item-currency-value\">\r\n                                            <span>");
                                        {
                                            Object var_41 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(exchangerateitem, "askRate"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_41));
                                        }
                                        out.write("</span>\r\n                                        </p>\r\n                                    </a>");
                                    }
                                }
                                var_index30++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar23_list_coerced$ = null;
}
out.write("\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"table-records__inner-footer\">\r\n                            <p class=\"inner-footer__right-text\">");
{
    String var_42 = ("\r\n                                " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "dateChangeNote"), "text")));
    out.write(renderContext.getObjectModel().toString(var_42));
}
out.write("</p>\r\n                            <a class=\"inner-footer__button-link\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent43 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneViewAll"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent43));
    out.write("\"");
}
{
    String var_attrcontent44 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneLinkInteractionType"), "attribute"))) + "'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent44));
    out.write("\"");
}
{
    Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneLink");
    {
        Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "uri");
        {
            boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
            if (var_shoulddisplayattr48) {
                out.write(" href");
                {
                    boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                    if (!var_istrueattr47) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrcontent49 = ("noopener noreferrer " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneNoFollow")) ? "nofollow" : ""), "attribute")));
    out.write(" rel=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent49));
    out.write("\"");
}
{
    Object var_attrvalue50 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneTarget");
    {
        Object var_attrcontent51 = renderContext.call("xss", var_attrvalue50, "attribute");
        {
            boolean var_shoulddisplayattr53 = (((null != var_attrcontent51) && (!"".equals(var_attrcontent51))) && ((!"".equals(var_attrvalue50)) && (!((Object)false).equals(var_attrvalue50))));
            if (var_shoulddisplayattr53) {
                out.write(" target");
                {
                    boolean var_istrueattr52 = (var_attrvalue50.equals(true));
                    if (!var_istrueattr52) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent51));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                <span class=\"button-link__text\">");
{
    Object var_54 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneViewAll"), "text");
    out.write(renderContext.getObjectModel().toString(var_54));
}
out.write("</span>\r\n                                <div class=\"button-link__svg\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue55 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconMobileImagePath");
    {
        Object var_attrcontent56 = renderContext.call("xss", var_attrvalue55, "attribute");
        {
            boolean var_shoulddisplayattr58 = (((null != var_attrcontent56) && (!"".equals(var_attrcontent56))) && ((!"".equals(var_attrvalue55)) && (!((Object)false).equals(var_attrvalue55))));
            if (var_shoulddisplayattr58) {
                out.write(" srcset");
                {
                    boolean var_istrueattr57 = (var_attrvalue55.equals(true));
                    if (!var_istrueattr57) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent56));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue59 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconMobileImagePath");
    {
        Object var_attrcontent60 = renderContext.call("xss", var_attrvalue59, "attribute");
        {
            boolean var_shoulddisplayattr62 = (((null != var_attrcontent60) && (!"".equals(var_attrcontent60))) && ((!"".equals(var_attrvalue59)) && (!((Object)false).equals(var_attrvalue59))));
            if (var_shoulddisplayattr62) {
                out.write(" srcset");
                {
                    boolean var_istrueattr61 = (var_attrvalue59.equals(true));
                    if (!var_istrueattr61) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent60));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue63 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconWebImagePath");
    {
        Object var_attrcontent64 = renderContext.call("xss", var_attrvalue63, "attribute");
        {
            boolean var_shoulddisplayattr66 = (((null != var_attrcontent64) && (!"".equals(var_attrcontent64))) && ((!"".equals(var_attrvalue63)) && (!((Object)false).equals(var_attrvalue63))));
            if (var_shoulddisplayattr66) {
                out.write(" srcset");
                {
                    boolean var_istrueattr65 = (var_attrvalue63.equals(true));
                    if (!var_istrueattr65) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent64));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconWebImagePath");
    {
        Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
        {
            boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
            if (var_shoulddisplayattr70) {
                out.write(" srcset");
                {
                    boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                    if (!var_istrueattr69) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <img");
{
    Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconAlt");
    {
        Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
        {
            boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
            if (var_shoulddisplayattr74) {
                out.write(" alt");
                {
                    boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                    if (!var_istrueattr73) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconWebImagePath");
    {
        Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "uri");
        {
            boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
            if (var_shoulddisplayattr78) {
                out.write(" src");
                {
                    boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                    if (!var_istrueattr77) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    </picture>\r\n                                </div>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"table-records__inner simple\">\r\n                        <div class=\"table-records__inner-top\">\r\n                            <div class=\"table-records__inner-top-body\">\r\n                                ");
{
    Object var_collectionvar79 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoFileSection");
    {
        long var_size80 = ((var_collectionvar79_list_coerced$ == null ? (var_collectionvar79_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar79)) : var_collectionvar79_list_coerced$).size());
        {
            boolean var_notempty81 = (var_size80 > 0);
            if (var_notempty81) {
                {
                    long var_end84 = var_size80;
                    {
                        boolean var_validstartstepend85 = (((0 < var_size80) && true) && (var_end84 > 0));
                        if (var_validstartstepend85) {
                            if (var_collectionvar79_list_coerced$ == null) {
                                var_collectionvar79_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar79);
                            }
                            long var_index86 = 0;
                            for (Object tabtwofileitem : var_collectionvar79_list_coerced$) {
                                {
                                    boolean var_traversal88 = (((var_index86 >= 0) && (var_index86 <= var_end84)) && true);
                                    if (var_traversal88) {
                                        out.write("\r\n                                    <a class=\"body__item\" data-tracking-click-event=\"linkClick\"");
                                        {
                                            String var_attrcontent89 = (("{'downlaod' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileName"), "attribute"))) + "'}");
                                            out.write(" data-tracking-click-info-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent89));
                                            out.write("\"");
                                        }
                                        {
                                            String var_attrcontent90 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileType"), "attribute"))) + "'}}");
                                            out.write(" data-tracking-web-interaction-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                            out.write("\"");
                                        }
                                        {
                                            Object var_attrvalue91 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "filePath");
                                            {
                                                Object var_attrcontent92 = renderContext.call("xss", var_attrvalue91, "uri");
                                                {
                                                    boolean var_shoulddisplayattr94 = (((null != var_attrcontent92) && (!"".equals(var_attrcontent92))) && ((!"".equals(var_attrvalue91)) && (!((Object)false).equals(var_attrvalue91))));
                                                    if (var_shoulddisplayattr94) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr93 = (var_attrvalue91.equals(true));
                                                            if (!var_istrueattr93) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">\r\n                                        <p class=\"body__item-currency-value\">");
                                        {
                                            Object var_95 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileName"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_95));
                                        }
                                        out.write("</p>\r\n                                        <span> <picture>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(max-width: 360px)\"");
                                        {
                                            Object var_attrvalue96 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconMobileImagePath");
                                            {
                                                Object var_attrcontent97 = renderContext.call("xss", var_attrvalue96, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr99 = (((null != var_attrcontent97) && (!"".equals(var_attrcontent97))) && ((!"".equals(var_attrvalue96)) && (!((Object)false).equals(var_attrvalue96))));
                                                    if (var_shoulddisplayattr99) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr98 = (var_attrvalue96.equals(true));
                                                            if (!var_istrueattr98) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent97));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(max-width: 576px)\"");
                                        {
                                            Object var_attrvalue100 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconMobileImagePath");
                                            {
                                                Object var_attrcontent101 = renderContext.call("xss", var_attrvalue100, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr103 = (((null != var_attrcontent101) && (!"".equals(var_attrcontent101))) && ((!"".equals(var_attrvalue100)) && (!((Object)false).equals(var_attrvalue100))));
                                                    if (var_shoulddisplayattr103) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr102 = (var_attrvalue100.equals(true));
                                                            if (!var_istrueattr102) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(min-width: 1080px)\"");
                                        {
                                            Object var_attrvalue104 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent105 = renderContext.call("xss", var_attrvalue104, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr107 = (((null != var_attrcontent105) && (!"".equals(var_attrcontent105))) && ((!"".equals(var_attrvalue104)) && (!((Object)false).equals(var_attrvalue104))));
                                                    if (var_shoulddisplayattr107) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr106 = (var_attrvalue104.equals(true));
                                                            if (!var_istrueattr106) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent105));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(min-width: 1200px)\"");
                                        {
                                            Object var_attrvalue108 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent109 = renderContext.call("xss", var_attrvalue108, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr111 = (((null != var_attrcontent109) && (!"".equals(var_attrcontent109))) && ((!"".equals(var_attrvalue108)) && (!((Object)false).equals(var_attrvalue108))));
                                                    if (var_shoulddisplayattr111) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr110 = (var_attrvalue108.equals(true));
                                                            if (!var_istrueattr110) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent109));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<img");
                                        {
                                            Object var_attrvalue112 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconAlt");
                                            {
                                                Object var_attrcontent113 = renderContext.call("xss", var_attrvalue112, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr115 = (((null != var_attrcontent113) && (!"".equals(var_attrcontent113))) && ((!"".equals(var_attrvalue112)) && (!((Object)false).equals(var_attrvalue112))));
                                                    if (var_shoulddisplayattr115) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr114 = (var_attrvalue112.equals(true));
                                                            if (!var_istrueattr114) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent113));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue116 = renderContext.getObjectModel().resolveProperty(tabtwofileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent117 = renderContext.call("xss", var_attrvalue116, "uri");
                                                {
                                                    boolean var_shoulddisplayattr119 = (((null != var_attrcontent117) && (!"".equals(var_attrcontent117))) && ((!"".equals(var_attrvalue116)) && (!((Object)false).equals(var_attrvalue116))));
                                                    if (var_shoulddisplayattr119) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr118 = (var_attrvalue116.equals(true));
                                                            if (!var_istrueattr118) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent117));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/> </picture>\r\n\t\t\t\t\t\t\t</span>\r\n                                    </a>");
                                    }
                                }
                                var_index86++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar79_list_coerced$ = null;
}
out.write("\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"table-records__inner-footer\">\r\n                            <a class=\"inner-footer__button-link\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent120 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoViewAll"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent120));
    out.write("\"");
}
{
    String var_attrcontent121 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoLinkInteractionType"), "attribute"))) + "'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent121));
    out.write("\"");
}
{
    Object var_attrvalue122 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoLink");
    {
        Object var_attrcontent123 = renderContext.call("xss", var_attrvalue122, "uri");
        {
            boolean var_shoulddisplayattr125 = (((null != var_attrcontent123) && (!"".equals(var_attrcontent123))) && ((!"".equals(var_attrvalue122)) && (!((Object)false).equals(var_attrvalue122))));
            if (var_shoulddisplayattr125) {
                out.write(" href");
                {
                    boolean var_istrueattr124 = (var_attrvalue122.equals(true));
                    if (!var_istrueattr124) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent123));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrcontent126 = ("noopener noreferrer " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoNoFollow")) ? "nofollow" : ""), "attribute")));
    out.write(" rel=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent126));
    out.write("\"");
}
{
    Object var_attrvalue127 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoTarget");
    {
        Object var_attrcontent128 = renderContext.call("xss", var_attrvalue127, "attribute");
        {
            boolean var_shoulddisplayattr130 = (((null != var_attrcontent128) && (!"".equals(var_attrcontent128))) && ((!"".equals(var_attrvalue127)) && (!((Object)false).equals(var_attrvalue127))));
            if (var_shoulddisplayattr130) {
                out.write(" target");
                {
                    boolean var_istrueattr129 = (var_attrvalue127.equals(true));
                    if (!var_istrueattr129) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent128));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                <span class=\"button-link__text\">");
{
    Object var_131 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoViewAll"), "text");
    out.write(renderContext.getObjectModel().toString(var_131));
}
out.write("</span>\r\n                                <div class=\"button-link__svg\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue132 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconMobileImagePath");
    {
        Object var_attrcontent133 = renderContext.call("xss", var_attrvalue132, "attribute");
        {
            boolean var_shoulddisplayattr135 = (((null != var_attrcontent133) && (!"".equals(var_attrcontent133))) && ((!"".equals(var_attrvalue132)) && (!((Object)false).equals(var_attrvalue132))));
            if (var_shoulddisplayattr135) {
                out.write(" srcset");
                {
                    boolean var_istrueattr134 = (var_attrvalue132.equals(true));
                    if (!var_istrueattr134) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent133));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue136 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconMobileImagePath");
    {
        Object var_attrcontent137 = renderContext.call("xss", var_attrvalue136, "attribute");
        {
            boolean var_shoulddisplayattr139 = (((null != var_attrcontent137) && (!"".equals(var_attrcontent137))) && ((!"".equals(var_attrvalue136)) && (!((Object)false).equals(var_attrvalue136))));
            if (var_shoulddisplayattr139) {
                out.write(" srcset");
                {
                    boolean var_istrueattr138 = (var_attrvalue136.equals(true));
                    if (!var_istrueattr138) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent137));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue140 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconWebImagePath");
    {
        Object var_attrcontent141 = renderContext.call("xss", var_attrvalue140, "attribute");
        {
            boolean var_shoulddisplayattr143 = (((null != var_attrcontent141) && (!"".equals(var_attrcontent141))) && ((!"".equals(var_attrvalue140)) && (!((Object)false).equals(var_attrvalue140))));
            if (var_shoulddisplayattr143) {
                out.write(" srcset");
                {
                    boolean var_istrueattr142 = (var_attrvalue140.equals(true));
                    if (!var_istrueattr142) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent141));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue144 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconWebImagePath");
    {
        Object var_attrcontent145 = renderContext.call("xss", var_attrvalue144, "attribute");
        {
            boolean var_shoulddisplayattr147 = (((null != var_attrcontent145) && (!"".equals(var_attrcontent145))) && ((!"".equals(var_attrvalue144)) && (!((Object)false).equals(var_attrvalue144))));
            if (var_shoulddisplayattr147) {
                out.write(" srcset");
                {
                    boolean var_istrueattr146 = (var_attrvalue144.equals(true));
                    if (!var_istrueattr146) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent145));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <img");
{
    Object var_attrvalue148 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconAlt");
    {
        Object var_attrcontent149 = renderContext.call("xss", var_attrvalue148, "attribute");
        {
            boolean var_shoulddisplayattr151 = (((null != var_attrcontent149) && (!"".equals(var_attrcontent149))) && ((!"".equals(var_attrvalue148)) && (!((Object)false).equals(var_attrvalue148))));
            if (var_shoulddisplayattr151) {
                out.write(" alt");
                {
                    boolean var_istrueattr150 = (var_attrvalue148.equals(true));
                    if (!var_istrueattr150) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent149));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue152 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconWebImagePath");
    {
        Object var_attrcontent153 = renderContext.call("xss", var_attrvalue152, "uri");
        {
            boolean var_shoulddisplayattr155 = (((null != var_attrcontent153) && (!"".equals(var_attrcontent153))) && ((!"".equals(var_attrvalue152)) && (!((Object)false).equals(var_attrvalue152))));
            if (var_shoulddisplayattr155) {
                out.write(" src");
                {
                    boolean var_istrueattr154 = (var_attrvalue152.equals(true));
                    if (!var_istrueattr154) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent153));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    </picture>\r\n                                </div>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"table-records__inner simple\">\r\n                        <div class=\"table-records__inner-top\">\r\n                            <div class=\"table-records__inner-top-body\">\r\n                                ");
{
    Object var_collectionvar156 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeFileSection");
    {
        long var_size157 = ((var_collectionvar156_list_coerced$ == null ? (var_collectionvar156_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar156)) : var_collectionvar156_list_coerced$).size());
        {
            boolean var_notempty158 = (var_size157 > 0);
            if (var_notempty158) {
                {
                    long var_end161 = var_size157;
                    {
                        boolean var_validstartstepend162 = (((0 < var_size157) && true) && (var_end161 > 0));
                        if (var_validstartstepend162) {
                            if (var_collectionvar156_list_coerced$ == null) {
                                var_collectionvar156_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar156);
                            }
                            long var_index163 = 0;
                            for (Object tabthreefileitem : var_collectionvar156_list_coerced$) {
                                {
                                    boolean var_traversal165 = (((var_index163 >= 0) && (var_index163 <= var_end161)) && true);
                                    if (var_traversal165) {
                                        out.write("\r\n                                    <a class=\"body__item\" data-tracking-click-event=\"linkClick\"");
                                        {
                                            String var_attrcontent166 = (("{'downlaod' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileName"), "attribute"))) + "'}");
                                            out.write(" data-tracking-click-info-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent166));
                                            out.write("\"");
                                        }
                                        {
                                            String var_attrcontent167 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileType"), "attribute"))) + "'}}");
                                            out.write(" data-tracking-web-interaction-value=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent167));
                                            out.write("\"");
                                        }
                                        {
                                            Object var_attrvalue168 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "filePath");
                                            {
                                                Object var_attrcontent169 = renderContext.call("xss", var_attrvalue168, "uri");
                                                {
                                                    boolean var_shoulddisplayattr171 = (((null != var_attrcontent169) && (!"".equals(var_attrcontent169))) && ((!"".equals(var_attrvalue168)) && (!((Object)false).equals(var_attrvalue168))));
                                                    if (var_shoulddisplayattr171) {
                                                        out.write(" href");
                                                        {
                                                            boolean var_istrueattr170 = (var_attrvalue168.equals(true));
                                                            if (!var_istrueattr170) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent169));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">\r\n                                        <p class=\"body__item-currency-value\">");
                                        {
                                            Object var_172 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileName"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_172));
                                        }
                                        out.write("</p>\r\n                                        <span> <picture>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(max-width: 360px)\"");
                                        {
                                            Object var_attrvalue173 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconMobileImagePath");
                                            {
                                                Object var_attrcontent174 = renderContext.call("xss", var_attrvalue173, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr176 = (((null != var_attrcontent174) && (!"".equals(var_attrcontent174))) && ((!"".equals(var_attrvalue173)) && (!((Object)false).equals(var_attrvalue173))));
                                                    if (var_shoulddisplayattr176) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr175 = (var_attrvalue173.equals(true));
                                                            if (!var_istrueattr175) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent174));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(max-width: 576px)\"");
                                        {
                                            Object var_attrvalue177 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconMobileImagePath");
                                            {
                                                Object var_attrcontent178 = renderContext.call("xss", var_attrvalue177, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr180 = (((null != var_attrcontent178) && (!"".equals(var_attrcontent178))) && ((!"".equals(var_attrvalue177)) && (!((Object)false).equals(var_attrvalue177))));
                                                    if (var_shoulddisplayattr180) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr179 = (var_attrvalue177.equals(true));
                                                            if (!var_istrueattr179) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent178));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(min-width: 1080px)\"");
                                        {
                                            Object var_attrvalue181 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent182 = renderContext.call("xss", var_attrvalue181, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr184 = (((null != var_attrcontent182) && (!"".equals(var_attrcontent182))) && ((!"".equals(var_attrvalue181)) && (!((Object)false).equals(var_attrvalue181))));
                                                    if (var_shoulddisplayattr184) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr183 = (var_attrvalue181.equals(true));
                                                            if (!var_istrueattr183) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent182));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<source media=\"(min-width: 1200px)\"");
                                        {
                                            Object var_attrvalue185 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent186 = renderContext.call("xss", var_attrvalue185, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr188 = (((null != var_attrcontent186) && (!"".equals(var_attrcontent186))) && ((!"".equals(var_attrvalue185)) && (!((Object)false).equals(var_attrvalue185))));
                                                    if (var_shoulddisplayattr188) {
                                                        out.write(" srcset");
                                                        {
                                                            boolean var_istrueattr187 = (var_attrvalue185.equals(true));
                                                            if (!var_istrueattr187) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent186));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n\t\t\t\t\t\t\t\t\t<img");
                                        {
                                            Object var_attrvalue189 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconAlt");
                                            {
                                                Object var_attrcontent190 = renderContext.call("xss", var_attrvalue189, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr192 = (((null != var_attrcontent190) && (!"".equals(var_attrcontent190))) && ((!"".equals(var_attrvalue189)) && (!((Object)false).equals(var_attrvalue189))));
                                                    if (var_shoulddisplayattr192) {
                                                        out.write(" alt");
                                                        {
                                                            boolean var_istrueattr191 = (var_attrvalue189.equals(true));
                                                            if (!var_istrueattr191) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent190));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        {
                                            Object var_attrvalue193 = renderContext.getObjectModel().resolveProperty(tabthreefileitem, "fileIconWebImagePath");
                                            {
                                                Object var_attrcontent194 = renderContext.call("xss", var_attrvalue193, "uri");
                                                {
                                                    boolean var_shoulddisplayattr196 = (((null != var_attrcontent194) && (!"".equals(var_attrcontent194))) && ((!"".equals(var_attrvalue193)) && (!((Object)false).equals(var_attrvalue193))));
                                                    if (var_shoulddisplayattr196) {
                                                        out.write(" src");
                                                        {
                                                            boolean var_istrueattr195 = (var_attrvalue193.equals(true));
                                                            if (!var_istrueattr195) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent194));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/> </picture>\r\n\t\t\t\t\t\t\t</span>\r\n                                    </a>");
                                    }
                                }
                                var_index163++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar156_list_coerced$ = null;
}
out.write("\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"table-records__inner-footer\">\r\n                            <a class=\"inner-footer__button-link\" data-tracking-click-event=\"linkClick\"");
{
    String var_attrcontent197 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeViewAll"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent197));
    out.write("\"");
}
{
    String var_attrcontent198 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeLinkInteractionType"), "attribute"))) + "'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent198));
    out.write("\"");
}
{
    Object var_attrvalue199 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeLink");
    {
        Object var_attrcontent200 = renderContext.call("xss", var_attrvalue199, "uri");
        {
            boolean var_shoulddisplayattr202 = (((null != var_attrcontent200) && (!"".equals(var_attrcontent200))) && ((!"".equals(var_attrvalue199)) && (!((Object)false).equals(var_attrvalue199))));
            if (var_shoulddisplayattr202) {
                out.write(" href");
                {
                    boolean var_istrueattr201 = (var_attrvalue199.equals(true));
                    if (!var_istrueattr201) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent200));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
{
    String var_attrcontent203 = ("noopener noreferrer " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeNoFollow")) ? "nofollow" : ""), "attribute")));
    out.write(" rel=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent203));
    out.write("\"");
}
{
    Object var_attrvalue204 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeTarget");
    {
        Object var_attrcontent205 = renderContext.call("xss", var_attrvalue204, "attribute");
        {
            boolean var_shoulddisplayattr207 = (((null != var_attrcontent205) && (!"".equals(var_attrcontent205))) && ((!"".equals(var_attrvalue204)) && (!((Object)false).equals(var_attrvalue204))));
            if (var_shoulddisplayattr207) {
                out.write(" target");
                {
                    boolean var_istrueattr206 = (var_attrvalue204.equals(true));
                    if (!var_istrueattr206) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent205));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n                                <span class=\"button-link__text\">");
{
    Object var_208 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeViewAll"), "text");
    out.write(renderContext.getObjectModel().toString(var_208));
}
out.write("</span>\r\n                                <div class=\"button-link__svg\">\r\n                                    <picture>\r\n                                        <source media=\"(max-width: 360px)\"");
{
    Object var_attrvalue209 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconMobileImagePath");
    {
        Object var_attrcontent210 = renderContext.call("xss", var_attrvalue209, "attribute");
        {
            boolean var_shoulddisplayattr212 = (((null != var_attrcontent210) && (!"".equals(var_attrcontent210))) && ((!"".equals(var_attrvalue209)) && (!((Object)false).equals(var_attrvalue209))));
            if (var_shoulddisplayattr212) {
                out.write(" srcset");
                {
                    boolean var_istrueattr211 = (var_attrvalue209.equals(true));
                    if (!var_istrueattr211) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent210));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(max-width: 576px)\"");
{
    Object var_attrvalue213 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconMobileImagePath");
    {
        Object var_attrcontent214 = renderContext.call("xss", var_attrvalue213, "attribute");
        {
            boolean var_shoulddisplayattr216 = (((null != var_attrcontent214) && (!"".equals(var_attrcontent214))) && ((!"".equals(var_attrvalue213)) && (!((Object)false).equals(var_attrvalue213))));
            if (var_shoulddisplayattr216) {
                out.write(" srcset");
                {
                    boolean var_istrueattr215 = (var_attrvalue213.equals(true));
                    if (!var_istrueattr215) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent214));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1080px)\"");
{
    Object var_attrvalue217 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconWebImagePath");
    {
        Object var_attrcontent218 = renderContext.call("xss", var_attrvalue217, "attribute");
        {
            boolean var_shoulddisplayattr220 = (((null != var_attrcontent218) && (!"".equals(var_attrcontent218))) && ((!"".equals(var_attrvalue217)) && (!((Object)false).equals(var_attrvalue217))));
            if (var_shoulddisplayattr220) {
                out.write(" srcset");
                {
                    boolean var_istrueattr219 = (var_attrvalue217.equals(true));
                    if (!var_istrueattr219) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent218));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <source media=\"(min-width: 1200px)\"");
{
    Object var_attrvalue221 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconWebImagePath");
    {
        Object var_attrcontent222 = renderContext.call("xss", var_attrvalue221, "attribute");
        {
            boolean var_shoulddisplayattr224 = (((null != var_attrcontent222) && (!"".equals(var_attrcontent222))) && ((!"".equals(var_attrvalue221)) && (!((Object)false).equals(var_attrvalue221))));
            if (var_shoulddisplayattr224) {
                out.write(" srcset");
                {
                    boolean var_istrueattr223 = (var_attrvalue221.equals(true));
                    if (!var_istrueattr223) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent222));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                        <img");
{
    Object var_attrvalue225 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconAlt");
    {
        Object var_attrcontent226 = renderContext.call("xss", var_attrvalue225, "attribute");
        {
            boolean var_shoulddisplayattr228 = (((null != var_attrcontent226) && (!"".equals(var_attrcontent226))) && ((!"".equals(var_attrvalue225)) && (!((Object)false).equals(var_attrvalue225))));
            if (var_shoulddisplayattr228) {
                out.write(" alt");
                {
                    boolean var_istrueattr227 = (var_attrvalue225.equals(true));
                    if (!var_istrueattr227) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent226));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue229 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconWebImagePath");
    {
        Object var_attrcontent230 = renderContext.call("xss", var_attrvalue229, "uri");
        {
            boolean var_shoulddisplayattr232 = (((null != var_attrcontent230) && (!"".equals(var_attrcontent230))) && ((!"".equals(var_attrvalue229)) && (!((Object)false).equals(var_attrvalue229))));
            if (var_shoulddisplayattr232) {
                out.write(" src");
                {
                    boolean var_istrueattr231 = (var_attrvalue229.equals(true));
                    if (!var_istrueattr231) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent230));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                                    </picture>\r\n\r\n                                </div>\r\n                            </a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <div class=\"quick-access__table-desktop-mobile\">\r\n            <div class=\"table-mobile__header\">\r\n                <div class=\"table-mobile__header-items\">\r\n                    <div class=\"header-item\">\r\n                        <span>");
{
    Object var_233 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_233));
}
out.write("</span>\r\n                        <div class=\"header-item__img\">\r\n                            <img");
{
    Object var_attrvalue234 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIconAlt");
    {
        Object var_attrcontent235 = renderContext.call("xss", var_attrvalue234, "attribute");
        {
            boolean var_shoulddisplayattr237 = (((null != var_attrcontent235) && (!"".equals(var_attrcontent235))) && ((!"".equals(var_attrvalue234)) && (!((Object)false).equals(var_attrvalue234))));
            if (var_shoulddisplayattr237) {
                out.write(" alt");
                {
                    boolean var_istrueattr236 = (var_attrvalue234.equals(true));
                    if (!var_istrueattr236) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent235));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue238 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabOneIcon");
    {
        Object var_attrcontent239 = renderContext.call("xss", var_attrvalue238, "uri");
        {
            boolean var_shoulddisplayattr241 = (((null != var_attrcontent239) && (!"".equals(var_attrcontent239))) && ((!"".equals(var_attrvalue238)) && (!((Object)false).equals(var_attrvalue238))));
            if (var_shoulddisplayattr241) {
                out.write(" src");
                {
                    boolean var_istrueattr240 = (var_attrvalue238.equals(true));
                    if (!var_istrueattr240) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent239));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"header-item\">\r\n                        <span>");
{
    Object var_242 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_242));
}
out.write("</span>\r\n                        <div class=\"header-item__img\">\r\n                            <img");
{
    Object var_attrvalue243 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIconAlt");
    {
        Object var_attrcontent244 = renderContext.call("xss", var_attrvalue243, "attribute");
        {
            boolean var_shoulddisplayattr246 = (((null != var_attrcontent244) && (!"".equals(var_attrcontent244))) && ((!"".equals(var_attrvalue243)) && (!((Object)false).equals(var_attrvalue243))));
            if (var_shoulddisplayattr246) {
                out.write(" alt");
                {
                    boolean var_istrueattr245 = (var_attrvalue243.equals(true));
                    if (!var_istrueattr245) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent244));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue247 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabTwoIcon");
    {
        Object var_attrcontent248 = renderContext.call("xss", var_attrvalue247, "uri");
        {
            boolean var_shoulddisplayattr250 = (((null != var_attrcontent248) && (!"".equals(var_attrcontent248))) && ((!"".equals(var_attrvalue247)) && (!((Object)false).equals(var_attrvalue247))));
            if (var_shoulddisplayattr250) {
                out.write(" src");
                {
                    boolean var_istrueattr249 = (var_attrvalue247.equals(true));
                    if (!var_istrueattr249) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent248));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"header-item\">\r\n                        <span>");
{
    Object var_251 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeHeading"), "text");
    out.write(renderContext.getObjectModel().toString(var_251));
}
out.write("</span>\r\n                        <div class=\"header-item__img\">\r\n                            <img");
{
    Object var_attrvalue252 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIconAlt");
    {
        Object var_attrcontent253 = renderContext.call("xss", var_attrvalue252, "attribute");
        {
            boolean var_shoulddisplayattr255 = (((null != var_attrcontent253) && (!"".equals(var_attrcontent253))) && ((!"".equals(var_attrvalue252)) && (!((Object)false).equals(var_attrvalue252))));
            if (var_shoulddisplayattr255) {
                out.write(" alt");
                {
                    boolean var_istrueattr254 = (var_attrvalue252.equals(true));
                    if (!var_istrueattr254) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent253));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(" data-nimg=\"fixed\" decoding=\"async\"");
{
    Object var_attrvalue256 = renderContext.getObjectModel().resolveProperty(_global_quickaccessmodel, "tabThreeIcon");
    {
        Object var_attrcontent257 = renderContext.call("xss", var_attrvalue256, "uri");
        {
            boolean var_shoulddisplayattr259 = (((null != var_attrcontent257) && (!"".equals(var_attrcontent257))) && ((!"".equals(var_attrvalue256)) && (!((Object)false).equals(var_attrvalue256))));
            if (var_shoulddisplayattr259) {
                out.write(" src");
                {
                    boolean var_istrueattr258 = (var_attrvalue256.equals(true));
                    if (!var_istrueattr258) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent257));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write("/>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"pop-up\">\r\n            <div class=\"pop-up-content\">\r\n                <div class=\"title\">\r\n                    <h6></h6>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

