/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.financialfilters;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class financialfilters__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Collection var_collectionvar15_list_coerced$ = null;
Collection var_collectionvar31_list_coerced$ = null;
Collection var_collectionvar48_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.FinancialFiltersModel.class.getName(), obj());
out.write("\r\n<section class=\"container statistics-table-component section__margin-medium\"");
{
    Object var_attrvalue5 = renderContext.getObjectModel().resolveProperty(_global_model, "selectLabel");
    {
        Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
        {
            boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
            if (var_shoulddisplayattr8) {
                out.write(" data-select-label");
                {
                    boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                    if (!var_istrueattr7) {
                        out.write("=\"");
                        out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                        out.write("\"");
                    }
                }
            }
        }
    }
}
out.write(">\r\n    <div class=\"statistics-table-container\">\r\n        <!-- filter reuse -->\r\n        <div class=\"select-checkbox-filter\">\r\n            <h2>");
{
    Object var_9 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "heading"), "html");
    out.write(renderContext.getObjectModel().toString(var_9));
}
out.write("</h2>\r\n            <div class=\"select-options\">\r\n                <input type=\"hidden\" class=\"dropdownAnalytics\" data-tracking-click-event=\"articleFilter\"");
{
    String var_attrcontent10 = (("{'articleFilter': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "selectLabel"), "attribute"))) + "'}");
    out.write(" data-tracking-click-info-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent10));
    out.write("\"");
}
{
    String var_attrcontent11 = (("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "heading"), "html"))) + "', 'type': 'Other'}}");
    out.write(" data-tracking-web-interaction-value=\"");
    out.write(renderContext.getObjectModel().toString(var_attrcontent11));
    out.write("\"");
}
out.write("/>\r\n                <div class=\"select\">\r\n                    <img class=\"calendar-icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/calendar.svg\"/>\r\n                    <h6>");
{
    String var_12 = (renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "yearLabel"), "text")) + ":");
    out.write(renderContext.getObjectModel().toString(var_12));
}
out.write("</h6>\r\n                    <span>");
{
    Object var_13 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "selectLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_13));
}
out.write("</span>\r\n                    <img class=\"fa-chevron-down\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/chevron-bottom-icon.svg\"/>\r\n                </div>\r\n                <div class=\"checkbox\">\r\n                    <div class=\"checkbox-list\">\r\n                        <div class=\"type\">\r\n                            <div class=\"year\">\r\n                                <p>");
{
    Object var_14 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "yearLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_14));
}
out.write("</p>\r\n                                <ul>\r\n                                    ");
{
    Object var_collectionvar15 = renderContext.getObjectModel().resolveProperty(_global_model, "highlights");
    {
        long var_size16 = ((var_collectionvar15_list_coerced$ == null ? (var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15)) : var_collectionvar15_list_coerced$).size());
        {
            boolean var_notempty17 = (var_size16 > 0);
            if (var_notempty17) {
                {
                    long var_end20 = var_size16;
                    {
                        boolean var_validstartstepend21 = (((0 < var_size16) && true) && (var_end20 > 0));
                        if (var_validstartstepend21) {
                            if (var_collectionvar15_list_coerced$ == null) {
                                var_collectionvar15_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar15);
                            }
                            long var_index22 = 0;
                            for (Object item : var_collectionvar15_list_coerced$) {
                                {
                                    boolean var_traversal24 = (((var_index22 >= 0) && (var_index22 <= var_end20)) && true);
                                    if (var_traversal24) {
                                        out.write("<li>\r\n                                        <label>\r\n                                            <input type=\"checkbox\"");
                                        {
                                            Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(item, "year");
                                            {
                                                Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                    if (var_shoulddisplayattr28) {
                                                        out.write(" value");
                                                        {
                                                            boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                            if (!var_istrueattr27) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                            <div>");
                                        {
                                            Object var_29 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "year"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_29));
                                        }
                                        out.write("</div>\r\n                                        </label>\r\n                                    </li>\n");
                                    }
                                }
                                var_index22++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar15_list_coerced$ = null;
}
out.write("\r\n                                </ul>\r\n                            </div>\r\n                            <div class=\"quarter\">\r\n                                <p>");
{
    Object var_30 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "quarterLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_30));
}
out.write("</p>\r\n                                <ul>\r\n                                    ");
{
    Object var_collectionvar31 = renderContext.getObjectModel().resolveProperty(_global_model, "quarters");
    {
        long var_size32 = ((var_collectionvar31_list_coerced$ == null ? (var_collectionvar31_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar31)) : var_collectionvar31_list_coerced$).size());
        {
            boolean var_notempty33 = (var_size32 > 0);
            if (var_notempty33) {
                {
                    long var_end36 = var_size32;
                    {
                        boolean var_validstartstepend37 = (((0 < var_size32) && true) && (var_end36 > 0));
                        if (var_validstartstepend37) {
                            if (var_collectionvar31_list_coerced$ == null) {
                                var_collectionvar31_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar31);
                            }
                            long var_index38 = 0;
                            for (Object item : var_collectionvar31_list_coerced$) {
                                {
                                    boolean var_traversal40 = (((var_index38 >= 0) && (var_index38 <= var_end36)) && true);
                                    if (var_traversal40) {
                                        out.write("<li>\r\n                                        <label>\r\n                                            <input type=\"checkbox\"");
                                        {
                                            Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(item, "name");
                                            {
                                                Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                                                    if (var_shoulddisplayattr44) {
                                                        out.write(" value");
                                                        {
                                                            boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                                            if (!var_istrueattr43) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write("/>\r\n                                            <div>");
                                        {
                                            Object var_45 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "jcr:title"), "text");
                                            out.write(renderContext.getObjectModel().toString(var_45));
                                        }
                                        out.write("</div>\r\n                                        </label>\r\n                                    </li>\n");
                                    }
                                }
                                var_index38++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar31_list_coerced$ = null;
}
out.write("\r\n                                </ul>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"popup\"></div>\r\n            <div class=\"popup-content\">\r\n                <div class=\"title\">");
{
    Object var_46 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "filtersLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_46));
}
out.write("</div>\r\n                <div class=\"content\"></div>\r\n                <div class=\"btn\">\r\n                    <button>");
{
    Object var_47 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "applyFiltersLabel"), "text");
    out.write(renderContext.getObjectModel().toString(var_47));
}
out.write("</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"list-items\">\r\n            ");
{
    Object var_collectionvar48 = renderContext.getObjectModel().resolveProperty(_global_model, "highlights");
    {
        long var_size49 = ((var_collectionvar48_list_coerced$ == null ? (var_collectionvar48_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar48)) : var_collectionvar48_list_coerced$).size());
        {
            boolean var_notempty50 = (var_size49 > 0);
            if (var_notempty50) {
                {
                    long var_end53 = var_size49;
                    {
                        boolean var_validstartstepend54 = (((0 < var_size49) && true) && (var_end53 > 0));
                        if (var_validstartstepend54) {
                            if (var_collectionvar48_list_coerced$ == null) {
                                var_collectionvar48_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar48);
                            }
                            long var_index55 = 0;
                            for (Object item : var_collectionvar48_list_coerced$) {
                                {
                                    boolean var_traversal57 = (((var_index55 >= 0) && (var_index55 <= var_end53)) && true);
                                    if (var_traversal57) {
                                        out.write("<div class=\"year-item\"");
                                        {
                                            Object var_attrvalue58 = renderContext.getObjectModel().resolveProperty(item, "year");
                                            {
                                                Object var_attrcontent59 = renderContext.call("xss", var_attrvalue58, "attribute");
                                                {
                                                    boolean var_shoulddisplayattr61 = (((null != var_attrcontent59) && (!"".equals(var_attrcontent59))) && ((!"".equals(var_attrvalue58)) && (!((Object)false).equals(var_attrvalue58))));
                                                    if (var_shoulddisplayattr61) {
                                                        out.write(" data-year");
                                                        {
                                                            boolean var_istrueattr60 = (var_attrvalue58.equals(true));
                                                            if (!var_istrueattr60) {
                                                                out.write("=\"");
                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent59));
                                                                out.write("\"");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        out.write(">\r\n                ");
                                        {
                                            Object var_resourcecontent62 = renderContext.call("includeResource", null, obj().with("path", renderContext.getObjectModel().resolveProperty(item, "path")).with("selectors", "content").with("wcmmode", "disabled"));
                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent62));
                                        }
                                        out.write("\r\n            </div>\n");
                                    }
                                }
                                var_index55++;
                            }
                        }
                    }
                }
            }
        }
    }
    var_collectionvar48_list_coerced$ = null;
}
out.write("\r\n        </div>\r\n    </div>\r\n</section>\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

