/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.imageCaurosel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class imageCaurosel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _dynamic_wcmmode = bindings.get("wcmmode");
Object _dynamic_component = bindings.get("component");
Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar14_list_coerced$ = null;
{
    Object var_testvariable0 = renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit");
    if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
        out.write("\r\n    <p");
        {
            Object var_attrvalue1 = renderContext.getObjectModel().resolveProperty(_dynamic_component, "title");
            {
                Object var_attrcontent2 = renderContext.call("xss", var_attrvalue1, "attribute");
                {
                    boolean var_shoulddisplayattr4 = (((null != var_attrcontent2) && (!"".equals(var_attrcontent2))) && ((!"".equals(var_attrvalue1)) && (!((Object)false).equals(var_attrvalue1))));
                    if (var_shoulddisplayattr4) {
                        out.write(" data-emptytext");
                        {
                            boolean var_istrueattr3 = (var_attrvalue1.equals(true));
                            if (!var_istrueattr3) {
                                out.write("=\"");
                                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                                out.write("\"");
                            }
                        }
                    }
                }
            }
        }
        out.write(" class=\"cq-placeholder\"></p>\r\n");
    }
}
out.write("\r\n");
_global_model = renderContext.call("use", com.techcombank.core.models.CarouselImage.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "imageItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div");
    {
        Object var_attrvalue5 = ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Carousel Image" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")));
        {
            Object var_attrcontent6 = renderContext.call("xss", var_attrvalue5, "attribute");
            {
                boolean var_shoulddisplayattr8 = (((null != var_attrcontent6) && (!"".equals(var_attrcontent6))) && ((!"".equals(var_attrvalue5)) && (!((Object)false).equals(var_attrvalue5))));
                if (var_shoulddisplayattr8) {
                    out.write(" data-panelcontainer");
                    {
                        boolean var_istrueattr7 = (var_attrvalue5.equals(true));
                        if (!var_istrueattr7) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent6));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    {
        Object var_attrvalue9 = renderContext.call("i18n", ((renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit")) ? "Please drag CarouselImage Item Component" : renderContext.getObjectModel().resolveProperty(_dynamic_wcmmode, "edit"))), obj().with("i18n", null));
        {
            Object var_attrcontent10 = renderContext.call("xss", var_attrvalue9, "attribute");
            {
                boolean var_shoulddisplayattr12 = (((null != var_attrcontent10) && (!"".equals(var_attrcontent10))) && ((!"".equals(var_attrvalue9)) && (!((Object)false).equals(var_attrvalue9))));
                if (var_shoulddisplayattr12) {
                    out.write(" data-placeholder-text");
                    {
                        boolean var_istrueattr11 = (var_attrvalue9.equals(true));
                        if (!var_istrueattr11) {
                            out.write("=\"");
                            out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                            out.write("\"");
                        }
                    }
                }
            }
        }
    }
    out.write(">\r\n    ");
    {
        Object var_testvariable13 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable13)) {
            out.write("\r\n        <section class=\"container carousel-images section__padding-medium\">\r\n            <div class=\"carousel-images__carousel-img\">\r\n                <div class=\"carousel-images__carousel-img-container\">\r\n                    ");
            {
                Object var_collectionvar14 = renderContext.getObjectModel().resolveProperty(_global_model, "imageItems");
                {
                    long var_size15 = ((var_collectionvar14_list_coerced$ == null ? (var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14)) : var_collectionvar14_list_coerced$).size());
                    {
                        boolean var_notempty16 = (var_size15 > 0);
                        if (var_notempty16) {
                            {
                                long var_end19 = var_size15;
                                {
                                    boolean var_validstartstepend20 = (((0 < var_size15) && true) && (var_end19 > 0));
                                    if (var_validstartstepend20) {
                                        if (var_collectionvar14_list_coerced$ == null) {
                                            var_collectionvar14_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar14);
                                        }
                                        long var_index21 = 0;
                                        for (Object item : var_collectionvar14_list_coerced$) {
                                            {
                                                boolean var_traversal23 = (((var_index21 >= 0) && (var_index21 <= var_end19)) && true);
                                                if (var_traversal23) {
                                                    out.write("<div class=\"carousel-images__carousel-item\">\r\n\r\n                        <picture>\r\n                            <source media=\"(max-width: 360px)\"");
                                                    {
                                                        Object var_attrvalue24 = renderContext.getObjectModel().resolveProperty(item, "imageMobile");
                                                        {
                                                            Object var_attrcontent25 = renderContext.call("xss", var_attrvalue24, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr27 = (((null != var_attrcontent25) && (!"".equals(var_attrcontent25))) && ((!"".equals(var_attrvalue24)) && (!((Object)false).equals(var_attrvalue24))));
                                                                if (var_shoulddisplayattr27) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr26 = (var_attrvalue24.equals(true));
                                                                        if (!var_istrueattr26) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
                                                    {
                                                        Object var_attrvalue28 = renderContext.getObjectModel().resolveProperty(item, "imageMobile");
                                                        {
                                                            Object var_attrcontent29 = renderContext.call("xss", var_attrvalue28, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr31 = (((null != var_attrcontent29) && (!"".equals(var_attrcontent29))) && ((!"".equals(var_attrvalue28)) && (!((Object)false).equals(var_attrvalue28))));
                                                                if (var_shoulddisplayattr31) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr30 = (var_attrvalue28.equals(true));
                                                                        if (!var_istrueattr30) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent29));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
                                                    {
                                                        Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(item, "image");
                                                        {
                                                            Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                                                if (var_shoulddisplayattr35) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                                                        if (!var_istrueattr34) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
                                                    {
                                                        Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(item, "image");
                                                        {
                                                            Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                                                if (var_shoulddisplayattr39) {
                                                                    out.write(" srcset");
                                                                    {
                                                                        boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                                                        if (!var_istrueattr38) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                            <img");
                                                    {
                                                        Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(item, "imgAltText");
                                                        {
                                                            Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                                                if (var_shoulddisplayattr43) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                                                        if (!var_istrueattr42) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-nimg=\"fixed\" decoding=\"async\"");
                                                    {
                                                        Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(item, "image");
                                                        {
                                                            Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                                                if (var_shoulddisplayattr47) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                                                        if (!var_istrueattr46) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                        </picture>\r\n                    </div>\n");
                                                }
                                            }
                                            var_index21++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar14_list_coerced$ = null;
            }
            out.write("\r\n                </div>\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

