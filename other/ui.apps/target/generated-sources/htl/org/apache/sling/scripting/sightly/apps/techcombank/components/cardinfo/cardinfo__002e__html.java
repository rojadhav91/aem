/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.cardinfo;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class cardinfo__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Object _dynamic_item = bindings.get("item");
Collection var_collectionvar4_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.CardInfoModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "cardInfoItemList");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section");
            {
                String var_attrcontent1 = (("list-card-info " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "theme"), "attribute"))) + "-theme");
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n            <div");
            {
                String var_attrcontent2 = ("list-card-info__container " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreButton")) ? "view-more" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent2));
                out.write("\"");
            }
            out.write(">\r\n                <div");
            {
                String var_attrcontent3 = ((("list-card-info__list-item " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "displayVariation"), "attribute"))) + " ") + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreButton")) ? "view-more" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                out.write("\"");
            }
            out.write(">\r\n                ");
            {
                boolean var_testvariable11 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_dynamic_item, "cardTitle")));
                if (var_testvariable11) {
                    {
                        Object var_collectionvar4 = renderContext.getObjectModel().resolveProperty(_global_model, "cardInfoItemList");
                        {
                            long var_size5 = ((var_collectionvar4_list_coerced$ == null ? (var_collectionvar4_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar4)) : var_collectionvar4_list_coerced$).size());
                            {
                                boolean var_notempty6 = (var_size5 > 0);
                                if (var_notempty6) {
                                    {
                                        long var_end9 = var_size5;
                                        {
                                            boolean var_validstartstepend10 = (((0 < var_size5) && true) && (var_end9 > 0));
                                            if (var_validstartstepend10) {
                                                if (var_collectionvar4_list_coerced$ == null) {
                                                    var_collectionvar4_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar4);
                                                }
                                                long var_index12 = 0;
                                                for (Object item : var_collectionvar4_list_coerced$) {
                                                    {
                                                        long itemlist_field$_index = var_index12;
                                                        {
                                                            boolean var_traversal14 = (((var_index12 >= 0) && (var_index12 <= var_end9)) && true);
                                                            if (var_traversal14) {
                                                                out.write("<div");
                                                                {
                                                                    String var_attrcontent15 = ((("list-card-info__item img-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "imagePosition"), "attribute"))) + " effects-") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "cardInfoEffects"), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent15));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">\r\n                        ");
                                                                {
                                                                    Object var_testvariable16 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable16)) {
                                                                        out.write("<span class=\"list-card-info__item--box\">\r\n                            <picture>\r\n                                <source media=\"(max-width: 360px)\"");
                                                                        {
                                                                            Object var_attrvalue17 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                            {
                                                                                Object var_attrcontent18 = renderContext.call("xss", var_attrvalue17, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr20 = (((null != var_attrcontent18) && (!"".equals(var_attrcontent18))) && ((!"".equals(var_attrvalue17)) && (!((Object)false).equals(var_attrvalue17))));
                                                                                    if (var_shoulddisplayattr20) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr19 = (var_attrvalue17.equals(true));
                                                                                            if (!var_istrueattr19) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent18));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                <source media=\"(max-width: 576px)\"");
                                                                        {
                                                                            Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(item, "mobileImagePath");
                                                                            {
                                                                                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                                    if (var_shoulddisplayattr24) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                                            if (!var_istrueattr23) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                <source media=\"(min-width: 1080px)\"");
                                                                        {
                                                                            Object var_attrvalue25 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                            {
                                                                                Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                                    if (var_shoulddisplayattr28) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                                            if (!var_istrueattr27) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                <source media=\"(min-width: 1200px)\"");
                                                                        {
                                                                            Object var_attrvalue29 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                            {
                                                                                Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                                    if (var_shoulddisplayattr32) {
                                                                                        out.write(" srcset");
                                                                                        {
                                                                                            boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                                            if (!var_istrueattr31) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                                <img class=\"list-card-info__item-img\"");
                                                                        {
                                                                            Object var_attrvalue33 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                                            {
                                                                                Object var_attrcontent34 = renderContext.call("xss", var_attrvalue33, "attribute");
                                                                                {
                                                                                    boolean var_shoulddisplayattr36 = (((null != var_attrcontent34) && (!"".equals(var_attrcontent34))) && ((!"".equals(var_attrvalue33)) && (!((Object)false).equals(var_attrvalue33))));
                                                                                    if (var_shoulddisplayattr36) {
                                                                                        out.write(" alt");
                                                                                        {
                                                                                            boolean var_istrueattr35 = (var_attrvalue33.equals(true));
                                                                                            if (!var_istrueattr35) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        {
                                                                            Object var_attrvalue37 = renderContext.getObjectModel().resolveProperty(item, "webImagePath");
                                                                            {
                                                                                Object var_attrcontent38 = renderContext.call("xss", var_attrvalue37, "uri");
                                                                                {
                                                                                    boolean var_shoulddisplayattr40 = (((null != var_attrcontent38) && (!"".equals(var_attrcontent38))) && ((!"".equals(var_attrvalue37)) && (!((Object)false).equals(var_attrvalue37))));
                                                                                    if (var_shoulddisplayattr40) {
                                                                                        out.write(" src");
                                                                                        {
                                                                                            boolean var_istrueattr39 = (var_attrvalue37.equals(true));
                                                                                            if (!var_istrueattr39) {
                                                                                                out.write("=\"");
                                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                                                                out.write("\"");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        out.write("/>\r\n                            </picture>\r\n                        </span>");
                                                                    }
                                                                }
                                                                out.write("\r\n                        <div class=\"list-card-info__item-wrapper\">\r\n                            <div class=\"list-card-info__item-content\">\r\n                                <div class=\"list-card-info__item-info-description\">\r\n                                    <div class=\"list-card-info__item-info-title\">");
                                                                {
                                                                    Object var_41 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "html");
                                                                    out.write(renderContext.getObjectModel().toString(var_41));
                                                                }
                                                                out.write("</div>\r\n                                    ");
                                                                {
                                                                    Object var_testvariable42 = renderContext.getObjectModel().resolveProperty(item, "cardDescription");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable42)) {
                                                                        out.write("<div class=\"list-card-info__item-info-card-description\">");
                                                                        {
                                                                            Object var_43 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "html");
                                                                            out.write(renderContext.getObjectModel().toString(var_43));
                                                                        }
                                                                        out.write("</div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                </div>\r\n                                ");
                                                                {
                                                                    Object var_testvariable44 = renderContext.getObjectModel().resolveProperty(item, "cardDescription2");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable44)) {
                                                                        out.write("<div class=\"list-card-info__item-info-footer\">");
                                                                        {
                                                                            String var_45 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription2"), "html"))) + "\r\n                                ");
                                                                            out.write(renderContext.getObjectModel().toString(var_45));
                                                                        }
                                                                        out.write("</div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                ");
                                                                {
                                                                    Object var_testvariable46 = renderContext.getObjectModel().resolveProperty(item, "ctaNeeded");
                                                                    if (renderContext.getObjectModel().toBoolean(var_testvariable46)) {
                                                                        out.write("<div class=\"list-card-info__item-action\">\r\n                                    ");
                                                                        {
                                                                            Object var_resourcecontent47 = renderContext.call("includeResource", itemlist_field$_index, obj().with("resourceType", "techcombank/components/button"));
                                                                            out.write(renderContext.getObjectModel().toString(var_resourcecontent47));
                                                                        }
                                                                        out.write("\r\n                                </div>");
                                                                    }
                                                                }
                                                                out.write("\r\n                            </div>\r\n                        </div>\r\n                    </div>\n");
                                                            }
                                                        }
                                                    }
                                                    var_index12++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        var_collectionvar4_list_coerced$ = null;
                    }
                }
            }
            out.write("\r\n                </div>\r\n            </div>\r\n            ");
            {
                Object var_testvariable48 = renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreButton");
                if (renderContext.getObjectModel().toBoolean(var_testvariable48)) {
                    out.write("<div class=\"list-card-info__view-more-btn\"");
                    {
                        Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabel");
                        {
                            Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                            {
                                boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                                if (var_shoulddisplayattr52) {
                                    out.write(" viewMoreLabel");
                                    {
                                        boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                        if (!var_istrueattr51) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(_global_model, "viewLessLabel");
                        {
                            Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
                            {
                                boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                                if (var_shoulddisplayattr56) {
                                    out.write(" viewLessLabel");
                                    {
                                        boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                                        if (!var_istrueattr55) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write(">\r\n                <a class=\"cta-button view-more\">\r\n                    <span class=\"cmp-button__text\">");
                    {
                        Object var_57 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabel"), "text");
                        out.write(renderContext.getObjectModel().toString(var_57));
                    }
                    out.write("</span>\r\n                    <img class=\"cmp-button__icon\" src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg\"/>\r\n                </a>\r\n            </div>");
                }
            }
            out.write("\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar58 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions59_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions59 = obj().with("isEmpty", var_templateoptions59_field$_isempty);
            callUnit(out, renderContext, var_templatevar58, var_templateoptions59);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

