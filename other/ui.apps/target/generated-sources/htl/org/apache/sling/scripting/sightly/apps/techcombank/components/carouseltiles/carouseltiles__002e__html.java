/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.carouseltiles;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class carouseltiles__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar10_list_coerced$ = null;
_global_model = renderContext.call("use", com.techcombank.core.models.CarouselTilesModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_model, "modelItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n  ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n    <section");
            {
                String var_attrcontent1 = ("card-slider-container " + renderContext.getObjectModel().toString(renderContext.call("xss", (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(_global_model, "inMobileViewIsItASlider")) ? "mobile-slide" : ""), "attribute")));
                out.write(" class=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            {
                Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_model, "numberOfTiles");
                {
                    Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                    {
                        boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                        if (var_shoulddisplayattr5) {
                            out.write(" num-alias");
                            {
                                boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                if (!var_istrueattr4) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_model, "viewMoreLabel");
                {
                    Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                    {
                        boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                        if (var_shoulddisplayattr9) {
                            out.write(" data-viewmore");
                            {
                                boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                                if (!var_istrueattr8) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write(">\r\n      <div class=\"card-slider-wrapper\">\r\n        <div class=\"cardslider-carousel-slickwrapper small-article\">\r\n          <div class=\"cardslider-carousel-slickcontainer mobile-unslide-container\">\r\n            <div class=\"cardslider-carousel-slicklist mobile-unslide-items\">\r\n              ");
            {
                Object var_collectionvar10 = renderContext.getObjectModel().resolveProperty(_global_model, "modelItems");
                {
                    long var_size11 = ((var_collectionvar10_list_coerced$ == null ? (var_collectionvar10_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar10)) : var_collectionvar10_list_coerced$).size());
                    {
                        boolean var_notempty12 = (var_size11 > 0);
                        if (var_notempty12) {
                            {
                                long var_end15 = var_size11;
                                {
                                    boolean var_validstartstepend16 = (((0 < var_size11) && true) && (var_end15 > 0));
                                    if (var_validstartstepend16) {
                                        if (var_collectionvar10_list_coerced$ == null) {
                                            var_collectionvar10_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar10);
                                        }
                                        long var_index17 = 0;
                                        for (Object item : var_collectionvar10_list_coerced$) {
                                            {
                                                long itemlist_field$_index = var_index17;
                                                {
                                                    boolean var_traversal19 = (((var_index17 >= 0) && (var_index17 <= var_end15)) && true);
                                                    if (var_traversal19) {
                                                        out.write("\r\n                <div");
                                                        {
                                                            String var_attrcontent20 = ("cardslider-carousel-slickitem " + renderContext.getObjectModel().toString(renderContext.call("xss", ((itemlist_field$_index >= 4) ? "slickitem-hidden" : ""), "attribute")));
                                                            out.write(" class=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            long var_attrvalue21 = itemlist_field$_index;
                                                            {
                                                                Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                    if (var_shoulddisplayattr24) {
                                                                        out.write(" card-index");
                                                                        {
                                                                            boolean var_istrueattr23 = (((Object)var_attrvalue21).equals(true));
                                                                            if (!var_istrueattr23) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(">\r\n                  <div");
                                                        {
                                                            String var_attrcontent25 = ("carousel-card-wrapper card-image-type-" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardImageType"), "attribute")));
                                                            out.write(" class=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent25));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n                    <a class=\"carousel-card-link\"");
                                                        {
                                                            Object var_attrvalue26 = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                            {
                                                                Object var_attrcontent27 = renderContext.call("xss", var_attrvalue26, "uri");
                                                                {
                                                                    boolean var_shoulddisplayattr29 = (((null != var_attrcontent27) && (!"".equals(var_attrcontent27))) && ((!"".equals(var_attrvalue26)) && (!((Object)false).equals(var_attrvalue26))));
                                                                    if (var_shoulddisplayattr29) {
                                                                        out.write(" href");
                                                                        {
                                                                            boolean var_istrueattr28 = (var_attrvalue26.equals(true));
                                                                            if (!var_istrueattr28) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent27));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue30 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "_self");
                                                            {
                                                                Object var_attrcontent31 = renderContext.call("xss", var_attrvalue30, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr33 = (((null != var_attrcontent31) && (!"".equals(var_attrcontent31))) && ((!"".equals(var_attrvalue30)) && (!((Object)false).equals(var_attrvalue30))));
                                                                    if (var_shoulddisplayattr33) {
                                                                        out.write(" target");
                                                                        {
                                                                            boolean var_istrueattr32 = (var_attrvalue30.equals(true));
                                                                            if (!var_istrueattr32) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent31));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        {
                                                            String var_attrvalue34 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                            {
                                                                Object var_attrcontent35 = renderContext.call("xss", var_attrvalue34, "attribute");
                                                                {
                                                                    boolean var_shoulddisplayattr37 = (((null != var_attrcontent35) && (!"".equals(var_attrcontent35))) && ((!"".equals(var_attrvalue34)) && (!((Object)false).equals(var_attrvalue34))));
                                                                    if (var_shoulddisplayattr37) {
                                                                        out.write(" rel");
                                                                        {
                                                                            boolean var_istrueattr36 = (var_attrvalue34.equals(true));
                                                                            if (!var_istrueattr36) {
                                                                                out.write("=\"");
                                                                                out.write(renderContext.getObjectModel().toString(var_attrcontent35));
                                                                                out.write("\"");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        out.write(" data-tracking-click-event=\"linkClick\"");
                                                        {
                                                            String var_attrcontent38 = (("{'linkClick' : '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "attribute"))) + "'}");
                                                            out.write(" data-tracking-click-info-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent38));
                                                            out.write("\"");
                                                        }
                                                        {
                                                            String var_attrcontent39 = (("{'webInteractions': {'name': 'Carousel Tiles', 'type': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                            out.write(" data-tracking-web-interaction-value=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n                      <article class=\"carousel-card-article\">\r\n                        ");
                                                        {
                                                            boolean var_testvariable40 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "cardImageType"), "background"));
                                                            if (var_testvariable40) {
                                                                out.write("<span class=\"carousel-card-picture\">\r\n                          <picture>\r\n                            <source media=\"(max-width: 360px)\"");
                                                                {
                                                                    Object var_attrvalue41 = renderContext.getObjectModel().resolveProperty(item, "mobileCardImagePath");
                                                                    {
                                                                        Object var_attrcontent42 = renderContext.call("xss", var_attrvalue41, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr44 = (((null != var_attrcontent42) && (!"".equals(var_attrcontent42))) && ((!"".equals(var_attrvalue41)) && (!((Object)false).equals(var_attrvalue41))));
                                                                            if (var_shoulddisplayattr44) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr43 = (var_attrvalue41.equals(true));
                                                                                    if (!var_istrueattr43) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent42));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                            <source media=\"(max-width: 576px)\"");
                                                                {
                                                                    Object var_attrvalue45 = renderContext.getObjectModel().resolveProperty(item, "mobileCardImagePath");
                                                                    {
                                                                        Object var_attrcontent46 = renderContext.call("xss", var_attrvalue45, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr48 = (((null != var_attrcontent46) && (!"".equals(var_attrcontent46))) && ((!"".equals(var_attrvalue45)) && (!((Object)false).equals(var_attrvalue45))));
                                                                            if (var_shoulddisplayattr48) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr47 = (var_attrvalue45.equals(true));
                                                                                    if (!var_istrueattr47) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent46));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                            <source media=\"(min-width: 1080px)\"");
                                                                {
                                                                    Object var_attrvalue49 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent50 = renderContext.call("xss", var_attrvalue49, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr52 = (((null != var_attrcontent50) && (!"".equals(var_attrcontent50))) && ((!"".equals(var_attrvalue49)) && (!((Object)false).equals(var_attrvalue49))));
                                                                            if (var_shoulddisplayattr52) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr51 = (var_attrvalue49.equals(true));
                                                                                    if (!var_istrueattr51) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent50));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                            <source media=\"(min-width: 1200px)\"");
                                                                {
                                                                    Object var_attrvalue53 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent54 = renderContext.call("xss", var_attrvalue53, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr56 = (((null != var_attrcontent54) && (!"".equals(var_attrcontent54))) && ((!"".equals(var_attrvalue53)) && (!((Object)false).equals(var_attrvalue53))));
                                                                            if (var_shoulddisplayattr56) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr55 = (var_attrvalue53.equals(true));
                                                                                    if (!var_istrueattr55) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent54));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                            <img");
                                                                {
                                                                    Object var_attrvalue57 = renderContext.getObjectModel().resolveProperty(item, "cardImageAltText");
                                                                    {
                                                                        Object var_attrcontent58 = renderContext.call("xss", var_attrvalue57, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr60 = (((null != var_attrcontent58) && (!"".equals(var_attrcontent58))) && ((!"".equals(var_attrvalue57)) && (!((Object)false).equals(var_attrvalue57))));
                                                                            if (var_shoulddisplayattr60) {
                                                                                out.write(" alt");
                                                                                {
                                                                                    boolean var_istrueattr59 = (var_attrvalue57.equals(true));
                                                                                    if (!var_istrueattr59) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent58));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" class=\"cardslider-head__image\"");
                                                                {
                                                                    Object var_attrvalue61 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent62 = renderContext.call("xss", var_attrvalue61, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr64 = (((null != var_attrcontent62) && (!"".equals(var_attrcontent62))) && ((!"".equals(var_attrvalue61)) && (!((Object)false).equals(var_attrvalue61))));
                                                                            if (var_shoulddisplayattr64) {
                                                                                out.write(" src");
                                                                                {
                                                                                    boolean var_istrueattr63 = (var_attrvalue61.equals(true));
                                                                                    if (!var_istrueattr63) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent62));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                          </picture>\r\n                        </span>");
                                                            }
                                                        }
                                                        out.write("\r\n                        <div class=\"cardslider-item-wrapper\">\r\n                          <div class=\"cardslider-item-content\">\r\n                            <div class=\"cardslider-item-body\">\r\n                              <div class=\"cardslider-item-inner\">\r\n                                <div");
                                                        {
                                                            String var_attrcontent65 = ("cardslider-item-navtext " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "dark")) ? "white-text" : "black-text"), "attribute")));
                                                            out.write(" class=\"");
                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                                            out.write("\"");
                                                        }
                                                        out.write(">\r\n\r\n                                  <!-- Foreground Image -->\r\n                                  ");
                                                        {
                                                            boolean var_testvariable66 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "cardImageType"), "foreground"));
                                                            if (var_testvariable66) {
                                                                out.write("<picture>\r\n                                    <source media=\"(max-width: 360px)\"");
                                                                {
                                                                    Object var_attrvalue67 = renderContext.getObjectModel().resolveProperty(item, "mobileCardImagePath");
                                                                    {
                                                                        Object var_attrcontent68 = renderContext.call("xss", var_attrvalue67, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr70 = (((null != var_attrcontent68) && (!"".equals(var_attrcontent68))) && ((!"".equals(var_attrvalue67)) && (!((Object)false).equals(var_attrvalue67))));
                                                                            if (var_shoulddisplayattr70) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr69 = (var_attrvalue67.equals(true));
                                                                                    if (!var_istrueattr69) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent68));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                    <source media=\"(max-width: 576px)\"");
                                                                {
                                                                    Object var_attrvalue71 = renderContext.getObjectModel().resolveProperty(item, "mobileCardImagePath");
                                                                    {
                                                                        Object var_attrcontent72 = renderContext.call("xss", var_attrvalue71, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr74 = (((null != var_attrcontent72) && (!"".equals(var_attrcontent72))) && ((!"".equals(var_attrvalue71)) && (!((Object)false).equals(var_attrvalue71))));
                                                                            if (var_shoulddisplayattr74) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr73 = (var_attrvalue71.equals(true));
                                                                                    if (!var_istrueattr73) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent72));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                    <source media=\"(min-width: 1080px)\"");
                                                                {
                                                                    Object var_attrvalue75 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent76 = renderContext.call("xss", var_attrvalue75, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr78 = (((null != var_attrcontent76) && (!"".equals(var_attrcontent76))) && ((!"".equals(var_attrvalue75)) && (!((Object)false).equals(var_attrvalue75))));
                                                                            if (var_shoulddisplayattr78) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr77 = (var_attrvalue75.equals(true));
                                                                                    if (!var_istrueattr77) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent76));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                    <source media=\"(min-width: 1200px)\"");
                                                                {
                                                                    Object var_attrvalue79 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent80 = renderContext.call("xss", var_attrvalue79, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr82 = (((null != var_attrcontent80) && (!"".equals(var_attrcontent80))) && ((!"".equals(var_attrvalue79)) && (!((Object)false).equals(var_attrvalue79))));
                                                                            if (var_shoulddisplayattr82) {
                                                                                out.write(" srcset");
                                                                                {
                                                                                    boolean var_istrueattr81 = (var_attrvalue79.equals(true));
                                                                                    if (!var_istrueattr81) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent80));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write("/>\r\n                                    <img");
                                                                {
                                                                    Object var_attrvalue83 = renderContext.getObjectModel().resolveProperty(item, "cardImageAltText");
                                                                    {
                                                                        Object var_attrcontent84 = renderContext.call("xss", var_attrvalue83, "attribute");
                                                                        {
                                                                            boolean var_shoulddisplayattr86 = (((null != var_attrcontent84) && (!"".equals(var_attrcontent84))) && ((!"".equals(var_attrvalue83)) && (!((Object)false).equals(var_attrvalue83))));
                                                                            if (var_shoulddisplayattr86) {
                                                                                out.write(" alt");
                                                                                {
                                                                                    boolean var_istrueattr85 = (var_attrvalue83.equals(true));
                                                                                    if (!var_istrueattr85) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent84));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                {
                                                                    Object var_attrvalue87 = renderContext.getObjectModel().resolveProperty(item, "webCardImagePath");
                                                                    {
                                                                        Object var_attrcontent88 = renderContext.call("xss", var_attrvalue87, "uri");
                                                                        {
                                                                            boolean var_shoulddisplayattr90 = (((null != var_attrcontent88) && (!"".equals(var_attrcontent88))) && ((!"".equals(var_attrvalue87)) && (!((Object)false).equals(var_attrvalue87))));
                                                                            if (var_shoulddisplayattr90) {
                                                                                out.write(" src");
                                                                                {
                                                                                    boolean var_istrueattr89 = (var_attrvalue87.equals(true));
                                                                                    if (!var_istrueattr89) {
                                                                                        out.write("=\"");
                                                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent88));
                                                                                        out.write("\"");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                out.write(" class=\"cardslider-head-foreground__image\"/>\r\n                                  </picture>");
                                                            }
                                                        }
                                                        out.write("\r\n\r\n                                  <!-- Nudge -->\r\n                                  ");
                                                        {
                                                            Object var_testvariable91 = renderContext.getObjectModel().resolveProperty(item, "nudgeText");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable91)) {
                                                                out.write("<p");
                                                                {
                                                                    String var_attrcontent92 = ("cardslider-item-nudge " + renderContext.getObjectModel().toString(renderContext.call("xss", ((((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "cardTitle"))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "cardDescription")))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText")))) ? "bottom-line" : ""), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent92));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">");
                                                                {
                                                                    Object var_93 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "nudgeText"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_93));
                                                                }
                                                                out.write("</p>");
                                                            }
                                                        }
                                                        out.write("\r\n\r\n                                  <!-- Title -->\r\n                                  ");
                                                        {
                                                            Object var_testvariable94 = renderContext.getObjectModel().resolveProperty(item, "cardTitle");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable94)) {
                                                                out.write("<h4");
                                                                {
                                                                    String var_attrcontent95 = ("cardslider-item-title " + renderContext.getObjectModel().toString(renderContext.call("xss", (((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "cardDescription"))) && (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText")))) ? "bottom-line" : ""), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent95));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">");
                                                                {
                                                                    Object var_96 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardTitle"), "text");
                                                                    out.write(renderContext.getObjectModel().toString(var_96));
                                                                }
                                                                out.write("</h4>");
                                                            }
                                                        }
                                                        out.write("\r\n\r\n                                  <!-- Description -->\r\n                                  ");
                                                        {
                                                            Object var_testvariable97 = renderContext.getObjectModel().resolveProperty(item, "cardDescription");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable97)) {
                                                                out.write("<div");
                                                                {
                                                                    String var_attrcontent98 = ("cardslider-item-description " + renderContext.getObjectModel().toString(renderContext.call("xss", ((!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText"))) ? "bottom-line" : ""), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">");
                                                                {
                                                                    String var_99 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "cardDescription"), "html"))) + "\r\n                                  ");
                                                                    out.write(renderContext.getObjectModel().toString(var_99));
                                                                }
                                                                out.write("</div>");
                                                            }
                                                        }
                                                        out.write("\r\n\r\n                                  <!-- Link Text & Arrow -->\r\n                                  ");
                                                        {
                                                            Object var_testvariable100 = renderContext.getObjectModel().resolveProperty(item, "linkText");
                                                            if (renderContext.getObjectModel().toBoolean(var_testvariable100)) {
                                                                out.write("<div class=\"cardslider-item-action\">\r\n                                    <div");
                                                                {
                                                                    String var_attrcontent101 = ("tcb-button tcb-button--link tcb-button--border-on-mobile center-align " + renderContext.getObjectModel().toString(renderContext.call("xss", ((org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "dark")) ? "white-text" : "black-text"), "attribute")));
                                                                    out.write(" class=\"");
                                                                    out.write(renderContext.getObjectModel().toString(var_attrcontent101));
                                                                    out.write("\"");
                                                                }
                                                                out.write(">");
                                                                {
                                                                    String var_102 = (("\r\n                                      " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "linkText"), "text"))) + "\r\n                                      ");
                                                                    out.write(renderContext.getObjectModel().toString(var_102));
                                                                }
                                                                {
                                                                    boolean var_testvariable103 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "dark"));
                                                                    if (var_testvariable103) {
                                                                        out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/white-arrow-icon.svg\" alt=\"icon\"/>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                      ");
                                                                {
                                                                    boolean var_testvariable104 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "light"));
                                                                    if (var_testvariable104) {
                                                                        out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\" alt=\"icon\"/>");
                                                                    }
                                                                }
                                                                out.write("\r\n                                    </div>\r\n                                  </div>");
                                                            }
                                                        }
                                                        out.write("\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                      </article>\r\n                    </a>\r\n                  </div>\r\n                  <!-- Fixed Arrow -->\r\n                  ");
                                                        {
                                                            boolean var_testvariable105 = (!renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "linkText")));
                                                            if (var_testvariable105) {
                                                                out.write("<div class=\"cardslider-item-arrow\">\r\n                    ");
                                                                {
                                                                    boolean var_testvariable106 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "dark"));
                                                                    if (var_testvariable106) {
                                                                        out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/white-arrow-icon.svg\" alt=\"icon\"/>");
                                                                    }
                                                                }
                                                                out.write("\r\n                    ");
                                                                {
                                                                    boolean var_testvariable107 = (org.apache.sling.scripting.sightly.compiler.expression.nodes.BinaryOperator.strictEq(renderContext.getObjectModel().resolveProperty(item, "theme"), "light"));
                                                                    if (var_testvariable107) {
                                                                        out.write("<img src=\"/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg\" alt=\"icon\"/>");
                                                                    }
                                                                }
                                                                out.write("\r\n                  </div>");
                                                            }
                                                        }
                                                        out.write("\r\n                </div>\r\n              ");
                                                    }
                                                }
                                            }
                                            var_index17++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar10_list_coerced$ = null;
            }
            out.write("\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </section>\r\n  ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar108 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions109_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions109 = obj().with("isEmpty", var_templateoptions109_field$_isempty);
            callUnit(out, renderContext, var_templatevar108, var_templateoptions109);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

