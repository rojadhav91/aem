/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.listrow;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class listrow__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_listrow = null;
Object _global_template = null;
Object _global_hascontent = null;
Collection var_collectionvar11_list_coerced$ = null;
Object _dynamic_component = bindings.get("component");
_global_listrow = renderContext.call("use", com.techcombank.core.models.ListRowModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = renderContext.getObjectModel().resolveProperty(_global_listrow, "listRowItems");
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <div class=\"sectioncontainer\">\r\n            <div class=\"tcb-sectionContainer\">\r\n                <div class=\"tcb-content-container\">\r\n                    <div class=\"list-row\">\r\n                        <div class=\"stock-information_right-col\">\r\n                            <div class=\"title-cmp\">\r\n                                <div class=\"title-cmp__title\">");
            {
                String var_1 = (("\r\n                                    " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listrow, "titleText"), "text"))) + "\r\n                                ");
                out.write(renderContext.getObjectModel().toString(var_1));
            }
            out.write("</div>\r\n                            </div>\r\n                            <div class=\"list-row-content\"");
            {
                Object var_attrvalue2 = renderContext.getObjectModel().resolveProperty(_global_listrow, "columnNumber");
                {
                    Object var_attrcontent3 = renderContext.call("xss", var_attrvalue2, "attribute");
                    {
                        boolean var_shoulddisplayattr5 = (((null != var_attrcontent3) && (!"".equals(var_attrcontent3))) && ((!"".equals(var_attrvalue2)) && (!((Object)false).equals(var_attrvalue2))));
                        if (var_shoulddisplayattr5) {
                            out.write(" data-columns");
                            {
                                boolean var_istrueattr4 = (var_attrvalue2.equals(true));
                                if (!var_istrueattr4) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent3));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue6 = renderContext.getObjectModel().resolveProperty(_global_listrow, "rowNumber");
                {
                    Object var_attrcontent7 = renderContext.call("xss", var_attrvalue6, "attribute");
                    {
                        boolean var_shoulddisplayattr9 = (((null != var_attrcontent7) && (!"".equals(var_attrcontent7))) && ((!"".equals(var_attrvalue6)) && (!((Object)false).equals(var_attrvalue6))));
                        if (var_shoulddisplayattr9) {
                            out.write(" data-rows");
                            {
                                boolean var_istrueattr8 = (var_attrvalue6.equals(true));
                                if (!var_istrueattr8) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent7));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                String var_attrcontent10 = (("grid-template-columns: repeat(" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_listrow, "columnNumber"), "html"))) + ", minmax(0, 1fr));");
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent10));
                out.write("\"");
            }
            out.write(">\r\n                                ");
            {
                Object var_collectionvar11 = _global_hascontent;
                {
                    long var_size12 = ((var_collectionvar11_list_coerced$ == null ? (var_collectionvar11_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar11)) : var_collectionvar11_list_coerced$).size());
                    {
                        boolean var_notempty13 = (var_size12 > 0);
                        if (var_notempty13) {
                            {
                                long var_end16 = var_size12;
                                {
                                    boolean var_validstartstepend17 = (((0 < var_size12) && true) && (var_end16 > 0));
                                    if (var_validstartstepend17) {
                                        if (var_collectionvar11_list_coerced$ == null) {
                                            var_collectionvar11_list_coerced$ = renderContext.getObjectModel().toCollection(var_collectionvar11);
                                        }
                                        long var_index18 = 0;
                                        for (Object item : var_collectionvar11_list_coerced$) {
                                            {
                                                boolean var_traversal20 = (((var_index18 >= 0) && (var_index18 <= var_end16)) && true);
                                                if (var_traversal20) {
                                                    out.write("\r\n                                    <a class=\"list-row-content_item\"");
                                                    {
                                                        Object var_attrvalue21 = renderContext.getObjectModel().resolveProperty(item, "linkUrl");
                                                        {
                                                            Object var_attrcontent22 = renderContext.call("xss", var_attrvalue21, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr24 = (((null != var_attrcontent22) && (!"".equals(var_attrcontent22))) && ((!"".equals(var_attrvalue21)) && (!((Object)false).equals(var_attrvalue21))));
                                                                if (var_shoulddisplayattr24) {
                                                                    out.write(" href");
                                                                    {
                                                                        boolean var_istrueattr23 = (var_attrvalue21.equals(true));
                                                                        if (!var_istrueattr23) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent22));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue25 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "openInNewTab")) ? "_blank" : "_self");
                                                        {
                                                            Object var_attrcontent26 = renderContext.call("xss", var_attrvalue25, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr28 = (((null != var_attrcontent26) && (!"".equals(var_attrcontent26))) && ((!"".equals(var_attrvalue25)) && (!((Object)false).equals(var_attrvalue25))));
                                                                if (var_shoulddisplayattr28) {
                                                                    out.write(" target");
                                                                    {
                                                                        boolean var_istrueattr27 = (var_attrvalue25.equals(true));
                                                                        if (!var_istrueattr27) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent26));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        String var_attrvalue29 = (renderContext.getObjectModel().toBoolean(renderContext.getObjectModel().resolveProperty(item, "noFollow")) ? "nofollow" : "");
                                                        {
                                                            Object var_attrcontent30 = renderContext.call("xss", var_attrvalue29, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr32 = (((null != var_attrcontent30) && (!"".equals(var_attrcontent30))) && ((!"".equals(var_attrvalue29)) && (!((Object)false).equals(var_attrvalue29))));
                                                                if (var_shoulddisplayattr32) {
                                                                    out.write(" rel");
                                                                    {
                                                                        boolean var_istrueattr31 = (var_attrvalue29.equals(true));
                                                                        if (!var_istrueattr31) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent30));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write(" data-tracking-click-event=\"linkClick\"");
                                                    {
                                                        String var_attrcontent33 = (("{'linkClick': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "rowTitle"), "attribute"))) + "'}");
                                                        out.write(" data-tracking-click-info-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                                        out.write("\"");
                                                    }
                                                    {
                                                        String var_attrcontent34 = (((("{'webInteractions': {'name': '" + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_dynamic_component, "title"), "attribute"))) + "','type': '") + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "webInteractionType"), "attribute"))) + "'}}");
                                                        out.write(" data-tracking-web-interaction-value=\"");
                                                        out.write(renderContext.getObjectModel().toString(var_attrcontent34));
                                                        out.write("\"");
                                                    }
                                                    out.write(">\r\n                                        <div class=\"list-row-content_main\">\r\n                                            <span class=\"list-row-content_title\">");
                                                    {
                                                        Object var_35 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "rowTitle"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_35));
                                                    }
                                                    out.write("</span>\r\n                                            <span class=\"list-row-content_date\">\r\n                                                <span class=\"list-row-content_type\">");
                                                    {
                                                        Object var_36 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "tagLabel"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_36));
                                                    }
                                                    out.write("</span>\r\n                                                <span class=\"list-row-content_divider\"></span>\r\n                                                <span class=\"color-gray\">");
                                                    {
                                                        Object var_37 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(item, "formattedDate"), "text");
                                                        out.write(renderContext.getObjectModel().toString(var_37));
                                                    }
                                                    out.write("</span>\r\n                                            </span>\r\n                                        </div>\r\n                                        <div class=\"list-row-content_arrow\">\r\n                                            <img");
                                                    {
                                                        Object var_attrvalue38 = renderContext.getObjectModel().resolveProperty(item, "altText");
                                                        {
                                                            Object var_attrcontent39 = renderContext.call("xss", var_attrvalue38, "attribute");
                                                            {
                                                                boolean var_shoulddisplayattr41 = (((null != var_attrcontent39) && (!"".equals(var_attrcontent39))) && ((!"".equals(var_attrvalue38)) && (!((Object)false).equals(var_attrvalue38))));
                                                                if (var_shoulddisplayattr41) {
                                                                    out.write(" alt");
                                                                    {
                                                                        boolean var_istrueattr40 = (var_attrvalue38.equals(true));
                                                                        if (!var_istrueattr40) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent39));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    {
                                                        Object var_attrvalue42 = renderContext.getObjectModel().resolveProperty(item, "iconImage");
                                                        {
                                                            Object var_attrcontent43 = renderContext.call("xss", var_attrvalue42, "uri");
                                                            {
                                                                boolean var_shoulddisplayattr45 = (((null != var_attrcontent43) && (!"".equals(var_attrcontent43))) && ((!"".equals(var_attrvalue42)) && (!((Object)false).equals(var_attrvalue42))));
                                                                if (var_shoulddisplayattr45) {
                                                                    out.write(" src");
                                                                    {
                                                                        boolean var_istrueattr44 = (var_attrvalue42.equals(true));
                                                                        if (!var_istrueattr44) {
                                                                            out.write("=\"");
                                                                            out.write(renderContext.getObjectModel().toString(var_attrcontent43));
                                                                            out.write("\"");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    out.write("/>\r\n                                        </div>\r\n                                    </a>\r\n                                ");
                                                }
                                            }
                                            var_index18++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var_collectionvar11_list_coerced$ = null;
            }
            out.write("\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar46 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions47_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions47 = obj().with("isEmpty", var_templateoptions47_field$_isempty);
            callUnit(out, renderContext, var_templatevar46, var_templateoptions47);
        }
    }
}
out.write("\r\n");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

