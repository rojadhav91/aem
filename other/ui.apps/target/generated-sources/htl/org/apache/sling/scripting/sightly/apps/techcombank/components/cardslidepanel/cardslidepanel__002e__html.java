/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.techcombank.components.cardslidepanel;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.render.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;

public final class cardslidepanel__002e__html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

Object _global_model = null;
Object _global_template = null;
Object _global_hascontent = null;
_global_model = renderContext.call("use", com.techcombank.core.models.CardSlidePanelModel.class.getName(), obj());
_global_template = renderContext.call("use", "core/wcm/components/commons/v1/templates.html", obj());
_global_hascontent = _global_model;
if (renderContext.getObjectModel().toBoolean(_global_hascontent)) {
    out.write("<div>\r\n    ");
    {
        Object var_testvariable0 = _global_hascontent;
        if (renderContext.getObjectModel().toBoolean(var_testvariable0)) {
            out.write("\r\n        <section class=\"security-slideshow\"");
            {
                String var_attrcontent1 = ("--bg-color: " + renderContext.getObjectModel().toString(renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "backgroundColour"), "styleString")));
                out.write(" style=\"");
                out.write(renderContext.getObjectModel().toString(var_attrcontent1));
                out.write("\"");
            }
            out.write(">\r\n            <div class=\"security-grid-container width-100\">\r\n                <div class=\"security-grid-root display-flex position-relative\">\r\n                    <div class=\"security-grid-item\">\r\n                        <div class=\"security-slideshow-container position-relative\">\r\n                            <div class=\"security-slideshow-title\">\r\n                                ");
            {
                Object var_testvariable2 = renderContext.getObjectModel().resolveProperty(_global_model, "title");
                if (renderContext.getObjectModel().toBoolean(var_testvariable2)) {
                    out.write("<h2>");
                    {
                        Object var_3 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "title"), "text");
                        out.write(renderContext.getObjectModel().toString(var_3));
                    }
                    out.write("</h2>");
                }
            }
            out.write("\r\n                                ");
            {
                Object var_testvariable4 = renderContext.getObjectModel().resolveProperty(_global_model, "description");
                if (renderContext.getObjectModel().toBoolean(var_testvariable4)) {
                    out.write("<p>");
                    {
                        Object var_5 = renderContext.call("xss", renderContext.getObjectModel().resolveProperty(_global_model, "description"), "html");
                        out.write(renderContext.getObjectModel().toString(var_5));
                    }
                    out.write("</p>");
                }
            }
            out.write("\r\n                            </div>\r\n                            <div class=\"security-slideshow-gallery security-slider\">\r\n                                <div class=\"security-gallery-images display-flex security-slider-container not-slider-mobile\">\r\n                                    ");
            {
                Object var_testvariable6 = renderContext.getObjectModel().resolveProperty(_global_model, "image1");
                if (renderContext.getObjectModel().toBoolean(var_testvariable6)) {
                    out.write("<div class=\"security-gallery-item\">\r\n                                        <div class=\"security-item-outer\">\r\n                                            <div class=\"security-item-inner\">\r\n                                                <span>\r\n                                                    <span>\r\n                                                        <img src=\"data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27568%27%20height=%27398%27/%3e\" alt=\"#\"/>\r\n                                                    </span>\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue7 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage1Path");
                        {
                            Object var_attrcontent8 = renderContext.call("xss", var_attrvalue7, "attribute");
                            {
                                boolean var_shoulddisplayattr10 = (((null != var_attrcontent8) && (!"".equals(var_attrcontent8))) && ((!"".equals(var_attrvalue7)) && (!((Object)false).equals(var_attrvalue7))));
                                if (var_shoulddisplayattr10) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr9 = (var_attrvalue7.equals(true));
                                        if (!var_istrueattr9) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent8));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue11 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage1Path");
                        {
                            Object var_attrcontent12 = renderContext.call("xss", var_attrvalue11, "attribute");
                            {
                                boolean var_shoulddisplayattr14 = (((null != var_attrcontent12) && (!"".equals(var_attrcontent12))) && ((!"".equals(var_attrvalue11)) && (!((Object)false).equals(var_attrvalue11))));
                                if (var_shoulddisplayattr14) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr13 = (var_attrvalue11.equals(true));
                                        if (!var_istrueattr13) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent12));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue15 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage1Path");
                        {
                            Object var_attrcontent16 = renderContext.call("xss", var_attrvalue15, "attribute");
                            {
                                boolean var_shoulddisplayattr18 = (((null != var_attrcontent16) && (!"".equals(var_attrcontent16))) && ((!"".equals(var_attrvalue15)) && (!((Object)false).equals(var_attrvalue15))));
                                if (var_shoulddisplayattr18) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr17 = (var_attrvalue15.equals(true));
                                        if (!var_istrueattr17) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent16));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue19 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage1Path");
                        {
                            Object var_attrcontent20 = renderContext.call("xss", var_attrvalue19, "attribute");
                            {
                                boolean var_shoulddisplayattr22 = (((null != var_attrcontent20) && (!"".equals(var_attrcontent20))) && ((!"".equals(var_attrvalue19)) && (!((Object)false).equals(var_attrvalue19))));
                                if (var_shoulddisplayattr22) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr21 = (var_attrvalue19.equals(true));
                                        if (!var_istrueattr21) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent20));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <img");
                    {
                        Object var_attrvalue23 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAltText1");
                        {
                            Object var_attrcontent24 = renderContext.call("xss", var_attrvalue23, "attribute");
                            {
                                boolean var_shoulddisplayattr26 = (((null != var_attrcontent24) && (!"".equals(var_attrcontent24))) && ((!"".equals(var_attrvalue23)) && (!((Object)false).equals(var_attrvalue23))));
                                if (var_shoulddisplayattr26) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr25 = (var_attrvalue23.equals(true));
                                        if (!var_istrueattr25) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent24));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue27 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage1Path");
                        {
                            Object var_attrcontent28 = renderContext.call("xss", var_attrvalue27, "uri");
                            {
                                boolean var_shoulddisplayattr30 = (((null != var_attrcontent28) && (!"".equals(var_attrcontent28))) && ((!"".equals(var_attrvalue27)) && (!((Object)false).equals(var_attrvalue27))));
                                if (var_shoulddisplayattr30) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr29 = (var_attrvalue27.equals(true));
                                        if (!var_istrueattr29) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent28));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                    </picture>\r\n                                                </span>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>");
                }
            }
            out.write("\r\n                                    ");
            {
                Object var_testvariable31 = renderContext.getObjectModel().resolveProperty(_global_model, "image2");
                if (renderContext.getObjectModel().toBoolean(var_testvariable31)) {
                    out.write("<div class=\"security-gallery-item\">\r\n                                        <div class=\"security-item-outer\">\r\n                                            <div class=\"security-item-inner\">\r\n                                                <span>\r\n                                                    <span>\r\n                                                        <img src=\"data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27568%27%20height=%27398%27/%3e\" alt=\"#\"/>\r\n                                                    </span>\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue32 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage2Path");
                        {
                            Object var_attrcontent33 = renderContext.call("xss", var_attrvalue32, "attribute");
                            {
                                boolean var_shoulddisplayattr35 = (((null != var_attrcontent33) && (!"".equals(var_attrcontent33))) && ((!"".equals(var_attrvalue32)) && (!((Object)false).equals(var_attrvalue32))));
                                if (var_shoulddisplayattr35) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr34 = (var_attrvalue32.equals(true));
                                        if (!var_istrueattr34) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent33));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue36 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage2Path");
                        {
                            Object var_attrcontent37 = renderContext.call("xss", var_attrvalue36, "attribute");
                            {
                                boolean var_shoulddisplayattr39 = (((null != var_attrcontent37) && (!"".equals(var_attrcontent37))) && ((!"".equals(var_attrvalue36)) && (!((Object)false).equals(var_attrvalue36))));
                                if (var_shoulddisplayattr39) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr38 = (var_attrvalue36.equals(true));
                                        if (!var_istrueattr38) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent37));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue40 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage2Path");
                        {
                            Object var_attrcontent41 = renderContext.call("xss", var_attrvalue40, "attribute");
                            {
                                boolean var_shoulddisplayattr43 = (((null != var_attrcontent41) && (!"".equals(var_attrcontent41))) && ((!"".equals(var_attrvalue40)) && (!((Object)false).equals(var_attrvalue40))));
                                if (var_shoulddisplayattr43) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr42 = (var_attrvalue40.equals(true));
                                        if (!var_istrueattr42) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent41));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue44 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage2Path");
                        {
                            Object var_attrcontent45 = renderContext.call("xss", var_attrvalue44, "attribute");
                            {
                                boolean var_shoulddisplayattr47 = (((null != var_attrcontent45) && (!"".equals(var_attrcontent45))) && ((!"".equals(var_attrvalue44)) && (!((Object)false).equals(var_attrvalue44))));
                                if (var_shoulddisplayattr47) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr46 = (var_attrvalue44.equals(true));
                                        if (!var_istrueattr46) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent45));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <img");
                    {
                        Object var_attrvalue48 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAltText2");
                        {
                            Object var_attrcontent49 = renderContext.call("xss", var_attrvalue48, "attribute");
                            {
                                boolean var_shoulddisplayattr51 = (((null != var_attrcontent49) && (!"".equals(var_attrcontent49))) && ((!"".equals(var_attrvalue48)) && (!((Object)false).equals(var_attrvalue48))));
                                if (var_shoulddisplayattr51) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr50 = (var_attrvalue48.equals(true));
                                        if (!var_istrueattr50) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent49));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue52 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage2Path");
                        {
                            Object var_attrcontent53 = renderContext.call("xss", var_attrvalue52, "uri");
                            {
                                boolean var_shoulddisplayattr55 = (((null != var_attrcontent53) && (!"".equals(var_attrcontent53))) && ((!"".equals(var_attrvalue52)) && (!((Object)false).equals(var_attrvalue52))));
                                if (var_shoulddisplayattr55) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr54 = (var_attrvalue52.equals(true));
                                        if (!var_istrueattr54) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent53));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                    </picture>\r\n                                                </span>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>");
                }
            }
            out.write("\r\n                                    <div class=\"security-gallery-item security-gallery-center\">\r\n                                        <div class=\"security-item-outer\">\r\n                                            <div class=\"security-item-inner\">\r\n                                                <span>\r\n                                                    <span>\r\n                                                        <img src=\"data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27472%27%20height=%27658%27/%3e\" alt=\"#\"/>\r\n                                                    </span>\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
            {
                Object var_attrvalue56 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileCenterImagePath");
                {
                    Object var_attrcontent57 = renderContext.call("xss", var_attrvalue56, "attribute");
                    {
                        boolean var_shoulddisplayattr59 = (((null != var_attrcontent57) && (!"".equals(var_attrcontent57))) && ((!"".equals(var_attrvalue56)) && (!((Object)false).equals(var_attrvalue56))));
                        if (var_shoulddisplayattr59) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr58 = (var_attrvalue56.equals(true));
                                if (!var_istrueattr58) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent57));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
            {
                Object var_attrvalue60 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileCenterImagePath");
                {
                    Object var_attrcontent61 = renderContext.call("xss", var_attrvalue60, "attribute");
                    {
                        boolean var_shoulddisplayattr63 = (((null != var_attrcontent61) && (!"".equals(var_attrcontent61))) && ((!"".equals(var_attrvalue60)) && (!((Object)false).equals(var_attrvalue60))));
                        if (var_shoulddisplayattr63) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr62 = (var_attrvalue60.equals(true));
                                if (!var_istrueattr62) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent61));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
            {
                Object var_attrvalue64 = renderContext.getObjectModel().resolveProperty(_global_model, "webCenterImagePath");
                {
                    Object var_attrcontent65 = renderContext.call("xss", var_attrvalue64, "attribute");
                    {
                        boolean var_shoulddisplayattr67 = (((null != var_attrcontent65) && (!"".equals(var_attrcontent65))) && ((!"".equals(var_attrvalue64)) && (!((Object)false).equals(var_attrvalue64))));
                        if (var_shoulddisplayattr67) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr66 = (var_attrvalue64.equals(true));
                                if (!var_istrueattr66) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent65));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
            {
                Object var_attrvalue68 = renderContext.getObjectModel().resolveProperty(_global_model, "webCenterImagePath");
                {
                    Object var_attrcontent69 = renderContext.call("xss", var_attrvalue68, "attribute");
                    {
                        boolean var_shoulddisplayattr71 = (((null != var_attrcontent69) && (!"".equals(var_attrcontent69))) && ((!"".equals(var_attrvalue68)) && (!((Object)false).equals(var_attrvalue68))));
                        if (var_shoulddisplayattr71) {
                            out.write(" srcset");
                            {
                                boolean var_istrueattr70 = (var_attrvalue68.equals(true));
                                if (!var_istrueattr70) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent69));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                                                        <img");
            {
                Object var_attrvalue72 = renderContext.getObjectModel().resolveProperty(_global_model, "centerImageAltText");
                {
                    Object var_attrcontent73 = renderContext.call("xss", var_attrvalue72, "attribute");
                    {
                        boolean var_shoulddisplayattr75 = (((null != var_attrcontent73) && (!"".equals(var_attrcontent73))) && ((!"".equals(var_attrvalue72)) && (!((Object)false).equals(var_attrvalue72))));
                        if (var_shoulddisplayattr75) {
                            out.write(" alt");
                            {
                                boolean var_istrueattr74 = (var_attrvalue72.equals(true));
                                if (!var_istrueattr74) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent73));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            {
                Object var_attrvalue76 = renderContext.getObjectModel().resolveProperty(_global_model, "webCenterImagePath");
                {
                    Object var_attrcontent77 = renderContext.call("xss", var_attrvalue76, "uri");
                    {
                        boolean var_shoulddisplayattr79 = (((null != var_attrcontent77) && (!"".equals(var_attrcontent77))) && ((!"".equals(var_attrvalue76)) && (!((Object)false).equals(var_attrvalue76))));
                        if (var_shoulddisplayattr79) {
                            out.write(" src");
                            {
                                boolean var_istrueattr78 = (var_attrvalue76.equals(true));
                                if (!var_istrueattr78) {
                                    out.write("=\"");
                                    out.write(renderContext.getObjectModel().toString(var_attrcontent77));
                                    out.write("\"");
                                }
                            }
                        }
                    }
                }
            }
            out.write("/>\r\n                                                    </picture>\r\n                                                </span>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                    ");
            {
                Object var_testvariable80 = renderContext.getObjectModel().resolveProperty(_global_model, "image4");
                if (renderContext.getObjectModel().toBoolean(var_testvariable80)) {
                    out.write("<div class=\"security-gallery-item\">\r\n                                        <div class=\"security-item-outer\">\r\n                                            <div class=\"security-item-inner\">\r\n                                                <span>\r\n                                                    <span>\r\n                                                        <img src=\"data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27568%27%20height=%27398%27/%3e\" alt=\"#\"/>\r\n                                                    </span>\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue81 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage4Path");
                        {
                            Object var_attrcontent82 = renderContext.call("xss", var_attrvalue81, "attribute");
                            {
                                boolean var_shoulddisplayattr84 = (((null != var_attrcontent82) && (!"".equals(var_attrcontent82))) && ((!"".equals(var_attrvalue81)) && (!((Object)false).equals(var_attrvalue81))));
                                if (var_shoulddisplayattr84) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr83 = (var_attrvalue81.equals(true));
                                        if (!var_istrueattr83) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent82));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue85 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage4Path");
                        {
                            Object var_attrcontent86 = renderContext.call("xss", var_attrvalue85, "attribute");
                            {
                                boolean var_shoulddisplayattr88 = (((null != var_attrcontent86) && (!"".equals(var_attrcontent86))) && ((!"".equals(var_attrvalue85)) && (!((Object)false).equals(var_attrvalue85))));
                                if (var_shoulddisplayattr88) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr87 = (var_attrvalue85.equals(true));
                                        if (!var_istrueattr87) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent86));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue89 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage4Path");
                        {
                            Object var_attrcontent90 = renderContext.call("xss", var_attrvalue89, "attribute");
                            {
                                boolean var_shoulddisplayattr92 = (((null != var_attrcontent90) && (!"".equals(var_attrcontent90))) && ((!"".equals(var_attrvalue89)) && (!((Object)false).equals(var_attrvalue89))));
                                if (var_shoulddisplayattr92) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr91 = (var_attrvalue89.equals(true));
                                        if (!var_istrueattr91) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent90));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue93 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage4Path");
                        {
                            Object var_attrcontent94 = renderContext.call("xss", var_attrvalue93, "attribute");
                            {
                                boolean var_shoulddisplayattr96 = (((null != var_attrcontent94) && (!"".equals(var_attrcontent94))) && ((!"".equals(var_attrvalue93)) && (!((Object)false).equals(var_attrvalue93))));
                                if (var_shoulddisplayattr96) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr95 = (var_attrvalue93.equals(true));
                                        if (!var_istrueattr95) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent94));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <img");
                    {
                        Object var_attrvalue97 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAltText4");
                        {
                            Object var_attrcontent98 = renderContext.call("xss", var_attrvalue97, "attribute");
                            {
                                boolean var_shoulddisplayattr100 = (((null != var_attrcontent98) && (!"".equals(var_attrcontent98))) && ((!"".equals(var_attrvalue97)) && (!((Object)false).equals(var_attrvalue97))));
                                if (var_shoulddisplayattr100) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr99 = (var_attrvalue97.equals(true));
                                        if (!var_istrueattr99) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent98));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue101 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage4Path");
                        {
                            Object var_attrcontent102 = renderContext.call("xss", var_attrvalue101, "uri");
                            {
                                boolean var_shoulddisplayattr104 = (((null != var_attrcontent102) && (!"".equals(var_attrcontent102))) && ((!"".equals(var_attrvalue101)) && (!((Object)false).equals(var_attrvalue101))));
                                if (var_shoulddisplayattr104) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr103 = (var_attrvalue101.equals(true));
                                        if (!var_istrueattr103) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent102));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                    </picture>\r\n                                                </span>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>");
                }
            }
            out.write("\r\n                                    ");
            {
                Object var_testvariable105 = renderContext.getObjectModel().resolveProperty(_global_model, "image5");
                if (renderContext.getObjectModel().toBoolean(var_testvariable105)) {
                    out.write("<div class=\"security-gallery-item\">\r\n                                        <div class=\"security-item-outer\">\r\n                                            <div class=\"security-item-inner\">\r\n                                                <span>\r\n                                                    <span>\r\n                                                        <img src=\"data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27568%27%20height=%27398%27/%3e\" alt=\"#\"/>\r\n                                                    </span>\r\n                                                    <picture>\r\n                                                        <source media=\"(max-width: 360px)\"");
                    {
                        Object var_attrvalue106 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage5Path");
                        {
                            Object var_attrcontent107 = renderContext.call("xss", var_attrvalue106, "attribute");
                            {
                                boolean var_shoulddisplayattr109 = (((null != var_attrcontent107) && (!"".equals(var_attrcontent107))) && ((!"".equals(var_attrvalue106)) && (!((Object)false).equals(var_attrvalue106))));
                                if (var_shoulddisplayattr109) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr108 = (var_attrvalue106.equals(true));
                                        if (!var_istrueattr108) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent107));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(max-width: 576px)\"");
                    {
                        Object var_attrvalue110 = renderContext.getObjectModel().resolveProperty(_global_model, "mobileImage5Path");
                        {
                            Object var_attrcontent111 = renderContext.call("xss", var_attrvalue110, "attribute");
                            {
                                boolean var_shoulddisplayattr113 = (((null != var_attrcontent111) && (!"".equals(var_attrcontent111))) && ((!"".equals(var_attrvalue110)) && (!((Object)false).equals(var_attrvalue110))));
                                if (var_shoulddisplayattr113) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr112 = (var_attrvalue110.equals(true));
                                        if (!var_istrueattr112) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent111));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1080px)\"");
                    {
                        Object var_attrvalue114 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage5Path");
                        {
                            Object var_attrcontent115 = renderContext.call("xss", var_attrvalue114, "attribute");
                            {
                                boolean var_shoulddisplayattr117 = (((null != var_attrcontent115) && (!"".equals(var_attrcontent115))) && ((!"".equals(var_attrvalue114)) && (!((Object)false).equals(var_attrvalue114))));
                                if (var_shoulddisplayattr117) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr116 = (var_attrvalue114.equals(true));
                                        if (!var_istrueattr116) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent115));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <source media=\"(min-width: 1200px)\"");
                    {
                        Object var_attrvalue118 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage5Path");
                        {
                            Object var_attrcontent119 = renderContext.call("xss", var_attrvalue118, "attribute");
                            {
                                boolean var_shoulddisplayattr121 = (((null != var_attrcontent119) && (!"".equals(var_attrcontent119))) && ((!"".equals(var_attrvalue118)) && (!((Object)false).equals(var_attrvalue118))));
                                if (var_shoulddisplayattr121) {
                                    out.write(" srcset");
                                    {
                                        boolean var_istrueattr120 = (var_attrvalue118.equals(true));
                                        if (!var_istrueattr120) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent119));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                        <img");
                    {
                        Object var_attrvalue122 = renderContext.getObjectModel().resolveProperty(_global_model, "imageAltText5");
                        {
                            Object var_attrcontent123 = renderContext.call("xss", var_attrvalue122, "attribute");
                            {
                                boolean var_shoulddisplayattr125 = (((null != var_attrcontent123) && (!"".equals(var_attrcontent123))) && ((!"".equals(var_attrvalue122)) && (!((Object)false).equals(var_attrvalue122))));
                                if (var_shoulddisplayattr125) {
                                    out.write(" alt");
                                    {
                                        boolean var_istrueattr124 = (var_attrvalue122.equals(true));
                                        if (!var_istrueattr124) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent123));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    {
                        Object var_attrvalue126 = renderContext.getObjectModel().resolveProperty(_global_model, "webImage5Path");
                        {
                            Object var_attrcontent127 = renderContext.call("xss", var_attrvalue126, "uri");
                            {
                                boolean var_shoulddisplayattr129 = (((null != var_attrcontent127) && (!"".equals(var_attrcontent127))) && ((!"".equals(var_attrvalue126)) && (!((Object)false).equals(var_attrvalue126))));
                                if (var_shoulddisplayattr129) {
                                    out.write(" src");
                                    {
                                        boolean var_istrueattr128 = (var_attrvalue126.equals(true));
                                        if (!var_istrueattr128) {
                                            out.write("=\"");
                                            out.write(renderContext.getObjectModel().toString(var_attrcontent127));
                                            out.write("\"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    out.write("/>\r\n                                                    </picture>\r\n                                                </span>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>");
                }
            }
            out.write("\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </section>\r\n    ");
        }
    }
    out.write("\r\n</div>");
}
out.write("\r\n");
{
    Object var_templatevar130 = renderContext.getObjectModel().resolveProperty(_global_template, "placeholder");
    {
        boolean var_templateoptions131_field$_isempty = (!renderContext.getObjectModel().toBoolean(_global_hascontent));
        {
            java.util.Map var_templateoptions131 = obj().with("isEmpty", var_templateoptions131_field$_isempty);
            callUnit(out, renderContext, var_templatevar130, var_templateoptions131);
        }
    }
}


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

