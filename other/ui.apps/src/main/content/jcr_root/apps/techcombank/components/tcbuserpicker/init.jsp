<%@ include file="/libs/granite/ui/global.jsp" %><%
%><%@ page session="false"
          import="java.util.HashMap,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field" %><%

    Config cfg = cmp.getConfig();

    String name = cfg.get("name", String.class);
    String value = cmp.getValue().get(name, String.class);

    ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
    vm.put("value", value);

    request.setAttribute(Field.class.getName(), vm);
%>
