(function (document, $) {
	"use strict";
	
	$(document).on('foundation-contentloaded', function (e) { //event fires when dialog loads
		if ($("#ContentFrame").contents().find("body.business-banking-template").length == 1
			|| $("#ContentFrame").contents().find("body.personal-banking-template").length == 1) {
			$('coral-numberinput[name="./startLevel"]')
				.addClass('is-disabled')
				.addClass('disabled')
				.attr("disabled", true);
		}
	});
	
})(document, Granite.$);