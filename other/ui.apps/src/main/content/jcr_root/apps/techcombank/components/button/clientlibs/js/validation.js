(function ($document, $, Coral) {
    $document.on("dialog-ready", function (e) {
        Coral.commons.ready(function (dialog) {
            if ($(".ctatype coral-select-item:selected").val() == "default") {
                $('input[name="./ctaColor"]').prop("required", true);
                $('input[name="./fontColor"]').prop("required", true);
            }
        });
        if ($(".ctatype coral-select-item:selected").val() == "link") {
            $(".buttoncolor").parent().addClass("hide");
            $(".fontcolor").parent().addClass("hide");
        }
        $(".ctatype", e.target).each(function (i, element) {
            Coral.commons.ready(element, function (dialog) {
                $(dialog).on("change", function (event) {
                    if (dialog.value == "modal" || dialog.value == "default") {
                        $(".buttoncolor").parent().removeClass("hide");
                        $(".fontcolor").parent().removeClass("hide");
                        $('input[name="./ctaColor"]').prop("required", true);
                        $('input[name="./fontColor"]').prop("required", true);
                    } else {
                        $(".buttoncolor").parent().addClass("hide");
                        $(".fontcolor").parent().addClass("hide");
                        $('input[name="./ctaColor"]').prop("required", false);
                        $('input[name="./fontColor"]').prop("required", false);
                    }
                });
            });
        });
    });
})($(document), Granite.$, Coral);
