<%@ include file="/libs/granite/ui/global.jsp" %><%
%><%@ page session="false"
          import="com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag" %><%

    // @deprecated read-only mode is deprecated

    Config cfg = cmp.getConfig();

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();

    String fieldLabel = cfg.get("fieldLabel", String.class);
    String name = cfg.get("name", String.class);
    String value = cmp.getValue().get(name, "");

    if (cmp.getOptions().rootField()) {
        attrs.addClass("coral-Form-fieldwrapper");
        attrs.addClass(cfg.get("wrapperClass", String.class));

        %><span <%= attrs.build() %>><%
            if (fieldLabel != null) {
                %><label class="coral-Form-fieldlabel"><%= outVar(xssAPI, i18n, fieldLabel) %></label><%
            }
            %><span class="coral-Form-field"><%= xssAPI.encodeForHTML(value) %></span
        ></span><%
    } else {
        %><span><%= xssAPI.encodeForHTML(value) %></span><%
    }
%>
