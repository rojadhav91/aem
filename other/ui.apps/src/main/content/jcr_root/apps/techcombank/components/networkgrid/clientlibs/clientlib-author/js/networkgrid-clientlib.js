(function ($, $document) {
    "use strict";
 
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./networkGridItems"]',
        validate: function() {
            let count = $("coral-multifield-item").length;
            if(count != 4){
                return "There must be 4 items";
            }
        }
    });
})($, $(document));