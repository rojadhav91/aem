(function (document, $) {
    "use strict";
    // when dialog gets injected
    $(document).on("foundation-contentloaded", function (e) {
        // if there is already an inital value make sure the
        //according target element becomes visible
        showHideHandler($(".cq-dialog-dropdown-showhide-calculation", e.target));
    });
$(document).on("change", 'coral-select[name*="calculationTypeDropdown"]', function (e) {
	const selector = $(this).closest('coral-multifield-item-content');
        if ($(this).val() == "decreasingBalanceSheet") {
            selector.find('foundation-autocomplete').filter("[data-outputpanelBg-validation]").prop("required", true);
            selector.find('foundation-autocomplete').filter("[data-outputpanel-validation]").prop("required", true);
            selector.find('input[name*="./decreasingImgAltText"]').prop("required", true);
            selector.find('input[name*="./decreasingMonthlyLabelText"]').prop("required", true);
            selector.find('input[name*="./decreasingFromLabelText"]').prop("required", true);
            selector.find('input[name*="./decreasingFromPayValue"]').prop("required", true);
            selector.find('input[name*="./decreasingToLabelText"]').prop("required",  true);
            selector.find('input[name*="./decreasingToPayValue"]').prop("required", true);
            selector.find('input[name*="./decreasingTotalIntLabel"]').prop("required", true);
            selector.find('input[name*="./decreasingTotalIntValue"]').prop("required", true);
            selector.find('input[name*="./decreasingCurrencyText"]').prop("required", true);
            selector.find('input[name*="./decreasingMonthTextLoanTerm"]').prop("required", true);
            selector.find('input[name*="./decreasingPercentageIntRate"]').prop("required", true);
            selector.find('input[name*="./decreasingLinkTextLabel"]').prop("required", true);

            selector.find('foundation-autocomplete').filter("[data-outputPanelFixed-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputPanelFixedIcon-validation]").prop("required", false);
            selector.find('input[name*="./fixedBg"]').prop("required", false);
            selector.find('input[name*="./fixedCardIcon"]').prop("required", false);
            selector.find('input[name*="./fixedImageAltText"]').prop("required", false);
            selector.find('input[name*="./fixedAmountLabelText"]').prop("required", false);
            selector.find('input[name*="./fixedMonthlyAmountValue"]').prop("required", false);
            selector.find('input[name*="./fixedTotalIntLabel"]').prop("required", false);
            selector.find('input[name*="./fixedTotalIntValue"]').prop("required", false);
            selector.find('input[name*="./fixedCurrencyText"]').prop("required", false);

            selector.find('foundation-autocomplete').filter("[data-outputPanelCarBg-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputPanelCarIcon-validation]").prop("required", false);
            selector.find('input[name*="./carBg"]').prop("required", false);
            selector.find('input[name*="./carCardIcon"]').prop("required", false);
            selector.find('input[name*="./carIconAltText"]').prop("required", false);
            selector.find('input[name*="./carMonPrincipalLabel"]').prop("required", false);
            selector.find('input[name*="./carMonPrincipalValue"]').prop("required", false);
            selector.find('input[name*="./carMonIntRateLabel"]').prop("required", false);
            selector.find('input[name*="./carMonIntRateValue"]').prop("required", false);
            selector.find('input[name*="./carMonInstallmentLabel"]').prop("required", false);
            selector.find('input[name*="./carMonInstallmentValue"]').prop("required", false);
            selector.find('input[name*="./carCurrencyText"]').prop("required", false);
            selector.find('input[name*="./carLinkText"]').prop("required", false);
        } else if ($(this).val() == "tableOfFixedMonthlyPayments") {
            selector.find('foundation-autocomplete').filter("[data-outputPanelFixed-validation]").prop("required", true);
            selector.find('foundation-autocomplete').filter("[data-outputPanelFixedIcon-validation]").prop("required", true);
            selector.find('input[name*="./fixedBg"]').prop("required", true);
            selector.find('input[name*="./fixedCardIcon"]').prop("required", true);
            selector.find('input[name*="./fixedImageAltText"]').prop("required", true);
            selector.find('input[name*="./fixedAmountLabelText"]').prop("required", true);
            selector.find('input[name*="./fixedMonthlyAmountValue"]').prop("required", true);
            selector.find('input[name*="./fixedTotalIntLabel"]').prop("required", true);
            selector.find('input[name*="./fixedTotalIntValue"]').prop("required", true);
            selector.find('input[name*="./fixedCurrencyText"]').prop("required", true);

            selector.find('foundation-autocomplete').filter("[data-outputpanelBg-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputpanel-validation]").prop("required", false);
            selector.find('input[name*="./decreasingImgAltText"]').prop("required", false);
            selector.find('input[name*="./decreasingMonthlyLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingFromLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingFromPayValue"]').prop("required", false);
            selector.find('input[name*="./decreasingToLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingToPayValue"]').prop("required", false);
            selector.find('input[name*="./decreasingTotalIntLabel"]').prop("required", false);
            selector.find('input[name*="./decreasingTotalIntValue"]').prop("required", false);
            selector.find('input[name*="./decreasingCurrencyText"]').prop("required", false);
            selector.find('input[name*="./decreasingMonthTextLoanTerm"]').prop("required", false);
            selector.find('input[name*="./decreasingPercentageIntRate"]').prop("required", false);
            selector.find('input[name*="./decreasingLinkTextLabel"]').prop("required", false);

            selector.find('foundation-autocomplete').filter("[data-outputPanelCarBg-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputPanelCarIcon-validation]").prop("required", false);
            selector.find('input[name*="./carBg"]').prop("required", false);
            selector.find('input[name*="./carCardIcon"]').prop("required", false);
            selector.find('input[name*="./carIconAltText"]').prop("required", false);
            selector.find('input[name*="./carMonPrincipalLabel"]').prop("required", false);
            selector.find('input[name*="./carMonPrincipalValue"]').prop("required", false);
            selector.find('input[name*="./carMonIntRateLabel"]').prop("required", false);
            selector.find('input[name*="./carMonIntRateValue"]').prop("required", false);
            selector.find('input[name*="./carMonInstallmentLabel"]').prop("required", false);
            selector.find('input[name*="./carMonInstallmentValue"]').prop("required", false);
            selector.find('input[name*="./carCurrencyText"]').prop("required", false);
            selector.find('input[name*="./carLinkText"]').prop("required", false);
        } else if ($(this).val() == "carLoan") {
			selector.find('foundation-autocomplete').filter("[data-outputPanelCarBg-validation]").prop("required", true);
            selector.find('foundation-autocomplete').filter("[data-outputPanelCarIcon-validation]").prop("required", true);
            selector.find('input[name*="./carBg"]').prop("required", true);
            selector.find('input[name*="./carCardIcon"]').prop("required", true);
            selector.find('input[name*="./carIconAltText"]').prop("required", true);
            selector.find('input[name*="./carMonPrincipalLabel"]').prop("required", true);
            selector.find('input[name*="./carMonPrincipalValue"]').prop("required", true);
            selector.find('input[name*="./carMonIntRateLabel"]').prop("required", true);
            selector.find('input[name*="./carMonIntRateValue"]').prop("required", true);
            selector.find('input[name*="./carMonInstallmentLabel"]').prop("required", true);
            selector.find('input[name*="./carMonInstallmentValue"]').prop("required", true);
            selector.find('input[name*="./carCurrencyText"]').prop("required", true);
            selector.find('input[name*="./carLinkText"]').prop("required", true);

            selector.find('foundation-autocomplete').filter("[data-outputpanelBg-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputpanel-validation]").prop("required", false);
            selector.find('input[name*="./decreasingImgAltText"]').prop("required", false);
            selector.find('input[name*="./decreasingMonthlyLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingFromLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingFromPayValue"]').prop("required", false);
            selector.find('input[name*="./decreasingToLabelText"]').prop("required", false);
            selector.find('input[name*="./decreasingToPayValue"]').prop("required", false);
            selector.find('input[name*="./decreasingTotalIntLabel"]').prop("required", false);
            selector.find('input[name*="./decreasingTotalIntValue"]').prop("required", false);
            selector.find('input[name*="./decreasingCurrencyText"]').prop("required", false);
            selector.find('input[name*="./decreasingMonthTextLoanTerm"]').prop("required", false);
            selector.find('input[name*="./decreasingPercentageIntRate"]').prop("required", false);
            selector.find('input[name*="./decreasingLinkTextLabel"]').prop("required", false);

            selector.find('foundation-autocomplete').filter("[data-outputPanelFixed-validation]").prop("required", false);
            selector.find('foundation-autocomplete').filter("[data-outputPanelFixedIcon-validation]").prop("required", false);
            selector.find('input[name*="./fixedBg"]').prop("required", false);
            selector.find('input[name*="./fixedCardIcon"]').prop("required", false);
            selector.find('input[name*="./fixedImageAltText"]').prop("required", false);
            selector.find('input[name*="./fixedAmountLabelText"]').prop("required", false);
            selector.find('input[name*="./fixedMonthlyAmountValue"]').prop("required", false);
            selector.find('input[name*="./fixedTotalIntLabel"]').prop("required", false);
            selector.find('input[name*="./fixedTotalIntValue"]').prop("required", false);
            selector.find('input[name*="./fixedCurrencyText"]').prop("required", false);
        }

    });

    $(document).on("change", ".cq-dialog-dropdown-showhide-calculation", function (e) {
        showHideHandler($(this));
    });

    function showHideHandler(el) {
        el.each(function (i, element) {
            // handle Coral3 base drop-down
            Coral.commons.ready(element, function (component) {
                showHideCustom(component, element);
                component.on("change", function () {
                    showHideCustom(component, element);
                });
            });
        });
    }

    function showHideCustom(component, element) {
        // get the selector to find the target elements.
        //its stored as data-.. attribute
        const target = $(element).data("cq-dialog-dropdown-showhide-target-calculation");
        const elementIndex = $(element).closest("coral-multifield-item").index();

        if (target) {
            let value;
            if (component.value) {
                value = component.value;
            } else {
                value = component.getValue();
            }
            $(element)
                .closest("coral-multifield-item")
                .find(target)
                .each(function (index) {
                    let tarIndex = $(this).closest("coral-multifield-item").index();
                    if (elementIndex == tarIndex) {
                        $(this).not(".hide").parent().addClass("hide");
                        $(this)
                            .filter("[data-showhidetargetvalue='" + value + "']")
                            .parent()
                            .removeClass("hide");
                    }
                });
        }
    }
})(document, Granite.$);
