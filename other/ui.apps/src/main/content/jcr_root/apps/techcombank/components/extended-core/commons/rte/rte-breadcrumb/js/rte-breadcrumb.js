(function($, $document) {
    "use strict";

    $(document).on("dialog-ready", function() {
        addBreadCrumb();
    });

    $(document).on("click", ".item-xpath", function(e) {
        const xpath = $(this).data("xpath");
        setCursor(getElementByXpath(xpath));
    });

    $(document).on("click", ".coral3-Button--secondary", function(e) {
        addBreadCrumb();
    });

    function addBreadCrumb() {
        let breadcrumbItem = '<div class="breadcrumb-rte"><p class="placeholder">Selected item breadcrumb will be shown here, and allow to select parents by clicking on breadcrumb</p></div>';
        setTimeout(function() {
            $('.coral-RichText').each(function() {
                $(this).parent().find(".breadcrumb-rte").remove();
                $(this).parent().find(".rte-sourceEditor").after(breadcrumbItem);
            });
            $('.coral-RichText').click(function(e) {
                let $target = $(e.target);
                if (!$target.hasClass("coral-RichText")) {
                    let xpath = getXPath(e.target);
                    $(this).parent().find('.breadcrumb-rte').html(createBreadCrumb(xpath, false));
                }
            });
        }, 1000);
    }

    function createBreadCrumb(xpath, sep) {
        let separator = '<div aria-hidden="true" class="item-sep"> >> </div>';
        let tagName = xpath.substr(xpath.lastIndexOf("/") + 1)
        let breadcrumbStr = '<div role="button" class="item-xpath" data-xpath="' + xpath.replace(/\|/g, "/") + '">' + tagName + '</div>'
        if (sep) {
            breadcrumbStr = breadcrumbStr + separator;
        }
        let subPath = xpath.substr(0, xpath.lastIndexOf("/"));
        if (subPath.indexOf("/") > -1)
            breadcrumbStr = createBreadCrumb(subPath, true) + breadcrumbStr;
        return breadcrumbStr;
    }

    function setCursor(element) {
        const el = element;
        let range = document.createRange();
        let sel = window.getSelection();
        range.setStart(el, 0);
        range.setEnd(el, el.childNodes.length);
        sel.removeAllRanges();
        sel.addRange(range);
    }

    function getXPath(element) {
        let xpath = '';
        let separator = '/';
        for (; element && element.nodeType == 1; element = element.parentNode) {
            if ($(element).hasClass("coral-RichText")) {
                separator = '|';
            }
            let id = $(element.parentNode).children(element.tagName).index(element) + 1;
            id > 1 ? (id = '[' + id + ']') : (id = '');
            xpath = separator + element.tagName.toLowerCase() + id + xpath;
        }
        return xpath;
    }

    function getElementByXpath(path) {
        return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    }
})($, $(document));
