// This code loads the IFrame Player API code asynchronously.
let tag = document.createElement("script");
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName("script")[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function youtubeParser(url) {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    let match = url.match(regExp);
    return match && match[7].length == 11 ? match[7] : false;
}

function matchYoutubeUrl(url) {
    const p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    if (url.match(p)) {
        return true;
    }
    return false;
}

function isValidUrl(string) {
    try {
        new URL(string);
        return true;
    } catch (err) {
        return false;
    }
}
// This function creates an <iframe> (and YouTube player) after the API code downloads.
let player;
function onYouTubeIframeAPIReady() {
    const slides = $(".cmp-custom-embed");
    for (let i = 0; i < slides.length; i++) {
        let videoWidth;
        let videoHeight;
        let parentClass = $(".cmp-custom-embed")[i];
        let layout = $(parentClass).find(".embed_video_layout").val();
        let aspectRatio = $(parentClass).find("#embed_video_aspect_ratio").val();
        let videoUrl = $(parentClass).find("#embed_url").val();
        let videoType = $(parentClass).find("#embed_type").val();
        let iframeUrl = $(parentClass).find("iframe").attr("src");
        let id = $(parentClass).find("#embed_id").val();
        if (layout == "fixed") {
            videoWidth = $(parentClass).find("#embed_video_width").val();
            videoHeight = $(parentClass).find("#embed_video_height").val();
        } else if (layout == "responsive") {
            videoWidth = $(".embed").width();
            videoHeight = (videoWidth * aspectRatio) / 100;
        }
        let videoId;
        if (videoType == "EMBEDDABLE") {
            videoId = $(parentClass).find("#embed_video_id").val();
            player = new YT.Player(id, {
                height: videoHeight,
                width: videoWidth,
                videoId: videoId,
                playerVars: {
                    playsinline: 1,
                },
                events: {
                    onStateChange: onPlayerStateChange,
                },
            });
        } else if (videoType == "URL") {
            if (matchYoutubeUrl(videoUrl) === true) {
                videoId = youtubeParser(videoUrl);
                player = new YT.Player(id, {
                    videoId: videoId,
                    playerVars: {
                        playsinline: 1,
                    },
                    events: {
                        onStateChange: onPlayerStateChange,
                    },
                });
            }
        } else if (isValidUrl(iframeUrl) === true && videoType == "HTML") {
            if (matchYoutubeUrl(iframeUrl) === true) {
                videoId = youtubeParser(iframeUrl);
                player = new YT.Player(id, {
                    videoId: videoId,
                    playerVars: {
                        playsinline: 1,
                    },
                    events: {
                        onStateChange: onPlayerStateChange,
                    },
                });
            }
        }
    }
}
// The API calls this function when the player's state changes.
// The function indicates that when playing a video (state=1)
function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
        let videoTitle = event.target.getVideoData().title.replaceAll("'", "");
        let embededVideoDataValues = $("#input-analytics-start").data().trackingClickInfoValue;
        let jsonStr = embededVideoDataValues.replace(/'/g, '"');
        let json = JSON.parse(jsonStr);
        json.videoName = videoTitle;
        let updatedValues = JSON.stringify(json).replace(/"/g, "'");
        $("#input-analytics-start").attr("data-tracking-click-info-value", updatedValues);
        $("#input-analytics-start").trigger("click");
    }

    if (event.data == YT.PlayerState.ENDED) {
        let videoTitle = event.target.getVideoData().title.replaceAll("'", "");
        let embededVideoDataValues = $("#input-analytics-complete").data().trackingClickInfoValue;
        let jsonStr = embededVideoDataValues.replace(/'/g, '"');
        let json = JSON.parse(jsonStr);
        json.videoName = videoTitle;
        let updatedValues = JSON.stringify(json).replace(/"/g, "'");
        $("#input-analytics-complete").attr("data-tracking-click-info-value", updatedValues);
        $("#input-analytics-complete").trigger("click");
    }
}
function stopVideo() {
    player.stopVideo();
}
