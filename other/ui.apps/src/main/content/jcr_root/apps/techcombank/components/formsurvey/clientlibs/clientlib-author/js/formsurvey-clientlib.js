(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: '.coral-Form-field[name="./minimumNumberOfAnswersAlert"]',
        validate: function (element) {
            let $minimumNumberOfAnswersAlert = $(element);
            let $minimumNumberOfAnswers = $minimumNumberOfAnswersAlert.closest('coral-panel-content').find('input[name="./minimumNumberOfAnswers"]');   
            
            if ($minimumNumberOfAnswers.val() && !$minimumNumberOfAnswersAlert.val())
                return 'Minimum Number Of Answers Alert is required';

            return;
        }
    });


    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./formSurveyItemList"]',
        validate: function(element) {
            let $answerListContainer = $(element);
            let $answerType = $answerListContainer.closest('coral-panel-content').find('select[name="./answerType"]');
            if ($answerType.val() == 'score-value') {
                let scoreInvalid = false;
                $answerListContainer.find('coral-multifield-item').each(function () {
                    let $answerItem = $(this);
                    let $value = $answerItem.find('input[name*="./value"]');
                    console.log($value.val());
                    if (isNaN($value.val())) {
                        scoreInvalid = true;
                    }
                });
                if (scoreInvalid)
                    return "Score value must be a number";
            }

            return;
        }
    });

    $.validator.register("foundation.validation.validator", {
        selector: '.coral-Select-select[name="./surveyType"]',
        validate: function(element) {
            let $surveyTypeElement = $(element);
            let $answerDisplayDirectionElement = $surveyTypeElement.closest('coral-panel-content').find('select[name="./answerDisplayDirection"]').parent().parent();
            let $minimumNumberOfAnswersElement = $surveyTypeElement.closest('coral-panel-content').find('coral-numberinput[name="./minimumNumberOfAnswers"]').parent();

            if ($surveyTypeElement.val() == 'single') {
                $answerDisplayDirectionElement.css('display', 'none');
                $minimumNumberOfAnswersElement.css('display', 'none');
            } else {
                $answerDisplayDirectionElement.css('display', '');
                $minimumNumberOfAnswersElement.css('display', '');
            }

            return;
        }
    });
})($, $(document));