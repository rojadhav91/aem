(function ($, $document) {
    "use strict";

    const nameSelector = ".form-hidden-name-dropdown";
    const valueSelector = ".form-hidden-value-dropdown";
    const valueHiddenSelector = ".form-hidden-value-dropdown-hidden";
    const acsGenericListPrefixUri = "/mnt/acs-commons/lists/";


    $document.on("dialog-ready", function (e) {
        setTimeout(() => {
          setValueDropdown(true);
        }, "500");
    });

    $document.on("change", nameSelector, function (e) {
        setValueDropdown(false);
    });

    function setValueDropdown(preSelect) {
        const nameDropdown = document.querySelector(nameSelector);
        const valueDropdown = document.querySelector(valueSelector);

        if (nameDropdown && valueDropdown) {
            //  Remove existing items from dropdown
            valueDropdown.items.clear();
            
            var url = acsGenericListPrefixUri + nameDropdown.value + ".json";
            $.get(url, function (data) {
                updateValueDropdownField(preSelect, data.options);
            });
        }
    }

    function updateValueDropdownField(preSelect, data) {
        const valueDropdown = document.querySelector(valueSelector);
        const valueDropdownValue = document.querySelector(valueHiddenSelector).value;

        for (var i in data) {
            valueDropdown.items.add({
                value: data[i].value,
                content: { innerHTML: data[i].title },
                selected:
                    preSelect && valueDropdownValue === data[i].value ? true : false,
            });
        }
    }
})($, $(document));
