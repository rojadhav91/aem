(function ($, $document) {
  'use strict';
  $.validator.register('foundation.validation.validator', {
    selector: 'coral-multifield[data-granite-coral-multifield-name="./usefulLinksItemList"]',
    validate: function () {
      let count = $(
        'coral-multifield[data-granite-coral-multifield-name="./usefulLinksItemList"] > coral-multifield-item',
      ).length;
      if (!count || count > 4) {
        return 'Min = 1 && Max = 4';
      }
    },
  });
})($, $(document));
