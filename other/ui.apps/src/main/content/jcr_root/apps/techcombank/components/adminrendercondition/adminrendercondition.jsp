<%@include file="/libs/granite/ui/global.jsp" %>
<%
%>
<%@page session="false"
        import="org.apache.sling.api.resource.ResourceResolver,
                javax.jcr.RepositoryException,
                javax.jcr.Session,
                org.apache.jackrabbit.api.security.user.User,
                org.apache.jackrabbit.api.security.user.UserManager,
                com.adobe.granite.ui.components.rendercondition.RenderCondition,
                com.adobe.granite.ui.components.Config,
                java.util.Iterator,
                org.apache.jackrabbit.api.security.user.Authorizable,
                org.apache.jackrabbit.api.security.user.Group,
                com.adobe.granite.ui.components.rendercondition.RenderCondition" %>
<%
    final String group = "administrators";
    final ResourceResolver resolver = slingRequest.getResourceResolver();
    request.setAttribute(RenderCondition.class.getName(), new RenderCondition() {
        public boolean check() {
            Session userSession = resolver.adaptTo(Session.class);
            try {
                UserManager userManager = resolver.adaptTo(UserManager.class);
                User currentUser = (User) userManager.getAuthorizable(userSession.getUserID());
                if (currentUser.isAdmin()) {
                    return true;
                }

                Authorizable auth = userManager.getAuthorizable(userSession.getUserID());
                Iterator<Group> groups = auth.memberOf();
                while (groups.hasNext()) {
                    Group g = (Group) (groups.next());
                    if (g.getID().contains(group)) {
                        return true;
                    }
                }

                return false;
            } catch (RepositoryException e) {
                return false;
            }
        }
    });
%>