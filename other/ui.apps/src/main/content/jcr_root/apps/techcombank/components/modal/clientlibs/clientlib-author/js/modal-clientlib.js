(function (document, $) {
	"use strict";
	$(document).on('foundation-contentloaded', function (e) { //event fires when dialog loads
        // 1. find Modal's dialog
        let $modalDialogForm = $('input[value="techcombank/components/modal"]').closest('form');
        if ($modalDialogForm.length) {
            // 2. find Modal's ID field
            let $idInput = $modalDialogForm.find('.coral-Form-field[name="./id"]');            
            // set readonly
            $idInput.prop('readonly', true);
            // generate value if not set
            let id_current = $idInput.val();            
            if (!id_current) {
                $idInput.val('modal-' + moment().format('YYYYMMDDHHmmssSSS'));
            }
            // 3. add Copy ID button
            $idInput.parent().append(
                `<button id="copyModalId">Copy Modal ID</button>`
            );
            let $copyModalIdBtn = $('#copyModalId');
            $copyModalIdBtn.on('click', function (e) {
                e.stopPropagation();
                e.preventDefault();
                if ($modalDialogForm.length) {
                    // copy to clipboard
                    navigator.clipboard.writeText($idInput.val());
                    $copyModalIdBtn.text("Copied!");
                    // animation show "Copied!" during 1 seconds
                    setTimeout(function () {
                        $copyModalIdBtn.text("Copy Modal ID");
                    }, 1000);
                }
            });
        }
	});
	
})(document, Granite.$);