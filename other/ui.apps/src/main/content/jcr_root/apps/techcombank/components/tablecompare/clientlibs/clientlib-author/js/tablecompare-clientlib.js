(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./tableCompareCardList"]',
        validate: function() {
            let count = $('coral-multifield[data-granite-coral-multifield-name="./tableCompareCardList"] > coral-multifield-item').length;
            if(count > 5){
                return "Number of cards must be less than 5";
            }
        }
    });
})($, $(document));