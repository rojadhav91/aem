/**
 * How to use:
 *
 * 1. Add class to checkbox
 *
 *		Example: granite:class="cq-dialog-checkbox-showhide"
 *
 * 2. Add cq-dialog-checkbox-showhide-target data-attribute to checkbox with the value being the selector to target for toggleing
 *
 *		Example: cq-dialog-checkbox-showhide-target=".togglefield"
 *
 * 3. Add target class to toggleable fields or components
 *
 *	    Example: granite:class="togglefield"
 */
(function(document, $) {
	"use strict";

	// when dialog gets injected
	$(document).on("foundation-contentloaded", function(e) {
		// if there is already an inital value make sure the according target element becomes visible
		checkboxShowHideHandler($(".cq-dialog-checkbox-showhide", e.target), true);
	});

	$(document).on("change", ".cq-dialog-checkbox-showhide", function(e) {
		checkboxShowHideHandler($(this), false);
	});

	function checkboxShowHideHandler(el, onload) {
		el.each(function(i, element) {
			var isChecked = element.checked;
			if (onload && isChecked) {
				showHide(element);
			} else if (!onload) {
				showHide(element);
			}
		})
	}

	function showHide(element) {
		// get the selector to find the target elements. its stored as data-.. attribute
		var target = $(element).data("cqDialogCheckboxShowhideTarget");
		var $target = $(target);
		if (target) {
			$target.each(function(i, element) {
				if ($(element).hasClass("hide")) {
					$(element).removeClass("hide");
					$(element).addClass("show");
				} else if ($(element).hasClass("show")) {
					$(element).removeClass("show");
					$(element).addClass("hide");
				}
			});
		}
	}
})(document, Granite.$);