<form id="form">
    <div class="form-row">
            <h4 acs-coral-heading class="coral-Heading coral-Heading--4">CSV File</h4>
            <span>
                <input
                        accept="*/*"
                        type="file"
                        name="csv"
                        ngf-select
                        required
                        placeholder="Select the Categories CSV file"/>
            </span>
    </div>
    <div class="form-row">
        <h4 acs-coral-heading class="coral-Heading coral-Heading--4">Content Fragment Parent Path</h4>
        <span>
            <input type="text"
                   name="parentPath"
                   class="coral-Textfield"
                   required="true"
                   placeholder=""
            value="/content/dam/techcombank"/>
        </span>
    </div>
    <div class="form-row">
        <h4 acs-coral-heading class="coral-Heading coral-Heading--4">Model Type</h4>
        <span>
            <select required="true" name="modelType" class="coral-Textfield">
                <option value="atm-cdm-fragment">ATM CDM Fragment</option>
                <option value="branch-fragment">Branch Fragment</option>
                <option value="card-detail-fragment">Credit/Debit Card Detail Content Fragment</option>
                <option value="compare-panel-fragment">Compare Panel Fragment</option>
                <option value="event-detail-content-fragment">Event Detail Content Fragment</option>
                <option value="exchange-rate-fragment">Exchange Rate Fragment</option>
                <option value="faqmodel">FAQ Content Fragment</option>
                <option value="fixing-rate-fragment">Fixing Rate Fragment</option>
                <option value="gold-rate-fragment">Gold Rate Fragment</option>
                <option value="offer-listing-fragment">Offer Listing Fragment</option>
                <option value="other-rate-fragment">Other Rate Fragment</option>
                <option value="tenor-int-rate-fragment">Tenor Int Rate Fragment</option>
                <option value="tenor-rate-fragment">Tenor Rate Fragment</option>
            </select>
        </span>
    </div>
    <div class="form-row">
        <div class="form-left-cell">&nbsp;</div>
        <button onclick="createContentFragment();return false;" class="coral-Button coral-Button--primary">Submit</button>
    </div>
</form>




