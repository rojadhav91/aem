(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./listTilesItems"]',
        validate: function(element) {
            let $listTilesItems = $(element).find('coral-multifield-item');
            let linkUrlValid = true;
            $listTilesItems.each(function () {
                let $listTilesItem = $(this);
                let $linkText = $listTilesItem.find('input[name*="./linkText"]');
                let $linkUrl = $listTilesItem.find('input[name*="./linkUrl"]');
                if ($linkText.val() && !$linkUrl.val()) {
                    $linkUrl.adaptTo("foundation-field").setInvalid(true);
                    linkUrlValid = false;
                }
            });
            if (!linkUrlValid) {
                return "Must author link url if authored link text."
            } else {
                return;
            }
        }
    });
})($, $(document));