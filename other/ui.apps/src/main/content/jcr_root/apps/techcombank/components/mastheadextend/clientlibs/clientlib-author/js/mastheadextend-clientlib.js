(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./mastheadExtendImageList"]',
        validate: function() {
            let count = $('coral-multifield[data-granite-coral-multifield-name="./mastheadExtendImageList"] > coral-multifield-item').length;
            if(count != 4){
                return "There must be 4 masthead extend images";
            }
        }
    });
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./mastheadExtendCardArticleImageList"]',
        validate: function() {
            let count = $('coral-multifield[data-granite-coral-multifield-name="./mastheadExtendCardArticleImageList"] > coral-multifield-item').length;
            if(count != 3){
                return "There must be 3 masthead extend card article images";
            }
        }
    });
})($, $(document));