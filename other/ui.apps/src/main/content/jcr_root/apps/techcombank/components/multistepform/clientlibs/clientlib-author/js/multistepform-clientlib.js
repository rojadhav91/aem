(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: '.coral-Form-field[name="./formId"]',
        validate: function (element) {
            let $formId = $(element);
            let val = $formId.val();
            if (typeof val !== 'string') {
                return 'It must be a string.'
            }
            const pattern = /(\w+)-(\w)([\w-]*)/
            if (!pattern.test(val) || val.includes('_'))
                return 'Following Kebab Case Format';
            return;
        }
    });
})($, $(document));