(function ($, $document) {
    "use strict";
    $.validator.register("foundation.validation.validator", {
        selector: 'coral-multifield[data-granite-coral-multifield-name="./listStepItems"]',
        validate: function(element) {
            let $multifield = $(element).find('coral-multifield-item');
            let isValid = true;
            let layout = $multifield.closest('form').find('select[name="./layout"]').val();
            $multifield.each(function () {
                let $item = $(this);
                let $image = $item.find('foundation-autocomplete[name*="./image"]').find('input');
                let $altText = $item.find('input[name*="./altText"]');
                if (
                    (layout == 'device-image-step-text' || layout == 'image-step-text')
                    && (!$image.val() && !$altText.val())) {
                    isValid = false;
                }
            });
            if (!isValid) {
                return "Image or Alt text required."
            } else {
                return;
            }
        }
    });
})($, $(document));