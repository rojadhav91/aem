(function ($document, $, Coral) {
    $document.on('dialog-ready', function (e) {
        Coral.commons.ready(function (dialog) {
            updateViewMoreViewLessFields()
        });
        $('coral-checkbox[name="./viewMoreButton"]', e.target).each(function (i, element) {
            Coral.commons.ready(element, function (dialog) {
                $(dialog).on('change', function (event) {
                    updateViewMoreViewLessFields()
                });
            });
        });

        function updateViewMoreViewLessFields() {
            let $viewMoreLabelInput = $('input[name="./viewMoreLabel"]');
            let $viewLessLabelInput = $('input[name="./viewLessLabel"]');
            if ($('coral-checkbox[name="./viewMoreButton"]').attr('checked') == 'checked') {
                $viewMoreLabelInput.prop('required', true);
                $viewLessLabelInput.prop('required', true);
                $viewMoreLabelInput.parent().removeClass('hide');
                $viewLessLabelInput.parent().removeClass('hide');
            }
            else {
                $viewMoreLabelInput.prop('required', false);
                $viewLessLabelInput.prop('required', false);
                $viewMoreLabelInput.parent().addClass('hide');
                $viewLessLabelInput.parent().addClass('hide');
            }
        }
    });
})($(document), Granite.$, Coral);
