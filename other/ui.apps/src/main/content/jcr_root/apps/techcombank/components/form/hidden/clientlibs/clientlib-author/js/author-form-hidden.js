(function ($, document, ns) {
    $(document).on("dialog-ready", function() {
        const showHide = function(e) {
            if($("[name='./checkboxValueText']").prop("checked")) {
                $("coral-select[name='./value']").closest(".coral-Form-fieldwrapper").hide();
                $("input[name='./valueText']").closest(".coral-Form-fieldwrapper").show();
            } else {
                $("input[name='./valueText']").closest(".coral-Form-fieldwrapper").hide();
                $("coral-select[name='./value']").closest(".coral-Form-fieldwrapper").show();
            }
        };
        $(document).on("change", "[name='./checkboxValueText']", showHide);
        showHide();
    });
})(Granite.$, document, Granite.author);
