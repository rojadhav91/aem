(function($, $document) {
    "use strict";
    $document.on("dialog-ready", function() {
           let registry = $(window).adaptTo("foundation-registry");
           // Validator for required for multifield max and min items
           registry.register("foundation.validation.validator", {
               selector: "[data-validation=addItemsRestrict]",
               validate: function(element) {
                   let el = $(element);
                   let max=el.data("max-items");
                   let min=el.data("min-items");
                   let items=el.children("coral-multifield-item").length;
                   let domitems=el.children("coral-multifield-item");
                   if(items>max){
                     /* Use below line if you don't want to add item in multifield more than max limit */
                     domitems.last().remove();
                     return "You can add maximum "+max+" items.";
                   }
                   if(items<min){
                       return "You add minimum "+min+" items.";
                   }
               }
           });
   });
})($, $(document));
