package com.techcombank.core.service.impl;



import com.adobe.acs.commons.notifications.InboxNotification;

import com.adobe.acs.commons.notifications.InboxNotificationSender;

import com.adobe.granite.taskmanagement.TaskManagerException;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;

import com.techcombank.core.constants.TCBConstants;

import com.techcombank.core.service.EmailService;

import com.techcombank.core.service.WorkflowNotificationService;

import org.apache.commons.lang.StringUtils;

import org.apache.jackrabbit.api.security.user.Authorizable;

import org.apache.jackrabbit.api.security.user.UserManager;

import org.apache.sling.api.resource.ResourceResolver;

import org.osgi.service.component.annotations.Component;

import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;



import javax.jcr.RepositoryException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component(service = WorkflowNotificationService.class)

public class WorkflowNotificationServiceImpl implements WorkflowNotificationService {

    @Reference
    private EmailService emailService;

    @Reference
    private InboxNotificationSender inboxNotificationSender;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void inboxNotification(final ResourceResolver resolver, final String initiator, final String emailTitle,
                                  final String emailBody, final String payload) {
        InboxNotification inboxNotification = inboxNotificationSender.buildInboxNotification();
        inboxNotification.setAssignee(initiator);
        inboxNotification.setTitle(emailTitle);
        inboxNotification.setMessage(emailBody);
        inboxNotification.setContentPath(payload);
        try {
            inboxNotificationSender.sendInboxNotification(resolver, inboxNotification);
        } catch (TaskManagerException e) {
            logger.error("Exception in sending notification {} {}", e.getMessage(), e);
        }
    }

    public void emailNotifications(final String payload, final String workflowTitle, final String approverName,
                                   final String subject, final String emailTitle, final String email) {
        String[] recipients = {email};
        StringBuilder sb = new StringBuilder();
        sb.append("<p> " + TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle + " </p><br/>");
        sb.append("<p>" + subject + " " + approverName + " </p>");
        sb.append("<p> Resource Path : " + payload + "</p>");
        emailService.sendEmail(emailTitle, sb.toString(), recipients);
    }

    @Override
    public void sendNotificationAndEmail(final ResourceResolver resourceResolver, final MetaDataMap wfMetaData,
                                         final String payload, final String initiator, final String approverName,
                                         final String subject, final String emailTitle) {
        boolean mailEnable = false;
        if (wfMetaData.containsKey(TCBConstants.EMAIL_ENABLE)) {
            mailEnable = wfMetaData.get(TCBConstants.EMAIL_ENABLE, Boolean.class);
        }
        String workflowTitle = StringUtils.EMPTY;
        if (wfMetaData.containsKey(TCBConstants.WORKFLOW_TITLE)) {
            workflowTitle = wfMetaData.get(TCBConstants.WORKFLOW_TITLE, String.class);
        }
        String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + workflowTitle;
        if (StringUtils.isNotBlank(initiator)) {
            inboxNotification(resourceResolver, initiator, emailTitle, emailBody, payload);
        }
        String email = wfMetaData.get(TCBConstants.USER_EMAIL, String.class);
        try {
            if (StringUtils.isBlank(email)) {
                UserManager manager = resourceResolver.adaptTo(UserManager.class);
                Authorizable authorizable = manager.getAuthorizable(initiator);
                if (authorizable.hasProperty("./profile/email")) {
                    email = Arrays.toString(authorizable.getProperty("./profile/email"));
                }
            }
        } catch (RepositoryException repoExp) {
            logger.error("Repository Exception occurred while getting initiator email {}", repoExp.getMessage());
        }
        if (mailEnable && StringUtils.isNotBlank(email)) {
            emailNotifications(payload, workflowTitle, approverName, subject, emailTitle, email);
        }
    }

    @Override
    public String getUserComment(final WorkItem workItem, final WorkflowSession workflowSession) {
        String comment = StringUtils.EMPTY;
        List<HistoryItem> historyList = null;
        try {
            historyList = workflowSession.getHistory(workItem.getWorkflow());
        } catch (WorkflowException wrkExp) {
            logger.error("Workflow Exception occurred while getting user comment {}", wrkExp.getMessage());
        }

        int listSize = Objects.requireNonNull(historyList).size();
        if (listSize > 0) {
            HistoryItem lastItem = historyList.get(listSize - 1);
            comment = lastItem.getComment();
        }
        return comment;
    }


}
