package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class TableCompareColumnHeaderModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableCompareColumnHeaderModel {

    /**
     * Column Header
     * - Description: To author column header
    */
    @ValueMapValue
    @Getter
    private String columnHeader;

}
