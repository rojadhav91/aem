package com.techcombank.core.models.impl;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.webservicesupport.ConfigurationConstants;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.RecaptchaModel;
import com.techcombank.core.service.RecaptchaService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * This model class is for Recaptcha Model
 */
@Model(adaptables = {SlingHttpServletRequest.class}, adapters = {
        RecaptchaModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        RecaptchaModelImpl.RESOURCE_TYPE)
public class RecaptchaModelImpl implements RecaptchaModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/proxy/form/recaptcha";

    @OSGiService
    private RecaptchaService recaptchaService;

    @ScriptVariable
    private Page currentPage;
    /**
     * The siteKey value.
     */
    private String siteKey;
    /**
     * The requiredMessage value.
     */
    @ValueMapValue
    @Via("resource")
    private String requiredMessage;
    /**
     * The isRequired value.
     */
    @ValueMapValue
    @Via("resource")
    private boolean isRequired;

    /**
     * The SecretKey value.
     */
    private String secretKey;

    @PostConstruct
    protected void init() {
        if (Objects.nonNull(currentPage)) {
            HierarchyNodeInheritanceValueMap pageProperties =
                    new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
            String[] allServices = pageProperties.getInherited(ConfigurationConstants.PN_CONFIGURATIONS, new String[0]);

            String configPath = Arrays.stream(allServices)
                    .filter(serviceName -> serviceName.startsWith(TCBConstants.RECAPTCHA)).findFirst()
                    .orElse(null);
            if (Objects.nonNull(configPath)) {
                Optional<String> siteKeyValue = recaptchaService.getSiteKey(configPath);
                siteKey = ((Optional<?>) siteKeyValue).isPresent()
                        ? siteKeyValue.get()
                        : StringUtils.EMPTY;
                Optional<String> secretKeyValue = recaptchaService.getSecretKey(configPath);
                secretKey = ((Optional<?>) secretKeyValue).isPresent()
                        ? secretKeyValue.get()
                        : StringUtils.EMPTY;
            }
        }

    }

    /**
     * @return the siteKey
     */
    @Override
    public String getSiteKey() {
        return siteKey;
    }

    /**
     * @return the secretKey
     */
    @Override
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * @return the requiredMessage
     */
    @Override
    public String getRequiredMessage() {
        return requiredMessage;
    }

    @Override
    public boolean isRequired() {
        return isRequired;
    }
}
