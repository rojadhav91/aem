package com.techcombank.core.models;

/**
 * Interface For NestedDropdownModel
 */
public interface NestedDropdownModel {
    /**
     * The fieldLabel.
     */
    String getFieldLabel();
    /**
     * The fieldName.
     */
    String getFieldName();

    /**
     * The placeholderText.
     */
    String getPlaceholderText();

    /**
     * The requiredMessage.
     */
    String getRequiredMessage();

    /**
     * The item.
     */
    String getItem();

    /**
     * The isAnalytics.
     */
    boolean isAnalytics();

}
