package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.models.EventListingModel;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Objects;

/**
 * This model class is for EventListingModelImpl Model
 */
@Model(adaptables = Resource.class, adapters = {
        EventListingModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        EventListingModelImpl.RESOURCE_TYPE)
@Getter
public class EventListingModelImpl implements EventListingModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/eventlist";

    /**
     * The ResourceResolver.
     */
    @Inject
    private ResourceResolver resolver;

    /**
     * The tagRootPath value.
     */
    @ValueMapValue
    private String tagRootPath;

    /**
     * The eventsCFRootPath value.
     */
    @ValueMapValue
    private String eventsCFRootPath;

    /**
     * The eventTypeLabel value.
     */
    @ValueMapValue
    private String eventTypeLabel;

    /**
     * The showMeLabel value.
     */
    @ValueMapValue
    private String showMeLabel;

    /**
     * The selectDateLabel value.
     */
    @ValueMapValue
    private String selectDateLabel;

    /**
     * The filterLabel value.
     */
    @ValueMapValue
    private String filterLabel;
    /**
     * The applyFilterLabel value.
     */
    @ValueMapValue
    private String applyFilterLabel;
    /**
     * The upcomingEventsLabel value.
     */
    @ValueMapValue
    private String upcomingEventsLabel;
    /**
     * The pastEventsLabel value.
     */
    @ValueMapValue
    private String pastEventsLabel;
    /**
     * The viewMoreLabel value.
     */
    @ValueMapValue
    private String viewMoreLabel;

    /**
     * The tagIds value.
     */
    private String tags;
    /**
     * The gsonObj value.
     */
    private Gson gsonObj;
    /**
     * The JsonArray value.
     */
    private JsonArray jsonAry;

    @PostConstruct
    void init() {
        gsonObj = new Gson();
        jsonAry = new JsonArray();
        if (StringUtils.isNotEmpty(tagRootPath) && StringUtils.isNotEmpty(eventsCFRootPath)
                && Objects.nonNull(resolver)) {
            final TagManager tagManager = resolver.adaptTo(TagManager.class);
            Tag rootTag = Objects.requireNonNull(tagManager).resolve(tagRootPath);
            if (Objects.nonNull(rootTag)) {
                filterEventListingTags(rootTag, tagManager);
            }
        }
        tags = gsonObj.toJson(jsonAry);
    }

    private void filterEventListingTags(final Tag rootTag, final TagManager tagManager) {
        Resource resource = resolver.getResource(rootTag.getPath());
        if (Objects.requireNonNull(resource).hasChildren()) {
            for (Resource value : Objects.requireNonNull(resource).getChildren()) {
                Tag tag = Objects.requireNonNull(tagManager).resolve(value.getPath());
                if (tagManager.find(eventsCFRootPath, new String[] {tag.getTagID()}).getSize() > 0) {
                    JsonObject jsonObj = new JsonObject();
                    jsonObj.add("title", gsonObj.toJsonTree(tag.getTitle()));
                    jsonObj.add("id", gsonObj.toJsonTree(tag.getTagID()));
                    jsonAry.add(jsonObj);
                }
            }
        }
    }
}
