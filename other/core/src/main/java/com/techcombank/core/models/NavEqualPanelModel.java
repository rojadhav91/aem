package com.techcombank.core.models;

import java.util.List;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import com.techcombank.core.models.impl.NavPanelModelImpl;

import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class NavEqualPanelModel used for mapping Properties of Nav Equal Panel
 * Component.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
resourceType = NavEqualPanelModel.RESOURCE_TYPE)
public class NavEqualPanelModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/navequalpanel";

    /**
     * The List for Nav Panel Section Items.
     */
    @ChildResource
    @Getter
    private List<NavPanelModelImpl> navPanelSection;

}
