package com.techcombank.core.service;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.pojo.Articles;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.List;

public interface ArticlesRelatedPostService {

    /**
     * Service to obtain related posts
     * @param resource
     * @param page
     * @param resourceResolver
     * @return
     */
    List<Articles> getArticlesRelatedPosts(Resource resource, Page page, ResourceResolver resourceResolver);
}
