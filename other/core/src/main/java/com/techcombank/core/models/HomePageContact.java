
package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * HomePageContact is a SlingModel that represents a contact on the homepage.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HomePageContact {

    /**
     * The text for the contact.
     *
     */
    @ValueMapValue
    @Getter
    private String contactText;

    /**
     * The phone number for the contact.
     *
     */
    @ValueMapValue
    @Getter
    private String number;

}
