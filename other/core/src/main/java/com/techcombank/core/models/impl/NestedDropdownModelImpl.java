package com.techcombank.core.models.impl;

import com.techcombank.core.models.NestedDropdownModel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This model class is for NestedDropdown Model
 */
@Model(adaptables = Resource.class, adapters = {NestedDropdownModel.class}, defaultInjectionStrategy =
        DefaultInjectionStrategy.OPTIONAL, resourceType = NestedDropdownModelImpl.RESOURCE_TYPE)
@Getter
public class NestedDropdownModelImpl implements NestedDropdownModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/form/nesteddropdown";

    /**
     * The fieldLabel value.
     */
    @ValueMapValue
    private String fieldLabel;

    /**
     * The fieldName value.
     */
    @ValueMapValue
    private String fieldName;

    /**
     * The placeholderText value.
     */
    @ValueMapValue
    private String placeholderText;

    /**
     * The requiredMessage value.
     */
    @ValueMapValue
    private String requiredMessage;

    /**
     * The item value.
     */
    @ValueMapValue
    private String item;

    /**
     * The analytics value.
     */
    @ValueMapValue
    private boolean analytics;
}
