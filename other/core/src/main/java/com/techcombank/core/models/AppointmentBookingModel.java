package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This model class is for mapping Appointment Booking details
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
resourceType = AppointmentBookingModel.RESOURCE_TYPE)
public class AppointmentBookingModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/appointmentbooking";

    /**
     * checks to enable tabs for book a visit flow.
     */
    @ValueMapValue
    @Getter
    private boolean enableTabs;

    /**
     * The Text for the identifier label.
     */
    @ValueMapValue
    @Getter
    private String identifierLabel;

    /**
     * The Text for the personal label
     */
    @ValueMapValue
    @Getter
    private String personalLabel;

    /**
     * The Text for the business label.
     */
    @ValueMapValue
    @Getter
    private String businessLabel;

    /**
     * The Text for the action Label.
     */
    @ValueMapValue
    @Getter
    private String actionLabel;

    /**
     * The Text for the branch label.
     */
    @ValueMapValue
    @Getter
    private String branchLabel;

    /**
     * The Text for the city label.
     */
    @ValueMapValue
    @Getter
    private String cityLabel;

    /**
     * The Text for the district label.
     */
    @ValueMapValue
    @Getter
    private String districtLabel;

    /**
     * The Text for the branch List Label.
     */
    @ValueMapValue
    @Getter
    private String branchListLabel;

    /**
     * The Text for the today label.
     */
    @ValueMapValue
    @Getter
    private String todayLabel;

    /**
     * The Text for the tomorrow label.
     */
    @ValueMapValue
    @Getter
    private String tomorrowLabel;

    /**
     * The Text for the time slot label.
     */
    @ValueMapValue
    @Getter
    private String timeSlotLabel;

    /**
     * The Text for the title field.
     */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * The Text for the name label.
     */
    @ValueMapValue
    @Getter
    private String nameLabel;

    /**
     * The Text for the name placeholder field.
     */
    @ValueMapValue
    @Getter
    private String namePlaceHolder;

    /**
     * The Text for the email label.
     */
    @ValueMapValue
    @Getter
    private String emailLabel;

    /**
     * The Text for the email placeholder field.
     */
    @ValueMapValue
    @Getter
    private String emailPlaceHolder;

    /**
     * The Text for the phone label.
     */
    @ValueMapValue
    @Getter
    private String phoneLabel;

    /**
     * The Text for the phone placeholder field.
     */
    @ValueMapValue
    @Getter
    private String phonePlaceHolder;

    /**
     * The Text for the Account Tax label.
     */
    @ValueMapValue
    @Getter
    private String accTaxLabel;

    /**
     * The Text for the Account Tax placeholder field.
     */
    @ValueMapValue
    @Getter
    private String accTaxPlaceHolder;

    /**
     * To check recaptcha is enabled or not.
     */
    @ValueMapValue
    @Getter
    private boolean enableRecaptcha;

    /**
     * The Text for the cta label.
     */
    @ValueMapValue
    @Getter
    private String ctaLabel;

    /**
     * The Icon for Confirmation.
     */
    @ValueMapValue
    @Getter
    private String confirmationIcon;

    /**
     * The Alt Text for confirmation icon.
     */
    @ValueMapValue
    @Getter
    private String confirmationIconAlt;

    /**
     * The Text for the confirmation message.
     */
    @ValueMapValue
    @Getter
    private String confirmationMessage;

    /**
     * The Text for the booking message.
     */
    @ValueMapValue
    @Getter
    private String bookingMessage;

    /**
     * The Text for the ticket number label.
     */
    @ValueMapValue
    @Getter
    private String ticketNumberLabel;

    /**
     * The Text for the ticket notes.
     */
    @ValueMapValue
    @Getter
    private String ticketNotes;

    /**
     * The Text for account/tax required message .
     */
    @ValueMapValue
    @Getter
    private String accTaxRequired;

    /**
     * The Text for email validation required message.
     */
    @ValueMapValue
    @Getter
    private String emailRequired;

    /**
     * The Text for email validation invalid message.
     */
    @ValueMapValue
    @Getter
    private String emailInvalid;

    /**
     * The Text for phone number validation required message.
     */
    @ValueMapValue
    @Getter
    private String phoneRequired;

    /**
     * The Text for phone number validation invalid message.
     */
    @ValueMapValue
    @Getter
    private String phoneInvalid;

    /**
     * Returns the web image rendition path for the confirmation icon.
     */
    public String getConfirmationIconWebImagePath() {
        return PlatformUtils.getWebImagePath(confirmationIcon);
    }

    /**
     * Returns the mobile image rendition path for the confirmation icon.
     */
    public String getConfirmationIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(confirmationIcon);
    }

}
