package com.techcombank.core.models;

import com.techcombank.core.models.impl.CardListingModelImpl;
import java.util.List;

/**
 * Interface For CardListingModel
 */
public interface CardListingModel {
    /**
     * The getCardTagRoot.
     */
    String getCardTagRoot();
    /**
     * The getNudgeTagRoot.
     */
    String getNudgeTagRoot();
    /**
     * The getCardCFRootPath.
     */
    String getCardCFRootPath();
    /**
     * The getCtaCompLabel.
     */
    String getCtaCompLabel();
    /**
     * The getCtaCompLink.
     */
    String getCtaCompLink();
    /**
     * The getCtaCompTarget.
     */
    String getCtaCompTarget();
    /**
     * The isCtaCompFollow.
     */
    boolean isCtaCompFollow();
    /**
     * The getBannerImg.
     */
    String getBannerImg();
    /**
     * The getImageAlt.
     */
    String getImageAlt();
    /**
     * The getBannerTitle.
     */
    String getBannerTitle();
    /**
     * The getBannerDesc.
     */
    String getBannerDesc();
    /**
     * The getBannerLink.
     */
    String getBannerLink();
    /**
     * The isBannerTarget.
     */
    boolean isBannerTarget();
    /**
     * The isBannerFollow.
     */
    boolean isBannerFollow();
    /**
     * The getCardTypeLabel.
     */
    String getCardTypeLabel();
    /**
     * The getRegImg.
     */
    String getRegImg();
    /**
     * The getRegImgMbl.
     */
    String getRegImgMobile();
    /**
     * The getRegTitle.
     */
    String getRegTitle();
    /**
     * The getRegSubTitle.
     */
    String getRegSubTitle();
    /**
     * The getImageRegAlt.
     */
    String getImageRegAlt();
    /**
     * The getRegCtaMobile.
     */
    String getRegCtaMobile();
    /**
     * The getSteps.
     */
    List<CardListingModelImpl.RegisterSteps> getSteps();
    /**
     * The bottomText value.
     */
    String getBottomText();
    /**
     * The bottomLinkLabel value.
     */
    String getBottomLinkLabel();
    /**
     * The bottomLink value.
     */
    String getBottomLink();
    /**
     * The ctaBtmTarget value.
     */
    String getCtaBtmTarget();
    /**
     * The ctaBtmFollow value.
     */
    boolean isCtaBtmFollow();
    /**
     * The getCardTags.
     */
    String getCardTags();
    /**
     * The getNudgeTags.
     */
    String getNudgeTags();

}
