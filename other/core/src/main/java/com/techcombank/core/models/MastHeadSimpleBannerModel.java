package com.techcombank.core.models;

import java.util.List;

import javax.inject.Inject;
import lombok.Getter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class MastHeadSimpleBanner used for mapping Properties of MastHead Simple
 * Banner Component.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = MastHeadSimpleBannerModel.RESOURCE_TYPE)
public class MastHeadSimpleBannerModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/mastheadsimplebanner";

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    private Page currentPage;

    /**
     * The List for Tab Section Items.
     */
    @ChildResource
    @Getter
    private List<SimpleBannerTabSectionModel> tabSection;

    /**
     * The Image for background banner.
     */
    @ValueMapValue
    @Getter
    private String bgBannerImage;

    /**
     * The Image for Mobile background banner.
     */
    @ValueMapValue
    @Getter
    private String bgBannerMobileImage;

    /**
     * The Alt Text for the Banner Image.
     */
    @ValueMapValue
    @Getter
    private String bgBannerImgAlt;

    /**
     * The Text for highlighted Text field.
     */
    @ValueMapValue
    @Getter
    private String highlightedtext;

    /**
     * The Text for banner title.
     */
    @ValueMapValue
    @Getter
    private String bannerTitle;

    /**
     * Flag to check to wrap banner title.
     */
    @ValueMapValue
    @Getter
    private boolean wrapBannerTitle;

    /**
     * Text to banner description.
     */
    @ValueMapValue
    @Getter
    private String bannerDesc;

    /**
     * The Text for button Label.
     */
    @ValueMapValue
    @Getter
    private String buttonLabel;

    /**
     * The URL for button link.
     */
    @ValueMapValue
    private String buttonlinkURL;

    /**
     * To select the size of banner.
     */
    @ValueMapValue
    @Getter
    private String bannerType;

    /**
     * To select the alignment of text.
     */
    @ValueMapValue
    @Getter
    private String textAlignment;

    /**
     * To check the button link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String buttonTarget;

    /**
     * To check button seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean buttonNoFollow;

    /**
     * Display Search Box
     */
    @ValueMapValue
    @Getter
    private boolean displaySearchBox;

    /**
     * @return the buttonlinkURL
     */
    public String getButtonlinkURL() {
        return PlatformUtils.isInternalLink(resourceResolver, buttonlinkURL);
    }

    /**
     * @return the currentActivePage
     */
    public String getCurrentActivePage() {
        String currentPagePath = PlatformUtils.currentPagePathWithAlias(resourceResolver, currentPage.getPath());
        return PlatformUtils.getMapUrl(resourceResolver, currentPagePath);
    }

}
