package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class IllustratedPanelModel {

    @ValueMapValue
    @Getter
    private String beforeText;

    @ValueMapValue
    @Getter
    private String centerImage;

    @ValueMapValue
    @Getter
    private String centerImageMobile;

    @ValueMapValue
    @Getter
    private String altText;

    @ValueMapValue
    @Getter
    private String afterText;

    @ValueMapValue
    @Getter
    private String beforeLabel;

    @ValueMapValue
    @Getter
    private String afterLabel;

    @ValueMapValue
    @Getter
    private String bottomAlignment;

    @ValueMapValue
    @Getter
    private String icon;

    @ValueMapValue
    @Getter
    private String iconMobile;

    @ValueMapValue
    @Getter
    private String iconAlt;

    @ValueMapValue
    @Getter
    private String bottomText;

    @ValueMapValue
    @Getter
    private String valueText;

    @ValueMapValue
    @Getter
    private String valueAmount;

    public String getCenterImage() {
        return PlatformUtils.getWebImagePath(centerImage);
    }

    public String getCenterImageMobile() {
        return PlatformUtils.getMobileImagePath(centerImage);
    }

    public String getIcon() {
        return PlatformUtils.getWebImagePath(icon);
    }

    public String getIconMobile() {
        return PlatformUtils.getMobileImagePath(icon);
    }
}
