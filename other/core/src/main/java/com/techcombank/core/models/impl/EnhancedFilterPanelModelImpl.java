package com.techcombank.core.models.impl;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

public class EnhancedFilterPanelModelImpl {

    /**
     * The Experience Fragment value.
     */
    @ChildResource
    private List<EnhancedFilterPanelModelImpl.ExpFragments> expFragments;

    @Getter
    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    public static class ExpFragments {
        /**
         * The path value.
         */
        @ValueMapValue
        private String path;

    }
}
