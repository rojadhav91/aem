package com.techcombank.core.service;

import org.apache.sling.api.resource.ResourceResolver;

import java.util.List;
import java.util.Map;

public interface StoryListing {
     Map<String, String> generateQuery(String rootPath,
                                       List<String> articleCategory, List<String> articleTag,
                                       String resultCount, String offset);
     String getResult(ResourceResolver resourceResolver, Map<String, String> queryMap);
}
