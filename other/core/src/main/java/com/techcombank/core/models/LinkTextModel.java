package com.techcombank.core.models;

import java.util.List;

import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.Model;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class LinkTextModel.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = HeaderModel.RESOURCE_TYPE)
public class LinkTextModel {

    /** The link text type */
    @ValueMapValue
    @Getter
    private String imagePosition;

    /** The link text view */
    @ValueMapValue
    @Getter
    private String linkTextView;

    /** The cards number */
    @ValueMapValue
    @Getter
    private int cardsNumber;

    /** The arrow option */
    @ValueMapValue
    @Getter
    private boolean arrow;

    /** The view more label */
    @ValueMapValue
    @Getter
    private String viewMoreLabel;

    /**
     * Link text images.
     */
    @ChildResource
    @Getter
    private List<LinkTextItemModel> linkTextItems;

    public int calculateColumnsNumber() {
        int columnsNumber = 1;
        if (linkTextView.equals("vertical")) {
            columnsNumber = (int) Math.ceil((double) linkTextItems.size() / cardsNumber);
        }
        if (linkTextView.equals("horizontal")) {
            columnsNumber = cardsNumber;
        }
        return columnsNumber;
    }

    public boolean calculatePosition() {
        int linkTextItemIndex = 1;
        int row = 0;
        int column = 0;
        if (linkTextView.equals("vertical")) {
            for (LinkTextItemModel linkTextItemModel : linkTextItems) {
                row = (linkTextItemIndex % cardsNumber == 0) ? cardsNumber : (linkTextItemIndex % cardsNumber);
                column = (int) Math.ceil((double) linkTextItemIndex / cardsNumber);
                linkTextItemModel.setRow(row);
                linkTextItemModel.setColumn(column);
                linkTextItemIndex++;
            }
        }
        if (linkTextView.equals("horizontal")) {
            for (LinkTextItemModel linkTextItemModel : linkTextItems) {
                column = (linkTextItemIndex % cardsNumber == 0) ? cardsNumber : (linkTextItemIndex % cardsNumber);
                row = (int) Math.ceil((double) linkTextItemIndex / cardsNumber);
                linkTextItemModel.setRow(row);
                linkTextItemModel.setColumn(column);
                linkTextItemIndex++;
            }
        }
        return true;
    }
}
