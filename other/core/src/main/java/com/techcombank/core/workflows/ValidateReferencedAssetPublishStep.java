package com.techcombank.core.workflows;

import com.adobe.granite.workflow.PayloadMap;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.commons.ReferenceSearch;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component(service = WorkflowProcess.class, property = {"process.label = Validate Referenced Asset Publish"})
public class ValidateReferencedAssetPublishStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<String> unPublishedAsset;

    private boolean pageOnly = false;

    @Reference
    private WorkflowNotificationService workflowNotificationService;
    private boolean isUnPublish = false;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {
        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Validate Referenced Asset Publish {} ", payload);
        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class);
        String processArgs = metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class);
        processUnPublishWorkflowArgs(processArgs);
        PayloadMap payloadMap = Objects.requireNonNull(resourceResolver).adaptTo(PayloadMap.class);
        int workflowListSize = Objects.requireNonNull(payloadMap).getWorkflowInstances(payload, true).size();
        if (StringUtils.startsWith(payload, TCBConstants.VAR) || StringUtils.startsWith(payload, TCBConstants.ETC)) {
            String resPath = payload.concat(TCBConstants.VAR_FILTER);
            Iterator<Resource> resItr =
                    Objects.requireNonNull(resourceResolver.getResource(resPath)).getChildren().iterator();
            unPublishedAsset = new ArrayList<>();
            workflowListSize =
                    multiResourcePayloadIterator(workItem, workflowSession, resItr, workflowListSize, payloadMap,
                            resourceResolver, workflowMetadataMap);
            multiPayloadUnPublishDuplicateCheck(workflowListSize, workflowMetadataMap);
        } else if (workflowListSize < 2 && !isUnPublish) {
            processPublishWorkflowPayload(payload, resourceResolver, workflowMetadataMap);
        } else if (workflowListSize < 2) {
            unPublishedAsset = new ArrayList<>();
            checkUnPublishedPayload(workItem, workflowSession, payload, resourceResolver, workflowMetadataMap);
            checkAssetReferencesPublished(resourceResolver, payload);
            processUnPublishedAssets(workflowMetadataMap);
        } else {
            duplicatePayload(workItem, workflowSession, payload, resourceResolver, workflowMetadataMap);
        }
    }

    private int multiResourcePayloadIterator(WorkItem workItem, WorkflowSession workflowSession,
                                             Iterator<Resource> resItr, int workflowListSize,
                                             PayloadMap payloadMap,
                                    ResourceResolver resourceResolver, MetaDataMap workflowMetadataMap)
            throws WorkflowException {
        String payload = workItem.getWorkflowData().getPayload().toString();
        while (resItr.hasNext()) {
            Resource res = resItr.next();
            String root = Objects.requireNonNull(res.adaptTo(ValueMap.class)).get("root", String.class);
            workflowListSize = payloadMap.getWorkflowInstances(root, true).size();
            if (workflowListSize > 1) {
                duplicatePayload(workItem, workflowSession, payload, resourceResolver, workflowMetadataMap);
                break;
            }
            pageStatus(root);
            if (!isUnPublish && pageOnly) {
                checkUnpublishedAssets(resourceResolver, root);
            }
            mutliPayloadUnPublishWorkflowProcess(workItem, workflowSession, root, resourceResolver,
                    workflowMetadataMap);
            if (!unPublishedAsset.isEmpty()) {
                break;
            }
        }
        return workflowListSize;
    }

    private void pageStatus(String root) {
        pageOnly = !isUnPublish && (StringUtils.startsWith(root, TCBConstants.CONTENT_TECHCOMBANK_PATH)
                || StringUtils.startsWith(root, TCBConstants.RDB_APP_CONTENT_FRAGMENT_PATH)
                || StringUtils.startsWith(root, TCBConstants.CDB_APP_CONTENT_FRAGMENT_PATH));
    }

    private void mutliPayloadUnPublishWorkflowProcess(WorkItem workItem, WorkflowSession workflowSession, String root,
                           ResourceResolver resourceResolver, MetaDataMap workflowMetadataMap)
            throws WorkflowException {
        if (!pageOnly && isUnPublish) {
            checkUnPublishedPayload(workItem, workflowSession, root, resourceResolver, workflowMetadataMap);
            checkAssetReferencesPublished(resourceResolver, root);
            processUnPublishedAssets(workflowMetadataMap);
        }
    }

    private void multiPayloadUnPublishDuplicateCheck(int workflowListSize, MetaDataMap workflowMetadataMap) {
        if (workflowListSize < 2 && !isUnPublish) {
            processUnPublishedAssets(workflowMetadataMap);
        }
    }

    private void processPublishWorkflowPayload(String payload, ResourceResolver resourceResolver,
                                               MetaDataMap workflowMetadataMap) {
        if (StringUtils.startsWith(payload, TCBConstants.CONTENT_TECHCOMBANK_PATH)
                || StringUtils.startsWith(payload, TCBConstants.RDB_APP_CONTENT_FRAGMENT_PATH)
                || StringUtils.startsWith(payload, TCBConstants.CDB_APP_CONTENT_FRAGMENT_PATH)) {
            logger.debug("Payload is a Page or a content fragment {}", payload);
            unPublishedAsset = new ArrayList<>();
            checkUnpublishedAssets(resourceResolver, payload);
            processUnPublishedAssets(workflowMetadataMap);
        } else {
            logger.debug("Payload is an Asset {}", payload);
            workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.TRUE);
        }
    }

    private void processUnPublishWorkflowArgs(String processArgs) {
        isUnPublish = false;
        if (Objects.nonNull(processArgs)) {
            String[] argumentArr = processArgs.split(TCBConstants.COMMA);
            String workflowType = argumentArr[0].split(TCBConstants.EQUALS_SYMBOL)[1];
            isUnPublish = workflowType.equals("unpublish");
        }
    }

    private void checkAssetReferencesPublished(ResourceResolver resourceResolver, String root) {
        List<String> allRef = getAllReferences(resourceResolver, root);
        for (String val : allRef) {
            Resource assetResource = resourceResolver.getResource(val);
            ReplicationStatus replicationStatus =
                    Objects.requireNonNull(assetResource).adaptTo(ReplicationStatus.class);
            if (Objects.requireNonNull(replicationStatus).isActivated()) {
                unPublishedAsset.add(val);
            }
        }
    }

    private void processUnPublishedAssets(final MetaDataMap workflowMetadataMap) {
        if (!unPublishedAsset.isEmpty()) {
            workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.FALSE);
            workflowMetadataMap.put(TCBConstants.ASSET_REFERENCE, unPublishedAsset);
        } else {
            workflowMetadataMap.put(TCBConstants.ASSET_PUBLISHED, TCBConstants.TRUE);
        }
    }

    private void checkUnPublishedPayload(final WorkItem workItem, final WorkflowSession workflowSession,
                                         final String payload, final ResourceResolver resourceResolver,
                                         final MetaDataMap workflowMetadataMap) throws WorkflowException {
        Resource assetResource = resourceResolver.getResource(payload);
        ReplicationStatus replicationStatus = Objects.requireNonNull(assetResource).adaptTo(ReplicationStatus.class);
        if (!Objects.requireNonNull(replicationStatus).isActivated()) {
            logger.debug("Resource is not published. Terminating workflow {}", payload);
            String initiator = workItem.getWorkflow().getInitiator();
            String emailTitle = TCBConstants.PAYLOAD_UNPUBLISHED_EMAIL_TITLE;
            workflowNotificationService.sendNotificationAndEmail(resourceResolver, workflowMetadataMap, payload,
                    initiator, StringUtils.EMPTY, StringUtils.EMPTY, emailTitle);
            Workflow workflow = workflowSession.getWorkflow(workItem.getWorkflow().getId());
            workflowSession.terminateWorkflow(workflow);
        }
    }

    private void duplicatePayload(final WorkItem workItem, final WorkflowSession workflowSession, final String payload,
                                  final ResourceResolver resourceResolver, final MetaDataMap workflowMetadataMap)
                                    throws WorkflowException {
        logger.debug("Resource is already in workflow. Terminating workflow {}", payload);
        String initiator = workItem.getWorkflow().getInitiator();
        String emailTitle = TCBConstants.WORKFLOW_TERMINATED_EMAIL_TITLE;
        workflowNotificationService.sendNotificationAndEmail(resourceResolver, workflowMetadataMap, payload,
                initiator, StringUtils.EMPTY, StringUtils.EMPTY, emailTitle);
        Workflow workflow = workflowSession.getWorkflow(workItem.getWorkflow().getId());
        workflowSession.terminateWorkflow(workflow);
    }

    private void checkUnpublishedAssets(final ResourceResolver resourceResolver, final String payload) {
        Map<String, Asset> allRef = getAssetReferences(resourceResolver, payload);
        getUnpublishedReferences(resourceResolver, allRef);
    }

    private Map<String, Asset> getAssetReferences(final ResourceResolver resourceResolver, final String payload) {
        Resource resource = resourceResolver.getResource(payload);
        Node node = resource.adaptTo(Node.class);
        AssetReferenceSearch ref = new AssetReferenceSearch(node, DamConstants.MOUNTPOINT_ASSETS, resourceResolver);
        Map<String, Asset> allRef = new HashMap<>();
        allRef.putAll(ref.search());
        return allRef;
    }

    private List<String> getAllReferences(final ResourceResolver resourceResolver, final String payload) {
        ReferenceSearch referenceSearch = new ReferenceSearch();
        referenceSearch.setExact(true);
        referenceSearch.setHollow(true);
        referenceSearch.setMaxReferencesPerPage(-1);
        List<String> allRef = new ArrayList<>();
        Collection<ReferenceSearch.Info> resultSet = referenceSearch.search(resourceResolver, payload).values();
        for (ReferenceSearch.Info info : resultSet) {
            allRef.add(info.getPagePath());
        }
        return allRef;
    }

    private void getUnpublishedReferences(final ResourceResolver resourceResolver, final Map<String, Asset> allRef) {
        for (Map.Entry<String, Asset> entry : allRef.entrySet()) {
            String val = entry.getKey();
            if (StringUtils.startsWith(val, TCBConstants.RDB_APP_CONTENT_FRAGMENT_PATH) || StringUtils.startsWith(
                    val, TCBConstants.CDB_APP_CONTENT_FRAGMENT_PATH)) {
                unpublishedAssetCheck(resourceResolver, val);
                checkUnpublishedAssets(resourceResolver, val);
            } else {
                unpublishedAssetCheck(resourceResolver, val);
            }
        }
    }

    private void unpublishedAssetCheck(final ResourceResolver resourceResolver, final String val) {
        Resource assetResource = resourceResolver.getResource(val);
        ReplicationStatus replicationStatus = Objects.requireNonNull(assetResource).adaptTo(ReplicationStatus.class);
        if (!Objects.requireNonNull(replicationStatus).isActivated()) {
            unPublishedAsset.add(val);
        }
    }
}
