package com.techcombank.core.pojo;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class in mapping between SEO properties
 * on Page to Java Class.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = SEO.class)
public class SEO {

    /**
     * SEO Title
     */
    @ValueMapValue
    @Getter
    private String seoTitle;

    /**
     * Meta Description
     */
    @ValueMapValue
    @Getter
    private String metaDescription;

    /**
     * Meta Keyword
     */
    @ValueMapValue
    @Getter
    private String metaKeyword;

    /**
     * Meta Image
     */
    @ValueMapValue
    @Getter
    private String metaImage;

    /**
     * Open Graph Title
     */
    @ValueMapValue
    @Getter
    private String openGraphTitle;

    /**
     * Open Graph Description
     */
    @ValueMapValue
    @Getter
    private String openGraphDescription;

    /**
     * Open Graph Type
     */
    @ValueMapValue
    @Getter
    private String openGraphType;

    /**
     * Change Frequency
     */
    @ValueMapValue
    @Getter
    private String changeFrequency;

    /**
     * SEO Priority
     */
    @ValueMapValue
    @Getter
    private String seoPriority;
}
