package com.techcombank.core.service;

import com.google.gson.JsonObject;
import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;

import java.util.List;
import java.util.Map;

public interface HttpClientExecutorService {

    JsonObject executeHttpPost(String url, Map<String, String> headerMap, StringEntity payLoad);

    JsonObject executeHttpGet(String url, Map<String, String> headerMap, List<NameValuePair> requestParamMap);
}
