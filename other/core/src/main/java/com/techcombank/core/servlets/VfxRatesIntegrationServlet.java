package com.techcombank.core.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.VfxRatesJobService;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Servlet receives the request from VFX Mule soft API and Create a job for building
 * VFX rates data.
 */
@Component(service = { Servlet.class })
@SlingServletPaths(value = "/bin/vfxintegration")
@ServiceDescription("VFX Rate Integration Servlet")
public class VfxRatesIntegrationServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final String CHAR_UTF_8 = "UTF-8";
    private static final int STATUS_CODE = 200;
    private static final String ERROR_STATUS_CODE = "400";
    private static final int CHAR_CODE = 128;
    private static final String STATUS = "statusCode";
    private static final String SOURCE = "source";
    private static final String SOURCE_VALUE = "AEM";
    private static final String ERROR_SOURCE_VALUE = "API";
    private static final String TX_DESC = "txDesc";
    private static final String TX_DESC_VALUE = "Success";
    private static final String ERROR_TX_DESC_VALUE = "Bad Request.Data object missing in the request input";
    private final transient Logger log = LoggerFactory.getLogger(VfxRatesIntegrationServlet.class);
    @Reference
    private transient VfxRatesJobService vfxRatesJobService;

    /**
     * This method reads the request data from the input stream
     *
     * @param reader
     * @return String VFXData
     * @throws IOException
     */
    private static String readAll(final Reader reader) throws IOException {

        StringBuilder builder = new StringBuilder();

        char[] charBuffer = new char[CHAR_CODE];
        int bytesRead = -1;
        while ((bytesRead = reader.read(charBuffer)) > 0) {
            builder.append(charBuffer, 0, bytesRead);
        }
        return builder.toString();
    }

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        log.debug("Vfx Integration Job servlet execution started");
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding(CHAR_UTF_8);
        JsonObject responseData = new JsonObject();

        try (InputStream is = request.getInputStream()) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String jsonText = readAll(reader);

            Gson gsonObj = new Gson();
            JsonElement vfxRate = gsonObj.fromJson(jsonText, JsonElement.class);
            JsonObject vfxRatesData = vfxRate.getAsJsonObject();
            if (null != vfxRatesData && null != vfxRatesData.get(TCBConstants.VFX_RATE_DATA)) {
                vfxRatesJobService.createVfxIntegrationJob(jsonText);
                responseData.addProperty(STATUS, STATUS_CODE);
                responseData.addProperty(SOURCE, SOURCE_VALUE);
                responseData.addProperty(TX_DESC, TX_DESC_VALUE);
            } else {
                responseData.addProperty(STATUS, ERROR_STATUS_CODE);
                responseData.addProperty(SOURCE, ERROR_SOURCE_VALUE);
                responseData.addProperty(TX_DESC, ERROR_TX_DESC_VALUE);
            }

            response.getWriter().write(responseData.toString());
            log.debug("Vfx Integration Job servlet execution completed");
        }

    }

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
