package com.techcombank.core.service.impl;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import com.techcombank.core.service.StockChartService;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = {StockChartService.class}, immediate = true)
@Designate(ocd = StockChartServiceImpl.StockChartConfig.class)
public class StockChartServiceImpl implements StockChartService {

    public static final String TICKER = "ticker";
    public static final String TYPE = "type";
    public static final String RESOLUTION = "resolution";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String RESOLUTION_DESC = "Resolution";

    private static final Logger LOG = LoggerFactory.getLogger(StockChartServiceImpl.class);

    @Reference
    private HttpClientExecutorService httpExecutor;

    @Reference
    private JwtTokenService jwtTokenService;

    private StockChartConfig stockChartConfig;

    @Activate
    public void activate(final StockChartConfig config) {
        this.stockChartConfig = config;
    }

    /**
     * This method fetch Stock Chart data
     *
     * @param isTokenExpired
     * @return JsonObject responseJson
     */
    @Override
    public JsonObject fetchStockChartData(final boolean isTokenExpired) {
        LOG.info("Calling Stock Chart service");
        String stockChartUrl = stockChartConfig.getStockChartUrl();
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headerMap = new HashMap<>();
        List<NameValuePair> requestParamMap = new ArrayList<>();
        headerMap.put(HttpHeaders.CONTENT_TYPE, TCBConstants.APPLICATION_JSON);
        headerMap.put(HttpHeaders.ACCEPT, TCBConstants.APPLICATION_JSON);
        headerMap.put(TCBConstants.AUTHORIZATION, TCBConstants.BEARER + token);
        requestParamMap.add(new BasicNameValuePair(TICKER, stockChartConfig.getTicker()));
        requestParamMap.add(new BasicNameValuePair(TYPE, stockChartConfig.getType()));
        requestParamMap.add(new BasicNameValuePair(RESOLUTION, stockChartConfig.getResolution()));
        requestParamMap.add(new BasicNameValuePair(FROM, getFromDate()));
        requestParamMap.add(new BasicNameValuePair(TO, getToDate()));
        return httpExecutor.executeHttpGet(stockChartUrl, headerMap, requestParamMap);
    }

    /**
     * This method returns the current date in the unix format. EX:'1693526399'
     *
     * @return String currentDate
     */
    private String getToDate() {
        long currentTime = Instant.now().getEpochSecond();
        return String.valueOf(currentTime);
    }

    /**
     * This method returns the year back date from current date in the unix format
     * based on configuration value.EX:'1693526399'
     *
     * @return String yearsAgo
     */
    private String getFromDate() {
        long yearsAgo = OffsetDateTime.now(ZoneOffset.UTC).minusYears(stockChartConfig.getYearInterval())
                .toEpochSecond();
        return String.valueOf(yearsAgo);
    }

    @ObjectClassDefinition(name = "Stock Chart Configuration", description = "Stock Chart Configuration")
    public @interface StockChartConfig {

        @AttributeDefinition(name = "stockChartUrl", description = "Stock Chart Url", type = AttributeType.STRING)
        String getStockChartUrl() default "";

        @AttributeDefinition(name = "ticker", description = "Ticker", type = AttributeType.STRING)
        String getTicker() default "TCB";

        @AttributeDefinition(name = "type", description = "Type", type = AttributeType.STRING)
        String getType() default "stock";

        @AttributeDefinition(name = RESOLUTION, description = RESOLUTION_DESC, type = AttributeType.STRING)
        String getResolution() default "D";

        @AttributeDefinition(name = "yearInterval", description = "Year Interval", type = AttributeType.INTEGER)
        int getYearInterval() default 1;

    }
}
