package com.techcombank.core.service;

public interface VfxRatesJobService {

    void createVfxIntegrationJob(String jsonText);

}
