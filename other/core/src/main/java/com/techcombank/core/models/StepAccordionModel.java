package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.inject.Inject;
import java.util.List;

/**
 * A Sling Model class that represents a step accordion.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StepAccordionModel {
    /**
     * The list of steps in the accordion.
     */
    @Inject
    @Getter
    private List<StepAccordionItem> stepAccordionItemList;
}
