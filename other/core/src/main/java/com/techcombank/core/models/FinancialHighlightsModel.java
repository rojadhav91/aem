package com.techcombank.core.models;

/**
 * Interface For FinancialHighlightsModel
 */
public interface FinancialHighlightsModel {
    /**
     * The getFhTitle.
     */
    String getFhTitle();

    /**
     * The getFhData.
     */
    String getFhData();

    /**
     * The getFhQuarter.
     */
    String getFhQuarter();

    /**
     * The getFhNotes.
     */
    String getFhNotes();
}
