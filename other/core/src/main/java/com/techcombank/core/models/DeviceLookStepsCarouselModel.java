package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class DeviceLookStepsCarouselModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = DeviceLookStepsCarouselModel.class)
public class DeviceLookStepsCarouselModel {

    /**
     * Number of card tiles to display
     */
    @ValueMapValue
    @Getter
    private int numberOfCardTilesToDisplay;

    /**
     * To choose the intended layout
     */
    @ValueMapValue
    @Getter
    private String layout;

    /**
     * List of Tiles
     */
    @ChildResource
    @Getter
    private List<DeviceLookStepsCarouselItemModel> listStepItems;

    public boolean isHaveStepImage() {
        if (listStepItems == null) {
            return false;
        }
        for (DeviceLookStepsCarouselItemModel item : listStepItems) {
            if (item.getImage() != null) {
                return true;
            }
        }
        return false;
    }

}
