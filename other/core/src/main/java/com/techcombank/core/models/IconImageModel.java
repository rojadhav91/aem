package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Icon images Properties of Accordion
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class IconImageModel {
    /**
     * The source of the activated logo.
     */
    @ValueMapValue
    @Getter
    private String iconSrcOn;

    /**
    * Returns the mobile image rendition path for the activated logo.
    */
    public String getMobileIconSrcOn() {
        return PlatformUtils.getMobileImagePath(iconSrcOn);
    }

    /**
    * Returns the web image rendition path for the deactivated logo.
    */
    public String getWebIconSrcOn() {
        return PlatformUtils.getWebImagePath(iconSrcOn);
    }

    /**
     * The source of the deactivated logo.
     */
    @ValueMapValue
    @Getter
    private String iconSrcOff;

    /**
    * Returns the mobile image rendition path for the activated logo.
    */
    public String getMobileIconSrcOff() {
        return PlatformUtils.getMobileImagePath(iconSrcOff);
    }

    /**
    * Returns the web image rendition path for the deactivated logo.
    */
    public String getWebIconSrcOff() {
        return PlatformUtils.getWebImagePath(iconSrcOff);
    }
}
