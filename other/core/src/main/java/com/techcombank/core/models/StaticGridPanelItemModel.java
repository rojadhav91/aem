package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for the panels in the static grid panel
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StaticGridPanelItemModel {
    /**
     * The icon image.
     */
    @ValueMapValue
    @Getter
    private String iconImage;

    /**
    * Returns the mobile image rendition path for the icon image.
    */
    public String getMobileIconImagePath() {
        return PlatformUtils.getMobileImagePath(iconImage);
    }

    /**
    * Returns the web image rendition path for the icon image.
    */
    public String getWebIconImagePath() {
        return PlatformUtils.getWebImagePath(iconImage);
    }

    /**
     * The icon title.
     */
    @ValueMapValue
    @Getter
    private String iconTitle;

    /**
     * The icon image.
     */
    @ValueMapValue
    @Getter
    private String iconImageAltText;

    /**
     * The description.
     */
    @ValueMapValue
    @Getter
    private String description;
}
