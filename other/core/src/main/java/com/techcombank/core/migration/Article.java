package com.techcombank.core.migration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.inject.Named;
import java.util.List;

/**
 * This class in mapping between Article properties
 * on Page to Java Class.
 */
@Data
public class Article {

    @Expose
    private int id;

    @Expose
    private String mastheadType;

    @Expose
    private String excerpt;

    @Expose
    private String slug;

    @Expose
    private String locale;

    @Expose
    private String title;

    @Expose
    private String subTitle;

    @Expose
    private String publishedDate;

    @Expose
    private String author;

    @Expose
    private String htmlContent;

    @Expose
    private List<ArticleContent> articleContent;

    @Expose
    private List<Sections> sections;

    @Expose
    @Named("meta_title")
    private String metaTitle;

    @Expose
    @SerializedName(value = "metaDescription", alternate = "meta_description")
    private String metaDescription;

    @Expose
    @SerializedName(value = "metaKeywords", alternate = "meta_keywords")
    private String metaKeywords;

    @Expose
    @SerializedName(value = "metaRobots", alternate = "meta_robots")
    private String metaRobots;

    @Expose
    private String changefreq;

    @Expose
    private String priority;

    @Expose
    @SerializedName(value = "metaImage", alternate = "meta_image")
    private MetaImage metaImage;

    @Expose
    private Masthead masthead;

    @Data
    public static class MetaImage {

        @Expose
        private String url;

        @Expose
        private String alternativeText;

    }

    @Data
    public static class ArticleContent {

        @Expose
        private String htmlContent;
        @Expose
        private BannerMediaSource bannerMediaSource;

        @Data
        public static class BannerMediaSource {
            @Expose
            private String url;
        }

    }

    @Data
    public static class Sections {

        @Expose
        private String url;

        @Expose
        private String navText;

        @Expose
        private List<Content> content;

        @Expose
        private Masthead.BackgroundImage backgroundImage;

        @Data
        public static class Content {

            @Expose
            private String title;

            @Expose
            private String ctaTitle;

            @Expose
            private String description;

            @Expose
            private String contentHtml;

        }

    }

    private ExcelPojo excelPojo;

}
