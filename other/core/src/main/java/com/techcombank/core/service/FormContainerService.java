package com.techcombank.core.service;

import com.google.gson.JsonObject;

import java.util.Map;

public interface FormContainerService {
    JsonObject formValues(boolean isTokenExpired, Map<String, String> map);
}
