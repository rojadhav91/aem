package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class CardSlidePanelModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CardSlidePanelModel.class)
public class CardSlidePanelModel {

    /**
     * Title
     * - Description: To add title text
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Description
     * - Description: To add description text
    */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * Background Colour
     * - Description: To author the background colour
    */
    @ValueMapValue
    @Getter
    private String backgroundColour;

    /**
     * Image 1
     * - Description: To add image from DAM
    */
    @ValueMapValue
    @Getter
    private String image1;

    /**
    * Returns the mobile image rendition path for the image 1.
    */
    public String getMobileImage1Path() {
        return PlatformUtils.getMobileImagePath(image1);
    }

    /**
    * Returns the web image rendition path for the image 1.
    */
    public String getWebImage1Path() {
        return PlatformUtils.getWebImagePath(image1);
    }

    /**
     * Image Alt Text 1
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String imageAltText1;

    /**
     * Image 2
     * - Description: To add image from DAM
    */
    @ValueMapValue
    @Getter
    private String image2;

    /**
    * Returns the mobile image rendition path for the image 2.
    */
    public String getMobileImage2Path() {
        return PlatformUtils.getMobileImagePath(image2);
    }

    /**
    * Returns the web image rendition path for the image 2.
    */
    public String getWebImage2Path() {
        return PlatformUtils.getWebImagePath(image2);
    }

    /**
     * Image Alt Text 2
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String imageAltText2;

    /**
     * Center Image
     * - Description: To add image from DAM
    */
    @ValueMapValue
    @Getter
    private String centerImage;

    /**
    * Returns the mobile image rendition path for the center image.
    */
    public String getMobileCenterImagePath() {
        return PlatformUtils.getMobileImagePath(centerImage);
    }

    /**
    * Returns the web image rendition path for the center image.
    */
    public String getWebCenterImagePath() {
        return PlatformUtils.getWebImagePath(centerImage);
    }

    /**
     * Center Image Alt Text
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String centerImageAltText;

    /**
     * Image 4
     * - Description: To add image from DAM
    */
    @ValueMapValue
    @Getter
    private String image4;

    /**
    * Returns the mobile image rendition path for the image 4.
    */
    public String getMobileImage4Path() {
        return PlatformUtils.getMobileImagePath(image4);
    }

    /**
    * Returns the web image rendition path for the image 4.
    */
    public String getWebImage4Path() {
        return PlatformUtils.getWebImagePath(image4);
    }

    /**
     * Image Alt Text 4
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String imageAltText4;

    /**
     * Image 5
     * - Description: To add image from DAM
    */
    @ValueMapValue
    @Getter
    private String image5;

    /**
    * Returns the mobile image rendition path for the image 5.
    */
    public String getMobileImage5Path() {
        return PlatformUtils.getMobileImagePath(image5);
    }

    /**
    * Returns the web image rendition path for the image 5.
    */
    public String getWebImage5Path() {
        return PlatformUtils.getWebImagePath(image5);
    }

    /**
     * Image Alt Text 5
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String imageAltText5;

}
