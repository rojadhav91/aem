package com.techcombank.core.models;

import java.util.Map;

public interface StoryListingModel {

    /**
     * Article Category type filter tags.
     */
    Map<String, String> getArticleCategoryTags();

    /**
     * Article Tag filter tags.
     */
    Map<String, String> getArticleTags();

}
