package com.techcombank.core.models.impl;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.models.GoldRateModel;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Gold Rate details
 */
@Model(adaptables = Resource.class, adapters = {GoldRateModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = GoldRateModelImpl.RESOURCE_TYPE)
public class GoldRateModelImpl implements GoldRateModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/goldrate";

    /**
     * The Text for the gold rate title.
     */
    @ValueMapValue
    @Getter
    private String goldRateTitle;

    /**
     * The Text for the tab day label.
     */
    @ValueMapValue
    @Getter
    private String dayLabel;

    /**
     * The Icon for day filed
     */
    @ValueMapValue
    @Getter
    private String dayIcon;

    /**
     * The Alt Text for day icon.
     */
    @ValueMapValue
    @Getter
    private String dayIconAlt;

    /**
     * The Text for the time label.
     */
    @ValueMapValue
    @Getter
    private String timeLabel;

    /**
     * The Icon for time filed.
     */
    @ValueMapValue
    @Getter
    private String timeIcon;

    /**
     * The Alt Text for time icon.
     */
    @ValueMapValue
    @Getter
    private String timeIconAlt;

    /**
     * The Text for the buy label.
     */
    @ValueMapValue
    @Getter
    private String buyLabel;

    /**
     * The Text for the sell label.
     */
    @ValueMapValue
    @Getter
    private String sellLabel;

    /**
     * The Text for the gold label.
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * Returns the web image rendition path for the day icon.
     */
    @Override
    public String getDayIconWebImagePath() {
        return PlatformUtils.getWebImagePath(dayIcon);
    }

    /**
     * Returns the mobile image rendition path for the day icon.
     */
    @Override
    public String getDayIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(dayIcon);
    }

    /**
     * Returns the web image rendition path for the time icon.
     */
    @Override
    public String getTimeIconWebImagePath() {
        return PlatformUtils.getWebImagePath(timeIcon);
    }

    /**
     * Returns the mobile image rendition path for the time icon.
     */
    @Override
    public String getTimeIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(timeIcon);
    }

}
