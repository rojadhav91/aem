package com.techcombank.core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.JsonFilterService;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

@Component(service = {JsonFilterService.class}, immediate = true)
@Designate(ocd = JsonFilterServiceImpl.Config.class)
public class JsonFilterServiceImpl implements JsonFilterService {

    public static final String TYPE = ":type";
    public static final String CONTENT_FRAGMENT = "content-fragment";
    public static final String JCR_CONTENT_DATA_1_JSON = "/jcr:content/data.-1.json";

    private void processImageData(final String[] keysRemove,
                                  final String imageDomain, final JsonObject jsonObject,
                                  final JsonObject processedObject, final String key) {
        if (jsonObject.get(key).isJsonObject()) {
            JsonObject imageJson = jsonObject.getAsJsonObject(key);
            if (!Objects.isNull(imageJson) && imageJson.has(TCBConstants.VALUE)) {

                switchToAbsolutePath(imageJson, imageDomain);
                for (String keyRemove : keysRemove) {
                    if (imageJson.has(keyRemove)) {
                        imageJson.remove(keyRemove);
                    }
                }
                processedObject.add(key, imageJson);
            }
        }
    }

    private Config config;

    @Reference
    private SlingSettingsService slingSettings;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Activate
    public void activate(final Config appConfig) {
        this.config = appConfig;
    }

    public String[] getJsonFilter() {
        return this.config.getJsonFilter();
    }

    @Override
    public String getAuthorizationToken() {
        return this.config.getAuthorizationToken();
    }

    public JsonElement processJSON(final JsonElement element,
                                   final String[] keysRemove, final String imageDomain,
                                   final String domain) {
        if (element.isJsonObject()) {
            JsonObject jsonObject = element.getAsJsonObject();
            JsonObject processedObject = new JsonObject();
            for (String key : jsonObject.keySet()) {
                if (!Arrays.asList(keysRemove).contains(key)) {
                    JsonElement value = processJSON(jsonObject.get(key), keysRemove, imageDomain, domain);
                    processedObject.add(key, value);
                }
                processImageData(keysRemove, imageDomain, jsonObject, processedObject, key);
            }
            processContentFragmentData(keysRemove, domain, imageDomain, jsonObject, processedObject);
            return processedObject;
        } else if (element.isJsonArray()) {
            JsonArray jsonArray = element.getAsJsonArray();
            JsonArray processedArray = new JsonArray();
            for (JsonElement item : jsonArray) {
                JsonElement processedItem = processJSON(item, keysRemove, imageDomain, domain);
                processedArray.add(processedItem);
            }
            return processedArray;
        } else {
            return element;
        }
    }

    private void switchToAbsolutePath(final JsonObject imageJson, final String imageDomain) {
        if (imageJson.get(TCBConstants.VALUE).isJsonPrimitive()) {
            String value = imageJson.get(TCBConstants.VALUE).getAsString();
            // If value starts with /content/dam/techcombank or /content/techcombank then
            // switch to absolute path
            boolean isConvertToAbsolutePath = StringUtils.startsWithIgnoreCase(value,
                    TCBConstants.CONTENT_TECHCOMBANK_PATH)
                    || StringUtils.startsWithIgnoreCase(value, TCBConstants.CONTENT_DAM_TECHCOMBANK_PATH);
            if (isConvertToAbsolutePath) {
                String updatedValue = imageDomain + value;
                imageJson.addProperty(TCBConstants.VALUE, updatedValue);
            }
        }
    }

    private void processContentFragmentData(final String[] keysRemove,
                                            final String domain, final String imageDomain, final JsonObject jsonObject,
                                            final JsonObject processedObject) {
        if (jsonObject.has(TYPE)) {
            String type = jsonObject.get(TYPE).getAsString();
            JsonArray valueArray = null;
            if (StringUtils.equals(CONTENT_FRAGMENT, type)) {
                if (jsonObject.has(TCBConstants.VALUE)) {
                    valueArray = jsonObject.getAsJsonArray(TCBConstants.VALUE);
                    processValueContentFragment(keysRemove, domain, imageDomain, valueArray);
                }
                processedObject.remove(TCBConstants.VALUE);
                processedObject.add(TCBConstants.VALUE, valueArray);
            }
        }
    }

    private void processValueContentFragment(final String[] keysRemove,
                 final String domain, final String imageDomain, final JsonArray valueArray) {
        for (int i = 0; i < valueArray.size(); i++) {
            JsonObject responseJson = new JsonObject();
            if (valueArray.get(i).isJsonObject()) {
                break;
            }
            String cfPath = valueArray.get(i).getAsString();
            String cfDomainPath = domain + cfPath + JCR_CONTENT_DATA_1_JSON;
            try {
                Map<String, Object> map = getHTTPResponse(responseJson, cfDomainPath);
                Gson gson = new Gson();
                if (null != map) {
                    convertRelToAbsPath(imageDomain, map);
                    JsonObject mapJsonObject = gson.toJsonTree(removeUnwantedKeysFromMap(map, keysRemove))
                            .getAsJsonObject();
                    valueArray.set(i, mapJsonObject);
                }
            } catch (IOException e) {
                logger.error("IOException {} {}", e.getMessage(), e);
            }

        }
    }

    private Map<String, Object> convertRelToAbsPath(final String imageDomain, final Map<String, Object> map) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = map.get(key);
            if (value instanceof Map) {
                convertRelToAbsPath(imageDomain, (Map<String, Object>) value);
            }
            if (value instanceof String) {
                String stringVal = (String) value;
                if (StringUtils.startsWithIgnoreCase(stringVal, TCBConstants.CONTENT_TECHCOMBANK_PATH)
                        || StringUtils.startsWithIgnoreCase(stringVal, TCBConstants.CONTENT_DAM_TECHCOMBANK_PATH)) {
                    map.put(key, imageDomain + value);
                }
            }
        }
        return map;
    }

    @Override
    public Map<String, Object> getHTTPResponse(final JsonObject responseJson,
                                               final String pagePath) throws IOException {
        Map<String, Object> map = null;

        URL url = new URL(pagePath);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setConnectTimeout(TCBConstants.CONNECTION_TIMEOUT);
        con.setReadTimeout(TCBConstants.CONNECTION_TIMEOUT);
        if (PlatformUtils.isAuthor(slingSettings)) {
            con.setRequestProperty(TCBConstants.AUTHORIZATION,
                    TCBConstants.BASIC + getAuthorizationToken());
        }

        con.setRequestMethod(HttpConstants.METHOD_GET);
        con.setRequestProperty(TCBConstants.CONTENT_TYPE, TCBConstants.APPLICATION_JSON);
        con.setRequestProperty(TCBConstants.ACCEPT, TCBConstants.APPLICATION_JSON);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),
                StandardCharsets.UTF_8))) {

            StringBuilder responseText = new StringBuilder();
            String responseLine = null;

            while ((responseLine = br.readLine()) != null) {
                responseText.append(responseLine.trim());
            }
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(responseText.toString(), Map.class);
        } catch (IOException e) {
            logger.error("Exception in JSON Utility : {} {}", e, e.getMessage());
            responseJson.addProperty(TCBConstants.ERROR, e.getMessage());
        }
        return map;
    }

    private Map<String, Object> removeUnwantedKeysFromMap(final Map<String, Object> map,
                                                          final String[] keysRemove) {
        map.keySet().removeIf(key -> key.contains("@")
                || key.contains("jcr:")
                || Arrays.asList(keysRemove).contains(key));
        for (Object value : map.values()) {
            if (value instanceof Map) {
                removeUnwantedKeysFromMap((Map<String, Object>) value, keysRemove);
            }
        }
        return map;
    }

    @ObjectClassDefinition(name = "Techcombank - Json Page Exporter Filter Config",
            description = "To filter un-wanted properties from page json")
    public @interface Config {
        @AttributeDefinition(name = "Json Filter", description = "Json Filter Key", type = AttributeType.STRING)
        String[] getJsonFilter();

        @AttributeDefinition(name = "Server Authorization Token", description = "Server Authorization Token",
                type = AttributeType.PASSWORD)
        String getAuthorizationToken() default "";

    }

}
