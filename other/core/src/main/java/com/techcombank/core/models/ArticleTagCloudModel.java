package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.ArticleTagCloud;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticleTagCloudModel {

    @ValueMapValue
    @Getter
    private String openInNewTab;

    @ValueMapValue
    @Getter
    private String tagLabel;

    @ScriptVariable
    private Page currentPage;

    @SlingObject
    private ResourceResolver resourceResolver;

    public List<ArticleTagCloud> getArticleList() {
        List<ArticleTagCloud> articleCloudList = new ArrayList<>();
        if (currentPage != null) {

            Tag[] tags = currentPage.getTags();
            String rootPath = currentPage.getAbsoluteParent(TCBConstants.COUNTRY_INDEX).getPath();
            Locale language = currentPage.getLanguage();
            for (Tag tag : tags) {
                if (tag.getPath().contains(TCBConstants.ARTICLE_CATEGORY_TAG)) {
                    ArticleTagCloud articleCloud = new ArticleTagCloud();
                    articleCloud.setTitle(tag.getTitle(language));
                    articleCloud.setOpenInNewTab(openInNewTab);
                    if (StringUtils.isNotEmpty(tag.getName())) {
                        String[] tagPath = tag.getPath().split(TCBConstants.ARTICLE_CATEGORY_TAG);
                        articleCloud.setPath(PlatformUtils.getMapUrl(resourceResolver,
                                rootPath + TCBConstants.SLASH + TCBConstants.COMPARE_PANEL_TAG + tagPath[1]));
                    }
                    articleCloud.setName(tag.getName());
                    articleCloudList.add(articleCloud);
                }
            }
        }
        return articleCloudList;
    }
    public String getHideStatus() {
       return currentPage.getProperties().get(TCBConstants.HIDE_ARTICLE_TAG_CLOUD, String.class);
    }
}
