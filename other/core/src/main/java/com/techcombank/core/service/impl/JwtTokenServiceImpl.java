package com.techcombank.core.service.impl;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import com.techcombank.core.service.ResourceResolverService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = {JwtTokenService.class}, immediate = true)
@Designate(ocd = JwtTokenServiceImpl.JwtTokenConfig.class)
public class JwtTokenServiceImpl implements JwtTokenService {
    private static final Logger LOG = LoggerFactory.getLogger(JwtTokenServiceImpl.class);
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/GCM/NoPadding";
    private static final String TOKEN_PARENT_PATH = "var";
    private static final String TOKEN_CHILD_PATH = "tokens";
    private static final String TOKEN = "token";
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 16;

    @Reference
    private HttpClientExecutorService httpExecutor;
    @Reference
    private ResourceResolverService resourceResolverService;
    private JwtTokenConfig jwtTokenConfig;

    @Activate
    public void activate(final JwtTokenConfig config) {
        this.jwtTokenConfig = config;
    }

    /**
     * This method returns JWT Token from QMS. It will first Check the token stored
     * in AEM. If the token is expired it will regenerate new one from QMS JWT token
     * service.
     *
     * @param isTokenExpired
     * @return String token
     */
    public String getJwtToken(final boolean isTokenExpired) {
        ResourceResolver resourceResolver = resourceResolverService.getWriteResourceResolver();
        String tokenPath = jwtTokenConfig.getJwtTokenPath();
        if (isTokenExpired) {
            generateJwtToken(resourceResolver, tokenPath);
        }
        String token = fetchJwtToken(resourceResolver, tokenPath);
        if (StringUtils.isEmpty(token)) {
            generateJwtToken(resourceResolver, tokenPath);
            token = fetchJwtToken(resourceResolver, tokenPath);
        }
        return token;
    }

    /**
     * This method generate new token from QMS Token Service and its stores the
     * token in AEM var/tokens folder.
     *
     * @param resourceResolver
     * @param tokenPath
     */
    private void generateJwtToken(final ResourceResolver resourceResolver, final String tokenPath) {
        String token = StringUtils.EMPTY;
        JsonObject responseJson;
        Map<String, String> headerMap = new HashMap<>();
        List<NameValuePair> requestParamMap = new ArrayList<>();
        headerMap.put(TCBConstants.CLIENT_ID, jwtTokenConfig.getJwtClientId());
        headerMap.put(TCBConstants.CLIENT_SECRET, jwtTokenConfig.getJwtClientSecret());
        headerMap.put(HttpHeaders.CONTENT_TYPE, TCBConstants.APPLICATION_JSON);
        headerMap.put(HttpHeaders.ACCEPT, TCBConstants.APPLICATION_JSON);
        responseJson = httpExecutor.executeHttpGet(jwtTokenConfig.getJwtTokenUrl(), headerMap, requestParamMap);
        if (null != responseJson && null != responseJson.get(TOKEN)) {
            token = responseJson.get(TOKEN).getAsString();
            if (StringUtils.isNotEmpty(token)) {
                createFolder(resourceResolver, tokenPath);
            }
        }
        saveToken(token, resourceResolver, tokenPath);
    }

    /**
     * This Method check and create the tokens folder in /var path if it is not
     * present
     *
     * @param resourceResolver
     * @param tokenPath
     */
    private void createFolder(final ResourceResolver resourceResolver, final String tokenPath) {
        try {
            Resource folderResource = resourceResolver.getResource(tokenPath);
            if (folderResource == null) {
                Resource parentResource = resourceResolver.getResource(TCBConstants.SLASH + TOKEN_PARENT_PATH);
                resourceResolver.create(parentResource, TOKEN_CHILD_PATH, null);
                resourceResolver.commit();
            }
        } catch (PersistenceException e) {
            LOG.error("Error while creating JWT Token folder: {} {}!", e.getMessage(), e);
        }
    }

    /**
     * This method saves/update the generated token in /var/tokens folder
     *
     * @param token
     * @param resourceResolver
     * @param tokenPath
     */
    private void saveToken(final String token, final ResourceResolver resourceResolver, final String tokenPath) {
        try {
            Resource folderResource = resourceResolver.getResource(tokenPath);
            if (null != folderResource) {
                ModifiableValueMap valueMap = folderResource.adaptTo(ModifiableValueMap.class);
                if (valueMap != null) {
                    if (StringUtils.isNotEmpty(token)) {
                        String encryptToken = encryptMessage(token.getBytes(StandardCharsets.UTF_8),
                                jwtTokenConfig.getEncKey().getBytes());
                        valueMap.put(TOKEN, encryptToken);
                    } else {
                        valueMap.remove(TOKEN);
                    }
                    resourceResolver.commit();
                }
            }
        } catch (NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | PersistenceException
                 | InvalidKeyException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            LOG.error("Error while saving Jwt token : {} {}!", e.getMessage(), e);
        }
    }

    /**
     * This method reads the token value from /var/tokens path in AEM
     *
     * @param resourceResolver
     * @param tokenPath
     * @return String token
     */
    private String fetchJwtToken(final ResourceResolver resourceResolver, final String tokenPath) {
        String token = StringUtils.EMPTY;
        try {
            Resource folderResource = resourceResolver.getResource(tokenPath);
            if (null != folderResource) {
                ModifiableValueMap valueMap = folderResource.adaptTo(ModifiableValueMap.class);
                if (null != valueMap && null != valueMap.get(TOKEN)) {
                    token = valueMap.get(TOKEN).toString();
                    token = decryptMessage(token, jwtTokenConfig.getEncKey().getBytes());
                }
            }
        } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
                 | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            token = StringUtils.EMPTY;
            LOG.error("Error While fetch and decrypting token : {} {}!", e.getMessage(), e);
        }

        return token;
    }

    /**
     * This method encrypt the token value with cipher algorithm
     *
     * @param tokenMessage
     * @param keyBytes
     * @return string encryptedTokenMessage
     * @throws InvalidKeyException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException
     */
    public String encryptMessage(final byte[] tokenMessage, final byte[] keyBytes) throws InvalidKeyException,
            NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException,
            InvalidAlgorithmParameterException {

        byte[] iv = new byte[GCM_IV_LENGTH];
        (new SecureRandom()).nextBytes(iv);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        SecretKey secretKey = new SecretKeySpec(keyBytes, ALGORITHM);
        GCMParameterSpec ivSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
        byte[] ciphertext = cipher.doFinal(tokenMessage);
        byte[] encryptedTokenMessage = new byte[iv.length + ciphertext.length];
        System.arraycopy(iv, 0, encryptedTokenMessage, 0, iv.length);
        System.arraycopy(ciphertext, 0, encryptedTokenMessage, iv.length, ciphertext.length);
        return Base64.getEncoder().encodeToString(encryptedTokenMessage);
    }

    /**
     * This method decrypt the token value using cipher algorithm
     *
     * @param encryptedTokenMessage
     * @param keyBytes
     * @return String decryptedTokenMessage
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws InvalidAlgorithmParameterException
     * @throws UnsupportedEncodingException
     */
    public String decryptMessage(final String encryptedTokenMessage, final byte[] keyBytes)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException {

        byte[] decoded = Base64.getDecoder().decode(encryptedTokenMessage);
        byte[] iv = Arrays.copyOfRange(decoded, 0, GCM_IV_LENGTH);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        SecretKey secretKey = new SecretKeySpec(keyBytes, ALGORITHM);
        GCMParameterSpec ivSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);
        byte[] ciphertext = cipher.doFinal(decoded, GCM_IV_LENGTH, decoded.length - GCM_IV_LENGTH);
        return new String(ciphertext, StandardCharsets.UTF_8);
    }

    @ObjectClassDefinition(name = "JWT Token Configuration", description = "JWT Token Configurations")
    public @interface JwtTokenConfig {

        @AttributeDefinition(name = "jwtClientID", description = "Jwt Client ID", type = AttributeType.STRING)
        String getJwtClientId() default "";

        @AttributeDefinition(name = "jwtClientSecret", description = "Jwt Client Secret", type = AttributeType.STRING)
        String getJwtClientSecret() default "";

        @AttributeDefinition(name = "jwtTokenUrl", description = "Jwt Token Url", type = AttributeType.STRING)
        String getJwtTokenUrl() default "";

        @AttributeDefinition(name = "encKey", description = "Encrypt Key", type = AttributeType.STRING)
        String getEncKey() default "";

        @AttributeDefinition(name = "jwtTokenPath", description = "Jwt Token Path", type = AttributeType.STRING)
        String getJwtTokenPath() default "/var/tokens";
    }

}
