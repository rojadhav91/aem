package com.techcombank.core.servlets;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.VfxRatesDataService;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

/**
 * Servlet that reads Vfx Rate data from content fragment and writes Vfx Rate
 * data json content into the response.
 */
@Component(service = { Servlet.class })

@SlingServletResourceTypes(resourceTypes = "techcombank/components/structure/basepage",
methods = HttpConstants.METHOD_GET, selectors = {
        TCBConstants.EXCHANGE_RATE, TCBConstants.GOLD_RATE, TCBConstants.OTHER_RATE, TCBConstants.TENOR_INT_RATE,
        TCBConstants.TENOR_RATE, TCBConstants.FIXING_RATE }, extensions = "json")

@ServiceDescription("Vfx Rate Data Servlet")
public class VfxRatesDataServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    @Reference
    private transient VfxRatesDataService vfxRatesDataService;

    private static final int S_INDEX_ZERO = 0;
    private static final int S_INDEX_ONE = 1;
    private static final int S_INDEX_TWO = 2;
    private static final int S_LENGTH_FOUR = 4;
    private static final int S_LENGTH_THREE = 3;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding("UTF-8");
        ResourceResolver resolver = request.getResourceResolver();
        String[] selectors = request.getRequestPathInfo().getSelectors();
        String date = StringUtils.EMPTY;
        String time = StringUtils.EMPTY;
        JsonObject vfxRateArray = new JsonObject();
        if (!ArrayUtils.isEmpty(selectors)) {
            // exchange-rates.2023-07-12.integration
            String vfxRateType = selectors[S_INDEX_ZERO];
            if (selectors.length == S_LENGTH_FOUR) {
                date = selectors[S_INDEX_ONE];
                time = selectors[S_INDEX_TWO];
            }
            if (selectors.length == S_LENGTH_THREE) {
                date = selectors[S_INDEX_ONE];
            }
            vfxRateArray = getVfxRateJson(vfxRateType, date, time, resolver);
        }
        response.getWriter().write(vfxRateArray.toString());
    }

    private JsonObject getVfxRateJson(final String vfxType, final String date, final String time,
            final ResourceResolver resolver) {
        String vfxRateDateTime = StringUtils.EMPTY;
        String vfxRateDateTimePath = StringUtils.EMPTY;
        String vfxRateDatePath = StringUtils.EMPTY;
        String vfxRateType = vfxType;

        JsonObject vfxRateData = new JsonObject();
        if (null != vfxRateType) {

            if (StringUtils.isNotEmpty(date) && StringUtils.isNotEmpty(time)) {
                vfxRateDateTime = date + " " + time;
                vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
                vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
            } else if (StringUtils.isNotEmpty(date)) {
                vfxRateDateTime = date;
                vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
            }
            vfxRateData = getVfxRateJsonData(vfxRateDateTime, date, time, vfxRateDateTimePath, vfxRateDatePath,
                    vfxRateType, resolver);
        }

        return vfxRateData;
    }

    private JsonObject getVfxRateJsonData(final String vfxRateDateTime, final String date, final String time,
            final String vfxRateDateTimePath, final String vfxRateDatePath, final String vfxRateType,
            final ResourceResolver resolver) {
        JsonObject vfxRateDetails;
        JsonObject vfxRate = new JsonObject();
        boolean isexchangeRate = false;
        String vfxRateDT = vfxRateDateTime;
        String vfxRateDTPath = vfxRateDateTimePath;
        String vfxRateDayPath = vfxRateDatePath;
        String vfxType = vfxRateType;
        List<String> vfxRateBasePath = vfxRatesDataService.getVfxRateBasePath(vfxType, true);
        for (String vfxBasePath : vfxRateBasePath) {
            if (StringUtils.isEmpty(date) && StringUtils.isEmpty(time) && !isexchangeRate) {
                vfxRateDT = vfxRatesDataService.getVfxRateUpdateDate(resolver, vfxBasePath);
                vfxRateDTPath = PlatformUtils.getVfxRateDateTimePath(vfxRateDT, true);
                vfxRateDayPath = PlatformUtils.getVfxRateDateTimePath(vfxRateDT, false);
                if (vfxType.equalsIgnoreCase(TCBConstants.EXCHANGE_RATE)) {
                    isexchangeRate = true;
                }
            }

            List<String> vfxRateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resolver, vfxRateDayPath,
                    vfxBasePath);

            if (StringUtils.isNotEmpty(date) && StringUtils.isEmpty(time) && !vfxRateTimeStamp.isEmpty()
                    && !isexchangeRate) {
                StringBuilder dateTime = new StringBuilder();
                dateTime.append(vfxRateDT);
                dateTime.append(" ");
                dateTime.append(vfxRateTimeStamp.get(S_INDEX_ZERO));
                vfxRateDTPath = PlatformUtils.getVfxRateDateTimePath(dateTime.toString(), true);
                if (vfxType.equalsIgnoreCase(TCBConstants.EXCHANGE_RATE)) {
                    isexchangeRate = true;
                }
            }

            Resource vfxRateResource = vfxRatesDataService.getVfxRate(resolver, vfxRateDTPath, vfxBasePath);
            vfxType = PlatformUtils.getVfxRateType(vfxBasePath);
            vfxRateDetails = vfxRatesDataService.getVfxRateData(vfxRateResource, resolver, vfxRateTimeStamp, vfxType);
            PlatformUtils.getVfxRateJsonData(vfxRateDetails, vfxRate, vfxType);
        }

        return vfxRate;
    }

}
