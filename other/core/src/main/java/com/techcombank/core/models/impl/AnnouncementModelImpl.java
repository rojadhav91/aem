package com.techcombank.core.models.impl;


import com.techcombank.core.models.AnnouncementModel;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;



@Model(adaptables = Resource.class, adapters = {AnnouncementModel.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = AnnouncementModelImpl.RESOURCE_TYPE)
public class AnnouncementModelImpl implements AnnouncementModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/announcement";

    /**
     * announcementText.
     */
    @ValueMapValue
    @Getter
    private String announcementText;

    /**
     * iconPath
     */
    @ValueMapValue
    @Getter
    private String iconPath;

    /**
     * iconPath
     */
    @ValueMapValue
    @Getter
    private String iconAltText;

    /**
     * titleText
     */
    @ValueMapValue
    @Getter
    private String titleText;

    /**
     * titleDescription
     */
    @ValueMapValue
    @Getter
    private String titleDescription;

    /**
     * ctaRequired
     */
    @ValueMapValue
    @Getter
    private Boolean ctaRequired;

    public String getIconPath() {
        return PlatformUtils.getWebImagePath(iconPath);
    }
    public String getIconPathMobile() {
        return PlatformUtils.getMobileImagePath(iconPath);
    }
}
