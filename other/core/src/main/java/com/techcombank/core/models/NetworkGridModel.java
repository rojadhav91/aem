package com.techcombank.core.models;

import java.util.List;

import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.Model;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class NetworkGridModel.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = HeaderModel.RESOURCE_TYPE)
public class NetworkGridModel {

    /**
     * Network grid items.
     */
    @ChildResource
    @Getter
    private List<NetworkGridItemModel> networkGridItems;
}
