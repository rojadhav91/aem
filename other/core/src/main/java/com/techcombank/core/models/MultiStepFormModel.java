package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class MultiStepFormModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = MultiStepFormModel.class)
public class MultiStepFormModel {

    /**
     * FormId
     * - Description: Following DOM ID format. Showing by steps number, each step will have StepID, it's a combination of FormID_{STEP_NUMBER}, this field is for reference only, not allowing to edit (disabled), auto-change following change of FormID.
    */
    @ValueMapValue
    @Getter
    private String formId;

    /**
     * Number Of Steps
     * - Description: Total number of steps in multi-step form
    */
    @ValueMapValue
    @Getter
    private int numberOfSteps;

    public int[] getStepIndexes() {
        int[] stepIndexes = new int[numberOfSteps];
        for (int i = 1; i <= numberOfSteps; i++) {
            stepIndexes[i - 1] = i;
        }
        return stepIndexes;
    }

}
