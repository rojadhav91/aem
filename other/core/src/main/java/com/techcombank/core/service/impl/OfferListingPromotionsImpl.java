package com.techcombank.core.service.impl;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.OfferListingCard;
import com.techcombank.core.service.OfferListingPromotions;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.adobe.aem.formsndocuments.util.FMConstants.DAM_ASSET_NODETYPE;

@Component(service = {OfferListingPromotions.class}, immediate = true)
public class OfferListingPromotionsImpl implements OfferListingPromotions {

    protected static final String ASC = "asc";
    protected static final String DSC = "desc";
    protected static final String TOTAL = "total";
    protected static final String RESULTS = "results";
    protected static final String EXPIRY = "expiryDate";
    protected static final String IMAGE = "thumbnail";
    protected static final String URL = "url";
    protected static final String PATH = "path";
    protected static final String TYPE = "type";
    protected static final String TRUE = "true";
    protected static final String MERCHANT_TAG = "jcr:content/data/master/@merchant";
    protected static final String PRODUCTS_TAG = "jcr:content/data/master/@card";
    protected static final String CATEGORY_TAG = "jcr:content/data/master/@category";
    protected static final String GROUP_ONE = "group.1_group.";
    protected static final String DATA_RANGE_PROPERTY = "daterange.property";
    protected static final String DATA_RANGE_LOWERBOUND = "daterange.lowerBound";
    protected static final String DATA_RANGE_LOWEROPERATIONS = "daterange.lowerOperation";
    protected static final String DATA_RANGE_LOWEROPERATIONS_VALUE = ">=";
    protected static final String FILTER_EXPIRYDATE = "jcr:content/data/master/@expiryDate";
    protected static final String MOST_POPULAR_FILTER_ONE = "@jcr:content/data/master/favourite";
    protected static final String MOST_POPULAR_FILTER_TWO = "@jcr:content/data/master/ranking";
    protected static final String LATEST_CARD_FILTER = "@jcr:created";
    protected static final String EXPIRIY_CARD_FILTER = "@jcr:content/data/master/expiryDate";
    protected static final String DESCRIPTION_SEARCH_FILTER = "jcr:content/data/master/@description";
    protected static final String VALUE = "_value";
    protected static final String PROPERTY = "_property";
    protected static final String LIKE_OPERATION = "like";
    protected static final String OPERATION = "operation";
    protected static final String ZERO = "0";
    protected static final String ORDERBY = "orderby";
    protected static final String ORDERBY_SORT = "orderby.sort";
    protected static final String ORDERBY_1 = "1_orderby";
    protected static final String ORDERBY_SORT_1 = "1_orderby.sort";
    protected static final String OFFSET = "p.offset";
    protected static final String LIMIT = "p.limit";
    protected static final String REQUEST_PARAM_LIMIT = "limit";
    protected static final String REQUEST_PARAM_OFFSET = "offset";
    protected static final String REQUEST_PARAM_ONLOAD = "isOnLoad";
    protected static final String REQUEST_PARAM_SORT = "sort";
    protected static final String REQUEST_PARAM_MOST_POPULAR = "most-popular";
    protected static final String REQUEST_PARAM_LATEST = "latest";
    protected static final String REQUEST_PARAM_MORETIME = "more-time-to-consider";
    protected static final String REQUEST_PARAM_EXPIRING = "expiring-soon";
    protected static final String REQUEST_PARAM_PRODUCTS = "products";
    protected static final String REQUEST_PARAM_PARTNER = "partner";
    protected static final String REQUEST_PARAM_TYPES = "types";
    protected static final String REQUEST_PARAM_IS_SEARCH = "isSearch";
    protected static final String REQUEST_PARAM_SEARCH_TEXT = "searchText";
    protected static final String DATEMATCHER = "dd MMM yyyy";
    protected static final String CURRENT_DATE_MATCHER = "YYYY-MM-dd'T'HH:mm:ss";
    private static final Logger LOG = LoggerFactory.getLogger(OfferListingPromotionsImpl.class);
    private static final String MERCHANT_TAG_PROPERTY = "techcombank:promotions/merchant/";
    private static final String PRODUCTS_TAGS_PROPERTY = "techcombank:promotions/product-types/";
    private static final String CATEGORY_TAGS_PROPERTY = "techcombank:promotions/promotion-categories/";
    private Map<String, Object> response;


    /**
     * Generates a list of offers based on the provided query parameters.
     *
     * @param resourceResolver The resource resolver for accessing the content repository.
     * @param queryMap         The query parameters for filtering and sorting.
     * @return A Map containing offer data, including total results and offer listings.
     */
    @Override
    public Map<String, Object> generateOffers(final ResourceResolver resourceResolver,
                                              final Map<String, String> queryMap, final Page currentPage) {
        try {
            OfferListingCard offerListingCard;
            List<OfferListingCard> listingOfferList = new ArrayList<>();
            Session session = resourceResolver.adaptTo(Session.class);
            QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
            Query query = queryBuilder.createQuery(PredicateGroup.create(queryMap), session);
            Locale language = currentPage.getLanguage();
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (!Objects.isNull(query)) {
                SearchResult result = query.getResult();
                String totalResultCount = String.valueOf(result.getTotalMatches());
                if (result.getTotalMatches() == 0) {
                    Map<String, Object> offerMap = new HashMap<>();
                    offerMap.put(TOTAL, totalResultCount);
                    offerMap.put(RESULTS, listingOfferList.toArray());
                    response = offerMap;
                } else {
                    Iterator<Node> nodeIterator = result.getNodes();
                    while (nodeIterator.hasNext()) {
                        Node it = nodeIterator.next();
                        Resource resource = resourceResolver.getResource(it.getPath() + TCBConstants.SLASH
                                + JcrConstants.JCR_CONTENT + "/data/master");
                        if (!Objects.isNull(resource)) {
                            offerListingCard = resource.adaptTo(OfferListingCard.class);
                            String[] cardarray = offerListingCard.getCard();
                            offerListingCard.setProducts(getTagTitleList(cardarray, tagManager, language));
                            String[] merchantarray = offerListingCard.getMerchant();
                            offerListingCard.setPartner(getTagTitleList(merchantarray, tagManager, language));
                            offerListingCard.setExpiryDate(getFormatedDate(resource.getValueMap()
                                    .get(EXPIRY, String.class), language));
                            offerListingCard.setThumbnail(PlatformUtils.
                                    getThumbnailImagePath(resource.getValueMap()
                                            .get(IMAGE, String.class)));
                            offerListingCard.setUrl(PlatformUtils.
                                    isInternalLink(resourceResolver, resource.getValueMap()
                                            .get(URL, String.class)));
                            offerListingCard.setInteractionType(PlatformUtils.
                                    getUrlLinkType(resource.getValueMap()
                                            .get(URL, String.class)));
                            listingOfferList.add(offerListingCard);
                        }
                    }
                    if (!listingOfferList.isEmpty()) {
                        Map<String, Object> offerMap = new HashMap<>();
                        offerMap.put(TOTAL, totalResultCount);
                        offerMap.put(RESULTS, listingOfferList.toArray());
                        response = offerMap;
                    }
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Exception while constructing data for articles : {0}", e);
        }
        return response;
    }

    /**
     * This Method returns list of tag titles for the given tags
     *
     * @param taglist
     * @param tagManager
     * @param language
     * @return tagTitleList
     */
    private ArrayList<String> getTagTitleList(final String[] taglist, final TagManager tagManager,
                                              final Locale language) {
        ArrayList<String> tagTitleList = new ArrayList<>();
        if (null != taglist) {
            for (String tagPath : taglist) {
                Tag tag = tagManager.resolve(tagPath);
                if (tag != null) {
                    tagTitleList.add(tag.getTitle(language));
                }
            }
        }
        return tagTitleList;
    }

    /**
     * Formats a date string into a human-readable format.
     *
     * @param date The date string to be formatted.
     * @return A formatted date string (e.g., "dd MMM yyyy").
     */
    private String getFormatedDate(final String date, final Locale language) {
        String formattedDate = null;
        if (StringUtils.isNotBlank(date)) {
            LocalDateTime dateTime = LocalDateTime.parse(date, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            formattedDate = PlatformUtils.setTimeFormat(language, dateTime);
        }
        return formattedDate;
    }

    /**
     * This method returns current date and time stamp     *
     *
     * @return String dateTime
     */
    private String getCurrentDateTime() {
        Instant instant = Instant.now();
        LocalDateTime localDateTime = instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
        return formatDate(localDateTime, CURRENT_DATE_MATCHER);
    }

    /**
     * This method formats the given date and time.
     *
     * @param localDateTime
     * @param dateFormat
     * @return String DataTime
     */
    private String formatDate(final LocalDateTime localDateTime, final String dateFormat) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return localDateTime.format(formatter);
    }

    /**
     * This Method builds the Query params for promotions offer listing
     * return queryMap
     */
    @Override
    public Map<String, String> generatePromotionsQuery(final Map<String, String> requestQueryParam,
                                                       final String cfDataPath) {
        final Map<String, String> queryMap = new HashMap<>();
        if (!requestQueryParam.isEmpty()) {
            addBasicQueryParamMap(queryMap, requestQueryParam, cfDataPath);
            if (requestQueryParam.containsKey(REQUEST_PARAM_SORT)) {
                addPromotionsFilterCards(queryMap, requestQueryParam);
                if (requestQueryParam.get(REQUEST_PARAM_SORT).equalsIgnoreCase(REQUEST_PARAM_MOST_POPULAR)) {
                    addOrderByQueryParamMap(queryMap, MOST_POPULAR_FILTER_ONE, DSC, false);
                    addOrderByQueryParamMap(queryMap, MOST_POPULAR_FILTER_TWO, ASC, true);
                } else if (requestQueryParam.get(REQUEST_PARAM_SORT).equalsIgnoreCase(REQUEST_PARAM_LATEST)) {
                    addOrderByQueryParamMap(queryMap, LATEST_CARD_FILTER, DSC, false);
                } else if (requestQueryParam.get(REQUEST_PARAM_SORT).equalsIgnoreCase(REQUEST_PARAM_MORETIME)) {
                    addOrderByQueryParamMap(queryMap, EXPIRIY_CARD_FILTER, DSC, false);
                } else if (requestQueryParam.get(REQUEST_PARAM_SORT).equalsIgnoreCase(REQUEST_PARAM_EXPIRING)) {
                    addOrderByQueryParamMap(queryMap, EXPIRIY_CARD_FILTER, ASC, false);
                }

            }

        }
        return queryMap;
    }

    /**
     * This Method adds the basic query param details
     *
     * @param queryMap
     * @param requestQueryParam
     */
    private void addBasicQueryParamMap(final Map<String, String> queryMap,
                                       final Map<String, String> requestQueryParam, final String cfDataPath) {
        queryMap.put(PATH, cfDataPath);
        queryMap.put(TYPE, DAM_ASSET_NODETYPE);
        queryMap.put(DATA_RANGE_PROPERTY, FILTER_EXPIRYDATE);
        queryMap.put(DATA_RANGE_LOWERBOUND, getCurrentDateTime());
        queryMap.put(DATA_RANGE_LOWEROPERATIONS, DATA_RANGE_LOWEROPERATIONS_VALUE);
        if (requestQueryParam.containsKey(REQUEST_PARAM_LIMIT) && requestQueryParam.containsKey(REQUEST_PARAM_OFFSET)) {
            queryMap.put(LIMIT, requestQueryParam.get(REQUEST_PARAM_LIMIT));
            queryMap.put(OFFSET, requestQueryParam.get(REQUEST_PARAM_OFFSET));
        }
    }

    /**
     * This Method adds the orderby query param details
     *
     * @param queryMap
     * @param orderByValue
     * @param sortingOrder
     */
    private void addOrderByQueryParamMap(final Map<String, String> queryMap, final String orderByValue,
                                         final String sortingOrder, final boolean isSecondFilter) {
        queryMap.put(isSecondFilter ? ORDERBY_1 : ORDERBY, orderByValue);
        queryMap.put(isSecondFilter ? ORDERBY_SORT_1 : ORDERBY_SORT, sortingOrder);
    }

    /**
     * This Method adds the Search query param details
     *
     * @param queryMap
     * @param searchText
     * @param propertyIndex
     */
    private void addTextSearchQueryParamMap(final Map<String, String> queryMap, final String searchText,
                                            final int propertyIndex) {
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY, DESCRIPTION_SEARCH_FILTER);
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY + TCBConstants.DOT + ZERO + VALUE,
                TCBConstants.PERCENTAGE + searchText + TCBConstants.PERCENTAGE);
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY + TCBConstants.DOT + OPERATION, LIKE_OPERATION);
    }

    /**
     * This Method adds the product query param details
     *
     * @param queryMap
     * @param filterTag
     * @param propertyIndex
     */
    private void addProductsQueryParamMap(final Map<String, String> queryMap, final String[] filterTag,
                                          final int propertyIndex) {
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY, PRODUCTS_TAG);
        int valueIndex = 0;
        for (String tagName : filterTag) {
            queryMap.put(GROUP_ONE + propertyIndex + PROPERTY + TCBConstants.DOT + valueIndex + VALUE,
                    PRODUCTS_TAGS_PROPERTY + tagName);
            valueIndex = valueIndex + 1;
        }

    }

    /**
     * This Method adds the partners query param details
     *
     * @param queryMap
     * @param filterTag
     * @param propertyIndex
     */
    private void addPartnersQueryParamMap(final Map<String, String> queryMap, final String[] filterTag,
                                          final int propertyIndex) {
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY, MERCHANT_TAG);
        int valueIndex = 0;
        for (String tagName : filterTag) {
            queryMap.put(GROUP_ONE + propertyIndex + PROPERTY + TCBConstants.DOT + valueIndex + VALUE,
                    MERCHANT_TAG_PROPERTY + tagName);
            valueIndex = valueIndex + 1;
        }

    }


    /**
     * This Method adds the category query param details
     *
     * @param queryMap
     * @param filterTag
     * @param propertyIndex
     */
    private void addTypesQueryParamMap(final Map<String, String> queryMap, final String[] filterTag,
                                       final int propertyIndex) {
        queryMap.put(GROUP_ONE + propertyIndex + PROPERTY, CATEGORY_TAG);
        int valueIndex = 0;
        for (String tagName : filterTag) {
            queryMap.put(GROUP_ONE + propertyIndex + PROPERTY + TCBConstants.DOT + valueIndex + VALUE,
                    CATEGORY_TAGS_PROPERTY + tagName);
            valueIndex = valueIndex + 1;
        }

    }

    /**
     * This method constuct the query param map based on the request param details
     *
     * @param queryMap
     * @param requestQueryParam
     */
    private void addPromotionsFilterCards(final Map<String, String> queryMap,
                                          final Map<String, String> requestQueryParam) {
        String[] filterTag;
        int propertyIndex = 1;
        if (requestQueryParam.containsKey(REQUEST_PARAM_PRODUCTS)) {
            filterTag = requestQueryParam.get(REQUEST_PARAM_PRODUCTS).split(TCBConstants.COMMA);
            addProductsQueryParamMap(queryMap, filterTag, propertyIndex);
            propertyIndex = propertyIndex + 1;
        }
        if (requestQueryParam.containsKey(REQUEST_PARAM_PARTNER)) {
            filterTag = requestQueryParam.get(REQUEST_PARAM_PARTNER).split(TCBConstants.COMMA);
            addPartnersQueryParamMap(queryMap, filterTag, propertyIndex);
            propertyIndex = propertyIndex + 1;
        }

        if (requestQueryParam.containsKey(REQUEST_PARAM_TYPES)) {
            filterTag = requestQueryParam.get(REQUEST_PARAM_TYPES).split(TCBConstants.COMMA);
            addTypesQueryParamMap(queryMap, filterTag, propertyIndex);
            propertyIndex = propertyIndex + 1;
        }

        // search based on description
        if (requestQueryParam.containsKey(REQUEST_PARAM_IS_SEARCH)
                && requestQueryParam.get(REQUEST_PARAM_IS_SEARCH).equalsIgnoreCase(TRUE)
                && requestQueryParam.containsKey(REQUEST_PARAM_SEARCH_TEXT)) {
            String searchText = requestQueryParam.get(REQUEST_PARAM_SEARCH_TEXT);
            addTextSearchQueryParamMap(queryMap, searchText, propertyIndex);
        }

    }
}
