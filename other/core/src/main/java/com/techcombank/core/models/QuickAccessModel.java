package com.techcombank.core.models;

import java.util.List;

/**
 * Interface For QuickAccessModal
 *
 */
public interface QuickAccessModel {

    /**
     * Tab one link url
     */
    String getTabOneLink();

    /**
     * Tab two link url
     */
    String getTabTwoLink();

    /**
     * Tab three link url
     */
    String getTabThreeLink();

    /**
     * Date change note text
     */
    String getDateChangeNote();

    /**
     * Returns the web image rendition path for the tab one icon.
     */
    String getTabOneIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the tab one icon.
     */
    String getTabOneIconMobileImagePath();

    /**
     * Returns the web image rendition path for the tab two Icon.
     */
    String getTabTwoIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the tab two Icon.
     */
    String getTabTwoIconMobileImagePath();

    /**
     * Returns the web image rendition path for the tab two Icon.
     */
    String getTabThreeIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the tab two Icon.
     */
    String getTabThreeIconMobileImagePath();

    /**
     * Returns first six Exchange rate Data
     */
    List<ExchangeRate> getExchangeRateList();

    /**
     * Returns the Tab one link interaction type for analytics.
     */
    String getTabOneLinkInteractionType();

    /**
     * Returns the Tab two link interaction type for analytics.
     */
    String getTabTwoLinkInteractionType();

    /**
     * Returns the Tab three link interaction type for analytics.
     */
    String getTabThreeLinkInteractionType();

}
