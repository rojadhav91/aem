package com.techcombank.core.service.impl;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.LanguageLabelModel;
import com.techcombank.core.pojo.HrefLangPojo;
import com.techcombank.core.service.SeoHref;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@Component(service = {SeoHref.class}, immediate = true)
public class SeoHrefImpl implements SeoHref {

    protected static final String X_DEFAULT = "x-default";
    protected static final String ALTERNATE_HREF = "alternate";
    protected static final String DEFAULT_LOCALE_LIST = "/etc/acs-commons/lists/system-configs/default-locale";
    protected static final String LOCALE = "locale";

    @Reference
    private Externalizer externalizer;

    @Override
    public List<HrefLangPojo> getPageList(final ResourceResolver resolver, final Page currentPagePath) {
        List<LanguageLabelModel> languageDetails = PlatformUtils.getLanguageDetails(resolver, currentPagePath, true);
        return getHrefLangPojoList(languageDetails, resolver);
    }

    /**
     * This method get the list of href language.
     *
     * @param languageDetails
     * @param resolver
     * @return List<HrefLangPojo>
     */
    private List<HrefLangPojo> getHrefLangPojoList(final List<LanguageLabelModel> languageDetails,
                                                   final ResourceResolver resolver) {
        List<HrefLangPojo> hrefLangPojoList = new ArrayList<>();
        PageManager pageManager = resolver.adaptTo(PageManager.class);
        Resource resourceGeneric = resolver.getResource(DEFAULT_LOCALE_LIST);
        Page page = resourceGeneric.adaptTo(Page.class);
        GenericList.Item defaultLanguageList = PlatformUtils
                .getGenericListConfigs(page.adaptTo(GenericList.class).getItems(), LOCALE);
        for (LanguageLabelModel languageData : languageDetails) {
            String pagePath = languageData.getLanguageUrl();
            if (StringUtils.endsWith(TCBConstants.HTMLEXTENSION, pagePath)) {
                pagePath = pagePath.substring(0, pagePath.lastIndexOf(TCBConstants.DOT));
            }
            Resource resource = resolver.resolve(pagePath);
            Page pageResource = pageManager.getPage(resource.getPath());
            if (null != pageResource) {
                HrefLangPojo hrefLangPojo = new HrefLangPojo();
                if (null != defaultLanguageList) {
                    hrefLangPojo
                            .setRel(languageData.getLanguageUrl().contains(defaultLanguageList.getValue()) ? X_DEFAULT
                                    : ALTERNATE_HREF);
                }
                hrefLangPojo.setHrefLang(languageData.getCountry() + TCBConstants.UNDERSCORE
                        + PlatformUtils.getLocale(languageData.getLanguageUrl(), resolver));
                hrefLangPojo.setHref(PlatformUtils.getMapUrl(resolver,
                        StringUtils.stripAccents(languageData.getLanguageUrl())));
                hrefLangPojoList.add(hrefLangPojo);
            }
        }

        return hrefLangPojoList;
    }
}
