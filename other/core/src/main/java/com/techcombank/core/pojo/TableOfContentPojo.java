package com.techcombank.core.pojo;


import lombok.Getter;
import lombok.Setter;

public class TableOfContentPojo {

    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    private String value;
}
