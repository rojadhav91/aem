package com.techcombank.core.servlets;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.google.gson.stream.JsonWriter;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


@Component(service = Servlet.class,
        property = {
                Constants.SERVICE_DESCRIPTION + TCBConstants.EQUALS_SYMBOL
                        + CreateGenericContentFragment.SERVICE_DESCRIPTION,
                ServletResolverConstants.SLING_SERVLET_PATHS + TCBConstants.EQUALS_SYMBOL
                        + "/bin/content/createContentFragment",
                ServletResolverConstants.SLING_SERVLET_METHODS + TCBConstants.EQUALS_SYMBOL + HttpConstants.METHOD_POST
        })
public class CreateGenericContentFragment extends SlingAllMethodsServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateGenericContentFragment.class);
    private static final long serialVersionUID = 1L;

    protected static final String SERVICE_DESCRIPTION = "Create Generic Content Fragment from CSV";
    protected static final String CF_TEMPLATE = "/conf/techcombank/web/settings/dam/cfm/models/";
    protected static final String[] EXCEPTION_LIST = new String[] {"branchId"};

    private static final String PARENT_PATH = "parentPath";
    private static final String MODEL_TYPE = "modelType";

    private static final String CSV = "csv";
    private static final String CHAR_UTF_8 = "UTF-8";

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException, ServletException {

        request.setCharacterEncoding(CHAR_UTF_8);
        response.setContentType("application/json");
        response.setHeader("Cache-Control", "nocache");
        response.setCharacterEncoding(CHAR_UTF_8);
        JsonWriter jsout = new JsonWriter(response.getWriter());
        jsout.beginObject();
        String parentPath = request.getParameter(PARENT_PATH);
        String modelType = request.getParameter(MODEL_TYPE);

        ResourceResolver resolver = request.getResourceResolver();


        if (StringUtils.isNotEmpty(parentPath) && StringUtils.isNotEmpty(modelType)) {
                Part filePart = request.getPart(CSV);
            try (InputStream fileContent = filePart.getInputStream()) {

                CSVReader header = new CSVReaderBuilder(new InputStreamReader(fileContent, CHAR_UTF_8)).withSkipLines(0).build();
                List<String> headerList = Arrays.asList(header.readNext());

                List<String> resourceList = header.readAll().stream()
                        .filter(data -> StringUtils.isNotEmpty(data[0]))
                        .map(data -> {
                            createContentFragment(resolver, parentPath, headerList, data, modelType);
                            return "Yes";
                        }).collect(Collectors.toList());


                jsout.name("status").value("Success Created total of " + resourceList.size() + " Content Fragments");
            } catch (CsvException e) {
                LOGGER.error("Exception while reading CSV from category csv ::: {}", e.getMessage(), e);
                jsout.name("status").value("Failed Created all Content Fragments");
            } finally {
                resolver.commit();
            }
        }


        jsout.endObject();
    }

    public void createContentFragment(final ResourceResolver resourceResolver, final String parentPath,
                                      final List<String> headerList,
                                      final String[] data, final String modelType) {

        try {
            Resource modelResource = resourceResolver.getResource(parentPath + "/" + data[0]);
            ContentFragment newFragment = null;
            if (modelResource == null) {
                Resource templateOrModelRsc = resourceResolver.getResource(CF_TEMPLATE + modelType);
                Resource parentResource = resourceResolver.getResource(parentPath);
                FragmentTemplate tpl = templateOrModelRsc.adaptTo(FragmentTemplate.class);
                newFragment = tpl.createFragment(parentResource, data[0], data[0]);
            } else {
                newFragment = modelResource.adaptTo(ContentFragment.class);
            }

            Iterator<ContentElement> elements = newFragment.getElements();

            while (elements.hasNext()) {
                ContentElement element = elements.next();
                for (int i = 0; i < headerList.size(); i++) {
                    if (StringUtils.equalsIgnoreCase(element.getName(), headerList.get(i))
                            || StringUtils.containsAny(element.getName(), EXCEPTION_LIST)) {
                        element.setContent(data[i], element.getContentType());
                        break;
                    }
                }
            }
        } catch (ContentFragmentException e) {
            LOGGER.error("Exception while creating CF ::: {}", e.getMessage(), e);
        }
    }
}


