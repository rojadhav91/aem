package com.techcombank.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.pojo.TabEventCard;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * A Sling Model for tab event card.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TabEventCardModel {

    /**
     * View all label for tab event card.
     */
    @ValueMapValue
    @Getter
    private String viewAll;
    /**
     * View all link label for tab event card.
     */
    @ValueMapValue
    @Getter
    private String link;
    /**
     * Weather the view all link should open in new tab.
     */
    @ValueMapValue
    @Getter
    private String newTab;

    /**
     * Whether the view all link should have a no-follow attribute.
     */
    @ValueMapValue
    @Getter
    private String noFollow;

    /**
     * Variation type for tab event card.
     */
    @ValueMapValue
    @Getter
    private String variationType;

    /**
     * List of content fragment for tab event card.
     */
    @Inject
    @Getter
    private List<TabEventCardPaths> paths;

    @Inject
    private ResourceResolver resourceResolver;

    @Getter
    private List<TabEventCard> tabEventList;

    /**
     * init method to
     */
    @PostConstruct
    void init() {
        tabEventList = new ArrayList<>();
        if (null != paths) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            for (TabEventCardPaths path : paths) {
                Resource resource = resourceResolver.getResource(path.getRootPath());
                if (null != resource) {
                    ContentFragment contentFragment = resource.adaptTo(ContentFragment.class);
                    TabEventCard tabEventCard = new TabEventCard();
                    if (null != contentFragment) {
                        tabEventCard.setImageForEvent(contentFragment.getElement("imageForEvent").getContent());
                        String inputDateStr = contentFragment.getElement("eventDate").getContent();
                        tabEventCard.setEventDate(inputDateStr);
                        tabEventCard.setEventMonth(inputDateStr);
                        String inputTimeStr = contentFragment.getElement("eventTime").getContent();
                        tabEventCard.setEventTime(inputTimeStr);
                        tabEventCard.setEventHeading(contentFragment.getElement("eventHeading").getContent());
                        tabEventCard.setEventSubHeading(contentFragment.getElement("eventSubHeading").getContent());
                        tabEventCard.setEventDesription(contentFragment.getElement("eventDesription").getContent());
                        tabEventCard.setAwardLabelText(contentFragment.getElement("awardLabelText").getContent());
                        tabEventCard.setCtaButtonLabel(contentFragment.getElement("ctaButtonLabel").getContent());
                        String ctaButtonLink = contentFragment.getElement("ctaButtonLink").getContent();
                        tabEventCard.setCtaButtonLink(PlatformUtils.isInternalLink(resourceResolver, ctaButtonLink));
                        String tagIDValue = contentFragment.getElement("eventTag").getContent();
                        String[] tagID = tagIDValue.split("\n");
                        tabEventCard.setEventTag(tagID, tagManager);
                        tabEventCard.setCtaTarget(contentFragment.getElement("ctaTarget").getContent());
                        tabEventCard.setCtaNoFollow(contentFragment.getElement("ctaNoFollow").getContent());
                        tabEventCard.setLinkUrl(ctaButtonLink);
                        tabEventList.add(tabEventCard);
                    }
                }
            }
        }
    }
}
