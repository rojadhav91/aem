package com.techcombank.core.models.impl;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.CurrencyConvertorModel;
import com.techcombank.core.models.SourceCurrencyModel;
import com.techcombank.core.models.TransactionTypeModel;
import com.techcombank.core.service.VfxRatesDataService;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class CurrencyConvertorToolModel used for mapping Properties of Currency
 * Convertor Tool Component.
 */
@Model(adaptables = Resource.class, adapters = {CurrencyConvertorModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = CurrencyConvertorModelImpl.RESOURCE_TYPE)
public class CurrencyConvertorModelImpl implements CurrencyConvertorModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/currencyconvertor";

    @OSGiService
    private VfxRatesDataService vfxRatesDataService;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * The List for transaction type.
     */
    @ChildResource
    @Getter
    private List<TransactionTypeModel> transactionTypeTab;

    /**
     * The List for Source Currency.
     */
    @ChildResource
    @Getter
    private List<SourceCurrencyModel> sourceCurrencyTab;

    /**
     * The Text for transaction.
     */
    @ValueMapValue
    @Getter
    private String transaction;

    /**
     * The Text for the foreign currency.
     */
    @ValueMapValue
    @Getter
    private String foreignCurrency;

    /**
     * The Text for source currency placeholder.
     */
    @ValueMapValue
    @Getter
    private String sourceCurrencyPlaceholder;

    /**
     * The Text for exchange info.
     */
    @ValueMapValue
    @Getter
    private String exchangeInfo;

    /**
     * The Text for target currency.
     */
    @ValueMapValue
    @Getter
    private String targetCurrency;

    /**
     * Text for target currency placeholder.
     */
    @ValueMapValue
    @Getter
    private String targetCurrencyPlaceholder;

    /**
     * The Text for currency change note.
     */
    @ValueMapValue
    private String currencyChangeNote;

    /**
     * The URL for error note.
     */
    @ValueMapValue
    @Getter
    private String errorNote;

    @Override
    public String getCurrencyChangeNote() {
        if (currencyChangeNote.contains(TCBConstants.EXCHANGE_RATE_DATE_TIME)) {
            List<String> vfxRateBasePath = vfxRatesDataService.getVfxRateBasePath(TCBConstants.EXCHANGE_RATE, false);
            String latestExchangeRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver,
                    vfxRateBasePath.get(0));
            currencyChangeNote = currencyChangeNote.replace(TCBConstants.EXCHANGE_RATE_DATE_TIME,
                    latestExchangeRateDateTime);
        }

        return currencyChangeNote;
    }

    @Override
    public String getCalculatorField() {
        String calculatorField = StringUtils.EMPTY;
        if (!transactionTypeTab.isEmpty() && !sourceCurrencyTab.isEmpty()) {
            calculatorField = transactionTypeTab.get(0).getTransactionLabel() + TCBConstants.BAR
                    + sourceCurrencyTab.get(0).getSourceCurrency();
        }

        return calculatorField;
    }

    @Override
    public String getInteractionNameType() {
        return PlatformUtils.getInteractionType(TCBConstants.CALCULATORS, TCBConstants.OTHER);
    }

}
