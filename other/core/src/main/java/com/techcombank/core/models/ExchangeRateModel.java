package com.techcombank.core.models;

/**
 * Interface For ExchangeRateModel
 *
 */
public interface ExchangeRateModel {

    /**
     * Returns the web image rendition path for the day icon.
     */
    String getDayIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the day icon.
     */
    String getDayIconMobileImagePath();

    /**
     * Returns the web image rendition path for the time icon.
     */
    String getTimeIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the time icon.
     */
    String getTimeIconMobileImagePath();

}
