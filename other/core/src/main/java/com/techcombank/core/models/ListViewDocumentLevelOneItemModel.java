package com.techcombank.core.models;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.NameConstants;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListViewDocumentLevelOneItemModel {

    @Inject
    private ResourceResolver resourceResolver;
    /**
     * Background image
     */
    @ValueMapValue
    @Getter
    private String documentPath;

    /**
     * Document date
     */
    @ValueMapValue
    @Getter
    private Date documentDate;

    /**
     * Document title
     */
    @ValueMapValue
    @Getter
    private String documentTitle;

    /**
     * Type of output
     */
    @ValueMapValue
    @Getter
    private String typeOfOutput;

    /**
     * Label
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * Icon
     */
    @ValueMapValue
    @Getter
    private String icon;

    /**
     * List of level two items
     */
    @ChildResource
    @Getter
    private List<ListViewDocumentLevelTwoItemModel> listViewDocumentItemsSecondLevel;

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private static final String CREATED_DATE_FORMAT = "yyyy-MM-dd";

    private static final String FILTER_YEAR_FORMAT = "yyyy";

    private static final String CREATED_DATE = "xmp:CreateDate";

    private static final String JCR_TITLE = NameConstants.PN_TITLE;

    @Getter
    private String formattedDate;

    @Getter
    private String year;

    /**
     * Init
     */
    @PostConstruct
    public void init() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        DateFormat yearFormat = new SimpleDateFormat(FILTER_YEAR_FORMAT);
        if (StringUtils.isNotBlank(documentPath)) {
            Resource resource = resourceResolver.getResource(documentPath);
            setDocumentData(resource);
            documentPath = PlatformUtils.isInternalLink(resourceResolver, documentPath);
        }
        if (documentDate != null) {
            formattedDate = dateFormat.format(documentDate);
            year = yearFormat.format(documentDate);
        }
    }

    private void setDocumentData(final Resource resource) throws ParseException {
        if (resource != null) {
            Asset asset = resource.adaptTo(Asset.class);
            if (asset != null) {
                if (documentDate == null && StringUtils.isNotBlank(asset.getMetadataValue(CREATED_DATE))) {
                    DateFormat createdDateFormat = new SimpleDateFormat(CREATED_DATE_FORMAT);
                    documentDate = createdDateFormat.parse(asset.getMetadataValue(CREATED_DATE));
                }
                if (StringUtils.isBlank(documentTitle)
                        && StringUtils.isNotBlank(asset.getMetadataValue(JCR_TITLE))) {
                    documentTitle = asset.getMetadataValue(JCR_TITLE);
                }
            }
        }
    }

    /**
     * To get icon for desktop
     */
    public String getWebIcon() {
        return PlatformUtils.getWebImagePath(icon);
    }
    /**
     * To get icon for mobile
     */
    public String getMobileIcon() {
        return PlatformUtils.getMobileImagePath(icon);
    }

    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(documentPath);
    }
}
