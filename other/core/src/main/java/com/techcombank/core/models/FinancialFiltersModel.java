package com.techcombank.core.models;

import com.techcombank.core.models.impl.FinancialFiltersModelImpl;
import org.apache.sling.api.resource.Resource;

import java.util.List;

/**
 * Interface For FinancialFiltersModel
 */
public interface FinancialFiltersModel {
    /**
     * The getHeading.
     */
    String getHeading();

    /**
     * The getYearLabel.
     */
    String getYearLabel();

    /**
     * The getQuarterLabel.
     */
    String getQuarterLabel();

    /**
     * The getQuarters.
     */
    List<Resource> getQuarters();

    /**
     * The getHighlights.
     */
    List<FinancialFiltersModelImpl.Highlights> getHighlights();

    /**
     * The getApplyFiltersLabel.
     */
    String getApplyFiltersLabel();

    /**
     * The getFiltersLabel.
     */
    String getFiltersLabel();

    /**
     * The getSelectLabel.
     */
    String getSelectLabel();

}

