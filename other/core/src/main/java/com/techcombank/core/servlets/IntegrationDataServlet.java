package com.techcombank.core.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.QmsIntegrationService;
import com.techcombank.core.service.StockChartService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Objects;

/**
 * Servlet that connect with mule Apis and writes json content into the
 * response.
 */
@Component(service = {Servlet.class})
@SlingServletResourceTypes(resourceTypes = "techcombank/components/structure/basepage",
        methods = {HttpConstants.METHOD_GET, HttpConstants.METHOD_POST}, selectors = {"branchlist", "stockchart",
        "branchslot", "confirmbooking", "servicelist"}, extensions =
        "json")
@ServiceDescription("Qms Data Servlet")
public class IntegrationDataServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final int S_INDEX_ZERO = 0;
    private static final int S_LENGTH_ONE = 1;
    private static final String BRANCH_LIST_API = "branchlist";
    private static final String BRANCH_SLOT_API = "branchslot";
    private static final String CONFIRM_BOOKING_API = "confirmbooking";
    private static final String SERVICE_LIST_API = "servicelist";
    private static final String STOCK_CHART_API = "stockchart";
    private static final String CHAR_UTF_8 = "UTF-8";

    @Reference
    private transient QmsIntegrationService qmsIntegrationService;

    @Reference
    private transient StockChartService stockChartService;


    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding(CHAR_UTF_8);
        String[] selectors = request.getRequestPathInfo().getSelectors();
        String apiName;
        JsonObject responseJson = new JsonObject();
        if (ArrayUtils.isNotEmpty(selectors) && selectors.length == S_LENGTH_ONE) {
            apiName = selectors[S_INDEX_ZERO];
            if (apiName.equalsIgnoreCase(STOCK_CHART_API)) {
                responseJson = callStockChartService(response);
            }
        }
        response.getWriter().write(responseJson.toString());
    }

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding(CHAR_UTF_8);
        String[] selectors = request.getRequestPathInfo().getSelectors();
        Gson gson = new Gson();
        JsonObject reqObj = new JsonObject();
        BufferedReader reader = request.getReader();
        if (Objects.nonNull(reader)) {
            reqObj = gson.fromJson(IOUtils.toString(reader), JsonObject.class);
        }
        String apiName;
        JsonObject responseJson = new JsonObject();
        if (ArrayUtils.isNotEmpty(selectors) && selectors.length == S_LENGTH_ONE) {
            apiName = selectors[S_INDEX_ZERO];
            if (apiName.equalsIgnoreCase(SERVICE_LIST_API)) {
                responseJson = callQMSServiceList(response, request, reqObj);
            }
            if (apiName.equalsIgnoreCase(BRANCH_LIST_API)) {
                responseJson = callQMSBranchService(response, request, reqObj);
            }
            if (apiName.equalsIgnoreCase(BRANCH_SLOT_API)) {
                responseJson = callQMSBranchSlotService(response, request, reqObj);
            }
            if (apiName.equalsIgnoreCase(CONFIRM_BOOKING_API)) {
                responseJson = callQMSBookingSubmitService(response, request, reqObj);
            }

        }
        response.getWriter().write(responseJson.toString());
    }

    /**
     * This method set the status code from API response to HTTP response
     *
     * @param response
     * @param responseJson
     */
    private void setStatusCode(final SlingHttpServletResponse response, final JsonObject responseJson) {
        if (null != responseJson.get(TCBConstants.ERROR_CODE)) {
            response.setStatus(responseJson.get(TCBConstants.ERROR_CODE).getAsInt());
        }
    }

    /**
     * This method calls the Stock service to get the Stock Chart data
     *
     * @param response
     * @return JsonObject stockChartResponseJson
     */
    private JsonObject callStockChartService(final SlingHttpServletResponse response) {
        JsonObject stockChartResponseJson;
        stockChartResponseJson = stockChartService.fetchStockChartData(false);
        if (null != stockChartResponseJson.get(TCBConstants.API_ERROR_VALUE) && stockChartResponseJson
                .get(TCBConstants.API_ERROR_VALUE).getAsString().equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
            stockChartResponseJson = stockChartService.fetchStockChartData(true);
        }
        setStatusCode(response, stockChartResponseJson);


        return stockChartResponseJson;
    }

    /**
     * This method calls the QMS Service list to get the list of available service
     *
     * @param response
     * @param request
     * @param reqObj
     * @return JsonObject serviceListJson
     */
    private JsonObject callQMSServiceList(final SlingHttpServletResponse response,
                                          final SlingHttpServletRequest request, final JsonObject reqObj) {
        JsonObject serviceListJson;
        serviceListJson = qmsIntegrationService.fetchQMSServiceList(false, request, reqObj);
        if (null != serviceListJson.get(TCBConstants.API_ERROR_VALUE) && serviceListJson
                .get(TCBConstants.API_ERROR_VALUE).getAsString().equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
            serviceListJson = qmsIntegrationService.fetchQMSServiceList(true, request, reqObj);
        }
        setStatusCode(response, serviceListJson);
        return serviceListJson;
    }

    /**
     * This method calls the QMS Branch service to get the branch list
     *
     * @param response
     * @param request
     * @param reqObj
     * @return JsonObject branchListResponseJson
     */
    private JsonObject callQMSBranchService(final SlingHttpServletResponse response,
                                            final SlingHttpServletRequest request, final JsonObject reqObj) {
        JsonObject branchListResponseJson;
        branchListResponseJson = qmsIntegrationService.fetchQMSBranchList(false, request, reqObj);
        if (null != branchListResponseJson.get(TCBConstants.API_ERROR_VALUE) && branchListResponseJson
                .get(TCBConstants.API_ERROR_VALUE).getAsString().equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
            branchListResponseJson = qmsIntegrationService.fetchQMSBranchList(true, request, reqObj);
        }
        setStatusCode(response, branchListResponseJson);

        return branchListResponseJson;
    }

    /**
     * This method calls the QMS Branch Slot service to get the branch available slot timings
     *
     * @param response
     * @param request
     * @param reqObj
     * @param type
     * @param branchId
     * @return JsonObject branchSlotResponseJson
     */
    private JsonObject callQMSBranchSlotService(final SlingHttpServletResponse response,
                                                final SlingHttpServletRequest request, final JsonObject reqObj) {
        JsonObject branchSlotResponseJson;
        branchSlotResponseJson = qmsIntegrationService.fetchQMSBranchSlot(false, request, reqObj);
        if (null != branchSlotResponseJson.get(TCBConstants.API_ERROR_VALUE) && branchSlotResponseJson
                .get(TCBConstants.API_ERROR_VALUE).getAsString().equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
            branchSlotResponseJson = qmsIntegrationService.fetchQMSBranchSlot(true, request, reqObj);
        }
        setStatusCode(response, branchSlotResponseJson);

        return branchSlotResponseJson;
    }

    /**
     * This method calls the QMS Branch Slot service to get the branch available slot timings
     *
     * @param response
     * @param request
     * @param reqObj
     * @return JsonObject confirmBookingResponseJson
     */
    private JsonObject callQMSBookingSubmitService(final SlingHttpServletResponse response,
                                                   final SlingHttpServletRequest request, final JsonObject reqObj) {
        JsonObject confirmBookingResponseJson;
        confirmBookingResponseJson = qmsIntegrationService.qmsSubmitBooking(false, request, reqObj);
        if (null != confirmBookingResponseJson.get(TCBConstants.API_ERROR_VALUE) && confirmBookingResponseJson
                .get(TCBConstants.API_ERROR_VALUE).getAsString().equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
            confirmBookingResponseJson = qmsIntegrationService.qmsSubmitBooking(true, request, reqObj);
        }
        setStatusCode(response, confirmBookingResponseJson);
        return confirmBookingResponseJson;
    }

}
