package com.techcombank.core.models;

import com.techcombank.core.pojo.Analytics;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

/**
 * This model class is for Mapping Page Properties to Analytics Object.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AnalyticsPageModel {
    /**
     * Current Page Resource Object.
     */
    @SlingObject
    private Resource currentResource;
    /**
     * Analytics Object.
     */
    @ValueMapValue
    @Getter
    private Analytics analyticsObj;

    /**
     * Init Method populate resource properties
     * to analytics object.
     */
    @PostConstruct
    protected void init() {
        analyticsObj = currentResource.adaptTo(Analytics.class);
    }
}
