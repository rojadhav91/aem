package com.techcombank.core.pojo;

import lombok.Getter;
import lombok.Setter;

public class Articles {

    @Getter
    @Setter
    private String webImage;

    @Getter
    @Setter
    private String mobileImage;

    @Getter
    @Setter
    private String altText;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String  articleCreationDate;

    @Getter
    @Setter
    private String path;

    @Getter
    @Setter
    private String openInNewTab;

}
