package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class TableCompareRowModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableCompareRowModel {

    /**
     * Row Value
     * - Description: To author row value
    */
    @ValueMapValue
    @Getter
    private String rowValue;

}
