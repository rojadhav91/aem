package com.techcombank.core.service;

public interface JwtTokenService {
    String getJwtToken(boolean isTokenExpired);
}
