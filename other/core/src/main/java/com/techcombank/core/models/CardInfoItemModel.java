package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CardInfoItemModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CardInfoItemModel extends CardBaseModel {

    /**
     * Image Position
    */
    @ValueMapValue
    @Getter
    private String imagePosition;

    /**
     * Card Description 2
     * - Description: To author 2nd Card description
    */
    @ValueMapValue
    @Getter
    private String cardDescription2;

    /**
     * Is CTA Needed ?
     * - Description: To choose if CTA is needed
    */
    @ValueMapValue
    @Getter
    private boolean ctaNeeded;

}
