package com.techcombank.core.models;

import java.util.List;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * The Class CreditRatingModel.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CreditRatingModel.class)
public class CreditRatingModel {
    private String languageTitle = "";

    @Inject
    private Page currentPage;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Filter Year Label
     * - Description: To author label for year in filter
    */
    @ValueMapValue
    @Getter
    private String filterYearLabel;

    /**
     * Filter Select Label
     * - Description: To author label for select in filter
    */
    @ValueMapValue
    @Getter
    private String filterSelectLabel;

    /**
     * Filter All Label
     * - Description: To author label for all option in filter
    */
    @ValueMapValue
    @Getter
    private String filterAllLabel;

    /**
     * Credit Rating Report List
     */
    @ChildResource
    @Getter
    private List<CreditRatingReportModel> creditRatingReportList;

    /**
     * Year for filter list
     */
    @ChildResource
    @Getter
    private List<YearForFilterModel> yearForFilterList;

    @PostConstruct
    public void init() {
        try {
            languageTitle = PlatformUtils.getLanguageTitle(currentPage.getContentResource(), false, resourceResolver);
        } catch (RuntimeException e) {
            languageTitle = "";
        }
    }

    public String getLanguageTitle() {
        return languageTitle;
    }
}
