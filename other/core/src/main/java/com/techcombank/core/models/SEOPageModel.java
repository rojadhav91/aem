package com.techcombank.core.models;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.pojo.HrefLangPojo;
import com.techcombank.core.pojo.SEO;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.SeoHref;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;


/**
 * This model class is for Mapping Page Properties to SEO Object.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SEOPageModel {

    @OSGiService
    private SeoHref seoHref;

    @OSGiService
    private ResourceResolverService resourceResolverService;

    /**
     * Current Page Resource Object.
     */
    @SlingObject
    private Resource currentResource;


    /**
     * CurrentPage.
     */
    @ScriptVariable
    private Page currentPage;

    /**
     * Externalizer.
     */
    @Inject
    private Externalizer externalizer;

    /**
     * SEO Object.
     */
    @Getter
    private SEO seoObj;

    /**
     * Pagepath.
     */
    @Getter
    private String pagePath;

    /**
     * PageTitle.
     */
    @Getter
    private String pageTitle;

    /**
     * PageDescription.
     */
    @Getter
    private String pageDescription;

    private ResourceResolver resolver;

    /**
     * Init Method populate resource properties
     * to seo object.
     */
    @PostConstruct
    protected void init() {
        resolver = resourceResolverService.getReadResourceResolver();
        if (null != currentPage.getTitle()) {
            pageTitle = currentPage.getTitle();
        }
        if (null != currentPage.getDescription()) {
            pageDescription = currentPage.getDescription();
        }
        seoObj = currentResource.adaptTo(SEO.class);
        pagePath = externalizer.publishLink(resolver, currentPage.getPath());
    }


    public List<HrefLangPojo> getHrefLangList() {
        return seoHref.getPageList(resolver, currentPage);
    }
}
