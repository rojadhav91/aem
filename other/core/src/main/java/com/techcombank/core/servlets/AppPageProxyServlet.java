package com.techcombank.core.servlets;

import com.day.cq.commons.Externalizer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.JsonFilterService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

@Component(service = Servlet.class,
        property = {"process.label=TCB App Page Proxy Servlet",
                Constants.SERVICE_DESCRIPTION + "=This servlet proxies the model.json API."})
@SlingServletResourceTypes(resourceTypes = "sling/servlet/default",
        methods = HttpConstants.METHOD_GET,
        extensions = "json",
        selectors = "mobile")
public class AppPageProxyServlet extends SlingAllMethodsServlet {
    private static final long serialVersionUID = 1L;

    private static final String MODEL_PATH = "/_jcr_content/root/container.model.json";

    private static final String RDBAPP_PATH = "/content/techcombank/rdb-app";

    private static final String CDBAPP_PATH = "/content/techcombank/cdb-app";

    private final transient Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private transient Externalizer externalizer;

    @Reference
    private transient JsonFilterService jsonFilterService;
    @Override
    protected void doGet(final SlingHttpServletRequest request,
                         final SlingHttpServletResponse response) throws IOException {
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        JsonObject responseJson = new JsonObject();
        responseJson.addProperty(TCBConstants.SUCCESS, false);

        Resource pageResource = request.getResource();
        ResourceResolver resourceResolver = request.getResourceResolver();

        if (pageResource.isResourceType(TCBConstants.CQ_PAGE)
                && (pageResource.getPath().contains(RDBAPP_PATH) || pageResource.getPath().contains(CDBAPP_PATH))) {

            String domain = PlatformUtils.getExternalizer(externalizer, resourceResolver, TCBConstants.SOURCE_APP);
            String pagePath = domain + pageResource.getPath() + MODEL_PATH;
            String imageDomain = PlatformUtils.getAppDomainExternalizer(externalizer, resourceResolver);
            Map<String, Object> map = jsonFilterService.getHTTPResponse(responseJson, pagePath);
            Gson gson = new Gson();

            JsonElement dataItems = gson.toJsonTree(map.get(TCBConstants.COLON_ITEMS));
            JsonElement dataItemsOrder = gson.toJsonTree(map.get(TCBConstants.COLON_ITEMSORDER));
            responseJson.add(TCBConstants.ITEMS,
                jsonFilterService.processJSON(dataItems, jsonFilterService.getJsonFilter(), imageDomain, domain)
            );
            responseJson.add(TCBConstants.COLON_ITEMSORDER, dataItemsOrder);
            responseJson.remove(TCBConstants.SUCCESS);
            responseJson.addProperty(TCBConstants.SUCCESS, TCBConstants.TRUE);
        } else {
            logger.error("Invalid resource type");
            responseJson.addProperty(TCBConstants.ERROR, TCBConstants.INVALID_RESOURCE_PATH);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        out.print(getJsonArray(responseJson));
        out.flush();
    }

    private JsonObject getJsonArray(final JsonObject convertedObject) {
        if (convertedObject.has(TCBConstants.ITEMS) && convertedObject.
                get(TCBConstants.ITEMS).isJsonObject()) {
            JsonObject itemsObject = convertedObject.getAsJsonObject(TCBConstants.ITEMS);
            Set<Map.Entry<String, JsonElement>> entries = itemsObject.entrySet();
            JsonArray jsonArray = new JsonArray();
            for (Map.Entry<String, JsonElement> entry : entries) {
                jsonArray.add(entry.getValue());
            }
            convertedObject.remove(TCBConstants.ITEMS);
            convertedObject.add(TCBConstants.ITEMS, jsonArray);
        }
        return convertedObject;
    }
}
