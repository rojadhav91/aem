package com.techcombank.core.models;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import java.util.List;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = MilestonePanelModel.RESOURCE_TYPE)
public class MilestonePanelModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/milestonepanel";
    private static final String COMPONENT_NAME = "Milestone Panel";

    /**
     * The List for Timeline Items.
     */
    @ChildResource
    @Getter
    private List<TimelineModel> timelines;

    /**
     * @return the Web Interation Value for analytics
     */
    public String getWebInteractionValue() {
        return PlatformUtils.getInteractionType(COMPONENT_NAME, TCBConstants.OTHER);
    }
}
