package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DeviceLookStepsCarouselItemModel {
    /**
     * Image
     */
    @ValueMapValue
    @Getter
    private String image;

    /**
    * Returns the mobile image rendition path for the device look step image.
    */
    public String getMobileImagePath() {
    return PlatformUtils.getMobileImagePath(image);
    }

    /**
    * Returns the web image rendition path for the device look step image.
    */
    public String getWebImagePath() {
    return PlatformUtils.getWebImagePath(image);
    }

    /**
     * Alt text
     */
    @ValueMapValue
    @Getter
    private String altText;

    /**
     * Step number
     */
    @ValueMapValue
    @Getter
    private String stepNumber;

    /**
     * Description
     */
    @ValueMapValue
    @Getter
    private String description;
}
