package com.techcombank.core.models;

import lombok.Getter;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Other Rate Content Fragment property to
 * Java Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = OtherRate.class)
public class OtherRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * ceiling
     */
    @ValueMapValue
    @Getter
    private String ceiling;

    /**
     * central
     */
    @ValueMapValue
    @Getter
    private String central;

    /**
     * floor
     */
    @ValueMapValue
    @Getter
    private String floor;
}
