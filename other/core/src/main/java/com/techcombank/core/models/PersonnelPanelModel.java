package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class PersonnelPanelModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = PersonnelPanelModel.class)
public class PersonnelPanelModel {

    /**
     * Personnel description
     */
    @ValueMapValue
    @Getter
    private String personnelDescription;

    /**
     * Personnel name
     */
    @ValueMapValue
    @Getter
    private String personnelName;

    /**
     * Personnel position
     */
    @ValueMapValue
    @Getter
    private String personnelPosition;

    /**
     * Panel button text
     */
    @ValueMapValue
    @Getter
    private String panelButtonText;

    /**
     * Panel button link URL
     */
    @ValueMapValue
    @Getter
    private String panelButtonLinkUrl;

    /**
     * Open in new tab
     */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No follow for SEO
     */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Right panel background color
     */
    @ValueMapValue
    @Getter
    private String rightPanelBackgroundColor;

    /**
     * Personnel image
     */
    @ValueMapValue
    @Getter
    private String personnelImage;

    /**
     * Image alt text
     */
    @ValueMapValue
    @Getter
    private String imageAltText;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(panelButtonLinkUrl);
    }

}
