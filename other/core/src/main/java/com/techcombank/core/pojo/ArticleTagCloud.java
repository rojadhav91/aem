package com.techcombank.core.pojo;

import lombok.Getter;
import lombok.Setter;

public class ArticleTagCloud {
    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String path;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String openInNewTab;
}
