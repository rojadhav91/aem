package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * A Sling model representing an individual list event tile item.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListEventTileItem {

    protected static final String RESOURCE_TYPE = "techcombank/components/listeventtile";

    /**
     * The URL of the card image associated with the list event tile item.
     */
    @ValueMapValue
    @Getter
    private String cardImage;

    /**
     * The alternative text for the card image.
     */
    @ValueMapValue
    @Getter
    private String cardImageAltText;

    /**
     * The label for the event card.
     */
    @ValueMapValue
    @Getter
    private String cardLabelText;

    /**
     * The date of the event.
     */
    @ValueMapValue
    @Getter
    private String eventDate;

    /**
     * The title of the event card.
     */
    @ValueMapValue
    @Getter
    private String cardTitle;

    /**
     * The description of the event card.
     */
    @ValueMapValue
    @Getter
    private String cardDescription;

    /**
     * The URL of the icon associated with the link.
     */
    @ValueMapValue
    @Getter
    private String linkIcon;

    /**
     * The alternative text for the link icon.
     */
    @ValueMapValue
    @Getter
    private String linkIconAltText;

    /**
     * The text for the link.
     */
    @ValueMapValue
    @Getter
    private String linkText;

    /**
     * The URL to which the link points.
     */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Indicates whether the link should open in a new tab or not.
     */
    @ValueMapValue
    @Getter
    private String openInNewTab;

    /**
     * Indicates whether the link should have a nofollow attribute.
     */
    @ValueMapValue
    @Getter
    private String noFollow;

    @OSGiService
    private ResourceResolverService resourceResolverService;

    @Getter
    private String linkInteraction;

    /**
     * For date format & interaction Value
     */
    @PostConstruct
    protected void init() {
        try (ResourceResolver resourceResolver = resourceResolverService.getReadResourceResolver()) {

            linkUrl = PlatformUtils.getMapUrl(resourceResolver, linkUrl);

            if (null != eventDate) {
                Instant instant = Instant.parse(eventDate);
                LocalDateTime localDateTime = instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                eventDate = localDateTime.format(formatter);
            }

            if (null != resourceResolver) {
                String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
                String linkType = PlatformUtils.getUrlLinkType(linkUrl);
                 linkInteraction = PlatformUtils.getInteractionType(componentName, linkType);
            } else {
                linkInteraction = StringUtils.EMPTY;
            }
        }
    }

}
