package com.techcombank.core.models;

/**
 * Interface For NavPanelModel
 *
 */
public interface NavPanelModel {

    /**
     * Cta Type one link url
     */
    String getCtaTypeOneLinkURL();

    /**
     * Cta Type Two link url
     */
    String getCtaTypeTwoLinkURL();

    /**
     * Cta Type one link url type
     */
    String getCtaTypeOneLinkURLType();

    /**
     * Cta Type Two link url type
     */
    String getCtaTypeTwoLinkURLType();

    /**
     * Returns the web image rendition path for the cta Type One Icon.
     */
    String getCtaTypeOneIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the cta Type One Icon.
     */
    String getCtaTypeOneIconMobileImagePath();

    /**
     * Returns the web image rendition path for the cta Type Two Icon.
     */
    String getCtaTypeTwoIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the cta Type Two Icon.
     */
    String getCtaTypeTwoIconMobileImagePath();

}
