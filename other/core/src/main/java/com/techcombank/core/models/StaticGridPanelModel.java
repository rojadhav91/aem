package com.techcombank.core.models;

import java.util.List;

import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.Model;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class StaticGridPanelModel.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = HeaderModel.RESOURCE_TYPE)
public class StaticGridPanelModel {
    /** The heading */
    @ValueMapValue
    @Getter
    private String heading;

    /** The layout (3/4/5 columns) */
    @ValueMapValue
    @Getter
    private String layout;

    /** The icon image alignment (left/center) */
    @ValueMapValue
    @Getter
    private String iconImageAlignment;

    /**
     * Static grid panel items.
     */
    @ChildResource
    @Getter
    private List<StaticGridPanelItemModel> staticGridPanelItems;
}
