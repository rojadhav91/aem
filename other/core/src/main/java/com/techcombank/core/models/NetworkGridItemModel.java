package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Icon images Properties of Network grid
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NetworkGridItemModel {
    /**
     * The grid panel list number.
     */
    @ValueMapValue
    @Getter
    private int gridPanelListNumber;

    /**
     * The grid panel list title text.
     */
    @ValueMapValue
    @Getter
    private String gridPanelListTitleText;

    /**
     * The grid panel list subtitle.
     */
    @ValueMapValue
    @Getter
    private String gridPanelListSubTitle;

    /**
     * The display image.
     */
    @ValueMapValue
    @Getter
    private String gridDisplayImage;

    /**
    * Returns the mobile image rendition path for the image.
    */
    public String getMobileGridDisplayImagePath() {
        return PlatformUtils.getMobileImagePath(gridDisplayImage);
    }

    /**
    * Returns the web image rendition path for the image.
    */
    public String getWebGridDisplayImagePath() {
        return PlatformUtils.getWebImagePath(gridDisplayImage);
    }

    /**
     * The display image alternative text.
     */
    @ValueMapValue
    @Getter
    private String displayImageAltText;

    /**
     * The grid image tag.
     */
    @ValueMapValue
    @Getter
    private String gridImageTag;

    /**
     * The grid image title.
     */
    @ValueMapValue
    @Getter
    private String gridImageTitle;

    /**
     * The grid image description.
     */
    @ValueMapValue
    @Getter
    private String gridImageDescription;
}
