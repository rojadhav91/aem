package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticlesRelatedPostService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;

/**
 * The Class ArticleRelatedPostModel.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticlesRelatedPostModel {

    @SlingObject
    private Resource resource;

    @Inject
    private Page currentPage;

    @SlingObject
    private ResourceResolver resourceResolver;

    /**
     * The label for the Related Post Section Title.
     */
    @ValueMapValue
    @Getter
    private String titleText;

    /**
     * The label for the view more Label.
     */
    @ValueMapValue
    @Getter
    private String viewMore;

    /**
     * Open In New Tab
     */
    @ValueMapValue
    @Getter
    private String newTab;

    /**
     * Image
     */
    @ValueMapValue
    @Getter
    private String defaultThumbnailImage;

    @OSGiService
    private ArticlesRelatedPostService articlesRelatedPostService;

    public List<Articles> getRelatedPost() {
        List<Articles> articlesList =
                articlesRelatedPostService.getArticlesRelatedPosts(resource, currentPage, resourceResolver);
        if (StringUtils.isNotEmpty(defaultThumbnailImage)) {
            for (Articles art : articlesList) {
                if (StringUtils.isEmpty(art.getWebImage())) {
                    if (defaultThumbnailImage.startsWith(TCBConstants.CONTENT_DAM_TECHCOMBANK_PATH)) {
                        art.setWebImage(PlatformUtils.getWebImagePath(defaultThumbnailImage));
                        art.setMobileImage(PlatformUtils.getWebImagePath(defaultThumbnailImage));
                    } else {
                        art.setWebImage(defaultThumbnailImage);
                        art.setMobileImage(defaultThumbnailImage);
                    }
                }
            }
        }
        return articlesList;
    }

    public String getHideStatus() {
        return currentPage.getProperties().get(TCBConstants.ARTICLE_RELATED_POST_VISIBILITY_FLAG, String.class);
    }
}
