package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class ArticleCardCrossItemModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticleCardCrossItemModel extends CardBaseModel {

    /**
     * CTA Link
     * - Description: Link for the card
    */
    @ValueMapValue
    @Getter
    private String ctaLink;

    /**
     * Open In New Tab
    */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No Follow
    */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(ctaLink);
    }

}
