package com.techcombank.core.models;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.ResourceResolverService;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import javax.annotation.PostConstruct;

/**
 * The Class MastHeadSpecialBannerModel used for mapping Properties of MastHead
 * special Banner Component.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
resourceType = MastHeadSpecialBannerModel.RESOURCE_TYPE)
public class MastHeadSpecialBannerModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/mastheadspecialbanner";

    @OSGiService
    private ResourceResolverService resourceResolverService;
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    /**
     * The Image for background banner.
     */
    @ValueMapValue
    @Getter
    private String bgBannerImage;

    /**
     * The Image for Mobile background banner.
     */
    @ValueMapValue
    @Getter
    private String bgBannerMobileImage;

    /**
     * The Alt Text for the Banner Background Image.
     */
    @ValueMapValue
    @Getter
    private String bgBannerImgAlt;

    /**
     * The Image for banner.
     */
    @ValueMapValue
    @Getter
    private String bannerImage;

    /**
     * The Alt Text for the Banner Image.
     */
    @ValueMapValue
    @Getter
    private String bannerImgAlt;

    /**
     * The Text for highlighted Text field.
     */
    @ValueMapValue
    @Getter
    private String highlightedtext;

    /**
     * The Text for banner title.
     */
    @ValueMapValue
    @Getter
    private String bannerTitle;

    /**
     * The Text for banner sub title.
     */
    @ValueMapValue
    @Getter
    private String bannerSubTitle;

    /**
     * Text to banner description.
     */
    @ValueMapValue
    @Getter
    private String bannerDesc;

    /**
     * The Text for button Label.
     */
    @ValueMapValue
    @Getter
    private String buttonLabel;

    /**
     * The URL for button link.
     */
    @ValueMapValue
    private String buttonlinkURL;

    /**
     * To check the button link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String buttonTarget;

    /**
     * To check button seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean buttonNoFollow;

    /**
     * To check button arrow is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean hideButtonArrow;

    /**
     * To check if this is register/contact CTA
     */
    @ValueMapValue
    @Getter
    private boolean registerContactCta;

    /**
     * @return the buttonlinkURL
     */
    public String getButtonlinkURL() {
        return PlatformUtils.isInternalLink(resourceResolver, buttonlinkURL);
    }

    /**
     * @return the Web Interation Value for analytics
     */
    public String getWebInteractionValue() {
        if (null != resourceResolver) {
            String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
            String linkType = PlatformUtils.getUrlLinkType(buttonlinkURL);
            return PlatformUtils.getInteractionType(componentName, linkType);
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * @return the Register now/Contact us click info Value for analytics
     */
    public String getRegisterContactClickInfoValue() {
        return PlatformUtils.getClickInfo(TCBConstants.CTA, buttonLabel);
    }

    /**
     * @return the default click info Value for analytics
     */
    public String getDefaultClickInfoValue() {
        return PlatformUtils.getClickInfo(TCBConstants.LINK_CLICK, buttonLabel);
    }

}
