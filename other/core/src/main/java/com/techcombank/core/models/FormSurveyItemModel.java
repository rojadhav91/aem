package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class FormSurveyItemModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormSurveyItemModel {

    /**
     * Title
     * - Description: Title of answer
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Value
     * - Description: Value of answer, to be sent to CRM
    */
    @ValueMapValue
    @Getter
    private String value;

}
