package com.techcombank.core.service;
/**
 * An interface representing a service for accessing run mode information.
 *
 */
public interface RunmodeService {
    /**
     * Retrieves the name of the current environment.
     *
     * @return The name of the environment.
     */
    String  getEnvironmentName();

    /**
     * Retrieves the type of the current environment.
     *
     * @return The type of the environment.
     */
    String  getEnvironmentType();
}
