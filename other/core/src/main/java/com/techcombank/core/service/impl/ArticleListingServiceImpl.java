package com.techcombank.core.service.impl;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticleListingService;
import com.techcombank.core.utils.ArticleUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component(service = ArticleListingService.class, immediate = true)
public class ArticleListingServiceImpl implements ArticleListingService {

    private static final Logger LOG = LoggerFactory.getLogger(ArticleListingServiceImpl.class);

    @Override
    public List<Articles> getArticlesList(final SlingHttpServletRequest request, final Locale locale)
            throws RepositoryException {

        List<Articles> articlesList = new ArrayList<>();

        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        String currentPagePath = request.getPathInfo();
        LOG.info("LOCALE = {} ", locale);
        LOG.info("Current Page Path : {} ", currentPagePath);
        String queryString = request.getQueryString();
        if (queryString != null && !(queryString.equalsIgnoreCase(TCBConstants.AUTHOR_QUERY_STRING))) {
            String tagName = queryString.substring(queryString.indexOf("=") + 1);
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            String tagValue = TCBConstants.ARTICLE_LISTING_ROOT_PATH + tagName;
            Tag tag = tagManager.resolve(tagValue);

            LOG.info("TAG VALUE = {} ", tag.getTitle());

            Map<String, String> articleMap = new HashMap<>();
            articleMap.put(TCBConstants.PATH,
                    TCBConstants.TCB_WEB_BASE_PATH + locale + TCBConstants.SLASH);
            articleMap.put(TCBConstants.PROPERTY_2, TCBConstants.JCR_CONTENT + TCBConstants.CQ_TEMPLATE);
            articleMap.put(TCBConstants.PROPERTYVALUE_2, TCBConstants.ARTICLE_PAGE_TEMPLATE_PATH);
            articleMap.put(TCBConstants.PROPERTY_1, TCBConstants.JCR_CONTENT + TCBConstants.CQ_TAGS);
            articleMap.put(TCBConstants.PROPERTYVALUE_1, tagValue);
            articleMap.put(TCBConstants.ORDER_BY,
                    TCBConstants.AT + TCBConstants.JCR_CONTENT + TCBConstants.ARTICLE_CREATION_DATE);
            articleMap.put(TCBConstants.ORDER_BY_SORT, TCBConstants.DESCENDING);
            articleMap.put(TCBConstants.RESULT_LIMIT, TCBConstants.ALL_RESULT);
            QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
            Query query = queryBuilder.createQuery(PredicateGroup.create(articleMap), session);

            SearchResult result = query.getResult();
            if (!result.getHits().isEmpty()) {
                articlesList = ArticleUtils.getArticles(result, currentPagePath, resourceResolver,
                        TCBConstants.ARTICLE_LISTING_VALUE);
            }
        }
        return articlesList;
    }
}
