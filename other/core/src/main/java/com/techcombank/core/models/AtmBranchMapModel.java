package com.techcombank.core.models;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.RunmodeService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This model class is for mapping ATM Branch Map details
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = AtmBranchMapModel.RESOURCE_TYPE)
public class AtmBranchMapModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/atmbranchmap";
    protected static final String GENERIC_LIST_PATH = "/etc/acs-commons/lists/system-configs/google-map";
    protected static final String PROD = "prod";

    protected static final String PROD_API_KEY = "AEM_PROD_API_KEY";

    protected static final String NON_PROD_API_KEY = "AEM_NON_PROD_API_KEY";


    /**
     * The Text for the personal label.
     */
    @ValueMapValue
    @Getter
    private String personalLabel;

    /**
     * The Icon for personal filed
     */
    @ValueMapValue
    @Getter
    private String personalIcon;

    /**
     * The Alt Text for personal icon.
     */
    @ValueMapValue
    @Getter
    private String personalIconAlt;

    /**
     * The Text for the business label.
     */
    @ValueMapValue
    @Getter
    private String businessLabel;

    /**
     * The Icon for business filed.
     */
    @ValueMapValue
    @Getter
    private String businessIcon;

    /**
     * The Alt Text for business icon.
     */
    @ValueMapValue
    @Getter
    private String businessIconAlt;

    /**
     * The Text for the title label.
     */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * The Icon for search filed.
     */
    @ValueMapValue
    @Getter
    private String searchIcon;

    /**
     * The Alt Text for search icon.
     */
    @ValueMapValue
    @Getter
    private String searchIconAlt;

    /**
     * The Text for the search place holder.
     */
    @ValueMapValue
    @Getter
    private String searchPlaceHolder;

    /**
     * The Text for the select value in drop down.
     */
    @ValueMapValue
    @Getter
    private String selectDropdown;

    /**
     * The Text for the branch value in drop down.
     */
    @ValueMapValue
    @Getter
    private String branchDropdown;

    /**
     * The Text for the district label.
     */
    @ValueMapValue
    @Getter
    private String districtLabel;

    /**
     * The Text for the atm label.
     */
    @ValueMapValue
    @Getter
    private String atmLabel;

    /**
     * The Icon for atm filed.
     */
    @ValueMapValue
    @Getter
    private String atmIcon;

    /**
     * The Alt Text for atm icon.
     */
    @ValueMapValue
    @Getter
    private String atmIconAlt;

    /**
     * The Text for the cdm label.
     */
    @ValueMapValue
    @Getter
    private String cdmLabel;

    /**
     * The Icon for cdm filed.
     */
    @ValueMapValue
    @Getter
    private String cdmIcon;

    /**
     * The Alt Text for cdm icon.
     */
    @ValueMapValue
    @Getter
    private String cdmIconAlt;

    /**
     * The Text for the branch label.
     */
    @ValueMapValue
    @Getter
    private String branchLabel;

    /**
     * The Icon for branch filed.
     */
    @ValueMapValue
    @Getter
    private String branchIcon;

    /**
     * The Alt Text for branch icon.
     */
    @ValueMapValue
    @Getter
    private String branchIconAlt;

    /**
     * The Text for the result message.
     */
    @ValueMapValue
    @Getter
    private String resultMessage;

    /**
     * The Text for the no option message.
     */
    @ValueMapValue
    @Getter
    private String noOptionMessage;

    /**
     * The Text for the back label.
     */
    @ValueMapValue
    @Getter
    private String backLabel;

    /**
     * The Text for the hotline label.
     */
    @ValueMapValue
    @Getter
    private String hotlineLabel;

    /**
     * The Text for the address label.
     */
    @ValueMapValue
    @Getter
    private String addressLabel;

    /**
     * The Text for the direction label.
     */
    @ValueMapValue
    @Getter
    private String directionLabel;

    @OSGiService
    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;
    @OSGiService
    private RunmodeService runmodeService;

    /**
     * Returns the web image rendition path for the personal icon.
     */
    public String getPersonalIconWebImagePath() {
        return PlatformUtils.getWebImagePath(personalIcon);
    }

    /**
     * Returns the mobile image rendition path for the personal icon.
     */
    public String getPersonalIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(personalIcon);
    }

    /**
     * Returns the web image rendition path for the business icon.
     */
    public String getBusinessIconWebImagePath() {
        return PlatformUtils.getWebImagePath(businessIcon);
    }

    /**
     * Returns the mobile image rendition path for the business icon.
     */
    public String getBusinessIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(businessIcon);
    }

    /**
     * Returns the web image rendition path for the search icon.
     */
    public String getSearchIconWebImagePath() {
        return PlatformUtils.getWebImagePath(searchIcon);
    }

    /**
     * Returns the mobile image rendition path for the search icon.
     */
    public String getSearchIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(searchIcon);
    }

    /**
     * Returns the web image rendition path for the atm icon.
     */
    public String getAtmIconWebImagePath() {
        return PlatformUtils.getWebImagePath(atmIcon);
    }

    /**
     * Returns the mobile image rendition path for the atm icon.
     */
    public String getAtmIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(atmIcon);
    }

    /**
     * Returns the web image rendition path for the cdm icon.
     */
    public String getCdmIconWebImagePath() {
        return PlatformUtils.getWebImagePath(cdmIcon);
    }

    /**
     * Returns the mobile image rendition path for the cdm icon.
     */
    public String getCdmIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(cdmIcon);
    }

    /**
     * Returns the web image rendition path for the branch icon.
     */
    public String getBranchIconWebImagePath() {
        return PlatformUtils.getWebImagePath(branchIcon);
    }

    /**
     * Returns the mobile image rendition path for the branch icon.
     */
    public String getBranchIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(branchIcon);
    }

    /**
     * Returns the google map secret value.
     */
    public String getGoogleMapValue() {
        resourceResolver = resourceResolverService.getWriteResourceResolver();
        Resource resource = resourceResolver.getResource(GENERIC_LIST_PATH);
        Page page = resource.adaptTo(Page.class);
        GenericList genericList = page.adaptTo(GenericList.class);
        if (null != genericList.getItems()) {
            if (runmodeService.getEnvironmentName().equalsIgnoreCase(PROD)) {
                return PlatformUtils.getGenericListConfigs(page.adaptTo(GenericList.class).getItems(), PROD_API_KEY)
                        .getValue();
            } else {
                return PlatformUtils.getGenericListConfigs(page.adaptTo(GenericList.class).getItems(), NON_PROD_API_KEY)
                        .getValue();
            }
        }
        return StringUtils.EMPTY;
    }
}
