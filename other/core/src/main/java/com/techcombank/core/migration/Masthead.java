package com.techcombank.core.migration;

import com.google.gson.annotations.Expose;
import lombok.Data;
/**
 * This class in mapping between Article Masthead properties
 * on Page to Java Class.
 */
@Data
public class Masthead {

    @Expose
    private String title;
    @Expose
    private String description;
    @Expose
    private BackgroundImage backgroundImage;

    @Data
    public static class BackgroundImage {
        @Expose
        private Desktop desktop;
        @Expose
        private Mobile mobile;

        @Data
       public static class Desktop {
            @Expose
            private String alternativeText;
            @Expose
            private String url;
        }

        @Data
        public static class Mobile {
            @Expose
            private String url;
        }

    }

}
