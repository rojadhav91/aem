package com.techcombank.core.models;

/**
 * Interface For Announcement Model
 *
 */
public interface AnnouncementModel {
    /**
     * AnnouncementText
     */
    String getAnnouncementText();

    /**
     * TitleText
     */
    String getTitleText();

    /**
     * TitleDescription
     */
    String getTitleDescription();

    /**
     * IconPath
     */
    String getIconPath();


    /**
     * Icon Alt Text
     */
    String getIconAltText();

    /**
     * CtaRequired
     */
    Boolean getCtaRequired();


}
