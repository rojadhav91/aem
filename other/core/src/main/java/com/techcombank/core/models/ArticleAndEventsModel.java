package com.techcombank.core.models;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Sling model class for Events/News/Articles.
 */
@Model(adaptables = {SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class ArticleAndEventsModel {

    private static final Logger LOG = LoggerFactory.getLogger(ArticleAndEventsModel.class);

    @SlingObject
    private Resource resource;

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    /**
     * Article image path.
     */
    @Inject
    @Via("resource")
    @Getter
    private String articleImagePath;

    /**
     * Article image alt text.
     */
    @Inject
    @Via("resource")
    @Getter
    private String articleImageAltText;

    /**
     * Article creation date.
     */
    @Inject
    @Via("resource")
    @Getter
    private String articleCreationDate;

    /**
     * Article Title.
     */
    @Inject
    @Via("resource")
    @Getter
    private String articleTitle;

    /**
     * Article Description.
     */
    @Inject
    @Via("resource")
    @Getter
    private String articleDescription;


    /**
     * Setting up article details .
     */
    @PostConstruct
    protected void init() {

        Resource articleResource = currentPage.getContentResource();
        ValueMap valueMap = articleResource.getValueMap();
        articleImagePath = valueMap.get(TCBConstants.ARTICLE_IMAGE_PATH, StringUtils.EMPTY);
        articleImageAltText = valueMap.get(TCBConstants.ARTICLE_IMAGE_ALT_TEXT, StringUtils.EMPTY);
        articleTitle = valueMap.get(NameConstants.PN_PAGE_TITLE, StringUtils.EMPTY);
        articleDescription = valueMap.get(JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY);

        //Parsing Article Creation Date
        GregorianCalendar gregorianCalendar;
        String fetchDate = JcrConstants.JCR_CREATED;
        if (valueMap.containsKey(TCBConstants.ARTICLE_CREATION_DATE)) {
            fetchDate = TCBConstants.ARTICLE_CREATION_DATE;
        }
        gregorianCalendar = (GregorianCalendar) valueMap.get(fetchDate);
        Date creationDate = gregorianCalendar.getTime();
        DateFormat df = new SimpleDateFormat(TCBConstants.DATE_VN_PATTERN);
        articleCreationDate = df.format(creationDate);
        LOG.info("ARTICLE CREATION DATE : {}", articleCreationDate);
    }

    /**
     * Returns the mobile image rendition path for the Article Image.
     */
    public String getArticleMobileImagePath() {
        return PlatformUtils.getMobileImagePath(articleImagePath);
    }

    /**
     * Returns the web image rendition path for the Article Image .
     */
    public String getArticleWebImagePath() {
        return PlatformUtils.getWebImagePath(articleImagePath);
    }
}
