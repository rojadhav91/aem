package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.models.OfferListingModel;
import lombok.Getter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Sling model for offer listing component.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = OfferListingModel.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = OfferListingModelImpl.RESOURCE_TYPE)
public class OfferListingModelImpl implements OfferListingModel {
    protected static final String RESOURCE_TYPE = "techcombank/components/offerlisting";

    /**
     * Category filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String categoryFilter;

    /**
     * Category filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String cardFilter;

    /**
     * A list of offer listing filter.
     */
    @ValueMapValue
    @Getter
    private String[] tagFilter;

    /**
     * Card type selection for offer listing.
     */
    @ValueMapValue
    @Getter
    private String[] cardType;

    /**
     * Merchant selection for offer listing.
     */
    @ValueMapValue
    @Getter
    private String[] merchantType;

    /**
     * Merchant placeholder text for offer listing.
     */
    @ValueMapValue
    @Getter
    private String merchantPlaceholder;

    /**
     * Sort filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String sortFilter;

    /**
     * Most Popular filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String mostPopular;

    /**
     * Latest filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String latest;

    /**
     * More Time filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String moreTime;

    /**
     * Expiring Soon filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String expiringSoon;

    /**
     * Filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String filter;

    /**
     * Apply filter label for offer listing.
     */
    @ValueMapValue
    @Getter
    private String apply;

    /**
     * Select Merchant label for offer listing
     */
    @ValueMapValue
    @Getter
    private String merchantFilter;

    /**
     * Select all label for merchant filter.
     */
    @ValueMapValue
    @Getter
    private String selectAllMerchant;

    /**
     * View Result Label.
     */
    @ValueMapValue
    @Getter
    private String viewResult;

    /**
     * View More Label.
     */
    @ValueMapValue
    @Getter
    private String viewMore;

    /**
     * Show Results count Label for results.
     */
    @ValueMapValue
    @Getter
    private String resultCountText;

    /**
     * Search Place Holder label.
     */
    @ValueMapValue
    @Getter
    private String searchPlaceHolderText;

    /**
     * Expiry Card Day label.
     */
    @ValueMapValue
    @Getter
    private String expiryDayLabel;

    /**
     * Empty Promotion label to display when no offers found.
     */
    @ValueMapValue
    @Getter
    private String emptyPromotionLabel;

    /**
     * Expiry label.
     */
    @ValueMapValue
    @Getter
    private String expiryLabel;


    @SlingObject
    private ResourceResolver resourceResolver;

    private TagManager tagManager;

    @Inject
    private Page currentPage;

    @PostConstruct
    protected void init() {
        tagManager = resourceResolver.adaptTo(TagManager.class);
    }

    public Map<String, String> getFilters(final String[] tagList) {
        Map<String, String> tagMap = new HashMap<>();
        Locale language = currentPage.getLanguage();
            if (ArrayUtils.isNotEmpty(tagList)) {
                for (String content : tagList) {
                    Tag tag = tagManager.resolve(content);
                    if (null != tag) {
                        tagMap.put(tag.getName(),
                                WordUtils.capitalize(tag.getTitle(language)));
                    }
                }
            }

        return tagMap;
    }

    @Override
    public Map<String, String> getFilterTags() {
        return getFilters(tagFilter);
    }

    @Override
    public Map<String, String> getCardTypeTags() {
        return getFilters(cardType);
    }

    @Override
    public Map<String, String> getMerchantTypeTags() {
        Map<String, String> merchantTypeMap = getFilters(merchantType);
        return merchantTypeMap.entrySet().stream()
                .sorted((merchantOne, merchantTwo) -> merchantOne.getValue().compareTo(merchantTwo.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public String getLocaleLanguage() {
        return currentPage.getLanguage().toString();
    }
}
