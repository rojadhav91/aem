package com.techcombank.core.models.impl;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.XFPageModel;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * This model class is for Mapping Page Properties to HeaderFooterMenu Object.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
adapters = {XFPageModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class XFPageModelImpl implements XFPageModel {
    /**
     * Current Page Resource Object.
     */
    @SlingObject
    private Resource currentResource;

    @Inject
    private Page currentPage;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Path for header XF
     */
    @ValueMapValue
    private String headerXF;

    /**
     * Path for footer XF
     */
    @ValueMapValue
    private String footerXF;

    /**
     * @return the headerXF
     */
    @Override
    public String getHeaderXF() {
        if (StringUtils.isNotBlank(headerXF)) {
            return headerXF;
        } else {
            String xfheaderPath = PlatformUtils.getInheritedPageProperties(currentPage)
                    .getInherited(TCBConstants.HEADER_XF, String.class);
            return PlatformUtils.checkPageValidity(resourceResolver, xfheaderPath) ? xfheaderPath : StringUtils.EMPTY;
        }
    }

    /**
     * @return the footerXF
     */
    @Override
    public String getFooterXF() {
        if (StringUtils.isNotBlank(footerXF)) {
            return footerXF;
        } else {
            String xfFooterPath = PlatformUtils.getInheritedPageProperties(currentPage)
                    .getInherited(TCBConstants.FOOTER_XF, String.class);
            return PlatformUtils.checkPageValidity(resourceResolver, xfFooterPath) ? xfFooterPath : StringUtils.EMPTY;
        }
    }

}
