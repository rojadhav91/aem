package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CarouselTilesModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CarouselTilesModel.class)
public class CarouselTilesModel {

    /**
     * Number of tiles
     */
    @ValueMapValue
    @Getter
    private String numberOfTiles;

    /** In Mobile View is it a slider ? */
    @ValueMapValue
    @Getter
    private boolean inMobileViewIsItASlider;

    /** The view more label */
    @ValueMapValue
    @Getter
    private String viewMoreLabel;

    /**
     * List of Tiles
     */
    @ChildResource
    @Getter
    private List<CarouselTilesItemModel> modelItems;

}
