package com.techcombank.core.models.impl;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.models.FixingRateModel;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Exchange Rate details
 */
@Model(adaptables = Resource.class, adapters = {FixingRateModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = FixingRateModelImpl.RESOURCE_TYPE)
public class FixingRateModelImpl implements FixingRateModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/fixingrate";

    /**
     * The Text for the fixing rate title.
     */
    @ValueMapValue
    @Getter
    private String fixingRateTitle;

    /**
     * The Text for the tab day label.
     */
    @ValueMapValue
    @Getter
    private String dayLabel;

    /**
     * The Icon for day filed
     */
    @ValueMapValue
    @Getter
    private String dayIcon;

    /**
     * The Alt Text for day icon.
     */
    @ValueMapValue
    @Getter
    private String dayIconAlt;

    /**
     * The Text for the time label.
     */
    @ValueMapValue
    @Getter
    private String timeLabel;

    /**
     * The Icon for time filed.
     */
    @ValueMapValue
    @Getter
    private String timeIcon;

    /**
     * The Alt Text for time icon.
     */
    @ValueMapValue
    @Getter
    private String timeIconAlt;

    /**
     * The Text for currency label.
     */
    @ValueMapValue
    @Getter
    private String currencyLabel;

    /**
     * The Text for the column one heading.
     */
    @ValueMapValue
    @Getter
    private String colOneHeading;

    /**
     * The Text for the column two heading.
     */
    @ValueMapValue
    @Getter
    private String colTwoHeading;

    /**
     * The Text for the column sub heading.
     */
    @ValueMapValue
    @Getter
    private String colSubHeading;

    /**
     * Returns the web image rendition path for the day icon.
     */
    @Override
    public String getDayIconWebImagePath() {
        return PlatformUtils.getWebImagePath(dayIcon);
    }

    /**
     * Returns the mobile image rendition path for the day icon.
     */
    @Override
    public String getDayIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(dayIcon);
    }

    /**
     * Returns the web image rendition path for the time icon.
     */
    @Override
    public String getTimeIconWebImagePath() {
        return PlatformUtils.getWebImagePath(timeIcon);
    }

    /**
     * Returns the mobile image rendition path for the time icon.
     */
    @Override
    public String getTimeIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(timeIcon);
    }

}
