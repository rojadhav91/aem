package com.techcombank.core.service.impl;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.FormContainerService;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Component(service = FormContainerService.class, immediate = true)
@Designate(ocd = FormContainerServiceImpl.CRMFormIntegrationServiceConfig.class)
public class FormContainerServiceImpl implements FormContainerService {
    private FormContainerServiceImpl.CRMFormIntegrationServiceConfig crmServiceConfig;
    private static final String LOG_REQUEST_PAYLOAD = "Pay Load :: {}";
    @Reference
    private JwtTokenService jwtTokenService;
    @Reference
    private HttpClientExecutorService httpExecutor;
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String BUSINESS_UNIT = "businessUnit";
    private static final String RBG_LEADS = "RBG";
    private static final String BB_LEADS = "BB";
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Activate
    public void activate(final FormContainerServiceImpl.CRMFormIntegrationServiceConfig config) {
        this.crmServiceConfig = config;
    }

    @Override
    public JsonObject formValues(final boolean isTokenExpired, final Map<String, String> map) {
        return submitFormFields(map, isTokenExpired);
    }

    /**
     * @param map requestParams
     * @return JsonObject objectPayLoad
     */
    private JsonObject constructRequestPayload(final Map<String, String> requestParams) {

        String[] keysRemove = crmServiceConfig.getFormFilter();
        Map<String, String> requestParamMap = removeUnwantedKeysFromMap(requestParams, keysRemove);

        JsonObject objectPayLoad = new JsonObject();
        JsonObject headerRequest = new JsonObject();
        headerRequest.addProperty(TCBConstants.MESSAGE_ID, UUID.randomUUID().toString());
        headerRequest.addProperty(TCBConstants.CREATE_DATE, getCurrentDate());
        headerRequest.addProperty(TCBConstants.FROM, crmServiceConfig.getCrmFormRequestFrom());
        headerRequest.addProperty(TCBConstants.TO, crmServiceConfig.getCrmFormRequestTo());
        headerRequest.addProperty(TCBConstants.SERVICE_TYPE, crmServiceConfig.getCrmFormServiceType());

        JsonObject document = new JsonObject();
        for (Map.Entry<String, String> entry : requestParamMap.entrySet()) {
            document.addProperty(entry.getKey(), entry.getValue());
        }

        objectPayLoad.add(TCBConstants.HEADER_REQUEST, headerRequest);
        objectPayLoad.add(TCBConstants.DOCUMENT, document);

        return objectPayLoad;
    }

    /**
     * This Method creates and returns the date in the format
     * '2022-07-09T07:14:42.716Z'
     *
     * @return String currentDate
     */
    private String getCurrentDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
        return dateFormatter.format(new Date());
    }

    /**
     * This method construct the header map for request payload
     *
     * @param token token
     * @return map headerMap
     */
    private Map<String, String> constructHeadersMap(final String token) {
        Map<String, String> headersMap = new HashMap<>();
        headersMap.put(TCBConstants.AUTHORIZATION, TCBConstants.BEARER + token);
        headersMap.put(HttpHeaders.CONTENT_TYPE, TCBConstants.APPLICATION_JSON);
        headersMap.put(HttpHeaders.ACCEPT, TCBConstants.APPLICATION_JSON);
        return headersMap;
    }

    /**
     * This method remove unwanted fields from header
     *
     * @param requestParamMap
     * @param keysRemove
     * @return requestParamMap
     */
    private Map<String, String> removeUnwantedKeysFromMap(final Map<String, String> requestParamMap,
                                                          final String[] keysRemove) {
        requestParamMap.keySet().removeIf(key -> Arrays.asList(keysRemove).contains(key));
        return requestParamMap;
    }

    /**
     * This saveFormField will save the form field values on AEM
     * This is Temporary
     * and will be removed
     * once CRM integration is ready
     */
    private JsonObject submitFormFields(final Map<String, String> requestParamMap, final boolean isTokenExpired) {
        JsonObject obj = new JsonObject();
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headersMap = constructHeadersMap(token);
        String crmFormSubmitUrl = StringUtils.EMPTY;
        String businessUnit = crmServiceConfig.getBusinessUnit();
        if (requestParamMap.containsKey(businessUnit)) {
            if (crmServiceConfig.getRBGLeads().equalsIgnoreCase(requestParamMap.get(businessUnit))) {
                crmFormSubmitUrl = crmServiceConfig.getCrmFormSubmitRBGUrl();
            } else if (crmServiceConfig.getBBGLeads().equalsIgnoreCase(requestParamMap.get(businessUnit))) {
                crmFormSubmitUrl = crmServiceConfig.getCrmFormSubmitBBUrl();
            }
        }
        JsonObject requestPayLoad = constructRequestPayload(requestParamMap);
        JsonObject responseJson;
        try {
            StringEntity payLoad = new StringEntity(requestPayLoad.toString());
            log.info(LOG_REQUEST_PAYLOAD, requestPayLoad);
            responseJson = httpExecutor.executeHttpPost(crmFormSubmitUrl, headersMap, payLoad);
            if (null != responseJson.get(TCBConstants.API_ERROR_VALUE)
                    && responseJson.get(TCBConstants.API_ERROR_VALUE).getAsString()
                    .equalsIgnoreCase(TCBConstants.INVALID_TOKEN)) {
                token = jwtTokenService.getJwtToken(true);
                headersMap.clear();
                headersMap = constructHeadersMap(token);
                responseJson = httpExecutor.executeHttpPost(crmFormSubmitUrl, headersMap, payLoad);
            }
            if (null != responseJson.get(TCBConstants.RESPONSE_STATUS)) {
                obj.addProperty(TCBConstants.ERROR_MESSAGE,
                        responseJson.get(TCBConstants.RESPONSE_STATUS).getAsJsonObject()
                                .get(TCBConstants.ERROR_MESSAGE).getAsString());
                obj.addProperty(TCBConstants.ERROR_CODE,
                        responseJson.get(TCBConstants.RESPONSE_STATUS).getAsJsonObject()
                                .get(TCBConstants.ERROR_CODE).getAsString());
                if (TCBConstants.ZERO.equals(responseJson.get(TCBConstants.RESPONSE_STATUS).getAsJsonObject()
                        .get(TCBConstants.ERROR_CODE).getAsString())) {
                    obj.addProperty(TCBConstants.SUCCESS, HttpServletResponse.SC_OK);
                } else {
                    obj.addProperty(TCBConstants.FAILURE, HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                obj.addProperty(TCBConstants.ERROR_MESSAGE, responseJson.toString());
                obj.addProperty(TCBConstants.ERROR_CODE, TCBConstants.ALL_RESULT);
                obj.addProperty(TCBConstants.FAILURE, HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (UnsupportedEncodingException e) {
            log.error("Error occurred in CRM Form Integration method ::: response code: {}, message: {}",
                    HttpStatus.SC_BAD_REQUEST, e.getMessage());
            obj.addProperty(TCBConstants.FAILURE, HttpServletResponse.SC_BAD_REQUEST);
            obj.addProperty(TCBConstants.ERROR_MESSAGE, "Bad Request");
            obj.addProperty(TCBConstants.ERROR_CODE, TCBConstants.ALL_RESULT);
        }
        return obj;
    }

    @ObjectClassDefinition(name = "CRM Form Integration Service Configuration",
            description = "CRM Form Integration Service Configuration")
    public @interface CRMFormIntegrationServiceConfig {
        @AttributeDefinition(name = "crmFormSubmitRBGUrl", description = "CRM Form Submit RBG Url",
                type = AttributeType.STRING) String getCrmFormSubmitRBGUrl() default StringUtils.EMPTY;

        @AttributeDefinition(name = "crmFormSubmitBBUrl", description = "CRM Form Submit BB Url",
                type = AttributeType.STRING) String getCrmFormSubmitBBUrl() default StringUtils.EMPTY;

        @AttributeDefinition(name = "crmFormRequestFrom", description = "CRM Form Request From",
                type = AttributeType.STRING) String getCrmFormRequestFrom() default StringUtils.EMPTY;

        @AttributeDefinition(name = "crmFormRequestTo", description = "CRM Form Request To",
                type = AttributeType.STRING) String getCrmFormRequestTo() default StringUtils.EMPTY;

        @AttributeDefinition(name = "crmFormServiceType", description = "CRM Form Service Type",
                type = AttributeType.STRING) String getCrmFormServiceType() default StringUtils.EMPTY;

        @AttributeDefinition(name = "Form Filter", description = "Form Filter Key", type = AttributeType.STRING)
        String[] getFormFilter();

        @AttributeDefinition(name = "businessUnit", description = "Business Unit", type = AttributeType.STRING)
        String getBusinessUnit() default BUSINESS_UNIT;

        @AttributeDefinition(name = "rbgLeads", description = "RBG Leads", type = AttributeType.STRING)
        String getRBGLeads() default RBG_LEADS;

        @AttributeDefinition(name = "bbLeads", description = "BB Leads", type = AttributeType.STRING)
        String getBBGLeads() default BB_LEADS;
    }
}
