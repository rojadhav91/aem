package com.techcombank.core.service.impl;

import com.techcombank.core.service.RunmodeService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Component(service = {RunmodeService.class}, immediate = true)
@Designate(ocd = RunmodeServiceImpl.RunmodeConfig.class)
public class RunmodeServiceImpl implements RunmodeService {

    private RunmodeConfig runmodeConfig;

    @Activate
    @Modified
    public void activate(final RunmodeConfig config) {
        this.runmodeConfig = config;
    }

    @Override
    public String getEnvironmentName() {
        return runmodeConfig.getEnvironmentName();
    }

    @Override
    public String getEnvironmentType() {
        return runmodeConfig.getEnvironmentType();
    }

    @ObjectClassDefinition(name = "Environment Configuration", description = "Environment Configurations")
    public @interface RunmodeConfig {

        /**
         * Retrieves the name of the current environment.
         *
         * @return The environment name. Defaults to "author".
         */
        @AttributeDefinition(name = "getEnvironmentType", description = "Environment Type", type = AttributeType.STRING)
        String getEnvironmentName() default "author";

        /**
         * Retrieves the type of the current environment.
         *
         * @return The environment type. Defaults to "dev".
         */
        @AttributeDefinition(name = "getEnvironmentName", description = "Environment Name", type = AttributeType.STRING)
        String getEnvironmentType() default "dev";
    }
}
