package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ListRowModel.class)
public class ListRowModel {
    /**
     * Title Text
     */
    @ValueMapValue
    @Getter
    private String titleText;
    /**
     * Row Number
     */
    @ValueMapValue
    @Getter
    private String rowNumber;
    /**
     * Column Number
     */
    @ValueMapValue
    @Getter
    private String columnNumber;
    /**
     * List of Row Items
     */
    @ChildResource
    @Getter
    private List<ListRowItemModel> listRowItems;

}
