package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class InspirePrivilegeContentListModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InspirePrivilegeContentListModel {

    /**
     * Content Icon
     * - Description: To add Icon from DAM
    */
    @ValueMapValue
    @Getter
    private String contentIcon;

    /**
     * Content Text
     * - Description: Text to be displayed on the content item
    */
    @ValueMapValue
    @Getter
    private String contentText;

}
