package com.techcombank.core.models;


import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

/**
 * HomePageBlockNavigation is a SlingModel that represents a navigation item on the homepageBlockComponent.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = HomePageBlockModel.class)
public class HomePageBlockModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/homepageblock";

    /**
     * Background image.
     */
    @ValueMapValue
    @Getter
    private String bgColor;
    /**
     * Background image for mobile.
     */
    @ValueMapValue
    @Getter
    private String imageMobile;
    /**
     * Background image for mobile.
     */
    @ValueMapValue
    @Getter
    private String image;
    /**
     * The title for the block.
     */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * A list of navigation items for the homepageBlock.
     */
    @Inject
    @Getter
    private List<HomePageBlockNavigation> navigation;

    /**
     * The label for the link.
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * The URL for the link.
     */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Whether the link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String openInNewTab;

    /**
     * Whether the link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String openInNewTabContact;

    /**
     * A list of contact information for the homepageBlock.
     */
    @Inject
    @Getter
    private List<HomePageContact> contact;

    /**
     * The Checkbox  for follow and unfollow.
     */
    @ValueMapValue
    @Getter
    private String nofollowContact;

    /**
     * The text for the button.
     */
    @ValueMapValue
    @Getter
    private String buttonText;

    /**
     * The URL for the button.
     */
    @ValueMapValue
    @Getter
    private String buttonUrl;
    /**
     * The Checkbox  for follow and unfollow.
     */
    @ValueMapValue
    @Getter
    private String nofollow;

    @OSGiService
    private ResourceResolverService resourceResolverService;
    private ResourceResolver resourceResolver;

    @Getter
    private String buttonInteraction;

    @Getter
    private String linkInteraction;

    public String getButtonInteraction() {
        return getWebInteractionValue(buttonUrl);
    }

    public String getLinkInteraction() {
        return getWebInteractionValue(linkUrl);
    }

    public String getLinkUrl() {
        return StringUtils.isBlank(PlatformUtils.isInternalLink(resourceResolver,
                linkUrl)) ? linkUrl : PlatformUtils.isInternalLink(resourceResolver, linkUrl);
    }

    public String getButtonUrl() {
        return StringUtils.isBlank(PlatformUtils.isInternalLink(resourceResolver,
                buttonUrl)) ? buttonUrl : PlatformUtils.isInternalLink(resourceResolver, buttonUrl);
    }

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    /**
     * Returns the web image rendition path for the Icon image.
     */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(image);
    }

    /**
     * Returns the mobile image rendition path for the imageMobile.
     */
    public String getImageMobilePath() {
        return PlatformUtils.getMobileImagePath(imageMobile);
    }

    public String getWebInteractionValue(final String element) {
        if (null != resourceResolver) {
            String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
            String linkType = PlatformUtils.getUrlLinkType(element);
            return PlatformUtils.getInteractionType(componentName, linkType);
        } else {
            return StringUtils.EMPTY;
        }
    }

}
