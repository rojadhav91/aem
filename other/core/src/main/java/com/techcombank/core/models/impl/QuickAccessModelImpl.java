package com.techcombank.core.models.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.QuickAccessModel;
import com.techcombank.core.models.ExchangeRate;
import com.techcombank.core.service.VfxRatesDataService;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Quick Access details
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = {QuickAccessModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = QuickAccessModelImpl.RESOURCE_TYPE)
public class QuickAccessModelImpl implements QuickAccessModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/quickaccess";

    protected static final int MAX_INDEX = 6;

    protected static final int START_INDEX = 0;

    private static final Logger LOG = LoggerFactory.getLogger(QuickAccessModelImpl.class);

    @OSGiService
    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    @OSGiService
    private VfxRatesDataService vfxRatesDataService;

    /**
     * The Text for the tab one heading.
     */
    @ValueMapValue
    @Getter
    private String tabOneHeading;

    /**
     * The Text for the tab one column one heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingOne;

    /**
     * The Text for the tab one column one sub heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingColOne;

    /**
     * The Text for the tab one column two sub heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingColTwo;

    /**
     * The Text for the tab one column three sub heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingColThree;

    /**
     * The Text for the tab one column two heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingTwo;

    /**
     * The Text for the tab One column three heading.
     */
    @ValueMapValue
    @Getter
    private String subbHeadingThree;

    /**
     * The Text for the tab one view All.
     */
    @ValueMapValue
    @Getter
    private String tabOneViewAll;

    /**
     * The Icon for tab one view all link
     */
    @ValueMapValue
    @Getter
    private String tabOneIcon;

    /**
     * The Alt Text for Tab one Icon.
     */
    @ValueMapValue
    @Getter
    private String tabOneIconAlt;

    /**
     * Link for tab one view all.
     */
    @ValueMapValue
    private String tabOneLink;

    /**
     * To check the tab one link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String tabOneTarget;

    /**
     * To check tab one seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean tabOneNoFollow;

    /**
     * The Text for date change note.
     */
    @ValueMapValue
    private String dateChangeNote;

    /**
     * The Text for the tab two heading.
     */
    @ValueMapValue
    @Getter
    private String tabTwoHeading;

    /**
     * The Text for the Tab two view All.
     */
    @ValueMapValue
    @Getter
    private String tabTwoViewAll;

    /**
     * The Icon for tab two view all link
     */
    @ValueMapValue
    @Getter
    private String tabTwoIcon;

    /**
     * The Alt Text for tab two icon.
     */
    @ValueMapValue
    @Getter
    private String tabTwoIconAlt;

    /**
     * Link for tab two view all.
     */
    @ValueMapValue
    private String tabTwoLink;

    /**
     * To check the tab two link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String tabTwoTarget;

    /**
     * To check tab two seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean tabTwoNoFollow;

    /**
     * The List for Tab two file section items.
     */
    @ChildResource
    @Getter
    private List<QuickAccessFileModelImpl> tabTwoFileSection;

    /**
     * The Text for the tab three heading.
     */
    @ValueMapValue
    @Getter
    private String tabThreeHeading;

    /**
     * The Text for the Tab three view All.
     */
    @ValueMapValue
    @Getter
    private String tabThreeViewAll;

    /**
     * The Icon for tab three view all link
     */
    @ValueMapValue
    @Getter
    private String tabThreeIcon;

    /**
     * The Alt Text for tab three icon.
     */
    @ValueMapValue
    @Getter
    private String tabThreeIconAlt;

    /**
     * Link for tab three view all.
     */
    @ValueMapValue
    private String tabThreeLink;

    /**
     * To check the tab three link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String tabThreeTarget;

    /**
     * To check tab three seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean tabThreeNoFollow;

    /**
     * The List for tab three file section items.
     */
    @ChildResource
    @Getter
    private List<QuickAccessFileModelImpl> tabThreeFileSection;

    /**
     * @return the tabOneLinkURL
     */
    @Override
    public String getTabOneLink() {
        return PlatformUtils.isInternalLink(resourceResolver, tabOneLink);
    }

    /**
     * @return the tabTwoLinkURL
     */
    @Override
    public String getTabTwoLink() {
        return PlatformUtils.isInternalLink(resourceResolver, tabTwoLink);
    }

    /**
     * @return the tabThreeLinkURL
     */
    @Override
    public String getTabThreeLink() {
        return PlatformUtils.isInternalLink(resourceResolver, tabThreeLink);
    }

    /**
     * Returns the web image rendition path for the tab one icon.
     */
    @Override
    public String getTabOneIconWebImagePath() {
        return PlatformUtils.getWebImagePath(tabOneIcon);
    }

    /**
     * Returns the mobile image rendition path for the tab One icon.
     */
    @Override
    public String getTabOneIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(tabOneIcon);
    }

    /**
     * Returns the web image rendition path for the tab two icon.
     */
    @Override
    public String getTabTwoIconWebImagePath() {
        return PlatformUtils.getWebImagePath(tabTwoIcon);
    }

    /**
     * Returns the mobile image rendition path for the tab two icon.
     */
    @Override
    public String getTabTwoIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(tabTwoIcon);
    }

    /**
     * Returns the web image rendition path for the tab three icon.
     */
    @Override
    public String getTabThreeIconWebImagePath() {
        return PlatformUtils.getWebImagePath(tabThreeIcon);
    }

    /**
     * Returns the mobile image rendition path for the three two icon.
     */
    @Override
    public String getTabThreeIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(tabThreeIcon);
    }

    @Inject
    private Page currentPage;

    /**
     * Returns first six exchange rate data
     */
    @Override
    public List<ExchangeRate> getExchangeRateList() {
        List<String> vfxRateBasePath = vfxRatesDataService.getVfxRateBasePath(TCBConstants.EXCHANGE_RATE, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, vfxRateBasePath.get(0));
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource latestExchangeRateResource = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                vfxRateBasePath.get(0));
        List<ExchangeRate> exchangeRateList = vfxRatesDataService
                .getExchangeRateDataList(latestExchangeRateResource, resourceResolver);
        if (exchangeRateList.size() > MAX_INDEX) {
            return exchangeRateList.subList(START_INDEX, MAX_INDEX);
        }
        return exchangeRateList;
    }

    /**
     * Returns dateChangeNote
     */
    @Override
    public String getDateChangeNote() {
        if (dateChangeNote.contains(TCBConstants.EXCHANGE_RATE_DATE_TIME)) {
            List<String> vfxRateBasePath = vfxRatesDataService.getVfxRateBasePath(TCBConstants.EXCHANGE_RATE, false);
            String latestExchangeRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver,
                    vfxRateBasePath.get(0));
            SimpleDateFormat inputDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat outputDf = new SimpleDateFormat("dd MMMM",
                    Locale.forLanguageTag(PlatformUtils.getLocale(currentPage.getPath(), resourceResolver)));
            try {
                Date latestExchangeRateDate = inputDf.parse(latestExchangeRateDateTime);
                latestExchangeRateDateTime = outputDf.format(latestExchangeRateDate);

            } catch (ParseException e) {
                LOG.error("Exception occure while formating date:", e);
            }
            dateChangeNote = dateChangeNote.replace(TCBConstants.EXCHANGE_RATE_DATE_TIME, latestExchangeRateDateTime);
        }

        return dateChangeNote;
    }

    /**
     * Returns the Tab one link interaction type for analytics.
     */
    @Override
    public String getTabOneLinkInteractionType() {
        return PlatformUtils.getUrlLinkType(tabOneLink);
    }

    /**
     * Returns the Tab two link interaction type for analytics.
     */
    @Override
    public String getTabTwoLinkInteractionType() {
        return PlatformUtils.getUrlLinkType(tabTwoLink);
    }

    /**
     * Returns the Tab two link interaction type for analytics.
     */
    @Override
    public String getTabThreeLinkInteractionType() {
        return PlatformUtils.getUrlLinkType(tabThreeLink);
    }

}
