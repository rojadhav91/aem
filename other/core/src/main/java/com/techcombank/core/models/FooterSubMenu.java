package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;


/**
 * This model class is for Footer Sub Menu.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterSubMenu {
    /**
     * Footer sub menu text.
     */
    @ValueMapValue
    @Getter
    private String subMenuText;
    /**
     * Page path to submenu.
     */
    @ValueMapValue
    @Getter
    private String subMenuUrl;
    @Getter
    private String isSubMenuUrlExternal;
    /**
     * Open Page in new tab.
     */
    @ValueMapValue
    @Getter
    private String openInNewTab;

    @SlingObject
    private ResourceResolver resourceResolver;

    public String getSubMenuUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, subMenuUrl);
    }

    public String getIsSubMenuUrlExternal() {
        return PlatformUtils.getUrlLinkType(subMenuUrl);
    }

}
