package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.models.CardListingResultModel;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

/**
 * This model class is for CardListingResultModelImpl Model
 */
@Model(adaptables = Resource.class, adapters = {
        CardListingResultModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        CardListingResultModelImpl.RESOURCE_TYPE)
@Getter
public class CardListingResultModelImpl implements CardListingResultModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/cardlistresult";

    /**
     * The ResourceResolver.
     */
    @Inject
    private ResourceResolver resolver;
    /**
     * The cardCFRootPath value.
     */
    @ValueMapValue
    private String cardCFRootPath;
    /**
     * The listingPage value.
     */
    @ValueMapValue
    private String listingPage;
    /**
     * The nudgeTagRoot value.
     */
    @ValueMapValue
    private String nudgeTagRoot;

    /**
     * The forLabel value.
     */
    @ValueMapValue
    private String forLabel;
    /**
     * The offersLabel value.
     */
    @ValueMapValue
    private String offersLabel;
    /**
     * The utilitiesLabel value.
     */
    @ValueMapValue
    private String utilitiesLabel;
    /**
     * The conditionsLabel value.
     */
    @ValueMapValue
    private String conditionsLabel;
    /**
     * The ratesLabel value.
     */
    @ValueMapValue
    private String ratesLabel;
    /**
     * The outstandingLabel value.
     */
    @ValueMapValue
    private String outstandingLabel;
    /**
     * The cardOffersLabel value.
     */
    @ValueMapValue
    private String cardOffersLabel;
    /**
     * The feesLabel value.
     */
    @ValueMapValue
    private String feesLabel;
    /**
     * The regImg value.
     */
    @ValueMapValue
    private String regImg;
    /**
     * The regImgMbl value.
     */
    private String regImgMobile;
    /**
     * The regTitle value.
     */
    @ValueMapValue
    private String regTitle;
    /**
     * The regSubTitle value.
     */
    @ValueMapValue
    private String regSubTitle;
    /**
     * The imageRegAlt value.
     */
    @ValueMapValue
    private String imageRegAlt;
    /**
     * The regCtaMobile value.
     */
    @ValueMapValue
    private String regCtaMobile;
    /**
     * The RegisterSteps value.
     */
    @ChildResource
    private List<CardListingModelImpl.RegisterSteps> steps;
    /**
     * The bottomText value.
     */
    @ValueMapValue
    private String bottomText;

    /**
     * The bottomLinkLabel value.
     */
    @ValueMapValue
    private String bottomLinkLabel;

    /**
     * The bottomLink value.
     */
    @ValueMapValue
    private String bottomLink;
    /**
     * The ctaBtmTarget value.
     */
    @ValueMapValue
    private String ctaBtmTarget;
    /**
     * The ctaBtmFollow value.
     */
    @ValueMapValue
    private boolean ctaBtmFollow;
    /**
     * The nudgeTags value.
     */
    private String nudgeTags;
    /**
     * The gsonObj value.
     */
    private Gson gsonObj;
    /**
     * The JsonArray value.
     */
    private JsonArray jsonAry;

    @PostConstruct
    void init() {
        gsonObj = new Gson();
        jsonAry = new JsonArray();
        if (StringUtils.isNotEmpty(cardCFRootPath) && Objects.nonNull(resolver)) {
            final TagManager tagManager = resolver.adaptTo(TagManager.class);
            if (StringUtils.isNotEmpty(nudgeTagRoot)) {
                Tag rootNudgeTag = Objects.requireNonNull(tagManager).resolve(nudgeTagRoot);
                if (Objects.nonNull(rootNudgeTag)) {
                    nudgeTags = listTags(rootNudgeTag, tagManager);
                }
            }
        }
        listingPage =  StringUtils.isNotEmpty(listingPage)
                ? PlatformUtils.isInternalLink(resolver, listingPage)
                : StringUtils.EMPTY;
    }

    private String listTags(final Tag rootTag, final TagManager tagManager) {
        Resource resource = resolver.getResource(rootTag.getPath());
        if (Objects.requireNonNull(resource).hasChildren()) {
            for (Resource value : Objects.requireNonNull(resource).getChildren()) {
                Tag tag = Objects.requireNonNull(tagManager).resolve(value.getPath());
                if (tagManager.find(cardCFRootPath, new String[] {tag.getTagID()}).getSize() > 0) {
                    JsonObject jsonObj = new JsonObject();
                    jsonObj.add("title", gsonObj.toJsonTree(tag.getTitle()));
                    jsonObj.add("id", gsonObj.toJsonTree(tag.getTagID()));
                    jsonAry.add(jsonObj);
                }
            }
        }
        return gsonObj.toJson(jsonAry);
    }
    /**
     * The regImg value.
     */
    @Override
    public String getRegImg() {
        return PlatformUtils.getWebImagePath(regImg);
    }
    /**
     * The regImgMbl value.
     */
    @Override
    public String getRegImgMobile() {
        return  PlatformUtils.getMobileImagePath(regImg);
    }
    /**
     * The regCtaMobile value.
     */
    @Override
    public String getRegCtaMobile() {
        return PlatformUtils.isInternalLink(resolver, regCtaMobile);
    }
    /**
     * The bottomLink value.
     */
    @Override
    public String getBottomLink() {
        return PlatformUtils.isInternalLink(resolver, bottomLink);
    }
}
