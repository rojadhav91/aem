package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class MastheadExtendCardArticleImageModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MastheadExtendCardArticleImageModel {

    /**
     * Card Article Image
     * - Description: To add image to display above card info
    */
    @ValueMapValue
    @Getter
    private String cardArticleImage;

    /**
    * Returns the mobile image rendition path for Card Article Image.
    */
    public String getMobileCardArticleImagePath() {
        return PlatformUtils.getMobileImagePath(cardArticleImage);
    }

    /**
    * Returns the web image rendition path for Card Article Image.
    */
    public String getWebCardArticleImagePath() {
        return PlatformUtils.getWebImagePath(cardArticleImage);
    }

    /**
     * Card Article Image Alt Text
     * - Description: To add alternate text for chosen image
    */
    @ValueMapValue
    @Getter
    private String cardArticleImageAltText;

    /**
     * Card Article Title
     * - Description: To add card title text
    */
    @ValueMapValue
    @Getter
    private String cardArticleTitle;

    /**
     * Card Article Descrition
     * - Description: To add description text to display below title
    */
    @ValueMapValue
    @Getter
    private String cardArticleDescrition;

}
