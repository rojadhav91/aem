package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.annotation.PostConstruct;

/**
 * This model class is for define each SocialShareInfoItemModel
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialShareInfoItemModel {


    public static final String RESOURCE_TYPE = "techcombank/components/socialshare";

    @OSGiService
    private ResourceResolverService resourceResolverService;

    @ValueMapValue
    @Getter
    @Setter
    private String socialShareIcon;

    /**
     * social Share Alt Text
     */
    @ValueMapValue
    @Getter
    private String socialShareAltText;

    /**
     * social Share Link
     */
    @ValueMapValue
    @Getter
    @Setter
    private String socialShareLink;

    /**
     * social Share Target
     */
    @ValueMapValue
    @Getter
    private String socialShareTarget;

    /**
     * social Share No Follow
     */
    @ValueMapValue
    @Getter
    private boolean socialShareNoFollow;

    @Getter
    private String webInteractionValue;

    @PostConstruct
    public void init() {
        try (ResourceResolver resourceResolver = resourceResolverService.getReadResourceResolver()) {
            socialShareLink = PlatformUtils.isInternalLink(resourceResolver, socialShareLink);

            if (null != resourceResolver) {
                String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
                String linkType = PlatformUtils.getUrlLinkType(socialShareLink);
                webInteractionValue = PlatformUtils.getInteractionType(componentName, linkType);
            } else {
                webInteractionValue = StringUtils.EMPTY;
            }
        }
    }

    public String getSocialShareIcon() {
        return PlatformUtils.getWebImagePath(socialShareIcon);
    }
    public String getSocialShareIconMobile() {
        return PlatformUtils.getMobileImagePath(socialShareIcon);
    }
}
