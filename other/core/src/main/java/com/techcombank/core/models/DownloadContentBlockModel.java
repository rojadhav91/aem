package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * The DownloadContentBlockModel class is a Sling Model that represents a Download Content Block.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = DownloadContentBlockModel.class)
public class DownloadContentBlockModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/downloadcontentblock";

    @OSGiService
    private ResourceResolverService resourceResolverService;

    /**
     * The Image Position.
     */
    @Inject
    @Getter
    private String imagePosition;

    /**
     * The Image Aligment.
     */
    @Inject
    @Getter
    private String imageAlignment;

    /**
     * The Image Alt Text.
     */
    @Inject
    @Getter
    private String imageAltText;


    /**
     * The Image Path.
     */
    @Inject
    @Getter
    private String imagePath;

    /**
     * The Image Aligment.
     */
    @Inject
    @Getter
    private String textAlignment;

    /**
     * Title
     */
    @Inject
    @Getter
    private String title;

    /**
     * Description
     */
    @Inject
    @Getter
    private String description;

    /**
     * Description Position
     */
    @Inject
    @Getter
    private String descriptionPosition;

    /**
     * Whether to show the scanner.
     */
    @Inject
    @Getter
    private String showScanner;

    /**
     * The scanner text.
     */
    @Inject
    @Getter
    private String scannerText;

    /**
     * The scanner image.
     */
    @Inject
    @Getter
    private String scannerImage;

    /**
     * The Scanner Link.
     */
    @Inject
    @Getter
    private String scannerLink;

    /**
     * The button text.
     */
    @Inject
    @Getter
    private String buttonText;

    /**
     * The button link.
     */
    @Inject
    @Getter
    private String buttonLink;

    /**
     * The Button Full Width.
     */
    @Inject
    @Getter
    private String buttonFullWidth;

    /**
     * Whether to open the link in a new tab.
     */
    @Inject
    @Getter
    private String openInNewTabButton;

    /**
     * Whether to add a nofollow attribute to the link.
     */
    @Inject
    @Getter
    private String noFollowButton;

    /**
     * The link text.
     */
    @Inject
    @Getter
    private String linkText;

    /**
     * The link URL.
     */
    @Inject
    @Getter
    private String linkUrl;

    /**
     * Whether to open the link in a new tab.
     */
    @Inject
    @Getter
    private String openInNewTabLink;

    /**
     * Whether to add a nofollow attribute to the link.
     */
    @Inject
    @Getter
    private String noFollowLink;

    /**
     * anchorId.
     */
    @Inject
    @Getter
    private String id;

    @Getter
    private String scannerLinkWebInteractionValue;

    @Getter
    private String linkUrlWebInteractionValue;

    /**
     * Returns the web image rendition path for the scanner image.
     */
    public String getScannerWebImagePath() {
        return PlatformUtils.getWebImagePath(scannerImage);
    }

    /**
     * Returns the mobile image rendition path for the scanner image.
     */
    public String getScannerMobileImagePath() {
        return PlatformUtils.getMobileImagePath(scannerImage);
    }

    /**
     * Returns the web image rendition path for the image.
     */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(imagePath);
    }

    /**
     * Returns the mobile image rendition path for the image.
     */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(imagePath);
    }

    @PostConstruct
    protected void init() {
        try (ResourceResolver resourceResolver = resourceResolverService.getReadResourceResolver()) {

            scannerLink = PlatformUtils.isInternalLink(resourceResolver, scannerLink);

            linkUrl = PlatformUtils.isInternalLink(resourceResolver, linkUrl);

            buttonLink = PlatformUtils.isInternalLink(resourceResolver, buttonLink);

            if (null != resourceResolver) {
                String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
                String linkType = PlatformUtils.getUrlLinkType(scannerLink);
                scannerLinkWebInteractionValue = PlatformUtils.getInteractionType(componentName, linkType);
            } else {
                scannerLinkWebInteractionValue = StringUtils.EMPTY;
            }

            if (null != resourceResolver) {
                String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
                String linkType = PlatformUtils.getUrlLinkType(linkUrl);
                linkUrlWebInteractionValue = PlatformUtils.getInteractionType(componentName, linkType);
            } else {
                linkUrlWebInteractionValue = StringUtils.EMPTY;
            }
        }
    }
}
