package com.techcombank.core.models;

import javax.inject.Inject;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Mapping Right Navigation Menu Properties to Primary
 * Navigation.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavigationMenuModel {

    /**
     * The label text for the Navigation Page item.
     */
    @ValueMapValue
    @Getter
    private String navigationLabel;

    /**
     * The URL for the Navigation Page.
     */
    @ValueMapValue
    private String navigationPageURL;

    /**
     * The Target Value for Navigation Page.
     */
    @ValueMapValue
    @Getter
    private String navTarget;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the navigationPageURL
     */
    public String getNavigationPageURL() {
        return PlatformUtils.isInternalLink(resourceResolver, navigationPageURL);
    }

    /**
     * Returns the discover Menu url interaction type for analytics.
     */
    public String getNavigationPageURLInteractionType() {
        return PlatformUtils.getUrlLinkType(navigationPageURL);
    }

}
