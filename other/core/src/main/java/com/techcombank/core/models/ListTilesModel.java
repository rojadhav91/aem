package com.techcombank.core.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class ListTilesModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ListTilesModel.class)
public class ListTilesModel {

    // 1. Left col 50% Right col 25% -25%
    private static final String LAYOUT_OPTION1 = "option1";
    // 2. Left col 25% -25% Right 50%
    private static final String LAYOUT_OPTION2 = "option2";
    // 3. Left col 50% Right col 2 rows 25%-25%
    private static final String LAYOUT_OPTION3 = "option3";
    // 4. Left col 2 rows 25%-25% Right col 50%
    private static final String LAYOUT_OPTION4 = "option4";
    // 5. Top row 100% Bottom row 50%-50%
    private static final String LAYOUT_OPTION5 = "option5";
    // 6. Top row 50%-50% Bottom row 100%
    private static final String LAYOUT_OPTION6 = "option6";
    // 7. Left col 50% Right col 2 rows, top 100% Bottom row 50%-50%
    private static final String LAYOUT_OPTION7 = "option7";
    // 8. 3 column 33%-33%-33%
    private static final String LAYOUT_OPTION8 = "option8";
    // 9. 2 Column 50%-50% Short
    private static final String LAYOUT_OPTION9 = "option9";
    // 10. 2 Column 50%-50% Tall
    private static final String LAYOUT_OPTION10 = "option10";

    // Container class of Left col 50% Right col 25% -25%
    private static final String CONTAINER_LAYOUT_OPTION1 = "normal grid-2-1-1";
    // Container class of Left col 25% -25% Right 50%
    private static final String CONTAINER_LAYOUT_OPTION2 = "normal grid-1-1-2";
    // Container class of Left col 50% Right col 2 rows 25%-25%
    private static final String CONTAINER_LAYOUT_OPTION3 = "large horizontal";
    // Container class of Left col 2 rows 25%-25% Right col 50%
    private static final String CONTAINER_LAYOUT_OPTION4 = "large horizontal";
    // Container class of Top row 100% Bottom row 50%-50%
    private static final String CONTAINER_LAYOUT_OPTION5 = "large vertical";
    // Container class of Top row 50%-50% Bottom row 100%
    private static final String CONTAINER_LAYOUT_OPTION6 = "large vertical";
    // Container class of Left col 50% Right col 2 rows, top 100% Bottom row 50%-50%
    private static final String CONTAINER_LAYOUT_OPTION7 = "large horizontal";
    // Container class of 3 column 33%-33%-33%
    private static final String CONTAINER_LAYOUT_OPTION8 = "square";
    // Container class of 2 column 50%-50%
    private static final String CONTAINER_LAYOUT_OPTION9 = "small";
    // Container class of 2 column 50%-50% Tall
    private static final String CONTAINER_LAYOUT_OPTION10 = "tall";

    // Class of Tiles of layout Left col 50% Right col 25% -25%
    private static final String[] LAYOUT_OPTION1_TILE_CLASSES
        = new String[] {"", "", ""};
    // Class of Tiles of layout Left col 25% -25% Right 50%
    private static final String[] LAYOUT_OPTION2_TILE_CLASSES
        = new String[] {"", "", ""};

    private static final String SPAN_WIDE = "span-wide";
    private static final String SPAN_SMALL_FULL = "span-small full";
    private static final String SPAN_SMALL = "span-small";

    // Class of Tiles of layout Left col 50% Right col 2 rows 25%-25%
    private static final String[] LAYOUT_OPTION3_TILE_CLASSES
        = new String[] {SPAN_WIDE, SPAN_SMALL_FULL, SPAN_SMALL_FULL};
    // Class of Tiles of layout Left col 2 rows 25%-25% Right col 50%
    private static final String[] LAYOUT_OPTION4_TILE_CLASSES
        = new String[] {SPAN_SMALL_FULL, SPAN_SMALL_FULL, SPAN_WIDE};
    // Class of Tiles of layout Top row 100% Bottom row 50%-50%
    private static final String[] LAYOUT_OPTION5_TILE_CLASSES
        = new String[] {SPAN_WIDE, SPAN_SMALL, SPAN_SMALL};
    // Class of Tiles of layout Top row 50%-50% Bottom row 100%
    private static final String[] LAYOUT_OPTION6_TILE_CLASSES
        = new String[] {SPAN_SMALL, SPAN_SMALL, SPAN_WIDE};
    // Class of Tiles of layout Left col 50% Right col 2 rows, top 100% Bottom row 50%-50%
    private static final String[] LAYOUT_OPTION7_TILE_CLASSES
        = new String[] {SPAN_WIDE, SPAN_SMALL_FULL, SPAN_SMALL, SPAN_SMALL};
    // Class of Tiles of layout 3 column 33%-33%-33%
    private static final String[] LAYOUT_OPTION8_TILE_CLASSES
        = new String[] {"", "", ""};
    // Class of Tiles of layout 2 column 50%-50% short
    private static final String[] LAYOUT_OPTION9_TILE_CLASSES
        = new String[] {"", ""};
    // Class of Tiles of layout 2 column 50%-50% tall
    private static final String[] LAYOUT_OPTION10_TILE_CLASSES
        = new String[] {"", ""};

    // Get container class of list tiles
    private static final HashMap<String, String> CONTAINER_CLASSES;
    static {
        CONTAINER_CLASSES = new HashMap<>();
        CONTAINER_CLASSES.put(LAYOUT_OPTION1, CONTAINER_LAYOUT_OPTION1);
        CONTAINER_CLASSES.put(LAYOUT_OPTION2, CONTAINER_LAYOUT_OPTION2);
        CONTAINER_CLASSES.put(LAYOUT_OPTION3, CONTAINER_LAYOUT_OPTION3);
        CONTAINER_CLASSES.put(LAYOUT_OPTION4, CONTAINER_LAYOUT_OPTION4);
        CONTAINER_CLASSES.put(LAYOUT_OPTION5, CONTAINER_LAYOUT_OPTION5);
        CONTAINER_CLASSES.put(LAYOUT_OPTION6, CONTAINER_LAYOUT_OPTION6);
        CONTAINER_CLASSES.put(LAYOUT_OPTION7, CONTAINER_LAYOUT_OPTION7);
        CONTAINER_CLASSES.put(LAYOUT_OPTION8, CONTAINER_LAYOUT_OPTION8);
        CONTAINER_CLASSES.put(LAYOUT_OPTION9, CONTAINER_LAYOUT_OPTION9);
        CONTAINER_CLASSES.put(LAYOUT_OPTION10, CONTAINER_LAYOUT_OPTION10);
    }

    // Get class of all tiles
    private static final HashMap<String, String[]> TILE_CLASSES;
    static {
        TILE_CLASSES = new HashMap<>();
        TILE_CLASSES.put(LAYOUT_OPTION1, LAYOUT_OPTION1_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION2, LAYOUT_OPTION2_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION3, LAYOUT_OPTION3_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION4, LAYOUT_OPTION4_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION5, LAYOUT_OPTION5_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION6, LAYOUT_OPTION6_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION7, LAYOUT_OPTION7_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION8, LAYOUT_OPTION8_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION9, LAYOUT_OPTION9_TILE_CLASSES);
        TILE_CLASSES.put(LAYOUT_OPTION10, LAYOUT_OPTION10_TILE_CLASSES);
    }


    @ValueMapValue
    @Getter
    private String displayLayout;

    /**
     * List of Tiles
     */
    @ChildResource
    @Getter
    private List<ListTilesItemModel> listTilesItems;

    public Map<String, String> getContainerClasses() {
        return CONTAINER_CLASSES;
    }

    public Map<String, String[]> getTileClasses() {
        return TILE_CLASSES;
    }

}
