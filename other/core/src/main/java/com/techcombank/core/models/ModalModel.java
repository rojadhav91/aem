package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class ModalModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ModalModel.class)
public class ModalModel {

    @ValueMapValue
    @Getter
    private String title;

    @ValueMapValue
    @Getter
    private int padding;

    @ValueMapValue
    @Getter
    private String size;

    @ValueMapValue
    @Getter
    private String id;

}
