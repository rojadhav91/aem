package com.techcombank.core.workflows;

import com.adobe.granite.workflow.PayloadMap;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.WorkflowNotificationService;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Objects;

@Component(service = WorkflowProcess.class,
        property = {"process.label = Check Duplicate Workflow"})
public class CheckDuplicateWorkflowProcessStep implements WorkflowProcess {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    private WorkflowNotificationService workflowNotificationService;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession,
                        final MetaDataMap metaDataMap) throws WorkflowException {
        String payload = workItem.getWorkflowData().getPayload().toString();
        logger.debug("Check duplicate workflow {} ", payload);

        MetaDataMap workflowMetadataMap = workItem.getWorkflowData().getMetaDataMap();
        ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class);

        PayloadMap payloadMap = resourceResolver.adaptTo(PayloadMap.class);
        int workflowListSize = payloadMap.getWorkflowInstances(payload, true).size();

        if (StringUtils.startsWith(payload, TCBConstants.VAR) || StringUtils.startsWith(payload, TCBConstants.ETC)) {

            String resPath = payload.concat(TCBConstants.VAR_FILTER);
            Iterator<Resource> resItr =
                    Objects.requireNonNull(resourceResolver.getResource(resPath)).getChildren().iterator();

            while (resItr.hasNext()) {
                Resource res = resItr.next();
                String root = Objects.requireNonNull(res.adaptTo(ValueMap.class)).get("root", String.class);
                workflowListSize = payloadMap.getWorkflowInstances(root, true).size();
                if (workflowListSize > 1) {
                    break;
                }
            }
        }

        if (workflowListSize > 1) {
            logger.info("Resource is already in workflow. Terminating workflow {}", payload);
            String initiator = workItem.getWorkflow().getInitiator();
            String emailTitle = TCBConstants.WORKFLOW_TERMINATED_EMAIL_TITLE;
            workflowNotificationService.sendNotificationAndEmail(resourceResolver, workflowMetadataMap, payload,
                    initiator, StringUtils.EMPTY, StringUtils.EMPTY, emailTitle);
            Workflow workflow = workflowSession.getWorkflow(workItem.getWorkflow().getId());
            workflowSession.terminateWorkflow(workflow);
        }
    }
}
