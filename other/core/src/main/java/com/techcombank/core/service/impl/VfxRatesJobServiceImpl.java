package com.techcombank.core.service.impl;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.VfxRatesJobService;

import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Component(service = { VfxRatesJobService.class }, immediate = true)
public class VfxRatesJobServiceImpl implements VfxRatesJobService {

    @Reference
    private JobManager jobManager;

    private static final Logger LOG = LoggerFactory.getLogger(VfxRatesJobServiceImpl.class);

    @Override
    public void createVfxIntegrationJob(final String vfxRatesData) {
        Map<String, Object> vfxJobparam = new HashMap<>();
        vfxJobparam.put(TCBConstants.VFX_RATE_JSON_DATA, vfxRatesData);
        Job job = jobManager.addJob("VFXDataIntegration/job", vfxJobparam);
        String jobInfo = "Job" + job.getTopic() + " is created successfully on " + job.getCreated().getTime()
                + ",job State:" + job.getJobState();
        LOG.info(jobInfo);

    }

}
