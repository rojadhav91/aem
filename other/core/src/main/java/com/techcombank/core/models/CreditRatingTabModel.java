package com.techcombank.core.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.techcombank.core.constants.TCBConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.TimeZone;

/**
 * The Class CreditRatingTabModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CreditRatingTabModel {
    /**
     * Tab Label
     * - Description: To author tab label
    */
    @ValueMapValue
    @Getter
    private Date tabLabel;

    /**
     * Category Header
     * - Description: To author heading text for category
    */
    @ValueMapValue
    @Getter
    private String categoryHeader;

    /**
     * Rating Header
     * - Description: To author heading text for rating
    */
    @ValueMapValue
    @Getter
    private String ratingHeader;

    /**
     * Credit Rating Data
     */
    @ChildResource
    @Getter
    private List<CreditRatingDataModel> creditRatingDataList;

    @Getter
    private String formattedDate;

    @PostConstruct
    public void init() {
        try {
            DateFormat df = new SimpleDateFormat(TCBConstants.DATE_VN_PATTERN);
            df.setTimeZone(TimeZone.getTimeZone(TCBConstants.TIMEZONE_VN));
            formattedDate = df.format(tabLabel);
        } catch (RuntimeException e) {
            formattedDate = "";
        }
    }
}
