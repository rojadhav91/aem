package com.techcombank.core.service;

import com.google.gson.JsonObject;

public interface StockChartService {

    JsonObject fetchStockChartData(boolean isTokenExpired);

}
