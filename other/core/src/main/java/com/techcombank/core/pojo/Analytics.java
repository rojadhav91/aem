package com.techcombank.core.pojo;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

/**
 * This class in mapping between Analytics properties
 * on Page to Java Class.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = Analytics.class)
public class Analytics {

    private static final int FOURTH_NODE_OF_RESOURCE_PATH = 4;
    /**
     * Business Unit
     */
    @ValueMapValue
    @Getter
    private String businessUnit;

    /**
     * Site Section
     */
    @ValueMapValue
    @Getter
    private String siteSection;

    /**
     * Environment
     */
    @ValueMapValue
    @Getter
    private String environment;

    /**
     * Platform
     */
    @ValueMapValue
    @Getter
    private String platform;

    /**
     * Product Name
     */
    @ValueMapValue
    @Getter
    private String productName;

    /**
     * Journey
     */
    @ValueMapValue
    @Getter
    private String journey;

    /**
     * Language Code
     */
    @Getter
    private String country;
    @SlingObject
    private Resource resource;

    @PostConstruct
    protected void init() {
        //this method will replace with utility method once Navigation code merge.
        if (null != resource && StringUtils.isNotBlank(resource.getPath())) {
            String relPath = resource.getPath();
            if (relPath.contains("/")) {
                String[] path = relPath.split("/");
                country = path[FOURTH_NODE_OF_RESOURCE_PATH];
            }
        }
    }
}
