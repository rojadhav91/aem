package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * This model class is for define each Row of List Row
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListRowItemModel {
    /**
     * Resource Resolver
     */
    @Inject
    private ResourceResolver resourceResolver;
    /**
     * Row Title
     */
    @ValueMapValue
    @Getter
    private String rowTitle;

    /**
     * Category
     */
    @ValueMapValue
    private String category;

    /**
     * Item Date
     */
    @ValueMapValue
    private Date itemDate;

    /**
     * Icon Image
     */
    @ValueMapValue
    @Getter
    private String iconImage;

    /**
     * Alt Text
     */
    @ValueMapValue
    @Getter
    private String altText;

    /**
     * Link Url
     */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Open in new tab
     */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No follow for SEO
     */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    @Getter
    private String tagLabel;

    @Getter
    private String formattedDate;

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    @PostConstruct
    public void init()  {
        linkUrl = PlatformUtils.isInternalLink(resourceResolver, linkUrl);
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        if (itemDate != null) {
            formattedDate = df.format(itemDate);
        }
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        if (tagManager != null) {
            Tag tag = tagManager.resolve(category);
            if (!Objects.isNull(tag)) {
                tagLabel = tag.getTitle();
            }
        }
    }

    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(linkUrl);
    }
}
