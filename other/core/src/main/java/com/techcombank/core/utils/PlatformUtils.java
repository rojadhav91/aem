package com.techcombank.core.utils;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.LanguageUtil;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.LanguageLabelModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility Class for TCB Module.
 */
public final class PlatformUtils {

    private PlatformUtils() {
    }

    /**
     * This Method Checks if link is internal and append .html to the link
     * @param resourceResolver
     * @param linkUrl
     * @return linkUrl
     */
    public static String isInternalLink(final ResourceResolver resourceResolver, final String linkUrl) {

        if (null != linkUrl
                && (resourceResolver.getResource(linkUrl) != null || linkUrl.startsWith(TCBConstants.TCB_BASE_PATH))
                && !StringUtils.contains(linkUrl, TCBConstants.HTMLEXTENSION)
                && !StringUtils.contains(linkUrl, TCBConstants.PDFEXTENSION)) {
            if (StringUtils.contains(linkUrl, TCBConstants.QUESTION_MARK)) {
                String[] urlSplit = linkUrl.split(TCBConstants.ESCAPED_QUESTION_MARK);
                String finalUrl = urlSplit[0].concat(TCBConstants.HTMLEXTENSION).
                        concat(TCBConstants.QUESTION_MARK).concat(urlSplit[1]);
                return PlatformUtils.getMapUrl(resourceResolver, finalUrl);
            } else if (StringUtils.contains(linkUrl, TCBConstants.HASH)) {
                String[] urlSplit = linkUrl.split(TCBConstants.HASH);
                String finalUrl = urlSplit[0].concat(TCBConstants.HTMLEXTENSION).
                        concat(TCBConstants.HASH).concat(urlSplit[1]);
                return PlatformUtils.getMapUrl(resourceResolver, finalUrl);
            } else {
                return PlatformUtils.getMapUrl(resourceResolver, linkUrl.concat(TCBConstants.HTMLEXTENSION));
            }
        } else if (null != linkUrl && linkUrl.startsWith(TCBConstants.HASH)) {
            return linkUrl;
        } else {
            return (null != linkUrl) ? PlatformUtils.getMapUrl(resourceResolver, linkUrl)
                    : StringUtils.EMPTY;
        }
    }

    /**
     * This Method returns List of Languages and its corresponding page page for the
     * current page request
     * @param resourceResolver
     * @param currentPage
     * @return List languageDetails
     */
    public static List<LanguageLabelModel> getLanguageDetails(final ResourceResolver resourceResolver,
                                                              final Page currentPage, final boolean isAllCountryFlag) {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        String currentPagePath = currentPage.getPath();
        List<LanguageLabelModel> languageDetails = new ArrayList<>();
        if (isTcbBasePath(currentPagePath)) {
            // EX Path:/content/techcombank/web/vn/en/personal.html
            String[] currentPageSubPaths = currentPagePath.split(TCBConstants.SLASH);
            // CountryList: vn,ko,kr
            List<String> countryList = getCountryDetails(currentPageSubPaths, resourceResolver, isAllCountryFlag);
            // EX Path: /content/techcombank/web/vn
            for (String country : countryList) {
                getLanguageDetailLists(languageDetails, resourceResolver, country, currentPageSubPaths, pageManager, isAllCountryFlag);
            }

        }
        return languageDetails;
    }

    /**
     * This method gets the list of language details
     *
     * @param languageDetails
     * @param resourceResolver
     * @param country
     * @param currentPageSubPaths
     * @param pageManager
     */
    public static void getLanguageDetailLists(final List<LanguageLabelModel> languageDetails,
                                              final ResourceResolver resourceResolver, final String country, final String[] currentPageSubPaths,
                                              final PageManager pageManager, final boolean hrefFlag) {
        String countryPath = TCBConstants.TCB_BASE_PATH + country;
        Resource countryPage = resourceResolver.getResource(countryPath);
        boolean isLanguagePage = false;
        for (Resource languageRes : countryPage.getChildren()) {
            if (!languageRes.getPath().contains(JcrConstants.JCR_CONTENT)) {
                isLanguagePage = true;
                String[] languageSubPaths = languageRes.getPath().split(TCBConstants.SLASH);
                String language = languageSubPaths.length >= TCBConstants.LANGUAGE_INDEX
                        ? languageSubPaths[TCBConstants.LANGUAGE_INDEX]
                        : StringUtils.EMPTY;
                LanguageLabelModel languageDetail = new LanguageLabelModel();
                String languageTitle = getLanguageTitle(languageRes, isLanguagePage, resourceResolver);
                if (StringUtils.isNotBlank(languageTitle)) {
                    String pageUrl = getAliasPagePath(country, language, currentPageSubPaths, pageManager);

                    if (!hrefFlag) {
                        pageUrl = PlatformUtils.getMapUrl(resourceResolver, pageUrl);
                    }
                    languageDetail.setLanguage(languageTitle);
                    languageDetail.setLanguageUrl(pageUrl);
                    languageDetail.setCountry(country);
                    languageDetails.add(languageDetail);
                }
            }
        }
    }

    /**
     * This method return list of country list
     *
     * @param currentPageSubPaths
     * @param resourceResolver
     * @param isAllCountryFlag
     * @return List<String> countryList
     */
    public static List<String> getCountryDetails(final String[] currentPageSubPaths,
                                                 final ResourceResolver resourceResolver, final boolean isAllCountryFlag) {
        List<String> countryList = new ArrayList<>();
        String country;
        if (isAllCountryFlag) {
            Resource countryPageResourceList = resourceResolver.getResource(TCBConstants.TCB_BASE_PATH);
            for (Resource countryResource : countryPageResourceList.getChildren()) {
                if (!countryResource.getPath().contains(JcrConstants.JCR_CONTENT)) {
                    countryList.add(countryResource.getName());
                }
            }
        } else {
            country = currentPageSubPaths[TCBConstants.COUNTRY_INDEX];
            countryList.add(country);
        }

        return countryList;
    }

    /**
     * This Method return Language Title for the given Page Path
     * @param isLanguagePage
     * @param resourceResolver
     * @return String languageTitle
     */
    public static String getLanguageTitle(final Resource languageRes, final boolean isLanguagePage,
                                          final ResourceResolver resourceResolver) {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page languagePage;
        String languageTitle = StringUtils.EMPTY;
        if (isLanguagePage) {
            languagePage = pageManager.getContainingPage(languageRes);
            if (null != languagePage) {
                languageTitle = languagePage.getTitle();
            }
        } else {
            String currentPagePath = languageRes.getPath();
            String[] languagePagePath = currentPagePath.split(TCBConstants.SLASH);
            if (isTcbBasePath(currentPagePath) && languagePagePath.length >= TCBConstants.LANGUAGE_INDEX) {
                String languagePath = TCBConstants.TCB_BASE_PATH + languagePagePath[TCBConstants.COUNTRY_INDEX]
                        + TCBConstants.SLASH + languagePagePath[TCBConstants.LANGUAGE_INDEX];
                languagePage = pageManager.getContainingPage(languagePath);
                if (null != languagePage) {
                    languageTitle = languagePage.getTitle();
                }
            }
        }
        return languageTitle;
    }

    /**
     * Thia Method check if path starts with TCB base path and returns true if it
     * matches
     * @param currentPagePath
     * @return boolean isValidPath
     */
    public static boolean isTcbBasePath(final String currentPagePath) {
        return currentPagePath.startsWith(TCBConstants.TCB_BASE_PATH);
    }

    /**
     * This Method Checks and returns Alias Page Path if page got authored with
     * Alias Page Property
     * @param country
     * @param language
     * @param currentPageSubPaths
     * @param pageManager
     * @return String languageurl
     */
    public static String getAliasPagePath(final String country, final String language,
                                          final String[] currentPageSubPaths, final PageManager pageManager) {
        String languageurl = TCBConstants.TCB_BASE_PATH + country + TCBConstants.SLASH + language;
        String pageAliasSubPath = TCBConstants.TCB_BASE_PATH + country + TCBConstants.SLASH + language;
        int subPathSize = currentPageSubPaths.length;
        // EX Path:/content/techcombank/web/vn/en/personal.html--Path start index->6(Persoanl)
        for (int i = TCBConstants.PATH_START_INDEX; i < subPathSize; i++) {
            pageAliasSubPath = pageAliasSubPath.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
            Page childAliasPage = pageManager.getContainingPage(pageAliasSubPath);
            if (null != childAliasPage) {
                ValueMap childPageProp = childAliasPage.getProperties();
                if (childPageProp.containsKey(TCBConstants.PROP_ALIAS)) {
                    String pageAliasName = childPageProp.get(TCBConstants.PROP_ALIAS).toString();
                    languageurl = languageurl.concat(TCBConstants.SLASH + pageAliasName);
                } else {
                    languageurl = languageurl.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
                }
            } else {
                languageurl = languageurl.concat(TCBConstants.SLASH + currentPageSubPaths[i]);
            }
        }
        languageurl = languageurl.concat(TCBConstants.HTMLEXTENSION);
        return languageurl;
    }

    /**
     * This Method checks and returns true when All product Link is enabled on
     * Template Level
     * @param currentPage
     * @return boolean isallProductEnabled
     */
    public static boolean checkAllProductLink(final Page currentPage) {
        ValueMap pageProp = currentPage.getProperties();
        boolean isallProductEnabled = false;
        if (pageProp.containsKey(TCBConstants.CQ_TEMPLATE) && pageProp.containsKey(TCBConstants.PROP_ENABLE_ALL_PRODUCT)
                && (TCBConstants.PERSONAL_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString())
                || TCBConstants.BUSINESS_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString())
                || TCBConstants.CONTENT_TEMPLATE_PATH
                .equalsIgnoreCase(pageProp.get(TCBConstants.CQ_TEMPLATE).toString()))) {
            isallProductEnabled = Boolean.valueOf(pageProp.get(TCBConstants.PROP_ENABLE_ALL_PRODUCT).toString());

        }
        return isallProductEnabled;
    }


    /**
     * This method construct and return Interaction Type string
     * @param clickInfo
     * @param type
     * @return String interactiontype
     */
    public static String getInteractionType(final String clickInfo, final String type) {
        return "{'webInteractions': {'name': '" + clickInfo + "','type': '" + type + "'}}";
    }

    /**
     * This method construct and return Click Info string
     * @param key
     * @param value
     * @return String clickInfo
     */
    public static String getClickInfo(final String key, final String value) {
        return "{'" + key + "' : '" + value + "'}";
    }

    /**
     * This method get the parent page properties for the give path
     * @param page
     * @return HierarchyNodeInheritanceValueMap
     */
    public static HierarchyNodeInheritanceValueMap getInheritedPageProperties(final Page page) {

        return new HierarchyNodeInheritanceValueMap(page.getContentResource());

    }
    /**
     * This method get the parent page local for the give path
     * @param currentPagePath
     * @param resourceResolver
     * @return locale
     */
    public static String getLocale(final String currentPagePath, final ResourceResolver resourceResolver) {
        Page page = Optional.ofNullable(resourceResolver)
                .map(resolver -> resolver.getResource(LanguageUtil.getLanguageRoot(currentPagePath)))
                .map(currentPageresource -> currentPageresource.adaptTo(Page.class)).orElse(null);
        return Objects.requireNonNull(page).getLanguage().toString();
    }
    /**
     * Check page validity
     * @param rr, pagePath
     * @return boolean on the page check
     */
    public static boolean checkPageValidity(final ResourceResolver rr, final String pagePath) {
        PageManager pm = rr.adaptTo(PageManager.class);
        if (pm != null) {
            return pm.getPage(pagePath) != null;
        }
        return false;
    }


    /**
     * Checks the Instance Mode
     */
    public static boolean isAuthor(final SlingSettingsService slingSettings) {
        return slingSettings.getRunModes().contains(TCBConstants.AUTHOR);
    }

    /**
     * Returns the domain
     */
    public static String getExternalizer(final Externalizer externalizer, final ResourceResolver resourceResolver,
                                         final String sourceWebsite) {
        String domain;
        if (StringUtils.equalsIgnoreCase(sourceWebsite, TCBConstants.SOURCE_APP)) {
            domain = externalizer.externalLink(resourceResolver, TCBConstants.APP_LOCAL_DOMAIN, StringUtils.EMPTY);
        } else {
            domain = externalizer.publishLink(resourceResolver, StringUtils.EMPTY);
        }
        return StringUtils.endsWith(domain, TCBConstants.SLASH) ? domain.substring(0, domain.length() - 1) : domain;
    }

    /**
     * Returns the APP_DOMAIN
     */
    public static String getAppDomainExternalizer(final Externalizer externalizer,
                                                  final ResourceResolver resourceResolver) {
        String domain = externalizer.externalLink(resourceResolver, TCBConstants.APP_DOMAIN, "");
        return StringUtils.endsWith(domain, TCBConstants.SLASH) ? domain.substring(0, domain.length() - 1) : domain;
    }

    /**
     * This method return current Alias page Path
     * @param resourceResolver
     * @param currentPagePath
     * @return String aliasPagePath
     */
    public static String currentPagePathWithAlias(final ResourceResolver resourceResolver,
                                                  final String currentPagePath) {
        // EX Path:/content/techcombank/web/vn/en/personal.html
        String[] currentPageSubPaths = currentPagePath.split(TCBConstants.SLASH);
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        // Country: Vn
        String country = currentPageSubPaths[TCBConstants.COUNTRY_INDEX];
        // Language:en
        String language = currentPageSubPaths.length >= TCBConstants.LANGUAGE_INDEX
                ? currentPageSubPaths[TCBConstants.LANGUAGE_INDEX]
                : StringUtils.EMPTY;
        return getAliasPagePath(country, language, currentPageSubPaths, pageManager);
    }

    /**
     * This Method Checks for type of the link which is used for analytics
     * @param linkUrl
     * @return String type
     */
    public static String getUrlLinkType(final String linkUrl) {
        if (StringUtils.isBlank(linkUrl)) {
            return StringUtils.EMPTY;
        } else {
            if (linkUrl.startsWith(TCBConstants.TCB_BASE_PATH) || linkUrl.contains(TCBConstants.TCB_DOMAIN)) {
                return TCBConstants.OTHER;
            } else if (linkUrl.endsWith(TCBConstants.PDFEXTENSION)) {
                return TCBConstants.DOWNLOAD;
            } else {
                return TCBConstants.EXIT;
            }
        }
    }

    /**
     * This method return web image rendition path
     * @param imagePath
     * @return String webImageRenditionPath
     */
    public static String getWebImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_SVG_EXTENSION)) {
                return imagePath;
            } else if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_PNG_EXTENSION)) {
                return imagePath + TCBConstants.PNG_WEB_IMAGE_RENDITION_PATH;
            } else {
                return imagePath + TCBConstants.JPEG_WEB_IMAGE_RENDITION_PATH;
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * This method return mobile image rendition path
     * @param imagePath
     * @return String mobileImageRenditionPath
     */
    public static String getMobileImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_SVG_EXTENSION)) {
                return imagePath;
            } else if (imagePath.toLowerCase().endsWith(TCBConstants.IMAGE_PNG_EXTENSION)) {
                return imagePath + TCBConstants.PNG_MOBILE_IMAGE_RENDITION_PATH;
            } else {
                return imagePath + TCBConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH;
            }
        } else {
            return StringUtils.EMPTY;
        }
    }

    public static String getThumbnailImagePath(final String imagePath) {
        if (StringUtils.isNotBlank(imagePath)) {
            return imagePath + TCBConstants.PNG_THUMBNAIL_IMAGE_RENDITION_PATH;
        } else {
            return StringUtils.EMPTY;
        }
    }

    public static String getJcrTitle(final ResourceResolver resourceResolver, final String resourcePath) {
        String jcrTitle = StringUtils.EMPTY;
        Resource resource = resourceResolver.getResource(resourcePath);
        if (!Objects.isNull(resource) && resource.getValueMap().containsKey(TCBConstants.JCR_TITLE)) {
            jcrTitle = resource.getValueMap().get(TCBConstants.JCR_TITLE, String.class);
        }
        return jcrTitle;
    }

    /** Method to return mapped URL in publish instances
     * @param resourceResolver
     * @param path
     * @return etc mapped url
     */
    public static String getMapUrl(final ResourceResolver resourceResolver, final String path) {
        return resourceResolver.map(path);
    }

    /**
     * This Method construct the date and time path for different vfx rates
     * @param vfxRateDateTime
     * @param isDateTime
     * @return string rateDateTimePath
     */
    public static String getVfxRateDateTimePath(final String vfxRateDateTime, final boolean isDateTime) {
        String vfxRateDateTimePath = StringUtils.EMPTY;
        // 2023-07-14 23:16:00
        List<String> dateTime = Arrays.stream(vfxRateDateTime.split(" ")).collect(Collectors.toList());
        if (!dateTime.isEmpty()) {
            String vfxRateDatePath = dateTime.get(0).replace(TCBConstants.DASH, TCBConstants.SLASH);
            vfxRateDateTimePath = vfxRateDatePath;
            if (isDateTime && dateTime.size() == 2) {
                Stream<String> vfxRateTime = Arrays.stream(dateTime.get(1).split(TCBConstants.COLON));
                String vfxRateTimePath = vfxRateTime.limit(TCBConstants.THREE)
                        .collect(Collectors.joining(TCBConstants.DASH));
                vfxRateDateTimePath += TCBConstants.SLASH + vfxRateTimePath;
            }
        }
        return vfxRateDateTimePath;
    }

    /**
     * This Method returns the vfx rate type based on the vfx rate base path
     * @param vfxBasePath
     * @return string vfxRateType
     */
    public static String getVfxRateType(final String vfxBasePath) {
        String vfxRateType = StringUtils.EMPTY;
        if (vfxBasePath.contains(TCBConstants.EXCHANGE_RATE)) {
            vfxRateType = TCBConstants.EXCHANGE_RATE;
        } else if (vfxBasePath.contains(TCBConstants.GOLD_RATE)) {
            vfxRateType = TCBConstants.GOLD_RATE;
        } else if (vfxBasePath.contains(TCBConstants.OTHER_RATE)) {
            vfxRateType = TCBConstants.OTHER_RATE;
        } else if (vfxBasePath.contains(TCBConstants.TENOR_INT_RATE)) {
            vfxRateType = TCBConstants.TENOR_INT_RATE;
        } else if (vfxBasePath.contains(TCBConstants.TENOR_RATE)) {
            vfxRateType = TCBConstants.TENOR_RATE;
        } else if (vfxBasePath.contains(TCBConstants.FIXING_RATE)) {
            vfxRateType = TCBConstants.FIXING_RATE;
        }
        return vfxRateType;
    }

    /**
     * This Method returns the vfx rate type based on the vfx rate base path
     * @param vfxRateDetails
     * @param vfxRateData
     * @param vfxRateType
     * @return string vfxRateData
     */
    public static JsonObject getVfxRateJsonData(final JsonObject vfxRateDetails, final JsonObject vfxRateData,
                                                final String vfxRateType) {

        if (vfxRateType.equalsIgnoreCase(TCBConstants.EXCHANGE_RATE)) {
            vfxRateData.add(TCBConstants.EXCHNAGE_RATE_JSON_ARRAY, vfxRateDetails);
        } else if (vfxRateType.equalsIgnoreCase(TCBConstants.GOLD_RATE)) {
            vfxRateData.add(TCBConstants.GOLD_RATE_JSON_ARRAY, vfxRateDetails);
        } else if (vfxRateType.equalsIgnoreCase(TCBConstants.OTHER_RATE)) {
            vfxRateData.add(TCBConstants.OTHER_RATE_JSON_ARRAY, vfxRateDetails);
        } else if (vfxRateType.equalsIgnoreCase(TCBConstants.TENOR_INT_RATE)) {
            vfxRateData.add(TCBConstants.TENOR_INT_RATE_JSON_ARRAY, vfxRateDetails);
        } else if (vfxRateType.equalsIgnoreCase(TCBConstants.TENOR_RATE)) {
            vfxRateData.add(TCBConstants.TENOR_RATE_JSON_ARRAY, vfxRateDetails);
        } else if (vfxRateType.equalsIgnoreCase(TCBConstants.FIXING_RATE)) {
            vfxRateData.add(TCBConstants.FIXING_RATE_JSON_ARRAY, vfxRateDetails);
        }
        return vfxRateData;
    }


    /**
     * This Method returns the formatted date based on the locale
     * @param locale
     * @param date
     * @return string date
     */
    public static String setTimeFormat(final Locale locale, final LocalDateTime date) {
        String format = TCBConstants.DATE_DEFAULT_PATTERN;
        if (null == date) {
            return null;
        }
        if (locale.getLanguage().equals("vi")) {
            format = TCBConstants.DATE_VN_PATTERN;
        }

        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern(format);
        return date.format(outputFormatter);
    }

    /**
     * This Method get the content fragment path from jcr node
     *
     * @param request
     * @return String CFDataPath
     */
    public static String getRootPath(final SlingHttpServletRequest request) {
        return request.getResource().getValueMap().get(TCBConstants.ROOTPATH, String.class);
    }

    /**
     * Retrieves the first item from a `List<GenericList.Item>` that matches a given target title.
     *
     * @param listItem    The list of items to search.
     * @param targetTitle The title to match against.
     * @return The first item that matches the target title, or `null` if no match is found.
     */
    public static GenericList.Item getGenericListConfigs(final List<GenericList.Item> listItem,
                                                         final String targetTitle) {
        if (null != listItem) {
            for (GenericList.Item item : listItem) {
                if (null != item.getTitle() && item.getTitle().equals(targetTitle)) {
                    return item;
                }
            }
        }
        return null;
    }

}
