package com.techcombank.core.service.impl;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.StoryListingBean;
import com.techcombank.core.service.StoryListing;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@Component(service = {StoryListing.class}, immediate = true)
public class StoryListingServiceImpl implements StoryListing {
    private static final Logger LOG = LoggerFactory.getLogger(StoryListingServiceImpl.class);
    protected static final String ARTICLE_TAGS_PROPERTY = "jcr:content/@" + NameConstants.PN_TAGS;
    protected static final String GROUP = "group.";
    protected static final String VALUE = "value";
    protected static final String TRUE = "true";
    protected static final String FILE_REFERNCE = "fileReference";
    protected static final String ARTICLE_DATE = "@jcr:content/articleCreationDate";

    private String json;
    protected static final String PATH = "path";
    protected static final String TYPE = "type";
    protected static final String ORDERBY = "orderby";
    protected static final String ORDERBYSORT = "orderby.sort";
    protected static final String OFFSET = "p.offset";
    protected static final String LIMIT = "p.limit";
    protected static final String PROPERTY = "property";
    protected static final String NOT = "not";
    protected static final String OPERATION = "operation";
    protected static final String AND = ".and";
    protected static final String OR = ".or";
    protected static final String ASC = "asc";

    protected static final String DESC = "desc";
    protected static final String ARTICLE_CATEGORY_TAGS_PROPERTY = "jcr:content/@categoryTag";
    protected static final String ONE = "1";
    protected static final String TWO = "2";
    protected static final String CATEGORY_PROPERTY = "categoryTag";
    protected static final String GROUPP = "group.p";
    protected static final String TOTAL = "total";
    protected static final String RESULT = "results";

    protected static final String IMAGE = "image";
    protected static final String ARTICLE_CREATION_DATE = "articleCreationDate";

    public Map<String, String> isPageLoad(final String rootPath,
                                          final List<String> articleCategory, final List<String> articleTag,
                                          final Map<String, String> queryMap, final String resultCount,
                                          final String offset) {
        queryMap.put(PATH, rootPath);
        queryMap.put(TYPE, NameConstants.NT_PAGE);

        queryMap.put(GROUPP + OR, TRUE);
        if (!CollectionUtils.isEmpty(articleCategory)) {
            queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                            + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY,
                    ARTICLE_CATEGORY_TAGS_PROPERTY);
            int articleCategoryCounterFilter = 0;
            for (String str : articleCategory) {
                queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                        + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY
                        + TCBConstants.DOT + articleCategoryCounterFilter + TCBConstants.UNDERSCORE + VALUE, str);
                articleCategoryCounterFilter++;
            }
        }
        if (!CollectionUtils.isEmpty(articleTag)) {
            queryMap.put(GROUP + TWO + TCBConstants.UNDERSCORE
                    + GROUP + ONE + TCBConstants.UNDERSCORE
                    + PROPERTY, ARTICLE_TAGS_PROPERTY);
            int articleTagfilter = 0;
            for (String str : articleTag) {
                queryMap.put(GROUP + TWO + TCBConstants.UNDERSCORE
                        + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY
                        + TCBConstants.DOT + articleTagfilter + TCBConstants.UNDERSCORE + VALUE, str);
                articleTagfilter++;
            }

        }
        queryMap.put(ORDERBY, ARTICLE_DATE);
        queryMap.put(ORDERBYSORT, DESC);
        queryMap.put(OFFSET, offset);
        queryMap.put(LIMIT, resultCount);
        return queryMap;
    }

    public Map<String, String> articleFilter(final String rootPath,
                                             final List<String> articleCategory, final List<String> articleTag,
                                             final Map<String, String> queryMap, final String resultCount,
                                             final String offset) {
        queryMap.put(PATH, rootPath);
        queryMap.put(TYPE, NameConstants.NT_PAGE);
        queryMap.put(GROUPP + OR, TRUE);

        if (CollectionUtils.isEmpty(articleCategory) && !CollectionUtils.isEmpty(articleTag)) {
            queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + GROUP + ONE
                    + TCBConstants.UNDERSCORE + PROPERTY, ARTICLE_TAGS_PROPERTY);
            int counter = 0;
            for (String str : articleTag) {
                queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                        + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY
                        + TCBConstants.DOT + counter + TCBConstants.UNDERSCORE + VALUE, str);
                counter++;
            }
            queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + GROUP
                    + TWO + TCBConstants.UNDERSCORE + PROPERTY, ARTICLE_CATEGORY_TAGS_PROPERTY);
            queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + GROUP
                    + TWO + TCBConstants.UNDERSCORE + PROPERTY + TCBConstants.DOT
                    + OPERATION, NOT);
            queryMap.put(ONE + TCBConstants.UNDERSCORE + GROUP + "p" + AND, TRUE);
        } else if (!CollectionUtils.isEmpty(articleCategory) && CollectionUtils.isEmpty(articleTag)) {
            queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                    + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY, ARTICLE_CATEGORY_TAGS_PROPERTY);
            int counter = 0;
            for (String str : articleCategory) {
                queryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                        + GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY
                        + TCBConstants.DOT + counter + TCBConstants.UNDERSCORE + VALUE, str);
                counter++;
            }
        } else if (!CollectionUtils.isEmpty(articleCategory) && !CollectionUtils.isEmpty(articleTag)) {
            int articleCategoryCounter = 1;
            int counter = 1;
            for (String articleCategoryTag : articleCategory) {
                queryMap.put(GROUP + articleCategoryCounter + TCBConstants.UNDERSCORE
                        + GROUP + counter + TCBConstants.UNDERSCORE + PROPERTY, ARTICLE_CATEGORY_TAGS_PROPERTY);
                int articleTagCounter = 1;
                queryMap.put(GROUP + articleCategoryCounter + TCBConstants.UNDERSCORE
                        + GROUP + counter + TCBConstants.UNDERSCORE + PROPERTY
                        + TCBConstants.DOT + counter + TCBConstants.UNDERSCORE + VALUE, articleCategoryTag);
                queryMap.put(GROUP + articleCategoryCounter + TCBConstants.UNDERSCORE
                        + GROUP + (counter + 1) + TCBConstants.UNDERSCORE
                        + PROPERTY, ARTICLE_TAGS_PROPERTY);
                for (String articleTagTag : articleTag) {
                    queryMap.put(GROUP + articleCategoryCounter + TCBConstants.UNDERSCORE
                            + GROUP + (counter + 1) + TCBConstants.UNDERSCORE
                            + PROPERTY + TCBConstants.DOT + articleTagCounter
                            + TCBConstants.UNDERSCORE + VALUE, articleTagTag);
                    articleTagCounter++;
                }
                queryMap.put(articleCategoryCounter + TCBConstants.UNDERSCORE + GROUPP + AND, TRUE);
                articleCategoryCounter++;
            }
        }
        queryMap.put(ORDERBY, ARTICLE_DATE);
        queryMap.put(ORDERBYSORT, DESC);
        queryMap.put(OFFSET, offset);
        queryMap.put(LIMIT, resultCount);
        return queryMap;
    }


    /**
     * This method returns query
     *
     * @param rootPath
     * @param articleTag
     * @param resultCount
     * @return Map<String, String> queryMap
     * generate query basis on content type filter and category filter.
     * content type filter or content type filter
     * category filter or category filter
     * content type filter and category filter
     */
    @Override
    public Map<String, String> generateQuery(final String rootPath, final List<String> articleCategory,
                                             final List<String> articleTag, final String resultCount,
                                             final String offset) {
        Map<String, String> queryMap = new TreeMap<>();
        return articleCategory.isEmpty() && articleTag.isEmpty()
                ? isPageLoad(rootPath, articleCategory, articleTag, queryMap,
                resultCount, offset) : articleFilter(rootPath, articleCategory,
                articleTag, queryMap, resultCount, offset);
    }

    /**
     * This method returns Article pages in Json
     *
     * @param resourceResolver
     * @param queryMap
     * @return JSONArray articleJson
     */
    @Override
    public String getResult(final ResourceResolver resourceResolver, final Map<String, String> queryMap) {
        try {
            List<StoryListingBean> listingBeanList = new ArrayList<>();
            Session session = resourceResolver.adaptTo(Session.class);
            QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
            Query query = queryBuilder.createQuery(PredicateGroup.create(queryMap), session);

            if (query == null) {
                return createEmptyResultJson();
            }
            SearchResult result = query.getResult();
            String totalResultCount = String.valueOf(result.getTotalMatches());
            if (result.getTotalMatches() > 0) {
                processResultNodes(result.getNodes(), resourceResolver, listingBeanList);
                createResultJson(listingBeanList, totalResultCount);
            } else {
                return createEmptyResultJson();
            }
        } catch (RepositoryException e) {
            LOG.error("Exception while constructing data for articles: {0}", e);
        }

        return json;
    }

    private String createEmptyResultJson() {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put(TOTAL, "0");
        jsonMap.put(RESULT, new ArrayList<>());
        return new Gson().toJson(jsonMap);
    }

    private void processResultNodes(final Iterator<Node> nodeIterator, final ResourceResolver resourceResolver,
                                    final List<StoryListingBean> listingBeanList) throws RepositoryException {
        while (nodeIterator.hasNext()) {
            Node node = nodeIterator.next();
            Resource resource = resourceResolver.getResource(node.getPath()
                    + TCBConstants.SLASH + JcrConstants.JCR_CONTENT);
            Resource thumbnail = resourceResolver.getResource(node.getPath()
                    + TCBConstants.SLASH + JcrConstants.JCR_CONTENT + TCBConstants.SLASH + IMAGE);

            if (resource != null) {
                StoryListingBean storyListingBeanObj = createStoryListingBean(resourceResolver,
                        resource, thumbnail, node.getPath());
                listingBeanList.add(storyListingBeanObj);
            }
        }
    }

    private void createResultJson(final List<StoryListingBean> listingBeanList, final String totalResultCount) {
        if (!CollectionUtils.isEmpty(listingBeanList)) {
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put(TOTAL, totalResultCount);
            jsonMap.put(RESULT, listingBeanList.toArray());
            json = new Gson().toJson(jsonMap);
        }
    }

    private StoryListingBean createStoryListingBean(final ResourceResolver resourceResolver,
                                                    final Resource resource, final Resource thumbnail,
                                                    final String path) {
        StoryListingBean storyListingBeanObj = resource.adaptTo(StoryListingBean.class);
        storyListingBeanObj.setPath(PlatformUtils.isInternalLink(resourceResolver,
                resource.getPath().replace(TCBConstants.SLASH + JcrConstants.JCR_CONTENT,
                        StringUtils.EMPTY)));
        LocalDateTime dateTime = LocalDateTime.parse(resource.getValueMap().get(ARTICLE_CREATION_DATE,
                String.class), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        PageManager pm = resourceResolver.adaptTo(PageManager.class);
        Page currentPage = pm.getContainingPage(resource);
        storyListingBeanObj.setArticleCreationDate(PlatformUtils.setTimeFormat(currentPage.getLanguage(true), dateTime));

        if (thumbnail != null) {
            storyListingBeanObj.setImage(PlatformUtils.getThumbnailImagePath(
                    thumbnail.getValueMap().get(FILE_REFERNCE, String.class)));
        }
        List<String> categoryList = getCategoryList(resource, resourceResolver, path);
        if (!CollectionUtils.isEmpty(categoryList)) {
            storyListingBeanObj.setArticleCategory(categoryList);
        }

        return storyListingBeanObj;
    }

    public List<String> getCategoryList(final Resource resource,
                                        final ResourceResolver resourceResolver, final String path) {
        List<String> category = new ArrayList<>();
        String[] tagsArray = resource.getValueMap().get(CATEGORY_PROPERTY, String[].class);
        for (String str : tagsArray) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            Tag tag = tagManager.resolve(str);
            if (null != tag) {
                Resource localeResource = resourceResolver.getResource(path);
                PageManager pm = resourceResolver.adaptTo(PageManager.class);
                Page currentPage = pm.getContainingPage(localeResource);
                category.add(tag.getTitle(currentPage.getLanguage()));
            }
        }
        return category;
    }
}

