package com.techcombank.core.service;

import java.util.Optional;

public interface RecaptchaService {
    Optional<String> getSiteKey(String configPath);

    Optional<String> getSecretKey(String configPath);
}
