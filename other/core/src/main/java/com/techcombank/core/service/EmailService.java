package com.techcombank.core.service;

import com.adobe.granite.workflow.exec.WorkItem;
import org.apache.sling.api.resource.ResourceResolver;

public interface EmailService {

    String sendEmail(String subjectLine, String messageBody, String[] recipients);

    String getUserEmail(WorkItem workItem, ResourceResolver resourceResolver);

}
