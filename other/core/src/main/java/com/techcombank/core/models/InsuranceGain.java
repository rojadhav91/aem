package com.techcombank.core.models;

import com.techcombank.core.service.InsuranceGainService;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InsuranceGain {

    @ValueMapValue
    @Getter
    private String dob;

    @ValueMapValue
    @Getter
    private String genderLabel;

    @ValueMapValue
    @Getter
    private String male;

    @ValueMapValue
    @Getter
    private String female;

    @ValueMapValue
    @Getter
    private String amount;

    @ValueMapValue
    @Getter
    private String amountInput;

    @ValueMapValue
    @Getter
    private String background;

    @Getter
    private String backgroundMobile;

    @ValueMapValue
    @Getter
    private String alt;

    @ValueMapValue
    @Getter
    private String icon;

    @Getter
    private String iconMobile;

    @ValueMapValue
    @Getter
    private String altIcon;

    @ValueMapValue
    @Getter
    private String interest;

    @ValueMapValue
    @Getter
    private String cta;

    @ValueMapValue
    @Getter
    private String button;

    @ValueMapValue
    @Getter
    private String buttonImage;

    @Getter
    private String buttonImageMobile;

    @ValueMapValue
    @Getter
    private String altButton;

    @ValueMapValue
    @Getter
    private String link;

    @ValueMapValue
    @Getter
    private String openInNewTab;

    @ValueMapValue
    @Getter
    private String nofollow;

    @ValueMapValue
    @Getter
    private String errorDate;

    @ValueMapValue
    @Getter
    private String errorAge;

    @ValueMapValue
    @Getter
    private String type;

    @ValueMapValue
    @Getter
    private String toolTipIcon;

    @Getter
    private String toolTipIconMobile;

    @ValueMapValue
    @Getter
    private String toolTipAltText;

    @ValueMapValue
    @Getter
    private String currencyText;

    @OSGiService
    private ResourceResolverService resourceResolverService;
    private ResourceResolver resourceResolver;

    @OSGiService
    private InsuranceGainService insuranceGainService;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    public String getBackground() {
        return PlatformUtils.getWebImagePath(background);
    }

    public String getBackgroundMobile() {
        return PlatformUtils.getMobileImagePath(background);
    }

    public String getIcon() {
        return PlatformUtils.getWebImagePath(icon);
    }

    public String getIconMobile() {
        return PlatformUtils.getMobileImagePath(icon);
    }

    public String getToolTipIcon() {
        return PlatformUtils.getWebImagePath(toolTipIcon);
    }

    public String getToolTipIconMobile() {
        return PlatformUtils.getMobileImagePath(toolTipIcon);
    }

    public String getGenericList() {
        return insuranceGainService.getGenericList(resourceResolver);
    }

    public String getButtonImage() {
        return PlatformUtils.getWebImagePath(buttonImage);
    }

    public String getButtonImageMobile() {
        return PlatformUtils.getMobileImagePath(buttonImage);
    }

    public String getLink() {
        return PlatformUtils.isInternalLink(resourceResolver, link);
    }

    public String getInterationType() {
        return PlatformUtils.getUrlLinkType(link);
    }

}
