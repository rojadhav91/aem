package com.techcombank.core.models;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface for XFPageModel
 */
@ConsumerType
public interface XFPageModel {

    /**
     * headerX F
     */
    String getHeaderXF();

    /**
     * footerXF
     */
    String getFooterXF();

}
