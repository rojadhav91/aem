package com.techcombank.core.workflows;

import com.adobe.granite.asset.api.AssetManager;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.WorkflowNotificationService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

import static com.techcombank.core.constants.TCBConstants.CONTENT_DAM_TECHCOMBANK_PATH;
import static com.techcombank.core.constants.TCBConstants.DASH;
import static com.techcombank.core.constants.TCBConstants.DOT;
import static com.techcombank.core.constants.TCBConstants.ETC;
import static com.techcombank.core.constants.TCBConstants.SLASH;
import static com.techcombank.core.constants.TCBConstants.VAR;

@Slf4j
@Component(service = WorkflowProcess.class, property = {"process.label = Archive Workflow Process"})
public class ArchiveWorkflowProcessStep implements WorkflowProcess {

    @Reference
    private WorkflowNotificationService workflowNotificationService;
    @Reference
    private ResourceResolverService resourceResolverService;
    private String[] paths;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession, final MetaDataMap metaDataMap)
            throws WorkflowException {
        String payload = workItem.getWorkflowData().getPayload().toString();
        log.debug("Archive Workflow Process {} ", payload);
        paths = new String[] {CONTENT_DAM_TECHCOMBANK_PATH + "/brand-assets",
                CONTENT_DAM_TECHCOMBANK_PATH + "/public-site", CONTENT_DAM_TECHCOMBANK_PATH + "/rdb-app",
                CONTENT_DAM_TECHCOMBANK_PATH + "/cdb-app", CONTENT_DAM_TECHCOMBANK_PATH + "/master-data"};

        try (ResourceResolver resourceResolver = workflowSession.adaptTo(ResourceResolver.class)) {
            if (StringUtils.startsWith(payload, VAR) || StringUtils.startsWith(payload, ETC)) {
                String resPath = payload.concat(TCBConstants.VAR_FILTER);
                for (Resource res : Objects.requireNonNull(
                        Objects.requireNonNull(resourceResolver).getResource(resPath)).getChildren()) {
                    String root = Objects.requireNonNull(res.adaptTo(ValueMap.class)).get("root", String.class);
                    archiveProcess(Objects.requireNonNull(root), workItem);
                }
            } else {
                archiveProcess(payload, workItem);
            }
        }
    }

    private void archiveProcess(@NonNull String payload, WorkItem workItem) {
        try (ResourceResolver resolver = resourceResolverService.getWriteResourceResolver()) {
            if (payload.startsWith(CONTENT_DAM_TECHCOMBANK_PATH)) {
                AssetManager assetMgr = resolver.adaptTo(AssetManager.class);
                String dest = payload;
                String year = String.valueOf(LocalDate.now().getYear());
                if (Arrays.stream(paths).parallel().anyMatch(payload::startsWith)) {
                    dest = dest.replace(CONTENT_DAM_TECHCOMBANK_PATH + SLASH, StringUtils.EMPTY);
                    dest = dest.substring(0, dest.indexOf(SLASH) + 1) + year + dest.substring(dest.indexOf(SLASH));
                    dest = CONTENT_DAM_TECHCOMBANK_PATH + "/archive/" + dest;
                } else {
                    dest = dest.replace(CONTENT_DAM_TECHCOMBANK_PATH,
                            CONTENT_DAM_TECHCOMBANK_PATH + "/archive/others/" + year);
                }
                createFolderPath(resolver, dest);
                dest = checkNameAvailability(resolver, dest);
                Objects.requireNonNull(assetMgr).moveAsset(payload, dest);
            } else {
                PageManager pageMgr = resolver.adaptTo(PageManager.class);
                Page page = Objects.requireNonNull(pageMgr).getPage(payload);
                Objects.requireNonNull(pageMgr).createRevision(page);
                Objects.requireNonNull(pageMgr).delete(page, false, true);
            }
            resolver.commit();
            String initiator = workItem.getWorkflow().getInitiator();
            String emailTitle = "Workflow archive successful";
            String emailBody = TCBConstants.WORKFLOW_TITLE_VAL + "archive successful";
            if (StringUtils.isNotBlank(initiator)) {
                workflowNotificationService.inboxNotification(resolver, initiator, emailTitle, emailBody, payload);
            }
        } catch (WCMException | PersistenceException e) {
            log.error("Failed archive process in workflow. {}", payload);
        }
    }

    private String checkNameAvailability(ResourceResolver resolver, String destination) {
        Resource res = resolver.getResource(destination);
        String tempPath = destination;
        int i = 1;
        while (Objects.nonNull(res)) {
            String name = res.getName();
            if (name.contains(DOT)) {
                tempPath = destination.replace(DOT, DASH + String.valueOf(i++) + DOT);
            } else {
                tempPath = destination.concat(DASH + String.valueOf(i++));
            }
            res = resolver.getResource(tempPath);
        }
        return tempPath;
    }

    private static void createFolderPath(ResourceResolver resolver, String destination) {
        if (!Objects.nonNull(resolver.getResource(destination.substring(0, destination.lastIndexOf(SLASH))))) {
            try {
                Session session = resolver.adaptTo(Session.class);
                JcrUtil.createPath(destination.substring(0, destination.lastIndexOf(SLASH)),
                        JcrResourceConstants.NT_SLING_FOLDER, Objects.requireNonNull(session));
                session.save();
            } catch (RepositoryException e) {
                log.error("Failed to create folder path. {}", destination, e);
            }
        }
    }
}
