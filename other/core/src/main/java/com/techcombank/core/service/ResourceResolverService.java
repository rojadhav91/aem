package com.techcombank.core.service;

import org.apache.sling.api.resource.ResourceResolver;

public interface ResourceResolverService {
    ResourceResolver getReadResourceResolver();

    ResourceResolver getWriteResourceResolver();
}
