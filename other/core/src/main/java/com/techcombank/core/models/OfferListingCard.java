package com.techcombank.core.models;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.google.gson.annotations.Expose;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = OfferListingCard.class)
public class OfferListingCard {
    /**
     * The thumbnail image of the offer listing.
     */
    @ValueMapValue
    @Setter
    @Expose
    private String thumbnail;

    /**
     * The description of the offer listing.
     */
    @ValueMapValue
    @Expose
    private String description;

    /**
     * The expiry date of the offer listing.
     */
    @ValueMapValue
    @Setter
    @Expose
    private String expiryDate;

    /**
     * The categories that the offer listing belongs to.
     */
    @ValueMapValue
    private String[] category;

    /**
     * The cards that the offer listing is valid for.
     */
    @ValueMapValue
    @Getter
    private String[] card;

    /**
     * The merchants that the offer listing is valid for.
     */
    @ValueMapValue
    @Getter
    private String[] merchant;

    @ValueMapValue
    @Expose
    private String favourite;

    /**
     * The URL of the offer listing.
     */
    @ValueMapValue
    @Setter
    @Expose
    private String url;

    /**
     * Whether the offer listing URL should be opened in a new tab.
     */
    @ValueMapValue
    @Expose
    private String openInNewTab;

    /**
     * Whether the offer listing URL should have a no-follow attribute.
     */
    @ValueMapValue
    @Expose
    private String noFollow;

    @ValueMapValue
    @Setter
    @Expose
    private String interactionType;

    @Setter
    @Expose
    private ArrayList<String> products;

    @Setter
    @Expose
    private ArrayList<String> partner;

}
