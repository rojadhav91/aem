package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

/**
 * A Sling model representing a list of event tile items.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListEventTileModel {

    /**
     * The list of event tile item.
     */
    @Inject
    @Getter
    private List<ListEventTileItem> eventTileItem;
}
