package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.models.CardListingModel;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Objects;

/**
 * This model class is for CardListingModelImpl Model
 */
@Model(adaptables = Resource.class, adapters = {
        CardListingModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        CardListingModelImpl.RESOURCE_TYPE)
@Getter
public class CardListingModelImpl implements CardListingModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/cardlist";

    /**
     * The ResourceResolver.
     */
    @Inject
    private ResourceResolver resolver;

    /**
     * The cardTagRoot value.
     */
    @ValueMapValue
    private String cardTagRoot;

    /**
     * The nudgeTagRoot value.
     */
    @ValueMapValue
    private String nudgeTagRoot;

    /**
     * The cardCFRootPath value.
     */
    @ValueMapValue
    private String cardCFRootPath;

    /**
     * The ctaCompLabel value.
     */
    @ValueMapValue
    private String ctaCompLabel;

    /**
     * The ctaCompLink value.
     */
    @ValueMapValue
    private String ctaCompLink;

    /**
     * The ctaCompTarget value.
     */
    @ValueMapValue
    private String ctaCompTarget;

    /**
     * The ctaCompFollow value.
     */
    @ValueMapValue
    private boolean ctaCompFollow;
    /**
     * The bannerImg value.
     */
    @ValueMapValue
    private String bannerImg;
    /**
     * The imageAlt value.
     */
    @ValueMapValue
    private String imageAlt;
    /**
     * The bannerTitle value.
     */
    @ValueMapValue
    private String bannerTitle;
    /**
     * The bannerDesc value.
     */
    @ValueMapValue
    private String bannerDesc;
    /**
     * The bannerLink value.
     */
    @ValueMapValue
    private String bannerLink;
    /**
     * The bannerTarget value.
     */
    @ValueMapValue
    private boolean bannerTarget;
    /**
     * The bannerFollow value.
     */
    @ValueMapValue
    private boolean bannerFollow;
    /**
     * The cardTypeLabel value.
     */
    @ValueMapValue
    private String cardTypeLabel;
    /**
     * The regImg value.
     */
    @ValueMapValue
    private String regImg;
    /**
     * The regImgMbl value.
     */
    private String regImgMobile;
    /**
     * The regTitle value.
     */
    @ValueMapValue
    private String regTitle;
    /**
     * The regSubTitle value.
     */
    @ValueMapValue
    private String regSubTitle;
    /**
     * The imageRegAlt value.
     */
    @ValueMapValue
    private String imageRegAlt;
    /**
     * The regCtaMobile value.
     */
    @ValueMapValue
    private String regCtaMobile;
    /**
     * The RegisterSteps value.
     */
    @ChildResource
    private List<RegisterSteps> steps;
    /**
     * The bottomText value.
     */
    @ValueMapValue
    private String bottomText;

    /**
     * The bottomLinkLabel value.
     */
    @ValueMapValue
    private String bottomLinkLabel;

    /**
     * The bottomLink value.
     */
    @ValueMapValue
    private String bottomLink;
    /**
     * The ctaBtmTarget value.
     */
    @ValueMapValue
    private String ctaBtmTarget;
    /**
     * The ctaBtmFollow value.
     */
    @ValueMapValue
    private boolean ctaBtmFollow;
    /**
     * The cardTags value.
     */
    private String cardTags;
    /**
     * The nudgeTags value.
     */
    private String nudgeTags;
    /**
     * The gsonObj value.
     */
    private Gson gsonObj;
    /**
     * The JsonArray value.
     */
    private JsonArray jsonAry;

    @PostConstruct
    void init() {
        gsonObj = new Gson();
        jsonAry = new JsonArray();
        if (StringUtils.isNotEmpty(cardTagRoot) && StringUtils.isNotEmpty(cardCFRootPath)
                && Objects.nonNull(resolver)) {
            final TagManager tagManager = resolver.adaptTo(TagManager.class);
            Tag rootCardTag = Objects.requireNonNull(tagManager).resolve(cardTagRoot);
            if (Objects.nonNull(rootCardTag)) {
                cardTags = listTags(rootCardTag, tagManager);
            }
            if (StringUtils.isNotEmpty(nudgeTagRoot)) {
                Tag rootNudgeTag = Objects.requireNonNull(tagManager).resolve(nudgeTagRoot);
                if (Objects.nonNull(rootNudgeTag)) {
                    nudgeTags = listTags(rootNudgeTag, tagManager);
                }
            }
        }
        ctaCompLink = StringUtils.isNotEmpty(ctaCompLink)
                ? PlatformUtils.isInternalLink(resolver, ctaCompLink)
                : StringUtils.EMPTY;
        bannerLink = StringUtils.isNotEmpty(bannerLink)
                ? PlatformUtils.isInternalLink(resolver, bannerLink)
                : StringUtils.EMPTY;
        bottomLink = StringUtils.isNotEmpty(bottomLink)
                ? PlatformUtils.isInternalLink(resolver, bottomLink)
                : StringUtils.EMPTY;
        regCtaMobile = StringUtils.isNotEmpty(regCtaMobile)
                ? PlatformUtils.isInternalLink(resolver, regCtaMobile)
                : StringUtils.EMPTY;
    }
    /**
     * The regImg value.
     */
    @Override
    public String getRegImg() {
        return PlatformUtils.getWebImagePath(regImg);
    }
    /**
     * The regImgMbl value.
     */
    @Override
    public String getRegImgMobile() {
        return  PlatformUtils.getMobileImagePath(regImg);
    }

    private String listTags(final Tag rootTag, final TagManager tagManager) {
        Resource resource = resolver.getResource(rootTag.getPath());
        if (Objects.requireNonNull(resource).hasChildren()) {
            for (Resource value : Objects.requireNonNull(resource).getChildren()) {
                Tag tag = Objects.requireNonNull(tagManager).resolve(value.getPath());
                if (tagManager.find(cardCFRootPath, new String[] {tag.getTagID()}).getSize() > 0) {
                    JsonObject jsonObj = new JsonObject();
                    jsonObj.add("title", gsonObj.toJsonTree(tag.getTitle()));
                    jsonObj.add("id", gsonObj.toJsonTree(tag.getTagID()));
                    jsonAry.add(jsonObj);
                }
            }
        }
        return gsonObj.toJson(jsonAry);
    }

    @Getter
    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    public static class RegisterSteps {
        /**
         * The stepTitle value.
         */
        @ValueMapValue
        private String stepTitle;
        /**
         * The stepImg value.
         */
        @ValueMapValue
        private String stepImg;
        /**
         * The stepImgMbl value.
         */
        private String stepImgMobile;
        /**
         * The imageStpAlt value.
         */
        @ValueMapValue
        private String imageStpAlt;
        /**
         * The stepImg value.
         */
        public String getStepImg() {
            return PlatformUtils.getWebImagePath(stepImg);
        }
        /**
         * The stepImgMbl value.
         */
        public String getStepImgMobile() {
            return PlatformUtils.getMobileImagePath(stepImg);
        }
    }
}
