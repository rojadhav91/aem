package com.techcombank.core.pojo;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/*
 *A Sling model that represents a management panel.
 */
@Model(adaptables = Resource.class,
        adapters = ManagementPanel.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ManagementPanel {

    /*
     *The profile image large size.
     */
    @Getter
    @ValueMapValue
    private String profileImage;

    /*
     *Alternate text for large profile image.
     */
    @Getter
    @ValueMapValue
    private String alt;

    /*
     *Profile name field.
     */
    @Getter
    @ValueMapValue
    private String name;

    /*
     *designation field for managment panel.
     */
    @Getter
    @ValueMapValue
    private String designation;

    /*
     *description field for managment panel.
     */
    @Getter
    @ValueMapValue
    private String description;

    @Getter
    private String profileImageMobile;

    public String getProfileImage() {
        return PlatformUtils.getWebImagePath(profileImage);
    }

    public String getProfileImageMobile() {
        return PlatformUtils.getMobileImagePath(profileImage);
    }

}
