package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

/**
 * The Class SocialShareInfoModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = SocialShareInfoModel.class)
public class SocialShareInfoModel {
    /**
     * List of Tiles
     */
    @ChildResource
    @Getter
    private List<SocialShareInfoItemModel> socialShareItems;

    @ValueMapValue
    @Getter
    private String socialShareTitle;

}
