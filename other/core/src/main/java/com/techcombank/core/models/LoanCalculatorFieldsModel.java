package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LoanCalculatorFieldsModel {

    /**
     * To author tab name.
     */
    @ValueMapValue
    @Getter
    private String tabName;

    /**
     * Calculation type and formula for loan calculator.
     */
    @ValueMapValue
    @Getter
    private String calculationTypeDropdown;

    /**
     * Output panel background for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingBg;

    @Getter
    private String decreasingBgMobile;

    /**
     * Output panel card icon for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingCardIcon;

    @Getter
    private String decreasingCardIconMobile;

    /**
     * Icon image alt text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingImgAltText;

    /**
     * Monthly label text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingMonthlyLabelText;

    /**
     * Payment from label text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingFromLabelText;


    /**
     * From payment value for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingFromPayValue;

    /**
     * Payment to label text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingToLabelText;

    /**
     * To payment value for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingToPayValue;

    /**
     * Total interest payable label text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingTotalIntLabel;

    /**
     * Total interest payable value for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingTotalIntValue;

    /**
     * Currency text for input and ouput amount for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingCurrencyText;

    /**
     * Month text for loan term for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingMonthTextLoanTerm;

    /**
     * %/year for interest rate for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingPercentageIntRate;

    /**
     * Link text label for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingLinkTextLabel;

    /**
     * Popup title text for decreasing balance sheet.
     */
    @ValueMapValue
    @Getter
    private String decreasingPopUpTitleText;
    /**
     * A list of left panel for loan calculator.
     */

    /**
     * Output panel background for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedBg;

    @Getter
    private String fixedBgMobile;

    /**
     * Output panel card icon image for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedCardIcon;

    @Getter
    private String fixedCardIconMobile;

    /**
     * Icon image alt text for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedImageAltText;

    /**
     * Monthly payment amount for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedAmountLabelText;

    /**
     * Monthly payment amount value for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedMonthlyAmountValue;

    /**
     * Total interest payable label text for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedTotalIntLabel;

    /**
     * Total interest payable value for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedTotalIntValue;

    /**
     * Currency text for input and output amount for Table of fixed monthly payments.
     */
    @ValueMapValue
    @Getter
    private String fixedCurrencyText;


    /**
     * Output panel background for car loan.
     */
    @ValueMapValue
    @Getter
    private String carBg;

    @Getter
    private String carBgMobile;

    /**
     * Output panel card icon for car loan.
     */
    @ValueMapValue
    @Getter
    private String carCardIcon;


    /**
     * Output panel card icon for car loan.
     */
    @Getter
    private String carCardIconMobile;

    /**
     * Icon image alt text for car loan.
     */
    @ValueMapValue
    @Getter
    private String carIconAltText;

    /**
     * monthly principal label text for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonPrincipalLabel;

    /**
     * monthly principal value for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonPrincipalValue;

    /**
     * monthly interest rate label text for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonIntRateLabel;

    /**
     * Monthly interest value for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonIntRateValue;

    /**
     * Monthly installment label text for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonInstallmentLabel;

    /**
     * Monthly installment value for car loan.
     */
    @ValueMapValue
    @Getter
    private String carMonInstallmentValue;

    /**
     * Currency text for input and ouput amount for car loan.
     */
    @ValueMapValue
    @Getter
    private String carCurrencyText;

    /**
     * Link text label for car loan.
     */
    @ValueMapValue
    @Getter
    private String carLinkText;

    /**
     * Popup title text for car loan.
     */
    @ValueMapValue
    @Getter
    private String carPopUpTitle;


    public String getDecreasingBg() {
        return PlatformUtils.getWebImagePath(decreasingBg);
    }

    public String getDecreasingBgMobile() {
        return PlatformUtils.getMobileImagePath(decreasingBg);
    }

    public String getDecreasingCardIcon() {
        return PlatformUtils.getWebImagePath(decreasingCardIcon);
    }

    public String getDecreasingCardIconMobile() {
        return PlatformUtils.getMobileImagePath(decreasingCardIcon);
    }

    public String getFixedBg() {
        return PlatformUtils.getWebImagePath(fixedBg);
    }

    public String getFixedBgMobile() {
        return PlatformUtils.getMobileImagePath(fixedBg);
    }

    public String getFixedCardIcon() {
        return PlatformUtils.getWebImagePath(fixedCardIcon);
    }

    public String getFixedCardIconMobile() {
        return PlatformUtils.getMobileImagePath(fixedCardIcon);
    }

    public String getCarBg() {
        return PlatformUtils.getWebImagePath(carBg);
    }

    public String getCarBgMobile() {
        return PlatformUtils.getMobileImagePath(carBg);
    }

    public String getCarCardIcon() {
        return PlatformUtils.getWebImagePath(carCardIcon);
    }

    public String getCarCardIconMobile() {
        return PlatformUtils.getMobileImagePath(carCardIcon);
    }

}
