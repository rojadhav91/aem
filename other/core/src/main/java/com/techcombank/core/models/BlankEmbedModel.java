package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class BlankEmbedModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = BlankEmbedModel.class)
public class BlankEmbedModel {

    /**
     * HTML
     * - Description: The code of the content to embed.
    */
    @ValueMapValue
    @Getter
    private String html;

}
