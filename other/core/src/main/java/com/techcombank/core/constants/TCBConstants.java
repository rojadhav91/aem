package com.techcombank.core.constants;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;

public final class TCBConstants {
    public static final String HTMLEXTENSION = ".html";
    public static final String PDFEXTENSION = ".pdf";
    public static final String AEM_DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DATE_DD_MM_YY_FORMAT = "dd MMMM yyyy";
    public static final String HASH = "#";
    public static final String QUESTION_MARK = "?";
    public static final String ESCAPED_QUESTION_MARK = "\\?";
    public static final String EXIT = "exit";
    public static final String OTHER = "other";
    public static final String DOWNLOAD = "download";
    public static final String CAROUSEL_CLICK = "carouselClick";

    // Header Constants
    public static final String TCB_BASE_PATH = "/content/techcombank/web/";

    public static final String TCB_DOMAIN = "techcombank.com";
    public static final String RECAPTCHA = "/etc/cloudservices/recaptcha";
    public static final String SECRET_KEY = "secretKey";
    public static final String SITE_KEY = "siteKey";
    public static final String PROP_ALIAS = "sling:alias";
    public static final String SLASH = "/";
    public static final String UNDERSCORE = "_";
    public static final String PERCENTAGE = "%";
    public static final String HYPHEN = "-";
    public static final String SPACE = " ";
    public static final String PERSONAL_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/personal-banking";
    public static final String BUSINESS_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/business-banking";
    public static final String CONTENT_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/content-page";
    public static final String CQ_TEMPLATE = NameConstants.NN_TEMPLATE;
    public static final String CQ_PAGE = NameConstants.NT_PAGE;
    public static final String PROP_ENABLE_ALL_PRODUCT = "enableAllProductLink";
    public static final int COUNTRY_INDEX = 4;
    public static final int LANGUAGE_INDEX = 5;
    public static final int PATH_START_INDEX = 6;
    public static final String APPLICATION_JSON = "application/json";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BASIC = "Basic ";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final int STATUS_ERROR = 500;
    public static final int STATUS_BAD_REQUEST = 400;
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String RESPONSE_STATUS = "responseStatus";
    public static final String HEADER_REQUEST = "headerRequest";
    public static final String DOCUMENT = "document";

    public static final String ERROR = "Error";
    public static final String INVALID_RESOURCE_PATH = "invalid_resource_path";
    public static final String ITEMS = "items";
    public static final String ITEMSORDER = "itemsOrder";
    public static final String COLON_ITEMS = ":items";
    public static final String COLON_ITEMSORDER = ":itemsOrder";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String AUTHOR = "author";
    public static final String HEADER_XF = "headerXF";
    public static final String FOOTER_XF = "footerXF";
    public static final int CONNECTION_TIMEOUT = 10000;

    // Rate Constants
    public static final String DASH = "-";
    public static final String COLON = ":";
    public static final String CF_DATA_PATH = "/data/master";
    public static final String VFX_RATE_LATEST_UPDATED_DATE = "latestUpdatedDate";
    public static final String EXCHANGE_RATE_DATE_TIME = "%exchangeRateDateTime";
    public static final String EXCHNAGE_RATE_DAY_MONTH_FORMAT = "%02d";
    public static final String RATE_CURRENCY_FORMAT = "%,.0f";
    public static final String VFX_RATE_DATA = "data";
    public static final String VFX_RATE_ID = "id";
    public static final String MY_PACKAGE_NAME = "my_packages";
    public static final String VFX_PACKAGE_NAME = "VFX-Rates-";
    public static final String EXCHNAGE_RATE_JSON_ARRAY = "exchangeRate";
    public static final String GOLD_RATE_JSON_ARRAY = "goldRate";
    public static final String OTHER_RATE_JSON_ARRAY = "otherRate";
    public static final String TENOR_RATE_JSON_ARRAY = "tenorRate";
    public static final String TENOR_INT_RATE_JSON_ARRAY = "tenorintRate";
    public static final String FIXING_RATE_JSON_ARRAY = "fixingRate";
    public static final String RATE_TIMESTAMP_JSON_ARRAY = "updatedTimes";
    public static final String CALCULATORS = "Calculators";
    public static final String BAR = "|";
    public static final String IMAGE_PNG_EXTENSION = ".png";
    public static final String IMAGE_SVG_EXTENSION = ".svg";
    public static final String APP_DOMAIN = "app-domain";
    public static final String APP_LOCAL_DOMAIN = "app-local-domain";
    public static final String SOURCE_APP = "app";
    public static final String IMAGE = "image";

    public static final String VALUE = "value";
    public static final String DOT = ".";
    public static final int THREE = 3;
    public static final int EIGHT = 8;
    public static final int SEVEN = 7;
    public static final String EXCHANGE_RATE = "exchange-rates";
    public static final String GOLD_RATE = "gold-rates";
    public static final String OTHER_RATE = "other-rates";
    public static final String TENOR_RATE = "tenor-rates";
    public static final String TENOR_INT_RATE = "tenor-int-rates";
    public static final String FIXING_RATE = "fixing-rates";
    public static final String VFX_RATE_JSON_DATA = "vfxData";

    // Nav Equal panel
    public static final String NAV_PANEL_TITLE = "Nav Equal Panel";

    public static final String PNG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.png";
    public static final String JPEG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
    public static final String PNG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.png";
    public static final String PNG_THUMBNAIL_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.thumbnail.319.319.png";
    public static final String JPEG_MOBILE_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.web.640.640.jpeg";
    public static final String JPEG_THUMBNAIL_IMAGE_RENDITION_PATH =
            "/jcr:content/renditions/cq5dam.thumbnail.319.319.jpeg";

    public static final String FIRST_APPORVER = "firstApprover";
    public static final String SECOND_APPORVER = "secondApprover";
    public static final String TARGET_PATH = "targetPath";
    public static final String ASSET_PUBLISHED = "assetPublished";
    public static final String ASSET_REFERENCE = "assetReference";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_ID = "userId";
    public static final String USER_PROFILE_EMAIL_PATH = "profile/email";
    public static final String EMAIL_ENABLE = "mailEnable";
    public static final String HISTORY_ENTRY_PATH = "historyEntryPath";
    public static final String CONTENT_TECHCOMBANK_PATH = "/content/techcombank";
    public static final String CONTENT_DAM_TECHCOMBANK_PATH = "/content/dam/techcombank";
    public static final String VAR = "/var";
    public static final String ETC = "/etc";
    public static final String VAR_FILTER = "/jcr:content/vlt:definition/filter";
    public static final String WORKITEM_METADATA = "/workItem/metaData";
    public static final String PROCESS_ARGS = "PROCESS_ARGS";
    public static final String WORKFLOW_TITLE = "workflowTitle";

    public static final String EQUALS_SYMBOL = "=";
    public static final String COMMA = ",";
    public static final String COMMENT = "\n Comment : ";
    public static final String STATUS = "Status";
    public static final String MESSAGE = "Message";
    public static final String MOVE = "Move";
    public static final String DESTINATION_FILE_NAME = "destinationFileName";
    public static final String WORKFLOW_TITLE_VAL = "Workflow Title : ";

    public static final String CTA = "cta";
    public static final String LINK_CLICK = "linkClick";
    public static final String JCR_TITLE = JcrConstants.JCR_TITLE;
    public static final String FAQ_DESCRIPTION = "faqDescription";
    public static final String FAQ_TITLE = "faqTitle";
    public static final String PATH = "path";
    public static final String CQ_FILTERPANELTAG = "cq:filterpaneltag";
    public static final String PROPERTY_1 = "1_property";
    public static final String PROPERTY_OPERATION_1 = "1_property.operation";
    public static final String EXISTS = "exists";
    public static final String RDB_APP_CONTENT_FRAGMENT_PATH = "/content/dam/techcombank/rdb-app/data";
    public static final String CDB_APP_CONTENT_FRAGMENT_PATH = "/content/dam/techcombank/cdb-app/data";
    public static final String WORKFLOW_TERMINATED_EMAIL_TITLE = "Workflow Terminated - Payload already in workflow";
    public static final String PAYLOAD_UNPUBLISHED_EMAIL_TITLE = "Workflow Terminated - Payload is not published";
    public static final String COMPARE_PANEL_PROJECT_NAME = "projectName";
    public static final String COMPARE_PANEL_LOAN_RATE_ON_CAPITAL_NEED = "loanRateOnCapitalNeed";
    public static final String COMPARE_PANEL_LOAN_RATE_ON_COLLATERAL_VALUE = "loanRateOnCollateralValue";
    public static final String COMPARE_PANEL_LOAN_TERM = "loanTerm";
    public static final String COMPARE_PANEL_COLLATERAL = "collateral";
    public static final String COMPARE_PANEL_TAG = "tag";

    //Credit rating
    public static final String DATE_VN_PATTERN = "dd/MM/yyyy";

    public static final String DATE_DEFAULT_PATTERN = "dd MMM yyyy";
    public static final String TIMEZONE_VN = "GMT+7";

    // QMS API
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String INVALID_TOKEN = "Invalid token.";
    public static final String API_ERROR_VALUE = "error";
    public static final String BEARER = "Bearer ";
    public static final String ERROR_CODE = "errorCode";
    public static final String ERROR_MSG = "errorMsg";

    //Sitemap Generator Related
    public static final String CHANGE_FREQUENCY_HOURLY = "hourly";
    public static final String CHANGE_FREQUENCY_DAILY = "daily";
    public static final String CHANGE_FREQUENCY_WEEKLY = "weekly";
    public static final String CHANGE_FREQUENCY_MONTHLY = "monthly";
    public static final String CHANGE_FREQUENCY_YEARLY = "yearly";
    public static final String CHANGE_FREQUENCY_ALWAYS = "always";
    public static final String CHANGE_FREQUENCY_NEVER = "never";

    public static final String SEO_PRIORITY = "seoPriority";
    public static final String CHANGE_FREQUENCY = "changeFrequency";

    public static final String CQ_ROBOTSTAGS = "cq:robotsTags";
    public static final String QUARTERS_TAGS = "/content/cq:tags/techcombank/quarters";
    public static final String NO_INDEX = "noIndex";

    public static final String CATEGORY_TAG = "categoryTag";
    public static final String CQ_TAGS = NameConstants.PN_TAGS;
    public static final String ORDER_BY = "orderby";
    public static final String ORDER_BY_SORT = "orderby.sort";
    public static final String ARTICLE_PAGE_TEMPLATE_PATH = "/conf/techcombank/settings/wcm/templates/article-page";
    public static final String ARTICLE_CREATION_DATE = "articleCreationDate";
    public static final String TYPE = "type";
    public static final String PROPERTYVALUE_1 = "1_property.value";
    public static final String PROPERTY_2 = "2_property";
    public static final String PROPERTYVALUE_2 = "2_property.value";
    public static final String PROPERTY_3 = "3_property";
    public static final String PROPERTYVALUE_3 = "3_property.value";
    public static final String UNDERSCORE_VALUE = "_value";
    public static final String JCR_CONTENT = "jcr:content/";
    public static final String RESULT_LIMIT = "p.limit";
    public static final String DESCENDING = "desc";
    public static final String TCB_WEB_BASE_PATH = "/content/techcombank/web/vn/";
    public static final String ARTICLE_IMAGE_PATH = "articleImagePath";
    public static final String ARTICLE_IMAGE_ALT_TEXT = "articleImageAltText";
    public static final String BG_BANNER_IMAGE = "bgBannerImage";
    public static final String BG_BANNER_IMAGE_ALT_TEXT = "bgBannerImgAlt";
    public static final String BG_BANNER_IMAGE_PATH = "/root/articleandevents/mastheadsimplebanner";
    public static final String PAGE_TITLE = NameConstants.PN_PAGE_TITLE;
    public static final String JCR_DESCRIPTION = NameConstants.PN_DESCRIPTION;
    public static final String AT = "@";
    public static final String ALL_RESULT = "-1";
    public static final String ZERO = "0";
    public static final String FROM = "from";

    public static final String TO = "to";
    public static final String SERVICE_TYPE = "serviceType";
    public static final String CREATE_DATE = "createDate";
    public static final String MESSAGE_ID = "messageId";
    public static final String ARTICLE_RELATED_POST_VALUE = "relatedPost";
    public static final String ARTICLE_LISTING_VALUE = "articleListing";
    public static final String ARTICLE_LISTING_ROOT_PATH =
            "techcombank:articles/article-category/article-secondary-category/";
    public static final String ARTICLE_RELATED_POST_VISIBILITY_FLAG = "hideArticleRelatedPost";
    public static final String AUTHOR_QUERY_STRING = "wcmmode=disabled";

    public static final String ARTICLE_TABLE_OF_CONTENT_VISIBILITY_FLAG = "hideTableOfContent";

    public static final String ARTICLE_CATEGORY_TAG = "/article-category/article-secondary-category";
    public static final String TAG = "tag";
    public static final String HIDE_ARTICLE_TAG_CLOUD = "hideArticleTagCloud";
    public static final String TEXT_COMPONENT_PATH = "techcombank/components/extended-core/text";

    public static final String ROOTPATH = "rootPath";

    private TCBConstants() {
    }
}
