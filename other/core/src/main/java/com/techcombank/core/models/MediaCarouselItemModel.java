package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.inject.Inject;

/**
 * The Class MediaCarouselItemModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MediaCarouselItemModel {

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Media Type
     * - Description: To choose individual carousel item type
    */
    @ValueMapValue
    @Getter
    private String mediaType;

    /**
     * Image
     * - Description: To choose carousel image /video thumbnail image
    */
    @ValueMapValue
    @Getter
    private String image;

    /**
     * Returns the mobile image rendition path for the carousel image.
     */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(image);
    }

    /**
     * Returns the web image rendition path for the carousel image.
     */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(image);
    }

    /**
     * Alt Text
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String altText;

    /**
     * Video Link URL
     * - Description: To choose video link URL (youtube)
    */
    @ValueMapValue
    @Getter
    private String videoLinkUrl;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getVideoLinkWebInteractionType() {
        return PlatformUtils.getUrlLinkType(videoLinkUrl);
    }

    /**
     * Carousel Title Text 1
     * - Description: To author title text to display per carousel
    */
    @ValueMapValue
    @Getter
    private String carouselTitleText1;

    /**
     * Carousel Title Text 2
     * - Description: To author additional title text to display per carousel
    */
    @ValueMapValue
    @Getter
    private String carouselTitleText2;

    /**
     * Sub Title Text
     * - Description: To author sub title text to display per carousel
    */
    @ValueMapValue
    @Getter
    private String subTitleText;

    /**
     * Description Text
     * - Description: To author summary text to display per carousel
    */
    @ValueMapValue
    @Getter
    private String descriptionText;

    /**
     * Link Text
     * - Description: To author link label text
    */
    @ValueMapValue
    @Getter
    private String linkText;

    /**
     * Link URL
     * - Description: To author destination page URL
    */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getLinkUrlWebInteractionType() {
        return PlatformUtils.getUrlLinkType(linkUrl);
    }

    /**
     * Open In New Tab
     * - Description: To select if link URL to open in new tab or same tab
    */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No Follow
     * - Description: To choose follow/no follow
    */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Use As Highlight
     * - Description: To choose which carousel item to be in focus display by default
    */
    @ValueMapValue
    @Getter
    private boolean useAsHighlight;

    /**
     * @return the linkURL
     */
    public String getLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, linkUrl);
    }
}
