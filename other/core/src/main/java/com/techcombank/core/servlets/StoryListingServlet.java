package com.techcombank.core.servlets;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.StoryListing;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component(service = {Servlet.class})
@SlingServletResourceTypes(resourceTypes = "techcombank/components/storylisting",
        methods = HttpConstants.METHOD_GET, selectors = "storylisting", extensions = "json")

@ServiceDescription("Story Listing Servlet")
public class StoryListingServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;
    @Reference
    private transient StoryListing storyListing;
    protected static final String PRIMARY_TYPE = "techcombank:articles/article-category/article-primary-category/";
    protected static final String SECONDARY_TYPE = "techcombank:articles/article-category/article-secondary-category/";
    protected static final String OFFSET = "offset";
    protected static final String LIMIT = "limit";
    protected static final String ROOTPATH = "rootPath";
    protected static final String ARTICLE_CATEGORY = "articleCategory";
    protected static final String ARTICLE_TAG = "articleTag";

    protected static final String UTF = "UTF-8";

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        List<String> articleCategory = new ArrayList<>();
        List<String> articleTag = new ArrayList<>();

        String offset = request.getParameter(OFFSET);
        String resultCount = request.getParameter(LIMIT);
        String rootPath = getRootPath(request);
        extractArticleFilters(request, articleCategory, articleTag);

        Map<String, String> queryMap = generateQueryMap(rootPath, articleCategory, articleTag, resultCount, offset);
        ResourceResolver resolver = request.getResourceResolver();
        String articleJson = storyListing.getResult(resolver, queryMap);

        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding(UTF);
        response.getWriter().write(articleJson);
    }

    private String getRootPath(final SlingHttpServletRequest request) {
        return request.getResource().getValueMap().get(ROOTPATH, String.class);
    }

    private void extractArticleFilters(final SlingHttpServletRequest request,
                                       final List<String> articleCategory, final List<String> articleTag) {
        String[] articleCategoryFilter = getParameterArray(request, ARTICLE_CATEGORY);
        String[] articleTagFilter = getParameterArray(request, ARTICLE_TAG);

        if (ArrayUtils.isNotEmpty(articleCategoryFilter) || ArrayUtils.isNotEmpty(articleTagFilter)) {
            populateArticleCategory(articleCategory, articleCategoryFilter, request);
            populateArticleTag(articleTag, articleTagFilter);
        } else {
            populateDefaultArticleFilters(articleCategory, request);
        }
    }

    private String[] getParameterArray(final SlingHttpServletRequest request, final String parameter) {
        String parameterValue = StringUtils.defaultIfEmpty(request.getParameter(parameter), StringUtils.EMPTY);
        return StringUtils.isEmpty(parameterValue) ? new String[]{} : parameterValue.split(TCBConstants.COMMA);
    }

    private void populateArticleCategory(final List<String> articleCategory, final String[] articleCategoryFilter,
                                         final SlingHttpServletRequest request) {
        if (ArrayUtils.isNotEmpty(articleCategoryFilter)) {
            for (String primaryType : articleCategoryFilter) {
                articleCategory.add(PRIMARY_TYPE + WordUtils.uncapitalize(primaryType)
                        .replace(TCBConstants.SPACE, TCBConstants.HYPHEN));
            }
        } else {
            articleCategory.addAll(Arrays.asList(request.getResource().getValueMap()
                    .get(ARTICLE_CATEGORY, String[].class)));
        }
    }

    private void populateArticleTag(final List<String> articleTag, final String[] articleTagFilter) {
        if (ArrayUtils.isNotEmpty(articleTagFilter)) {
            for (String secondaryType : articleTagFilter) {
                articleTag.add(SECONDARY_TYPE + WordUtils.uncapitalize(secondaryType)
                        .replace(TCBConstants.SPACE, TCBConstants.HYPHEN));
            }
        }
    }

    private void populateDefaultArticleFilters(final List<String> articleCategory,
                                               final SlingHttpServletRequest request) {
        articleCategory.addAll(Arrays.asList(request.getResource().getValueMap()
                .get(ARTICLE_CATEGORY, String[].class)));
    }

    private Map<String, String> generateQueryMap(final String rootPath, final List<String> articleCategory,
                                                 final List<String> articleTag, final String resultCount,
                                                 final String offset) {
        return storyListing.generateQuery(rootPath, articleCategory, articleTag, resultCount, offset);
    }
}
