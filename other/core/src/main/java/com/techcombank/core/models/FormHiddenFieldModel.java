package com.techcombank.core.models;

import com.adobe.cq.wcm.core.components.models.form.Field;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = Field.class)
public class FormHiddenFieldModel implements Field {

    @ValueMapValue
    @Getter
    private String id;

    @ValueMapValue
    @Getter
    private String name;

    @ValueMapValue
    @Getter
    private String value;

    @ValueMapValue
    @Getter
    private boolean checkboxValueText;

    @ValueMapValue
    @Getter
    private String valueText;
}
