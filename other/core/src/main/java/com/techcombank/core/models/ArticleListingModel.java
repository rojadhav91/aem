package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticleListingService;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;
import java.util.List;
import java.util.Locale;

/**
 * The Class ArticleListingModel.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticleListingModel {

    private static final Logger LOG = LoggerFactory.getLogger(ArticleListingModel.class);

    @Self
    private SlingHttpServletRequest request;

    /**
     * The label for the Subtitle.
     */
    @ValueMapValue
    @Getter
    private String subTitle;

    /**
     * The label for the load more Label.
     */
    @ValueMapValue
    @Getter
    private String loadMore;

    /**
     * The label for the see details Label.
     */
    @ValueMapValue
    @Getter
    private String seeDetails;

    /**
     * Open In New Tab
     */
    @ValueMapValue
    @Getter
    private String newTab;

    @Getter
    private String tagValue;

    @Inject
    private Page currentPage;

    private Locale language;

    @PostConstruct
    private void init() {
        ResourceResolver resourceResolver = request.getResourceResolver();
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        language = currentPage.getLanguage();
        LOG.info("LOCALE LANG = {}", language);
        String queryString = request.getQueryString();
        String tagName = queryString.substring(queryString.indexOf("=") + 1);
        String pageTag = TCBConstants.ARTICLE_LISTING_ROOT_PATH + tagName;
        Tag tag = tagManager.resolve(pageTag);
        if (tag != null) {
            tagValue = tag.getTitle(language);
        }
    }

    @OSGiService
    private ArticleListingService articleListingService;

    public List<Articles> getRelatedArticleList() throws RepositoryException {
        return articleListingService.getArticlesList(request, language);
    }

}
