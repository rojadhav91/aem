package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.StoryListingModel;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        adapters = StoryListingModel.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = StoryListingModelImpl.RESOURCE_TYPE)
public class StoryListingModelImpl implements StoryListingModel {
    protected static final String RESOURCE_TYPE = "techcombank/components/storylisting";

    /**
     * The label for the view more button Label.
     */
    @ValueMapValue
    @Getter
    private String viewMore;

    /**
     * The label for the Article Category label.
     */
    @ValueMapValue
    @Getter
    private String articleCategoryLabel;

    /**
     * The label for the Article ategory filter.
     */
    @ValueMapValue
    @Getter
    private String[] articleCategory;

    /**
     * The label for the ArticleTag label.
     */
    @ValueMapValue
    @Getter
    private String articleTagLabel;

    /**
     * The number of result.
     */
    @ValueMapValue
    @Getter
    private String result;
    /**
     * The cta label.
     */
    @ValueMapValue
    @Getter
    private String ctaLabel;

    /**
     * To open page in new tab.
     */
    @ValueMapValue
    @Getter
    private String newTab;

    /**
     * To open page in new tab.
     */
    @ValueMapValue
    @Getter
    private String nofollow;

    /**
     * To root path to search for the articleList.
     */
    @ValueMapValue
    @Getter
    private String rootPath;

    @ValueMapValue
    @Getter
    private String apply;

    /**
     * Default image for Story listing component
     */
    @ValueMapValue
    @Getter
    private String image;

    /**
     * A list of Story listing articleTag.
     */
    @ValueMapValue
    @Getter
    private String[] articleTag;

    /**
     * Error message if no article found.
     */
    @ValueMapValue
    @Getter
    private String errorMsg;

    /**
     * All filter label for Story listing component
     */
    @ValueMapValue
    @Getter
    private String allFilter;

    /**
     * filter label for Story listing component
     */
    @ValueMapValue
    @Getter
    private String filter;

    /**
     * Alt text for default image in Story listing component
     */
    @ValueMapValue
    @Getter
    private String alt;

    /**
     * Error title text.
     */
    @ValueMapValue
    @Getter
    private String errorTitle;

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    private TagManager tagManager;

    @PostConstruct
    protected void init() {
        tagManager = resourceResolver.adaptTo(TagManager.class);
        image = PlatformUtils.getThumbnailImagePath(image);

    }

    /**
     * Map of content type filter tag.
     */

    public Map<String, String> getContentTagMap(final String[] tagList) {
        Map<String, String> conentMap = new HashMap<>();
        if (!ArrayUtils.isEmpty(tagList)) {
            for (String content : tagList) {
                String[] arr = content.split(TCBConstants.SLASH);
                Tag tag = tagManager.resolve(content);
                if (null != tag) {
                    conentMap.put(arr[arr.length - 1], WordUtils.capitalize(tag.getTitle(currentPage.getLanguage())));
                }
            }
        }
        return conentMap;
    }

    /**
     * Map of content type filter tag.
     */
    @Override
    public Map<String, String> getArticleCategoryTags() {
        return getContentTagMap(articleCategory);
    }

    /**
     * Map of category type filter tag.
     */
    @Override
    public Map<String, String> getArticleTags() {
        return getContentTagMap(articleTag);
    }

    public String getInteraction() {
        return PlatformUtils.getInteractionType(viewMore, TCBConstants.OTHER);
    }

}
