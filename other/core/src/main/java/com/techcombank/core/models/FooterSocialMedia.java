package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This model class is for Social Media Icons.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterSocialMedia {
    /**
     * Social Media Icon.
     */
    @ValueMapValue
    @Getter
    private String socialImage;
    /**
     * Social Media text.
     */
    @ValueMapValue
    @Getter
    private String socialText;
    /**
     * Social Media URL.
     */
    @ValueMapValue
    @Getter
    private String socialUrl;
    /**
     * To open social media page in new tab
     */
    @ValueMapValue
    @Getter
    private String openInNewTab;
    @Getter
    private String externalLink;

    /**
     * Returns the mobile image rendition path for the Social Image.
     */
    public String getSocialMobileImagePath() {
        return PlatformUtils.getMobileImagePath(socialImage);
    }

    /**
     * Returns the web image rendition path for the Social Image.
     */
    public String getSocialWebImagePath() {
        return PlatformUtils.getWebImagePath(socialImage);
    }

    public String getExternalLink() {
        return PlatformUtils.getUrlLinkType(socialUrl);
    }

}
