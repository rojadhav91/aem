package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * This model class is for Icon images Properties of Accordion
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinkTextItemModel {
    /**
     * The icon image.
     */
    @ValueMapValue
    @Getter
    private String iconImage;

    /**
    * Returns the mobile image rendition path for the icon image.
    */
    public String getMobileIconImagePath() {
        return PlatformUtils.getMobileImagePath(iconImage);
    }

    /**
    * Returns the web image rendition path for the icon image.
    */
    public String getWebIconImagePath() {
        return PlatformUtils.getWebImagePath(iconImage);
    }

    /**
     * The icon image alt text.
     */
    @ValueMapValue
    @Getter
    private String iconImageAltText;

    /**
     * The title text.
     */
    @ValueMapValue
    @Getter
    private String titleText;

    /**
     * The card description.
     */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * The link url.
     */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * The icon image alt text.
     */
    @ValueMapValue
    @Getter
    private String linkUrlType;

    /**
     * The open in new tab option.
     */
    @ValueMapValue
    @Getter
    private Boolean openInNewTab;

    /**
     * The no follow option.
     */
    @ValueMapValue
    @Getter
    private Boolean noFollow;

    /**
     * The row of the item in the grid model
     */
    @ValueMapValue
    @Getter
    @Setter
    private int row;

    /**
     * The column of the item in the grid model
     */
    @ValueMapValue
    @Getter
    @Setter
    private int column;
}
