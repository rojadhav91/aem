package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListViewDocumentTabItemModel {
    /**
     * Tab title
     */
    @ValueMapValue
    @Getter
    private String tabTitle;

    /**
     * List of level one items
     */
    @ChildResource
    @Getter
    private List<ListViewDocumentLevelOneItemModel> listViewDocumentItemsFirstLevel;

}
