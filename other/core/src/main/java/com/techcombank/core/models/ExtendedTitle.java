package com.techcombank.core.models;

import com.adobe.cq.wcm.core.components.models.Title;
import com.day.cq.commons.jcr.JcrConstants;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Named;

/**
 * Sling model that represents a title component.
 */
@Model(adaptables = Resource.class, adapters = Title.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExtendedTitle implements Title {

    /**
     * The unique identifier of the title component.
     */
    @ValueMapValue
    @Getter
    private String id;

    /**
     * The text of the title component.
     */
    @ValueMapValue
    @Getter
    @Named(value = JcrConstants.JCR_TITLE)
    private String text;

    /**
     * The size of the title component.
     */
    @ValueMapValue
    @Getter
    private String type;

    /**
     * The alignment of the title component.
     */
    @ValueMapValue
    @Getter
    private String alignment;

    /**
     * The separator style of the title component.
     */
    @ValueMapValue
    @Getter
    private String separatorStyle;

    /**
     * The font color style of the title component.
     */
    @ValueMapValue
    @Getter
    private String fontColorStyle;

}
