package com.techcombank.core.models.impl;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.models.QuickAccessFileModel;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Quick Access File section details
 */
@Model(adaptables = Resource.class, adapters = {
        QuickAccessFileModel.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class QuickAccessFileModelImpl implements QuickAccessFileModel {

    /**
     * The Text for the File Name.
     */
    @ValueMapValue
    @Getter
    private String fileName;

    /**
     * The Path for the file.
     */
    @ValueMapValue
    @Getter
    private String filePath;

    /**
     * The path for the file icon.
     */
    @ValueMapValue
    @Getter
    private String fileIcon;

    /**
     * The Alt text for file icon.
     */
    @ValueMapValue
    @Getter
    private String fileIconAlt;

    /**
     * Returns the web image rendition path for the file icon.
     */
    @Override
    public String getFileIconWebImagePath() {
        return PlatformUtils.getWebImagePath(fileIcon);
    }

    /**
     * Returns the mobile image rendition path for the file Icon.
     */
    @Override
    public String getFileIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(fileIcon);
    }

    /**
     * @return the fileType
     */
    @Override
    public String getFileType() {
        return PlatformUtils.getUrlLinkType(filePath);
    }
}
