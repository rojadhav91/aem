package com.techcombank.core.models;

import lombok.Getter;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Gold Rate Content Fragment property to
 * Java Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = GoldRate.class)
public class GoldRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * Label
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * Ask Rate
     */
    @ValueMapValue
    @Getter
    private String askRate;

    /**
     * Bid Rate
     */
    @ValueMapValue
    @Getter
    private String bidRate;
}
