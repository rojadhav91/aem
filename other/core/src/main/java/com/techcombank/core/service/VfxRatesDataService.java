package com.techcombank.core.service;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.google.gson.JsonObject;
import com.techcombank.core.models.ExchangeRate;

public interface VfxRatesDataService {
    Resource getVfxRate(ResourceResolver resolver, String vfxRateDateTimePath, String vfxRateBasePath);

    JsonObject getVfxRateData(Resource vfxRate, ResourceResolver resolver, List<String> vfxRateTimeStamp,
            String vfxRateType);

    String getVfxRateUpdateDate(ResourceResolver resolver, String rateBasePath);

    List<String> getVfxRateBasePath(String vfxRateType, boolean addAllVfxRates);

    List<ExchangeRate> getExchangeRateDataList(Resource exchangeRateLatest, ResourceResolver resolver);

    List<String> getVfxRateTimeStamp(ResourceResolver resolver, String vfxRateDatePath, String vfxRateBasePath);

    boolean createVfxRateData(ResourceResolver resolver, JsonObject vfxData);

}
