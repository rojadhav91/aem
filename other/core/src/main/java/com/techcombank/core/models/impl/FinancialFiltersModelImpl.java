package com.techcombank.core.models.impl;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.FinancialFiltersModel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This model class is for FinancialFiltersModelImpl Model
 */
@Model(adaptables = Resource.class, adapters = {
        FinancialFiltersModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        FinancialFiltersModelImpl.RESOURCE_TYPE)
@Getter
public class FinancialFiltersModelImpl implements FinancialFiltersModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/financialfilters";

    /**
     * The ResourceResolver.
     */
    @Inject
    private static ResourceResolver resolver;
    /**
     * The heading value.
     */
    @ValueMapValue
    private String heading;

    /**
     * The yearLabel value.
     */
    @ValueMapValue
    private String yearLabel;

    /**
     * The quarterLabel value.
     */
    @ValueMapValue
    private String quarterLabel;
    /**
     * The applyFiltersLabel value.
     */
    @ValueMapValue
    private String applyFiltersLabel;
    /**
     * The filtersLabel value.
     */
    @ValueMapValue
    private String filtersLabel;
    /**
     * The selectLabel value.
     */
    @ValueMapValue
    private String selectLabel;
    /**
     * The highlights value.
     */
    @ChildResource
    private List<Highlights> highlights;
    /**
     * The quarters value.
     */
    private List<Resource> quarters;

    @PostConstruct
    void init() {
        if (Objects.nonNull(resolver)) {
            quarters = new ArrayList<>();
            Resource tagsRoot = resolver.getResource(TCBConstants.QUARTERS_TAGS);
            if (Objects.nonNull(tagsRoot) && Objects.requireNonNull(tagsRoot).hasChildren()) {
                for (Resource res : tagsRoot.getChildren()) {
                    quarters.add(res);
                }
            }
        }
    }

    @Getter
    @Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    public static class Highlights {
        /**
         * The path value.
         */
        @ValueMapValue
        private String path;

        /**
         * The year value.
         */
        private String year;
        /**
         * The year value.
         */
        public String getYear() {
            return Objects.requireNonNull(Objects.requireNonNull(resolver.getResource(path)).getParent()).getName();
        }
    }

}
