package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * The Class BreadcrumbModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = BreadcrumbModel.class)
public class BreadcrumbModel {

    @ValueMapValue
    @Getter
    private boolean hideBreadcrumb;

}
