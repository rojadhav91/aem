package com.techcombank.core.models;

import com.google.gson.Gson;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import java.io.Serializable;

/**
 * The Class SurveyPanelModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = SurveyPanelModel.class)
public class SurveyPanelModel implements Serializable {

    @OSGiService
    private transient ResourceResolverService resourceResolverService;

    private transient ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
        this.mobileEverydayCardImage = PlatformUtils.getMobileImagePath(everydayCardImage);
        this.webEverydayCardImage = PlatformUtils.getWebImagePath(everydayCardImage);
        this.mobileStyleCardImage = PlatformUtils.getMobileImagePath(styleCardImage);
        this.webStyleCardImage = PlatformUtils.getWebImagePath(styleCardImage);
        this.mobileSparkCardImage = PlatformUtils.getMobileImagePath(sparkCardImage);
        this.webSparkCardImage = PlatformUtils.getWebImagePath(sparkCardImage);
        this.mobileSignatureCardImage = PlatformUtils.getMobileImagePath(signatureCardImage);
        this.webSignatureCardImage = PlatformUtils.getWebImagePath(signatureCardImage);
        this.mobileVietnamAirlineCardImage = PlatformUtils.getMobileImagePath(vietnamAirlineCardImage);
        this.webVietnamAirlineCardImage = PlatformUtils.getWebImagePath(vietnamAirlineCardImage);
    }

    /**
     * Everyday Card Image
     * - Description: To author card image
    */
    @ValueMapValue
    @Getter
    private String everydayCardImage;

    @ValueMapValue
    @Getter
    private String mobileEverydayCardImage;

    @ValueMapValue
    @Getter
    private String webEverydayCardImage;

    /**
     * Everyday Card Image Alt Text
     * - Description: To author alt text for credit card image
    */
    @ValueMapValue
    @Getter
    private String everydayCardImageAltText;

    /**
     * Everyday Card URL
     * - Description: To author URL link for 'Learn more' CTA
    */
    @ValueMapValue
    @Getter
    private String everydayCardUrl;

    /**
     * Style Card Image
     * - Description: To author card image
    */
    @ValueMapValue
    @Getter
    private String styleCardImage;

    @ValueMapValue
    @Getter
    private String mobileStyleCardImage;

    @ValueMapValue
    @Getter
    private String webStyleCardImage;

    /**
     * Style Card Image Alt Text
     * - Description: To author alt text for credit card image
    */
    @ValueMapValue
    @Getter
    private String styleCardImageAltText;

    /**
     * Style Card URL
     * - Description: To author URL link for 'Learn more' CTA
    */
    @ValueMapValue
    @Getter
    private String styleCardUrl;

    /**
     * Spark Card Image
     * - Description: To author card image
    */
    @ValueMapValue
    @Getter
    private String sparkCardImage;

    @ValueMapValue
    @Getter
    private String mobileSparkCardImage;

    @ValueMapValue
    @Getter
    private String webSparkCardImage;

    /**
     * Spark Card Image Alt Text
     * - Description: To author alt text for credit card image
    */
    @ValueMapValue
    @Getter
    private String sparkCardImageAltText;

    /**
     * Spark Card URL
     * - Description: To author URL link for 'Learn more' CTA
    */
    @ValueMapValue
    @Getter
    private String sparkCardUrl;

    /**
     * Signature Card Image
     * - Description: To author card image
    */
    @ValueMapValue
    @Getter
    private String signatureCardImage;

    @ValueMapValue
    @Getter
    private String mobileSignatureCardImage;

    @ValueMapValue
    @Getter
    private String webSignatureCardImage;

    /**
     * Signature Card Image Alt Text
     * - Description: To author alt text for credit card image
    */
    @ValueMapValue
    @Getter
    private String signatureCardImageAltText;

    /**
     * Signature Card URL
     * - Description: To author URL link for 'Learn more' CTA
    */
    @ValueMapValue
    @Getter
    private String signatureCardUrl;

    /**
     * Vietnam Airline Card Image
     * - Description: To author card image
    */
    @ValueMapValue
    @Getter
    private String vietnamAirlineCardImage;

    @ValueMapValue
    @Getter
    private String mobileVietnamAirlineCardImage;

    @ValueMapValue
    @Getter
    private String webVietnamAirlineCardImage;
    /**
     * Vietnam Airline Card Image Alt Text
     * - Description: To author alt text for credit card image
    */
    @ValueMapValue
    @Getter
    private String vietnamAirlineCardImageAltText;

    /**
     * Vietnam Airline URL
     * - Description: To author URL link for 'Learn more' CTA
    */
    @ValueMapValue
    @Getter
    private String vietnamAirlineUrl;

    /**
     * Q1 Question Count Text
     * - Description: Question count text
    */
    @ValueMapValue
    @Getter
    private String q1QuestionCountText;

    /**
     * Question 1
     * - Description: Question for survey
    */
    @ValueMapValue
    @Getter
    private String question1;

    /**
     * Answer Option 1
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q1AnswerOption1;

    /**
     * Answer Option 2
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q1AnswerOption2;

    /**
     * Answer Option 3
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q1AnswerOption3;

    /**
     * Answer Option 4
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q1AnswerOption4;

    /**
     * Q1 Progress Bar Description Text
     * - Description: Appears as “% Completed” behind progress value
    */
    @ValueMapValue
    @Getter
    private String q1ProgressBarDescriptionText;

    /**
     * Q2 Question Count Text
     * - Description: Question count text
    */
    @ValueMapValue
    @Getter
    private String q2QuestionCountText;

    /**
     * Question 2
     * - Description: Question for survey
    */
    @ValueMapValue
    @Getter
    private String question2;

    /**
     * Answer Option 1
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q2AnswerOption1;

    /**
     * Answer Option 2
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q2AnswerOption2;

    /**
     * Answer Option 3
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q2AnswerOption3;

    /**
     * Answer Option 4
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q2AnswerOption4;

    /**
     * Answer Option 5
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q2AnswerOption5;

    /**
     * Q2 Progress Bar Description Text
     * - Description: Appears as “% Completed” behind progress value
    */
    @ValueMapValue
    @Getter
    private String q2ProgressBarDescriptionText;

    /**
     * Q3 Question Count Text
     * - Description: Question count text
    */
    @ValueMapValue
    @Getter
    private String q3QuestionCountText;

    /**
     * Question 3
     * - Description: Question for survey
    */
    @ValueMapValue
    @Getter
    private String question3;

    /**
     * Answer Option 1
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q3AnswerOption1;

    /**
     * Answer Option 2
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q3AnswerOption2;

    /**
     * Answer Option 3
     * - Description: Text for answer
    */
    @ValueMapValue
    @Getter
    private String q3AnswerOption3;

    /**
     * Q3 Progress Bar Description Text
     * - Description: Appears as “% Completed” behind progress value
    */
    @ValueMapValue
    @Getter
    private String q3ProgressBarDescriptionText;

    /**
     * 'Next' CTA
     * - Description: Button text
    */
    @ValueMapValue
    @Getter
    private String nextCta;

    /**
     * 'Try Again' CTA
     * - Description: Button text for retry
    */
    @ValueMapValue
    @Getter
    private String tryAgainCta;

    /**
     * 'Learn More' CTA
     * - Description: Button text to credit card detail text
    */
    @ValueMapValue
    @Getter
    private String learnMoreCta;

    /**
     * Open In New Tab
     * - Description: To open link URL In new tab or same tab
    */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No Follow
     * - Description: To select follow/no-follow for link text
    */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Sets the interaction type for card urls
     */
    @ValueMapValue
    @Getter
    @Setter
    private String everydayCardWebInteractionType;

    @ValueMapValue
    @Getter
    @Setter
    private String styleCardWebInteractionType;

    @ValueMapValue
    @Getter
    @Setter
    private String sparkCardWebInteractionType;

    @ValueMapValue
    @Getter
    @Setter
    private String signatureCardWebInteractionType;

    @ValueMapValue
    @Getter
    @Setter
    private String vietnamAirlineCardWebInteractionType;

    public ResourceResolver getResourceResolver() {
        return resourceResolver;
    }

    public boolean setWebInteractionType() {
        try {
            this.setEverydayCardWebInteractionType(PlatformUtils.getUrlLinkType(everydayCardUrl));
            this.setStyleCardWebInteractionType(PlatformUtils.getUrlLinkType(styleCardUrl));
            this.setSparkCardWebInteractionType(PlatformUtils.getUrlLinkType(sparkCardUrl));
            this.setSignatureCardWebInteractionType(PlatformUtils.getUrlLinkType(signatureCardUrl));
            this.setVietnamAirlineCardWebInteractionType(PlatformUtils.getUrlLinkType(vietnamAirlineUrl));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String getJson() {
        return new Gson().toJson(this);
    }
}
