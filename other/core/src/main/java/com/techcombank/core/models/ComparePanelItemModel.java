package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;
import lombok.Setter;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ComparePanelItemModel.class)
public class ComparePanelItemModel {


    @ValueMapValue
    @Getter
    @Setter
    private String projectName;

    @ValueMapValue
    @Getter
    @Setter
    private String loanRateOnCapitalNeed;

    @ValueMapValue
    @Getter
    @Setter
    private String loanRateOnCollateralValue;

    @ValueMapValue
    @Getter
    @Setter
    private String loanTerm;

    @ValueMapValue
    @Getter
    @Setter
    private String collateral;

    @ValueMapValue
    @Getter
    @Setter
    private String tag;
}
