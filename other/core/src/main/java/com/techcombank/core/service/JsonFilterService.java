package com.techcombank.core.service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.Map;

public interface JsonFilterService {
    String[] getJsonFilter();

    String getAuthorizationToken();

    JsonElement processJSON(JsonElement element, String[] keysRemove, String imageDomain, String domain);

    Map<String, Object> getHTTPResponse(JsonObject responseJson, String pagePath)
            throws IOException;
}
