package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CCashScoreBoardModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CCashScoreBoardModel.class)
public class CCashScoreBoardModel {

    /**
     * Title
     * - Description: To author score board title
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Criteria Title
     * - Description: To author title for criteria column of score board
    */
    @ValueMapValue
    @Getter
    private String criteriaTitle;

    /**
     * Average Score Title
     * - Description: To author title for average score column of score board
    */
    @ValueMapValue
    @Getter
    private String averageScoreTitle;

    /**
     * Total Score Title
     * - Description: To author title for total score row of score board
    */
    @ValueMapValue
    @Getter
    private String totalScoreTitle;

    /**
     * Caption
     * - Description: To author caption of score board
    */
    @ValueMapValue
    @Getter
    private String caption;

}
