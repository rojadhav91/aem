package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * The Class TabsModel.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = TabsModel.class)
public class TabsModel {
    private String languageTitle = "";

    @Inject
    private Page currentPage;

    @Inject
    private ResourceResolver resourceResolver;

    /** The tabs label alignment. */
    @ValueMapValue
    @Getter
    private String tabLabelAlignment;

    @PostConstruct
    public void init() {
        try {
            languageTitle = PlatformUtils.getLanguageTitle(currentPage.getContentResource(), false, resourceResolver);
        } catch (RuntimeException e) {
            languageTitle = "";
        }
    }

    public String getLanguageTitle() {
        return languageTitle;
    }
}
