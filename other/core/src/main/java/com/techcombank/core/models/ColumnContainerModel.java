package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * Sling model for a column container.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ColumnContainerModel {

    /**
     * The width of the column container.
     */
    @ValueMapValue
    @Getter
    @Default(values = "5050")
    private String width;

    /**
     * The left padding of the column container.
     */
    @ValueMapValue
    @Getter
    private String leftPadding;

    /**
     * The right padding of the column container.
     */
    @ValueMapValue
    @Getter
    private String rightPadding;

    /**
     * The top padding of the column container.
     */
    @ValueMapValue
    @Getter
    private String topPadding;

    /**
     * The bottom padding of the column container.
     */
    @ValueMapValue
    @Getter
    private String bottomPadding;

    /**
     * The Activate Vertical Divider.
     */
    @ValueMapValue
    @Getter
    private String activateVerticalDivider;

    /**
     * The flip view.
     */
    @ValueMapValue
    @Getter
    private String flipView;
}
