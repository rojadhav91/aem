package com.techcombank.core.service;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.pojo.HrefLangPojo;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.List;

public interface SeoHref {
    List<HrefLangPojo> getPageList(ResourceResolver resolver, Page currentPagePath);
}

