package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class ImageCardSlideModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ImageCardSlideModel.class)
public class ImageCardSlideModel {

    @ValueMapValue
    @Getter
    private String cardImage;

    /**
    * Returns the mobile image rendition path for the card image.
    */
    public String getMobileCardImagePath() {
        return PlatformUtils.getMobileImagePath(cardImage);
    }

    /**
    * Returns the web image rendition path for the card image.
    */
    public String getWebCardImagePath() {
        return PlatformUtils.getWebImagePath(cardImage);
    }

    @ValueMapValue
    @Getter
    private String cardImageAltText;

    @ValueMapValue
    @Getter
    private String cardTitle;

    @ValueMapValue
    @Getter
    private String cardDescription;

    @ValueMapValue
    @Getter
    private String cardBackgroundColour;

    @ValueMapValue
    @Getter
    private String textAlignmentOnCard;

}
