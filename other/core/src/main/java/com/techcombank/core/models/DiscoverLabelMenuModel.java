package com.techcombank.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Mapping Discover Menu Properties to Primary
 * Navigation.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DiscoverLabelMenuModel {

    /**
     * The label text for the Discover Menu Item.
     */
    @ValueMapValue
    @Getter
    private String discoverMenuLabel;

    /**
     * The URL of the Discover Menu page.
     */
    @ValueMapValue
    private String discoverMenuPageURL;

    /**
     * The Target for Discover Page
     */
    @ValueMapValue
    @Getter
    private String discoverTarget;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the discoverMenuPageURL
     */
    public String getDiscoverMenuPageURL() {
        return PlatformUtils.isInternalLink(resourceResolver, discoverMenuPageURL);
    }

    /**
     * Returns the discover Menu url interaction type for analytics.
     */
    public String getDiscoverMenuPageURLInteractionType() {
        return PlatformUtils.getUrlLinkType(discoverMenuPageURL);
    }

}
