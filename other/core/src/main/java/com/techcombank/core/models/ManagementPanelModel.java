package com.techcombank.core.models;

import com.techcombank.core.pojo.ManagementPanel;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;


import javax.inject.Inject;
import java.util.List;

/*
 *A Sling model that represents a management panel.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ManagementPanelModel {

    /*
     *The background image of the panel.
     */
    @Getter
    @ValueMapValue
    private String panelBgImage;


    /*
     *The background image of the panel.
     */
    @Getter
    @ValueMapValue
    private String panelBgMobile;


    /*
     *Read more text for card.
     */
    @Getter
    @ValueMapValue
    private String cardText;

    /*
     *View more text for managment panel.
     */
    @Getter
    @ValueMapValue
    private String viewMore;

    /*
     *Multifield for management panel component.
     */
    @Getter
    @Inject
    private List<ManagementPanel> profileItem;

    @Getter
    private String panelImageMobile;

    public String getPanelBgImage() {
        return PlatformUtils.getWebImagePath(panelBgImage);
    }

    public String getPanelImageMobile() {
        return PlatformUtils.getMobileImagePath(panelBgImage);
    }
}
