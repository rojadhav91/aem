package com.techcombank.core.models.impl;

import com.techcombank.core.models.TermsAndConditionsModel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This model class is for TermsAndConditionsModel
 */
@Model(adaptables = Resource.class, adapters = {TermsAndConditionsModel.class}, defaultInjectionStrategy =
        DefaultInjectionStrategy.OPTIONAL, resourceType = TermsAndConditionsModelImpl.RESOURCE_TYPE)
@Getter
public class TermsAndConditionsModelImpl implements TermsAndConditionsModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/form/termsconditions";

    /**
     * The fieldLabel value.
     */
    @ValueMapValue
    private String fieldTitle;

    /**
     * The fieldName value.
     */
    @ValueMapValue
    private String fieldName;

    /**
     * The fieldValue value.
     */
    @ValueMapValue
    private String fieldValue;
    /**
     * The fieldId value.
     */
    @ValueMapValue
    private String fieldId;

    /**
     * The requiredMessage value.
     */
    @ValueMapValue
    private String requiredMessage;

    /**
     * The isRequired value.
     */
    @ValueMapValue
    private boolean required;

    /**
     * The analytics value.
     */
    @ValueMapValue
    private boolean analytics;
}
