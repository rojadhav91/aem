package com.techcombank.core.servlets;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.FormContainerService;
import lombok.NonNull;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component(service = {Servlet.class})
@SlingServletResourceTypes(resourceTypes = "sling/servlet/default",
        methods = HttpConstants.METHOD_POST, selectors = "forms", extensions = "html")
public class FormContainerServlet extends SlingAllMethodsServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(FormContainerServlet.class);
    private static final long serialVersionUID = 1L;
    private static final String FORM_TYPE = "formtype";
    private static final String LEADS = "Leads";
    private static final String FORM_ERROR_MESSAGE = "form_empty_or_non_lead";
    @Reference
    private transient FormContainerService formContainerService;


    @Override
    protected void doPost(final @NonNull SlingHttpServletRequest request,
                          final SlingHttpServletResponse response) {
        try {
        response.setContentType(TCBConstants.APPLICATION_JSON);
        Map<String, String> requestParamMap = getRequestParametersMap(request);
        if (!requestParamMap.isEmpty() && requestParamMap.containsKey(FORM_TYPE)
                && LEADS.equalsIgnoreCase(requestParamMap.get(FORM_TYPE))) {
            JsonObject result = formContainerService.formValues(false, requestParamMap);
            if (Objects.nonNull(result) && result.has(TCBConstants.FAILURE)) {
                response.setStatus(TCBConstants.STATUS_ERROR);
            }
            response.getWriter().print(result);
            LOGGER.info("Form Response message {} : ", result);
        } else {
            response.setStatus(TCBConstants.STATUS_BAD_REQUEST);
            response.getWriter().print(FORM_ERROR_MESSAGE);
            LOGGER.info("Form Parameters are empty {} : ", requestParamMap);
        }
        } catch (IOException e) {
            response.setStatus(TCBConstants.STATUS_ERROR);
            LOGGER.error("[CRM FORM] Submission failed {} : ", e.getMessage());
        }
    }

    /**
     * getRequestParametersMap will add
     * the form field values into
     * Map
     */
    private Map<String, String> getRequestParametersMap(final SlingHttpServletRequest request) {
        Map<String, String> requestParam = new HashMap<>();
        List<RequestParameter> requestParameterList = request.getRequestParameterList();
        for (RequestParameter requestParameter : requestParameterList) {
            String reqParamName = requestParameter.getName();
            String reqParamValue = request.getParameter(reqParamName);
            if (!StringUtils.isBlank(reqParamName) && !StringUtils.isBlank(reqParamValue)) {
                requestParam.put(reqParamName, reqParamValue);
            }
        }
        return requestParam;
    }
}
