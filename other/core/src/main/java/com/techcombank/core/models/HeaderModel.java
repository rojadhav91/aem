package com.techcombank.core.models;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import lombok.Getter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class HeaderModel used for mapping Properties of Header Component.
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = HeaderModel.RESOURCE_TYPE)
public class HeaderModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/header";

    @Inject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource currentResource;

    @Inject
    private Page currentPage;

    /**
     * The List for Landing Page Menu Items.
     */
    @ChildResource
    @Getter
    private List<LandingPageMenuModel> landingPageMenu;

    /**
     * The List for Discover Menu Items.
     */
    @ChildResource
    @Getter
    private List<DiscoverLabelMenuModel> discoverMenu;

    /**
     * The List for Right Navigation Menu Items.
     */
    @ChildResource
    @Getter
    private List<NavigationMenuModel> rightNavigationMenu;

    /**
     * The Text for Default dropdown Menu Label.
     */
    @ValueMapValue
    @Getter
    private String defaultMenuLabel;

    /**
     * The Text for Discover Label.
     */
    @ValueMapValue
    @Getter
    private String discoverLabel;

    /**
     * The URL for ALl Product Link.
     */
    @ValueMapValue
    @Getter
    private String allProductsLink;

    /**
     * The Text for All Product Link.
     */
    @ValueMapValue
    @Getter
    private String allProductsURL;

    /**
     * Flag to check is a Home Page.
     */
    @ValueMapValue
    @Getter
    private boolean enableHomePage;

    /* Secondary Nav Starts */

    /**
     * The URL of the logo image.
     */
    @ValueMapValue
    @Getter
    private String logoImage;

    /**
     * The URL of the logo mobile image.
     */
    @ValueMapValue
    @Getter
    private String logoMobileImage;

    /**
     * The link to the logo image.
     */
    @ValueMapValue
    private String logoImageLink;

    /**
     * The alternative text for the logo image.
     */
    @ValueMapValue
    @Getter
    private String logoImageAltText;

    /**
     * The colour of the login button.
     */

    @ValueMapValue
    @Getter
    private String loginButtonColour;

    /**
     * The text of the login label.
     */

    @ValueMapValue
    @Getter
    private String loginLabelText;

    /**
     * The URL of the login label link.
     */

    @ValueMapValue
    private String loginLabelLinkUrl;

    /**
     * Whether the login label link should open in a new tab.
     */

    @ValueMapValue
    @Getter
    private String openinnewtab;

    /**
     * A list of the secondary menu items at level 1.
     */
    @ChildResource
    @Getter
    private List<SecondaryMenuNavLevel1> secondaryMenuNavLevel1;

    /**
     * A list of the Login items at for home page.
     */
    @ChildResource
    @Getter
    private List<DropDownLoginModel> loginData;

    /* secondary End */

    /**
     * @return the allProductsURL
     */
    public String getAllProductsURL() {
        return PlatformUtils.isInternalLink(resourceResolver, allProductsURL);
    }

    /**
     * turn on/off Search feature on navigation
     */
    @ValueMapValue
    @Getter
    private boolean turnOnSearchOnNavigation;

    /**
     * @return the languageDetails
     */
    public List<LanguageLabelModel> getLanguageDetails() {
        List<LanguageLabelModel> languageDetails;
        languageDetails = PlatformUtils.getLanguageDetails(resourceResolver, currentPage, false);
        return languageDetails;
    }

    /**
     * @return the allProductEnabled
     */
    public boolean isAllProductEnabled() {
        return PlatformUtils.checkAllProductLink(currentPage);
    }

    /**
     * @return the loginLabelLinkUrl
     */
    public String getLoginLabelLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, loginLabelLinkUrl);
    }

    /**
     * @return the currentActivePage
     */
    public String getCurrentActivePage() {
        return currentPage.getPath().concat(TCBConstants.HTMLEXTENSION);
    }

    /**
     * @return the activePageMenuListed based on page path
     */
    public boolean isActivePageMenuListed() {
        boolean isPageMenuListed = false;
        String currentPrimaryPage = getPrimarySecondaryPagePath(TCBConstants.SEVEN);
        for (LandingPageMenuModel pageMenuItem : landingPageMenu) {
            if (pageMenuItem.getLandingPageURL().equalsIgnoreCase(currentPrimaryPage)) {
                isPageMenuListed = true;
                break;
            }
        }

        return isPageMenuListed;
    }

    /**
     * @return the activePrimaryMenuPagePath
     */
    public String getActivePrimaryMenuPagePath() {
        return getPrimarySecondaryPagePath(TCBConstants.SEVEN);
    }

    /**
     * @return the activeSecondaryMenuPagePath
     */
    public String getActiveSecondaryMenuPagePath() {
        return getPrimarySecondaryPagePath(TCBConstants.EIGHT);
    }

    /**
     * This method returns primary level and secondary level path
     *
     * @param pageIndex
     * @return pagePath
     */
    public String getPrimarySecondaryPagePath(final int pageIndex) {
        String pagePath = getCurrentAliasActivePage();
        // EX Path:/content/techcombank/web/vn/en/personal/borrow/real-home-loan
        String[] currentPageSubPaths = pagePath.split(TCBConstants.SLASH);
        if (currentPageSubPaths.length > pageIndex) {
            // EX:/content/techcombank/web/vn/en/personal/borrow
            pagePath = Arrays.stream(currentPageSubPaths).limit(pageIndex)
                    .collect(Collectors.joining(TCBConstants.SLASH)) + TCBConstants.HTMLEXTENSION;
        }
        pagePath = PlatformUtils.getMapUrl(resourceResolver, pagePath);
        return pagePath;
    }

    /**
     * @return the logoImageLink
     */
    public String getLogoImageLink() {
        return PlatformUtils.isInternalLink(resourceResolver, logoImageLink);
    }

    /**
     * @return the currentActiveAliasPage
     */
    public String getCurrentAliasActivePage() {
        return PlatformUtils.currentPagePathWithAlias(resourceResolver, currentPage.getPath());
    }

    /**
     * Returns the mobile image rendition path for the logoMobileImage.
     */
    public String getLogoMobileImagePath() {
        return PlatformUtils.getMobileImagePath(logoMobileImage);
    }

    /**
     * Returns the web image rendition path for the logoImage.
     */
    public String getLogoWebImagePath() {
        return PlatformUtils.getWebImagePath(logoImage);
    }

    /**
     * Returns the logo image link interaction type for analytics.
     */
    public String getLogoImageLinkInteractionType() {
        return PlatformUtils.getUrlLinkType(logoImageLink);
    }

    /**
     * Returns the all products link interaction type for analytics.
     */
    public String getAllProductsURLInteractionType() {
        return PlatformUtils.getUrlLinkType(allProductsURL);
    }

    /**
     * Returns the login label link url interaction type for analytics.
     */
    public String getLoginLabelLinkUrlInteractionType() {
        return PlatformUtils.getUrlLinkType(loginLabelLinkUrl);
    }

}
