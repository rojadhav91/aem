package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;

/**
 * This model class is for Footer Menu.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterMenu {
    /**
     * Footer Menu header Text.
     */
    @ValueMapValue
    @Getter
    private String footerMenuHeader;
    /**
     * Page Path to footer Menu.
     */
    @ValueMapValue
    private String footerMenuUrl;

    @Getter
    private String isFooterMenuUrlExternal;
    /**
     * Open page in new Tab.
     */
    @ValueMapValue
    @Getter
    private String openInNewTab;
    /**
     * Submenu List
     */
    @Inject
    @Getter
    private List<FooterSubMenu> subMenuList;

    @SlingObject
    private ResourceResolver resourceResolver;

    public String getFooterMenuUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, footerMenuUrl);
    }

    public String getIsFooterMenuUrlExternal() {
        return PlatformUtils.getUrlLinkType(footerMenuUrl);
    }

}
