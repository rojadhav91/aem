package com.techcombank.core.jobs;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.VfxRatesDataService;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = JobConsumer.class, immediate = true, property = {
        Constants.SERVICE_DESCRIPTION + "= VFX Data Integration Job Consumer",
        JobConsumer.PROPERTY_TOPICS + "=VFXDataIntegration/job" })
public class VfxDataJobConsumer implements JobConsumer {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Reference
    private VfxRatesDataService vfxRatesDataService;
    @Reference
    private ResourceResolverService resourceResolverService;

    @Override
    public JobResult process(final Job job) {
        log.info("VFX Integration JOB Started");
        ResourceResolver resourceResolver = resourceResolverService.getWriteResourceResolver();
        boolean isVFXDataCreated = false;
        Gson gsonObj = new Gson();
        String vfxRatesData = (String) job.getProperty(TCBConstants.VFX_RATE_JSON_DATA);
        JsonElement vfxRate = gsonObj.fromJson(vfxRatesData, JsonElement.class);
        JsonObject vfxRates = vfxRate.getAsJsonObject();
        JsonObject vfxData = vfxRates.get(TCBConstants.VFX_RATE_DATA).getAsJsonObject();
        String vfxId = vfxData.get(TCBConstants.VFX_RATE_ID).getAsString();
        isVFXDataCreated = vfxRatesDataService.createVfxRateData(resourceResolver, vfxData);
        log.info("VFX Integration Data Created:{}", isVFXDataCreated);
        if (isVFXDataCreated) {
            log.info("VFX Integration Job Successfully Completed:{}", vfxId);
            return JobResult.OK;
        } else {
            log.error("VFX Integration Job Failed:{}", vfxId);
            return JobResult.FAILED;
        }
    }

}
