package com.techcombank.core.service.impl;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.InsuranceGainService;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;


import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Objects;


@Component(service = {InsuranceGainService.class}, immediate = true)
@Designate(ocd = InsuranceGainServiceImpl.InsuranceGainConfig.class)
public class InsuranceGainServiceImpl implements InsuranceGainService {

    protected static final String AGE = "age: ";


    @ObjectClassDefinition(name = "Insurance gain Configuration", description = "Insurance gain Configurations")
    public @interface InsuranceGainConfig {
        @AttributeDefinition(name = "Insurance gain generic list", description = "Insurance gain generic list",
                type = AttributeType.STRING)
        String getInsuranceGainList() default "/etc/acs-commons/lists/insuranceGain";
    }

    private InsuranceGainServiceImpl.InsuranceGainConfig insuranceGainConfig;

    @Activate
    @Modified
    public void activate(final InsuranceGainServiceImpl.InsuranceGainConfig config) {
        this.insuranceGainConfig = config;
    }

    @Override
    public String getGenericList(final ResourceResolver resolver) {
        Map<String, List<String>>  insuranceGainMap = new HashMap<>();
        Resource resource = resolver.getResource(insuranceGainConfig.getInsuranceGainList());
        Iterator<Resource> resourceIterator = resource.listChildren();
        List<String> response = new ArrayList<>();
        if (resourceIterator != null) {
            while (resourceIterator.hasNext()) {
                Resource pages = resourceIterator.next();
                if (!pages.getName().equalsIgnoreCase(NameConstants.NN_CONTENT)) {
                    Resource pageResource = resolver.getResource(pages.getPath());
                    Page gainPage = pageResource.adaptTo(Page.class);
                    GenericList insuranceList = gainPage.adaptTo(GenericList.class);
                    getInsuranceGainMap(insuranceList, insuranceGainMap);
                }
            }
        }
        getInsuranceGainList(insuranceGainMap, response);

        return response.toString();
    }

    /**
     * This method populates the insurance gain list
     *
     * @param insuranceGainMap
     * @param response
     */
    private void getInsuranceGainList(final Map<String, List<String>> insuranceGainMap, final List<String> response) {
        for (Map.Entry<String, List<String>> set : insuranceGainMap.entrySet()) {
            StringBuilder insuraceGainObj = new StringBuilder("{ " + AGE + set.getKey());
            for (String insuranceGainItem : set.getValue()) {
                insuraceGainObj.append(TCBConstants.COMMA + insuranceGainItem);
            }
            insuraceGainObj.append(" }");
            response.add(insuraceGainObj.toString());
        }
    }

    /**
     * This method populates insurance gain map
     *
     * @param insuranceList
     * @param insuranceGainMap
     */
    private void getInsuranceGainMap(final GenericList insuranceList,
                                     final Map<String, List<String>> insuranceGainMap) {

        if (!Objects.isNull(insuranceList.getItems())) {
            for (GenericList.Item listItem : insuranceList.getItems()) {
                if (null != listItem.getValue()) {
                    List<String> titleList = new ArrayList<>();
                    if (insuranceGainMap.containsKey(listItem.getValue())) {
                        insuranceGainMap.get(listItem.getValue()).add(listItem.getTitle());
                    } else {
                        titleList.add(listItem.getTitle());
                        insuranceGainMap.put(listItem.getValue(), titleList);
                    }
                }
            }
        }
    }
}
