package com.techcombank.core.pojo;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Locale;
import java.util.List;

public class TabEventCard {
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATE_FORMAT_TIME = "hh:mm:ss";

    @Setter
    @Getter
    private String imageForEvent;

    @Getter
    private String eventMonth;

    @Getter
    private String eventDate;

    @Getter
    private String eventTime;

    @Setter
    @Getter
    private String eventHeading;

    @Setter
    @Getter
    private String eventSubHeading;

    @Setter
    @Getter
    private String eventDesription;

    @Setter
    @Getter
    private String awardLabelText;

    @Setter
    @Getter
    private String ctaButtonLabel;

    @Setter
    @Getter
    private String ctaButtonLink;

    @Getter
    private List<String> eventTag;
    @Setter
    @Getter
    private String ctaTarget;

    @Setter
    @Getter
    private String ctaNoFollow;

    @Getter
    private String linkUrl;

    private static final Logger LOG = LoggerFactory.getLogger(TabEventCard.class);

    public void setEventMonth(final String date) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String monthStr = StringUtils.EMPTY;
        try {
            Date month = inputDateFormat.parse(date);
            SimpleDateFormat monthDateFormat = new SimpleDateFormat("MMMM", Locale.ENGLISH);
            monthStr = monthDateFormat.format(month);
        } catch (ParseException e) {
            LOG.error("Parse Exceptionin event month {} ", e.getMessage());
        }
        this.eventMonth = monthStr;
    }

    public void setEventDate(final String date) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String dateStr = StringUtils.EMPTY;
        try {
            Date dateDate = inputDateFormat.parse(date);
            SimpleDateFormat dateDateFormat = new SimpleDateFormat("dd", Locale.ENGLISH);
            dateStr = dateDateFormat.format(dateDate);
        } catch (ParseException e) {
            LOG.error("Parse Exception in event date {} ", e.getMessage());
        }
        this.eventDate = dateStr;
    }

    public void setEventTime(final String date) {
        SimpleDateFormat inputDateFormat = new SimpleDateFormat(DATE_FORMAT_TIME);
        String timeStr = StringUtils.EMPTY;
        try {
            Date timeDate = inputDateFormat.parse(date);
            SimpleDateFormat timeDateFormat = new SimpleDateFormat("hh:mm a");
            timeStr = timeDateFormat.format(timeDate);
        } catch (ParseException e) {
            LOG.error("Parse Exception in event time {} ", e.getMessage());
        }
        this.eventTime = timeStr;
    }

    public void setEventTag(final String[] tags, final TagManager tagManager) {
        List<String> tagList = new ArrayList<>();
        for (String tagName : tags) {
            Tag tag = tagManager.resolve(tagName);
            if (Objects.nonNull(tag)) {
                tagList.add(tag.getTitle());
            }
        }
        this.eventTag = tagList;
    }

    public void setLinkUrl(final String url) {
        this.linkUrl = PlatformUtils.getUrlLinkType(url);
    }
}
