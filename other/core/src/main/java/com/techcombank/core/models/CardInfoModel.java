package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CardInfoModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CardInfoModel.class)
public class CardInfoModel {

    /**
     * Display Variation
    */
    @ValueMapValue
    @Getter
    private String displayVariation;

    /**
     * Theme
    */
    @ValueMapValue
    @Getter
    private String theme;

    /**
     * Card Info effects
     */
    @ValueMapValue
    @Getter
    private String cardInfoEffects;

    /**
     * View more button
     */
    @ValueMapValue
    @Getter
    private Boolean viewMoreButton;

    /**
     * View more label
     */
    @ValueMapValue
    @Getter
    private String viewMoreLabel;

    /**
     * View less label
     */
    @ValueMapValue
    @Getter
    private String viewLessLabel;

    /**
     * Cards
     */
    @ChildResource
    @Getter
    private List<CardInfoItemModel> cardInfoItemList;

}
