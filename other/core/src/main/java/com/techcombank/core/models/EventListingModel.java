package com.techcombank.core.models;

/**
 * Interface For EventListingModel
 */
public interface EventListingModel {

    /**
     * The getEventTypeLabel.
     */
    String getEventTypeLabel();

    /**
     * The getShowMeLabel.
     */
    String getShowMeLabel();

    /**
     * The getSelectDateLabel.
     */
    String getSelectDateLabel();

    /**
     * The getViewMoreLabel.
     */
    String getViewMoreLabel();

    /**
     * The getFilterLabel.
     */
    String getFilterLabel();

    /**
     * The getApplyFilterLabel.
     */
    String getApplyFilterLabel();

    /**
     * The getUpcomingEventsLabel.
     */
    String getUpcomingEventsLabel();

    /**
     * The getPastEventsLabel.
     */
    String getPastEventsLabel();

    /**
     * The eventsCFRootPath.
     */
    String getEventsCFRootPath();


}
