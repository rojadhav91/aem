package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.Getter;
import lombok.Setter;

/**
 * This model class is for Storing Language details
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LanguageLabelModel {

    /**
     * The label text for the Language.
     */
    @Getter
    @Setter
    private String language;

    /**
     * The URL for the Language page
     */
    @Getter
    @Setter
    private String languageUrl;

    /**
     * country.
     */
    @Getter
    @Setter
    private String country;

}
