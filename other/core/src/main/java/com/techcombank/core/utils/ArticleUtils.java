package com.techcombank.core.utils;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.crx.JcrConstants;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.Articles;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import javax.jcr.RepositoryException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public final class ArticleUtils {

    private ArticleUtils() {
    }

    public static List<Articles> getArticles(final SearchResult result, final String currentPagePath,
                                             final ResourceResolver resourceResolver, final String articleValue)
            throws RepositoryException {
        List<Articles> relatedArticlesList = new ArrayList<>();

        for (final Hit hit : result.getHits()) {
            String path = hit.getPath() + TCBConstants.SLASH + JcrConstants.JCR_CONTENT;
            if (!StringUtils.equalsIgnoreCase(currentPagePath + TCBConstants.SLASH + JcrConstants.JCR_CONTENT,
                    path)) {
                getArticleList(resourceResolver, path, hit, relatedArticlesList);
                if (articleValue.equalsIgnoreCase(TCBConstants.ARTICLE_RELATED_POST_VALUE)
                        && relatedArticlesList.size() == TCBConstants.THREE) {
                    break;
                }
            }
        }
        return relatedArticlesList;
    }

    /**
     * This method populate article list
     *
     * @param resourceResolver
     * @param path
     * @param hit
     * @param relatedArticlesList
     */
    private static void getArticleList(final ResourceResolver resourceResolver, final String path, final Hit hit,
                                       final List<Articles> relatedArticlesList) throws RepositoryException {
        String viewMoreUrl = PlatformUtils.getMapUrl(resourceResolver, hit.getPath());
        Resource contentResource = resourceResolver.getResource(path);
        if (null != contentResource) {
            Articles articles = new Articles();
            final ValueMap vm = contentResource.getValueMap();
            String image = vm.get(TCBConstants.ARTICLE_IMAGE_PATH, StringUtils.EMPTY);
            String altText;
            altText = vm.get(TCBConstants.ARTICLE_IMAGE_ALT_TEXT, StringUtils.EMPTY);
            GregorianCalendar gregorianCalendar;
            gregorianCalendar = (GregorianCalendar) vm.get(TCBConstants.ARTICLE_CREATION_DATE);
            Date articleCreationDate = gregorianCalendar.getTime();
            DateFormat df = new SimpleDateFormat(TCBConstants.DATE_VN_PATTERN);
            String formattedDate = df.format(articleCreationDate);
            if (StringUtils.isBlank(image)) {
                String articlePath = path + TCBConstants.BG_BANNER_IMAGE_PATH;
                contentResource = resourceResolver.getResource(articlePath);
                if (contentResource != null) {
                    ValueMap mastheadValues = contentResource.getValueMap();
                    image = mastheadValues.get(TCBConstants.BG_BANNER_IMAGE, StringUtils.EMPTY);
                    altText = mastheadValues.get(TCBConstants.BG_BANNER_IMAGE_ALT_TEXT, StringUtils.EMPTY);
                }
            }
            String desc = vm.get(TCBConstants.JCR_DESCRIPTION, StringUtils.EMPTY);
            String title = vm.get(TCBConstants.PAGE_TITLE, StringUtils.EMPTY);
            articles.setPath(viewMoreUrl);
            articles.setArticleCreationDate(formattedDate);
            articles.setWebImage(image);
            articles.setMobileImage(image);
            articles.setAltText(altText);
            articles.setTitle(title);
            articles.setDescription(desc);
            relatedArticlesList.add(articles);
        }
    }
}
