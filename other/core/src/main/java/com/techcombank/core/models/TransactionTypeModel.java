package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * This model class is for Transaction Type section details
 */
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TransactionTypeModel {

    /**
     * The Text for transaction type.
     */
    @ValueMapValue
    @Getter
    private String transactionType;

    @ValueMapValue
    @Getter
    private String transactionLabel;
}
