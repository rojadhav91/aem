package com.techcombank.core.service;

import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.wcm.api.Page;

import java.util.Map;

public interface OfferListingPromotions {

    Map<String, Object> generateOffers(ResourceResolver resourceResolver, Map<String, String> queryMap,
                                       Page currentPage);

    Map<String, String> generatePromotionsQuery(Map<String, String> requestQueryParam, String cfDataPath);
}
