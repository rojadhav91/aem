package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ColumnPanelListModel.class)
public class ColumnPanelListModel {

    /**
     * Icon Image
     */
    @ValueMapValue
    @Getter
    private String iconImage;

    /**
     * Icon Image Alt Text
     */
    @ValueMapValue
    @Getter
    private String iconImageAltText;

    /**
     * Category Title
     */
    @ValueMapValue
    @Getter
    private String categoryTitle;

    /**
     * List of Cards
     */
    @ChildResource
    @Getter
    private List<ColumnPanelCardListModel> cardItems;
}
