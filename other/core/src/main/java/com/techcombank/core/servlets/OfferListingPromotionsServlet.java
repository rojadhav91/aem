package com.techcombank.core.servlets;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.OfferListingPromotions;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component(service = {Servlet.class})
@SlingServletResourceTypes(resourceTypes = "techcombank/components/offerlisting",
        methods = HttpConstants.METHOD_GET, selectors = {"offerlisting"}, extensions = "json")

@ServiceDescription("Offer Listing Servlet")
public class OfferListingPromotionsServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Reference
    private transient OfferListingPromotions offerListing;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        ResourceResolver resolver = request.getResourceResolver();
        Page currentPage = Optional.ofNullable(request.getResourceResolver().adaptTo(PageManager.class))
                .map(pm -> pm.getContainingPage(request.getResource())).orElse(null);
        Map<String, String> requestQueryParam = getRequestParametersMap(request);
        String cfDataPath = PlatformUtils.getRootPath(request);
        Map<String, String> promotionsQueryMap = offerListing.generatePromotionsQuery(requestQueryParam, cfDataPath);
        Map<String, Object> promotionsCardsJsons = offerListing.generateOffers(resolver, promotionsQueryMap,
                currentPage);
        getJsonResponse(response, promotionsCardsJsons);
    }

    /**
     * This method set the final promotions card list json to the response object
     *
     * @param response
     * @param promotionsCardsJsons
     * @return response
     * @throws IOException
     */
    public SlingHttpServletResponse getJsonResponse(final SlingHttpServletResponse response,
                                                    final Map<String, Object> promotionsCardsJsons) throws IOException {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        String json = gson.toJson(promotionsCardsJsons);
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        return response;
    }

    /**
     * This Method construct the request param map which will be used for building promotions query
     *
     * @param request
     * @return requestParamMap
     */
    private Map<String, String> getRequestParametersMap(final SlingHttpServletRequest request) {
        Map<String, String> requestParamMap = new HashMap<>();
        List<RequestParameter> requestParameterList = request.getRequestParameterList();
        for (RequestParameter requestParameter : requestParameterList) {
            String reqParamName = requestParameter.getName();
            String reqParamValue = request.getParameter(reqParamName);
            if (!StringUtils.isBlank(reqParamName) && !StringUtils.isBlank(reqParamValue)) {
                reqParamValue = reqParamValue.replace(" ", TCBConstants.HYPHEN);
                requestParamMap.put(reqParamName, reqParamValue);
            }
        }
        return requestParamMap;
    }
}
