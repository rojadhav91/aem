package com.techcombank.core.models.impl;

import javax.inject.Inject;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.ButtonModel;
import com.techcombank.core.utils.PlatformUtils;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class ButtonModelImpl used for mapping Properties of button Component.
 */
@Model(adaptables = Resource.class, adapters = {ButtonModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = ButtonModelImpl.RESOURCE_TYPE)
public class ButtonModelImpl implements ButtonModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/button";

    private static final String BUTTON_TITLE = "Button";

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * CTA type.
     */
    @ValueMapValue
    @Getter
    private String ctaType;

    /**
     * The Text for CTA ID
     */
    @ValueMapValue
    @Getter
    private String ctaID;

    /**
     * The Text for CTA Model ID.
     */
    @ValueMapValue
    @Getter
    private String ctaModelId;

    /**
     * The Text for CTA label.
     */
    @ValueMapValue
    @Getter
    private String ctaLabel;

    /**
     * To check datalayer enabled for contact us / register now cta.
     */
    @ValueMapValue
    @Getter
    private boolean contactRegisterCTA;

    /**
     * The URL for CTA.
     */
    @ValueMapValue
    private String ctaLink;

    /**
     * To check the button link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String ctaTarget;

    /**
     * To check button seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean ctaNoFollow;

    /**
     * To check CTA QR image is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean ctaQR;

    /**
     * The path for CTA icon.
     */
    @ValueMapValue
    @Getter
    private String ctaIcon;

    /**
     * The path for CTA QR image.
     */
    @ValueMapValue
    @Getter
    private String ctaQRImage;

    /**
     * The Alt Text for the CTA QR image.
     */
    @ValueMapValue
    @Getter
    private String ctaQRImageAltText;

    /**
     * CTA button color.
     */
    @ValueMapValue
    @Getter
    private String ctaColor;

    /**
     * CTA button font color.
     */
    @ValueMapValue
    @Getter
    private String fontColor;

    /**
     * Returns the interaction type for analytics.
     */
    @Override
    public String getInteractionNameType() {
        String ctaLinkType = PlatformUtils.getUrlLinkType(ctaLink);
        return PlatformUtils.getInteractionType(BUTTON_TITLE, ctaLinkType);
    }

    /**
     * Returns the button modal interaction type for analytics.
     */
    @Override
    public String getCtaModalInteractionNameType() {
        return PlatformUtils.getInteractionType(BUTTON_TITLE, TCBConstants.OTHER);
    }

    /**
     * Returns the web image rendition path for the CTA icon.
     */
    @Override
    public String getCtaIconWebImagePath() {
        return PlatformUtils.getWebImagePath(ctaIcon);
    }

    /**
     * Returns the mobile image rendition path for the CTA icon.
     */
    @Override
    public String getCtaIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(ctaIcon);
    }

    /**
     * Returns the web image rendition path for the QR image.
     */
    @Override
    public String getCtaQRWebImagePath() {
        return PlatformUtils.getWebImagePath(ctaQRImage);
    }

    /**
     * Returns the mobile image rendition path for the QR image.
     */
    @Override
    public String getCtaQRMobileImagePath() {
        return PlatformUtils.getMobileImagePath(ctaQRImage);
    }

    /**
     * @return the ctaLink
     */
    public String getCtaLink() {
        return PlatformUtils.isInternalLink(resourceResolver, ctaLink);
    }

}
