package com.techcombank.core.models;

/**
 * Interface For TermsAndConditionsModel
 */
public interface TermsAndConditionsModel {
    /**
     * The getFieldTitle.
     */
    String getFieldTitle();

    /**
     * The getFieldName.
     */
    String getFieldName();

    /**
     * The getFieldValue.
     */
    String getFieldValue();

    /**
     * The requiredMessage.
     */
    String getRequiredMessage();

    /**
     * The isRequired.
     */
    boolean isRequired();

    /**
     * The isAnalytics.
     */
    boolean isAnalytics();

    /**
     * The getFieldId.
     */
    String getFieldId();
}
