package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

/**
 * The Class CardBaseModel.
 */
public class CardBaseModel {

    /**
     * Image
     * - Description: Card image path from dam
    */
    @ValueMapValue
    @Getter
    private String image;

    /**
    * Returns the mobile image rendition path for the image.
    */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(image);
    }

    /**
    * Returns the web image rendition path for the image.
    */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(image);
    }

    /**
     * Image Alt
     * - Description: To author alternate text for the image
    */
    @ValueMapValue
    @Getter
    private String imageAlt;

    /**
     * Card Title
     * - Description: To author Card title
    */
    @ValueMapValue
    @Getter
    private String cardTitle;

    /**
     * Card Description
     * - Description: To author Card description
    */
    @ValueMapValue
    @Getter
    private String cardDescription;

}
