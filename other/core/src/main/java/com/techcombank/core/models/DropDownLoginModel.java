package com.techcombank.core.models;

import javax.inject.Inject;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Mapping Login Properties to Primary Navigation.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DropDownLoginModel {

    /**
     * The label text for the Drop Down Login.
     */
    @ValueMapValue
    @Getter
    private String dropdownLoginText;

    /**
     * The url for the Drop Down Login Page.
     */
    @ValueMapValue
    private String dropdownLoginURL;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the dropdownLoginURL
     */
    public String getDropdownLoginURL() {
        return PlatformUtils.isInternalLink(resourceResolver, dropdownLoginURL);
    }

    /**
     * Returns the login label link url interaction type for analytics.
     */
    public String getDropdownLoginURLInteractionType() {
        return PlatformUtils.getUrlLinkType(dropdownLoginURL);
    }
}
