package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

/**
 * The Class Image Carousel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = CarouselImage.class)
public class CarouselImage {
    @ChildResource
    @Getter
    private List<CarouselImageItemModel> imageItems;

}
