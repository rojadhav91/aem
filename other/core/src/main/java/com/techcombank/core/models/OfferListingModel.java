package com.techcombank.core.models;

import java.util.Map;

public interface OfferListingModel {

    /**
     * offer listing filters tags
     */
    Map<String, String> getFilterTags();

    /**
     * Card type tags
     */
    Map<String, String> getCardTypeTags();

    /**
     * Merchant type tags
     */
    Map<String, String> getMerchantTypeTags();

}
