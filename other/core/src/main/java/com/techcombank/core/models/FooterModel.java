package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;

/**
 * This model class is for Mapping Page Footer Properties to Footer.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterModel {
    /*
     * Techcombank logo.
     *
     * */
    @ValueMapValue
    @Getter
    private String logo;
    /*
     * Alternate text for logo.
     *
     * */
    @ValueMapValue
    @Getter
    private String logoAltText;
    /*
     * Button text placed on right side.
     *
     * */
    @ValueMapValue
    @Getter
    private String rightButtonText;
    /*
     * Copyright Text.
     *
     * */
    @ValueMapValue
    @Getter
    private String copyrightText;
    /*
     * Term and condition Text.
     *
     * */
    @ValueMapValue
    @Getter
    private String termsAndCondition;
    /*
     * Page path to terms and condition.
     *
     * */
    @ValueMapValue
    private String termsAndConditionUrl;

    @Getter
    private String isTermsAndConditionExternal;
    /*
     * Page to open in new tab.
     *
     * */
    @ValueMapValue
    @Getter
    private String openInNewTabTerms;
    /*
     * Text to privacy statement.
     *
     * */
    @ValueMapValue
    @Getter
    private String privacyStatement;
    /*
     * Page path to Privacy statement.
     *
     * */
    @ValueMapValue
    private String privacyStatementLinkUrl;
    @Getter
    private String isPrivacyStatementExternal;
    /*
     * Page to open in new tab.
     *
     * */
    @ValueMapValue
    @Getter
    private String openInNewTabPrivacy;
    /*
     * Personal Banking Contact details.
     *
     * */
    @ValueMapValue
    @Getter
    private String personalBanking;
    /*
     * Business Banking Contact details.
     *
     * */
    @ValueMapValue
    @Getter
    private String businessBanking;
    /*
     * Text to social media section.
     *
     * */
    @ValueMapValue
    @Getter
    private String socialMediaText;
    /*
     * MenuList.
     *
     * */
    @Inject
    @Getter
    private List<FooterMenu> menuList;
    /*
     * Social Media List.
     *
     * */
    @Inject
    @Getter
    private List<FooterSocialMedia> socialMediaList;

    @SlingObject
    private ResourceResolver resourceResolver;

    /**
     * Returns the mobile image rendition path for the Logo.
     */
    public String getLogoMobileImagePath() {
        return PlatformUtils.getMobileImagePath(logo);
    }

    /**
     * Returns the web image rendition path for the Logo.
     */
    public String getLogoWebImagePath() {
        return PlatformUtils.getWebImagePath(logo);
    }

    public String getTermsAndConditionUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, termsAndConditionUrl);
    }

    public String getPrivacyStatementLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, privacyStatementLinkUrl);
    }

    public String getIsTermsAndConditionExternal() {
        return PlatformUtils.getUrlLinkType(termsAndConditionUrl);
    }

    public String getIsPrivacyStatementExternal() {
        return PlatformUtils.getUrlLinkType(privacyStatementLinkUrl);
    }

}
