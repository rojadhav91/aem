package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TimelineModel {

    /**
     * The timeline year.
     */
    @ValueMapValue
    @Getter
    private String timelineYear;

    /**
     * The timeline slider title.
     */
    @ValueMapValue
    @Getter
    private String timelineSliderTitle;

    /**
     * The timeline slider description text.
     */
    @ValueMapValue
    @Getter
    private String timelineSliderDescriptionText;

    /**
     * The image for the timeline.
     */
    @ValueMapValue
    @Getter
    private String timelineImage;

    /**
     * The alt text for the timeline image.
     */
    @ValueMapValue
    @Getter
    private String timelineImageAltText;

    /**
     * Returns the web image rendition path for the timeline image.
     */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(timelineImage);
    }

    /**
     * Returns the mobile image rendition path for the timeline image.
     */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(timelineImage);
    }
}
