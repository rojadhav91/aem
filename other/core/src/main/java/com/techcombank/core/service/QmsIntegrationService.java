package com.techcombank.core.service;

import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;

public interface QmsIntegrationService {
    JsonObject fetchQMSServiceList(boolean isTokenExpired, SlingHttpServletRequest request, JsonObject reqObj);

    JsonObject fetchQMSBranchList(boolean isTokenExpired, SlingHttpServletRequest request, JsonObject reqObj);

    JsonObject fetchQMSBranchSlot(boolean isTokenExpired, SlingHttpServletRequest request, JsonObject reqObj);

    JsonObject qmsSubmitBooking(boolean isTokenExpired, SlingHttpServletRequest request, JsonObject reqObj);

}
