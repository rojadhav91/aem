package com.techcombank.core.service.impl;

import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;

@Component(service = {EmailService.class}, immediate = true)
public class EmailServiceImpl implements EmailService {

    @Reference
    private MessageGatewayService messageGatewayService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String sendEmail(final String subjectLine, final String messageBody, final String[] recipients) {
        logger.info("Email Service Start :");
        try {
            Email email = new HtmlEmail();
            if (recipients != null) {
                for (String recipient : recipients) {
                    email.addTo(recipient, recipient);
                }
            }
            email.setSubject(subjectLine);
            email.setMsg(messageBody);
            MessageGateway<Email> messageGateway = messageGatewayService.getGateway(HtmlEmail.class);
            messageGateway.send(email);
            return TCBConstants.SUCCESS;
        } catch (EmailException e) {
            logger.error("Error in Mail Service {} {}", e.getMessage(), e);
            return TCBConstants.ERROR;
        }
    }

    public String getUserEmail(final WorkItem workItem, final ResourceResolver resourceResolver) {

        MetaDataMap metaDataMap = workItem.getWorkflowData().getMetaDataMap();
        String email = StringUtils.EMPTY;
        if (metaDataMap.containsKey(TCBConstants.USER_ID)) {
            String username = metaDataMap.get(TCBConstants.USER_ID, String.class);
            UserManager userManager = resourceResolver.adaptTo(UserManager.class);
            try {
                Authorizable authorizable = userManager.getAuthorizable(username);
                if (authorizable.hasProperty(TCBConstants.USER_PROFILE_EMAIL_PATH)) {
                    email = authorizable.getProperty(TCBConstants.USER_PROFILE_EMAIL_PATH)[0].getString().toLowerCase();
                }
            } catch (RepositoryException e) {
                logger.error("Exception in getting User Details {} {}", e.getMessage(), e);
            }
        }
        return email;
    }
}
