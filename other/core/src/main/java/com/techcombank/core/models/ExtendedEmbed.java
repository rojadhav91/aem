package com.techcombank.core.models;

import com.adobe.cq.wcm.core.components.models.embeddable.YouTube;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;


@Model(adaptables = Resource.class, adapters = YouTube.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExtendedEmbed implements YouTube {

    @ValueMapValue
    @Getter
    private String youtubeVideoId;
    @ValueMapValue
    @Getter
    private String youtubeWidth;

    @ValueMapValue
    @Getter
    private String youtubeHeight;

    @ValueMapValue
    @Getter
    private String youtubeAspectRatio;
}
