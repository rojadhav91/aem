package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class ArticleCardCrossModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ArticleCardCrossModel.class)
public class ArticleCardCrossModel {

    /**
     * Display Variation
    */
    @ValueMapValue
    @Getter
    private String displayVariation;

    /**
     * Theme
    */
    @ValueMapValue
    @Getter
    private String theme;

    /**
     * Display Image in mobile view ?
     * - Description: To display Image in mobile view
    */
    @ValueMapValue
    @Getter
    private boolean displayImageInMobileView;

    /**
     * Cards
     */
    @ChildResource
    @Getter
    private List<ArticleCardCrossItemModel> articleCardCrossItemList;

}
