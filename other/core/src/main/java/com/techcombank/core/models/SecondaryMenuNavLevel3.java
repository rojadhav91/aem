package com.techcombank.core.models;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import javax.inject.Inject;

/**
 * Sling model for the secondary menu navigation level 3.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = SecondaryMenuNavLevel3.class)
public class SecondaryMenuNavLevel3 {

    /**
     * The label text for the submenu.
     */
    @ValueMapValue
    @Getter
    private String subMenuLabelText;

    /**
     * The label text for the submenu.
     */
    @ValueMapValue
    @Getter
    private String subTabMenuLabelDesc;

    /**
     * The URL for the submenu link.
     */
    @ValueMapValue
    private String subMenuLinkUrl;

    /**
     * The path to the icon image for the submenu.
     */
    @ValueMapValue
    @Getter
    private String subMenuIconImage;

    /**
     * The alternative text for the icon image.
     */
    @ValueMapValue
    @Getter
    private String imageAltText;

    /**
     * Whether the submenu link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String openinnewtab;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the subMenuLinkUrl
     */
    public String getSubMenuLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, subMenuLinkUrl);
    }

    /**
     * Returns the mobile image rendition path for the subMenuIconImage.
     */
    public String getSubMenuIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(subMenuIconImage);
    }

    /**
     * Returns the web image rendition path for the subMenuIconImage.
     */
    public String getSubMenuWebIconImagePath() {
        return PlatformUtils.getWebImagePath(subMenuIconImage);
    }

    /**
     * Returns the sub menu link url interaction type for analytics.
     */
    public String getSubMenuLinkUrlInteractionType() {
        return PlatformUtils.getUrlLinkType(subMenuLinkUrl);
    }
}
