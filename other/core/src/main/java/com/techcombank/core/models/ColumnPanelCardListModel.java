package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.annotation.PostConstruct;

/**
 * This model class is for define each Tile of Carousel Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ColumnPanelCardListModel.class)
public class ColumnPanelCardListModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/columnpanel";

    @OSGiService
    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    /**
     * Card Title
     */
    @ValueMapValue
    @Getter
    private String cardTitle;

    /**
     * Card Descrption
     */
    @ValueMapValue
    private String cardDescription;

    /**
     * Card Link URL
     */
    @ValueMapValue
    private String cardLinkUrl;

    /**
     *card Target
     */
    @ValueMapValue
    @Getter
    private String cardTarget;

    /**
     * cardNoFollow
     */
    @ValueMapValue
    @Getter
    private boolean cardNoFollow;

    /**
     * Card Arrow
     */
    @ValueMapValue
    @Getter
    private boolean cardArrow;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    public String getCardLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, cardLinkUrl);
    }

    public String getCardDescription() {

        return cardDescription.replace("<p>", "").replace(("</p>"), "");
    }
    /**
     * Returns the  interaction type for analytics.
     */
    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(cardLinkUrl);
    }
}
