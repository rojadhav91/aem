package com.techcombank.core.models;

import lombok.Getter;

import javax.inject.Inject;

import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Fixing Rate Content Fragment property to Java Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = FixingRate.class)
public class FixingRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * Input Date
     */
    @ValueMapValue
    @Getter
    private String inputDate;

    /**
     * Input Time
     */
    @ValueMapValue
    @Getter
    @Setter
    private String inputTime;

    /**
     * Source Currency
     */
    @ValueMapValue
    @Getter
    private String sourceCurrency;

    /**
     * Target Currency
     */
    @ValueMapValue
    @Getter
    private String targetCurrency;

    /**
     * VND Loan
     */
    @ValueMapValue
    @Getter
    private String vndLoan;

    /**
     * Smart Money
     */
    @ValueMapValue
    @Getter
    private String smartMoney;
}
