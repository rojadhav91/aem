package com.techcombank.core.pojo;

import lombok.Getter;
import lombok.Setter;

public class HrefLangPojo {

    @Getter
    @Setter
    private String rel;

    @Getter
    @Setter
    private String hrefLang;

    @Getter
    @Setter
    private String href;
}
