package com.techcombank.core.service.impl;

import com.techcombank.core.service.ResourceResolverService;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Component(service = ResourceResolverService.class, immediate = true)
public class ResourceResolverServiceImpl implements ResourceResolverService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String READ_SYSTEM_USER = "tcb-read-service-user";

    private static final String WRITE_SYSTEM_USER = "tcb-write-service-user";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public ResourceResolver getReadResourceResolver() {
        Map<String, Object> param = new HashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, READ_SYSTEM_USER);
        try {
            return resourceResolverFactory.getServiceResourceResolver(param);
        } catch (LoginException loginExp) {
            logger.error("Login Exception while getting service resource resolver: {}", loginExp.getMessage());
            return null;
        }
    }
    @Override
    public ResourceResolver getWriteResourceResolver() {
        Map<String, Object> param = new HashMap<>();
        param.put(ResourceResolverFactory.SUBSERVICE, WRITE_SYSTEM_USER);
        try {
            return resourceResolverFactory.getServiceResourceResolver(param);
        } catch (LoginException loginExp) {
            logger.error("Login Exception while setting service resource resolver: {} {}",
                    loginExp.getMessage(), loginExp);
            return null;
        }
    }
}
