package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class SpacerModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = SpacerModel.class)
public class SpacerModel {

    /** The spacer desktop padding. */
    @ValueMapValue
    @Getter
    private int desktopPadding;

    /** The spacer mobile padding. */
    @ValueMapValue
    @Getter
    private int mobilePadding;
}
