package com.techcombank.core.models;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ListViewDocumentModel.class)
public class ListViewDocumentModel {
    /**
     * Title
     */
    @ValueMapValue
    @Getter
    private String title;
    /**
     * Year Colon Label
     */
    @ValueMapValue
    @Getter
    private String yearColonLabel;
    /**
     * Year Label
     */
    @ValueMapValue
    @Getter
    private String yearLabel;
    /**
     * Quarter Label
     */
    @ValueMapValue
    @Getter
    private String quarterLabel;

    /**
     * Number of records
     */
    @ValueMapValue
    @Getter
    private String numberOfRecords;

    /**
     * Loading PDF
     */
    @ValueMapValue
    @Getter
    private String loadingPDF;

    /**
     * Download
     */
    @ValueMapValue
    @Getter
    private String download;
    /**
     * Apply label
     */
    @ValueMapValue
    @Getter
    private String applyLabel;

    @ValueMapValue
    @Getter
    private String filterPlaceholderLabel;

    @ValueMapValue
    @Getter
    private boolean hideFilter;

    /**
     * List of Tab Items
     */
    @ChildResource
    @Getter
    private List<ListViewDocumentTabItemModel> tabItems;
    /**
     * To get tabs details
     */
    public String getTabs() {
        String tabs = StringUtils.EMPTY;
        for (ListViewDocumentTabItemModel tabItem : tabItems) {
            if (!StringUtils.isEmpty(tabs)) {
                tabs = tabs.concat(";");
            }
            tabs = tabs.concat(tabItem.getTabTitle());
        }
        return tabs;
    }


    /**
     * To get year list
     */
    public Set<String> getYearList() {
        Set<String> yearList = new HashSet<>();
        for (ListViewDocumentTabItemModel tabItem : tabItems) {
            if (tabItem.getListViewDocumentItemsFirstLevel() != null) {
            for (ListViewDocumentLevelOneItemModel itemModel : tabItem.getListViewDocumentItemsFirstLevel()) {
                    String year = itemModel.getYear();
                    if (StringUtils.isNotBlank(year)) {
                        yearList.add(year);
                    }
                }
            }
        }
        return yearList;
    }
}
