package com.techcombank.core.models.impl;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.models.ExchangeRateModel;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Exchange Rate details
 */
@Model(adaptables = Resource.class, adapters = {ExchangeRateModel.class },
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = ExchangeRateModelImpl.RESOURCE_TYPE)
public class ExchangeRateModelImpl implements ExchangeRateModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/exchangerate";

    /**
     * The Text for the exchange rate title.
     */
    @ValueMapValue
    @Getter
    private String exchangeRateTitle;

    /**
     * The Text for the tab day label.
     */
    @ValueMapValue
    @Getter
    private String dayLabel;

    /**
     * The Icon for day filed
     */
    @ValueMapValue
    @Getter
    private String dayIcon;

    /**
     * The Alt Text for day icon.
     */
    @ValueMapValue
    @Getter
    private String dayIconAlt;

    /**
     * The Text for the time label.
     */
    @ValueMapValue
    @Getter
    private String timeLabel;

    /**
     * The Icon for time filed.
     */
    @ValueMapValue
    @Getter
    private String timeIcon;

    /**
     * The Alt Text for time icon.
     */
    @ValueMapValue
    @Getter
    private String timeIconAlt;

    /**
     * The Text for currency label.
     */
    @ValueMapValue
    @Getter
    private String currencyLabel;

    /**
     * The Text for the purchase label.
     */
    @ValueMapValue
    @Getter
    private String purchaseLabel;

    /**
     * The Text for the cash label.
     */
    @ValueMapValue
    @Getter
    private String cashLabel;

    /**
     * The Text for the transfer label.
     */
    @ValueMapValue
    @Getter
    private String transferLabel;

    /**
     * The Text for the sell label.
     */
    @ValueMapValue
    @Getter
    private String sellLabel;

    /**
     * The Text for the summary description.
     */
    @ValueMapValue
    @Getter
    private String summaryDesc;

    /**
     * The Text for central label.
     */
    @ValueMapValue
    @Getter
    private String centralLabel;

    /**
     * The Text for the floor label.
     */
    @ValueMapValue
    @Getter
    private String floorLabel;

    /**
     * The Text for the ceiling label.
     */
    @ValueMapValue
    @Getter
    private String ceilingLabel;

    /**
     * The Text for the USD label.
     */
    @ValueMapValue
    @Getter
    private String usdLabel;

    /**
     * The Text for the reference label.
     */
    @ValueMapValue
    @Getter
    private String refLabel;

    /**
     * The Text for the term label.
     */
    @ValueMapValue
    @Getter
    private String termLabel;

    /**
     * The Text for the gold label.
     */
    @ValueMapValue
    @Getter
    private String goldLabel;

    /**
     * The Text for the see more label.
     */
    @ValueMapValue
    @Getter
    private String seeMoreLabel;

    /**
     * The Text for the cta label.
     */
    @ValueMapValue
    @Getter
    private String ctaLabel;

    /**
     * The Text for the error message.
     */
    @ValueMapValue
    @Getter
    private String errorMessage;

    /**
     * The Text for the footer label one
     */
    @ValueMapValue
    @Getter
    private String footerLabelOne;

    /**
     * The Text for the footer label two
     */
    @ValueMapValue
    @Getter
    private String footerLabelTwo;

    /**
     * The Text for the footer label three
     */
    @ValueMapValue
    @Getter
    private String footerLabelThree;

    /**
     * Returns the web image rendition path for the day icon.
     */
    @Override
    public String getDayIconWebImagePath() {
        return PlatformUtils.getWebImagePath(dayIcon);
    }

    /**
     * Returns the mobile image rendition path for the day icon.
     */
    @Override
    public String getDayIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(dayIcon);
    }

    /**
     * Returns the web image rendition path for the time icon.
     */
    @Override
    public String getTimeIconWebImagePath() {
        return PlatformUtils.getWebImagePath(timeIcon);
    }

    /**
     * Returns the mobile image rendition path for the time icon.
     */
    @Override
    public String getTimeIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(timeIcon);
    }

}
