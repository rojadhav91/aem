package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

/**
 * HomePageBlockNavigation is a SlingModel that represents a navigation item on the homepageBlock Component.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HomePageBlockNavigation {

    protected static final String RESOURCE_TYPE = "techcombank/components/homepageblock";

    /**
     * The text for the navigation item.
     */
    @ValueMapValue
    @Getter
    private String text;

    /**
     * The URL for the navigation item.
     */
    @ValueMapValue
    @Getter
    private String homeLink;

    /**
     * Whether the link should open in a new tab.
     * true if the link should open in a new tab, false otherwise
     */
    @ValueMapValue
    @Getter
    private String openInNewTabNavigation;

    /**
     * The icon for the navigation item.
     */
    @ValueMapValue
    @Getter
    private String icon;

    /**
     * The text for the icon.
     */
    @ValueMapValue
    @Getter
    private String iconText;

    /**
     * The Checkbox  for follow and unfollow.
     */
    @ValueMapValue
    @Getter
    private String nofollow;

    @Getter
    private String interaction;

    @OSGiService
    private ResourceResolverService resourceResolverService;
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        resourceResolver = resourceResolverService.getReadResourceResolver();
    }

    public String getHomeLink() {
        return StringUtils.isBlank(PlatformUtils.isInternalLink(resourceResolver,
                homeLink)) ? homeLink : PlatformUtils.isInternalLink(resourceResolver, homeLink);
    }

    public String getInteraction() {
        return getWebInteractionValue(homeLink);
    }

    /**
     * Returns the web image rendition path for the Icon image.
     */
    public String getIconWebImagePath() {
        return PlatformUtils.getWebImagePath(icon);
    }

    /**
     * Returns the mobile image rendition path for the Icon image.
     */
    public String getIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(icon);
    }

    public String getWebInteractionValue(final String element) {
        if (null != resourceResolver) {
            String componentName = PlatformUtils.getJcrTitle(resourceResolver, RESOURCE_TYPE);
            String linkType = PlatformUtils.getUrlLinkType(element);
            return PlatformUtils.getInteractionType(componentName, linkType);
        } else {
            return StringUtils.EMPTY;
        }
    }

}
