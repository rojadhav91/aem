package com.techcombank.core.service.impl;

import com.day.cq.wcm.webservicesupport.ConfigurationManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.RecaptchaService;
import com.techcombank.core.service.ResourceResolverService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Optional;

@Slf4j
@Component(service = {RecaptchaService.class}, immediate = true)
public class RecaptchaServiceImpl implements RecaptchaService {

    @Reference
    private ResourceResolverService resourceResolverService;

    /**
     * This method returns siteKey
     *
     * @return String siteKey
     */
    public Optional<String> getSiteKey(final String configPath) {
        return getConfigurationParameter(configPath, TCBConstants.SITE_KEY);
    }

    /**
     * This method returns key value
     *
     * @return String key value
     */
    private Optional<String> getConfigurationParameter(final @NonNull String configPath, final @NonNull String key) {

        try (ResourceResolver resolver = resourceResolverService.getReadResourceResolver()) {
            return Optional.ofNullable(resolver.adaptTo(ConfigurationManager.class))
                    .map(manager -> manager.getConfiguration(configPath))
                    .map(configuration -> configuration.get(key, (String) null));
        } catch (RuntimeException e) {
            log.error("Get Configuration Parameter Error {}", e);
            return Optional.empty();
        }

    }

    /**
     * This method get secretKey
     *
     * @return String secretKey
     */
    public Optional<String> getSecretKey(final String configPath) {
        return getConfigurationParameter(configPath, TCBConstants.SECRET_KEY);
    }

}
