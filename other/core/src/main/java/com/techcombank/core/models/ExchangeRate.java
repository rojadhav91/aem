package com.techcombank.core.models;

import lombok.Getter;
import lombok.Setter;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Exchange Rate Content Fragment property to
 * Java Class.
 */
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = ExchangeRate.class)
public class ExchangeRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * Label
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * Ask Rate
     */
    @ValueMapValue
    @Getter
    @Setter
    private String askRate;

    /**
     * Bid Rate
     */
    @ValueMapValue
    @Getter
    @Setter
    private String bidRateCK;

    /**
     * Bid Rate TM
     */
    @ValueMapValue
    @Getter
    @Setter
    private String bidRateTM;

    /**
     * Source Currency
     */
    @ValueMapValue
    @Getter
    private String sourceCurrency;

    /**
     * Target Currency
     */
    @Getter
    @Setter
    private String targetCurrency;

    /**
     * Ask Rate TM
     */
    @ValueMapValue
    @Getter
    @Setter
    private String askRateTM;

    /**
     * Flag
     */
    @Getter
    @Setter
    private String flag;
}
