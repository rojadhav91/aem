package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import lombok.Getter;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ColumnPanelModel.class)
public class ColumnPanelModel {
    /**
     * View more label text
     */
    @ValueMapValue
    @Getter
    private String viewMoreLabelText;

    /**
     * View less label text
     */
    @ValueMapValue
    @Getter
    private String viewLessLabelText;

    /**
     * List of Cards
     */
    @ChildResource
    @Getter
    private List<ColumnPanelListModel> cardLists;
}
