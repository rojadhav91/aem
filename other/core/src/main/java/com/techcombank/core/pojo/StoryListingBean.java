package com.techcombank.core.pojo;

import com.day.cq.commons.jcr.JcrConstants;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Named;
import java.util.List;


/**
 * A Sling model that represents a story listing.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = StoryListingBean.class)
public class StoryListingBean {

    /**
     * The title of the story.
     */
    @ValueMapValue
    @Getter
    @Setter
    @Named(value = JcrConstants.JCR_TITLE)
    private String title;


    /**
     * The Article category of the article.
     */
    @Getter
    @Setter
    private List<String> articleCategory;

    /**
     * The description of the story.
     */
    @ValueMapValue
    @Getter
    @Setter
    @Named(value = JcrConstants.JCR_DESCRIPTION)
    private String description;

    /**
     * The path to the article image.
     */
    @Getter
    @Setter
    private String image;

    /**
     * The creation date of the article.
     */
    @ValueMapValue
    @Getter
    @Setter
    private String articleCreationDate;

    @Getter
    @Setter
    private String path;

}
