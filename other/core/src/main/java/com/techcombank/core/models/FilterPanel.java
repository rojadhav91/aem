package com.techcombank.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FilterPanel {

    @Inject
    @Getter
    private String categoryLabel;

    @Inject
    @Getter
    private String viewAll;

    @Getter
    private Set<String> tags;

    @Inject
    private ResourceResolver resourceResolver;

    @Self
    private Resource resource;

    @PostConstruct
    public void init() throws RepositoryException {
        String path = resource.getPath();
        QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
        Session session = resourceResolver.adaptTo(Session.class);
        Map<String, String> predicates = new HashMap<>();
        predicates.put(TCBConstants.PATH, path);
        predicates.put(TCBConstants.PROPERTY_1, TCBConstants.CQ_FILTERPANELTAG);
        predicates.put(TCBConstants.PROPERTY_OPERATION_1, TCBConstants.EXISTS);
        Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        SearchResult result = query.getResult();
        if (null != result) {
            tags = new HashSet<>();
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            for (Hit hit : result.getHits()) {
                getTagsFromNode(tagManager, hit);
            }
        }
    }

    private void getTagsFromNode(final TagManager tagManager, final Hit hit) throws RepositoryException {
        Resource hitResource = resourceResolver.getResource(hit.getPath());
        if (null != hitResource) {
            Node node = hitResource.adaptTo(Node.class);
            if (null != node && node.hasProperty(TCBConstants.CQ_FILTERPANELTAG)) {
                String tagName = node.getProperty(TCBConstants.CQ_FILTERPANELTAG).getString();
                Tag tag = tagManager.resolve(tagName);
                if (null != tag) {
                    tags.add(tag.getTitle());
                }
            }
        }
    }
}
