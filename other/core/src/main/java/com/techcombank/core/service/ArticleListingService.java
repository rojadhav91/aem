package com.techcombank.core.service;

import com.techcombank.core.pojo.Articles;
import org.apache.sling.api.SlingHttpServletRequest;

import javax.jcr.RepositoryException;
import java.util.List;
import java.util.Locale;

public interface ArticleListingService {

    /**
     * Service to obtain articles List
     * @param request
     * @return
     */
    List<Articles> getArticlesList(SlingHttpServletRequest request, Locale locale) throws RepositoryException;
}
