package com.techcombank.core.models;

/**
 * Interface For RecaptchaModel
 *
 */
public interface RecaptchaModel {

    /**
     * getSiteKey
     */
    String getSiteKey();

    /**
     * getSecretKey
     */
    String getSecretKey();

    /**
     * getRequiredMessage
     */
    String getRequiredMessage();

    /**
     * isRequired
     */
    boolean isRequired();
}
