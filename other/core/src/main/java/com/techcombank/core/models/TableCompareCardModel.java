package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class TableCompareCardModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableCompareCardModel {

    /**
     * Card Title
     * - Description: To author card title
    */
    @ValueMapValue
    @Getter
    private String cardTitle;

    /**
     * Card Title Image
     * - Description: To author card title image
    */
    @ValueMapValue
    @Getter
    private String cardTitleImage;

    /**
    * Returns the mobile image rendition path for the card title image.
    */
    public String getMobileCardTitleImagePath() {
        return PlatformUtils.getMobileImagePath(cardTitleImage);
    }

    /**
    * Returns the web image rendition path for the card title image.
    */
    public String getWebCardTitleImagePath() {
        return PlatformUtils.getWebImagePath(cardTitleImage);
    }

    /**
     * Card Background Colour
     * - Description: To author card background colour
    */
    @ValueMapValue
    @Getter
    private String cardBackgroundColour;

    /**
     * Rows list
     */
    @ChildResource
    @Getter
    private List<TableCompareRowModel> tableCompareRowList;

}
