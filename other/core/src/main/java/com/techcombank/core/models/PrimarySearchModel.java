package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class PrimarySearchModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = PrimarySearchModel.class)
public class PrimarySearchModel {

    /**
     * Result Slug
     * - Description: Used as prefix slug for the search result page. Ec: Result Slug = /search .Then when user search a keyword, they we be redirected to https://techcombank.com/search?q=keyword
    */
    @ValueMapValue
    @Getter
    private String resultSlug;

    /**
     * Input Place Holder
     * - Description: Place holder text shown on the search bar.
    */
    @ValueMapValue
    @Getter
    private String inputPlaceHolder;

    /**
     * Input Search Icon
     * - Description: Search icon (Magnify glass) shown at the website's top menu.
    */
    @ValueMapValue
    @Getter
    private String inputSearchIcon;

    /**
     * History Label
     * - Description: Title for the recently searched keywords section.
    */
    @ValueMapValue
    @Getter
    private String historyLabel;

    /**
     * UsefulLinks Label
     * - Description: Title for the UsefulLinks section.
    */
    @ValueMapValue
    @Getter
    private String usefulLinksTitle;

    /**
     * UsefulLinks
     */
    @ChildResource
    @Getter
    private List<UsefulLinksItemModel> usefulLinksItemList;

}
