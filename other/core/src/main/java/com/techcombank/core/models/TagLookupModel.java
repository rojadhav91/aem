package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Objects;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TagLookupModel {

    @Inject
    private ResourceResolver resourceResolver;

    @ValueMapValue(name = TCBConstants.CQ_FILTERPANELTAG)
    private String filterpaneltag;

    @Getter
    private String tagLabel;

    @PostConstruct
    public void init() {
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        Tag tag = tagManager.resolve(filterpaneltag);
        if (!Objects.isNull(tag)) {
            tagLabel = tag.getTitle();
        }
    }
}
