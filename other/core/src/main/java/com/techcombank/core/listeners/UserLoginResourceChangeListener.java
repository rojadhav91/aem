package com.techcombank.core.listeners;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.ResourceResolverService;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.util.List;

@Component(service = ResourceChangeListener.class, immediate = true, property = {
        ResourceChangeListener.PATHS + TCBConstants.EQUALS_SYMBOL + UserLoginResourceChangeListener.HOME_USER,
        ResourceChangeListener.CHANGES + TCBConstants.EQUALS_SYMBOL + UserLoginResourceChangeListener.ADDED})
@Designate(ocd = UserLoginResourceChangeListener.AdminLoginEventConfig.class)
public class UserLoginResourceChangeListener implements ResourceChangeListener {

    protected static final String HOME_USER = "/home/users";
    protected static final String ADDED = "ADDED";
    protected static final String TOKEN_PATH = TCBConstants.SLASH + ".tokens" + TCBConstants.SLASH;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private AdminLoginEventConfig adminLoginEventConfig;
    @Reference
    private ResourceResolverService resourceResolverService;

    @Activate
    public void activate(final AdminLoginEventConfig config) {
        this.adminLoginEventConfig = config;
    }

    @Override
    public void onChange(final List<ResourceChange> changes) {

        ResourceChange loginEvent = changes.stream()
                .filter(change -> change.getType().toString().equals(ADDED)
                        && change.getPath().startsWith(HOME_USER) && change.getPath().contains(TOKEN_PATH))
                .findFirst().orElse(null);

        changes.forEach(change -> {
            logger.debug("RESOURCE_CHANGE_EVENT");
            logger.debug("Event Type: {}", change.getType());
            logger.debug("Event Path: {}", change.getPath());
            logger.debug("Is Users: {}", change.getPath().startsWith(HOME_USER));
            logger.debug("Is Token: {}", change.getPath().contains(TOKEN_PATH));
            logger.debug("Event External: {}", change.isExternal());

            if (change.getType().toString().equals(ADDED) && change.getPath().startsWith(HOME_USER)
                    && change.getPath().contains(TOKEN_PATH)) {
                logger.debug("User login event");
            }
        });

        if (null != loginEvent) {
            String eventResourcePath = loginEvent.getPath();
            if (null != eventResourcePath) {
                checkUserRole(eventResourcePath);
            } else {
                logger.debug("Empty user resource path");
            }
        } else {
            logger.debug("No Matching event");
        }

    }

    private void checkUserRole(final String loggedInUserPath) {
        String userPath = loggedInUserPath.substring(0, loggedInUserPath.indexOf(TOKEN_PATH));
        ResourceResolver resourceResolver = resourceResolverService.getReadResourceResolver();
        Resource userResource = resourceResolver.getResource(userPath);
        try {
            if (null != userResource) {
                Authorizable user = userResource.adaptTo(Authorizable.class);

                Resource adminGroupResource = resourceResolver.getResource(adminLoginEventConfig.getUserCreationPath());

                if (null != user && null != adminGroupResource) {
                    UserManager userManager = resourceResolver.adaptTo(UserManager.class);
                    User userRole = (User) userManager.getAuthorizable(user.getID());
                    Group adminGroup = adminGroupResource.adaptTo(Group.class);
                    if (adminGroup.isMember(user) || (null != userRole && userRole.isAdmin())) {
                        logger.info("[ADMIN_LOGIN_EVENT] Login event for admin user: {}", user.getID());
                    } else {
                        logger.debug("[NON_ADMIN_LOGIN_EVENT] Login event for non admin user: {}", user.getID());
                    }
                } else {
                    logger.debug("Admin user group resource is not available");
                }
            } else {
                logger.debug("Invalid user resource path: {}", userPath);
            }

        } catch (RepositoryException e) {
            logger.error("Exception occured in checkUserRole method:", e);
        }

    }

    @ObjectClassDefinition(name = "Admin Login Event Configurations", description = "Admin Login Event Configurations")
    public @interface AdminLoginEventConfig {
        @AttributeDefinition(name = "userCreationPath",
                description = "User Creation Path", type = AttributeType.STRING)
        String getUserCreationPath() default "/home/groups/a/administrators";
    }
}
