package com.techcombank.core.pojo;

import lombok.Getter;
import lombok.Setter;

public class FAQ {
    @Getter
    @Setter
    private String faqTitle;
    @Getter
    @Setter
    private String faqDescription;
}
