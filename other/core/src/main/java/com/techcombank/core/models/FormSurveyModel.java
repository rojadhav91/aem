package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class FormSurveyModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = FormSurveyModel.class)
public class FormSurveyModel {

    /**
     * Survey Type
     * - Description: To pick the type of this survey question
    */
    @ValueMapValue
    @Getter
    private String surveyType;

    /**
     * Title
     * - Description: Title of this survey question
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Caption
     * - Description: Additional description
    */
    @ValueMapValue
    @Getter
    private String caption;

    /**
     * Name
     * - Description: Name of field in form to submit
    */
    @ValueMapValue
    @Getter
    private String name;

    /**
     * Answer Display Direction
     * - Description: Select answer layout
    */
    @ValueMapValue
    @Getter
    private String answerDisplayDirection;

    /**
     * Minimum Number Of Answers
     * - Description: Minimum number of answers that
    */
    @ValueMapValue
    @Getter
    private int minimumNumberOfAnswers;

    /**
     * Minimum Number Of Answers Alert
     * - Description: Error alert for minimum number of answers if not meet the requirement
    */
    @ValueMapValue
    @Getter
    private String minimumNumberOfAnswersAlert;

    /**
     * Answer Type
     * - Description: Type of answer
    */
    @ValueMapValue
    @Getter
    private String answerType;

    /**
     * Answers
     */
    @ChildResource
    @Getter
    private List<FormSurveyItemModel> formSurveyItemList;

    // public int[] getAnswerIndexes() {
    //     if (formSurveyItemList == null || formSurveyItemList.isEmpty()) return new int[0];
    //     int[] indexes = new int[formSurveyItemList.size()];
    //     for (int i = 0; i < formSurveyItemList.size(); i++) {
    //         indexes[i] = i + 1;
    //     }
    //     return indexes;
    // }

}
