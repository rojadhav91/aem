package com.techcombank.core.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.HttpClientExecutorService;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component(service = HttpClientExecutorService.class)
public class HttpClientExecutorServiceImpl implements HttpClientExecutorService {

    private static final Logger LOG = LoggerFactory.getLogger(HttpClientExecutorServiceImpl.class);

    private static final int CONNECT_TIMEOUT_SECONDS = 30;
    private static final int CONNECTION_REQUEST_TIMEOUT_SECONDS = 30;
    private static final int SOCKET_TIMEOUT_SECONDS = 30;
    private static final int MAX_CONN_TOTAL = 200;
    private static final int MAX_CONN_PER_ROUTE = 20;
    private static final String LOG_RESPONSE_INFO = "Received HTTP response code: {}, message {}";
    private static final String LOG_RESPONSE_ERROR =
            "Error while executing HTTP request ::: response code: {}, message: {}";

    private final CloseableHttpClient httpClient;

    public HttpClientExecutorServiceImpl() {
        CredentialsProvider provider = new BasicCredentialsProvider();
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout((int) TimeUnit.SECONDS.toMillis(CONNECT_TIMEOUT_SECONDS))
                .setConnectionRequestTimeout((int) TimeUnit.SECONDS.toMillis(CONNECTION_REQUEST_TIMEOUT_SECONDS))
                .setSocketTimeout((int) TimeUnit.SECONDS.toMillis(SOCKET_TIMEOUT_SECONDS)).build();
        httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).setMaxConnTotal(MAX_CONN_TOTAL)
                .setMaxConnPerRoute(MAX_CONN_PER_ROUTE).setConnectionManagerShared(true)
                .setDefaultCredentialsProvider(provider).build();
    }

    /**
     * This method is used for HTTP POST call. It execute the POST request and send
     * the response back
     *
     * @param url
     * @param headerMap
     * @param requestParamMap
     * @param payLoad
     * @return JsonObject responseJsonObject
     */
    @Override
    public JsonObject executeHttpPost(final String url, final Map<String, String> headerMap,
            final StringEntity payLoad) {

        HttpPost postRequest = new HttpPost(url);
        HttpResponse response;
        JsonObject responseJson = new JsonObject();
        try {
            for (Map.Entry<String, String> headerVal : headerMap.entrySet()) {
                postRequest.addHeader(new BasicHeader(headerVal.getKey(), headerVal.getValue()));
            }

            if (payLoad != null) {
                postRequest.setEntity(payLoad);
            }

            response = httpClient.execute(postRequest);
            responseJson = populateResponseData(response);
        } catch (JsonSyntaxException | ParseException | IOException e) {
            responseJson = populateErrorResponseData(e);
        }
        return responseJson;
    }

    /**
     * This method is used for HTTP GET call. It execute the GET request and send
     * the response back
     *
     * @param url
     * @param headerMap
     * @return JsonObject responseJsonObject
     */
    @Override
    public JsonObject executeHttpGet(final String url, final Map<String, String> headerMap,
            final List<NameValuePair> requestParamMap) {
        HttpGet getRequest = new HttpGet(url);
        HttpResponse response;
        JsonObject responseJson = new JsonObject();
        try {

            if (null != requestParamMap && !requestParamMap.isEmpty()) {
                URI uriBuilder = new URIBuilder(getRequest.getURI()).addParameters(requestParamMap).build();
                getRequest.setURI(uriBuilder);
            }

            for (Map.Entry<String, String> headerVal : headerMap.entrySet()) {
                getRequest.addHeader(new BasicHeader(headerVal.getKey(), headerVal.getValue()));
            }

            response = httpClient.execute(getRequest);
            responseJson = populateResponseData(response);

        } catch (JsonSyntaxException | ParseException | IOException | URISyntaxException e) {
            responseJson = populateErrorResponseData(e);
        }
        return responseJson;
    }

    /**
     * This method populate json response from the mulesoft api.
     *
     * @param response responseJsonObject
     * @return JsonObject
     * @throws JsonSyntaxException
     * @throws ParseException
     * @throws IOException
     */
    private JsonObject populateResponseData(final HttpResponse response)
            throws JsonSyntaxException, ParseException, IOException {
        JsonObject responseJsonObject;
        int statusCode = response.getStatusLine().getStatusCode();
        String responseMessage = response.getStatusLine().getReasonPhrase();

        if (statusCode == HttpStatus.SC_OK) {
            responseJsonObject = new Gson().fromJson(EntityUtils.toString(response.getEntity()), JsonObject.class);
            LOG.info(LOG_RESPONSE_INFO, statusCode, responseMessage);
        } else {
            LOG.info(LOG_RESPONSE_INFO, statusCode, responseMessage);
            responseJsonObject = new Gson().fromJson(EntityUtils.toString(response.getEntity()),
                    JsonObject.class);
            if (null == responseJsonObject) {
                responseJsonObject = new JsonObject();
            }
            if (!responseJsonObject.has(TCBConstants.ERROR_CODE)) {
                responseJsonObject.addProperty(TCBConstants.ERROR_CODE, statusCode);
            }

        }

        return responseJsonObject;
    }

    /**
     * This method sets the error response data
     *
     * @param e
     * @return JsonObject errorResponseJsonObject
     */
    private JsonObject populateErrorResponseData(final Exception e) {
        JsonObject errorResponseJsonObject = new JsonObject();
        errorResponseJsonObject.addProperty(TCBConstants.ERROR_MSG, e.getMessage());
        errorResponseJsonObject.addProperty(TCBConstants.ERROR_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        LOG.error(LOG_RESPONSE_ERROR, HttpStatus.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        return errorResponseJsonObject;
    }

}
