package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class TableCompareModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = TableCompareModel.class)
public class TableCompareModel {

    /**
     * Column Header Title
     * - Description: To author column header title
    */
    @ValueMapValue
    @Getter
    private String columnHeaderTitle;

    /**
     * Column headers list
     */
    @ChildResource
    @Getter
    private List<TableCompareColumnHeaderModel> tableCompareColumnHeaderList;

    /**
     * Cards list
     */
    @ChildResource
    @Getter
    private List<TableCompareCardModel> tableCompareCardList;

}
