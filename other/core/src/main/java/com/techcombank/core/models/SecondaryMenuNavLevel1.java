package com.techcombank.core.models;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import javax.inject.Inject;
import java.util.List;

/**
 * Sling Model representing a secondary menu navigation item.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = SecondaryMenuNavLevel1.class)
public class SecondaryMenuNavLevel1 {

    /**
     * The label text for the menu item.
     */
    @ValueMapValue
    @Getter
    private String menuLabelText;

    /**
     * The URL of the menu item link.
     */
    @ValueMapValue
    private String menuLinkUrl;

    /**
     * Whether the menu item should open in a new tab.
     */

    @ValueMapValue
    @Getter
    private String openinnewtab;

    /**
     * A list of secondary menu navigation items for the current item.
     */
    @ChildResource
    @Getter
    private List<SecondaryMenuNavLevel2> secondaryMenuNavLevel2;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the menuLinkUrl
     */
    public String getMenuLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, menuLinkUrl);
    }

    /**
     * Returns the menu link url interaction type for analytics.
     */
    public String getMenuLinkUrlInteractionType() {
        return PlatformUtils.getUrlLinkType(menuLinkUrl);
    }

}
