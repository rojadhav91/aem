package com.techcombank.core.service;

import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import org.apache.sling.api.resource.ResourceResolver;

public interface WorkflowNotificationService {

    void inboxNotification(ResourceResolver resolver, String initiator, String emailSubject,
                           String workflowTitleMsg, String payload);

    void emailNotifications(String payload, String workflowTitle, String approverName,
                            String subject, String emailTitle, String email);
    void sendNotificationAndEmail(ResourceResolver resourceResolver, MetaDataMap wfMetaData, String payload,
                                  String initiator, String approverName, String subject, String emailTitle);

    String getUserComment(WorkItem workItem, WorkflowSession workflowSession);
}
