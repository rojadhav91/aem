package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.inject.Inject;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListTilesItemModel {

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Background image
     */
    @ValueMapValue
    @Getter
    private String backgroundImage;

    /**
    * Returns the mobile image rendition path for the Background image.
    */
    public String getMobileBackgroundImagePath() {
        return PlatformUtils.getMobileImagePath(backgroundImage);
    }

    /**
    * Returns the web image rendition path for the Background image.
    */
    public String getWebBackgroundImagePath() {
        return PlatformUtils.getWebImagePath(backgroundImage);
    }

    /**
     * Nudge Text
     */
    @ValueMapValue
    @Getter
    private String nudgeText;

    /**
     * Theme
     */
    @ValueMapValue
    @Getter
    private String theme;

    /**
     * Text (Title)
     */
    @ValueMapValue
    @Getter
    private String textTitle;

    /**
     * Description (RTE)
     */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * Link Text
     */
    @ValueMapValue
    @Getter
    private String linkText;

    /**
     * Link Url
     */
    @ValueMapValue
    private String linkUrl;

    /**
     * Open in new tab
     */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No follow for SEO
     */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * @return the linkURL
     */
    public String getLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, linkUrl);
    }
}
