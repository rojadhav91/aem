package com.techcombank.core.service.impl;

import com.adobe.acs.commons.genericlists.GenericList;
import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.drew.lang.annotations.NotNull;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.ExchangeRate;
import com.techcombank.core.models.FixingRate;
import com.techcombank.core.models.GoldRate;
import com.techcombank.core.models.OtherRate;
import com.techcombank.core.models.TenorIntRate;
import com.techcombank.core.models.TenorRate;
import com.techcombank.core.service.VfxRatesDataService;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


import javax.jcr.Session;



@Component(service = { VfxRatesDataService.class }, immediate = true)
@Designate(ocd = VfxRatesDataServiceImpl.VfxRatesDataConfig.class)
public class VfxRatesDataServiceImpl implements VfxRatesDataService {
    @ObjectClassDefinition(name = "VFX Rates Data Configuration", description = "VFX Rates Data Configurations")
    public @interface VfxRatesDataConfig {

        @AttributeDefinition(name = "exchangeRateBasePath",
                description = "Exchange Rate CF Path", type = AttributeType.STRING)
        String getExchangeRateBasePath() default "/content/dam/techcombank/master-data/exchange-rates";

        @AttributeDefinition(name = "goldRateBasePath",
                description = "Gold Rate CF Path", type = AttributeType.STRING)
        String getGoldRateBasePath() default "/content/dam/techcombank/master-data/gold-rates";

        @AttributeDefinition(name = "otherRateBasePath",
                description = "Other Rate CF Path", type = AttributeType.STRING)
        String getOtherRateBasePath() default "/content/dam/techcombank/master-data/other-rates";

        @AttributeDefinition(name = "tenorRateBasePath",
                description = "Tenor Rate CF Path", type = AttributeType.STRING)
        String getTenorRateBasePath() default "/content/dam/techcombank/master-data/tenor-rates";

        @AttributeDefinition(name = "tenorIntRateBasePath",
                description = "Tenor Int Rate CF Path", type = AttributeType.STRING)
        String getTenorIntRateBasePath() default "/content/dam/techcombank/master-data/tenor-int-rates";

        @AttributeDefinition(name = "fixingRateBasePath",
                description = "Fixing Rate CF Path", type = AttributeType.STRING)
        String getFixingRateBasePath() default "/content/dam/techcombank/master-data/fixing-rates";

        @AttributeDefinition(name = "targetCurrency",
                description = "Target Currency Value", type = AttributeType.STRING)
        String getTargetCurrency() default "VND";

        @AttributeDefinition(name = "countryFlag",
                description = "Country Flag Path", type = AttributeType.STRING)
        String getCountryFlagPath() default "/etc/acs-commons/lists/ExchangRateFlags";

        @AttributeDefinition(name = "countryFlagIcon",
                description = "Country Flag Icon Base Path", type = AttributeType.STRING)
        String getCountryFlagIconBasePath() default "/content/dam/techcombank/flags/";

        // VFX Rates Creation Config Start
        @AttributeDefinition(name = "exchangeRateModalPath",
                description = "Exchange Rate CF Modal Path", type = AttributeType.STRING)
        String getExchangeRateModalPath()
        default "/conf/techcombank/web/settings/dam/cfm/models/exchange-rate-fragment";

        @AttributeDefinition(name = "tenorIntRateModalPath",
                description = "Tenor Int Rate CF Modal Path", type = AttributeType.STRING)
        String getTenorIntRateModalPath()
        default "/conf/techcombank/web/settings/dam/cfm/models/tenor-int-rate-fragment";

        @AttributeDefinition(name = "tenorRateModalPath",
                description = "Tenor Rate CF Modal Path", type = AttributeType.STRING)
        String getTenorRateModalPath()
        default "/conf/techcombank/web/settings/dam/cfm/models/tenor-rate-fragment";

        @AttributeDefinition(name = "goldRateModalPath",
                description = "Gold Rate CF Modal Path", type = AttributeType.STRING)
        String getGoldRateModalPath()
        default "/conf/techcombank/web/settings/dam/cfm/models/gold-rate-fragment";

        @AttributeDefinition(name = "otherRateModalPath",
                description = "Other Rate CF Modal Path", type = AttributeType.STRING)
        String getOtherRateModalPath()
        default "/conf/techcombank/web/settings/dam/cfm/models/other-rate-fragment";

        // Exchange Rate variable json Mapping

        @AttributeDefinition(name = "exchangeRateJsonName",
                description = "Exchange Rate Json Name", type = AttributeType.STRING)
        String getExchangeRateJsonName() default "spot_rate";

        @AttributeDefinition(name = "exchangeRateLabelJsonName",
                description = "Exchange Rate Json Label Name", type = AttributeType.STRING)
        String getExchangeRateLabelJsonName() default LABEL;

        @AttributeDefinition(name = "exchangeRatePairCurJsonName",
                description = "Exchange Rate Json PairCur Name", type = AttributeType.STRING)
        String getExchangeRatePairCurJsonName() default "pair_cur_val";

        @AttributeDefinition(name = "exchangeRateJsonBitRateTMName",
                description = "Exchange Rate Json Bid RateTM Name", type = AttributeType.STRING)
        String getExchangeRateJsonBitRateTmName() default "bid_rate_tm";

        @AttributeDefinition(name = "exchangeRateJsonAskRateTmName",
                description = "Exchange Rate Json Ask RateTM Name", type = AttributeType.STRING)
        String getExchangeRateJsonAskRateTmName() default "ask_rate_tm";

        @AttributeDefinition(name = "exchangeRateBidRateCkJsonName",
                description = "Exchange Rate Json Bid RateCK Name", type = AttributeType.STRING)
        String getExchangeRateBidRateCkJsonName() default "bid_rate_ck";

        @AttributeDefinition(name = "exchangeRateAskRateJsonName",
                description = "Exchange Rate Json Ask Rate Name", type = AttributeType.STRING)
        String getExchangeRateAskRateJsonName() default ASK_RATE_VFX;

        // Tenor Rate variable json Mapping

        @AttributeDefinition(name = "tenorRateJsonName",
                description = "Tenor Rate Json Name", type = AttributeType.STRING)
        String getTenorRateJsonName() default "tenor_rate";

        @AttributeDefinition(name = "tenorRatePairCurJsonName",
                description = "Tenor Rate Json PairCur Name", type = AttributeType.STRING)
        String getTenorRatePairCurJsonName() default "pair_cur_val";

        @AttributeDefinition(name = "tenorRateBidRateJsonName",
                description = "Tenor Rate Json Bit Rate Name", type = AttributeType.STRING)
        String getTenorRateBidRateJsonName() default "bid_rate";

        @AttributeDefinition(name = "TenorRateAskRateJsonName",
                description = "Tenor Rate Json Ask Rate Name", type = AttributeType.STRING)
        String getTenorRateAskRateJsonName() default ASK_RATE_VFX;

        @AttributeDefinition(name = "tenorRateTenorJsonName",
                description = "Tenor Rate Json Tenor Name", type = AttributeType.STRING)
        String getTenorRateTenorJsonName() default TENOR;

        // Tenor Int Rate variable json Mapping

        @AttributeDefinition(name = "tenorIntRateJsonName",
                description = "Tenor Int Rate Json Name", type = AttributeType.STRING)
        String getTenorIntRateJsonName() default "tenor_int_rate";

        @AttributeDefinition(name = "tenorIntRateRateCDJsonName",
                description = "Tenor Int Rate Json Ratecd Name", type = AttributeType.STRING)
        String getTenorIntRateRateCDJsonName() default "rate_cd";

        @AttributeDefinition(name = "tenorIntRateRateLbJsonName",
                description = "Tenor Int Rate Json Ratelb Name", type = AttributeType.STRING)
        String getTenorIntRateRateLbJsonName() default "rate_lb";

        @AttributeDefinition(name = "tenorIntRateTenorJsonName",
                description = "Tenor Int Rate Json Tenor Name", type = AttributeType.STRING)
        String getTenorIntRateTenorJsonName() default TENOR;

        @AttributeDefinition(name = "tenorIntRateTenorIntJsonName",
                description = "Tenor Int Rate Json TenorInt Name", type = AttributeType.STRING)
        String getTenorIntRateTenorIntJsonName() default "tenor_int";

        // Gold Rate variable json Mapping

        @AttributeDefinition(name = "goldRateJsonName",
                description = "Gold Rate Json Name", type = AttributeType.STRING)
        String getGoldRateJsonName() default "gold_rate";

        @AttributeDefinition(name = "goldRateBidRateJsonName",
                description = "Gold Rate Json Bid Rate Name", type = AttributeType.STRING)
        String getGoldRateBidRateJsonName() default "bid_rate";

        @AttributeDefinition(name = "goldRateAskRateJsonName",
                description = "Gold Rate Json Ask Rate Name", type = AttributeType.STRING)
        String getGoldRateAskRateJsonName() default ASK_RATE_VFX;

        @AttributeDefinition(name = "goldRateLabelJsonName",
                description = "Gold Rate Json Label Name", type = AttributeType.STRING)
        String getGoldRateLabelJsonName() default LABEL;

        // Other Rate variable json Mapping

        @AttributeDefinition(name = "otherRateJsonName",
                description = "Other Rate Json Name", type = AttributeType.STRING)
        String getOtherRateJsonName() default "other_rate";

        @AttributeDefinition(name = "otherRateCentralRateJsonName",
                description = "Other Rate Json Central Rate Name", type = AttributeType.STRING)
        String getOtherRateCentralRateJsonName() default "central_rate";

        @AttributeDefinition(name = "otherRateFlrRateJsonName",
                description = "Other Rate Json Flr Rate Name", type = AttributeType.STRING)
        String getOtherRateFlrRateJsonName() default "flr_rate";

        @AttributeDefinition(name = "otherRateCelRateJsonName",
                description = "Other Rate Json Cel Rate Name", type = AttributeType.STRING)
        String getOtherRateCelRateJsonName() default "cel_rate";

        @AttributeDefinition(name = "vfxDateJsonName",
                description = "VFX Date Json Name", type = AttributeType.STRING)
        String getVfxDateJsonName() default "input_date";

        // Vfx Rates Creation Config End
    }

    private VfxRatesDataConfig vfxRatesDataConfig;
    private static final String ITEM_ID = "itemId";

    private static final String LABEL = "label";

    private static final String SOURCE_CURRENCY = "sourceCurrency";

    private static final String BIT_RATE_TM = "bidRateTM";

    private static final String ASK_RATE_TM = "askRateTM";

    private static final String BIT_RATE_CK = "bidRateCK";

    private static final String ASK_RATE = "askRate";

    private static final String RATE_CD = "rateCD";

    private static final String RATE_LB = "rateLB";

    private static final String TENOR = "tenor";

    private static final String TENOR_INT = "tenorInt";

    private static final String BID_RATE = "bidRate";

    private static final String CENTRAL = "central";

    private static final String FLOOR = "floor";

    private static final String CEILING = "ceiling";

    private static final String ASK_RATE_VFX = "ask_rate";

    private static final int TEN_THOUSAND = 10000;

    private static final int TWENTY_THOUSAND = 2000;

    private static final int DATE_LENGTH = 3;

    private static final String YEAR = "year";

    private static final String DAY = "month";

    private static final String MONTH = "day";

    private static final String TIME = "time";

    @Reference
    private Replicator replicator;

    private final Logger log = LoggerFactory.getLogger(VfxRatesDataServiceImpl.class);

    @Activate
    public void activate(final VfxRatesDataConfig config) {
        this.vfxRatesDataConfig = config;
    }

    /**
     * This method returns different vfx rate resource based on date and time
     * @param resolver
     * @param vfxRateDateTimePath
     * @param vfxRateBasePath
     * @return Resource vfxRatePath
     */
    public Resource getVfxRate(final ResourceResolver resolver, final String vfxRateDateTimePath,
            final String vfxRateBasePath) {

        String vfxRatePath = vfxRateBasePath + TCBConstants.SLASH + vfxRateDateTimePath;
        return resolver.getResource(vfxRatePath);
    }

    /**
     * This method returns vfx rate TimeStamp List for data passed
     * @param resolver
     * @param vfxRateDatePath
     * @param vfxRateBasePath
     * @return List vfxRateTimeStamp
     */
    public List<String> getVfxRateTimeStamp(final ResourceResolver resolver, final String vfxRateDatePath,
            final String vfxRateBasePath) {
        List<String> vfxRateTimeStamp = new ArrayList<>();
        String vfxRatePath = vfxRateBasePath + TCBConstants.SLASH + vfxRateDatePath;
        Resource rate = null;
        rate = resolver.getResource(vfxRatePath);
        if (null != rate) {
            for (Resource exchangeRate : (Iterable<? extends Resource>) vfxRateList(rate)::iterator) {
                Resource data = resolver.getResource(exchangeRate.getPath());
                if (null != data) {
                    vfxRateTimeStamp.add(data.getName().replace(TCBConstants.DASH, TCBConstants.COLON));
                }
            }
        }
        vfxRateTimeStamp = vfxRateTimeStamp.stream()
                .sorted(Comparator.comparingInt(this::getFormattedVfxRateTime).reversed()).collect(Collectors.toList());
        return vfxRateTimeStamp;
    }

    /**
     * This method is used to replace the special character ":" with empty in the
     * timestamp.This is called to sort the time value from latest to oldest.
     * @param timeStamp
     * @return Integer
     */
    protected Integer getFormattedVfxRateTime(@NotNull final String timeStamp) {
        return Integer.valueOf(timeStamp.replace(TCBConstants.COLON, StringUtils.EMPTY));
    }

    /**
     * This method returns Exchange rate Data Json
     * @param exchangeRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject exchangeRateJson
     */
    public JsonObject getExchangeRateData(final Resource exchangeRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        List<ExchangeRate> exchangeRateDataList = new ArrayList<>();
        JsonObject exchangeRateJson = new JsonObject();
        Gson gsonObj = new Gson();
        if (null != exchangeRates) {
            for (Resource exchangeRate : (Iterable<? extends Resource>) vfxRateList(exchangeRates)::iterator) {
                Resource data = resolver.getResource(exchangeRate.getPath() + TCBConstants.SLASH
                        + JcrConstants.JCR_CONTENT + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    ExchangeRate exchangeRateData = data.adaptTo(ExchangeRate.class);
                    exchangeRateData.setTargetCurrency(vfxRatesDataConfig.getTargetCurrency());
                    exchangeRateDataList.add(exchangeRateData);
                }
            }
        }

        exchangeRateDataList.sort(Comparator.comparing(ExchangeRate::getLabel));
        JsonElement exchangeRateDataElement = gsonObj.toJsonTree(exchangeRateDataList);
        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        exchangeRateJson.add(TCBConstants.VFX_RATE_DATA, exchangeRateDataElement);
        exchangeRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return exchangeRateJson;
    }

    /**
     * This method returns Gold rate Data Json
     * @param goldRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject goldRateJson
     */
    public JsonObject getGoldRateData(final Resource goldRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        JsonObject goldRateJson = new JsonObject();
        List<GoldRate> goldRateDataList = new ArrayList<>();
        Gson gsonObj = new Gson();
        if (null != goldRates) {
            for (Resource goldRate : (Iterable<? extends Resource>) vfxRateList(goldRates)::iterator) {
                Resource data = resolver.getResource(
                        goldRate.getPath() + TCBConstants.SLASH + JcrConstants.JCR_CONTENT + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    GoldRate goldRateData = data.adaptTo(GoldRate.class);
                    goldRateDataList.add(goldRateData);
                }
            }
        }

        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        JsonElement goldRateDataElement = gsonObj.toJsonTree(goldRateDataList);
        goldRateJson.add(TCBConstants.VFX_RATE_DATA, goldRateDataElement);
        goldRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return goldRateJson;
    }

    /**
     * This method returns Other rate Data Json
     * @param otherRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject otherRateJson
     */
    public JsonObject getOtherRateData(final Resource otherRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        JsonObject otherRateJson = new JsonObject();
        List<OtherRate> otherRateDataList = new ArrayList<>();
        Gson gsonObj = new Gson();
        if (null != otherRates) {
            for (Resource otherRate : (Iterable<? extends Resource>) vfxRateList(otherRates)::iterator) {
                Resource data = resolver.getResource(otherRate.getPath() + TCBConstants.SLASH + JcrConstants.JCR_CONTENT
                        + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    OtherRate otherRateData = data.adaptTo(OtherRate.class);
                    otherRateDataList.add(otherRateData);
                }
            }
        }
        JsonElement otherRateDataElement = gsonObj.toJsonTree(otherRateDataList);
        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        otherRateJson.add(TCBConstants.VFX_RATE_DATA, otherRateDataElement);
        otherRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return otherRateJson;
    }

    /**
     * This method returns Tenor rate Data Json
     * @param tenorRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject tenorRateJson
     */
    public JsonObject getTenorRateData(final Resource tenorRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        List<TenorRate> tenorRateDataList = new ArrayList<>();
        JsonObject tenorRateJson = new JsonObject();
        Gson gsonObj = new Gson();
        if (null != tenorRates) {
            for (Resource tenorRate : (Iterable<? extends Resource>) vfxRateList(tenorRates)::iterator) {
                Resource data = resolver.getResource(tenorRate.getPath() + TCBConstants.SLASH + JcrConstants.JCR_CONTENT
                        + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    TenorRate tenorRateData = data.adaptTo(TenorRate.class);
                    tenorRateData.setTargetCurrency(vfxRatesDataConfig.getTargetCurrency());
                    tenorRateDataList.add(tenorRateData);
                }
            }
        }
        JsonElement tenorRateDataElement = gsonObj.toJsonTree(tenorRateDataList);
        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        tenorRateJson.add(TCBConstants.VFX_RATE_DATA, tenorRateDataElement);
        tenorRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return tenorRateJson;
    }

    /**
     * This method returns Tenor Int rate Data
     * @param tenorIntRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject tenorIntRateJson
     */
    public JsonObject getTenorIntRateData(final Resource tenorIntRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        List<TenorIntRate> tenorIntRateDataList = new ArrayList<>();
        JsonObject tenorIntRateJson = new JsonObject();
        Gson gsonObj = new Gson();
        if (null != tenorIntRates) {
            for (Resource tenorIntRate : (Iterable<? extends Resource>) vfxRateList(tenorIntRates)::iterator) {
                Resource data = resolver.getResource(tenorIntRate.getPath() + TCBConstants.SLASH
                        + JcrConstants.JCR_CONTENT + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    TenorIntRate tenorIntRateData = data.adaptTo(TenorIntRate.class);
                    tenorIntRateDataList.add(tenorIntRateData);
                }
            }
        }
        JsonElement tenorIntRateDataElement = gsonObj.toJsonTree(tenorIntRateDataList);
        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        tenorIntRateJson.add(TCBConstants.VFX_RATE_DATA, tenorIntRateDataElement);
        tenorIntRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return tenorIntRateJson;
    }

    /**
     * This method returns fixing rate Data Json
     * @param fixingRates
     * @param resolver
     * @param vfxRateTimeStamp
     * @return JsonObject fixingRateJson
     */
    public JsonObject getFixingRateData(final Resource fixingRates, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp) {
        List<FixingRate> fixingRateDataList = new ArrayList<>();
        JsonObject fixingRateJson = new JsonObject();
        Gson gsonObj = new Gson();
        if (null != fixingRates) {
            for (Resource fixingRate : (Iterable<? extends Resource>) vfxRateList(fixingRates)::iterator) {
                Resource data = resolver.getResource(fixingRate.getPath() + TCBConstants.SLASH
                        + JcrConstants.JCR_CONTENT + TCBConstants.CF_DATA_PATH);
                if (null != data) {
                    FixingRate fixingRateData = data.adaptTo(FixingRate.class);
                    if (StringUtils.isBlank(fixingRateData.getInputTime())) {
                        fixingRateData.setInputTime(fixingRate.getParent()
                                .getName().replace(TCBConstants.DASH, TCBConstants.COLON));
                    }
                    fixingRateDataList.add(fixingRateData);
                }
            }
        }
        JsonElement fixingRateDataElement = gsonObj.toJsonTree(fixingRateDataList);
        JsonElement vfxRateTimeStampElement = gsonObj.toJsonTree(vfxRateTimeStamp);
        fixingRateJson.add(TCBConstants.VFX_RATE_DATA, fixingRateDataElement);
        fixingRateJson.add(TCBConstants.RATE_TIMESTAMP_JSON_ARRAY, vfxRateTimeStampElement);
        return fixingRateJson;
    }

    /**
     * This method returns Vfx rate Data Json based on the type passed
     * @param vfxRate
     * @param resolver
     * @param vfxRateTimeStamp
     * @param vfxRateType
     * @return JsonObject vfxRateJson
     */
    public JsonObject getVfxRateData(final Resource vfxRate, final ResourceResolver resolver,
            final List<String> vfxRateTimeStamp, final String vfxRateType) {
        JsonObject vfxRateJson = new JsonObject();
        if (TCBConstants.EXCHANGE_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getExchangeRateData(vfxRate, resolver, vfxRateTimeStamp);
        } else if (TCBConstants.GOLD_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getGoldRateData(vfxRate, resolver, vfxRateTimeStamp);
        } else if (TCBConstants.OTHER_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getOtherRateData(vfxRate, resolver, vfxRateTimeStamp);
        } else if (TCBConstants.TENOR_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getTenorRateData(vfxRate, resolver, vfxRateTimeStamp);
        } else if (TCBConstants.TENOR_INT_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getTenorIntRateData(vfxRate, resolver, vfxRateTimeStamp);
        } else if (TCBConstants.FIXING_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateJson = getFixingRateData(vfxRate, resolver, vfxRateTimeStamp);
        }
        return vfxRateJson;
    }

    /**
     * This method returns list of vfx Rate data resource
     * @param vfxRateResource
     * @return stream of vfxRateDataResource
     */
    private Stream<Resource> vfxRateList(@NotNull final Resource vfxRateResource) {
        return StreamSupport.stream(vfxRateResource.getChildren().spliterator(), false).filter(this::filterVfxRate);
    }

    /**
     * This method is used to filter the vfx resource that is not ending with
     * jcr:content and rep:policy
     * @param vfxRateresource
     * @return boolean true/false
     */
    protected boolean filterVfxRate(@NotNull final Resource vfxRateresource) {
        return !(vfxRateresource.getPath().endsWith(JcrConstants.JCR_CONTENT)
                || vfxRateresource.getPath().endsWith("rep:policy"));
    }

    /**
     * This method get vfx rate updated date
     * @param resolver
     * @param vfxRateBasePath
     * @return String vfxUpdatedDate
     */
    public String getVfxRateUpdateDate(final ResourceResolver resolver, final String vfxRateBasePath) {
        String vfxUpdatedDate = StringUtils.EMPTY;
        Resource rateResource = resolver.getResource(vfxRateBasePath);
        if (null != rateResource && null != rateResource.getValueMap()
                && null != rateResource.getValueMap().get(TCBConstants.VFX_RATE_LATEST_UPDATED_DATE)) {
            vfxUpdatedDate = rateResource.getValueMap().get(TCBConstants.VFX_RATE_LATEST_UPDATED_DATE).toString();
        }
        return vfxUpdatedDate;
    }

    /**
     * This method get the vfx rates base path
     * @param vfxRateType
     * @param addAllVfxRates
     * @return String rateBasepath
     */
    public List<String> getVfxRateBasePath(final String vfxRateType, final boolean addAllVfxRates) {
        List<String> vfxRateBasePath = new ArrayList<>();
        if (TCBConstants.EXCHANGE_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getExchangeRateBasePath());
            if (addAllVfxRates) {
                vfxRateBasePath.add(vfxRatesDataConfig.getGoldRateBasePath());
                vfxRateBasePath.add(vfxRatesDataConfig.getOtherRateBasePath());
                vfxRateBasePath.add(vfxRatesDataConfig.getTenorRateBasePath());
                vfxRateBasePath.add(vfxRatesDataConfig.getTenorIntRateBasePath());
            }
        } else if (TCBConstants.GOLD_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getGoldRateBasePath());
        } else if (TCBConstants.OTHER_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getOtherRateBasePath());
        } else if (TCBConstants.TENOR_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getTenorRateBasePath());
        } else if (TCBConstants.TENOR_INT_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getTenorIntRateBasePath());
        } else if (TCBConstants.FIXING_RATE.equalsIgnoreCase(vfxRateType)) {
            vfxRateBasePath.add(vfxRatesDataConfig.getFixingRateBasePath());
        }

        return vfxRateBasePath;
    }

    /**
     * This method returns Exchange rate Data
     * @param latestExchangeRate
     * @param resolver
     * @return List exchangeRateDataList
     */
    public List<ExchangeRate> getExchangeRateDataList(final Resource latestExchangeRate,
            final ResourceResolver resolver) {
        List<ExchangeRate> exchangeRateDataList = new ArrayList<>();
        PageManager pageManager = resolver.adaptTo(PageManager.class);
        Page flagListPage = pageManager.getPage(vfxRatesDataConfig.getCountryFlagPath());
        GenericList flagList = null;
        if (null != flagListPage) {
            flagList = flagListPage.adaptTo(GenericList.class);
        }
        if (null != latestExchangeRate) {
            for (Resource exchangeRate : (Iterable<? extends Resource>) vfxRateList(latestExchangeRate)::iterator) {
                Resource data = resolver.getResource(exchangeRate.getPath() + TCBConstants.SLASH
                        + JcrConstants.JCR_CONTENT + TCBConstants.CF_DATA_PATH);
                updateFlagExchangeRate(exchangeRateDataList, flagList, data);
            }
        }
        return exchangeRateDataList;
    }

    /**
     * This Method Add the Flags for the currency and calls the
     * formattedExchangeRate to format the currency
     * @param flagList
     * @param exchangeRateDataList
     * @param data
     */
    public void updateFlagExchangeRate(final List<ExchangeRate> exchangeRateDataList, final GenericList flagList,
            final Resource data) {
        if (null != data) {
            ExchangeRate exchangeRateData = data.adaptTo(ExchangeRate.class);
            if (null != exchangeRateData.getLabel() && null != flagList) {
                exchangeRateData.setFlag(vfxRatesDataConfig.getCountryFlagIconBasePath()
                        + flagList.lookupTitle(exchangeRateData.getLabel()));
            }
            if (null != exchangeRateData.getBidRateCK()) {
                exchangeRateData.setBidRateCK(formattedVfxRate(exchangeRateData.getBidRateCK()));
            }
            if (null != exchangeRateData.getAskRate()) {
                exchangeRateData.setAskRate(formattedVfxRate(exchangeRateData.getAskRate()));
            }

            if (null != exchangeRateData.getBidRateTM()) {
                exchangeRateData.setBidRateTM(formattedVfxRate(exchangeRateData.getBidRateTM()));
            }
            if (null != exchangeRateData.getAskRateTM()) {
                exchangeRateData.setAskRateTM(formattedVfxRate(exchangeRateData.getAskRateTM()));
            }

            exchangeRateDataList.add(exchangeRateData);
        }

    }

    /**
     * This Method formats vfx Rates with comma separated
     * @param vfxRateVal
     * @return String vfxRateVal
     */
    public String formattedVfxRate(final String vfxRateVal) {
        if (!vfxRateVal.contains(TCBConstants.DOT)) {
            Double numParsed = Double.parseDouble(vfxRateVal);
            return String.format(TCBConstants.RATE_CURRENCY_FORMAT, numParsed);
        }
        return vfxRateVal;
    }

    // Start: VFX CF Data Creation

    /**
     * This method create VFX Data for different rates and replicate it.
     * @param resolver
     * @param vfxData
     * @return boolean true if creation and replication is success
     */
    @Override
    public boolean createVfxRateData(final ResourceResolver resolver, final JsonObject vfxData) {
        Map<String, String> previousUpdatedData = getPreviousRateUpdatedDate(resolver);
        String[] vfxRatedate = null;
        String vfxRatetime = StringUtils.EMPTY;
        boolean isVFXrateDataCreated = false;
        String vfxId = StringUtils.EMPTY;
        List<String> vfxRateCFPathList = new ArrayList<>();
        try {
            vfxId = vfxData.get(TCBConstants.VFX_RATE_ID).getAsString();
            log.info("VFX Rate content creation started: {}", vfxId);
            if (null != vfxData.get(vfxRatesDataConfig.getVfxDateJsonName())) {
                String dateId = vfxData.get(vfxRatesDataConfig.getVfxDateJsonName())
                        .getAsString(); // 2022-07-09T07:14:42.716Z
                String[] dateTime = getFormattedDateTime(dateId).split(" ");
                if (dateTime.length >= 2) {
                    vfxRatedate = dateTime[0].split(TCBConstants.DASH);
                    vfxRatetime = dateTime[1].replace(TCBConstants.COLON, TCBConstants.DASH);
                    createVFXCFModelData(vfxData, resolver, vfxRatedate, vfxRatetime, vfxId, vfxRateCFPathList);
                    replicateVFXData(resolver, vfxRateCFPathList);
                }

                isVFXrateDataCreated = true;
            }

        } catch (ContentFragmentException | IOException | ReplicationException e) {
            log.error("VFX Rate content creation failure {} {} {}", vfxId, e.getMessage(), e);
            deleteVfxData(resolver, vfxRatedate, vfxRatetime, vfxId);
            updateVFXPreviousDate(resolver, previousUpdatedData, vfxId);
            isVFXrateDataCreated = false;
        }
        log.info("VFX Rate content creation completed: {}", vfxId);
        return isVFXrateDataCreated;
    }

    /**
     * This method format the passed date string and return date and time stamp
     * @param dateId
     * @return String dateTime
     */
    private String getFormattedDateTime(final String dateId) {
        Instant instant = Instant.parse(dateId);
        LocalDateTime localDateTime = instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(formatter);
    }

    /**
     * This method Replicate the VFX base path and newly created VFX Rate content
     * fragment time path
     * @param resolver
     * @param vfxRateCFPathList
     * @throws ReplicationException
     */
    private void replicateVFXData(final ResourceResolver resolver, final List<String> vfxRateCFPathList)
            throws ReplicationException {
        log.info("VFX Rate replication process started");
        Session session = resolver.adaptTo(Session.class);
        ReplicationOptions opts = new ReplicationOptions();
        String[] vfxRateCFPathArray = new String[vfxRateCFPathList.size()];
        vfxRateCFPathArray = vfxRateCFPathList.toArray(vfxRateCFPathArray);
        replicator.replicate(session, ReplicationActionType.ACTIVATE, vfxRateCFPathArray, opts);
        log.info("VFX Rate replication process completed");
    }

    /**
     * This method returns the previous rate updated date for all VFX rate. This
     * date will be used to replace the new updated date in case of new VFX rate
     * content fragment creation failure
     * @param resolver
     * @return Map previousRateUpdatedTime
     */
    private Map<String, String> getPreviousRateUpdatedDate(final ResourceResolver resolver) {
        Map<String, String> previousRateUpdatedTime = new HashMap<>();
        previousRateUpdatedTime.put(TCBConstants.EXCHANGE_RATE,
                getVfxRateUpdateDate(resolver, vfxRatesDataConfig.getExchangeRateBasePath()));
        previousRateUpdatedTime.put(TCBConstants.GOLD_RATE,
                getVfxRateUpdateDate(resolver, vfxRatesDataConfig.getGoldRateBasePath()));
        previousRateUpdatedTime.put(TCBConstants.TENOR_INT_RATE,
                getVfxRateUpdateDate(resolver, vfxRatesDataConfig.getTenorIntRateBasePath()));
        previousRateUpdatedTime.put(TCBConstants.TENOR_RATE,
                getVfxRateUpdateDate(resolver, vfxRatesDataConfig.getTenorRateBasePath()));
        previousRateUpdatedTime.put(TCBConstants.OTHER_RATE,
                getVfxRateUpdateDate(resolver, vfxRatesDataConfig.getOtherRateBasePath()));
        return previousRateUpdatedTime;

    }

    /**
     * This method check for the VFX rate type and updated the previous updated date
     * in case of case of new VFX rate content fragment creation failure
     * @param resolver
     * @param previousUpdatedData
     * @param vfxId
     */
    private void updateVFXPreviousDate(final ResourceResolver resolver, final Map<String, String> previousUpdatedData,
            final String vfxId) {
        log.info("VFX Rate updating to previous updated date process started: {}", vfxId);
        List<String> vfxRateBasePath = getVfxRateBasePath(TCBConstants.EXCHANGE_RATE, true);
        String previousUpdatedDate = StringUtils.EMPTY;
        for (String basePath : vfxRateBasePath) {
            if (basePath.contains(TCBConstants.EXCHANGE_RATE)) {
                previousUpdatedDate = previousUpdatedData.get(TCBConstants.EXCHANGE_RATE);
            } else if (basePath.contains(TCBConstants.GOLD_RATE)) {
                previousUpdatedDate = previousUpdatedData.get(TCBConstants.GOLD_RATE);
            } else if (basePath.contains(TCBConstants.TENOR_INT_RATE)) {
                previousUpdatedDate = previousUpdatedData.get(TCBConstants.TENOR_INT_RATE);
            } else if (basePath.contains(TCBConstants.TENOR_RATE)) {
                previousUpdatedDate = previousUpdatedData.get(TCBConstants.TENOR_RATE);
            } else if (basePath.contains(TCBConstants.OTHER_RATE)) {
                previousUpdatedDate = previousUpdatedData.get(TCBConstants.OTHER_RATE);
            }
            updateVFXUpdatedDate(resolver, basePath, previousUpdatedDate, vfxId);
        }
        log.info("VFX Rate updating to previous updated date process completed: {}", vfxId);
    }

    /**
     * This method updated the "latestUpdatedDate" field in VFX base path for all
     * VFX Rates
     * @param resolver
     * @param vfxRateBasePath
     * @param vfxRateUpdatedDate
     * @param vfxId
     */
    private void updateVFXUpdatedDate(final ResourceResolver resolver, final String vfxRateBasePath,
                                      final String vfxRateUpdatedDate, final String vfxId) {
        Resource folderResource = resolver.getResource(vfxRateBasePath);
        try {
            if (null != folderResource) {
                ModifiableValueMap valueMap = folderResource.adaptTo(ModifiableValueMap.class);
                if (valueMap != null) {
                    valueMap.put(TCBConstants.VFX_RATE_LATEST_UPDATED_DATE, vfxRateUpdatedDate);
                    resolver.commit();
                }
            }
        } catch (PersistenceException e) {
            log.error("Error while Updating Vfx Rate Time stamp : {} {} {}!", vfxId, e.getMessage(), e);
        }
    }

    /**
     * This method deletes the VFX rate time path in case of any error occurred
     * while creating/replication new VFX rates content fragments
     * @param resolver
     * @param vfxRatedate
     * @param vfxRatetime
     * @param vfxId
     */
    private void deleteVfxData(final ResourceResolver resolver, final String[] vfxRatedate, final String vfxRatetime,
            final String vfxId) {
        log.info("VFX Rate folder time stamp deletion started: {}", vfxId);
        if (null != vfxRatedate && null != vfxRatetime && vfxRatedate.length >= DATE_LENGTH) {
            String year = vfxRatedate[0];
            String month = vfxRatedate[1];
            String day = vfxRatedate[2];
            List<String> vfxRateBasePath = getVfxRateBasePath(TCBConstants.EXCHANGE_RATE, true);
            String vfxRateTimePath = year + TCBConstants.SLASH + month + TCBConstants.SLASH + day + TCBConstants.SLASH
                    + vfxRatetime;
            for (String basePath : vfxRateBasePath) {
                String vfxTimePath = basePath + TCBConstants.SLASH + vfxRateTimePath;
                deleteFolder(resolver, vfxTimePath, vfxId);
            }

        }
        log.info("VFX Rate folder time stamp deletion completed: {}", vfxId);
    }

    /**
     * This method creates all VFX rate content fragment model and populate the VFX
     * rates data
     * @param vfxData
     * @param resolver
     * @param vfxRatedate
     * @param vfxRatetime
     * @param vfxId
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createVFXCFModelData(final JsonObject vfxData, final ResourceResolver resolver,
                                      final String[] vfxRatedate, final String vfxRatetime, final String vfxId,
                                      final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        String basePath;
        if (null != vfxRatedate && null != vfxRatetime && vfxRatedate.length >= DATE_LENGTH) {
            Map<String, String> dateTimeMap = new HashMap<>();
            dateTimeMap.put(YEAR, vfxRatedate[0]);
            dateTimeMap.put(MONTH, vfxRatedate[1]);
            dateTimeMap.put(DAY, vfxRatedate[2]);
            dateTimeMap.put(TIME, vfxRatetime);
            String dataPath;
            boolean createTimeFolder = true;
            String vfxRateUpdatedDate = dateTimeMap.get(YEAR) + TCBConstants.DASH + dateTimeMap.get(MONTH)
                    + TCBConstants.DASH + dateTimeMap.get(DAY) + " "
                    + dateTimeMap.get(TIME).replace(TCBConstants.DASH, TCBConstants.COLON);
            if (null != vfxData.get(vfxRatesDataConfig.getExchangeRateJsonName())) {
                log.info("VFX Exchange Rate data creation started: {}", vfxId);
                basePath = vfxRatesDataConfig.getExchangeRateBasePath();
                vfxRateCFPathList.add(basePath);
                JsonArray exchangeRates = vfxData.get(vfxRatesDataConfig.getExchangeRateJsonName()).getAsJsonArray();
                createTimeFolder = !exchangeRates.isEmpty();
                createRatefolder(dateTimeMap, basePath, resolver, vfxRateUpdatedDate, createTimeFolder);
                dataPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR) + TCBConstants.SLASH
                        + dateTimeMap.get(MONTH) + TCBConstants.SLASH + dateTimeMap.get(DAY) + TCBConstants.SLASH
                        + dateTimeMap.get(TIME);
                createExchangeRateData(exchangeRates, resolver, dataPath, createTimeFolder, vfxRateCFPathList);
                updateVFXUpdatedDate(resolver, basePath, vfxRateUpdatedDate, vfxId);
                log.info("VFX Exchange Rate data creation completed: {}", vfxId);
            }

            if (null != vfxData.get(vfxRatesDataConfig.getTenorRateJsonName())) {
                log.info("VFX Tenor Rate data creation started: {}", vfxId);
                basePath = vfxRatesDataConfig.getTenorRateBasePath();
                vfxRateCFPathList.add(basePath);
                JsonArray tenorRates = vfxData.get(vfxRatesDataConfig.getTenorRateJsonName()).getAsJsonArray();
                createTimeFolder = !tenorRates.isEmpty();
                createRatefolder(dateTimeMap, basePath, resolver, vfxRateUpdatedDate, createTimeFolder);
                dataPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR) + TCBConstants.SLASH
                        + dateTimeMap.get(MONTH) + TCBConstants.SLASH + dateTimeMap.get(DAY) + TCBConstants.SLASH
                        + dateTimeMap.get(TIME);
                createTenorRateData(tenorRates, resolver, dataPath, createTimeFolder, vfxRateCFPathList);
                updateVFXUpdatedDate(resolver, basePath, vfxRateUpdatedDate, vfxId);
                log.info("VFX Tenor Rate data creation completed: {}", vfxId);
            }

            if (null != vfxData.get(vfxRatesDataConfig.getGoldRateJsonName())) {
                log.info("VFX Gold Rate data creation started: {}", vfxId);
                basePath = vfxRatesDataConfig.getGoldRateBasePath();
                vfxRateCFPathList.add(basePath);
                JsonObject goldRate = vfxData.get(vfxRatesDataConfig.getGoldRateJsonName()).getAsJsonObject();
                createTimeFolder = checkRatesDataSize(goldRate);
                createRatefolder(dateTimeMap, basePath, resolver, vfxRateUpdatedDate, createTimeFolder);
                dataPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR) + TCBConstants.SLASH
                        + dateTimeMap.get(MONTH) + TCBConstants.SLASH + dateTimeMap.get(DAY) + TCBConstants.SLASH
                        + dateTimeMap.get(TIME);
                createGoldRateData(goldRate, resolver, dataPath, createTimeFolder, vfxRateCFPathList);
                updateVFXUpdatedDate(resolver, basePath, vfxRateUpdatedDate, vfxId);
                log.info("VFX Gold Rate data creation completed: {}", vfxId);
            }

            if (null != vfxData.get(vfxRatesDataConfig.getTenorIntRateJsonName())) {
                log.info("VFX Tenor Int Rate data creation started: {}", vfxId);
                basePath = vfxRatesDataConfig.getTenorIntRateBasePath();
                vfxRateCFPathList.add(basePath);
                JsonArray tenorIntRates = vfxData.get(vfxRatesDataConfig.getTenorIntRateJsonName()).getAsJsonArray();
                createTimeFolder = !tenorIntRates.isEmpty();
                createRatefolder(dateTimeMap, basePath, resolver, vfxRateUpdatedDate, createTimeFolder);
                dataPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR) + TCBConstants.SLASH
                        + dateTimeMap.get(MONTH) + TCBConstants.SLASH + dateTimeMap.get(DAY) + TCBConstants.SLASH
                        + dateTimeMap.get(TIME);
                createTenorIntRateData(tenorIntRates, resolver, dataPath, createTimeFolder, vfxRateCFPathList);
                updateVFXUpdatedDate(resolver, basePath, vfxRateUpdatedDate, vfxId);
                log.info("VFX Tenor Int Rate data creation completed: {}", vfxId);
            }

            if (null != vfxData.get(vfxRatesDataConfig.getOtherRateJsonName())) {
                log.info("VFX Other Rate data creation started: {}", vfxId);
                basePath = vfxRatesDataConfig.getOtherRateBasePath();
                vfxRateCFPathList.add(basePath);
                JsonObject otherRate = vfxData.get(vfxRatesDataConfig.getOtherRateJsonName()).getAsJsonObject();
                createTimeFolder = checkRatesDataSize(otherRate);
                createRatefolder(dateTimeMap, basePath, resolver, vfxRateUpdatedDate, createTimeFolder);
                dataPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR) + TCBConstants.SLASH
                        + dateTimeMap.get(MONTH) + TCBConstants.SLASH + dateTimeMap.get(DAY) + TCBConstants.SLASH
                        + dateTimeMap.get(TIME);
                createOtherRateData(otherRate, resolver, dataPath, createTimeFolder, vfxRateCFPathList);
                updateVFXUpdatedDate(resolver, basePath, vfxRateUpdatedDate, vfxId);
                log.info("VFX Other Rate data creation completed: {}", vfxId);
            }

        }
    }

    /**
     * This method checks for whether data are present for gold and other rate
     * @param rateData
     * @return boolean true or false
     */
    private boolean checkRatesDataSize(final JsonObject rateData) {
        boolean isRateDataAvailable = false;
        if (rateData.size() != 0) {
            isRateDataAvailable = true;
        }
        return isRateDataAvailable;
    }

    /**
     * This method creates the VFX Rate folder based on date and time
     * @param dateTimeMap
     * @param basePath
     * @param resolver
     * @param vfxRateUpdatedDate
     * @param createTimeFolder
     * @throws PersistenceException
     */
    private void createRatefolder(final Map<String, String> dateTimeMap, final String basePath,
            final ResourceResolver resolver, final String vfxRateUpdatedDate, final boolean createTimeFolder)
            throws PersistenceException {
        Map<String, Object> properties = new HashMap<>();
        String vfxFolderTitle = getFolderTitle(basePath);
        properties.put(TCBConstants.VFX_RATE_LATEST_UPDATED_DATE, vfxRateUpdatedDate);
        properties.put(JcrConstants.JCR_TITLE, vfxFolderTitle);
        createFolder(resolver, basePath, properties);

        String yearFolderPath = basePath + TCBConstants.SLASH + dateTimeMap.get(YEAR);
        properties.clear();
        properties.put(JcrConstants.JCR_TITLE, dateTimeMap.get(YEAR));
        createFolder(resolver, yearFolderPath, properties);

        String monthFolderPath = yearFolderPath + TCBConstants.SLASH + dateTimeMap.get(MONTH);
        properties.put(JcrConstants.JCR_TITLE, dateTimeMap.get(MONTH));
        createFolder(resolver, monthFolderPath, properties);

        String dayFolderPath = monthFolderPath + TCBConstants.SLASH + dateTimeMap.get(DAY);
        properties.put(JcrConstants.JCR_TITLE, dateTimeMap.get(DAY));
        createFolder(resolver, dayFolderPath, properties);

        if (createTimeFolder) {
            String timeFolderPath = dayFolderPath + TCBConstants.SLASH + dateTimeMap.get(TIME);
            properties.put(JcrConstants.JCR_TITLE,
                    dateTimeMap.get(TIME).replace(TCBConstants.DASH, TCBConstants.COLON));
            createFolder(resolver, timeFolderPath, properties);
        }

    }

    /**
     * This method return the title for VFX rate base folder
     * @param basePath
     * @return String baseFolderTitle
     */
    private String getFolderTitle(final String basePath) {
        String baseFolderTitle = StringUtils.EMPTY;
        if (basePath.contains(TCBConstants.EXCHANGE_RATE)) {
            baseFolderTitle = "Exchange Rates";
        }
        if (basePath.contains(TCBConstants.TENOR_INT_RATE)) {
            baseFolderTitle = "Tenor Int Rates";
        }
        if (basePath.contains(TCBConstants.TENOR_RATE)) {
            baseFolderTitle = "Tenor Rates";
        }
        if (basePath.contains(TCBConstants.GOLD_RATE)) {
            baseFolderTitle = "Gold Rates";
        }
        if (basePath.contains(TCBConstants.OTHER_RATE)) {
            baseFolderTitle = "Other Rates";
        }
        return baseFolderTitle;
    }

    /**
     * This method will check for the folder resource and create the folder if it is
     * not present
     * @param resourceResolver
     * @param resFolderPath
     * @param properties
     * @throws PersistenceException
     */
    private void createFolder(final ResourceResolver resourceResolver, final String resFolderPath,
            final Map<String, Object> properties) throws PersistenceException {
        Resource folderResource = resourceResolver.getResource(resFolderPath);
        if (folderResource == null) {
            String parentPath = resFolderPath.substring(0, resFolderPath.lastIndexOf(TCBConstants.SLASH));
            String subPath = resFolderPath.substring(resFolderPath.lastIndexOf(TCBConstants.SLASH) + 1,
                    resFolderPath.length());
            Resource parentResource = resourceResolver.getResource(parentPath);
            resourceResolver.create(parentResource, subPath, properties);
            resourceResolver.commit();
        }
    }

    /**
     * This method will check for the folder resource and delete it if the folder is
     * available for the passed path
     * @param resourceResolver
     * @param resFolderPath
     * @param vfxId
     */
    private void deleteFolder(final ResourceResolver resourceResolver, final String resFolderPath, final String vfxId) {

        try {
            Resource folderResource = resourceResolver.getResource(resFolderPath);
            if (null != folderResource) {
                resourceResolver.delete(folderResource);
                resourceResolver.commit();
            }
        } catch (PersistenceException e) {
            log.error("Exception occured when deleting CF folder {} {} {}", vfxId, e.getMessage(), e);
        }
    }

    /**
     * This method return random 5 digit ID which will be used as name and title for
     * creating VFX rates content fragment
     * @return String Id
     */
    private String getItemIdNumber() {
        Random r = new Random(System.currentTimeMillis());
        return String.valueOf(TEN_THOUSAND + r.nextInt(TWENTY_THOUSAND));
    }

    /**
     * This method creates Exchange rate data
     * @param exchangeRates
     * @param resolver
     * @param dataPath
     * @param createTimeFolder
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createExchangeRateData(final JsonArray exchangeRates, final ResourceResolver resolver,
                                        final String dataPath, final boolean createTimeFolder,
                                        final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        if (createTimeFolder) {
            Resource exchangeRateTemplateRsc = resolver.getResource(vfxRatesDataConfig.getExchangeRateModalPath());
            Resource parentRsc = resolver.getResource(dataPath);
            FragmentTemplate exchangeRateTemplate = exchangeRateTemplateRsc.adaptTo(FragmentTemplate.class);
            for (JsonElement rate : exchangeRates) {
                JsonObject exchangeRate = rate.getAsJsonObject();
                String itemId = getItemIdNumber();
                ContentFragment exchangeRateFragment = exchangeRateTemplate.createFragment(parentRsc, itemId, itemId);
                Iterator<ContentElement> exchangeRateIter = exchangeRateFragment.getElements();
                while (exchangeRateIter.hasNext()) {
                    ContentElement exchangeRateElement = exchangeRateIter.next();
                    if (StringUtils.isNotBlank(exchangeRateElement.getName())) {
                        setExchangeRateData(exchangeRateElement, itemId, exchangeRate);
                    }
                }
                vfxRateCFPathList.add(dataPath + TCBConstants.SLASH + exchangeRateFragment.getName());
            }
            resolver.commit();
        }
    }

    /**
     * This method sets the exchange rate from the VFX request data
     * @param exchangeRateElement
     * @param itemId
     * @param exchangeRate
     * @throws ContentFragmentException
     */
    private void setExchangeRateData(final ContentElement exchangeRateElement, final String itemId,
            final JsonObject exchangeRate) throws ContentFragmentException {
        if (exchangeRateElement.getName().equalsIgnoreCase(ITEM_ID)) {
            exchangeRateElement.setContent(itemId, itemId);
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(LABEL)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRateLabelJsonName())) {
            exchangeRateElement.setContent(
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateLabelJsonName()).getAsString(),
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateLabelJsonName()).getAsString());
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(SOURCE_CURRENCY)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRatePairCurJsonName())) {
            String pairCurVal = exchangeRate.get(vfxRatesDataConfig.getExchangeRatePairCurJsonName()).getAsString();
            String[] currencyLabel = pairCurVal.split(TCBConstants.SLASH);
            exchangeRateElement.setContent(currencyLabel[0], currencyLabel[0]);
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(BIT_RATE_TM)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonBitRateTmName())) {
            exchangeRateElement.setContent(
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonBitRateTmName()).getAsString(),
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonBitRateTmName()).getAsString());
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(ASK_RATE_TM)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonAskRateTmName())) {
            exchangeRateElement.setContent(
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonAskRateTmName()).getAsString(),
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateJsonAskRateTmName()).getAsString());
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(BIT_RATE_CK)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRateBidRateCkJsonName())) {
            exchangeRateElement.setContent(
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateBidRateCkJsonName()).getAsString(),
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateBidRateCkJsonName()).getAsString());
        }
        if (exchangeRateElement.getName().equalsIgnoreCase(ASK_RATE)
                && null != exchangeRate.get(vfxRatesDataConfig.getExchangeRateAskRateJsonName())) {
            exchangeRateElement.setContent(
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateAskRateJsonName()).getAsString(),
                    exchangeRate.get(vfxRatesDataConfig.getExchangeRateAskRateJsonName()).getAsString());
        }
    }

    /**
     * This method creates TenorInt rate data
     * @param tenorIntRates
     * @param resolver
     * @param dataPath
     * @param createTimeFolder
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createTenorIntRateData(final JsonArray tenorIntRates, final ResourceResolver resolver,
                                        final String dataPath, final boolean createTimeFolder,
                                        final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        if (createTimeFolder) {
            Resource tenorIntRateTemplateRsc = resolver.getResource(vfxRatesDataConfig.getTenorIntRateModalPath());
            Resource parentRsc = resolver.getResource(dataPath);
            FragmentTemplate tenorIntRateTemplate = tenorIntRateTemplateRsc.adaptTo(FragmentTemplate.class);
            for (JsonElement rate : tenorIntRates) {
                JsonObject tenorIntRate = rate.getAsJsonObject();
                String itemId = getItemIdNumber();
                ContentFragment tenorIntRateFragment = tenorIntRateTemplate.createFragment(parentRsc, itemId, itemId);
                Iterator<ContentElement> tenorIntRateIter = tenorIntRateFragment.getElements();
                while (tenorIntRateIter.hasNext()) {
                    ContentElement tenorIntRateElement = tenorIntRateIter.next();
                    if (StringUtils.isNotBlank(tenorIntRateElement.getName())) {
                        setTenorIntRateData(tenorIntRateElement, itemId, tenorIntRate);
                    }
                }
                vfxRateCFPathList.add(dataPath + TCBConstants.SLASH + tenorIntRateFragment.getName());
            }
            resolver.commit();
        }
    }

    /**
     * This method sets the Tenor Int rate from the VFX request data
     * @param tenorIntRateElement
     * @param itemId
     * @param tenorIntRate
     * @throws ContentFragmentException
     */
    private void setTenorIntRateData(final ContentElement tenorIntRateElement, final String itemId,
            final JsonObject tenorIntRate) throws ContentFragmentException {
        if (tenorIntRateElement.getName().equalsIgnoreCase(ITEM_ID)) {
            tenorIntRateElement.setContent(itemId, itemId);
        }
        if (tenorIntRateElement.getName().equalsIgnoreCase(RATE_CD)
                && null != tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateCDJsonName())) {
            tenorIntRateElement.setContent(
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateCDJsonName()).getAsString(),
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateCDJsonName()).getAsString());
        }
        if (tenorIntRateElement.getName().equalsIgnoreCase(RATE_LB)
                && null != tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateLbJsonName())) {
            tenorIntRateElement.setContent(
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateLbJsonName()).getAsString(),
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateRateLbJsonName()).getAsString());
        }
        if (tenorIntRateElement.getName().equalsIgnoreCase(TENOR)
                && null != tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorJsonName())) {
            tenorIntRateElement.setContent(
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorJsonName()).getAsString(),
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorJsonName()).getAsString());
        }
        if (tenorIntRateElement.getName().equalsIgnoreCase(TENOR_INT)
                && null != tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorIntJsonName())) {
            tenorIntRateElement.setContent(
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorIntJsonName()).getAsString(),
                    tenorIntRate.get(vfxRatesDataConfig.getTenorIntRateTenorIntJsonName()).getAsString());
        }
    }

    /**
     * This method creates Tenor rate data
     * @param tenorRates
     * @param resolver
     * @param dataPath
     * @param createTimeFolder
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createTenorRateData(final JsonArray tenorRates, final ResourceResolver resolver, final String dataPath,
                                     final boolean createTimeFolder, final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        if (createTimeFolder) {
            Resource tenorRateTemplateRsc = resolver.getResource(vfxRatesDataConfig.getTenorRateModalPath());
            Resource parentRsc = resolver.getResource(dataPath);
            FragmentTemplate tenorRateTemplate = tenorRateTemplateRsc.adaptTo(FragmentTemplate.class);

            for (JsonElement rate : tenorRates) {
                JsonObject tenorRate = rate.getAsJsonObject();
                String itemId = getItemIdNumber();
                ContentFragment tenorRateFragment = tenorRateTemplate.createFragment(parentRsc, itemId, itemId);
                Iterator<ContentElement> tenorRateIter = tenorRateFragment.getElements();
                while (tenorRateIter.hasNext()) {
                    ContentElement tenorRateElement = tenorRateIter.next();
                    if (StringUtils.isNotBlank(tenorRateElement.getName())) {
                        setTenorRateData(tenorRateElement, itemId, tenorRate);
                    }
                }
                vfxRateCFPathList.add(dataPath + TCBConstants.SLASH + tenorRateFragment.getName());
            }
            resolver.commit();
        }
    }

    /**
     * This method sets the Tenor rate from the VFX request data
     * @param tenorRateElement
     * @param itemId
     * @param tenorRate
     * @throws ContentFragmentException
     */
    private void setTenorRateData(final ContentElement tenorRateElement, final String itemId,
            final JsonObject tenorRate) throws ContentFragmentException {

        if (tenorRateElement.getName().equalsIgnoreCase(ITEM_ID)) {
            tenorRateElement.setContent(itemId, itemId);
        }

        if (tenorRateElement.getName().equalsIgnoreCase(SOURCE_CURRENCY)
                && null != tenorRate.get(vfxRatesDataConfig.getTenorRatePairCurJsonName())) {
            String pairCurVal = tenorRate.get(vfxRatesDataConfig.getTenorRatePairCurJsonName()).getAsString();
            String[] currencyLabel = pairCurVal.split(TCBConstants.SLASH);
            tenorRateElement.setContent(currencyLabel[0], currencyLabel[0]);
        }
        if (tenorRateElement.getName().equalsIgnoreCase(BID_RATE)
                && null != tenorRate.get(vfxRatesDataConfig.getTenorRateBidRateJsonName())) {
            tenorRateElement.setContent(tenorRate.get(vfxRatesDataConfig.getTenorRateBidRateJsonName()).getAsString(),
                    tenorRate.get(vfxRatesDataConfig.getTenorRateBidRateJsonName()).getAsString());
        }

        if (tenorRateElement.getName().equalsIgnoreCase(ASK_RATE)
                && null != tenorRate.get(vfxRatesDataConfig.getTenorRateAskRateJsonName())) {
            tenorRateElement.setContent(tenorRate.get(vfxRatesDataConfig.getTenorRateAskRateJsonName()).getAsString(),
                    tenorRate.get(vfxRatesDataConfig.getTenorRateAskRateJsonName()).getAsString());
        }
        if (tenorRateElement.getName().equalsIgnoreCase(TENOR)
                && null != tenorRate.get(vfxRatesDataConfig.getTenorRateTenorJsonName())) {
            tenorRateElement.setContent(tenorRate.get(vfxRatesDataConfig.getTenorRateTenorJsonName()).getAsString(),
                    tenorRate.get(vfxRatesDataConfig.getTenorRateTenorJsonName()).getAsString());
        }
    }

    /**
     * This method creates Gold rate data
     * @param goldRate
     * @param resolver
     * @param dataPath
     * @param createTimeFolder
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createGoldRateData(final JsonObject goldRate, final ResourceResolver resolver, final String dataPath,
                                    final boolean createTimeFolder, final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        if (createTimeFolder) {
            Resource goldRateTemplateRsc = resolver.getResource(vfxRatesDataConfig.getGoldRateModalPath());
            Resource parentRsc = resolver.getResource(dataPath);
            FragmentTemplate goldRateTemplate = goldRateTemplateRsc.adaptTo(FragmentTemplate.class);
            String itemId = getItemIdNumber();
            ContentFragment goldRateFragment = goldRateTemplate.createFragment(parentRsc, itemId, itemId);

            Iterator<ContentElement> goldRateIter = goldRateFragment.getElements();
            while (goldRateIter.hasNext()) {
                ContentElement goldRateElement = goldRateIter.next();
                if (StringUtils.isNotBlank(goldRateElement.getName())) {
                    setGoldRateData(goldRateElement, itemId, goldRate);
                }
            }
            vfxRateCFPathList.add(dataPath + TCBConstants.SLASH + goldRateFragment.getName());
            resolver.commit();
        }

    }

    /**
     * This method sets the Gold rate from the VFX request data
     * @param goldRateElement
     * @param itemId
     * @param goldRate
     * @throws ContentFragmentException
     */
    private void setGoldRateData(final ContentElement goldRateElement, final String itemId, final JsonObject goldRate)
            throws ContentFragmentException {
        if (goldRateElement.getName().equalsIgnoreCase(ITEM_ID)) {
            goldRateElement.setContent(itemId, itemId);
        }

        if (goldRateElement.getName().equalsIgnoreCase(LABEL)
                && null != goldRate.get(vfxRatesDataConfig.getGoldRateLabelJsonName())) {
            goldRateElement.setContent(goldRate.get(vfxRatesDataConfig.getGoldRateLabelJsonName()).getAsString(),
                    goldRate.get(vfxRatesDataConfig.getGoldRateLabelJsonName()).getAsString());
        }

        if (goldRateElement.getName().equalsIgnoreCase(BID_RATE)
                && null != goldRate.get(vfxRatesDataConfig.getGoldRateBidRateJsonName())) {
            goldRateElement.setContent(goldRate.get(vfxRatesDataConfig.getGoldRateBidRateJsonName()).getAsString(),
                    goldRate.get(vfxRatesDataConfig.getGoldRateBidRateJsonName()).getAsString());
        }

        if (goldRateElement.getName().equalsIgnoreCase(ASK_RATE)
                && null != goldRate.get(vfxRatesDataConfig.getGoldRateAskRateJsonName())) {
            goldRateElement.setContent(goldRate.get(vfxRatesDataConfig.getGoldRateAskRateJsonName()).getAsString(),
                    goldRate.get(vfxRatesDataConfig.getGoldRateAskRateJsonName()).getAsString());
        }
    }

    /**
     * This method creates Other rate data
     * @param otherRate
     * @param resolver
     * @param dataPath
     * @param createTimeFolder
     * @param vfxRateCFPathList
     * @throws ContentFragmentException
     * @throws PersistenceException
     */
    private void createOtherRateData(final JsonObject otherRate, final ResourceResolver resolver, final String dataPath,
                                     final boolean createTimeFolder, final List<String> vfxRateCFPathList)
            throws ContentFragmentException, PersistenceException {
        if (createTimeFolder) {
            Resource otherRateTemplateRsc = resolver.getResource(vfxRatesDataConfig.getOtherRateModalPath());
            Resource parentRsc = resolver.getResource(dataPath);
            FragmentTemplate otherRateTemplat = otherRateTemplateRsc.adaptTo(FragmentTemplate.class);
            String itemId = getItemIdNumber();
            ContentFragment otherRateFragment = otherRateTemplat.createFragment(parentRsc, itemId, itemId);
            Iterator<ContentElement> otherRateIter = otherRateFragment.getElements();
            while (otherRateIter.hasNext()) {
                ContentElement otherRateElement = otherRateIter.next();
                if (StringUtils.isNotBlank(otherRateElement.getName())) {

                    setOtherRateData(otherRateElement, itemId, otherRate);
                }
            }
            vfxRateCFPathList.add(dataPath + TCBConstants.SLASH + otherRateFragment.getName());
            resolver.commit();
        }
    }

    /**
     * This method sets the Other rate from the VFX request data
     * @param otherRateElement
     * @param itemId
     * @param otherRate
     * @throws ContentFragmentException
     */
    private void setOtherRateData(final ContentElement otherRateElement, final String itemId,
            final JsonObject otherRate) throws ContentFragmentException {
        if (otherRateElement.getName().equalsIgnoreCase(ITEM_ID)) {
            otherRateElement.setContent(itemId, itemId);
        }

        if (otherRateElement.getName().equalsIgnoreCase(CENTRAL)
                && null != otherRate.get(vfxRatesDataConfig.getOtherRateCentralRateJsonName())) {
            otherRateElement.setContent(
                    otherRate.get(vfxRatesDataConfig.getOtherRateCentralRateJsonName()).getAsString(),
                    otherRate.get(vfxRatesDataConfig.getOtherRateCentralRateJsonName()).getAsString());
        }

        if (otherRateElement.getName().equalsIgnoreCase(FLOOR)
                && null != otherRate.get(vfxRatesDataConfig.getOtherRateFlrRateJsonName())) {
            otherRateElement.setContent(otherRate.get(vfxRatesDataConfig.getOtherRateFlrRateJsonName()).getAsString(),
                    otherRate.get(vfxRatesDataConfig.getOtherRateFlrRateJsonName()).getAsString());
        }

        if (otherRateElement.getName().equalsIgnoreCase(CEILING)
                && null != otherRate.get(vfxRatesDataConfig.getOtherRateCelRateJsonName())) {
            otherRateElement.setContent(otherRate.get(vfxRatesDataConfig.getOtherRateCelRateJsonName()).getAsString(),
                    otherRate.get(vfxRatesDataConfig.getOtherRateCelRateJsonName()).getAsString());
        }
    }

    // End: VFX CF Data Creation

}
