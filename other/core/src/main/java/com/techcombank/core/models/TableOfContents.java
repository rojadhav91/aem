package com.techcombank.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.TableOfContentPojo;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/*
 * This class represents a Sling Model that generates a table of contents (TOC) from the headings (h2 and h3) in a page.
 * It uses the QueryBuilder to search for text components in the page and extracts the headings using Jsoup.
 * The TOC is then structured into a JSON format for rendering.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableOfContents {

    private static final Logger LOG = LoggerFactory.getLogger(TableOfContents.class);

    /**
     * The label for the Table of Content Title.
     */
    @ValueMapValue
    @Getter
    private String title;

    @Inject
    private Page page;

    @Setter
    private Map<String, Object> jsonMap;

    @ScriptVariable
    private SlingHttpServletRequest request;

    public Map<String, Object> getJsonMap() {
        return jsonMap;
    }

    @PostConstruct
    protected void init() throws IOException, RepositoryException {
        // Initialize variables
        String path = page.getPath();
        String textValue = retrieveTextFromResources(path);

        Map<String, TableOfContentPojo> h2Map = extractTableOfContents(textValue, "h2");
        Map<String, TableOfContentPojo> h3Map = extractTableOfContents(textValue, "h3");
        organizeTableOfContents(h2Map, h3Map);
    }

    /**
     * Get all text components in the page and get consolidated HTML from them.
     * @param path
     * @return
     * @throws RepositoryException
     */
    String retrieveTextFromResources(final String path) throws RepositoryException {
        ResourceResolver resolver = request.getResourceResolver();
        QueryBuilder queryBuilder = resolver.adaptTo(QueryBuilder.class);
        Session session = resolver.adaptTo(Session.class);

        if (queryBuilder == null || session == null) {
            return "";
        }

        Map<String, String> predicates = new HashMap<>();
        predicates.put(TCBConstants.PATH, path);
        predicates.put(TCBConstants.PROPERTY_1, JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
        predicates.put(TCBConstants.PROPERTYVALUE_1, TCBConstants.TEXT_COMPONENT_PATH);
        Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        SearchResult result = query.getResult();

        StringBuilder textValue = new StringBuilder();
        for (Hit hit : result.getHits()) {
            Resource resource = hit.getResource();
            ValueMap properties = resource.adaptTo(ValueMap.class);
            textValue.append(properties.get("text", String.class));
        }
        return textValue.toString();
    }

    Map<String, TableOfContentPojo> extractTableOfContents(final String text, final String tagName) {
        Document document = Jsoup.parse(text);
        Elements elements = document.getElementsByTag(tagName);
        Map<String, TableOfContentPojo> map = new LinkedHashMap<>();

        for (Element element : elements) {
            TableOfContentPojo pojo = new TableOfContentPojo();
            pojo.setId(element.attr("id"));
            pojo.setValue(element.text());
            String indexValue = Integer.toString(element.elementSiblingIndex());
            map.put(indexValue, pojo);
        }
        return map;
    }

    void organizeTableOfContents(final Map<String, TableOfContentPojo> h2Map,
                                 final Map<String, TableOfContentPojo> h3Map)
            throws IOException {

        Map<String, Object> unsortedJsonMap;
        Gson gson = new Gson();

        //converting map to string
        String jsonh2 = gson.toJson(h2Map);
        String jsonh3 = gson.toJson(h3Map);

        //converting string to com.google.gson.JSONObject
        JsonObject jsonObjecth2 = gson.fromJson(jsonh2, JsonObject.class);
        JsonObject jsonObjecth3 = gson.fromJson(jsonh3, JsonObject.class);

        // Sort JSON keys and create lists for h2 and h3
        Integer[] jsonKeysh2 = sortJsonKeysFromGson(jsonObjecth2);
        Integer[] jsonKeysh3 = sortJsonKeysFromGson(jsonObjecth3);

        JsonObject finalJsonObject = new JsonObject();

        // Organize h3 elements under corresponding h2 elements in the JSON structure
        for (int i = 0; i <= jsonKeysh2.length - 1; i++) {
            int k = i;
            int h2FirstIndex = jsonKeysh2[i];

            // Check if the index is last h2 or not
            if (i < jsonKeysh2.length - 1) {
                int h2SecondIndex = jsonKeysh2[k + 1];
                addH3ToH2(jsonObjecth2, jsonObjecth3, h2FirstIndex, h2SecondIndex, -1, finalJsonObject);
            } else {
                // If it's the last h2 element, include remaining h3 elements
                if (jsonKeysh3.length > 0) {
                    int h3LastIndex = jsonKeysh3[jsonKeysh3.length - 1];
                    addH3ToH2(jsonObjecth2, jsonObjecth3, h2FirstIndex, -1, h3LastIndex, finalJsonObject);
                } else {
                    // If there is just single h2 authored with no h3 elements
                    addH3ToH2(jsonObjecth2, jsonObjecth3, h2FirstIndex, -1, -1, finalJsonObject);
                }
            }
        }
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(finalJsonObject.toString());
        unsortedJsonMap = objectMapper.convertValue(jsonNode, Map.class);

        jsonMap = new TreeMap<>(Comparator.comparingInt((String s) -> Integer.parseInt(s)));
        jsonMap = unsortedJsonMap;
        LOG.info("Printing JSON Map = {}", jsonMap);
    }

    public void addH3ToH2(final JsonObject jsonObjecth2, final JsonObject jsonObjecth3, final int h2FirstIndex,
                              final int h2SecondIndex, final int h3LastIndex, final JsonObject finalJsonObject) {
        JsonArray h3Array = new JsonArray();
        if (h2SecondIndex != -1) {
            for (int i = h2FirstIndex + 1; i < h2SecondIndex; i++) {
                if (jsonObjecth3.has(Integer.toString(i))) {
                    JsonObject h3Object = new JsonObject();
                    h3Object.add(Integer.toString(i), jsonObjecth3.getAsJsonObject(Integer.toString(i)));
                    h3Array.add(h3Object);
                }
            }
        } else if (h3LastIndex != -1) {
            for (int i = h2FirstIndex + 1; i <= h3LastIndex; i++) {
                if (jsonObjecth3.has(Integer.toString(i))) {
                    JsonObject h3Object = new JsonObject();
                    h3Object.add(Integer.toString(i), jsonObjecth3.getAsJsonObject(Integer.toString(i)));
                    h3Array.add(h3Object);
                }
            }
        }
        JsonObject tempH2JsonObject = jsonObjecth2.getAsJsonObject(Integer.toString(h2FirstIndex));
        tempH2JsonObject.add("h3", h3Array);
        finalJsonObject.add(Integer.toString(h2FirstIndex), tempH2JsonObject);
    }

    // Method takes the keys from the json object and store in array in sorted format(Ascending order).
    static Integer[] sortJsonKeysFromGson(final JsonObject jsonObject) {
        Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
        ArrayList<Integer> sortedKeysList = new ArrayList<>();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            String key = entry.getKey();
            sortedKeysList.add(Integer.parseInt(key));
        }
        Collections.sort(sortedKeysList);
        return sortedKeysList.toArray(new Integer[0]);
    }

    public String getHideStatus() {
        return page.getProperties().get(TCBConstants.ARTICLE_TABLE_OF_CONTENT_VISIBILITY_FLAG, String.class);
    }
}
