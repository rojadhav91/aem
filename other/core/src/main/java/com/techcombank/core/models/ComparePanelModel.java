package com.techcombank.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The Class ComparePanelModel.
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ComparePanelModel.class)
public class ComparePanelModel {

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    @Getter
    private String languageTitle = "English";

    @Getter
    private String languageCode = "en";

    @Getter
    private List<ComparePanelTagModel> customerTypeTags;

    @Getter
    private List<ComparePanelItemModel> dataItems;

    @PostConstruct
    protected void init() {
        initLanguageTitle();
        initLanguageCode();
        // 2. Load filter tags
        customerTypeTags = new ArrayList<>();
        if (StringUtils.isNotEmpty(tagFolder)) {
            final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            final Tag rootTag = tagManager
                    .resolve("/content/cq:tags/" + tagFolder.replace(":", "/") + "/vn/" + languageCode);
            if (null != rootTag) {
                // query customerTypes
                rootTag.listChildren().forEachRemaining(customerType -> {
                    List<ComparePanelTagModel> houseTypeTags = new ArrayList<>();
                    // query houseTypes
                    customerType.listChildren().forEachRemaining(houseType -> {
                        List<ComparePanelTagModel> investorTypeTags = new ArrayList<>();
                        // query investorTypes
                        houseType.listChildren().forEachRemaining(investorType -> {
                            ComparePanelTagModel investorTypeTag = new ComparePanelTagModel();
                            investorTypeTag.setTagId(investorType.getTagID());
                            investorTypeTag.setTagTitle(investorType.getTitle());
                            investorTypeTag.setTagPath(investorType.getPath());
                            investorTypeTags.add(investorTypeTag);
                        });
                        ComparePanelTagModel houseTypeTag = new ComparePanelTagModel();
                        houseTypeTag.setTagId(houseType.getTagID());
                        houseTypeTag.setTagTitle(houseType.getTitle());
                        houseTypeTag.setTagPath(houseType.getPath());
                        houseTypeTag.setChildTags(investorTypeTags);
                        houseTypeTags.add(houseTypeTag);
                    });
                    ComparePanelTagModel customerTypeTag = new ComparePanelTagModel();
                    customerTypeTag.setTagId(customerType.getTagID());
                    customerTypeTag.setTagTitle(customerType.getTitle());
                    customerTypeTag.setTagPath(customerType.getPath());
                    customerTypeTag.setChildTags(houseTypeTags);
                    customerTypeTags.add(customerTypeTag);
                });
            }
        }

        // 3. Load data table from content fragment
        dataItems = new ArrayList<>();
        if (StringUtils.isNotEmpty(contentFragmentFolder)) {
            Resource comparePanelCFFolder =
                    resourceResolver.resolve(contentFragmentFolder + "/vn/" + languageCode);
            if (comparePanelCFFolder != null) {
                Iterable<Resource> comparePanelRecords = comparePanelCFFolder.getChildren();
                for (Resource child : comparePanelRecords) {
                    ContentFragment contentFragment = child.adaptTo(ContentFragment.class);
                    if (contentFragment != null) {
                        dataItems.add(readComparePanelItemModel(contentFragment));
                    }
                }
            }
        }
    }

    private ComparePanelItemModel readComparePanelItemModel(ContentFragment contentFragment) {
        ComparePanelItemModel item = new ComparePanelItemModel();
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_PROJECT_NAME) != null) {
            item.setProjectName(
                    contentFragment.getElement(TCBConstants.COMPARE_PANEL_PROJECT_NAME)
                            .getContent());
        }
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_LOAN_RATE_ON_CAPITAL_NEED) != null) {
            item.setLoanRateOnCapitalNeed(
                    contentFragment
                            .getElement(TCBConstants.COMPARE_PANEL_LOAN_RATE_ON_CAPITAL_NEED)
                            .getContent());
        }
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_LOAN_RATE_ON_COLLATERAL_VALUE)
                != null) {
            item.setLoanRateOnCollateralValue(
                    contentFragment
                            .getElement(TCBConstants.COMPARE_PANEL_LOAN_RATE_ON_COLLATERAL_VALUE)
                            .getContent());
        }
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_LOAN_TERM) != null) {
            item.setLoanTerm(
                    contentFragment
                            .getElement(TCBConstants.COMPARE_PANEL_LOAN_TERM)
                            .getContent());
        }
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_COLLATERAL) != null) {
            item.setCollateral(
                    contentFragment
                            .getElement(TCBConstants.COMPARE_PANEL_COLLATERAL)
                            .getContent());
        }
        if (contentFragment.getElement(TCBConstants.COMPARE_PANEL_TAG) != null) {
            item.setTag(
                    contentFragment
                            .getElement(TCBConstants.COMPARE_PANEL_TAG)
                            .getContent());
        }
        return item;
    }

    private void initLanguageTitle() {
        try {
            languageTitle = PlatformUtils.getLanguageTitle(currentPage.getContentResource(), false, resourceResolver);
        } catch (RuntimeException e) {
            languageTitle = "";
        }
    }

    private void initLanguageCode() {
        if (Objects.equals(languageTitle, "Vietnamese")) {
            languageCode = "vi";
        }
        if (Objects.equals(languageTitle, "English")) {
            languageCode = "en";
        }
    }

    /**
     * Tag Folder
     * - Description: To choose Tag folder
     */
    @ValueMapValue
    @Getter
    private String tagFolder;

    /**
     * Content Fragment Folder
     * - Description: To choose content fragment data
     */
    @ValueMapValue
    @Getter
    private String contentFragmentFolder;

    /**
     * Customer Type Placeholder Label
     * - Description: To author customer type placeholder label
     */
    @ValueMapValue
    @Getter
    private String customerTypePlaceholderLabel;

    /**
     * House Type Placeholder Label
     * - Description: To author house type placeholder label
     */
    @ValueMapValue
    @Getter
    private String houseTypePlaceholderLabel;

    /**
     * Investor Type Placeholder Label
     * - Description: To author investor placeholder type
     */
    @ValueMapValue
    @Getter
    private String investorTypePlaceholderLabel;

    /**
     * Project’s Name Label
     * - Description: To author Project’s Name label
     */
    @ValueMapValue
    @Getter
    private String projectNameLabel;

    /**
     * Loan Rate On Capital Need Label
     * - Description: To author Loan rate on capital need label
     */
    @ValueMapValue
    @Getter
    private String loanRateOnCapitalNeedLabel;

    /**
     * Tool Tip - Loan Rate On Capital Need
     * - Description: To author tool tip for Loan rate on capital need label
     */
    @ValueMapValue
    @Getter
    private String toolTipLoanRateOnCapitalNeed;

    /**
     * Loan Rate On Collateral Value Label
     * - Description: To author Loan rate on collateral value label
     */
    @ValueMapValue
    @Getter
    private String loanRateOnCollateralValueLabel;

    /**
     * Tool Tip - Loan Rate On Collateral Value Label
     * - Description: To author tool tip for Loan rate on collateral value label
     */
    @ValueMapValue
    @Getter
    private String toolTipLoanRateOnCollateralValueLabel;

    /**
     * Loan Term Label
     * - Description: To author Loan term label
     */
    @ValueMapValue
    @Getter
    private String loanTermLabel;

    /**
     * Tool Tip - Loan Term Label
     * - Description: To author tool tip for Loan term label
     */
    @ValueMapValue
    @Getter
    private String toolTipLoanTermLabel;

    /**
     * Collateral Label
     * - Description: To author Collateral label
     */
    @ValueMapValue
    @Getter
    private String collateralLabel;

    /**
     * Tool Tip - Collateral
     * - Description: To author tool tip for Collateral label
     */
    @ValueMapValue
    @Getter
    private String toolTipCollateral;

    /**
     * Tool Tip Icon
     * - Description: To author Tool Tip icon
     */
    @ValueMapValue
    @Getter
    private String toolTipIcon;

    /**
     * Returns the mobile image rendition path for the tooltip icon.
     */
    public String getMobileTooltipIconPath() {
        return PlatformUtils.getMobileImagePath(toolTipIcon);
    }

    /**
     * Returns the web image rendition path for the tooltip icon.
     */
    public String getWebTooltipIconPath() {
        return PlatformUtils.getWebImagePath(toolTipIcon);
    }

    /**
     * 'View Result' CTA Label
     * - Description: To author CTA label
     */
    @ValueMapValue
    @Getter
    private String viewResultCtaLabel;

}
