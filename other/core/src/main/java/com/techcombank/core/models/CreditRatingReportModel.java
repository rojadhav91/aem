package com.techcombank.core.models;

import java.util.List;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CreditRatingReportModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CreditRatingReportModel {

    /**
     * Credit Rating Logo Image
     * - Description: To author brand logo image which provided the credit rating
    */
    @ValueMapValue
    @Getter
    private String creditRatingLogoImage;

    /**
     * Returns the mobile image rendition path for the Credit Rating Logo Image.
     */
    public String getMobileCreditRatingLogoImagePath() {
        return PlatformUtils.getMobileImagePath(creditRatingLogoImage);
    }

    /**
     * Returns the web image rendition path for the Credit Rating Logo Image.
     */
    public String getWebCreditRatingLogoImagePath() {
        return PlatformUtils.getWebImagePath(creditRatingLogoImage);
    }

    /**
     * Logo Image Alt Text
     * - Description: To author image alt text for logo image
    */
    @ValueMapValue
    @Getter
    private String logoImageAltText;

    /**
     * Credit Rating Tab List
     */
    @ChildResource
    @Getter
    private List<CreditRatingTabModel> creditRatingTabList;

}
