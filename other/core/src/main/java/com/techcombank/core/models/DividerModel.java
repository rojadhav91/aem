package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class DividerModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = DividerModel.class)
public class DividerModel {


    /** The divider style. */
    @ValueMapValue
    @Setter
    @Getter
    private String dividerStyle;

    /** The label text. */
    @ValueMapValue
    @Setter
    @Getter
    private String labelText;

}
