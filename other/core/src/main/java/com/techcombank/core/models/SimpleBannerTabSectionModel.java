package com.techcombank.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Simple Banner Tab section details
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SimpleBannerTabSectionModel {

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * The Image for the tab Icon.
     */
    @ValueMapValue
    @Getter
    private String tabIcon;

    /**
     * The Alt Text for the tab Icon.
     */
    @ValueMapValue
    @Getter
    private String tabIconAlt;

    /**
     * The Text for the tab name.
     */
    @ValueMapValue
    @Getter
    private String tabName;

    /**
     * The URL for tab link.
     */
    @ValueMapValue
    private String tabLinkURL;

    /**
     * To check the tab link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String tabTarget;

    /**
     * To check tab seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean tabNoFollow;

    /**
     * @return the tabLinkURL
     */
    public String getTabLinkURL() {
        return PlatformUtils.isInternalLink(resourceResolver, tabLinkURL);
    }

}
