package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * This model class is for define each Image of the Carousel
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CarouselImageItemModel {
    /**
     * Image
     */
    @ValueMapValue
    @Getter
    private String image;

    /**
     * Image Alt Text
     */
    @ValueMapValue
    @Getter
    private String imgAltText;

    public String getImage() {
        return PlatformUtils.getWebImagePath(image);
    }
    public String getImageMobile() {
        return PlatformUtils.getMobileImagePath(image);
    }
}

