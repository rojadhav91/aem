package com.techcombank.core.models;

import lombok.Getter;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Tenor Int Rate Content Fragment property to Java Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = TenorIntRate.class)
public class TenorIntRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * RateCD
     */
    @ValueMapValue
    @Getter
    private String rateCD;

    /**
     * rateLB
     */
    @ValueMapValue
    @Getter
    private String rateLB;

    /**
     * tenorInt
     */
    @ValueMapValue
    @Getter
    private String tenorInt;

    /**
     * tenor
     */
    @ValueMapValue
    @Getter
    private String tenor;
}
