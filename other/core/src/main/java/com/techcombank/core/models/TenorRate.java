package com.techcombank.core.models;

import lombok.Getter;
import lombok.Setter;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This class maps Tenor Rate Content Fragment property to Java Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = TenorRate.class)
public class TenorRate {

    /**
     * Item Id
     */
    @Inject
    @Getter
    private String itemId;

    /**
     * Ask Rate
     */
    @ValueMapValue
    @Getter
    private String askRate;

    /**
     * Bid Rate
     */
    @ValueMapValue
    @Getter
    private String bidRate;

    /**
     * Source Currency
     */
    @ValueMapValue
    @Getter
    private String sourceCurrency;

    /**
     * Target Currency
     */
    @Getter
    @Setter
    private String targetCurrency;

    /**
     * Tenor
     */
    @ValueMapValue
    @Getter
    private String tenor;
}
