package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

import javax.inject.Inject;
import java.util.List;

/**
 * The Class InspirePrivilegeModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InspirePrivilegeModel {

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Title
     * - Description: Title text for component
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Display Variation
    */
    @ValueMapValue
    @Getter
    private String displayVariation;

    /**
     * Image
     * - Description: To choose image
    */
    @ValueMapValue
    @Getter
    private String image;

    /**
     * Alt Text
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String altText;

    /**
     * Enable shadow
     * - Description: To add shadow for image
    */
    @ValueMapValue
    @Getter
    private boolean enableShadow;

    /**
     * Link Text
     * - Description: To author link label text
    */
    @ValueMapValue
    @Getter
    private String linkText;

    /**
     * Link URL
     * - Description: To author destination page URL
    */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Open In New Tab
     * - Description: To select if link URL to open in new tab or same tab
    */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No Follow
     * - Description: To choose follow/no follow
    */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(linkUrl);
    }

    /**
     * Enable Separator
     * - Description: Displays the separator line at the bottom of the component
    */
    @ValueMapValue
    @Getter
    private boolean enableSeparator;

    /**
     * Images
     */
    @ChildResource
    @Getter
    private List<InspirePrivilegeImageRowModel> slideImageRow1List;

    /**
     * Images
     */
    @ChildResource
    @Getter
    private List<InspirePrivilegeImageRowModel> slideImageRow2List;

    /**
     * Images
     */
    @ChildResource
    @Getter
    private List<InspirePrivilegeImageRowModel> slideImageRow3List;

    /**
     * Content Items
     */
    @ChildResource
    @Getter
    private List<InspirePrivilegeContentListModel> contentList;

    /**
     * @return the linkURL
     */
    public String getLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, linkUrl);
    }
}
