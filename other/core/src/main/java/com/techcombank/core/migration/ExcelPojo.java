package com.techcombank.core.migration;

import com.google.gson.annotations.Expose;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class in mapping between Excel properties to pojo
 */
@Data
@NoArgsConstructor
public class ExcelPojo {

    @Expose
    private String viId;

    @Expose
    private String viApiUrl;

    @Expose
    private String viAemUrl;

    @Expose
    private String enId;

    @Expose
    private String enApiUrl;

    @Expose
    private String enAemUrl;

    @Expose
    private String viPrimaryTag;

    @Expose
    private String viSecondaryTag;

    @Expose
    private String imgPrefixPath;

    @Expose
    private String hideArticleRelatedPost;

    @Expose
    private String hideArticleTagCloud;

    @Expose
    private String hideTableOfContent;

    @Expose
    private String alias;

}
