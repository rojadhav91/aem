package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

/**
 * The Class MediaCarouselModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = MediaCarouselModel.class)
public class MediaCarouselModel {

    /**
     * Carousel Items
     */
    @ChildResource
    @Getter
    private List<MediaCarouselItemModel> mediaCarouselItemList;

}
