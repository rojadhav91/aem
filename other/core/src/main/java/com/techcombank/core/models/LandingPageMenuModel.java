package com.techcombank.core.models;

import javax.inject.Inject;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for Mapping Landing Page Menu Properties to Primary
 * Navigation.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LandingPageMenuModel {

    /**
     * The label text for the Landing Page Title.
     */
    @ValueMapValue
    @Getter
    private String landingMenuTitle;

    /**
     * The URL for the landing Page.
     */
    @ValueMapValue
    private String landingPageURL;

    /**
     * The Target for the Landing page URl.
     */
    @ValueMapValue
    @Getter
    private String landingTarget;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the landingPageURL
     */
    public String getLandingPageURL() {
        return PlatformUtils.isInternalLink(resourceResolver, landingPageURL);
    }

    /**
     * Returns the landing page url interaction type for analytics.
     */
    public String getLandingPageURLInteractionType() {
        return PlatformUtils.getUrlLinkType(landingPageURL);
    }

}
