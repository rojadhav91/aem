package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class MastheadExtendImageModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MastheadExtendImageModel {

    /**
     * Image
     * - Description: To select individual image from DAM
    */
    @ValueMapValue
    @Getter
    private String image;

    /**
    * Returns the mobile image rendition path for the image.
    */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(image);
    }

    /**
    * Returns the web image rendition path for the image.
    */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(image);
    }

    /**
     * Image Alt Text
     * - Description: To add alternate text for image
    */
    @ValueMapValue
    @Getter
    private String imageAltText;

}
