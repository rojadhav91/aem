package com.techcombank.core.servlets;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.crx.JcrConstants;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.sitemap.SitemapException;
import org.apache.sling.sitemap.builder.Sitemap;
import org.apache.sling.sitemap.builder.Url;
import org.apache.sling.sitemap.spi.common.SitemapLinkExternalizer;
import org.apache.sling.sitemap.spi.generator.ResourceTreeSitemapGenerator;
import org.apache.sling.sitemap.spi.generator.SitemapGenerator;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Optional;

@Component(
        service = SitemapGenerator.class,
        property = { "service.ranking:Integer=100" }
)
public class CustomSitemapGenerator extends ResourceTreeSitemapGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomSitemapGenerator.class);

    @Reference
    private SitemapLinkExternalizer sitemapLinkExternalizer;

    @Override
    protected void addResource(final String name, final Sitemap sitemap, final Resource resource)
            throws SitemapException {
        final Page page = resource.adaptTo(Page.class);
        String priority;
        String changeFrequency;

        InheritanceValueMap inheritedProp = new HierarchyNodeInheritanceValueMap(page.getContentResource());
        priority = inheritedProp.getInherited(TCBConstants.SEO_PRIORITY, String.class);
        changeFrequency = inheritedProp.getInherited(TCBConstants.CHANGE_FREQUENCY, String.class);

        final String location = this.sitemapLinkExternalizer.externalize(resource);
        final Url url = sitemap.addUrl(location);
                if (priority != null) {
                    Double seoPriority = Double.valueOf(priority);
                    LOGGER.info("Priority of the page = {} ", seoPriority);
                    url.setPriority(seoPriority);
                }
                if (changeFrequency != null) {
                    Url.ChangeFrequency urlChangeFrequency = Url.ChangeFrequency.NEVER;
                    if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_HOURLY)) {
                        urlChangeFrequency = Url.ChangeFrequency.HOURLY;
                    } else if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_DAILY)) {
                        urlChangeFrequency = Url.ChangeFrequency.DAILY;
                    } else if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_WEEKLY)) {
                        urlChangeFrequency = Url.ChangeFrequency.WEEKLY;
                    } else if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_MONTHLY)) {
                        urlChangeFrequency = Url.ChangeFrequency.MONTHLY;
                    } else if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_YEARLY)) {
                        urlChangeFrequency = Url.ChangeFrequency.YEARLY;
                    } else if (StringUtils.equalsIgnoreCase(changeFrequency, TCBConstants.CHANGE_FREQUENCY_ALWAYS)) {
                        urlChangeFrequency = Url.ChangeFrequency.ALWAYS;
                    }
                    url.setChangeFrequency(urlChangeFrequency);
                    LOGGER.info("Change Frequency = {} ", changeFrequency);
                }

                Calendar lastModifiedDate = page.getContentResource().getValueMap().get(JcrConstants.JCR_CREATED,
                        Calendar.class);
                final Calendar lastModified = Optional.ofNullable(page.getLastModified()).orElse(lastModifiedDate);
                if (lastModified != null) {
                    url.setLastModified(lastModified.toInstant());
                    LOGGER.info("Last Modified date = {}  ", lastModified.toInstant());
                }
    }

    @Override
    protected final boolean shouldInclude(final Resource resource) {
        return super.shouldInclude(resource)
                && Optional.ofNullable(resource.adaptTo(Page.class)).map(this::shouldInclude).orElse(Boolean.FALSE);
    }
    private boolean shouldInclude(final Page page) {
        ValueMap pageProperties = page.getContentResource().getValueMap();
        boolean flag = true;
        if (pageProperties.get(TCBConstants.CQ_ROBOTSTAGS) != null
                && StringUtils.equalsIgnoreCase(TCBConstants.NO_INDEX,
                pageProperties.get(TCBConstants.CQ_ROBOTSTAGS, StringUtils.EMPTY))) {
            flag = false;
        }
        return flag;
    }
}
