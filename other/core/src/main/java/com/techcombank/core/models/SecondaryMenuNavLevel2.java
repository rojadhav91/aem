package com.techcombank.core.models;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import javax.inject.Inject;
import java.util.List;

/**
 * A SlingModel representing a secondary menu navigation level 2.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = SecondaryMenuNavLevel2.class)
public class SecondaryMenuNavLevel2 {

    /**
     * The label text for the sub tab menu.
     */
    @ValueMapValue
    @Getter
    private String subTabMenuLabelText;

    /**
     * The URL for the sub tab menu link.
     */
    @ValueMapValue
    private String subTabMenuLinkUrl;

    /**
     * Whether the sub tab menu link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String openinnewtab;

    /**
     * The reduction text for the sub tab menu.
     */
    @ValueMapValue
    @Getter
    private String viewLessSubTab;

    /**
     * The expansion text for the sub tab menu.
     */
    @ValueMapValue
    @Getter
    private String viewAllSubTab;

    /**
     * A list of the third level sub menus.
     */
    @ChildResource
    @Getter
    private List<SecondaryMenuNavLevel3> secondaryMenuNavLevel3;
    /**
     * The icon for the bottom link.
     */
    @ValueMapValue
    @Getter
    private String subMenuBottomLinkIcon;
    /**
     * The alt text for the icon image.
     */
    @ValueMapValue
    @Getter
    private String iconImageAltText;
    /**
     * The text for the bottom link.
     */
    @ValueMapValue
    @Getter
    private String subMenuBottomLinkText;
    /**
     * The URL for the bottom link.
     */
    @ValueMapValue
    private String subMenuBottomLinkUrl;
    /**
     * The text for the right bottom link.
     */
    @ValueMapValue
    @Getter
    private String subMenuBottomLinkTextRight;
    /**
     * The URL for the right bottom link.
     */
    @ValueMapValue
    private String subMenuBottomLinkUrlRight;

    /**
     * Whether the Left submenu link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String subMenuTargetLeft;

    /**
     * Whether the Right submenu link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String subMenuTargetRight;

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the subTabMenuLinkUrl
     */
    public String getSubTabMenuLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, subTabMenuLinkUrl);
    }

    /**
     * @return the subMenuBottomLinkUrl
     */
    public String getSubMenuBottomLinkUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, subMenuBottomLinkUrl);
    }

    /**
     * @return the subMenuBottomLinkUrlRight
     */
    public String getSubMenuBottomLinkUrlRight() {
        return PlatformUtils.isInternalLink(resourceResolver, subMenuBottomLinkUrlRight);
    }

    /**
     * Returns the mobile image rendition path for the subMenuBottomLinkIcon.
     */
    public String getSubMenuBottomLinkMobileIconPath() {
        return PlatformUtils.getMobileImagePath(subMenuBottomLinkIcon);
    }

    /**
     * Returns the web image rendition path for the subMenuBottomLinkIcon.
     */
    public String getSubMenuBottomLinkWebIconPath() {
        return PlatformUtils.getWebImagePath(subMenuBottomLinkIcon);
    }

    /**
     * Returns the sub tab menu link url interaction type for analytics.
     */
    public String getSubTabMenuLinkUrlInteractionType() {
        return PlatformUtils.getUrlLinkType(subTabMenuLinkUrl);
    }

    /**
     * Returns the sub menu bottom link url interaction type for analytics.
     */
    public String getSubMenuBottomLinkUrllInteractionType() {
        return PlatformUtils.getUrlLinkType(subMenuBottomLinkUrl);
    }

    /**
     * Returns the sub menu bottom link url right interaction type for analytics.
     */
    public String getSubMenuBottomLinkUrlRightInteractionType() {
        return PlatformUtils.getUrlLinkType(subMenuBottomLinkUrlRight);

    }
}
