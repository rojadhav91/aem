package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.models.FinancialHighlightsModel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Objects;

/**
 * This model class is for FinancialHighlightsModelImpl Model
 */
@Model(adaptables = Resource.class, adapters = {
        FinancialHighlightsModel.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType =
        FinancialHighlightsModelImpl.RESOURCE_TYPE)
@Getter
public class FinancialHighlightsModelImpl implements FinancialHighlightsModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/financialhighlights";

    /**
     * The ResourceResolver.
     */
    @Inject
    private ResourceResolver resolver;

    /**
     * The fhTitle value.
     */
    @ValueMapValue
    private String fhTitle;

    /**
     * The fhData value.
     */
    @ValueMapValue
    private String fhData;

    /**
     * The fhQuarter value.
     */
    @ValueMapValue
    private String fhQuarter;

    /**
     * The fhNotes value.
     */
    @ValueMapValue
    private String fhNotes;

    @PostConstruct
    void init() {
        if (Objects.nonNull(fhQuarter)) {
            final TagManager tagManager = resolver.adaptTo(TagManager.class);
            Tag tag = Objects.requireNonNull(tagManager).resolve(fhQuarter);
            fhQuarter = tag.getName();
        }
    }
}
