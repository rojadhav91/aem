package com.techcombank.core.service.impl;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.wcm.webservicesupport.ConfigurationConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import com.techcombank.core.service.QmsIntegrationService;
import com.techcombank.core.service.RecaptchaService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component(service = {QmsIntegrationService.class}, immediate = true)
@Designate(ocd = QmsIntegrationServiceImpl.QmsIntegrationServiceConfig.class)
public class QmsIntegrationServiceImpl implements QmsIntegrationService {

    //Common Constant for All service
    public static final String TX_ID = "txId";
    public static final String CHAR_SET = "charSet";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String BIZ_MSG_IDR = "bizMsgIdr";
    public static final String BIZ_SVC = "bizSvc";
    public static final String CREATE_DATE = "creDt";
    public static final String LANGUAGE = "language";
    public static final String APP_HDR = "appHdr";
    public static final String MSG_REQ_HDR = "msgReqHdr";
    public static final String CHANNEL = "channel";
    public static final String DOCUMENT = "document";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String BRANCH_LIST_SERVICE = "branchlist";
    public static final String SERVICE_LIST = "servicelist";
    public static final String BOOKING_SLOT_SERVICE = "bookingslot";
    public static final String BOOKING_CONFIRMATION_SERVICE = "bookingconfirmation";
    public static final String TYPE = "type";

    //Branch List service Constant
    public static final String SEARCH = "search";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "long";
    public static final String BRANCH_INF = "branchInf";
    public static final String SRV_IDENT = "srvIdent";
    public static final String BRANCH_LIST = "branchList";

    //Branch Slot Service Constants
    public static final String SRV_ID = "srvID";
    public static final String BRANCH_ID = "branchID";
    public static final String SLOT_INFO = "slotInf";
    public static final String CUSTOMER_INFO = "customerInf";


    //Branch Booking Confirmation Service  & Slot Service Constants
    public static final String ID_CODE = "iDCode";
    public static final String ID_TYPE = "iDType";
    public static final String DEPT_ID = "deptid";

    //Branch Booking Confirmation Service Constants
    public static final String BOOKING_BRANCH_ID = "branchId";
    public static final String SERVICE_INFO = "serviceInf";
    public static final String SERVICES = "services";
    public static final String BOOKING_DATE = "bkDt";
    public static final String BOOKING_TIME = "bkTm";
    public static final String CUSTOMER_NAME = "customerNm";
    public static final String CUSTOMER_ID = "customerID";
    public static final String CAPTCH_KEY = "captchakey";
    public static final String BRANCH_INFO = "branchInf";
    public static final String ID_DETAILS = "iDDtls";
    public static final String CONTACT_DETAILS = "contactDtls";
    public static final String PHONE_NUM = "phneNb";
    public static final String ACC_NUM = "accno";
    public static final String EMAIL_ADDR = "emailAdr";
    public static final String SERV_ID = "servid";
    public static final String SERV_NM = "servnm";
    public static final String LOG_REQUEST_PAYLOAD = "Pay Load :: {}";

    private static final Logger LOG = LoggerFactory.getLogger(QmsIntegrationServiceImpl.class);
    @Reference
    private HttpClientExecutorService httpExecutor;
    @Reference
    private JwtTokenService jwtTokenService;
    @Reference
    private RecaptchaService recaptchaService;
    private QmsIntegrationServiceConfig qmsServiceConfig;

    @Activate
    public void activate(final QmsIntegrationServiceConfig config) {
        this.qmsServiceConfig = config;
    }

    /**
     * This method fetch service list from QMS
     *
     * @param isTokenExpired
     * @return JsonObject responseJson
     */
    @Override
    public JsonObject fetchQMSServiceList(final boolean isTokenExpired, final SlingHttpServletRequest request,
                                          final JsonObject reqObj) {

        String qmsServiceListUrl = qmsServiceConfig.getQmsServiceListUrl();
        JsonObject requestPayLoad = contructRequestPayload(request, SERVICE_LIST, reqObj);
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headerMap = constructHeaderMap(token);
        JsonObject responseJson = new JsonObject();
        try {
            StringEntity payLoad = new StringEntity(requestPayLoad.toString());
            LOG.debug(LOG_REQUEST_PAYLOAD, requestPayLoad);
            responseJson = httpExecutor.executeHttpPost(qmsServiceListUrl, headerMap, payLoad);

        } catch (UnsupportedEncodingException e) {
            responseJson = setErrorCode(e.getMessage());
            LOG.error("Error occured in fetchQMSServiceList method ::: response code: {}, message: {}",
                    HttpStatus.SC_BAD_REQUEST, e.getMessage());
        }

        return responseJson;
    }

    /**
     * This method fetch branch details from QMS
     *
     * @param isTokenExpired
     * @param request
     * @return JsonObject responseJson
     */
    @Override
    public JsonObject fetchQMSBranchList(final boolean isTokenExpired, final SlingHttpServletRequest request,
                                         final JsonObject reqObj) {

        String qmsBranchUrl = qmsServiceConfig.getQmsBranchUrl();
        JsonObject requestPayLoad = contructRequestPayload(request, BRANCH_LIST_SERVICE, reqObj);
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headerMap = constructHeaderMap(token);
        JsonObject responseJson = new JsonObject();
        try {
            StringEntity payLoad = new StringEntity(requestPayLoad.toString());
            LOG.debug(LOG_REQUEST_PAYLOAD, requestPayLoad);
            responseJson = httpExecutor.executeHttpPost(qmsBranchUrl, headerMap, payLoad);

        } catch (UnsupportedEncodingException e) {
            responseJson = setErrorCode(e.getMessage());
            LOG.error("Error occured in fetchQMSBranchList method ::: response code: {}, message: {}",
                    HttpStatus.SC_BAD_REQUEST, e.getMessage());
        }

        return responseJson;
    }

    /**
     * This method fetch branch available slot details from QMS
     *
     * @param isTokenExpired
     * @param request
     * @param reqObj
     * @return JsonObject responseJson
     */
    @Override
    public JsonObject fetchQMSBranchSlot(final boolean isTokenExpired, final SlingHttpServletRequest request,
                                         final JsonObject reqObj) {

        String qmsBranchSlotUrl = qmsServiceConfig.getQmsBranchSlotUrl();
        JsonObject requestPayLoad = contructRequestPayload(request, BOOKING_SLOT_SERVICE, reqObj);
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headerMap = constructHeaderMap(token);
        JsonObject responseJson = new JsonObject();
        try {
            StringEntity payLoad = new StringEntity(requestPayLoad.toString());
            LOG.debug(LOG_REQUEST_PAYLOAD, requestPayLoad);
            responseJson = httpExecutor.executeHttpPost(qmsBranchSlotUrl, headerMap, payLoad);

        } catch (UnsupportedEncodingException e) {
            responseJson = setErrorCode(e.getMessage());
            LOG.error("Error occured in fetchQMSBranchSlot method ::: response code: {}, message: {}",
                    HttpStatus.SC_BAD_REQUEST, e.getMessage());
        }

        return responseJson;
    }

    /**
     * This method fetch branch available slot details from QMS
     *
     * @param isTokenExpired
     * @param request
     * @param reqObj
     * @return JsonObject responseJson
     */
    @Override
    public JsonObject qmsSubmitBooking(final boolean isTokenExpired, final SlingHttpServletRequest request,
                                            final JsonObject reqObj) {

        String qmsBranchBookingSubmitUrl = qmsServiceConfig.getQmsBookingSubmitUrl();
        JsonObject requestPayLoad = contructRequestPayload(request, BOOKING_CONFIRMATION_SERVICE, reqObj);
        String token = jwtTokenService.getJwtToken(isTokenExpired);
        Map<String, String> headerMap = constructHeaderMap(token);
        JsonObject responseJson = new JsonObject();
        try {
            StringEntity payLoad = new StringEntity(requestPayLoad.toString());
            LOG.debug(LOG_REQUEST_PAYLOAD, requestPayLoad);
            responseJson = httpExecutor.executeHttpPost(qmsBranchBookingSubmitUrl, headerMap, payLoad);

        } catch (UnsupportedEncodingException e) {
            responseJson = setErrorCode(e.getMessage());
            LOG.error("Error occured in fetchQMSBookingSubmit method ::: response code: {}, message: {}",
                    HttpStatus.SC_BAD_REQUEST, e.getMessage());
        }

        return responseJson;
    }

    /**
     * This Method returns unique id used for TXID in the payload
     *
     * @return String UUID
     */
    private String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * This method construct the Payload data for the QMS branch API request
     *
     * @param request
     * @param ServiceName
     * @param reqObj
     * @return JsonObject requestPayload
     */
    private JsonObject contructRequestPayload(final SlingHttpServletRequest request, final String serviceName,
                                              final JsonObject reqObj) {
        JsonObject requestPayload = new JsonObject();
        addMessageRequestHeader(requestPayload, request);
        if (serviceName.equalsIgnoreCase(SERVICE_LIST)) {
            addServiceListDocumentData(requestPayload, reqObj);
        } else if (serviceName.equalsIgnoreCase(BRANCH_LIST_SERVICE)) {
            addBranchListDocumentData(requestPayload, reqObj);
        } else if (serviceName.equalsIgnoreCase(BOOKING_SLOT_SERVICE)) {
            addBranchSlotDocumentData(requestPayload, reqObj);
        } else if (serviceName.equalsIgnoreCase(BOOKING_CONFIRMATION_SERVICE)) {
            addBookingConfirmationDocumentData(requestPayload, request, reqObj);
        }

        return requestPayload;
    }

    /**
     * This Method construct the msgReqHdr data for the request payload
     *
     * @param requestPayload
     * @param request
     */
    private void addMessageRequestHeader(final JsonObject requestPayload, final SlingHttpServletRequest request) {
        JsonObject msgRequestHeader = new JsonObject();
        JsonObject appHeader = new JsonObject();
        msgRequestHeader.addProperty(TX_ID, getUUID());
        appHeader.addProperty(CHAR_SET, qmsServiceConfig.getCharSet());
        appHeader.addProperty(FROM, qmsServiceConfig.getFrom());
        appHeader.addProperty(TO, qmsServiceConfig.getTo());
        appHeader.addProperty(BIZ_MSG_IDR, qmsServiceConfig.getBizMsgIdr());
        appHeader.addProperty(BIZ_SVC, qmsServiceConfig.getBizSvc());
        appHeader.addProperty(CREATE_DATE, getCurrentDate());
        appHeader.addProperty(LANGUAGE, PlatformUtils.getLocale(request.getResource().getPath(),
                request.getResourceResolver()));
        msgRequestHeader.add(APP_HDR, appHeader);
        requestPayload.add(MSG_REQ_HDR, msgRequestHeader);

    }

    /**
     * This Method construct the document data for the service list payload
     *
     * @param requestPayload
     * @param reqObj
     */
    private void addServiceListDocumentData(final JsonObject requestPayload, final JsonObject reqObj) {
        JsonObject document = new JsonObject();
        document.addProperty(CHANNEL, qmsServiceConfig.getChannel());
        document.addProperty(TYPE, Integer.valueOf(reqObj.get(TYPE).getAsString()));
        requestPayload.add(DOCUMENT, document);
    }

    /**
     * This Method construct the document data for the branch request payload
     *
     * @param requestPayload
     * @param reqObj
     */
    private void addBranchListDocumentData(final JsonObject requestPayload, final JsonObject reqObj) {
        JsonObject branchList = new JsonObject();
        JsonObject branchInfo = new JsonObject();
        JsonArray srvIdentArray = new JsonArray();
        JsonObject srvIdentInfo = new JsonObject();
        JsonObject branch = new JsonObject();
        branchList.addProperty(SEARCH, reqObj.get(SEARCH).getAsString());
        branchList.addProperty(CHANNEL, qmsServiceConfig.getChannel());
        branchInfo.addProperty(LATITUDE, Float.valueOf(reqObj.get(LATITUDE).getAsString()));
        branchInfo.addProperty(LONGITUDE, Float.valueOf(reqObj.get(LONGITUDE).getAsString()));
        branchList.add(BRANCH_INF, branchInfo);
        srvIdentInfo.addProperty(SRV_IDENT, reqObj.get(SRV_IDENT).getAsString());
        srvIdentArray.add(srvIdentInfo);
        branchList.add(SRV_IDENT, srvIdentArray);
        branch.add(BRANCH_LIST, branchList);
        requestPayload.add(DOCUMENT, branch);
    }

    /**
     * This Method construct the document data for the request payload
     *
     * @param requestPayload
     * @param reqObj
     */
    private void addBranchSlotDocumentData(final JsonObject requestPayload, final JsonObject reqObj) {
        JsonObject customerInfo = new JsonObject();
        JsonObject slotInfo = new JsonObject();
        JsonObject branchSlot = new JsonObject();
        customerInfo.addProperty(ID_TYPE, qmsServiceConfig.getIdType());
        customerInfo.addProperty(ID_CODE, StringUtils.EMPTY);

        slotInfo.addProperty(BRANCH_ID, reqObj.get(BRANCH_ID).getAsString());
        slotInfo.addProperty(SRV_ID, reqObj.get(SRV_ID).getAsString());

        branchSlot.add(CUSTOMER_INFO, customerInfo);
        branchSlot.add(SLOT_INFO, slotInfo);
        branchSlot.addProperty(DEPT_ID, qmsServiceConfig.getDeptId());
        branchSlot.addProperty(CHANNEL, qmsServiceConfig.getChannel());
        requestPayload.add(DOCUMENT, branchSlot);
    }

    /**
     * This Method construct the Booking confirmation document data for the request payload
     *
     * @param requestPayload
     * @param request
     * @param reqObj
     */
    private void addBookingConfirmationDocumentData(final JsonObject requestPayload,
                                                    final SlingHttpServletRequest request, final JsonObject reqObj) {

        JsonObject document = new JsonObject();
        JsonObject serviceInfo = new JsonObject();
        JsonObject branchInfo = new JsonObject();
        JsonObject iDDtls = new JsonObject();
        JsonObject contactDtls = new JsonObject();


        iDDtls.addProperty(ID_TYPE, Integer.valueOf(reqObj.get(ID_TYPE).getAsString()));
        branchInfo.addProperty(BOOKING_BRANCH_ID, reqObj.get(BOOKING_BRANCH_ID).getAsString());
        contactDtls.addProperty(PHONE_NUM, reqObj.get(PHONE_NUM).getAsString());
        contactDtls.addProperty(ACC_NUM, reqObj.get(ACC_NUM).getAsString());
        contactDtls.addProperty(EMAIL_ADDR, reqObj.get(EMAIL_ADDR).getAsString());

        String services = "{\"servid\":\"{servid}\",\"servnm\":\"{servnm}\",\"required\":[]}";
        String[] keywords = {"{servid}", "{servnm}"};
        String[] replacements = {reqObj.get(SERV_ID).getAsString(), reqObj.get(SERV_NM).getAsString()};
        serviceInfo.addProperty(SERVICES, StringUtils.replaceEach(services, keywords, replacements));
        serviceInfo.addProperty(BOOKING_DATE, reqObj.get(BOOKING_DATE).getAsString());
        serviceInfo.addProperty(BOOKING_TIME, reqObj.get(BOOKING_TIME).getAsString());
        serviceInfo.addProperty(CUSTOMER_NAME, reqObj.get(CUSTOMER_NAME).getAsString());
        serviceInfo.addProperty(CUSTOMER_ID, StringUtils.EMPTY);
        serviceInfo.addProperty(TYPE, Integer.valueOf(reqObj.get(TYPE).getAsString()));
        serviceInfo.addProperty(CAPTCH_KEY, getCaptchaKey(request));

        serviceInfo.add(BRANCH_INFO, branchInfo);
        serviceInfo.add(ID_DETAILS, iDDtls);
        serviceInfo.add(CONTACT_DETAILS, contactDtls);

        document.add(SERVICE_INFO, serviceInfo);
        requestPayload.add(DOCUMENT, document);
    }

    /**
     * This method construct the header map for request payload
     *
     * @param token
     * @return map headerMap
     */
    private Map<String, String> constructHeaderMap(final String token) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(HttpHeaders.CONTENT_TYPE, TCBConstants.APPLICATION_JSON);
        headerMap.put(HttpHeaders.ACCEPT, TCBConstants.APPLICATION_JSON);
        headerMap.put(TCBConstants.AUTHORIZATION, TCBConstants.BEARER + token);
        return headerMap;
    }

    /**
     * This method is called and set the error code and message when there is an issue in the request payload
     *
     * @param errorMsg
     * @return JsonObject responseJson
     */
    private JsonObject setErrorCode(final String errorMsg) {
        JsonObject responseJson = new JsonObject();
        responseJson.addProperty(TCBConstants.ERROR_CODE, HttpStatus.SC_BAD_REQUEST);
        responseJson.addProperty(TCBConstants.ERROR_MSG, errorMsg);
        return responseJson;
    }

    /**
     * This method get the captcha key from captcha service
     *
     * @param request
     * @return String siteKey
     */
    private String getCaptchaKey(final SlingHttpServletRequest request) {
        HierarchyNodeInheritanceValueMap pageProperties =
                new HierarchyNodeInheritanceValueMap(Objects.requireNonNull(request.getResource()));
        String[] allServices = pageProperties.getInherited(ConfigurationConstants.PN_CONFIGURATIONS, new String[0]);
        String configPath = Arrays.stream(allServices)
                .filter(serviceName -> serviceName.startsWith(TCBConstants.RECAPTCHA)).findFirst()
                .orElse(null);
        String siteKey = StringUtils.EMPTY;
        if (Objects.nonNull(configPath)) {
            Optional<String> siteKeyValue = recaptchaService.getSiteKey(configPath);
            siteKey = siteKeyValue.isPresent()
                    ? siteKeyValue.get()
                    : StringUtils.EMPTY;
        }
        return siteKey;
    }

    /**
     * This Method creates and returns the date in the format
     * '2022-07-09T07:14:42.716Z'
     *
     * @return String currentDate
     */
    private String getCurrentDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
        return dateFormatter.format(new Date());
    }

    @ObjectClassDefinition(name = "QMS Integration service Configuration",
            description = "QMS Integration service Configuration")
    public @interface QmsIntegrationServiceConfig {

        @AttributeDefinition(name = "qmsBranchUrl", description = "QMS Branch Url", type = AttributeType.STRING)
        String getQmsBranchUrl() default "";

        @AttributeDefinition(name = "qmsServiceListUrl", description = "QMS Service List Url",
                type = AttributeType.STRING)
        String getQmsServiceListUrl() default "";

        @AttributeDefinition(
                name = "qmsBranchSlotUrl", description = "QMS Branch Slot Url", type = AttributeType.STRING)
        String getQmsBranchSlotUrl()
                default "";

        @AttributeDefinition(
                name = "qmsBookingSubmitUrl", description = "QMS Booking Submit Url", type = AttributeType.STRING)
        String getQmsBookingSubmitUrl()
                default "";

        @AttributeDefinition(name = "channel", description = "Channel", type = AttributeType.INTEGER)
        int getChannel() default 2;

        @AttributeDefinition(
                name = "deptId", description = "DeptId", type = AttributeType.INTEGER)
        int getDeptId() default 1;

        @AttributeDefinition(
                name = "idType", description = "IdType", type = AttributeType.INTEGER)
        int getIdType() default 0;

        @AttributeDefinition(name = "charSet", description = "charSet", type = AttributeType.STRING)
        String getCharSet() default "UTF-8";

        @AttributeDefinition(name = "from", description = "from", type = AttributeType.STRING)
        String getFrom() default "AEM";

        @AttributeDefinition(name = "to", description = "to", type = AttributeType.STRING)
        String getTo() default "QMS";

        @AttributeDefinition(name = "bizMsgIdr", description = "bizMsgIdr", type = AttributeType.STRING)
        String getBizMsgIdr() default "Techcombank";

        @AttributeDefinition(name = "bizSvc", description = "bizSvc", type = AttributeType.STRING)
        String getBizSvc() default "QMS";
    }

}
