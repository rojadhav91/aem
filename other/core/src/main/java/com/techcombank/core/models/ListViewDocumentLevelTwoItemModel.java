package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * This model class is for define each Tile of List Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListViewDocumentLevelTwoItemModel {

    @Inject
    private ResourceResolver resourceResolver;
    /**
     * Document title
     */
    @ValueMapValue
    @Getter
    private String documentTitleChild;

    /**
     * Type of output
     */
    @ValueMapValue
    @Getter
    private String typeOfOutput;

    /**
     * Label
     */
    @ValueMapValue
    @Getter
    private String label;

    /**
     * Icon
     */
    @ValueMapValue
    @Getter
    private String icon;

    /**
     * URL
     */
    @ValueMapValue
    private String url;
    /**
     * To get URL
     */
    public String getUrl() {
        return PlatformUtils.isInternalLink(resourceResolver, url);
    }
    /**
     * To get icon for desktop
     */
    public String getWebIcon() {
        return PlatformUtils.getWebImagePath(icon);
    }
    /**
     * To get icon for mobile
     */
    public String getMobileIcon() {
        return PlatformUtils.getMobileImagePath(icon);
    }

    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(url);
    }
}
