package com.techcombank.core.models.impl;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.models.NavPanelModel;
import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * This model class is for mapping Nav Panel section details
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavPanelModelImpl implements NavPanelModel {

    @Inject
    private ResourceResolver resourceResolver;

    /**
     * The Text for the nav panel card description.
     */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * The Image for cta type 1.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeOneIcon;

    /**
     * The Alt text for cta type 1.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeOneAltText;

    /**
     * The Text for cta Type 1 label.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeOneLabel;

    /**
     * The URL for cta type 1 link.
     */
    @ValueMapValue
    private String ctaTypeOneLinkURL;

    /**
     * To check the cta type 1 link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeOneTarget;

    /**
     * To check cta type 1 seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean ctaTypeOneNoFollow;

    /**
     * The Image for cta type 2.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeTwoIcon;

    /**
     * The Alt text for cta type 2.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeTwoAltText;

    /**
     * The Text for cta Type 2 label.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeTwoLabel;

    /**
     * The URL for cta type 2 link.
     */
    @ValueMapValue
    private String ctaTypeTwoLinkURL;

    /**
     * To check the cta type 2 link should open in a new tab.
     */
    @ValueMapValue
    @Getter
    private String ctaTypeTwoTarget;

    /**
     * To check cta type 2 seo is enabled or not
     */
    @ValueMapValue
    @Getter
    private boolean ctaTypeTwoNoFollow;

    /**
     * @return the ctaTypeOneLinkURL
     */
    @Override
    public String getCtaTypeOneLinkURL() {
        return PlatformUtils.isInternalLink(resourceResolver, ctaTypeOneLinkURL);
    }

    /**
     * @return the ctaTypeTwoLinkURL
     */
    @Override
    public String getCtaTypeTwoLinkURL() {
        return PlatformUtils.isInternalLink(resourceResolver, ctaTypeTwoLinkURL);
    }

    /**
     * @return the ctaTypeOneLinkURLType
     */
    @Override
    public String getCtaTypeOneLinkURLType() {
        String ctaLinkOneType = PlatformUtils.getUrlLinkType(ctaTypeOneLinkURL);
        return PlatformUtils.getInteractionType(TCBConstants.NAV_PANEL_TITLE, ctaLinkOneType);
    }

    /**
     * @return the ctaTypeTwoLinkURLType
     */
    @Override
    public String getCtaTypeTwoLinkURLType() {
        String ctaLinkTwoType = PlatformUtils.getUrlLinkType(ctaTypeTwoLinkURL);
        return PlatformUtils.getInteractionType(TCBConstants.NAV_PANEL_TITLE, ctaLinkTwoType);
    }

    /**
     * Returns the web image rendition path for the cta Type One Icon.
     */
    @Override
    public String getCtaTypeOneIconWebImagePath() {
        return PlatformUtils.getWebImagePath(ctaTypeOneIcon);
    }

    /**
     * Returns the mobile image rendition path for the cta Type One Icon.
     */
    @Override
    public String getCtaTypeOneIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(ctaTypeOneIcon);
    }

    /**
     * Returns the web image rendition path for the cta Type Two Icon.
     */
    @Override
    public String getCtaTypeTwoIconWebImagePath() {
        return PlatformUtils.getWebImagePath(ctaTypeTwoIcon);
    }

    /**
     * Returns the mobile image rendition path for the cta Type Two Icon.
     */
    @Override
    public String getCtaTypeTwoIconMobileImagePath() {
        return PlatformUtils.getMobileImagePath(ctaTypeTwoIcon);
    }

}
