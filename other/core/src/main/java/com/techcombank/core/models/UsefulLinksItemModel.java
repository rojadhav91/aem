package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class UsefulLinksItemModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class UsefulLinksItemModel {

    /**
     * Title
     * - Description: Title for a useful link item
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Banner
     * - Description: Only accept png, jpg, jpeg type.
    */
    @ValueMapValue
    @Getter
    private String banner;

    /**
     * Slug
     * - Description: Redirection link used when users click on the useful link item.
    */
    @ValueMapValue
    @Getter
    private String slug;

}
