package com.techcombank.core.service.impl;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticlesRelatedPostService;
import com.techcombank.core.utils.ArticleUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = ArticlesRelatedPostService.class, immediate = true)
@Designate(ocd = ArticlesRelatedPostServiceImpl.ArticleRelatedPostConfig.class)
public class ArticlesRelatedPostServiceImpl implements ArticlesRelatedPostService {

    private static final Logger LOG = LoggerFactory.getLogger(ArticlesRelatedPostServiceImpl.class);

    @Override
    public List<Articles> getArticlesRelatedPosts(final Resource resource, final Page currentPage,
                                                  final ResourceResolver resourceResolver) {

        List<Articles> relatedArticlesList = new ArrayList<>();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            Resource articleResource = currentPage.getContentResource();
            String currentPagePath = currentPage.getPath();
            LOG.info("Current Page Path : {}", currentPagePath);
            String locale = currentPage.getLanguage().toString();
            ValueMap valueMap = articleResource.getValueMap();
            String[] keywordsTag = new String[] {};
            if (valueMap.containsKey(TCBConstants.CQ_TAGS)) {
                keywordsTag = valueMap.get(TCBConstants.CQ_TAGS, String[].class);
            }
            String category = valueMap.get(TCBConstants.CATEGORY_TAG, StringUtils.EMPTY);

            Map<String, String> articleMap = new HashMap<>();
            articleMap.put(TCBConstants.PATH,
                    TCBConstants.TCB_WEB_BASE_PATH + locale + articleRelatedPostConfig.getDefaultPath());
            articleMap.put(TCBConstants.TYPE, TCBConstants.CQ_PAGE);
            articleMap.put(TCBConstants.PROPERTY_1, TCBConstants.JCR_CONTENT + TCBConstants.CQ_TEMPLATE);
            articleMap.put(TCBConstants.PROPERTYVALUE_1, TCBConstants.ARTICLE_PAGE_TEMPLATE_PATH);
            articleMap.put(TCBConstants.PROPERTY_3, TCBConstants.JCR_CONTENT + TCBConstants.CATEGORY_TAG);
            articleMap.put(TCBConstants.PROPERTYVALUE_3, category);
            for (int i = 0; i < keywordsTag.length; i++) {
                articleMap.put(TCBConstants.PROPERTY_2, TCBConstants.JCR_CONTENT + TCBConstants.CQ_TAGS);
                articleMap.put(TCBConstants.PROPERTY_2 + TCBConstants.DOT + (i + 1) + TCBConstants.UNDERSCORE_VALUE,
                        keywordsTag[i]);
            }
            articleMap.put(TCBConstants.ORDER_BY,
                    TCBConstants.AT + TCBConstants.JCR_CONTENT + TCBConstants.ARTICLE_CREATION_DATE);
            articleMap.put(TCBConstants.ORDER_BY_SORT, TCBConstants.DESCENDING);
            articleMap.put(TCBConstants.RESULT_LIMIT, TCBConstants.ALL_RESULT);
            QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
            Query query = queryBuilder.createQuery(PredicateGroup.create(articleMap), session);

            SearchResult result = query.getResult();

            if (result.getHits().size() > TCBConstants.THREE) {
                relatedArticlesList = ArticleUtils.getArticles(result, currentPagePath, resourceResolver,
                        TCBConstants.ARTICLE_RELATED_POST_VALUE);
            } else {
                for (int i = 0; i < keywordsTag.length; i++) {
                    articleMap.remove(TCBConstants.PROPERTY_2, TCBConstants.JCR_CONTENT + TCBConstants.CQ_TAGS);
                    articleMap.remove(
                            TCBConstants.PROPERTY_2 + TCBConstants.DOT + (i + 1) + TCBConstants.UNDERSCORE_VALUE,
                            keywordsTag[i]);
                }
                query = queryBuilder.createQuery(PredicateGroup.create(articleMap), session);
                result = query.getResult();
                if (result.getHits().size() > TCBConstants.THREE) {
                    relatedArticlesList = ArticleUtils.getArticles(result, currentPagePath, resourceResolver,
                            TCBConstants.ARTICLE_RELATED_POST_VALUE);
                }
            }
            return relatedArticlesList;
        } catch (RepositoryException ex) {
            LOG.error("Exception while constructing data for articles : {0}", ex);
        }
        return relatedArticlesList;
    }

    private ArticlesRelatedPostServiceImpl.ArticleRelatedPostConfig articleRelatedPostConfig;

    @Activate
    @Modified
    public void activate(final ArticlesRelatedPostServiceImpl.ArticleRelatedPostConfig config) {
        this.articleRelatedPostConfig = config;
    }

    @ObjectClassDefinition(name = "Configuration for Article Related Posts",
            description = "Configuration to provide default path for Articles")
    public @interface ArticleRelatedPostConfig {
        @AttributeDefinition(name = "Root Path for Articles", description = "Root Path for articles",
                type = AttributeType.STRING)
        String getDefaultPath() default "/thong-tin/";
    }
}
