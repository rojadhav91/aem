package com.techcombank.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.techcombank.core.utils.PlatformUtils;

import lombok.Getter;

/**
 * The Class MastheadExtendModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = MastheadExtendModel.class)
public class MastheadExtendModel {
    /**
     * Title
     * - Description: To author title text
    */
    @ValueMapValue
    @Getter
    private String title;

    /**
     * Description
     * - Description: To author description text
    */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * Banner Top Background Color
     * - Description: To apply background color for the top part of the banner
    */
    @ValueMapValue
    @Getter
    private String bannerTopBackgroundColor;

    /**
     * Banner Bottom Background Color
     * - Description: To apply background color for the bottom part of the banner
    */
    @ValueMapValue
    @Getter
    private String bannerBottomBackgroundColor;

    /**
     * Apply Gradient Over Background Color
     * - Description: To apply gradient for the background colour
    */
    @ValueMapValue
    @Getter
    private boolean applyGradientOverBackgroundColor;

    /**
     * CTA Button Required
     * - Description: To author using CTA Button or not
    */
    @ValueMapValue
    @Getter
    private boolean ctaButtonRequired;

    /**
     * Phone Screenshot Image
     * - Description: Image used at the center of masthead, with phone boarder
    */
    @ValueMapValue
    @Getter
    private String phoneScreenshotImage;

    /**
    * Returns the mobile image rendition path for the Phone Screenshot Image.
    */
    public String getMobilePhoneScreenShotImagePath() {
        return PlatformUtils.getMobileImagePath(phoneScreenshotImage);
    }

    /**
    * Returns the web image rendition path for the Phone Screenshot Image.
    */
    public String getWebPhoneScreenShotImagePath() {
        return PlatformUtils.getWebImagePath(phoneScreenshotImage);
    }

    /**
     * Phone Screenshot Image Alt Text
     * - Description: To author alt text for phone screenshot image
    */
    @ValueMapValue
    @Getter
    private String phoneScreenshotImageAltText;

    /**
     * Masthead extend image list
     */
    @ChildResource
    @Getter
    private List<MastheadExtendImageModel> mastheadExtendImageList;

    /**
     * Masthead extend card article image list
     */
    @ChildResource
    @Getter
    private List<MastheadExtendCardArticleImageModel> mastheadExtendCardArticleImageList;

}
