package com.techcombank.core.models;

/**
 * Interface For Button Model
 *
 */
public interface ButtonModel {
    /**
     * URL for CTA link
     */
    String getCtaLink();

    /**
     * Interaction text for Analytics
     */
    String getInteractionNameType();

    /**
     * Modal Interaction text for Analytics
     */
    String getCtaModalInteractionNameType();

    /**
     * Returns the web image rendition path for the icon.
     */
    String getCtaIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the icon.
     */
    String getCtaIconMobileImagePath();

    /**
     * Returns the web image rendition path for the QR image.
     */
    String getCtaQRWebImagePath();

    /**
     * Returns the mobile image rendition path for the QR image.
     */
    String getCtaQRMobileImagePath();

}
