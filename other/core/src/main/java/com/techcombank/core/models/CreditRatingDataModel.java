package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * The Class CreditRatingDataModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CreditRatingDataModel {

    /**
     * Category Data
     * - Description: To author category data for selected year
    */
    @ValueMapValue
    @Getter
    private String categoryData;

    /**
     * Rating Data
     * - Description: To author rating data for selected year
    */
    @ValueMapValue
    @Getter
    private String ratingData;

}
