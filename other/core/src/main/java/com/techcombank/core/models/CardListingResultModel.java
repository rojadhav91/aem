package com.techcombank.core.models;

import com.techcombank.core.models.impl.CardListingModelImpl;

import java.util.List;

/**
 * Interface For CardListingResultModel
 */
public interface CardListingResultModel {

    /**
     * The getNudgeTagRoot.
     */
    String getNudgeTagRoot();
    /**
     * The getCardCFRootPath.
     */
    String getCardCFRootPath();
    /**
     * The getListingPage.
     */
    String getListingPage();
    /**
     * The getForLabel.
     */
    String getForLabel();
    /**
     * The getOffersLabel.
     */
    String getOffersLabel();
    /**
     * The getUtilitiesLabel.
     */
    String getUtilitiesLabel();
    /**
     * The getConditionsLabel.
     */
    String getConditionsLabel();
    /**
     * The getRatesLabel.
     */
    String getRatesLabel();
    /**
     * The getOutstandingLabel.
     */
    String getOutstandingLabel();
    /**
     * The getCardOffersLabel.
     */
    String getCardOffersLabel();
    /**
     * The getFeesLabel.
     */
    String getFeesLabel();
    /**
     * The getNudgeTags.
     */
    String getNudgeTags();
    /**
     * The getRegImg.
     */
    String getRegImg();
    /**
     * The getRegImgMbl.
     */
    String getRegImgMobile();
    /**
     * The getRegTitle.
     */
    String getRegTitle();
    /**
     * The getRegSubTitle.
     */
    String getRegSubTitle();
    /**
     * The getImageRegAlt.
     */
    String getImageRegAlt();
    /**
     * The getRegCtaMobile.
     */
    String getRegCtaMobile();
    /**
     * The getSteps.
     */
    List<CardListingModelImpl.RegisterSteps> getSteps();
    /**
     * The bottomText value.
     */
    String getBottomText();
    /**
     * The bottomLinkLabel value.
     */
    String getBottomLinkLabel();
    /**
     * The bottomLink value.
     */
    String getBottomLink();
    /**
     * The ctaBtmTarget value.
     */
    String getCtaBtmTarget();
    /**
     * The ctaBtmFollow value.
     */
    boolean isCtaBtmFollow();


}
