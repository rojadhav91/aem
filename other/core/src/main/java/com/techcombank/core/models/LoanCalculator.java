package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

/**
 * Sling model for loan calculator.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LoanCalculator {

    /**
     * Tab text for left panel.
     */
    @ValueMapValue
    @Getter
    private String tabText;

    /**
     * Value label text for left panel.
     */
    @ValueMapValue
    @Getter
    private String valueLabelText;

    /**
     * Value label tool tip for left panel.
     */
    @ValueMapValue
    @Getter
    private String valueLabelToolTip;

    /**
     * Value label input for left panel.
     */
    @ValueMapValue
    @Getter
    private String valueLabelInput;

    /**
     * Loan amount label text for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanAmountLabelText;

    /**
     * Loan amount tool tip for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanAmountToolTip;

    /**
     * Loan amount slider for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanAmountSlider;

    /**
     * Loan amount input for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanAmountInput;

    /**
     * Loan description for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanDescription;

    /**
     * Loan term label text for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanTermLabelText;

    /**
     * Loan term tool tip for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanTermToolTip;

    /**
     * Loan term input for left panel.
     */
    @ValueMapValue
    @Getter
    private String loanTermInput;

    /**
     * Interest rate label text for left panel.
     */
    @ValueMapValue
    @Getter
    private String interestRateLabelText;

    /**
     * Interest rate tool tip for left panel.
     */
    @ValueMapValue
    @Getter
    private String interestRateToolTip;

    /**
     * Interest rate input for left panel.
     */
    @ValueMapValue
    @Getter
    private String interestRateInput;

    /**
     * Disbursement date label text for left panel.
     */
    @ValueMapValue
    @Getter
    private String disbursementDateLabelText;

    /**
     * Disbursement date tool tip for left panel.
     */
    @ValueMapValue
    @Getter
    private String disbursementDateToolTip;

    /**
     * Disbursement date picker for left panel.
     */
    @ValueMapValue
    @Getter
    private String disbursementDatePicker;


    /**
     * header text for modal.
     */
    @ValueMapValue
    @Getter
    private String modalHeader;

    /**
     * label for Serial No.
     */
    @ValueMapValue
    @Getter
    private String serialNoLabel;

    /**
     * label for Repayment Period.
     */
    @ValueMapValue
    @Getter
    private String repaymentPeriodLabel;

    /**
     * label for Remaining Amount.
     */
    @ValueMapValue
    @Getter
    private String remainingAmountLabel;

    /**
     * label for Original Amount.
     */
    @ValueMapValue
    @Getter
    private String originalAmountLabel;

    /**
     * label for Interest Amount.
     */
    @ValueMapValue
    @Getter
    private String interestAmountLabel;

    /**
     * label for Total Principal + Interest .
     */
    @ValueMapValue
    @Getter
    private String sumLabel;

    /**
     * label for Total in the bottom of modal.
     */
    @ValueMapValue
    @Getter
    private String totalLabel;

    /**
     * label for Total in the bottom of modal.
     */
    @ValueMapValue
    @Getter
    private String tooltipIcon;
    @ValueMapValue
    @Getter
    private String tooltipAlt;

    @Getter
    private String tooltipIconMobile;

    @Inject
    @Getter
    private List<LoanCalculatorFieldsModel> calculatorFields;

    public String getTooltipIcon() {
        return PlatformUtils.getWebImagePath(tooltipIcon);
    }

    public String getTooltipIconMobile() {
        return PlatformUtils.getMobileImagePath(tooltipIcon);
    }

    public String getMonths() {
        LocalDate currentdate = LocalDate.now();
        return currentdate.getMonth() + " " + currentdate.getYear();
    }

    public String getTabs() {
        StringBuilder loanCalculatorTabs = new StringBuilder();
        if (calculatorFields != null) {
            for (int i = 0; i < calculatorFields.size(); i++) {
                if (null != calculatorFields.get(i).getTabName()) {
                    loanCalculatorTabs.append(calculatorFields.get(i).getTabName()).append(";");
                }
            }
            if (StringUtils.isNotEmpty(String.valueOf(loanCalculatorTabs))) {
                return loanCalculatorTabs.substring(0, loanCalculatorTabs.length() - 1);
            }
        }
        return StringUtils.EMPTY;
    }
}
