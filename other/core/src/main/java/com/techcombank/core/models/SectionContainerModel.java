package com.techcombank.core.models;

import lombok.Getter;
import lombok.Setter;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

/**
 * The Class SectionContainerModel.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = SectionContainerModel.class)
public class SectionContainerModel {

    /** The large BG image reference. */
    @ValueMapValue
    private String largeBGImageReference;

    /** The small BG image reference. */
    @ValueMapValue
    private String smallBGImageReference;

    /** The top padding. */
    @ValueMapValue
    @Default(values = "20")
    private String topPadding;

    /** The background color. */
    @ValueMapValue
    private String backgroundColor;

    /** The Title Text. */
    @ValueMapValue
    private String titleText;

    /** The text Color. */
    @ValueMapValue
    private String textColor;

    /** The bottom padding. */
    @ValueMapValue
    @Default (values = "20")
    private String bottomPadding;

    /** The left padding. */
    @ValueMapValue
    @Default (values = "0")
    private String leftPadding;

    /** The right padding. */
    @ValueMapValue
    @Default (values = "0")
    private String rightPadding;

    /** The container action. */
    private String containerAction;

    /**
     * Is background image rounded
     */
    @ValueMapValue
    @Getter
    @Setter
    private boolean backgroundImageRounded;

    /**
     * Inits the.
     */
    @PostConstruct
    protected void init() {

        /*
         * If both images are authored then large image will be displayed for desktop
         * and small for mobile.( ll -> large image  and sl - small image
         * bgColor ->  background color)
         */
        if (StringUtils.isNotEmpty(largeBGImageReference) && StringUtils.isNotEmpty(smallBGImageReference)) {
            containerAction = "ll-ll:sl-sl";
        }
        /*
         * large image not authored then desktop will display background color and small
         * image authored will display for mobile.
         */
        if (StringUtils.isEmpty(largeBGImageReference) && StringUtils.isNotEmpty(smallBGImageReference)) {
            containerAction = "ll-bgColor:sl-sl";
        }
        /*
         * If no images are authored both desktop and mobile will display Background
         * color.
         */
        if (StringUtils.isEmpty(largeBGImageReference) && StringUtils.isEmpty(smallBGImageReference)) {
            if (StringUtils.isNotEmpty(backgroundColor)) {
                containerAction = "bgColor";
            } else {
                containerAction = StringUtils.EMPTY;
            }

        }
        /* No small image then mobile will display large image. */
        if (StringUtils.isNotEmpty(largeBGImageReference) && StringUtils.isEmpty(smallBGImageReference)) {
            containerAction = "ll-ll:sl-ll";
        }
    }

    /**
     * Gets the background color.
     *
     * @return the background color
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets the background color.
     *
     * @param bgColor the new background color
     */
    public void setBackgroundColor(final String bgColor) {
        this.backgroundColor = bgColor;
    }

    /**
     * Gets the large BG image reference.
     *
     * @return the large BG image reference
     */
    public String getLargeBGImageReference() {
        return largeBGImageReference;
    }

    /**
     * Sets the large BG image reference.
     *
     * @param largeBGImage the new large BG image reference
     */
    public void setLargeBGImageReference(final String largeBGImage) {
        this.largeBGImageReference = largeBGImage;
    }

    /**
     * Gets the small BG image reference.
     *
     * @return the small BG image reference
     */
    public String getSmallBGImageReference() {
        return smallBGImageReference;
    }

    /**
     * Sets the small BG image reference.
     *
     * @param smallBGImage the new small BG image reference
     */
    public void setSmallBGImageReference(final String smallBGImage) {
        this.smallBGImageReference = smallBGImage;
    }

    /**
     * Gets the top padding.
     *
     * @return the top padding
     */
    public String getTopPadding() {
        return topPadding;
    }

    /**
     * Sets the top padding.
     *
     * @param tpPadding the new top padding
     */
    public void setTopPadding(final String tpPadding) {
        this.topPadding = tpPadding;
    }

    /**
     * Gets the bottom padding.
     *
     * @return the bottom padding
     */
    public String getBottomPadding() {
        return bottomPadding;
    }

    /**
     * Sets the bottom padding.
     *
     * @param btPadding the new bottom padding
     */
    public void setBottomPadding(final String btPadding) {
        this.bottomPadding = btPadding;
    }

    /**
     * Gets the container action.
     *
     * @return the container action
     */
    public String getContainerAction() {
        return containerAction;
    }

    /**
     * Sets the container action.
     *
     * @param containerAction the new container action
     */
    public void setContainerAction(final String ctAction) {
        this.containerAction = ctAction;
    }

    /**
     * Returns the web image rendition path for the image.
     */
    public String getWebImagePath() {
        return PlatformUtils.getWebImagePath(largeBGImageReference);
    }

    /**
     * Returns the mobile image rendition path for the image.
     */
    public String getMobileImagePath() {
        return PlatformUtils.getMobileImagePath(smallBGImageReference);
    }

    /**
     * Gets the Title Text.
     *
     * @return the title text
     */
    public String getTitleText() {
        return titleText;
    }

    /**
     * Sets the Title Text
     *
     * @param text the new title text
     */
    public void setTitleText(final String text) {
        this.titleText = text;
    }

    /**
     * Gets the text color.
     *
     * @return the text color
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * Sets the text color.
     *
     * @param tColor the new text color
     */
    public void setTextColor(final String tColor) {
        this.textColor = tColor;
    }

    /**
     * Gets the left padding.
     *
     * @return the left padding
     */
    public String getLeftPadding() {
        return leftPadding;
    }

    /**
     * Sets the left padding.
     *
     * @param ltPadding the new left padding
     */
    public void setLeftPadding(String ltPadding) {
        this.leftPadding = ltPadding;
    }

    /**
     * Gets the right padding.
     *
     * @return the right padding
     */
    public String getRightPadding() {
        return rightPadding;
    }

    /**
     * Sets the bottom padding.
     *
     * @param rtPadding the new right padding
     */
    public void setRightPadding(String rtPadding) {
        this.rightPadding = rtPadding;
    }
}
