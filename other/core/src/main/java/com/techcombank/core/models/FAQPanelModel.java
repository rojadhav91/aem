package com.techcombank.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.FAQ;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FAQPanelModel {

    @Inject
    private String faqCFFolderPath;

    @Inject
    @Getter
    private String viewMoreLabel;

    @Getter
    private List<FAQ> faqCFList;

    @Inject
    private ResourceResolver resourceResolver;

    @PostConstruct
    public void init() {
        faqCFList = new ArrayList<>();
        Resource resource = resourceResolver.resolve(faqCFFolderPath);
        if (resource != null) {
            Iterable<Resource> childResources = resource.getChildren();
            for (Resource child : childResources) {
                ContentFragment contentFragment = child.adaptTo(ContentFragment.class);
                if (contentFragment != null) {
                    FAQ faq = new FAQ();
                    faq.setFaqTitle(contentFragment.getElement(TCBConstants.FAQ_TITLE).getContent());
                    faq.setFaqDescription(contentFragment.getElement(TCBConstants.FAQ_DESCRIPTION).getContent());
                    faqCFList.add(faq);
                }
            }
        }
    }
}
