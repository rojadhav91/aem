package com.techcombank.core.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * This Model represents a complex panel.
 */
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ComplexPanelModel {

    /**
     * The title of the panel.
     */
    @ValueMapValue
    @Getter
    private String panelTitle;

    /**
     * The description of the panel.
     */
    @ValueMapValue
    @Getter
    private String description;

    /**
     * The dateText of the panel.
     */
    @ValueMapValue
    @Getter
    private String dateText;

    /**
     * The path of the topImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String topImage;

    /**
     * The alt text for the topImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String topAltText;

    /**
     * The path of the bottomImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String bottomImage;

    /**
     * The alt text for the bottomImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String bottomAltText;

    /**
     * The path of the rightImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String rightImage;

    /**
     * The alt text for the rightImage in the panel.
     */
    @ValueMapValue
    @Getter
    private String rightAltText;
}
