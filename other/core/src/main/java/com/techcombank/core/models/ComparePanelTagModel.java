package com.techcombank.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = ComparePanelTagModel.class)
public class ComparePanelTagModel {

    @ValueMapValue
    @Getter
    @Setter
    private String tagId;

    @ValueMapValue
    @Getter
    @Setter
    private String tagTitle;

    @ValueMapValue
    @Getter
    @Setter
    private String tagPath;

    @ValueMapValue
    @Getter
    @Setter
    private List<ComparePanelTagModel> childTags;

}
