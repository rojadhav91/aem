package com.techcombank.core.models;

/**
 * Interface For QuickAccessFileModel
 *
 */
public interface QuickAccessFileModel {

    /**
     * File type
     */
    String getFileType();

    /**
     * Returns the web image rendition path for the file icon.
     */
    String getFileIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the file icon.
     */
    String getFileIconMobileImagePath();

}
