package com.techcombank.core.models;

/**
 * Interface For FixingRateModel
 *
 */
public interface FixingRateModel {

    /**
     * Returns the web image rendition path for the day icon.
     */
    String getDayIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the day icon.
     */
    String getDayIconMobileImagePath();

    /**
     * Returns the web image rendition path for the time icon.
     */
    String getTimeIconWebImagePath();

    /**
     * Returns the mobile image rendition path for the time icon.
     */
    String getTimeIconMobileImagePath();

}
