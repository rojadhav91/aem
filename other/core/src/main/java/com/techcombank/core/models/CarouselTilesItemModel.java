package com.techcombank.core.models;

import com.techcombank.core.utils.PlatformUtils;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

/**
 * This model class is for define each Tile of Carousel Tiles
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CarouselTilesItemModel {

    /**
     * Card image
     */
    @ValueMapValue
    @Getter
    private String cardImage;

    /**
     * Card image type
     */
    @ValueMapValue
    @Getter
    private String cardImageType;

    /**
    * Returns the mobile image rendition path for the card image.
    */
    public String getMobileCardImagePath() {
        return PlatformUtils.getMobileImagePath(cardImage);
    }

    /**
    * Returns the web image rendition path for the card image.
    */
    public String getWebCardImagePath() {
        return PlatformUtils.getWebImagePath(cardImage);
    }

    /**
     * Card image Alt text
     */
    @ValueMapValue
    @Getter
    private String cardImageAltText;

    /**
     * Theme
     */
    @ValueMapValue
    @Getter
    private String theme;

    /**
     * Nudge Text
     */
    @ValueMapValue
    @Getter
    private String nudgeText;

    /**
     * Card title
     */
    @ValueMapValue
    @Getter
    private String cardTitle;

    /**
     * Card description
     */
    @ValueMapValue
    @Getter
    private String cardDescription;

    /**
     * Link Text
     */
    @ValueMapValue
    @Getter
    private String linkText;

    /**
     * Link Url
     */
    @ValueMapValue
    @Getter
    private String linkUrl;

    /**
     * Open in new tab
     */
    @ValueMapValue
    @Getter
    private boolean openInNewTab;

    /**
     * No follow for SEO
     */
    @ValueMapValue
    @Getter
    private boolean noFollow;

    /**
     * Returns the  interaction type for analytics.
     */
    public String getWebInteractionType() {
        return PlatformUtils.getUrlLinkType(linkUrl);
    }
}
