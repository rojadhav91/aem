package com.techcombank.core.migration;

import com.adobe.aemds.guide.utils.JcrResourceConstants;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.NameConstants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.ResourceResolverService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.day.cq.commons.jcr.JcrConstants.JCR_PRIMARYTYPE;
import static com.day.cq.tagging.TagConstants.PN_TAGS;
import static com.techcombank.core.constants.TCBConstants.COMMA;
import static com.techcombank.core.constants.TCBConstants.DASH;
import static com.techcombank.core.constants.TCBConstants.DOT;
import static com.techcombank.core.constants.TCBConstants.FALSE;
import static com.techcombank.core.constants.TCBConstants.PDFEXTENSION;
import static com.techcombank.core.constants.TCBConstants.SEO_PRIORITY;
import static com.techcombank.core.constants.TCBConstants.SLASH;
import static com.techcombank.core.constants.TCBConstants.SPACE;

@Component(service = {Servlet.class})
@SlingServletPaths(value = "/bin/articlemigration")
@Slf4j
public class MigrationServlet extends SlingAllMethodsServlet {
    private static final String API_URL = "https://techcombank.com/api/data/articles/";
    private static final String LOG_RESPONSE_INFO = "Received HTTP response code: {}, message {}";
    private static final String START = "_start";
    private static final String CLASS = "class";
    private static final String STYLE = "style";
    private static final String LIMIT = "_limit";
    private static final String MAX_LIMIT = "ignoreMaxLimit";
    private String maxLimit = "100";
    private static final String CHAR_UTF_8 = "UTF-8";
    private static final String SEPARATOR = "\n<== ******** ==>\n";
    private static final String EXCEL_LOC = "Check The Excel In This Location -> ";
    private static final String ERROR = "Errors:::";
    private static final String TEMPLATE = "/conf/techcombank/settings/wcm/templates/article-page";
    private static final String RESOURCE_TYPE = "techcombank/components/structure/article-page";
    private static final String LEVEL_PAGE_TEMPLATE = "/conf/techcombank/settings/wcm/templates/content-page";
    private static final String LEVEL_PAGE_RESOURCE_TYPE = "techcombank/components/structure/contentpage";
    private static final String EXCEL_FILE_PATH = SLASH + "content/dam/techcombank/migration/article-migration.xlsx";
    @Reference
    private transient ResourceResolverService resourceResolverService;
    private int count;
    private int archiveCount;
    private transient Gson gson;
    private List<String> allImagePaths;
    private List<String> errorTags;
    private List<String> duplicatePagePaths;
    private List<String> noHtmlUrls;
    private List<String> slugIssues;
    private List<String> imgStyles;
    private List<String> spanStyles;
    private List<String> pStyles;
    private List<String> pdfList;
    private int startExcel;
    private int limitExcel;

    @Override
    protected void doPost(@NonNull final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException {
        startExcel = 0;
        count = 0;
        limitExcel = Integer.parseInt(maxLimit);
        gson = new Gson();
        allImagePaths = new ArrayList<>();
        slugIssues = new ArrayList<>();
        imgStyles = new ArrayList<>();
        spanStyles = new ArrayList<>();
        pStyles = new ArrayList<>();
        pdfList = new ArrayList<>();
        errorTags = new ArrayList<>();
        duplicatePagePaths = new ArrayList<>();
        noHtmlUrls = new ArrayList<>();
        response.setContentType(TCBConstants.APPLICATION_JSON);
        response.setCharacterEncoding(CHAR_UTF_8);
        JsonObject resObjVi;
        JsonObject resObjEn;
        List<NameValuePair> requestParamMap = new ArrayList<>();
        getReqParams(request);
        try (ResourceResolver resolver = resourceResolverService.getWriteResourceResolver()) {
            int countEN = 0;
            List<ExcelPojo> excelData = new ArrayList<>();
            if (getExcelData(response, resolver, startExcel, limitExcel, excelData)) {
                return;
            }
            for (ExcelPojo excelDatum : excelData) {
                resObjVi = httpGetExecute(requestParamMap, response, excelDatum.getViId());
                Article articleVi = gson.fromJson(resObjVi, Article.class);
                response.getWriter().println("Start | " + excelDatum.getViId());
                if (articleVi.getId() != 0) {
                    articleVi.setExcelPojo(excelDatum);
                    checkValidName(articleVi.getExcelPojo().getViAemUrl(), response, articleVi);
                    if (Objects.isNull(resolver.getResource(articleVi.getExcelPojo().getViAemUrl()))) {
                        createPage(articleVi, resolver, response, articleVi.getExcelPojo().getViAemUrl());
                    } else {
                        response.getWriter().println(
                                "Page already exists " + articleVi.getId() + articleVi.getExcelPojo().getViAemUrl());
                        duplicatePagePaths.add(articleVi.getExcelPojo().getViAemUrl());
                    }
                }
                response.getWriter().println("End | " + excelDatum.getViId() + "\n");
                if (!excelDatum.getEnId().equals("0")) {
                    countEN++;
                    resObjEn = httpGetExecute(requestParamMap, response, excelDatum.getEnId());
                    Article articleEn = gson.fromJson(resObjEn, Article.class);
                    response.getWriter().println("Start | " + excelDatum.getEnId());
                    if (articleEn.getId() != 0) {
                        articleEn.setExcelPojo(excelDatum);
                        enArticlesPages(response, resolver, articleEn);
                    }
                    response.getWriter().println("End | " + excelDatum.getEnId());
                    response.getWriter().println(" ");
                }
            }
            scriptResultOutPut(response, countEN, resolver);
            extraInformation(response);
        } catch (URISyntaxException e) {
            response.getWriter().println("Error in do post | " + e.getMessage());
            log.error(" httpGetExecute | {}", e.getMessage());
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void scriptResultOutPut(SlingHttpServletResponse response, int countEN, ResourceResolver resolver)
            throws IOException {
        slugIssues = slugIssues.stream().distinct().collect(Collectors.toList());
        if (count > 0 && (count == limitExcel || ((count + slugIssues.size()) == (limitExcel - (
                duplicatePagePaths.size() - countEN))))) {
            resolver.commit();
            response.getWriter().println("\n<=== Script success, saved data, check response output ===>\n");
        } else {
            response.getWriter().println("\n<=== Script failed to save data check response output ===>\n");
        }
        response.getWriter().println("created count " + count);
        response.getWriter().println("limit " + limitExcel);
        response.getWriter().println("en count " + countEN);
        response.getWriter().println("vi count " + (count - countEN));
        response.getWriter().println("archive count " + archiveCount);
        response.getWriter().println("duplicate count " + duplicatePagePaths.size());
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void checkValidName(String path, SlingHttpServletResponse response, Article art) throws IOException {
       String pageName = path.substring(path.lastIndexOf("/") + 1);
       if (!JcrUtil.isValidName(pageName)) {
           response.getWriter().println("Illegal page Name | " + pageName);
           pageName = pageName.replace(".", "-");
           response.getWriter().println("Updated page Name | " + pageName);
           path = path.substring(0, path.lastIndexOf("/") + 1) + pageName;
           if (art.getLocale().equals("en")) {
               art.getExcelPojo().setEnAemUrl(path);
           }
           if (art.getLocale().equals("vi")) {
               art.getExcelPojo().setViAemUrl(path);
           }
       }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void enArticlesPages(SlingHttpServletResponse response, ResourceResolver resolver, Article article)
            throws IOException {
        checkValidName(article.getExcelPojo().getEnAemUrl(), response, article);
        if (Objects.isNull(resolver.getResource(article.getExcelPojo().getEnAemUrl()))) {
            createPage(article, resolver, response, article.getExcelPojo().getEnAemUrl());
        } else {
            response.getWriter()
                    .println("Page already exists " + article.getId() + article.getExcelPojo().getEnAemUrl());
            duplicatePagePaths.add(article.getExcelPojo().getEnAemUrl());
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void extraInformation(SlingHttpServletResponse response) throws IOException {
        allImagePaths = allImagePaths.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("Image Paths are :: " + allImagePaths.size());
        for (String path : allImagePaths) {
            response.getWriter().println(path);
        }
        response.getWriter().println(SEPARATOR);
        duplicatePagePaths = duplicatePagePaths.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("Duplicate Page Paths - Errors :: " + duplicatePagePaths.size());
        for (String path : duplicatePagePaths) {
            response.getWriter().println(path);
        }
        response.getWriter().println(SEPARATOR);
        errorTags = errorTags.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("Tags - Errors :: " + errorTags.size());
        for (String path : errorTags) {
            response.getWriter().println(path);
        }
        response.getWriter().println(SEPARATOR);
        response.getWriter().println("No HTML Content - Errors :: " + noHtmlUrls.size());
        for (String path : noHtmlUrls) {
            response.getWriter().println(path);
        }
        response.getWriter().println(SEPARATOR);
        response.getWriter().println("Slug Mismatch - Errors :: " + slugIssues.size());
        for (String path : slugIssues) {
            response.getWriter().println(path);
        }
        response.getWriter().println(SEPARATOR);
        imgStyles = imgStyles.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("Image Styles :: " + imgStyles.size());
        for (String style : imgStyles) {
            response.getWriter().println(style + "\n");
        }
        response.getWriter().println(SEPARATOR);
        spanStyles = spanStyles.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("Span Styles :: " + spanStyles.size());
        for (String style : spanStyles) {
            response.getWriter().println(style + "\n");
        }
        response.getWriter().println(SEPARATOR);
        pStyles = pStyles.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("P Styles :: " + pStyles.size());
        for (String style : pStyles) {
            response.getWriter().println(style + "\n");
        }
        response.getWriter().println(SEPARATOR);
        pdfList = pdfList.stream().distinct().collect(Collectors.toList());
        response.getWriter().println("PDF List :: " + pdfList.size());
        for (String pdf : pdfList) {
            response.getWriter().println(pdf + "\n");
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void getReqParams(final SlingHttpServletRequest request) {
        if (StringUtils.isNotEmpty(request.getParameter(START))) {
            startExcel = Integer.parseInt(request.getParameter(START));
        }
        if (StringUtils.isNotEmpty(request.getParameter(LIMIT))) {
            limitExcel = Integer.parseInt(request.getParameter(LIMIT));
            if (StringUtils.isNotEmpty(request.getParameter(MAX_LIMIT)) && request.getParameter(MAX_LIMIT).equals(
                    "true") && limitExcel > 0) {
                maxLimit = String.valueOf(limitExcel);
            }
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private boolean getExcelData(SlingHttpServletResponse response, ResourceResolver resolver, int start,
                                        int limit, List<ExcelPojo> excelData) throws IOException {
        Resource excelResource = resolver.getResource(EXCEL_FILE_PATH);
        if (start < 2) {
            response.getWriter().println(SEPARATOR);
            response.getWriter().println("Excel Sheet Data Starts From 2nd Row, Try Start Number Greater Than 1");
            return true;
        }
        if (limit > Integer.parseInt(maxLimit)) {
            response.getWriter().println(SEPARATOR);
            response.getWriter().println("Max Limit Allowed Is " + maxLimit + ", Try Again...!!");
            return true;
        }
        if (limit < 1) {
            response.getWriter().println(SEPARATOR);
            response.getWriter().println("Min Limit Allowed Is 1, Try Again...!!");
            return true;
        }
        if (Objects.isNull(excelResource)) {
            response.getWriter().println(SEPARATOR);
            response.getWriter().println("Excel Sheet Not Found ");
            response.getWriter().println("Upload The Excel In This Location -> " + EXCEL_FILE_PATH);
            return true;
        }
        Asset asset = Objects.requireNonNull(excelResource).adaptTo(Asset.class);
        if (Objects.isNull(asset)) {
            response.getWriter().println(SEPARATOR);
            response.getWriter().println("Excel Sheet Issue ");
            response.getWriter().println(EXCEL_LOC + EXCEL_FILE_PATH);
            return true;
        }
        InputStream excelInputStream = Objects.requireNonNull(asset).getOriginal().getStream();
        Workbook workbook = new XSSFWorkbook(excelInputStream);
        if (Objects.nonNull(workbook.getSheetAt(0))) {
            Sheet sheet = workbook.getSheetAt(0);
            if (sheet.getLastRowNum() < start + limit - 2) {
                response.getWriter().println(SEPARATOR);
                response.getWriter().println("Given Limit Exceeded The Rows In Excel " + sheet.getLastRowNum());
                response.getWriter().println(EXCEL_LOC + EXCEL_FILE_PATH);
                return true;
            }
            DataFormatter dataFormatter = new DataFormatter();
            for (int n = start - 1; n < start + limit - 1; n++) {
                Row row = sheet.getRow(n);
                ExcelPojo info = new ExcelPojo();
                int i = 0;
                if (Objects.nonNull(row)) {
                    info.setViId(dataFormatter.formatCellValue(row.getCell(i)));
                    info.setViApiUrl(getStringCallValue(row, ++i));
                    i += 2;
                    info.setViAemUrl(getStringCallValue(row, ++i));
                    info.setEnId(getNumCallValue(row, ++i));
                    info.setEnApiUrl(getStringCallValue(row, ++i));
                    info.setAlias(getStringCallValue(row, ++i));
                    info.setEnAemUrl(getStringCallValue(row, ++i));
                    info.setViPrimaryTag(getStringCallValue(row, ++i));
                    info.setViSecondaryTag(getStringCallValue(row, ++i));
                    info.setImgPrefixPath(getStringCallValue(row, ++i));
                    i += 1;
                    info.setHideArticleRelatedPost(getStringCallValue(row, ++i));
                    info.setHideArticleTagCloud(getStringCallValue(row, ++i));
                    info.setHideTableOfContent(getStringCallValue(row, ++i));
                }
                excelData.add(info);
            }
        }
        if (excelData.isEmpty()) {
            response.getWriter().println("Excel Sheet Has No Data");
            response.getWriter().println(EXCEL_LOC + EXCEL_FILE_PATH);
            return true;
        }
        return false;
    }

    private static String getStringCallValue(final Row row, final int i) {
        if (Objects.nonNull(row.getCell(i))) {
            return row.getCell(i).getStringCellValue();
        } else {
            return StringUtils.EMPTY;
        }
    }

    private static String getNumCallValue(final Row row, final int i) {
        if (Objects.nonNull(row.getCell(i))) {
            return String.valueOf(row.getCell(i).getNumericCellValue()).replace(".0", StringUtils.EMPTY);
        } else {
            return StringUtils.EMPTY;
        }
    }

    @SuppressWarnings({"checkstyle:FinalParameters"})
    private void createPage(@NonNull Article art, final @NonNull ResourceResolver resolver,
                            @NonNull SlingHttpServletResponse response, @NonNull String pagePath) throws IOException {
        String pageName = pagePath.substring(pagePath.lastIndexOf("/") + 1);
        if (checkSlugApiUrlMatch(art)) {
            String path =
                    art.getLocale().equals("en") ? art.getExcelPojo().getEnApiUrl() : art.getExcelPojo().getViApiUrl();
            response.getWriter().println(ERROR + path + "::: API Slug Name Not Matching :::" + art.getSlug());
            slugIssues.add(art.getId() + "--" + path + "--Slug Mismatch-->" + art.getSlug());
            return;
        } else if (!JcrUtil.isValidName(pageName)) {
            response.getWriter().println(ERROR + "Illegal page Name | " + pageName);
            slugIssues.add(art.getId() + "--In Valid Name-->" + art.getSlug());
            log.error("Illegal page Name | {}", pageName);
            return;
        }
        Resource parentRes = getOrCreateParentResource(resolver, pagePath, response);
        Resource pageRes;
        Map<String, Object> props = new HashMap<>();
        appProperty(JCR_PRIMARYTYPE, NameConstants.NT_PAGE, props);
        try {
            pageRes = resolver.create(Objects.requireNonNull(parentRes), pageName, props);
            response.getWriter().println("Page Path | " + pageRes.getPath());
            if (pageRes.getPath().contains("/archive/")) {
                archiveCount++;
            }
            props = new HashMap<>();
            pageProperties(art, props, resolver, response);
            pageRes = addChildNode(resolver, pageRes, NameConstants.NN_CONTENT, props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("altValueFromDAM", FALSE, props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "core/wcm/components/image/v3/image", props);
            addChildNode(resolver, pageRes, "cq:featuredimage", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("title", art.getLocale().equals("en") ? "The content of the article" : "Nội dung bài viết",
                    props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/articletableofcontent", props);
            addChildNode(resolver, pageRes, "articletableofcontent", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("layout", "responsiveGrid", props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "wcm/foundation/components/responsivegrid", props);
            pageRes = addChildNode(resolver, pageRes, "root", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("editable", "true", props);
            appProperty("startLevel", "4", props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/extended-core/breadcrumb",
                    props);
            addChildNode(resolver, pageRes, "breadcrumb", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("titleText", art.getLocale().equals("en") ? "Related Stories" : "Các bài viết liên quan",
                    props);
            appProperty("viewMore", art.getLocale().equals("en") ? "View detail" : "Xem chi tiết", props);
            appProperty("image",
                "/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/default-thumbnail.png", props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/articlesrelatedpost",
                    props);
            addChildNode(resolver, pageRes, "articlerelatedpost", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("tagLabel", "Tag", props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/articleTagCloud",
                    props);
            addChildNode(resolver, pageRes, "articletagcloud", props, response);

            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/articleandevents", props);
            addChildNode(resolver, pageRes, "articleandevents", props, response);
            // commented to remove the mastheadsimplebanner as we are adding in page properties.
            // we will uncomment in future if we need to add banner images to mastheadsimplebanner
            /*Resource mastheadRes = addChildNode(resolver, pageRes, "articleandevents", props, response);
            if (Objects.nonNull(art.getMasthead())) {
                props = new HashMap<>();
                appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
                appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/mastheadsimplebanner",
                        props);
                appProperty("bannerTitle", art.getMasthead().getTitle(), props);
                appProperty("bannerDesc", art.getMasthead().getDescription(), props);
                if (Objects.nonNull(art.getMasthead().getBackgroundImage())) {
                    appProperty("bgBannerImgAlt",
                            art.getMasthead().getBackgroundImage().getDesktop().getAlternativeText(),
                            props);
                    String imgDesktop = addImagePrefix(art.getExcelPojo().getImgPrefixPath(),
                            art.getMasthead().getBackgroundImage().getDesktop().getUrl());
                    appProperty("bgBannerImage", imgDesktop, props);
                    String imgMbl = addImagePrefix(art.getExcelPojo().getImgPrefixPath(),
                            art.getMasthead().getBackgroundImage().getMobile().getUrl());
                    appProperty("bgBannerMobileImage", imgMbl, props);
                }
                addChildNode(resolver, mastheadRes, "mastheadsimplebanner", props, response);
            }*/
            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("layout", "responsiveGrid", props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/proxy/container", props);
            Resource contRes = addChildNode(resolver, pageRes, "container", props, response);

            addComponents(resolver, response, contRes, art);

            addSectionContainerSocialShare(art, resolver, response, pageRes);

            count++;
        } catch (ParseException e) {
            response.getWriter().println(ERROR + "in page creation | " + art.getId() + " | " + e.getMessage());
            log.error("create page Error | {}", e.getMessage());
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void addSectionContainerSocialShare(Article art, ResourceResolver resolver,
                                                SlingHttpServletResponse response,
                                                Resource pageRes) throws IOException {
        Map<String, Object> props;
        props = new HashMap<>();
        appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
        if (!art.getSections().isEmpty() && art.getSections().get(0).getBackgroundImage() != null
                && art.getSections().get(0).getBackgroundImage().getDesktop() != null && StringUtils.isNotEmpty(
                art.getSections().get(0).getBackgroundImage().getDesktop().getUrl())) {
            appProperty("bottomPadding", "0", props);
            appProperty("topPadding", "80", props);
            appProperty("cq:styleIds", new String[]{"1691126434216"}, props);
            appProperty("largeBGImageReference", addImagePrefix(art.getExcelPojo().getImgPrefixPath(),
                    art.getSections().get(0).getBackgroundImage().getDesktop().getUrl(), art.getId()), props);
        }
        appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/extended-core/sectioncontainer",
                props);
        Resource secContRes = addChildNode(resolver, pageRes, "sectioncontainer", props, response);

        props = new HashMap<>();
        appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
        if (art.getLocale().equals("en")) {
            appProperty("fragmentVariationPath",
                    "/content/experience-fragments/techcombank/web/vn/en/site/social-share/master", props);
        } else if (art.getLocale().equals("vi")) {
            appProperty("fragmentVariationPath",
                    "/content/experience-fragments/techcombank/web/vn/vi/site/social-share/master", props);
        }
        appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/proxy/experiencefragment", props);
        addChildNode(resolver, pageRes, "experiencefragment", props, response);

        if (!art.getSections().isEmpty() && art.getSections().get(0).getContent() != null && !art.getSections().get(0)
                .getContent().isEmpty() && StringUtils.isNotEmpty(
                art.getSections().get(0).getContent().get(0).getCtaTitle())) {
            props = new HashMap<>();
            String text =
                    "<h3><span style=\"font-weight: normal;\">" + art.getSections().get(0).getContent().get(0)
                            .getCtaTitle() + "</span></h3><h3>&nbsp;</h3>";
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("text", text, props);
            appProperty("textIsRich", true, props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/extended-core/text", props);
            addChildNode(resolver, secContRes, "text", props, response);
        }
        if (!art.getSections().isEmpty() && StringUtils.isNotEmpty(art.getSections().get(0).getUrl())
                && StringUtils.isNotEmpty(art.getSections().get(0).getNavText())
                && art.getSections().get(0).getBackgroundImage() != null
                && art.getSections().get(0).getBackgroundImage().getDesktop() != null && StringUtils.isNotEmpty(
                art.getSections().get(0).getBackgroundImage().getDesktop().getUrl())) {
            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty("contactRegisterCTA", FALSE, props);
            appProperty("ctaNoFollow", FALSE, props);
            appProperty("ctaQR", FALSE, props);
            appProperty("ctaTarget", "_blank", props);
            appProperty("ctaType", "default", props);
            appProperty("fontColor", "#FFFFFF", props);
            appProperty("ctaColor", "#000000", props);
            appProperty("ctaIcon",
                    "/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg", props);
            appProperty("ctaLabel", art.getSections().get(0).getNavText(), props);
            appProperty("ctaLink", "https://techcombank.com/" + art.getSections().get(0).getUrl(), props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/button", props);
            addChildNode(resolver, secContRes, "button", props, response);
        }
    }

    private boolean checkSlugApiUrlMatch(final Article art) {
        if (Objects.nonNull(art.getLocale())) {
            if (art.getLocale().equals("en") && art.getExcelPojo().getEnApiUrl().contains(art.getSlug())) {
                return false;
            } else {
                return !art.getLocale().equals("vi") || !art.getExcelPojo().getViApiUrl().contains(art.getSlug());
            }
        } else {
            return true;
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private Resource getOrCreateParentResource(ResourceResolver resolver, String pagePath,
                                               @NonNull SlingHttpServletResponse response) throws IOException {
        String parentPath = pagePath.substring(0, pagePath.lastIndexOf(SLASH));
        String[] parts = parentPath.split(SLASH);
        Resource res = null;
        StringBuilder resPath = new StringBuilder();
        // Commented to, not to add 404-page redirect for folder level pages
        //boolean firstItr = false;
        for (String part : parts) {
            resPath.append(SLASH).append(part);
            res = resolver.getResource(resPath.toString());
            if (Objects.isNull(res)) {
                String path = resPath.toString().replace((SLASH + part), StringUtils.EMPTY);
                res = resolver.getResource(path);
                Map<String, Object> props = new HashMap<>();
                appProperty(JCR_PRIMARYTYPE, NameConstants.NT_PAGE, props);
                res = resolver.create(Objects.requireNonNull(res), part, props);
                response.getWriter().println("Parent Page Path | " + res.getPath());
                props = new HashMap<>();
                // Commented to, not to add 404-page redirect for folder level pages
                /*if (Boolean.valueOf(firstItr).equals(false) && res.getPath().startsWith(TCB_BASE_PATH + "vn/vi")) {
                    appProperty(PN_REDIRECT_TARGET, "/content/techcombank/web/vn/vi/errors/404", props);
                }
                if (Boolean.valueOf(firstItr).equals(false) && res.getPath().startsWith(TCB_BASE_PATH + "vn/en")) {
                    appProperty(PN_REDIRECT_TARGET, "/content/techcombank/web/vn/en/errors/404", props);
                }*/
                appProperty(JCR_PRIMARYTYPE, JcrResourceConstants.CQ_PAGE_CONTENT, props);
                appProperty(NameConstants.NN_TEMPLATE, LEVEL_PAGE_TEMPLATE, props);
                appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, LEVEL_PAGE_RESOURCE_TYPE, props);
                appProperty(JcrConstants.JCR_TITLE, part, props);
                appProperty("pageTitle", part, props);
                appProperty(SEO_PRIORITY, "0.5", props);
                appProperty("changeFrequency", "always", props);
                addChildNode(resolver, res, NameConstants.NN_CONTENT, props, response);
                // Commented to, not to add 404-page redirect for folder level pages
                //firstItr = true;
            }
        }
        return res;
    }

    @SuppressWarnings({"checkstyle:FinalParameters"})
    private void pageProperties(Article art, Map<String, Object> props, final @NonNull ResourceResolver resolver,
                                @NonNull SlingHttpServletResponse response) throws IOException, ParseException {
        appProperty(JCR_PRIMARYTYPE, JcrResourceConstants.CQ_PAGE_CONTENT, props);
        appProperty(NameConstants.NN_TEMPLATE, TEMPLATE, props);
        appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, RESOURCE_TYPE, props);
        appProperty(JcrConstants.JCR_TITLE, art.getTitle(), props);
        appProperty("pageTitle", art.getTitle(), props);
        appProperty(JcrConstants.JCR_DESCRIPTION, art.getSubTitle(), props);
        if (StringUtils.isNotEmpty(art.getPublishedDate())) {
            Calendar cd = new GregorianCalendar();
            cd.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(art.getPublishedDate()));
            appProperty("articleCreationDate", cd, props);
        }
        appProperty(NameConstants.PN_PAGE_LAST_MOD_BY, art.getAuthor(), props);
        appProperty("seoTitle", art.getMetaTitle(), props);
        appProperty("metaDescription", art.getMetaDescription(), props);
        appProperty("metaKeyword", art.getMetaKeywords(), props);
        appProperty("cq:robotsTags", art.getMetaRobots(), props);
        appProperty("changeFrequency", art.getChangefreq(), props);
        appProperty("hideArticleRelatedPost", art.getExcelPojo().getHideArticleRelatedPost(), props);
        appProperty("hideArticleTagCloud", art.getExcelPojo().getHideArticleTagCloud(), props);
        appProperty("hideTableOfContent", art.getExcelPojo().getHideTableOfContent(), props);
        if (art.getLocale().equals("en") && StringUtils.isNotEmpty(art.getExcelPojo().getAlias())) {
            appProperty("sling:alias", art.getExcelPojo().getAlias(), props);
        }
        if (Objects.nonNull(art.getMasthead()) && Objects.nonNull(art.getMasthead().getBackgroundImage())) {
            String img = addImagePrefix(art.getExcelPojo().getImgPrefixPath(),
                    art.getMasthead().getBackgroundImage().getDesktop().getUrl(), art.getId());
            appProperty("articleImagePath", img, props);
        }
        addPageCategoryTags(art, props, resolver, response);
        if (StringUtils.isEmpty(art.getPriority())) {
            appProperty(SEO_PRIORITY, "0.5", props);
        } else {
            appProperty(SEO_PRIORITY, art.getPriority(), props);
        }
        if (Objects.nonNull(art.getMetaImage())) {
            appProperty("metaImage", addImagePrefix(art.getExcelPojo().getImgPrefixPath(), art.getMetaImage().getUrl(),
                            art.getId()),
                    props);
            appProperty("metaImageAlt", art.getMetaImage().getAlternativeText(), props);
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void addPageCategoryTags(Article art, Map<String, Object> props, ResourceResolver resolver,
                                     SlingHttpServletResponse response) throws IOException {
        if (!art.getExcelPojo().getViPrimaryTag().isEmpty() || !art.getExcelPojo().getViSecondaryTag().isEmpty()) {
            List<String> tagPrimary = Arrays.asList(StringUtils.split(art.getExcelPojo().getViPrimaryTag(), COMMA));
            List<String> tagSecondary = Arrays.asList(StringUtils.split(art.getExcelPojo().getViSecondaryTag(), COMMA));
            List<String> tagPPaths = getTagIDs(art, response, tagPrimary, resolver, true);
            if (!tagPPaths.isEmpty()) {
                String[] arr = tagPPaths.toArray(new String[0]);
                appProperty("categoryTag", arr, props);
            } else {
                response.getWriter().println("No primary tags added for this page");
            }
            List<String> tagSPaths = getTagIDs(art, response, tagSecondary, resolver, false);
            if (!tagSPaths.isEmpty()) {
                String[] arr = tagSPaths.toArray(new String[0]);
                appProperty(PN_TAGS, arr, props);
            } else {
                response.getWriter().println("No secondary tags added for this page");
            }
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private List<String> getTagIDs(Article art, SlingHttpServletResponse response, List<String> list,
                                   ResourceResolver resolver, boolean b)
            throws IOException {
        String pTag = "/content/cq:tags/techcombank/articles/article-category/article-primary-category/";
        String sTag = "/content/cq:tags/techcombank/articles/article-category/article-secondary-category/";
        List<String> tagPaths = new ArrayList<>();
        for (String tag : list) {
            if (StringUtils.isNotEmpty(tag)) {
                pTag = b ? pTag : sTag;
                Resource tagRes = resolver.getResource(pTag + tag);
                if (Objects.nonNull(tagRes)) {
                    if (b) {
                        tagPaths.add("techcombank:articles/article-category/article-primary-category/" + tag);
                    } else {
                        tagPaths.add("techcombank:articles/article-category/article-secondary-category/" + tag);
                    }
                } else {
                    errorTags.add(tag + " " + DASH + " " + art.getId());
                    response.getWriter().println(ERROR + "Tag not found |  " + pTag + tag);
                }
            }
        }
        return tagPaths;
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private static void appProperty(final @NonNull String propKey, final Object propValue,
                                    @NonNull Map<String, Object> props) {
        if (Objects.nonNull(propValue)) {
            props.put(propKey, propValue);
        }
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private void addComponents(final ResourceResolver resolver, SlingHttpServletResponse response, Resource pageRes,
                               Article art) throws IOException {
        Map<String, Object> props;
        int htmlSize = StringUtils.isNotBlank(art.getHtmlContent()) ? 1 : 0;
        int artCon = 0;
        int secCon = 0;

        Result result = getResult(art, artCon, secCon);

        if ((result.artCont + result.secCont + htmlSize) < 1) {
            response.getWriter().println("ZERO HTML CONTENT" + art.getId());
            if (art.getLocale().equals("vi")) {
                noHtmlUrls.add(art.getId() + "-" + art.getExcelPojo().getViApiUrl());
            } else {
                noHtmlUrls.add(art.getId() + "-" + art.getExcelPojo().getEnApiUrl());
            }
        }
        if ((result.artCont + result.secCont + htmlSize) > 1) {
            response.getWriter().println("TWO HTML CONTENT" + art.getId());
        }
        if (StringUtils.isNotEmpty(art.getHtmlContent())) {
            art.setHtmlContent(updateHeadingTagIds(art.getHtmlContent()));
            art.setHtmlContent(
                    changeImagePaths(art.getHtmlContent(), art.getExcelPojo().getImgPrefixPath(), art.getId()));
            props = new HashMap<>();
            appProperty(JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED, props);
            appProperty(ResourceResolver.PROPERTY_RESOURCE_TYPE, "techcombank/components/extended-core/text", props);
            appProperty("textIsRich", "true", props);
            appProperty("text", art.getHtmlContent().replace("\\t", ""), props);
            addChildNode(resolver, pageRes, "text", props, response);
        }

    }

    @SuppressWarnings({"checkstyle:FinalParameters", "checkstyle:MagicNumber"})
    private String updateHeadingTagIds(String htmlContent) {
        Document doc = Jsoup.parse(htmlContent);
        for (int h = 2; h < 4; h++) {
            Elements hTag = doc.getElementsByTag("h" + h);
            int k = 1;
            for (Element el : hTag) {
                String id = el.text();
                id = Normalizer.normalize(id, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
                id = id.replace(DOT, StringUtils.EMPTY);
                id = id.replace(" ", "-");
                el.attr("id", id.toLowerCase() + DASH + h + DASH + k++);
            }
        }
        return doc.body().html();
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private Result getResult(Article art, int artCon, int secCon) {
        if (StringUtils.isEmpty(art.getHtmlContent())) {
            art.setHtmlContent(StringUtils.EMPTY);
        }
        if (!art.getArticleContent().isEmpty() && null != art.getArticleContent().get(0).getBannerMediaSource()
                && null != art.getArticleContent().get(0).getBannerMediaSource().getUrl()) {
            art.setHtmlContent(
                    art.getHtmlContent() + "<img src=" + art.getArticleContent().get(0).getBannerMediaSource().getUrl()
                            + ">");
        }

        for (int u = 0; u < art.getArticleContent().size(); u++) {
            if (null != art.getArticleContent().get(u).getHtmlContent()) {
                art.setHtmlContent(art.getHtmlContent() + art.getArticleContent().get(u).getHtmlContent());
                artCon = 1;
            }
        }

        for (int u = 0; u < art.getSections().size(); u++) {
            if (null != art.getSections().get(u).getContent().get(0).getTitle()) {
                art.setHtmlContent(
                        art.getHtmlContent() + "<h3>" + art.getSections().get(u).getContent().get(0).getTitle()
                                + "</h3>");
            }
            if (null != art.getSections().get(u).getContent().get(0).getDescription()) {
                art.setHtmlContent(
                        art.getHtmlContent() + art.getSections().get(u).getContent().get(0).getDescription());
                secCon = 1;
            }
        }

        if (!art.getSections().isEmpty() && null != art.getSections().get(0).getContent().get(0).getContentHtml()) {
            art.setHtmlContent(art.getHtmlContent() + art.getSections().get(0).getContent().get(0).getContentHtml());
            secCon = 1;
        }
        if (!art.getSections().isEmpty() && art.getSections().size() > 1
                && null != art.getSections().get(1).getContent()
                && !art.getSections().get(1).getContent().isEmpty()
                && null != art.getSections().get(1).getContent().get(0).getContentHtml()) {
            art.setHtmlContent(art.getHtmlContent() + art.getSections().get(1).getContent().get(0).getContentHtml());
            secCon = 1;
        }

        return new Result(artCon, secCon);
    }

    private static class Result {
        private final int artCont;
        private final int secCont;

        Result(final int artCon, final int secCon) {
            this.artCont = artCon;
            this.secCont = secCon;
        }
    }

    private String changeImagePaths(final String content, final String imagePrefixPath, final int id) {
        Document doc = Jsoup.parse(content);
        Elements img = doc.getElementsByTag("img");
        for (Element el : img) {
            String srcValue = el.attr("src");
            String styleValue = el.attr(STYLE);
            if (StringUtils.containsIgnoreCase(srcValue, "file/get")) {
                el.remove();
            } else if (srcValue.contains(".")) {
                srcValue = addImagePrefix(imagePrefixPath, srcValue, id);
                el.attr("src", srcValue);
                convertStylesToClasses(id, el, styleValue);
                el.removeAttr(STYLE);
            } else {
                allImagePaths.add("ArticleId:: " + id + "--" + srcValue);
                convertStylesToClasses(id, el, styleValue);
                el.removeAttr(STYLE);
            }
        }
        Elements href = doc.getElementsByTag("a");
        for (Element el : href) {
            String hrefValue = el.attr("href");
            if (StringUtils.containsIgnoreCase(hrefValue, "file/get")) {
                el.attr("href", "#");
                el.attr("data-old-url", hrefValue);
                hrefValue = "#";
            }
            if (StringUtils.containsIgnoreCase(hrefValue, PDFEXTENSION)) {
                pdfList.add(hrefValue + "~" + id);
            }
        }
        Elements span = doc.getElementsByTag("span");
        for (Element el : span) {
            String styleValue = el.attr(STYLE);
            if (!styleValue.isEmpty()) {
                spanStyles.add(styleValue + "~" + id);
                String spanClass = el.attr(CLASS) + SPACE + composeStyleClasses(styleValue);
                el.attr(CLASS, spanClass);
                spanStyles.add(spanClass + "~" + id);
                el.removeAttr(STYLE);
            }
        }
        Elements p = doc.getElementsByTag("p");
        for (Element el : p) {
            String styleValue = el.attr(STYLE);
            if (!styleValue.isEmpty()) {
                pStyles.add(styleValue + "~" + id);
                String pClass = el.attr(CLASS) + SPACE + composeStyleClasses(styleValue);
                el.attr(CLASS, pClass);
                pStyles.add(pClass + "~" + id);
                el.removeAttr(STYLE);
            }
        }
        return doc.body().html();
    }

    private void convertStylesToClasses(final int id, final Element el, final String styleValue) {
        if (!styleValue.isEmpty()) {
            imgStyles.add(styleValue + "~" + id);
            if (!styleValue.startsWith("-webkit")) {
                String styleClasses = composeStyleClasses(styleValue);
                if (!el.attr(CLASS).isEmpty()) {
                    styleClasses = el.attr(CLASS) + styleClasses;
                }
                el.attr(CLASS, styleClasses);
                imgStyles.add(CLASS + "~" + styleClasses + "~" + id);
            } else {
                String wekKitStyle = styleValue.substring(styleValue.indexOf(";width:") + 1);
                if (!wekKitStyle.isEmpty()) {
                    String widthStyle = wekKitStyle.split(";")[0];
                    String styleClasses = composeStyleClasses(widthStyle);
                    styleClasses = el.attr(CLASS) + styleClasses;
                    el.attr(CLASS, styleClasses);
                    imgStyles.add(CLASS + "~" + styleClasses + "~" + id);
                }
            }
        }
    }

    private String composeStyleClasses(final String styleValue) {
        String finalStyle = StringUtils.EMPTY;
        for (String style : styleValue.split(";")) {
            String[] searchList = new String[] {SPACE, "#", "\"", ")", "!", ":", "(", ",", ".", "%"};
            String[] replacementList =
                    new String[] {StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, "-",
                            "-", "-", "-", "-", "pc"};
            style = "mig-" + StringUtils.replaceEach(style, searchList, replacementList);
            finalStyle = finalStyle.concat(SPACE).concat(style);
        }
        return finalStyle.toLowerCase();
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private String addImagePrefix(String imagePrefixPath, String srcValue, int id) {
        if (!StringUtils.isEmpty(srcValue) && !StringUtils.isEmpty(imagePrefixPath)) {
            if (imagePrefixPath.endsWith(SLASH)) {
                srcValue = imagePrefixPath + srcValue.substring(srcValue.lastIndexOf(SLASH) + 1);
            } else {
                srcValue = imagePrefixPath + srcValue.substring(srcValue.lastIndexOf(SLASH));
            }
            allImagePaths.add(srcValue + "~" + id);
        } else {
            srcValue = StringUtils.EMPTY;
        }
        return srcValue;
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    private Resource addChildNode(ResourceResolver resolver, final Resource pageRes, final String nodeName,
                                  final Map<String, Object> props, SlingHttpServletResponse response)
            throws IOException {
        Resource res = pageRes;
        try {
            res = resolver.create(Objects.requireNonNull(pageRes), nodeName, props);
        } catch (PersistenceException e) {
            log.error("Node Creation Error | {} | {}", nodeName, pageRes.getPath());
            response.getWriter().println(ERROR + "Node Creation Error | " + nodeName + " | " + pageRes.getPath());
        }
        return res;
    }

    @SuppressWarnings("checkstyle:FinalParameters")
    public JsonObject httpGetExecute(final List<NameValuePair> requestParamMap, SlingHttpServletResponse res,
                                     final @NonNull String articleId) throws IOException, URISyntaxException {
        if (articleId.equals("0")) {
            return new JsonObject();
        }
        HttpGet request = new HttpGet(API_URL + articleId);
        request.addHeader("accept", "application/json");
        request.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 Firefox/26.0");
        if (null != requestParamMap && !requestParamMap.isEmpty()) {
            URI uriBuilder = new URIBuilder(request.getURI()).addParameters(requestParamMap).build();
            request.setURI(uriBuilder);
        }
        JsonObject jsonObject;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            res.getWriter().println("API Call | " + request.getURI() + "\n");
            CloseableHttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            String responseMessage = response.getStatusLine().getReasonPhrase();
            String resString = EntityUtils.toString(response.getEntity());
            if (statusCode == HttpStatus.SC_OK && !resString.startsWith("<html>")) {
                jsonObject = gson.fromJson(resString, JsonObject.class);
                log.info(LOG_RESPONSE_INFO, statusCode, responseMessage);
            } else {
                res.getWriter().println(ERROR + "API Response | \n");
                log.error(ERROR + "API Response | \n" + resString);
                jsonObject = new JsonObject();
                count++;
            }
        }
        return jsonObject;
    }
}
