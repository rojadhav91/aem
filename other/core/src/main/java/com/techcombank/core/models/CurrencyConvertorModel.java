package com.techcombank.core.models;

/**
 * Interface For CurrencyConvertorToolModel
 *
 */
public interface CurrencyConvertorModel {

    /**
     * Currency Change Note Text
     */
    String getCurrencyChangeNote();

    /**
     * Calculator text for Analytics
     */
    String getCalculatorField();

    /**
     * Interaction text for Analytics
     */
    String getInteractionNameType();

}
