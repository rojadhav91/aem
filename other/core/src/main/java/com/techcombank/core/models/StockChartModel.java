package com.techcombank.core.models;

import lombok.Getter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

/**
 * The Class StockChartModel used for mapping Properties of Stock Chart
 * Information Component.
 */
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, resourceType = StockChartModel.RESOURCE_TYPE)
public class StockChartModel {

    protected static final String RESOURCE_TYPE = "techcombank/components/stockchart";

    /**
     * The Text for Last price field.
     */
    @ValueMapValue
    @Getter
    private String lastPrice;

    /**
     * The Text for the date prefix label field.
     */
    @ValueMapValue
    @Getter
    private String datePrefix;
}
