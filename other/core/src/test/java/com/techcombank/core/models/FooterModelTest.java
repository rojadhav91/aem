package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class FooterModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private FooterModel footerModel;

    @Mock
    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private List<FooterMenu> footerMenuList;

    @BeforeEach
    void setUp() {
        resourceResolverService = mock(ResourceResolverService.class);
        resourceResolver = mock(ResourceResolver.class);
        aemContext.addModelsForClasses(FooterModel.class);
        aemContext.load().json(TestConstants.FOOTER, "/footer");
        aemContext.currentResource(TestConstants.FOOTERCOMP);
        resource = mock(Resource.class);
        aemContext.registerService(Resource.class, resource);
        resource = aemContext.request().getResource();
        footerModel = resource.adaptTo(FooterModel.class);
        footerModel.getMenuList();
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        aemContext.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        aemContext.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        aemContext.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getLogo() {
        final String expected = "/content/dam/techcombank/fr_logo_909a5a9b8b.png";
        assertEquals(expected, footerModel.getLogo());
        assertEquals(expected + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                footerModel.getLogoMobileImagePath());
        assertEquals(expected + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                footerModel.getLogoWebImagePath());
    }

    @Test
    void getLogoAltText() {
        final String expected = "Techcombank Logo";
        assertEquals(expected, footerModel.getLogoAltText());
    }

    @Test
    void getRightButtonText() {
        final String expected = "Additional Links";
        assertEquals(expected, footerModel.getRightButtonText());
    }

    @Test
    void getCopyrightText() {
        final String expected = "Copyright © 2023 Techcombank All rights reserved.";
        assertEquals(expected, footerModel.getCopyrightText());
    }

    @Test
    void getTermsAndCondition() {
        final String expected = "Terms & Conditions";
        assertEquals(expected, footerModel.getTermsAndCondition());
    }

    @Test
    void getTermsAndConditionUrl() {
        final String expected = TestConstants.FOOTERURL;
        assertEquals(expected, footerModel.getTermsAndConditionUrl());

    }

    @Test
    void getOpenInNewTabTerms() {
        final String expected = "true";
        assertEquals(expected, footerModel.getOpenInNewTabTerms());
    }

    @Test
    void getPrivacyStatement() {
        final String expected = "Data Privacy Statement";
        assertEquals(expected, footerModel.getPrivacyStatement());
    }

    @Test
    void getPrivacyStatementLinkUrl() {
        final String expected = TestConstants.FOOTERURL;
        assertEquals(expected, footerModel.getPrivacyStatementLinkUrl());
    }

    @Test
    void getOpenInNewTabPrivacy() {
        final String expected = "true";
        assertEquals(expected, footerModel.getOpenInNewTabPrivacy());
    }

    @Test
    void getPersonalBanking() {
        final String expected = "<p>☎ Personal customers: 1800 588 822 (local) - 84 24 3944 6699 (international)</p>";
        assertEquals(expected, footerModel.getPersonalBanking());
    }

    @Test
    void getBusinessBanking() {
        final String expected = "<p>☎ Business customers: 1800 6556 (local) - 84 24 7303 6556 (international)</p>";
        assertEquals(expected, footerModel.getBusinessBanking());
    }

    @Test
    void getSocialMediaText() {
        final String expected = "Stay connected with Techcombank";
        assertEquals(expected, footerModel.getSocialMediaText());
    }

    @Test
    void isTermsAndConditionExternal() {
        final String expected = "other";
        assertEquals(expected, footerModel.getIsTermsAndConditionExternal());
    }

    @Test
    void isPrivacyStatementExternal() {
        final String expected = "other";
        assertEquals(expected, footerModel.getIsPrivacyStatementExternal());
    }

    @Test
    void getMenuList() {
        footerMenuList = footerModel.getMenuList();
        List<FooterSubMenu> footerSubMenuList = footerMenuList.get(0).getSubMenuList();
        assertEquals(TestConstants.FOOTERURL, footerMenuList.get(0).getFooterMenuUrl());
        assertEquals("Spend", footerSubMenuList.get(0).getSubMenuText());
        assertEquals(TestConstants.FOOTERURL, footerSubMenuList.get(0).getSubMenuUrl());
        assertEquals("/content/techcombank/web/vn/en/sample.html", footerSubMenuList.get(0).getSubMenuUrl());
        assertEquals("other",
                footerSubMenuList.get(0).getIsSubMenuUrlExternal());
        assertEquals("/content/techcombank/web/vn/en/sample.html",
                footerSubMenuList.get(0).getSubMenuUrl());
        assertEquals("Spend", footerSubMenuList.get(0).getSubMenuText());
        assertEquals("true", footerSubMenuList.get(0).getOpenInNewTab());
        assertEquals("Personal", footerMenuList.get(0).getFooterMenuHeader());
        assertEquals("true", footerSubMenuList.get(0).getOpenInNewTab());
        assertEquals("true", footerMenuList.get(0).getOpenInNewTab());
        assertEquals("other",
                footerMenuList.get(0).getIsFooterMenuUrlExternal());
    }

    @Test
    void getSocialMediaList() {
        List<FooterSocialMedia> footerSocialMediaList = footerModel.getSocialMediaList();
        String socialImage = "/content/dam/techcombank/fb_87a424484b.svg";
        assertEquals("https://www.facebook.com/Techcombank",
                footerSocialMediaList.get(0).getSocialUrl());
        assertEquals(socialImage,
                footerSocialMediaList.get(0).getSocialImage());
        assertEquals(socialImage,
                footerSocialMediaList.get(0).getSocialMobileImagePath());
        assertEquals(socialImage,
                footerSocialMediaList.get(0).getSocialWebImagePath());
        assertEquals("Facebook", footerSocialMediaList.get(0).getSocialText());
        assertEquals("true", footerSocialMediaList.get(0).getOpenInNewTab());
        assertEquals("exit",
                footerSocialMediaList.get(0).getExternalLink());
    }

    @Test
    void getLogoMobileImagePath() {
        final String expected =
                "/content/dam/techcombank/fr_logo_909a5a9b8b.png/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, footerModel.getLogoMobileImagePath());
    }

    @Test
    void getLogoWebImagePath() {
        final String expected =
                "/content/dam/techcombank/fr_logo_909a5a9b8b.png/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, footerModel.getLogoWebImagePath());
    }

}
