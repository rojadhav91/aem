package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class NetworkGridItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private NetworkGridItemModel networkGridItemModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(NetworkGridItemModel.class);
        aemContext.load().json(TestConstants.NETWORKGRIDITEM, "/networkGridItem");
        aemContext.currentResource(TestConstants.NETWORKGRIDITEMCOMP);
        Resource resource = aemContext.request().getResource();
        networkGridItemModel = resource.adaptTo(NetworkGridItemModel.class);
    }

    @Test
    void getGridPanelListNumber() {
        final int expected = 1;
        assertEquals(expected, networkGridItemModel.getGridPanelListNumber());
    }

    @Test
    void getGridPanelListTitleText() {
        final String expected = "Techcombank Tower Head Office";
        assertEquals(expected, networkGridItemModel.getGridPanelListTitleText());
    }

    @Test
    void getGridPanelListSubTitle() {
        final String expected = "Hanoi";
        assertEquals(expected, networkGridItemModel.getGridPanelListSubTitle());
    }

    @Test
    void getGridDisplayImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, networkGridItemModel.getGridDisplayImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(networkGridItemModel.getGridDisplayImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                networkGridItemModel.getWebGridDisplayImagePath());
        assertEquals(networkGridItemModel.getGridDisplayImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                networkGridItemModel.getMobileGridDisplayImagePath());
    }

    @Test
    void getDisplayImageAltText() {
        final String expected = "TCB HO";
        assertEquals(expected, networkGridItemModel.getDisplayImageAltText());
    }

    @Test
    void getGridImageTag() {
        final String expected = "Hanoi";
        assertEquals(expected, networkGridItemModel.getGridImageTag());
    }

    @Test
    void getGridImageTitle() {
        final String expected = "Techcombank Tower Head Office";
        assertEquals(expected, networkGridItemModel.getGridImageTitle());
    }

    @Test
    void getGridImageDescription() {
        final String expected = "6 Quang Trung";
        assertEquals(expected, networkGridItemModel.getGridImageDescription());
    }
}
