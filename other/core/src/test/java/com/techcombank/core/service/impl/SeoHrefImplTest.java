package com.techcombank.core.service.impl;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.HrefLangPojo;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class SeoHrefImplTest {

    @Mock
    private Externalizer externalizer;

    @InjectMocks
    private SeoHrefImpl seoHrefImpl = new SeoHrefImpl();

    private final AemContext aemContext = new AemContext();

    @Mock
    private ResourceResolver resourceResolver;


    @Mock
    private Resource resource;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page page;

    @Mock
    private GenericList genericList;

    @Mock
    private GenericList.Item item;


    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(SeoHrefImpl.class);
        aemContext.registerService(Externalizer.class, mock(Externalizer.class));
        aemContext.registerInjectActivateService(seoHrefImpl);
    }

    @Test
    void getPageList() {
        String pagePath = "/content/techcombank/web/vn/en/personal";
        lenient().when(page.getPath()).thenReturn(pagePath);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolver.resolve(Mockito.anyString())).thenReturn(resource);
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        lenient().when(pageManager.getContainingPage(resource)).thenReturn(page);
        lenient().when(pageManager.getPage(Mockito.any())).thenReturn(page);
        lenient().when(page.getTitle()).thenReturn("Vitanam");
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(resourceResolver.map(pagePath + TCBConstants.HTMLEXTENSION))
                .thenReturn(pagePath + TCBConstants.HTMLEXTENSION);
        List<Resource> resList = new ArrayList<>();
        resList.add(resource);
        lenient().when(resource.getChildren()).thenReturn(resList);
        lenient().when(resource.getPath()).thenReturn(pagePath);
        lenient().when(resource.getName()).thenReturn("vn");
        List<GenericList.Item> mockItems = new ArrayList<>();
        GenericList.Item mockItem = mock(GenericList.Item.class);
        mockItems.add(mockItem);
        lenient().when(mockItem.getTitle()).thenReturn("locale");
        lenient().when(mockItem.getValue()).thenReturn("/content/techcombank/web/vn/en");
        lenient().when(page.adaptTo(GenericList.class)).thenReturn(genericList);
        lenient().when(genericList.getItems()).thenReturn(mockItems);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        List<HrefLangPojo> pojoList = seoHrefImpl.getPageList(resourceResolver, page);
        assertEquals("x-default", pojoList.get(0).getRel());
        assertEquals("vn_en", pojoList.get(0).getHrefLang());
        assertTrue(pojoList.get(0).getHref().contains(pagePath + TCBConstants.HTMLEXTENSION));

        lenient().when(mockItem.getValue()).thenReturn("/content/techcombank/web/vn/vi");
        pojoList = seoHrefImpl.getPageList(resourceResolver, page);
        assertEquals("alternate", pojoList.get(0).getRel());
    }
}
