package com.techcombank.core.models;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class InspirePrivilegeModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private InspirePrivilegeModel model;


    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.INSPIREPRIVILEGE, "/inspirePrivilege");
        ctx.addModelsForClasses(InspirePrivilegeModel.class);
        model = getModel(TestConstants.INSPIREPRIVILEGECOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private InspirePrivilegeModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(InspirePrivilegeModel.class);
    }

    private void mockObjects() {

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getDisplayVariation() {
        final String expected = "select-item";
        assertEquals(expected, model.getDisplayVariation());
    }

    @Test
    void getImage() {
        final String expected = "path";
        assertEquals(expected, model.getImage());
    }

    @Test
    void getAltText() {
        final String expected = "text";
        assertEquals(expected, model.getAltText());
    }

    @Test
    void isEnableShadow() {
        final boolean expected = true;
        assertEquals(expected, model.isEnableShadow());
    }

    @Test
    void getLinkText() {
        final String expected = "text";
        assertEquals(expected, model.getLinkText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getLinkUrl());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = true;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void isEnableSeparator() {
        final boolean expected = true;
        assertEquals(expected, model.isEnableSeparator());
    }

    @Test
    void getSlideImageRow1List() {
        assert model.getSlideImageRow1List() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getSlideImageRow1List().size());
    }

    @Test
    void getSlideImageRow2List() {
        assert model.getSlideImageRow2List() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getSlideImageRow2List().size());
    }

    @Test
    void getSlideImageRow3List() {
        assert model.getSlideImageRow3List() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getSlideImageRow3List().size());
    }

    @Test
    void getContentList() {
        assert model.getContentList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getContentList().size());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit",
            model.getWebInteractionType());
    }

}
