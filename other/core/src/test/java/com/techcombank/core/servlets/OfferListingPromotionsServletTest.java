package com.techcombank.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.OfferListingPromotions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class OfferListingPromotionsServletTest {
    private final AemContext ctx = new AemContext();
    @InjectMocks
    private OfferListingPromotionsServlet offerListingPromotionsServlet = new OfferListingPromotionsServlet();
    @Mock
    private SlingHttpServletRequest req;
    @Mock
    private SlingHttpServletResponse res;
    @Mock
    private PrintWriter out;
    @Mock
    private RequestParameter reqParam;
    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private OfferListingPromotions offerListingPromotions;
    @Mock
    private Resource resource;
    @Mock
    private ValueMap valueMap;

    @BeforeEach
    void setUp() throws Exception {
        ctx.addModelsForClasses(OfferListingPromotionsServlet.class);
    }

    @Test
    void testPostMethod(final AemContext context) throws ServletException, IOException {
        MockSlingHttpServletResponse response = ctx.response();
        List<RequestParameter> requestParameterList = new ArrayList<>();
        Map<String, Object> promotionsCardsJsons = new HashMap<>();
        promotionsCardsJsons.put("expiryDate", "31 Dec 2024");
        promotionsCardsJsons.put("favourite", "true");
        requestParameterList.add(reqParam);
        lenient().when(req.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(req.getRequestParameterList()).thenReturn(requestParameterList);
        lenient().when(reqParam.getName()).thenReturn("products");
        lenient().when(req.getParameter(Mockito.any())).thenReturn("debit+card");
        lenient().when(offerListingPromotions.generateOffers(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(promotionsCardsJsons);
        lenient().when(req.getResource()).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(valueMap.get(TCBConstants.ROOTPATH, String.class)).thenReturn("promotions");
        offerListingPromotionsServlet.doGet(req, response);
        assertEquals("{\"expiryDate\":\"31 Dec 2024\",\"favourite\":\"true\"}", response.getOutputAsString());

    }
}
