package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class DeviceLookStepsCarouselModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private DeviceLookStepsCarouselModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(DeviceLookStepsCarouselModel.class);
        aemContext.load().json(TestConstants.DEVICELOOKSTEPSCAROUSEL, "/deviceLookStepsCarousel");
        aemContext.currentResource(TestConstants.DEVICELOOKSTEPSCAROUSELCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(DeviceLookStepsCarouselModel.class);
    }

    @Test
    void getNumberOfCardTilesToDisplay() {
        final int expected = 1;
        assertEquals(expected, model.getNumberOfCardTilesToDisplay());
    }

    @Test
    void getLayout() {
        final String expected = "select";
        assertEquals(expected, model.getLayout());
    }

    @Test
    void isHaveStepImage() {
        final boolean expected = true;
        assertEquals(expected, model.isHaveStepImage());
    }

    @Test
    void getListStepItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getListStepItems().size());
    }

}
