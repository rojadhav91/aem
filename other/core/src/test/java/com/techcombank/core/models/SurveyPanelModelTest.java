package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class SurveyPanelModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private SurveyPanelModel model;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.SURVEYPANEL, "/surveyPanel");
        ctx.addModelsForClasses(SurveyPanelModel.class);
        model = getModel(TestConstants.SURVEYPANELCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(resourceResolver.getResource("https://www.google.com")).thenReturn(null);
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "https://www.google.com")).
                thenReturn("https://www.google.com");
    }

    private SurveyPanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(SurveyPanelModel.class);
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getEverydayCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getEverydayCardImage());
    }

    @Test
    void getEverydayCardImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getEverydayCardImageAltText());
    }

    @Test
    void getEverydayCardUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getEverydayCardUrl());
    }

    @Test
    void getStyleCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getStyleCardImage());
    }

    @Test
    void getStyleCardImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getStyleCardImageAltText());
    }

    @Test
    void getStyleCardUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getStyleCardUrl());
    }

    @Test
    void getSparkCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getSparkCardImage());
    }

    @Test
    void getSparkCardImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getSparkCardImageAltText());
    }

    @Test
    void getSparkCardUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getSparkCardUrl());
    }

    @Test
    void getSignatureCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getSignatureCardImage());
    }

    @Test
    void getSignatureCardImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getSignatureCardImageAltText());
    }

    @Test
    void getSignatureCardUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getSignatureCardUrl());
    }

    @Test
    void getVietnamAirlineCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getVietnamAirlineCardImage());
    }

    @Test
    void getVietnamAirlineCardImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getVietnamAirlineCardImageAltText());
    }

    @Test
    void getVietnamAirlineUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getVietnamAirlineUrl());
    }

    @Test
    void getQ1QuestionCountText() {
        final String expected = "text";
        assertEquals(expected, model.getQ1QuestionCountText());
    }

    @Test
    void getQuestion1() {
        final String expected = "text";
        assertEquals(expected, model.getQuestion1());
    }

    @Test
    void getQ1AnswerOption1() {
        final String expected = "text";
        assertEquals(expected, model.getQ1AnswerOption1());
    }

    @Test
    void getQ1AnswerOption2() {
        final String expected = "text";
        assertEquals(expected, model.getQ1AnswerOption2());
    }

    @Test
    void getQ1AnswerOption3() {
        final String expected = "text";
        assertEquals(expected, model.getQ1AnswerOption3());
    }

    @Test
    void getQ1AnswerOption4() {
        final String expected = "text";
        assertEquals(expected, model.getQ1AnswerOption4());
    }

    @Test
    void getQ1ProgressBarDescriptionText() {
        final String expected = "text";
        assertEquals(expected, model.getQ1ProgressBarDescriptionText());
    }

    @Test
    void getQ2QuestionCountText() {
        final String expected = "text";
        assertEquals(expected, model.getQ2QuestionCountText());
    }

    @Test
    void getQuestion2() {
        final String expected = "text";
        assertEquals(expected, model.getQuestion2());
    }

    @Test
    void getQ2AnswerOption1() {
        final String expected = "text";
        assertEquals(expected, model.getQ2AnswerOption1());
    }

    @Test
    void getQ2AnswerOption2() {
        final String expected = "text";
        assertEquals(expected, model.getQ2AnswerOption2());
    }

    @Test
    void getQ2AnswerOption3() {
        final String expected = "text";
        assertEquals(expected, model.getQ2AnswerOption3());
    }

    @Test
    void getQ2AnswerOption4() {
        final String expected = "text";
        assertEquals(expected, model.getQ2AnswerOption4());
    }

    @Test
    void getQ2AnswerOption5() {
        final String expected = "text";
        assertEquals(expected, model.getQ2AnswerOption5());
    }

    @Test
    void getQ2ProgressBarDescriptionText() {
        final String expected = "text";
        assertEquals(expected, model.getQ2ProgressBarDescriptionText());
    }

    @Test
    void getQ3QuestionCountText() {
        final String expected = "text";
        assertEquals(expected, model.getQ3QuestionCountText());
    }

    @Test
    void getQuestion3() {
        final String expected = "text";
        assertEquals(expected, model.getQuestion3());
    }

    @Test
    void getQ3AnswerOption1() {
        final String expected = "text";
        assertEquals(expected, model.getQ3AnswerOption1());
    }

    @Test
    void getQ3AnswerOption2() {
        final String expected = "text";
        assertEquals(expected, model.getQ3AnswerOption2());
    }

    @Test
    void getQ3AnswerOption3() {
        final String expected = "text";
        assertEquals(expected, model.getQ3AnswerOption3());
    }

    @Test
    void getQ3ProgressBarDescriptionText() {
        final String expected = "text";
        assertEquals(expected, model.getQ3ProgressBarDescriptionText());
    }

    @Test
    void getNextCta() {
        final String expected = "text";
        assertEquals(expected, model.getNextCta());
    }

    @Test
    void getTryAgainCta() {
        final String expected = "text";
        assertEquals(expected, model.getTryAgainCta());
    }

    @Test
    void getLearnMoreCta() {
        final String expected = "text";
        assertEquals(expected, model.getLearnMoreCta());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = true;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void getEverydayCardWebInteractionType() {
        assertEquals("exit",
            model.getEverydayCardWebInteractionType());
    }

    @Test
    void getStyleCardWebInteractionType() {
        assertEquals("exit",
            model.getStyleCardWebInteractionType());
    }

    @Test
    void getSparkCardWebInteractionType() {
        assertEquals("exit",
            model.getSparkCardWebInteractionType());
    }

    @Test
    void getSignatureCardWebInteractionType() {
        assertEquals("exit",
            model.getSignatureCardWebInteractionType());
    }

    @Test
    void getVietnamAirlineCardWebInteractionType() {
        assertEquals("exit",
            model.getVietnamAirlineCardWebInteractionType());
    }

    @Test
    void testSetWebInteractionType() {
        assertTrue(model.setWebInteractionType());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getEverydayCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebEverydayCardImage());
        assertEquals(model.getEverydayCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileEverydayCardImage());

        assertEquals(model.getEverydayCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebEverydayCardImage());
        assertEquals(model.getEverydayCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileEverydayCardImage());


        assertEquals(model.getStyleCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebStyleCardImage());
        assertEquals(model.getStyleCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileStyleCardImage());


        assertEquals(model.getSparkCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebSparkCardImage());
        assertEquals(model.getSparkCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileSparkCardImage());


        assertEquals(model.getSignatureCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebSignatureCardImage());
        assertEquals(model.getSignatureCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileSignatureCardImage());


        assertEquals(model.getVietnamAirlineCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebVietnamAirlineCardImage());
        assertEquals(model.getVietnamAirlineCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileVietnamAirlineCardImage());
    }
}
