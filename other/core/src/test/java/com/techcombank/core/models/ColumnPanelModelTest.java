package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ColumnPanelModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();

    private ColumnPanelModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ColumnPanelModel.class);
        aemContext.load().json(TestConstants.COLUMNPANELITEM, "/columnpanel");
        aemContext.currentResource("/columnpanel/columnpanel/");
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ColumnPanelModel.class);
    }

    @Test
    void getCardLists() {
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getCardLists().size());
    }

    @Test
    void getViewMoreLabelText() {
        final String expected = "View More";
        assertEquals(expected, model.getViewMoreLabelText());
    }

    @Test
    void getViewLessLabelText() {
        final String expected = "Collapse";
        assertEquals(expected, model.getViewLessLabelText());
    }

}
