package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticleListingService;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleListingModelTest {

    private AemContext aemContext = AppAemContext.newAemContext();

    private ArticleListingModel articleListingModel;

    @Mock
    private Page currentPage;

    @Mock
    private ArticleListingService articleListingService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private TagManager tagManager;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private Tag tag;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        MockitoAnnotations.openMocks(this);
        articleListingModel = new ArticleListingModel();
        PrivateAccessor.setField(articleListingModel, "loadMore", "LOAD MORE");
        PrivateAccessor.setField(articleListingModel, "seeDetails", "SEE DETAILS");
        PrivateAccessor.setField(articleListingModel, "newTab", "true");
        PrivateAccessor.setField(articleListingModel, "tagValue", "tagValue");
        PrivateAccessor.setField(articleListingModel, "subTitle", "Articles Related To:");
        PrivateAccessor.setField(articleListingModel, "currentPage", currentPage);
        PrivateAccessor.setField(articleListingModel, "request", request);
        PrivateAccessor.setField(articleListingModel, "articleListingService", articleListingService);
    }

    @Test
    void testSample() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        when(request.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
        when(currentPage.getLanguage()).thenReturn(Locale.ENGLISH); // Assuming English locale for this test
        when(request.getQueryString()).thenReturn("tagName=testTag"); // Sample query string
        when(tagManager.resolve(anyString())).thenReturn(mock(Tag.class));
        when(tagManager.resolve(anyString()).getTitle(Locale.ENGLISH)).thenReturn("Test Tag Title");
        Method initMethod = ArticleListingModel.class.getDeclaredMethod("init");
        initMethod.setAccessible(true);
        initMethod.invoke(articleListingModel);
        assertEquals("tagName=testTag", request.getQueryString());
    }

    @Test
    void testGetTagValue() throws NoSuchFieldException {
        String tagValue = articleListingModel.getTagValue();
        assertEquals("tagValue", tagValue);
    }

    @Test
    void testGetSubTitle() {
        final String expected = "Articles Related To:";
        assertEquals(expected, articleListingModel.getSubTitle());
    }

    @Test
    void testGetLoadMore() {
        final String expected = "LOAD MORE";
        assertEquals(expected, articleListingModel.getLoadMore());
    }

    @Test
    void testGetSeeDetails() {
        final String expected = "SEE DETAILS";
        assertEquals(expected, articleListingModel.getSeeDetails());
    }

    @Test
    void testGetNewTab() {
        final String expected = "true";
        assertEquals(expected, articleListingModel.getNewTab());
    }

    @Test
    void testGetRelatedArticleList() throws NoSuchFieldException, RepositoryException {
        PrivateAccessor.setField(articleListingModel, "request", request);
        PrivateAccessor.setField(articleListingModel, "articleListingService", articleListingService);
        List<Articles> articlesList = articleListingModel.getRelatedArticleList();
        assertEquals(0, articlesList.size());
    }
}
