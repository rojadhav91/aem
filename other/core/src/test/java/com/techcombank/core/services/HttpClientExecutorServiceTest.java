package com.techcombank.core.services;

import com.google.gson.JsonObject;
import com.techcombank.core.service.impl.HttpClientExecutorServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class HttpClientExecutorServiceTest {

    private final AemContext ctx = new AemContext();
    @InjectMocks
    private HttpClientExecutorServiceImpl httpClientExecutorService; // = new HttpClientExecutorServiceImpl();
    @Mock
    private CloseableHttpClient httpClient;

    @Mock
    private CloseableHttpResponse httpResponse;

    @Mock
    private StatusLine statusLine;

    @Mock
    private HttpEntity httpEntity;

    @BeforeEach
    public void setup() throws Exception {
        PrivateAccessor.setField(httpClientExecutorService, "httpClient", httpClient);
        ctx.addModelsForClasses(HttpClientExecutorServiceImpl.class);
    }

    @Test
    void testexecuteHttpGet() throws IOException {
        Map<String, String> headerMap = new HashMap<>();
        List<NameValuePair> requestParamMap = new ArrayList<>();
        requestParamMap.add(new BasicNameValuePair("ticker", "TCB"));
        requestParamMap.add(new BasicNameValuePair("type", "stock"));
        headerMap.put(HttpHeaders.CONTENT_TYPE, "application/json");
        headerMap.put(HttpHeaders.ACCEPT, "application/json");
        lenient().when(httpClient.execute(Mockito.any())).thenReturn(httpResponse);
        lenient().when(httpResponse.getStatusLine()).thenReturn(statusLine);
        lenient().when(httpResponse.getStatusLine().getStatusCode()).thenReturn(HttpStatus.SC_OK);
        lenient().when(httpResponse.getStatusLine().getReasonPhrase()).thenReturn("success");
        lenient().when(httpResponse.getEntity()).thenReturn(httpEntity);
        JsonObject response = httpClientExecutorService.executeHttpGet("http://jwt-token-service", headerMap,
                requestParamMap);
        assertNull(response);

        lenient().when(httpResponse.getStatusLine().getStatusCode()).thenReturn(HttpStatus.SC_UNAUTHORIZED);
        lenient().when(httpResponse.getStatusLine().getReasonPhrase()).thenReturn("Unauthorized");
        response = httpClientExecutorService.executeHttpGet("http://jwt-token-service", headerMap, requestParamMap);
        assertEquals(HttpStatus.SC_UNAUTHORIZED, response.get("errorCode").getAsInt());

        lenient().when(httpClient.execute(Mockito.any())).thenThrow(IOException.class);
        response = httpClientExecutorService.executeHttpGet("http://jwt-token-service", headerMap, requestParamMap);
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, response.get("errorCode").getAsInt());
    }

    @Test
    void testexecuteHttpPost() throws IOException {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(HttpHeaders.CONTENT_TYPE, "application/json");
        headerMap.put(HttpHeaders.ACCEPT, "application/json");
        JsonObject requestPayLoad = new JsonObject();
        StringEntity payLoad = new StringEntity(requestPayLoad.toString());
        lenient().when(httpClient.execute(Mockito.any())).thenReturn(httpResponse);
        lenient().when(httpResponse.getStatusLine()).thenReturn(statusLine);
        lenient().when(httpResponse.getStatusLine().getStatusCode()).thenReturn(HttpStatus.SC_OK);
        lenient().when(httpResponse.getStatusLine().getReasonPhrase()).thenReturn("success");
        lenient().when(httpResponse.getEntity()).thenReturn(httpEntity);
        JsonObject response = httpClientExecutorService.executeHttpPost("http://QMS-service", headerMap, payLoad);
        assertNull(response);

        lenient().when(httpResponse.getStatusLine().getStatusCode()).thenReturn(HttpStatus.SC_UNAUTHORIZED);
        lenient().when(httpResponse.getStatusLine().getReasonPhrase()).thenReturn("Unauthorized");
        response = httpClientExecutorService.executeHttpPost("http://QMS-service", headerMap, payLoad);
        assertEquals(HttpStatus.SC_UNAUTHORIZED, response.get("errorCode").getAsInt());

        lenient().when(httpClient.execute(Mockito.any())).thenThrow(IOException.class);
        response = httpClientExecutorService.executeHttpPost("http://QMS-service", headerMap, payLoad);
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, response.get("errorCode").getAsInt());
    }

}
