package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class SocialShareInfoItemModelTest {

    private SocialShareInfoItemModel socialShareInfoItemModel;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        socialShareInfoItemModel = new SocialShareInfoItemModel();
        socialShareInfoItemModel.setSocialShareIcon("/content/dam/techcombank/brand-assets/images/icon_fb_40684f140a.png");
        socialShareInfoItemModel.setSocialShareLink("https://www.facebook.com/sharer/sharer/php");
        PrivateAccessor.setField(socialShareInfoItemModel, "socialShareAltText", "socialShareAltText");
        PrivateAccessor.setField(socialShareInfoItemModel, "socialShareTarget", "_blank");
        PrivateAccessor.setField(socialShareInfoItemModel, "socialShareNoFollow", true);
        PrivateAccessor.setField(socialShareInfoItemModel, "webInteractionValue", "{'webInteractions': {'name': 'Social Share','type': 'exit'}}");
        PrivateAccessor.setField(socialShareInfoItemModel, "resourceResolverService", resourceResolverService);
    }

    @Test
    void testInit() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
        final String expected = "init";
        Method initMethod = SocialShareInfoItemModel.class.getDeclaredMethod("init");
        initMethod.setAccessible(true);
        initMethod.invoke(socialShareInfoItemModel);
        assertEquals("init", expected);
    }

    @Test
    void getSocialShareIcon() {
        final String expected = "/content/dam/techcombank/brand-assets/images/"
                + "icon_fb_40684f140a.png/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, socialShareInfoItemModel.getSocialShareIcon());
    }

    @Test
    void getSocialShareIconMobile() {
        final String expected = "/content/dam/techcombank/brand-assets/images/"
                + "icon_fb_40684f140a.png/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, socialShareInfoItemModel.getSocialShareIconMobile());
    }

    @Test
    void getSocialShareAltText() {
        final String expected = "socialShareAltText";
        assertEquals(expected, socialShareInfoItemModel.getSocialShareAltText());
    }

   @Test
    void getSocialShareLink() {
        final String expected = "https://www.facebook.com/sharer/sharer/php";
        assertEquals(expected, socialShareInfoItemModel.getSocialShareLink());
    }

    @Test
    void isSocialShareTarget() {
        final String expected = "_blank";
        assertEquals(expected, socialShareInfoItemModel.getSocialShareTarget());
    }

    @Test
    void isSocialShareNoFollow() {
        final Boolean expected = true;
        assertEquals(expected, socialShareInfoItemModel.isSocialShareNoFollow());
    }

    @Test
    void getWebInteractionValue() {
        assertEquals("{'webInteractions': {'name': 'Social Share','type': 'exit'}}",
                socialShareInfoItemModel.getWebInteractionValue());
    }
}
