package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class FirstApproverParticipantStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private MetaDataMap metaDataMap;

    @InjectMocks
    private FirstApproverParticipantStep firstApproverParticipantStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflowData().getMetaDataMap()).thenReturn(metaDataMap);
        when(metaDataMap.get(TCBConstants.FIRST_APPORVER, String.class)).thenReturn("first.approver@example.com");
    }

    @Test
    void testGetParticipantWithFirstApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.FIRST_APPORVER)).thenReturn(true);
        String participant = firstApproverParticipantStep.getParticipant(workItem, workflowSession, metaDataMap);
        assertEquals("first.approver@example.com", participant);
    }

    @Test
    void testGetParticipantWithoutFirstApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.FIRST_APPORVER)).thenReturn(false);
        String participant = firstApproverParticipantStep.getParticipant(workItem, workflowSession, metaDataMap);
        assertEquals(StringUtils.EMPTY, participant);
    }
}
