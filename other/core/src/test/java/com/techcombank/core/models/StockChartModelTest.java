
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class StockChartModelTest to perform junit for StockChartModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class StockChartModelTest {

    private final AemContext ctx = new AemContext();

    private StockChartModel stockChartModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.STOCK_CHART_CMP_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(StockChartModel.class);
        stockChartModel = getModel("/content/jcr:content/root/container/stockchart");

    }

    private StockChartModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        StockChartModel stockChart = res.adaptTo(StockChartModel.class);
        return stockChart;
    }

    @Test
    void testStockChart() {
        assertEquals("Last Price (in VND)", stockChartModel.getLastPrice());
        assertEquals("as of", stockChartModel.getDatePrefix());
    }

}
