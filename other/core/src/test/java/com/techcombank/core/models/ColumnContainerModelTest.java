package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ColumnContainerModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ColumnContainerModel columnContainerModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ColumnContainerModel.class);
        aemContext.load().json(TestConstants.COLUMNCONTAINER, "/column");
        aemContext.currentResource(TestConstants.COLUMN);
        Resource resource = aemContext.request().getResource();
        columnContainerModel = resource.adaptTo(ColumnContainerModel.class);
    }

    @Test
    void getLayOut() {
        final String expected = "5col";
        assertEquals(expected, columnContainerModel.getWidth());
    }

    @Test
    void getLeftPadding() {
        final String expected = "10";
        assertEquals(expected, columnContainerModel.getLeftPadding());
    }

    @Test
    void getRightPadding() {
        final String expected = "10";
        assertEquals(expected, columnContainerModel.getRightPadding());
    }

    @Test
    void getTopPadding() {
        final String expected = "10";
        assertEquals(expected, columnContainerModel.getTopPadding());
    }

    @Test
    void getBottomPadding() {
        final String expected = "10";
        assertEquals(expected, columnContainerModel.getBottomPadding());
    }

    @Test
    void getActivateVerticalDivider() {
        final String expected = "true";
        assertEquals(expected, columnContainerModel.getActivateVerticalDivider());
    }

    @Test
    void getFlipView() {
        final String expected = "true";
        assertEquals(expected, columnContainerModel.getFlipView());
    }

}
