package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ModalModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ModalModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ModalModel.class);
        aemContext.load().json(TestConstants.MODAL, "/modal");
        aemContext.currentResource(TestConstants.MODALCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ModalModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "Đặt lịch hẹn tại Chi nhánh";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getPadding() {
        final int expected = 20;
        assertEquals(expected, model.getPadding());
    }

    @Test
    void getSize() {
        final String expected = "medium";
        assertEquals(expected, model.getSize());
    }

    @Test
    void getId() {
        final String expected = "appointment-at-the-branch";
        assertEquals(expected, model.getId());
    }

}
