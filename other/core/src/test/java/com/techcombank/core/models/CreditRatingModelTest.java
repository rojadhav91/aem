package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CreditRatingModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CreditRatingModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CreditRatingModel.class);
        aemContext.load().json(TestConstants.CREDITRATING, "/creditRating");
        aemContext.currentResource(TestConstants.CREDITRATINGCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CreditRatingModel.class);
    }

    @Test
    void getFilterYearLabel() {
        final String expected = "text";
        assertEquals(expected, model.getFilterYearLabel());
    }

    @Test
    void getFilterSelectLabel() {
        final String expected = "text";
        assertEquals(expected, model.getFilterSelectLabel());
    }

    @Test
    void getFilterAllLabel() {
        final String expected = "text";
        assertEquals(expected, model.getFilterAllLabel());
    }

    @Test
    void getCreditRatingReportList() {
        assert model.getCreditRatingReportList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getCreditRatingReportList().size());
    }

    @Test
    void getYearForFilterList() {
        assert model.getYearForFilterList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getYearForFilterList().size());
    }

    @Test
    void getLanguageTitle() {
        final String expected = "";
        assertEquals(expected, model.getLanguageTitle());
    }
}
