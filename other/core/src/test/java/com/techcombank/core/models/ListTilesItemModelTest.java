package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListTilesItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListTilesItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListTilesItemModel.class);
        aemContext.load().json(TestConstants.LISTTILESITEM, "/listTilesItem");
        aemContext.currentResource(TestConstants.LISTTILESITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListTilesItemModel.class);
    }

    @Test
    void getBackgroundImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getBackgroundImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getBackgroundImage() + TCBConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebBackgroundImagePath());
        assertEquals(model.getBackgroundImage() + TCBConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileBackgroundImagePath());
    }

    @Test
    void getNudgeText() {
        final String expected = "a";
        assertEquals(expected, model.getNudgeText());
    }

    @Test
    void getTheme() {
        final String expected = "dark";
        assertEquals(expected, model.getTheme());
    }

    @Test
    void getTextTitle() {
        final String expected = "a";
        assertEquals(expected, model.getTextTitle());
    }

    @Test
    void getDescription() {
        final String expected = "a";
        assertEquals(expected, model.getDescription());
    }

    @Test
    void getLinkText() {
        final String expected = "a";
        assertEquals(expected, model.getLinkText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "a";
        assertEquals(expected, model.getLinkUrl());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = false;
        assertEquals(expected, model.isNoFollow());
    }

}
