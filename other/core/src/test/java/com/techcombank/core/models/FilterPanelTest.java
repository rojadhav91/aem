package com.techcombank.core.models;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class FilterPanelTest {

    private AemContext aemContext = TCBAemContext.newAemContext();

    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private Hit hit;
    @Mock
    private TagManager tagManager;
    @Mock
    private Tag tag;

    private NodeIterator nodeIterator;
    private Session session;
    private QueryBuilder queryBuilder;
    private Query query;
    private SearchResult searchResult;
    private SearchResult searchResultNone;
    private Iterator<Node> nodeIterators;
    private FilterPanel filterPanel;
    private Resource hitResource;
    private Property property;
    private Node node;
    private Set<String> tags = new HashSet<>();
    public static final String COMPONENT_PATH = "/filterpanel/filterpanel";

    @BeforeEach
    public void setup() throws RepositoryException {
        aemContext.load().json("/content/techcombank/web/vn/en/filterpanel.json", "/filterpanel");
        aemContext.addModelsForClasses(FilterPanel.class);
        aemContext.currentResource(COMPONENT_PATH);
        Resource resource = aemContext.request().getResource();
        nodeIterator = mock(NodeIterator.class);
        node = mock(Node.class);
        queryBuilder = mock(QueryBuilder.class);
        session = mock(Session.class);
        query = mock(Query.class);
        searchResult = mock(SearchResult.class);
        hitResource = mock(Resource.class);
        property = mock(Property.class);
        aemContext.registerAdapter(ResourceResolver.class, QueryBuilder.class, queryBuilder);
        when(queryBuilder.createQuery(any(), any())).thenReturn(query);
        when(query.getResult()).thenReturn(searchResult);
        List<Hit> hitsList = Arrays.asList(new Hit[]{hit});
        lenient().when(searchResult.getHits()).thenReturn(hitsList);
        lenient().when(hit.getPath()).thenReturn(COMPONENT_PATH);
        lenient().when(resourceResolver.getResource(COMPONENT_PATH)).thenReturn(hitResource);
        lenient().when(hitResource.adaptTo(Node.class)).thenReturn(node);
        lenient().when(node.hasProperty(TCBConstants.CQ_FILTERPANELTAG)).thenReturn(true);
        lenient().when(node.getProperty(TCBConstants.CQ_FILTERPANELTAG)).thenReturn(property);
        lenient().when(property.getString()).thenReturn("tag-name");
        lenient().when(tagManager.resolve(anyString())).thenReturn(tag);
        lenient().when(tag.getTitle()).thenReturn("Tag Title");
        filterPanel = resource.adaptTo(FilterPanel.class);
    }

    @Test
    void testInit() {
        assertEquals("View All", filterPanel.getViewAll());
        assertEquals("Types", filterPanel.getCategoryLabel());
        assertEquals(tags, filterPanel.getTags());
    }
}

