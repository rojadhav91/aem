
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.ButtonModelImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class ButtonModelTest to perform junit for Button Model
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ButtonModelTest {

    private final AemContext ctx = new AemContext();

    private ButtonModelImpl buttonModel;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.BUTTON_CTA_MODEL_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(ButtonModelImpl.class);
        buttonModel = getModel("/content/jcr:content/root/container/button");
    }

    private ButtonModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        ButtonModelImpl buttonCta = res.adaptTo(ButtonModelImpl.class);
        return buttonCta;
    }

    @Test
    void testCtaType() {
        String expected = "link";
        assertEquals(expected, buttonModel.getCtaType());
    }

    @Test
    void testCtaID() {
        String expected = "linkId";
        assertEquals(expected, buttonModel.getCtaID());
    }

    @Test
    void testCtaModelId() {
        String expected = "ModelId";
        assertEquals(expected, buttonModel.getCtaModelId());
    }

    @Test
    void testCtaLabel() {
        String expected = "Open an account now";
        assertEquals(expected, buttonModel.getCtaLabel());
    }

    @Test
    void testCtaLink() {
        String expected = "/content/techcombank/web/vn/simplebanner.html";
        assertEquals(expected, buttonModel.getCtaLink());
    }

    @Test
    void testCtaTarget() {
        String expected = "_blank";
        assertEquals(expected, buttonModel.getCtaTarget());
    }

    @Test
    void testCtaNoFollow() {
        assertTrue(buttonModel.isCtaNoFollow());
    }

    @Test
    void testCtaQR() {
        assertTrue(buttonModel.isCtaQR());
    }

    @Test
    void testContactRegisterCTA() {
        assertTrue(buttonModel.isContactRegisterCTA());
    }

    @Test
    void testCtaIcon() {
        String expected = "/content/dam/techcombank/images/Frame.svg";
        assertEquals(expected, buttonModel.getCtaIcon());
    }

    @Test
    void testCtaIconWebImagePath() {
        String expected = "/content/dam/techcombank/images/Frame.svg";
        assertEquals(expected, buttonModel.getCtaIconWebImagePath());
    }

    @Test
    void testCtaIconMobileImagePath() {
        String expected = "/content/dam/techcombank/images/Frame.svg";
        assertEquals(expected, buttonModel.getCtaIconMobileImagePath());
    }

    @Test
    void testCtaQRImage() {
        String expected = "/content/dam/techcombank/images/bank.png";
        assertEquals(expected, buttonModel.getCtaQRImage());
    }

    @Test
    void testCtaQRWebImagePath() {
        String expected = "/content/dam/techcombank/images/bank.png";
        assertEquals(expected + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                buttonModel.getCtaQRWebImagePath());
    }

    @Test
    void testCtaQRMobileImagePath() {
        String expected = "/content/dam/techcombank/images/bank.png";
        assertEquals(expected + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                buttonModel.getCtaQRMobileImagePath());
    }

    @Test
    void testCtaQRImageAltText() {
        String expected = "QR ALt text";
        assertEquals(expected, buttonModel.getCtaQRImageAltText());
    }

    @Test
    void testInteractionNameType() {
        String expected = "{'webInteractions': {'name': 'Button','type': 'other'}}";
        assertEquals(expected, buttonModel.getInteractionNameType());
        assertEquals(expected, buttonModel.getCtaModalInteractionNameType());
    }

    @Test
    void testCtaColor() {
        String expected = "rgb(255,127,127)";
        assertEquals(expected, buttonModel.getCtaColor());
    }

    @Test
    void testFontColor() {
        String expected = "rgb(127,127,255)";
        assertEquals(expected, buttonModel.getFontColor());
    }
}
