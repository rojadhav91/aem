package com.techcombank.core.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.techcombank.core.pojo.TableOfContentPojo;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONException;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class TableOfContentsTest {

    //private final AemContext aemContext = AppAemContext.newAemContext();

    @Mock
    private Resource resource;

    @Mock
    private SlingHttpServletRequest request;

    private TableOfContents tableOfContents;

    @Mock
    private Page currentPage;

    @Mock
    private ValueMap valueMap;

    @Mock
    private Elements elements;

    @Mock
    private Element element1;

    @Mock
    private Element element2;

    private final List<Hit> hits = new ArrayList<>();

    @Mock
    private Hit hit;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Session session;

    @Mock
    private SearchResult searchResult;

    @Mock
    private JsonNode jsonNode;

    @Mock
    private Map<String, Object> jsonMap;

    @Mock
    private Query query;

    @BeforeEach
    void setUp() throws JSONException, RepositoryException, IOException, NoSuchFieldException {
        MockitoAnnotations.openMocks(this);
        tableOfContents = new TableOfContents();
        PrivateAccessor.setField(tableOfContents, "title", "Title");
        PrivateAccessor.setField(tableOfContents, "page", currentPage);
        PrivateAccessor.setField(tableOfContents, "request", request);
    }

    @Test
    void testInitTest()
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, RepositoryException {
        when(currentPage.getPath()).thenReturn("/content/techcombank/abc.page");
        when(request.getResourceResolver()).thenReturn(resourceResolver);
        when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        when(queryBuilder.createQuery(any(PredicateGroup.class), any(Session.class))).thenReturn(query);
        when(query.getResult()).thenReturn(searchResult);
        hits.add(hit);
        when(searchResult.getHits()).thenReturn(hits);
        when(hit.getResource()).thenReturn(resource);
        when(resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.get("text", String.class)).
                thenReturn("<html><body><h2 id='1'>SampleH2</h2><h2 id='2'>"
                        + "SampleH2</h2><h3 id='1'>SampleH3</h3><h3 id='2'>SampleH3</h3></body></html>");
        Method initMethod = TableOfContents.class.getDeclaredMethod("init");
        initMethod.setAccessible(true);
        initMethod.invoke(tableOfContents);
        assertEquals("/content/techcombank/abc.page", currentPage.getPath());
    }

   @Test
   void testSortJsonKeysFromGson() {
       String jsonString = "{\"1\": \"value1\", \"2\": \"value2\"}";
       JsonObject jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();

       Integer[] sortedKeys = TableOfContents.sortJsonKeysFromGson(jsonObject);
       assertArrayEquals(new Integer[]{1, 2}, sortedKeys);

   }

   @Test
   void testGetJsonMap() {

       Map<String, Object> expectedJsonMap = new HashMap<>();
       expectedJsonMap.put("key1", "value1");
       expectedJsonMap.put("key2", "value2");
       tableOfContents.setJsonMap(expectedJsonMap);

       Map<String, Object> result = tableOfContents.getJsonMap();
       assertEquals(expectedJsonMap, result);

   }

    @Test
    void testExtractTableOfContentsWhenElementsExist() {
        String sampleHtml = "<html><body><h2 id='1'>Title1</h2><h2 id='2'>Title2</h2></body></html>";
        Map<String, TableOfContentPojo> result = tableOfContents.extractTableOfContents(sampleHtml, "h2");

        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.containsKey("0"));
        assertTrue(result.containsKey("1"));
        assertEquals("1", result.get("0").getId());
        assertEquals("Title1", result.get("0").getValue());
        assertEquals("2", result.get("1").getId());
        assertEquals("Title2", result.get("1").getValue());
    }

    @Test
    void testExtractTableOfContentsWhenNoElementsExist() {
        String sampleHtml = "<html><body><p>This is a sample text.</p></body></html>";
        Map<String, TableOfContentPojo> result = tableOfContents.extractTableOfContents(sampleHtml, "h2");

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void testAddH3ToH2WhenH3Exists() {
        JsonObject jsonObjecth2 = new JsonObject();
        JsonObject jsonObjecth3 = new JsonObject();
        JsonObject jsonList = new JsonObject();

        JsonObject mockH2Element = new JsonObject();
        jsonObjecth2.add("1", mockH2Element);

        JsonObject mockH3Element = new JsonObject();
        jsonObjecth3.add("1", mockH3Element);

        tableOfContents.addH3ToH2(jsonObjecth2, jsonObjecth3, 1, 1, 1, jsonList);

        assertTrue(jsonObjecth2.has("1"));
        assertTrue(jsonObjecth2.getAsJsonObject("1").has("h3"));
    }

    @Test
    void testAddH3ToH2WhenH3DoesNotExist() {
        JsonObject jsonObjecth2 = new JsonObject();
        jsonObjecth2.add("1", new JsonObject());
        JsonObject jsonObjecth3 = new JsonObject();
        JsonObject jsonList = new JsonObject();

        tableOfContents.addH3ToH2(jsonObjecth2, jsonObjecth3, 1, -1, -1, jsonList);
        assertTrue(jsonObjecth2.has("1"));
    }

    @Test
    void getHideStatusTest() throws NoSuchFieldException {
        PrivateAccessor.setField(tableOfContents, "page", currentPage);
        lenient().when(currentPage.getProperties()).thenReturn(valueMap);
        lenient().when(valueMap.get("hideTableOfContent", String.class)).thenReturn("hello");
        String hideStatus = tableOfContents.getHideStatus();
        assertEquals("hello", hideStatus);
    }

    @Test
    void getTitleTest() {
       String expctedValue = "Title";
       assertEquals(expctedValue, tableOfContents.getTitle());
    }
}
