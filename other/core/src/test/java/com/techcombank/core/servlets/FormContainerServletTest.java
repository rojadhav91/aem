package com.techcombank.core.servlets;

import com.google.gson.JsonObject;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.FormContainerService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class FormContainerServletTest {

    private final AemContext aemContext = new AemContext();
    @InjectMocks
    private FormContainerServlet formContainerServletTest;

    @Mock
    private FormContainerService formContainerService;

    private static final int STATUS = 200;


    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(FormContainerServlet.class);
    }

    @Test
    void doPost() throws IOException {
        MockSlingHttpServletRequest request = aemContext.request();
        MockSlingHttpServletResponse response = aemContext.response();
        JsonObject expectedResult = new JsonObject();
        expectedResult.addProperty("success", "true");
        expectedResult.addProperty("failure", "false");
        aemContext.request().addRequestParameter("name", "forms");
        aemContext.request().addRequestParameter("formtype", "Leads");
        formContainerServletTest.doPost(request, response);
        assertEquals("forms", request.getParameter("name"));
        assertEquals(STATUS, response.getStatus());
    }
    @Test
    void doPostFailure() throws IOException {
        MockSlingHttpServletRequest request = aemContext.request();
        MockSlingHttpServletResponse response = aemContext.response();
        JsonObject expectedResult = new JsonObject();
        expectedResult.addProperty("success", "true");
        expectedResult.addProperty("failure", "false");
        formContainerServletTest.doPost(request, response);
        assertEquals(TCBConstants.STATUS_BAD_REQUEST, response.getStatus());
    }
}
