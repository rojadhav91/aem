package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.ArticleTagCloud;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.utils.PlatformUtils;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static junit.framework.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleTagCloudModelTest {
    private final AemContext aemContext = TCBAemContext.newAemContext();

    private ArticleTagCloudModel articleTagCloudModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ValueMap valueMap;

    @Mock
    private Page currentPage;

    @Mock
    private PageManager pageManager;

    private Resource resource;

    @BeforeEach
    void setUp() {
        currentPage = mock(Page.class);
        aemContext.registerService(Page.class, currentPage);
        aemContext.addModelsForClasses(ExtendedTitle.class);
        aemContext.load().json(TestConstants.ARTICLE_TAG, "/articletagcloud");
        aemContext.currentResource(TestConstants.ARTICLE_TAG_COMPONENT);
        aemContext.registerService(Page.class, currentPage);
        resource = aemContext.request().getResource();
        articleTagCloudModel = resource.adaptTo(ArticleTagCloudModel.class);
    }

    @Test
    void getOpenInNewTab() {
        final String expected = "true";
        assertEquals(expected, articleTagCloudModel.getOpenInNewTab());
    }

    @Test
    void getTagLabel() {
        final String expected = "TagLabel";
        assertEquals(expected, articleTagCloudModel.getTagLabel());
    }

    @Test
    void getHideStatus() throws NoSuchFieldException {
        PrivateAccessor.setField(articleTagCloudModel, "currentPage", currentPage);
        lenient().when(currentPage.getProperties()).thenReturn(valueMap);
        lenient().when(valueMap.get("hideArticleTagCloud", String.class)).thenReturn("hello");
        String hideStatus = articleTagCloudModel.getHideStatus();
        assertEquals("hello", hideStatus);
    }

    @Test
    void getArticleList() throws NoSuchFieldException {
        PrivateAccessor.setField(articleTagCloudModel, "currentPage", currentPage);
        List<Tag> tags = new ArrayList<>();
        Locale lang = currentPage.getLanguage();
        Tag keywordTag = mock(Tag.class);
        lenient().when(keywordTag.getPath())
                .thenReturn("/content/dam/test/article-category/article-secondary-category/tag1");
        lenient().when(keywordTag.getTitle(lang)).thenReturn("Tag1");
        lenient().when(keywordTag.getName()).thenReturn("tag1");
        tags.add(keywordTag);
        lenient().when(currentPage.getTags()).thenReturn(tags.toArray(new Tag[0]));
        lenient().when(currentPage.getAbsoluteParent(TCBConstants.COUNTRY_INDEX)).thenReturn(mock(Page.class));
        String rootPath = "/content/dam/test/articles/article-category/article-secondary-category";
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, rootPath + "/tag/tag1"))
                .thenReturn("/articles/tag/tag1");
        List<ArticleTagCloud> articleCloudList = articleTagCloudModel.getArticleList();
        assertNotNull(articleCloudList);
        assertEquals(1, articleCloudList.size());
        ArticleTagCloud articleCloud = articleCloudList.get(0);
        assertEquals("Tag1", articleCloud.getTitle());
        assertEquals("true", articleCloud.getOpenInNewTab());
    }
}
