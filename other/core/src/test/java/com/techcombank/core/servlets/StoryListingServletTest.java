package com.techcombank.core.servlets;


import com.techcombank.core.service.StoryListing;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StoryListingServletTest {

    @InjectMocks
    private StoryListingServlet storyListingServlet = new StoryListingServlet();
    private final AemContext aemContext = TCBAemContext.newAemContext();
    @Mock
    private Resource resource;
    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private PrintWriter out;
    @Mock
    private StoryListing storyListing;
    @Mock
    private ValueMap valueMap;

    @BeforeEach
    void setUp() {
        aemContext.load().
                json("/content/techcombank/web/vn/en/articles.json", "/article");
        aemContext.currentResource("/article/article1");
    }

    @Test
    void doGet() throws ServletException, IOException {
        SlingHttpServletRequest request = mock(SlingHttpServletRequest.class);
        SlingHttpServletResponse response = mock(SlingHttpServletResponse.class);
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        Resource artticle1 = aemContext.resourceResolver().resolve("/article/article1");
        Resource article2 = aemContext.resourceResolver().resolve("/article/article2");
        List<String> stringList = new ArrayList<>();
        stringList.add(artticle1.toString());
        stringList.add(article2.toString());

        lenient().when(response.getWriter()).thenReturn(out);
        lenient().when(request.getParameter("articleTag")).thenReturn("");
        lenient().when(request.getParameter("articleCategory")).thenReturn("");
        lenient().when(request.getResource()).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(valueMap.get("rootPath", String.class)).thenReturn("/content/techcombank/vn/en/storylisting");
        lenient().when(request.getParameter("articleCategory")).thenReturn("article");
        lenient().when(request.getParameter("articleTag")).thenReturn("promotions1");

        lenient().when(storyListing.getResult(Mockito.any(), Mockito.any())).thenReturn(stringList.toString());
        storyListingServlet.doGet(request, response);
        assertNull(response.getCharacterEncoding());
    }
}
