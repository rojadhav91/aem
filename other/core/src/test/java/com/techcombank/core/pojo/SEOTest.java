package com.techcombank.core.pojo;

import com.techcombank.core.testcontext.AppAemContext;
import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class SEOTest {

    private final AemContext aemContext = AppAemContext.newAemContext();
    private SEO seo;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(SEO.class);
        aemContext.load().json(TestConstants.SEO_JSON, "/seo");
        aemContext.currentResource(TestConstants.SEO_COMP);
        Resource resource = aemContext.request().getResource();
        seo = resource.adaptTo(SEO.class);
    }

    @Test
    void getSeoTitle() {
        final String expected = "SEO Title";
        assertEquals(expected, seo.getSeoTitle());
    }

    @Test
    void getMetaDescription() {
        final String expected = "Meta Description";
        assertEquals(expected, seo.getMetaDescription());
    }

    @Test
    void getMetaKeyword() {
        final String expected = "Keyword";
        assertEquals(expected, seo.getMetaKeyword());
    }

    @Test
    void getMetaImage() {
        final String expected = "/content/dam/techcombank/public-site/seo/image.jpg";
        assertEquals(expected, seo.getMetaImage());
    }

    @Test
    void getOpenGraphTitle() {
        final String expected = "Graph Title";
        assertEquals(expected, seo.getOpenGraphTitle());
    }

    @Test
    void getOpenGraphDescription() {
        final String expected = "Graph Description";
        assertEquals(expected, seo.getOpenGraphDescription());
    }

    @Test
    void getOpenGraphType() {
        final String expected = "Graph type";
        assertEquals(expected, seo.getOpenGraphType());
    }

    @Test
    void getChangeFrequency() {
        final String expected = "Always";
        assertEquals(expected, seo.getChangeFrequency());
    }

    @Test
    void getSeoPriority() {
        final String expected = "0.5";
        assertEquals(expected, seo.getSeoPriority());
    }
}
