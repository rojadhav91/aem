package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CardInfoModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CardInfoModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CardInfoModel.class);
        aemContext.load().json(TestConstants.CARDINFO, "/cardInfo");
        aemContext.currentResource(TestConstants.CARDINFOCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CardInfoModel.class);
    }

    @Test
    void getDisplayVariation() {
        final String expected = "select-item";
        assertEquals(expected, model.getDisplayVariation());
    }

    @Test
    void getTheme() {
        final String expected = "select-item";
        assertEquals(expected, model.getTheme());
    }

    @Test
    void getCardInfoItemList() {
        assert model.getCardInfoItemList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getCardInfoItemList().size());
    }

    @Test
    void getCardInfoEffects() {
        final String expected = "select-item";
        assertEquals(expected, model.getCardInfoEffects());
    }

    @Test
    void getViewMoreButton() {
        final Boolean expected = false;
        assertEquals(expected, model.getViewMoreButton());
    }

    @Test
    void getViewMoreLabel() {
        final String expected = "text";
        assertEquals(expected, model.getViewMoreLabel());
    }

    @Test
    void getViewLessLabel() {
        final String expected = "text";
        assertEquals(expected, model.getViewLessLabel());
    }
}
