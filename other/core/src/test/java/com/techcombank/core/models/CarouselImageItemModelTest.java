package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CarouselImageItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CarouselImageItemModel model;
    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CarouselImageItemModel.class);
        aemContext.load().json(TestConstants.CAROUSELINFOITEM, "/carouselInfoItem");
        aemContext.currentResource(TestConstants.CAROUSELINFOITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CarouselImageItemModel.class);
    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/brand-assets/images/Meduim_19829f8a09.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, model.getImage());
    }

    @Test
    void getImageAltText() {
        final String expected = "item0altText";
        assertEquals(expected, model.getImgAltText());
    }

    @Test
    void getImageMobile() {
        final String expected = "/content/dam/techcombank/brand-assets/images/Meduim_19829f8a09.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, model.getImageMobile());
    }
}
