package com.techcombank.core.services;

import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.impl.EmailServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class EmailServiceTest {

    @InjectMocks
    private EmailServiceImpl emailService = new EmailServiceImpl();

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private MessageGateway messageGateway;

    @Mock
    private WorkItem workItem;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private UserManager userManager;

    @Mock
    private Authorizable authorizable;

    private Value[] values;

    @BeforeEach
    void setup() throws RepositoryException {
        Mockito.lenient().when(messageGatewayService.getGateway(HtmlEmail.class)).thenReturn(messageGateway);
        Property p = Mockito.mock(Property.class);
        Value value = Mockito.mock(Value.class);
        Mockito.lenient().when(value.getString()).thenReturn("test@test.com");
        values = new Value[]{value};

        Mockito.lenient().when(workItem.getWorkflowData()).thenReturn(workflowData);
        Mockito.lenient().when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
        Mockito.lenient().when(metaDataMap.containsKey(TCBConstants.USER_ID)).thenReturn(true);
        Mockito.lenient().when(metaDataMap.get(TCBConstants.USER_ID, String.class)).thenReturn("user1");
        Mockito.lenient().when(resolver.adaptTo(UserManager.class)).thenReturn(userManager);
        Mockito.lenient().when(userManager.getAuthorizable("user1")).thenReturn(authorizable);
        Mockito.lenient().when(authorizable.hasProperty(TCBConstants.USER_PROFILE_EMAIL_PATH)).thenReturn(true);
        Mockito.lenient().when(authorizable.getProperty(TCBConstants.USER_PROFILE_EMAIL_PATH)).thenReturn(values);
    }

    @Test
    void testSendEmail() {
        String subjectLine = "Test Mail";
        String messageBody = "Test Mail Body";
        String[] recipients = {"test@test.com"};
        String status = emailService.sendEmail(subjectLine, messageBody, recipients);
        assertEquals("Success", status);
    }

    @Test
    void testGetUserEmail() throws RepositoryException {
        String email = emailService.getUserEmail(workItem, resolver);
        assertEquals("test@test.com", email);
    }
}
