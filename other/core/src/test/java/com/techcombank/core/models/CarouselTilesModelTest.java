package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CarouselTilesModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private CarouselTilesModel model;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.CAROUSELTILES, "/carouselTiles");
        ctx.addModelsForClasses(CarouselTilesModel.class);
        model = getModel(TestConstants.CAROUSELTILESCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(resourceResolver.getResource("https://www.google.com")).thenReturn(null);
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "https://www.google.com")).
                thenReturn("https://www.google.com");
    }

    private CarouselTilesModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(CarouselTilesModel.class);
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getNumberOfTiles() {
        final String expected = "a";
        assertEquals(expected, model.getNumberOfTiles());
    }

    @Test
    void isInMobileViewIsItASlider() {
        final boolean expected = true;
        assertEquals(expected, model.isInMobileViewIsItASlider());
    }

    @Test
    void getViewMoreLabel() {
        final String expected = "View More";
        assertEquals(expected, model.getViewMoreLabel());
    }

    @Test
    void getModelItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getModelItems().size());
    }

}
