package com.techcombank.core.pojo;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static junitx.framework.Assert.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticlesTest {

    @Test
    void testArticlesGetterSetter() {

        Articles articles = new Articles();

        articles.setWebImage("webImage");
        articles.setMobileImage("mobileImage");
        articles.setAltText("altText");
        articles.setTitle("title");
        articles.setDescription("description");
        articles.setArticleCreationDate("2023-01-01");
        articles.setPath("/content/article");
        articles.setOpenInNewTab("true");

        assertEquals(articles.getWebImage(), "webImage");
        assertEquals(articles.getMobileImage(), "mobileImage");
        assertEquals(articles.getAltText(), "altText");
        assertEquals(articles.getTitle(), "title");
        assertEquals(articles.getDescription(), "description");
        assertEquals(articles.getArticleCreationDate(), "2023-01-01");
        assertEquals(articles.getPath(), "/content/article");
        assertEquals(articles.getOpenInNewTab(), "true");

    }
}
