package com.techcombank.core.testcontext;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.testing.mock.sling.ResourceResolverType;

public final class TCBAemContext {
    private TCBAemContext() {
    }

    public static AemContext newAemContext() {
        return newAemContext(null);
    }

    public static AemContext newAemContext(final String currentResource, final String... paths) {
        return new AemContextBuilder().resourceResolverType(
                ResourceResolverType.RESOURCERESOLVER_MOCK).<AemContext>afterSetUp(context -> {
            context.addModelsForPackage(TestConstants.TCB_SLING_MODELS);

            if (paths != null) {
                int index = 0;
                for (String path : paths) {
                    String fileName = path + TestConstants.SLASH
                            + path.split(TestConstants.SLASH)[path.split(TestConstants.SLASH).length - 1]
                            + TestConstants.PERIOD + TestConstants.JSON_FILE;
                    context.load().json(fileName, path);

                    if (index == 0 && StringUtils.isNotBlank(currentResource)) {
                        context.currentResource(path + currentResource);
                    }
                    index++;
                }
            }
        }).build();
    }
}
