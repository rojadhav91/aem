
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.CurrencyConvertorModelImpl;
import com.techcombank.core.service.VfxRatesDataService;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;

/**
 * The Class CurrencyConvertorToolModelTest to perform junit for
 * CurrencyConvertorToolModel Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CurrencyConvertorModelTest {

    private final AemContext ctx = new AemContext();

    private CurrencyConvertorModelImpl currencyConvertorModel;

    @Mock
    private VfxRatesDataService vfxRatesDataService;

    @Mock
    private Resource resource;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CURRENCY_CONVERTOR_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(CurrencyConvertorModelImpl.class);
        currencyConvertorModel = getModel(
                "/content/techcombank/web/vn/vi/currencyconvertor/jcr:content/root/container/currencyconvertor");
        PrivateAccessor.setField(currencyConvertorModel, "vfxRatesDataService", vfxRatesDataService);
        PrivateAccessor.setField(currencyConvertorModel, "resourceResolver", resourceResolver);
    }

    private CurrencyConvertorModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        CurrencyConvertorModelImpl currencyConvertor = res.adaptTo(CurrencyConvertorModelImpl.class);
        return currencyConvertor;
    }

    @Test
    void testTransaction() {
        String expected = "Transaction";
        assertEquals(expected, currencyConvertorModel.getTransaction());
    }

    @Test
    void testForeignCurrency() {
        String expected = "Currency";
        assertEquals(expected, currencyConvertorModel.getForeignCurrency());
    }

    @Test
    void testSourceCurrencyPlaceholder() {
        String expected = "Enter the amount";
        assertEquals(expected, currencyConvertorModel.getSourceCurrencyPlaceholder());
    }

    @Test
    void testExchangeInfo() {
        String expected = "Convert to";
        assertEquals(expected, currencyConvertorModel.getExchangeInfo());
    }

    @Test
    void testTargetCurrency() {
        String expected = "VND";
        assertEquals(expected, currencyConvertorModel.getTargetCurrency());
    }

    @Test
    void testTargetCurrencyPlaceholder() {
        String expected = "Amount To Be Converted";
        assertEquals(expected, currencyConvertorModel.getTargetCurrencyPlaceholder());
    }

    @Test
    void testCurrencyChangeNote() {
        String expected = "Rates are updated at 2023-07-11 23:30:00 and are for reference only";
        List<String> basePath = new ArrayList<>();
        basePath.add("/content/dam/techcombank/master-data/exchange-rates");
        lenient().when(vfxRatesDataService.getVfxRateBasePath(TestConstants.EXCHANGE_RATE, false))
                .thenReturn(basePath);
        lenient().when(vfxRatesDataService.getVfxRateUpdateDate(Mockito.any(), Mockito.any()))
                .thenReturn("2023-07-11 23:30:00");
        assertEquals(expected, currencyConvertorModel.getCurrencyChangeNote());
    }

    @Test
    void testErrorNote() {
        String expected = "Error while converting text";
        assertEquals(expected, currencyConvertorModel.getErrorNote());
    }

    @Test
    void testTransactionTab() {
        List<TransactionTypeModel> transactionType = currencyConvertorModel.getTransactionTypeTab();
        assertEquals("Sell (cash/cheque)", transactionType.get(0).getTransactionLabel());
        assertEquals("askRate", transactionType.get(0).getTransactionType());
    }

    @Test
    void testSourceCurrency() {
        List<SourceCurrencyModel> currency = currencyConvertorModel.getSourceCurrencyTab();
        String expected = "KRW";
        assertEquals(expected, currency.get(0).getSourceCurrency());
    }

    @Test
    void testCalculatorField() {
        String expected = "Sell (cash/cheque)|KRW";
        assertEquals(expected, currencyConvertorModel.getCalculatorField());
    }

    @Test
    void testInteractionNameType() {
        String expected = "{'webInteractions': {'name': 'Calculators','type': 'other'}}";
        assertEquals(expected, currencyConvertorModel.getInteractionNameType());
    }

}
