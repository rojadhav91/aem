package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.AnnouncementModelImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class AnnouncementModelTest {

    private final AemContext aemContext = new AemContext();

    private AnnouncementModelImpl model;

    @BeforeEach
    void setUp() throws Exception {

        aemContext.addModelsForClasses(AnnouncementModelImpl.class);
        aemContext.load().json(TestConstants.ANNOUNCEMENTINFO, "/announcementInfo");
        aemContext.currentResource("/announcementInfo/jcr:content/container/announcement/");
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(AnnouncementModelImpl.class);
    }


    @Test
    void getAnnouncementText() {
        final String expected = "Announcement for F@st EBank business users";
        assertEquals(expected, model.getAnnouncementText());
    }

    @Test
    void getTitleText() {
        final String expected = "PLAN FOR MIGRATION TO TECHCOMBANK BUSINESS";
        assertEquals(expected, model.getTitleText());
    }

    @Test
    void getTitleDescription() {
        final String expected = "<p>In order to provide a seamless digital experience,</p>\r\n";
        assertEquals(expected, model.getTitleDescription());
    }

    @Test
    void getCtaRequired() {
        final boolean expected = true;
        assertEquals(expected, model.getCtaRequired());
    }

    @Test
    void getIconPath() {
        final String expected = "/content/dam/techcombank/dev/images/announce_9f2882b6d1.svg";
        assertEquals(expected, model.getIconPath());
    }

    @Test
    void getIconAltText() {
        final String expected = "image";
        assertEquals(expected, model.getIconAltText());
    }
}
