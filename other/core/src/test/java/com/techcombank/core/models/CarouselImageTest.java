package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CarouselImageTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private CarouselImage model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CarouselImage.class);
        aemContext.load().json(TestConstants.CAROUSELINFO, "/carouselInfo");
        aemContext.currentResource(TestConstants.CAROUSELINFOCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CarouselImage.class);
    }

    @Test
    void getImageItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getImageItems().size());
    }
}
