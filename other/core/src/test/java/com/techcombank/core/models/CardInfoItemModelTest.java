package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CardInfoItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CardInfoItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CardInfoItemModel.class);
        aemContext.load().json(TestConstants.CARDINFOITEM, "/cardInfoItem");
        aemContext.currentResource(TestConstants.CARDINFOITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CardInfoItemModel.class);
    }

    @Test
    void getImagePosition() {
        final String expected = "select-item";
        assertEquals(expected, model.getImagePosition());
    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImagePath());
        assertEquals(model.getImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImagePath());
    }

    @Test
    void getImageAlt() {
        final String expected = "text";
        assertEquals(expected, model.getImageAlt());
    }

    @Test
    void getCardTitle() {
        final String expected = "text";
        assertEquals(expected, model.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "richtext";
        assertEquals(expected, model.getCardDescription());
    }

    @Test
    void getCardDescription2() {
        final String expected = "richtext";
        assertEquals(expected, model.getCardDescription2());
    }

    @Test
    void isCtaNeeded() {
        final boolean expected = true;
        assertEquals(expected, model.isCtaNeeded());
    }

}
