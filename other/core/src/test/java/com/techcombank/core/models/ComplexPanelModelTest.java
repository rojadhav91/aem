package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ComplexPanelModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private ComplexPanelModel complexPanelModel;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(ComplexPanelModel.class);
        aemContext.load().json(TestConstants.COMPLEX_PANEL, "/complexPanel");
        aemContext.currentResource(TestConstants.COMPLEX_PANEL_COMP);
        Resource resource = aemContext.request().getResource();
        complexPanelModel = resource.adaptTo(ComplexPanelModel.class);
    }

    @Test
    void getPanelTitle() {
        final String expected = "Coperation with strategic partner Manulife";
        assertEquals(expected, complexPanelModel.getPanelTitle());
    }

    @Test
    void getDescription() {
        final String expected = "<p>The Description for Component.</p>\r\n";
        assertEquals(expected, complexPanelModel.getDescription());
    }

    @Test
    void getDateText() {
        final String expected = "Date 3rd August";
        assertEquals(expected, complexPanelModel.getDateText());
    }

    @Test
    void getTopImage() {
        final String expected = "/content/dam/techcombank/adobe-qa/Banner_khach_hang_ca_nhan_desktop_05657b81f4.jpg";
        assertEquals(expected, complexPanelModel.getTopImage());
    }

    @Test
    void getTopAltText() {
        final String expected = "text 1";
        assertEquals(expected, complexPanelModel.getTopAltText());
    }

    @Test
    void getBottomImage() {
        final String expected = "/content/dam/techcombank/adobe-qa/CASA_desktop_fc17150904.jpg";
        assertEquals(expected, complexPanelModel.getBottomImage());
    }

    @Test
    void getBottomAltText() {
        final String expected = "text 2";
        assertEquals(expected, complexPanelModel.getBottomAltText());
    }

    @Test
    void getRightImage() {
        final String expected = "/content/dam/techcombank/adobe-qa/Capture24.PNG";
        assertEquals(expected, complexPanelModel.getRightImage());
    }

    @Test
    void getRightAltText() {
        final String expected = "text 3";
        assertEquals(expected, complexPanelModel.getRightAltText());
    }
}
