package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class LoanCalculatorTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private LoanCalculator loanCalculator;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(LoanCalculator.class);
        aemContext.load().json(TestConstants.LOANCALCULATOR, "/loanCalculator");
        aemContext.currentResource(TestConstants.LOANCALULATORCOMP);
        Resource resource = aemContext.request().getResource();
        loanCalculator = resource.adaptTo(LoanCalculator.class);
    }

    @Test
    void getTooltipIcon() {
        String expected = "/content/dam/techcombank/brand-assets/images/icon_8c14fabb1a.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, loanCalculator.getTooltipIcon());
    }

    @Test
    void getTooltipIconMobile() {
        String expected = "/content/dam/techcombank/brand-assets/images/icon_8c14fabb1a.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, loanCalculator.getTooltipIconMobile());
    }

    @Test
    void getMonths() {
        LocalDate currentdate = LocalDate.now();
        assertEquals(currentdate.getMonth() + " " + currentdate.getYear(), loanCalculator.getMonths());
    }

    @Test
    void getTabs() {
        String expected = "Decreasing balance sheet;Table of fixed monthly payment";
        assertEquals(expected, loanCalculator.getTabs());
    }

    @Test
    void getTabText() {
        String expected = "Disbursement date";
        assertEquals(expected, loanCalculator.getTabText());
    }

    @Test
    void getValueLabelText() {
        String expected = "Property value";
        assertEquals(expected, loanCalculator.getValueLabelText());
    }

    @Test
    void getValueLabelToolTip() {
        String expected = "Disbursement date";
        assertEquals(expected, loanCalculator.getValueLabelToolTip());
    }

    @Test
    void getValueLabelInput() {
        String expected = "VND";
        assertEquals(expected, loanCalculator.getValueLabelInput());
    }

    @Test
    void getLoanAmountLabelText() {
        String expected = "Loan Amount";
        assertEquals(expected, loanCalculator.getLoanAmountLabelText());
    }

    @Test
    void getLoanAmountToolTip() {
        String expected = "tooltip";
        assertEquals(expected, loanCalculator.getLoanAmountToolTip());
    }

    @Test
    void getLoanAmountSlider() {
        String expected = "1";
        assertEquals(expected, loanCalculator.getLoanAmountSlider());
    }

    @Test
    void getLoanAmountInput() {
        String expected = "VND";
        assertEquals(expected, loanCalculator.getLoanAmountInput());
    }

    @Test
    void getLoanDescription() {
        String expected = "Enter the amount you want to borrow or drag to select the"
                + " loan rate according to the property value";
        assertEquals(expected, loanCalculator.getLoanDescription());
    }

    @Test
    void getLoanTermLabelText() {
        String expected = "Loan term";
        assertEquals(expected, loanCalculator.getLoanTermLabelText());
    }

    @Test
    void getLoanTermToolTip() {
        String expected = "tooltip";
        assertEquals(expected, loanCalculator.getLoanTermToolTip());
    }

    @Test
    void getLoanTermInput() {
        String expected = "Month";
        assertEquals(expected, loanCalculator.getLoanTermInput());
    }

    @Test
    void getInterestRateLabelText() {
        String expected = "Interest rate";
        assertEquals(expected, loanCalculator.getInterestRateLabelText());
    }

    @Test
    void getInterestRateToolTip() {
        String expected = "interest rate";
        assertEquals(expected, loanCalculator.getInterestRateToolTip());
    }

    @Test
    void getInterestRateInput() {
        String expected = "%/Year";
        assertEquals(expected, loanCalculator.getInterestRateInput());
    }

    @Test
    void getDisbursementDateLabelText() {
        String expected = "Disbursement date";
        assertEquals(expected, loanCalculator.getDisbursementDateLabelText());
    }

    @Test
    void getDisbursementDateToolTip() {
        String expected = "Disbursement date";
        assertEquals(expected, loanCalculator.getDisbursementDateToolTip());
    }

    @Test
    void getDisbursementDatePicker() {
        String expected = "date";
        assertEquals(expected, loanCalculator.getDisbursementDatePicker());
    }

    @Test
    void getModalHeader() {
        String expected = "Loan payment schedule sheet with decreasing balance";
        assertEquals(expected, loanCalculator.getModalHeader());
    }

    @Test
    void getSerialNoLabel() {
        String expected = "No";
        assertEquals(expected, loanCalculator.getSerialNoLabel());
    }

    @Test
    void getRepaymentPeriodLabel() {
        String expected = "REPAYMENT PERIOD";
        assertEquals(expected, loanCalculator.getRepaymentPeriodLabel());
    }

    @Test
    void getRemainingAmountLabel() {
        String expected = "REMAINING ORIGINAL AMOUNT";
        assertEquals(expected, loanCalculator.getRemainingAmountLabel());
    }

    @Test
    void getOriginalAmountLabel() {
        String expected = "Origin";
        assertEquals(expected, loanCalculator.getOriginalAmountLabel());
    }

    @Test
    void getInterestAmountLabel() {
        String expected = "INTEREST";
        assertEquals(expected, loanCalculator.getInterestAmountLabel());
    }

    @Test
    void getSumLabel() {
        String expected = "TOTAL PRINCIPAL + INTEREST";
        assertEquals(expected, loanCalculator.getSumLabel());
    }

    @Test
    void getTotalLabel() {
        String expected = "TOTAL";
        assertEquals(expected, loanCalculator.getTotalLabel());
    }

    @Test
    void getTooltipAlt() {
        String expected = "3";
        assertEquals(expected, loanCalculator.getTooltipAlt());
    }

    @Test
    void getCalculatorFieldsPartOne() {
        LoanCalculatorFieldsModel loanCalculatorFieldsModel = loanCalculator.getCalculatorFields().get(0);
        assertEquals("/content/dam/techcombank/brand-assets/images/money_0f1c128322.svg",
                loanCalculatorFieldsModel.getDecreasingCardIcon());
        assertEquals("Decreasing balance sheet", loanCalculatorFieldsModel.getTabName());
        assertEquals("decreasingBalanceSheet", loanCalculatorFieldsModel.getCalculationTypeDropdown());
        assertEquals("Decreasing amount balance sheet", loanCalculatorFieldsModel.getDecreasingPopUpTitleText());
        assertEquals("VND", loanCalculatorFieldsModel.getDecreasingFromPayValue());
        assertEquals("alt text", loanCalculatorFieldsModel.getDecreasingImgAltText());
        assertEquals("VND", loanCalculatorFieldsModel.getDecreasingToPayValue());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png",
                loanCalculatorFieldsModel.getDecreasingBg());
        assertEquals("Month", loanCalculatorFieldsModel.getDecreasingMonthTextLoanTerm());
        assertEquals("See more", loanCalculatorFieldsModel.getDecreasingLinkTextLabel());
        assertEquals("Monthly payment amount", loanCalculatorFieldsModel.getDecreasingMonthlyLabelText());
        assertEquals("Total Interest payable", loanCalculatorFieldsModel.getDecreasingTotalIntLabel());
        assertEquals("VND", loanCalculatorFieldsModel.getDecreasingTotalIntValue());
        assertEquals("VND", loanCalculatorFieldsModel.getDecreasingCurrencyText());
        assertEquals("%/year", loanCalculatorFieldsModel.getDecreasingPercentageIntRate());
        assertEquals("VND", loanCalculatorFieldsModel.getFixedTotalIntValue());
        assertEquals("VND", loanCalculatorFieldsModel.getFixedTotalIntLabel());
        assertEquals("From", loanCalculatorFieldsModel.getCarMonIntRateValue());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png", loanCalculatorFieldsModel.getCarBg());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png", loanCalculatorFieldsModel.getCarBgMobile());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png/"
                + "jcr:content/renditions/cq5dam.web.1280.1280.png", loanCalculatorFieldsModel.getCarCardIcon());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png/"
                + "jcr:content/renditions/cq5dam.web.640.640.png", loanCalculatorFieldsModel.getCarCardIconMobile());

    }

    @Test
    void getCalculatorFieldsPartTwo() {
        LoanCalculatorFieldsModel loanCalculatorFieldsModel = loanCalculator.getCalculatorFields().get(0);
        assertEquals("To", loanCalculatorFieldsModel.getDecreasingToLabelText());
        assertEquals("To", loanCalculatorFieldsModel.getDecreasingToLabelText());
        assertEquals("From", loanCalculatorFieldsModel.getDecreasingFromLabelText());
        assertEquals("/content/dam/techcombank/brand-assets/images/"
                        + "compare_saving_bg3_541ff3a195.png/jcr:content/renditions/cq5dam.web.1280.1280.png",
                loanCalculatorFieldsModel.getFixedBg());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.640.640.png",
                loanCalculatorFieldsModel.getFixedBgMobile());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.1280.1280.png",
                loanCalculatorFieldsModel.getFixedCardIcon());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.640.640.png",
                loanCalculatorFieldsModel.getFixedCardIconMobile());
        assertEquals("From", loanCalculatorFieldsModel.getFixedImageAltText());
        assertEquals("From", loanCalculatorFieldsModel.getFixedAmountLabelText());
        assertEquals("VND", loanCalculatorFieldsModel.getFixedMonthlyAmountValue());
        assertEquals("Total Interest payable", loanCalculatorFieldsModel.getDecreasingTotalIntLabel());
        assertEquals("VND", loanCalculatorFieldsModel.getDecreasingTotalIntValue());
        assertEquals("VND", loanCalculatorFieldsModel.getFixedCurrencyText());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.1280.1280.png",
                loanCalculatorFieldsModel.getCarBg());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.640.640.png",
                loanCalculatorFieldsModel.getCarBgMobile());

    }

    @Test
    void getCalculatorFieldsPartThree() {
        LoanCalculatorFieldsModel loanCalculatorFieldsModel = loanCalculator.getCalculatorFields().get(0);
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.1280.1280.png",
                loanCalculatorFieldsModel.getCarCardIcon());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.640.640.png",
                loanCalculatorFieldsModel.getCarCardIconMobile());
        assertEquals("/content/dam/techcombank/brand-assets/images/compare_saving_bg3_541ff3a195.png"
                        + "/jcr:content/renditions/cq5dam.web.640.640.png",
                loanCalculatorFieldsModel.getDecreasingBgMobile());
        assertEquals("/content/dam/techcombank/brand-assets/images/money_0f1c128322.svg",
                loanCalculatorFieldsModel.getDecreasingCardIconMobile());
        assertEquals("From", loanCalculatorFieldsModel.getCarIconAltText());
        assertEquals("VND", loanCalculatorFieldsModel.getCarMonInstallmentLabel());
        assertEquals("VND", loanCalculatorFieldsModel.getCarMonInstallmentValue());
        assertEquals("From", loanCalculatorFieldsModel.getCarMonIntRateLabel());
        assertEquals("From", loanCalculatorFieldsModel.getCarMonPrincipalLabel());
        assertEquals("From", loanCalculatorFieldsModel.getCarMonPrincipalValue());
        assertEquals("VND", loanCalculatorFieldsModel.getCarCurrencyText());
        assertEquals("From", loanCalculatorFieldsModel.getCarLinkText());
        assertEquals("From", loanCalculatorFieldsModel.getCarPopUpTitle());
    }
}
