package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListViewDocumentModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListViewDocumentModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListRowModel.class);
        aemContext.load().json(TestConstants.LISTVIEWDOCUMENT, "/listViewDocument");
        aemContext.currentResource(TestConstants.LISTVIEWDOCUMENTCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListViewDocumentModel.class);
    }

    @Test
    void testGetMethods() {
        final String title = "Title";
        assertEquals(title, model.getTitle());
        final String quarter = "Quarter";
        assertEquals(quarter, model.getQuarterLabel());
        final String year = "Year";
        assertEquals(year, model.getYearLabel());
        final String yearColon = "Year:";
        assertEquals(yearColon, model.getYearColonLabel());
        final String number = "10";
        assertEquals(number, model.getNumberOfRecords());
        final String loading = "Loading PDF";
        assertEquals(loading, model.getLoadingPDF());
        final String download = "Download";
        assertEquals(download, model.getDownload());
        final String tabs = "Tab1;Tab2;Tab3";
        assertEquals(tabs, model.getTabs());
        final String apply = "Apply";
        assertEquals(apply, model.getApplyLabel());
        final String select = "Select";
        assertEquals(select, model.getFilterPlaceholderLabel());
        assertTrue(model.isHideFilter());
    }

    @Test
    void testNotNull() {
        assertNotNull(model.getTabItems());
        assertNotNull(model.getYearList());
    }

}
