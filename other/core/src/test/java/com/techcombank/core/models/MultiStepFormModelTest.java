package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MultiStepFormModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private MultiStepFormModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(MultiStepFormModel.class);
        aemContext.load().json(TestConstants.MULTISTEPFORM, "/multiStepForm");
        aemContext.currentResource(TestConstants.MULTISTEPFORMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(MultiStepFormModel.class);
    }

    @Test
    void getFormId() {
        final String expected = "text";
        assertEquals(expected, model.getFormId());
    }

    @Test
    void getNumberOfSteps() {
        final int expected = 1;
        assertEquals(expected, model.getNumberOfSteps());
    }

}
