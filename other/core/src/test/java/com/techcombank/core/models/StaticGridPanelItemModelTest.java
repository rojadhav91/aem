package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class StaticGridPanelItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private StaticGridPanelItemModel staticGridPanelItemModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(StaticGridPanelModel.class);
        aemContext.load().json(TestConstants.STATICGRIDPANELITEM, "/staticGridPanelItem");
        aemContext.currentResource(TestConstants.STATICGRIDPANELITEMCOMP);
        Resource resource = aemContext.request().getResource();
        staticGridPanelItemModel = resource.adaptTo(StaticGridPanelItemModel.class);
    }

    @Test
    void getIconImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, staticGridPanelItemModel.getIconImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(staticGridPanelItemModel.getIconImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                staticGridPanelItemModel.getWebIconImagePath());
        assertEquals(staticGridPanelItemModel.getIconImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                staticGridPanelItemModel.getMobileIconImagePath());
    }

    @Test
    void getIconTitle() {
        final String expected = "icon1";
        assertEquals(expected, staticGridPanelItemModel.getIconTitle());
    }

    @Test
    void getIconImageAltText() {
        final String expected = "icon1 alt";
        assertEquals(expected, staticGridPanelItemModel.getIconImageAltText());
    }

    @Test
    void getDescription() {
        final String expected = "icon description";
        assertEquals(expected, staticGridPanelItemModel.getDescription());
    }
}
