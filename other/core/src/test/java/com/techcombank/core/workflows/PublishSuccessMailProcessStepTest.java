package com.techcombank.core.workflows;

import com.adobe.acs.commons.notifications.InboxNotification;
import com.adobe.acs.commons.notifications.InboxNotificationSender;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PublishSuccessMailProcessStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private Workflow workflow;

    @Mock
    private InboxNotificationSender inboxNotificationSender;

    @Mock
    private InboxNotification inboxNotification;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private PublishSuccessMailProcessStep publishSuccessMailProcessStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workItem.getWorkflowData().getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn("/content/sample/page");
        when(metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class)).
                thenReturn("approver=FIRST_APPROVER,title=Sample,subject=Sample");
        when(metaDataMap.get(TCBConstants.FIRST_APPORVER, String.class)).thenReturn("First Approver");
        when(workflow.getInitiator()).thenReturn("initiator@example.com");
        when(inboxNotificationSender.buildInboxNotification()).thenReturn(inboxNotification);
        when(inboxNotification.getTitle()).thenReturn("Approval Title");
        when(metaDataMap.containsKey(TCBConstants.EMAIL_ENABLE)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("sample@example.com");
        when(metaDataMap.containsKey(TCBConstants.WORKFLOW_TITLE)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.WORKFLOW_TITLE, String.class)).thenReturn("Publisg Success Mail");
        when(metaDataMap.containsKey(TCBConstants.DESTINATION_FILE_NAME)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.DESTINATION_FILE_NAME, String.class)).thenReturn("image.jpg");
    }

    @Test
    void testExecuteMailEnabled() throws WorkflowException {
        publishSuccessMailProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_3)).getWorkflowData();
    }
}
