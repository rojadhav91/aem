package com.techcombank.core.models;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })

class TabsModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private TabsModel tabsModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TabsModel.class);
        aemContext.load().json(TestConstants.TABS, "/tabs");
        aemContext.currentResource(TestConstants.TABSCOMP);
        Resource resource = aemContext.request().getResource();
        tabsModel = resource.adaptTo(TabsModel.class);
    }
    @Test
    void getTabLabelAlignment() {
        final String expected = "vertical";
        assertEquals(expected, tabsModel.getTabLabelAlignment());
    }
    @Test
    void testLanguageTitle() {
        String expected = StringUtils.EMPTY;
        assertEquals(expected, tabsModel.getLanguageTitle());
    }
}
