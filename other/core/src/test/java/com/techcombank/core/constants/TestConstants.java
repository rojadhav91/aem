package com.techcombank.core.constants;

public final class TestConstants {

    public static final String ASSECT = "/content/dam/techcombank/asset.jpg";
    public static final String MYPAGE = "/content/mypage";
    public static final String SCTION_CONTAINER = "section-container";
    public static final String SECTION_CONTAINER_RESOURCE = "techcombank/components/atomic/sectionContainer";
    public static final String SLING_RESOURCETYPE = "sling:resourceType";
    public static final String DIVIDER_RESOURCE = "techcombank/components/atomic/divider";
    public static final String DIVIDER = "divider";

    public static final String FOOTER = "/content/techcombank/web/vn/en/footer.json";

    public static final String MANAGEMENTPANEL = "/content/techcombank/web/vn/en/management-panel.json";

    public static final String STORYLISTING = "/content/techcombank/web/vn/en/storyListing.json";
    public static final String STORYLISTINGCOMP = "/storylisting";
    public static final String FOOTERCOMP = "/footer/footer";

    public static final String MANAGEMENT = "/management-panel/management-panel";
    public static final String FOOTERURL = "/content/techcombank/web/vn/en/sample.html";
    public static final String TRANSACTION_JSON = "/content/techcombank/web/vn/en/transactionTypeModel.json";
    public static final String TRANSACTIONTYPE_COMP = "/transactiontypemodel/transactiontypemodel";
    public static final String SEO_JSON = "/content/techcombank/web/vn/en/seo.json";
    public static final String SEO_COMP = "/seo/seo";
    public static final String FOOTER_MENU = "/content/techcombank/web/vn/en/footermenu.json";
    public static final String FOOTERMENUCOMP = "/footermenu/footermenu";

    public static final String INSURANCE_GAIN = "/content/techcombank/web/vn/en/insuranceGain.json";

    public static final String INSURANCE_GAIN_COMP = "/insurance-gain/insurance-gain";

    public static final String INSURANCE_GAIN_LIST = "/insurance-gain/insurance-list";

    public static final String STEP_ACCORDION = "/content/techcombank/web/vn/en/stepaccordion.json";
    public static final String STEP_ACCORDION_COMP = "/stepaccordion/stepaccordion";

    public static final String COMPLEX_PANEL = "/content/techcombank/web/vn/en/complexPanel.json";
    public static final String COMPLEX_PANEL_COMP = "/complexPanel/complexPanel";

    public static final String LIST_EVENT_TILE = "/content/techcombank/web/vn/en/listEventTile.json";
    public static final String LIST_EVENT_TILE_COMP = "/listEventTile/listEventTile";

    public static final String ACCORDION = "/content/techcombank/web/vn/en/accordion.json";
    public static final String ACCORDIONCOMP = "/accordion/accordion";

    public static final String ICONIMAGE = "/content/techcombank/web/vn/en/iconImage.json";
    public static final String ICONIMAGECOMP = "/iconImage/iconImage";

    public static final String LINKTEXT = "/content/techcombank/web/vn/en/linkText.json";
    public static final String LINKTEXTCOMP = "/linkText/linkText";
    public static final String LINKTEXTCOMPHOR = "/linkText/linkText1";
    public static final int LINK_TEXT_COLUMNS_NUMBER = 3;
    public static final int LINK_TEXT_COLUMNS_NUMBER_ONE = 1;

    public static final String LINKTEXTITEM = "/content/techcombank/web/vn/en/linkTextItem.json";
    public static final String LINKTEXTITEMCOMP = "/linkTextItem/linkTextItem";
    public static final int LINK_TEXT_ITEM_INDEX = 1;

    public static final String NETWORKGRID = "/content/techcombank/web/vn/en/networkGrid.json";
    public static final String NETWORKGRIDCOMP = "/networkGrid/networkGrid";

    public static final String OFFER_LISTING_PROMOTIONS_CARD = "offerListingPromotionCard.json";

    public static final String NETWORKGRIDITEM = "/content/techcombank/web/vn/en/networkGridItem.json";

    public static final String NESTEDDROPDOWN = "/content/techcombank/web/vn/en/nesteddropdown.json";
    public static final String TERMS = "/content/techcombank/web/vn/en/terms.json";
    public static final String NETWORKGRIDITEMCOMP = "/networkGridItem/networkGridItem";
    public static final String NESTEDDROPDOWNCOMP = "/content/nesteddropdown/jcr:content/root/container/nesteddropdown";
    public static final String TERMSCOMP = "/content/termsconditions/jcr:content/root/container/termsconditions";

    public static final String SPACER = "/content/techcombank/web/vn/en/spacer.json";
    public static final String SPACERCOMP = "/spacer/spacer";

    public static final String STATICGRIDPANEL = "/content/techcombank/web/vn/en/staticGridPanel.json";
    public static final String STATICGRIDPANELCOMP = "/staticGridPanel/staticGridPanel";

    public static final String STATICGRIDPANELITEM = "/content/techcombank/web/vn/en/staticGridPanelItem.json";
    public static final String STATICGRIDPANELITEMCOMP = "/staticGridPanelItem/staticGridPanelItem";

    public static final String TABS = "/content/techcombank/web/vn/en/tabs.json";
    public static final String TABSCOMP = "/tabs/tabs";

    public static final String IMAGECARDSLIDE = "/content/techcombank/web/vn/en/imageCardSlide.json";
    public static final String IMAGECARDSLIDECOMP = "/imagecardslide/imagecardslide";

    public static final String MODAL = "/content/techcombank/web/vn/en/modal.json";
    public static final String MODALCOMP = "/modal/modal";

    public static final String BREADCRUMB = "/content/techcombank/web/vn/en/breadcrumb.json";
    public static final String BREADCRUMBCOMP = "/breadcrumb/breadcrumb";

    public static final String LISTTILES = "/content/techcombank/web/vn/en/listTiles.json";
    public static final String LISTTILESCOMP = "/listTiles/listTiles";

    public static final String LISTTILESITEM = "/content/techcombank/web/vn/en/listTilesItem.json";
    public static final String LISTTILESITEMCOMP = "/listTilesItem/listTilesItem";

    public static final String LISTROWITEM = "/content/techcombank/web/vn/en/listRowItem.json";
    public static final String LISTROWITEMCOMP = "/listRowItem/listRowItem";

    public static final String LISTROW = "/content/techcombank/web/vn/en/listRow.json";
    public static final String LISTROWCOMP = "/listRow/listRow";

    public static final String LISTVIEWDOCUMENT = "/content/techcombank/web/vn/en/listViewDocument.json";
    public static final String LISTVIEWDOCUMENTCOMP = "/listViewDocument/listViewDocument";

    public static final String LISTVIEWDOCUMENTLEVELTWO =
            "/content/techcombank/web/vn/en/listViewDocumentLevelTwoItem.json";
    public static final String LISTVIEWDOCUMENTLEVELTWOCOMP =
            "/listViewDocumentItemsSecondLevel/listViewDocumentItemsSecondLevel";

    public static final String LISTVIEWDOCUMENTTABITEM =
            "/content/techcombank/web/vn/en/listViewDocumentTabItem.json";
    public static final String LISTVIEWDOCUMENTTABITEMCOMP = "/tabItems/tabItems";

    public static final String LISTVIEWDOCUMENTLEVELONE =
            "/content/techcombank/web/vn/en/listViewDocumentLevelOneItem.json";
    public static final String LISTVIEWDOCUMENTEVELONECOMP =
            "/listViewDocumentItemsFirstLevel/listViewDocumentItemsFirstLevel";

    public static final String DEVICELOOKSTEPSCAROUSEL = "/content/techcombank/web/vn/en/deviceLookStepsCarousel.json";
    public static final String DEVICELOOKSTEPSCAROUSELCOMP = "/deviceLookStepsCarousel/deviceLookStepsCarousel";

    public static final String DEVICELOOKSTEPSCAROUSELITEM =
            "/content/techcombank/web/vn/en/deviceLookStepsCarouselItem.json";
    public static final String DEVICELOOKSTEPSCAROUSELITEMCOMP =
            "/deviceLookStepsCarouselItem/deviceLookStepsCarouselItem";

    public static final String CARDINFO = "/content/techcombank/web/vn/en/cardInfo.json";
    public static final String CARDINFOCOMP = "/cardInfo/cardInfo";

    public static final String COMPAREPANELITEM = "/content/techcombank/web/vn/en/comparePanelItem.json";
    public static final String COMPAREPANELITEMCOMP = "/comparePanelItem/comparePanelItem";

    public static final String COMPAREPANELTAG = "/content/techcombank/web/vn/en/comparePanelTag.json";
    public static final String COMPAREPANELTAGCOMP = "/comparePanelTag/comparePanelTag";

    public static final String CARDINFOITEM = "/content/techcombank/web/vn/en/cardInfoItem.json";
    public static final String CARDINFOITEMCOMP = "/cardInfoItem/cardInfoItem";

    public static final String PERSONNELPANEL = "/content/techcombank/web/vn/en/personnelPanel.json";
    public static final String PERSONNELPANELCOMP = "/personnelPanel/personnelPanel";

    public static final String ARTICLECARDCROSS = "/content/techcombank/web/vn/en/articleCardCross.json";
    public static final String ARTICLECARDCROSSCOMP = "/articleCardCross/articleCardCross";

    public static final String ARTICLECARDCROSSITEM = "/content/techcombank/web/vn/en/articleCardCrossItem.json";
    public static final String ARTICLECARDCROSSITEMCOMP = "/articleCardCrossItem/articleCardCrossItem";

    public static final String CAROUSELINFO = "/content/techcombank/web/vn/en/carouselInfo.json";
    public static final String CAROUSELINFOCOMP = "/carouselInfo/carouselInfo";

    public static final String CAROUSELINFOITEM = "/content/techcombank/web/vn/en/carouselInfoItem.json";
    public static final String CAROUSELINFOITEMCOMP = "/carouselInfoItem/carouselInfoItem";

    public static final String CAROUSELTILES = "/content/techcombank/web/vn/en/carouselTiles.json";
    public static final String CAROUSELTILESCOMP = "/carouselTiles/carouselTiles";

    public static final String CAROUSELTILESITEM = "/content/techcombank/web/vn/en/carouselTilesItem.json";
    public static final String CAROUSELTILESITEMCOMP = "/carouselTilesItem/carouselTilesItem";

    public static final String CCASHSCOREBOARD = "/content/techcombank/web/vn/en/cCashScoreBoard.json";
    public static final String CCASHSCOREBOARDCOMP = "/cCashScoreBoard/cCashScoreBoard";

    public static final String SOCIALSHAREINFO = "/content/techcombank/web/vn/en/socialShareInfo.json";
    public static final String SOCIALSHAREINFOCOMP = "/socialShareInfo/socialshare";

    public static final String SOCIALSHAREINFOITEM = "/content/techcombank/web/vn/en/socialShareInfoItem.json";
    public static final String SOCIALSHAREINFOITEMCOMP = "/socialShareInfoItem/socialShareInfoItem";

    public static final String COLUMNPANELITEM = "/content/techcombank/web/vn/en/columnPanel.json";
    public static final String COLUMNPANELLISTITEM = "/content/techcombank/web/vn/en/ColumnPanelList.json";

    public static final String COLUMNPANELCARDLISTITEM = "/content/techcombank/web/vn/en/ColumnPanelCardList.json";

    public static final String SURVEYPANEL = "/content/techcombank/web/vn/en/surveyPanel.json";
    public static final String SURVEYPANELCOMP = "/surveyPanel/surveyPanel";

    public static final String CREDITRATING = "/content/techcombank/web/vn/en/creditRating.json";
    public static final String CREDITRATINGCOMP = "/creditRating/creditRating";

    public static final String CREDITRATINGDATA = "/content/techcombank/web/vn/en/creditRatingData.json";
    public static final String CREDITRATINGDATACOMP = "/creditRatingData/creditRatingData";

    public static final String CREDITRATINGREPORT = "/content/techcombank/web/vn/en/creditRatingReport.json";
    public static final String CREDITRATINGREPORTCOMP = "/creditRatingReport/creditRatingReport";

    public static final String CREDITRATINGTAB = "/content/techcombank/web/vn/en/creditRatingTab.json";
    public static final String CREDITRATINGTABCOMP = "/creditRatingTab/creditRatingTab";

    public static final String YEARFORFILTER = "/content/techcombank/web/vn/en/yearForFilter.json";
    public static final String YEARFORFILTERCOMP = "/yearForFilter/yearForFilter";

    public static final String COMPAREPANEL = "/content/techcombank/web/vn/en/comparePanel.json";
    public static final String COMPAREPANELCOMP = "/comparePanel/comparePanel";

    public static final String ANNOUNCEMENTINFO = "/content/techcombank/web/vn/en/announcementmodel.json";

    public static final String ANNOUNCEMENTINFOCOMP = "/announcementmodel/announcement";

    public static final String CARDSLIDEPANEL = "/content/techcombank/web/vn/en/cardSlidePanel.json";
    public static final String CARDSLIDEPANELCOMP = "/cardSlidePanel/cardSlidePanel";

    public static final String MEDIACAROUSEL = "/content/techcombank/web/vn/en/mediaCarousel.json";
    public static final String MEDIACAROUSELCOMP = "/mediaCarousel/mediaCarousel";
    public static final String MEDIACAROUSELITEM = "/content/techcombank/web/vn/en/mediaCarouselItem.json";
    public static final String MEDIACAROUSELITEMCOMP = "/mediaCarouselItem/mediaCarouselItem";

    public static final String MASTHEADEXTEND = "/content/techcombank/web/vn/en/mastheadExtend.json";
    public static final String MASTHEADEXTENDCOMP = "/mastheadExtend/mastheadExtend";

    public static final String MASTHEADEXTENDIMAGE = "/content/techcombank/web/vn/en/mastheadExtendImage.json";
    public static final String MASTHEADEXTENDIMAGECOMP = "/mastheadExtendImage/mastheadExtendImage";

    public static final String MASTHEADEXTENDCARDARTICLEIMAGE =
            "/content/techcombank/web/vn/en/mastheadExtendCardArticleImage.json";
    public static final String MASTHEADEXTENDCARDARTICLEIMAGECOMP =
            "/mastheadExtendCardArticleImage/mastheadExtendCardArticleImage";

    public static final String TABLECOMPARE = "/content/techcombank/web/vn/en/tableCompare.json";
    public static final String TABLECOMPARECOMP = "/tableCompare/tableCompare";

    public static final String TABLECOMPARECOLUMNHEADER =
            "/content/techcombank/web/vn/en/tableCompareColumnHeader.json";
    public static final String TABLECOMPARECOLUMNHEADERCOMP = "/tableCompareColumnHeader/tableCompareColumnHeader";

    public static final String TABLECOMPARECARD = "/content/techcombank/web/vn/en/tableCompareCard.json";
    public static final String TABLECOMPARECARDCOMP = "/tableCompareCard/tableCompareCard";

    public static final String TABLECOMPAREROW = "/content/techcombank/web/vn/en/tableCompareRow.json";
    public static final String TABLECOMPAREROWCOMP = "/tableCompareRow/tableCompareRow";

    public static final String INSPIREPRIVILEGE = "/content/techcombank/web/vn/en/inspirePrivilege.json";
    public static final String INSPIREPRIVILEGECOMP = "/inspirePrivilege/inspirePrivilege";

    public static final String INSPIREPRIVILEGEIMAGEROW =
            "/content/techcombank/web/vn/en/inspirePrivilegeImageRow.json";
    public static final String INSPIREPRIVILEGEIMAGEROWCOMP =
            "/inspirePrivilegeImageRow/inspirePrivilegeImageRow";

    public static final String INSPIREPRIVILEGECONTENTLIST =
            "/content/techcombank/web/vn/en/inspirePrivilegeContentList.json";
    public static final String INSPIREPRIVILEGECONTENTLISTCOMP =
            "/inspirePrivilegeContentList/inspirePrivilegeContentList";

    public static final String PRIMARYSEARCH = "/content/techcombank/web/vn/en/primarySearch.json";
    public static final String PRIMARYSEARCHCOMP = "/primarySearch/primarySearch";

    public static final String USEFULLINKSITEM = "/content/techcombank/web/vn/en/usefulLinksItem.json";
    public static final String USEFULLINKSITEMCOMP = "/usefulLinksItem/usefulLinksItem";

    public static final String MULTISTEPFORM = "/content/techcombank/web/vn/en/multiStepForm.json";
    public static final String MULTISTEPFORMCOMP = "/multiStepForm/multiStepForm";

    public static final String BLANKEMBED = "/content/techcombank/web/vn/en/blankEmbed.json";
    public static final String BLANKEMBEDCOMP = "/blankEmbed/blankEmbed";

    // Header Navigation
    public static final String CONTENTPATH = "/content";
    public static final String HEADER_MODEL_JSON = "/content/techcombank/web/vn/en/HeaderTest.json";
    public static final String PROP_ALIAS = "sling:alias";
    public static final String LANG_VI = "Vietnam";
    public static final String LANG_EN = "English";
    public static final int LIST_SIZE_THREE = 3;
    public static final int LIST_SIZE_NINE = 9;
    public static final int LIST_SIZE_TWO = 2;
    public static final int LIST_SIZE_TEN = 10;
    public static final int LIST_SIZE_ZERO = 0;
    public static final int LIST_SIZE_FOUR = 4;
    public static final String COLUMNCONTAINER = "/content/techcombank/web/vn/en/columnContainer.json";

    public static final String TITLE = "/content/techcombank/web/vn/en/title.json";

    public static final String ARTICLE_TAG = "/content/techcombank/web/vn/en/articleTagCloud.json";
    public static final String TITLECOMPONENT = "/title/title";

    public static final String ARTICLE_TAG_COMPONENT = "/articletagcloud/articletagcloud";
    public static final String EXTENDEDEMBED = "extendedEmbed.json";
    public static final String EXTENDEDEMBEDCOMPONENT = "/extendedEmbed/extendedEmbed";
    public static final String COLUMN = "/column/column";

    public static final String HOMEPAGEBLOCK = "/content/techcombank/web/vn/en/homepageBlock.json";

    public static final String HOMEPAGEBLOCKCOMP = "/homepageBlock/homepageBlock";

    public static final String COMMONURL = "/content/techcombank/web/vn/en/sample.html";

    // MastHead Banner
    public static final String SIMPLE_BANNER_MODEL_JSON = "/content/techcombank/web/vn/en/SimpleBannerTest.json";
    public static final String SPECIAL_BANNER_MODEL_JSON = "/content/techcombank/web/vn/en/SpecialBannerTest.json";

    // Rate Constants
    public static final String CURRENCY_CONVERTOR_JSON = "currencyConvertorTest.json";
    public static final String EXCHANGE_RATE_JSON = "exchangeRateTest.json";
    public static final String VFX_RATES_JSON = "vfxRateDataTest.json";
    public static final String CURRENCY_CONVERTOR_SERVICE_JSON = "currencyConvertorServiceTest.json";
    public static final String EXCHANGE_RATE = "exchange-rates";
    public static final String GOLD_RATE = "gold-rates";
    public static final String OTHER_RATE = "other-rates";
    public static final String TENOR_RATE = "tenor-rates";
    public static final String TENOR_INT_RATE = "tenor-int-rates";
    public static final String FIXING_RATE = "fixing-rates";
    public static final String EXCHNAGE_RATE_JSON_ARRAY = "exchangeRate";
    public static final String GOLD_RATE_JSON_ARRAY = "goldRate";
    public static final String OTHER_RATE_JSON_ARRAY = "otherRate";
    public static final String TENOR_RATE_JSON_ARRAY = "tenorRate";
    public static final String TENOR_INT_RATE_JSON_ARRAY = "tenorintRate";
    public static final String FIXING_RATE_JSON_ARRAY = "fixingRate";
    public static final String VFX_RATE_DATA = "data";

    public static final String CONTENT_BASE_PATH = "/content/techcombank/web/vn/en/";
    public static final String CONTENT_EVENTS_TAG = "/content/cq:tags/techcombank/event-type";
    public static final String CONTENT_CARD_TAG = "/content/cq:tags/techcombank/card-types/credit-card";
    public static final String CONTENT_QUARTER_TAG = "/content/cq:tags/techcombank";
    public static final String CONTENT_ROOT_SRC_PATH = "src/test/resources/";

    // ModalModel
    public static final String MODAL_MODEL_JSON = "/content/techcombank/web/vn/en/modalModel.json";
    public static final String MODAL_MODEL_COMP = "/modal/modal";

    // Nav equal panel
    public static final String NAV_EQUAL_PANEL_MODEL_JSON = "/content/techcombank/web/vn/en/NavEqualPanelTest.json";

    public static final String MILESTONE_PANEL_MODEL_JSON = "/content/techcombank/web/vn/en/MilestonePanelTest.json";
    public static final String FORM_HIDDEN_MODEL_JSON = "/content/techcombank/web/vn/en/formHiddenFieldModel.json";

    // Button CTA
    public static final String BUTTON_CTA_MODEL_JSON = "ButtonCtaTest.json";

    public static final String RECAPTCHA_JSON = "recaptcha.json";



    // Image Optimization
    public static final String PNG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.png";
    public static final String JPEG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
    public static final String PNG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.png";
    public static final String JPEG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.jpeg";

    // Quick Access
    public static final String QUICK_ACCESS_JSON = "QuickAccessTest.json";
    public static final String DEBTPANEL = "/content/techcombank/web/vn/en/debtPanel.json";
    public static final String DEBTPANELCOMP = "/debtPanel/debtObj";

    // Exchange Rate
    public static final String EXCHANGE_RATE_CMP_JSON = "exchangeRateCmpTest.json";
    public static final String LOANCALCULATOR = "/content/techcombank/web/vn/en/loanCalculator.json";
    public static final String LOANCALULATORCOMP = "/loanCalculator/loanCalculator";

    // Gold Rate
    public static final String GOLD_RATE_CMP_JSON = "goldRateCmpTest.json";

    // Fixing Rate
    public static final String FIXING_RATE_CMP_JSON = "fixingRateCmpTest.json";
    public static final String OFFERLISTING_JSON = "offerListing.json";
    public static final String OFFERLISTINGCOMP = "/offerListing/offerListing";

    public static final String FORMSURVEY = "/content/techcombank/web/vn/en/formSurvey.json";
    public static final String FORMSURVEYCOMP = "/formSurvey/formSurvey";

    public static final String FORMSURVEYITEM = "/content/techcombank/web/vn/en/formSurveyItem.json";
    public static final String FORMSURVEYITEMCOMP = "/formSurveyItem/formSurveyItem";

    //Event Listing
    public static final String EVENT_LISTING_JSON = "eventListing.json";
    public static final String CARD_LISTING_JSON = "cardListing.json";
    public static final String FINANCIALHIGHLIGHTS_JSON = "financialhighlights.json";
    public static final String EVENT_LIST = "/eventlist/eventlist";
    public static final String CARD_LIST = "/cardlist/cardlist";
    public static final String HIGHLIGHTS = "/content/highlights";

    // Stock Chart
    public static final String STOCK_CHART_CMP_JSON = "StockChartTest.json";

    // ATM Branch Map
    public static final String ATM_BRANCH_MAP_JSON = "AtmBranchMap.json";

    // Appointment Booking
    public static final String APPOINTMENT_BOOKING_JSON = "AppointmentBooking.json";

    public static final String ERROR_MODEL_JSON = "errorComponent.json";

    public static final String ENHANCED_FAQ_PANEL_JSON = "enhancedFAQPanel.json";

    public static final String ENHANCED_FILTER_PANEL_JSON = "enhancedFilterPanel.json";

    public static final String TAB_EVENT_CARD_JSON = "tabeventcard.json";

    public static final String TAB_EVENT_CARD_COMP = "/tabEventCard/tabEventCard";
    public static final String TABEVENTCF_JSON = "tabeventcf.json";

    public static final String ILLUSTRATED_PANEL_BENEFITS_JSON = "illustratedPanel.json";
    public static final String ILLUSTRATED_PANEL_BENEFITS_COMP = "/illustratedPanel/illustratedPanel";

    public static final String ARTICLE_RELATEDPOST = "/content/techcombank/web/vn/en/articlerelatedpost.json";
    public static final String ARTICLERELATEDPOST = "/articlerelatedpost/articlerelatedpost";


    public static final String ARTICLE_LISTING = "/content/techcombank/web/vn/en/articlelisting.json";
    public static final String ARTICLELISTING = "/articlelisting/articlelisting";
    private TestConstants() {
    }
}

