package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListTilesModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListTilesModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListTilesModel.class);
        aemContext.load().json(TestConstants.LISTTILES, "/listTiles");
        aemContext.currentResource(TestConstants.LISTTILESCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListTilesModel.class);
    }

    @Test
    void getDisplayLayout() {
        final String expected = "option1";
        assertEquals(expected, model.getDisplayLayout());
    }

    @Test
    void getContainerClasses() {
        assertEquals(TestConstants.LIST_SIZE_TEN, model.getContainerClasses().size());
    }

    @Test
    void getTileClasses() {
        assertEquals(TestConstants.LIST_SIZE_TEN, model.getTileClasses().size());
    }

}
