package com.techcombank.core.service.impl;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.pojo.StoryListingBean;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.utils.PlatformUtils;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;


@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StoryListingServiceImplTest {

    @InjectMocks
    private StoryListingServiceImpl storyListing = new StoryListingServiceImpl();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    private AemContext aemContext = TCBAemContext.newAemContext();
    private String rootPath = "/content/techcombank/web/vn/en/articlepages";

    protected static final String ARTICLE_CREATION_DATE = "articleCreationDate";
    private List<String> content = new ArrayList<>();
    private List<String> category = new ArrayList<>();
    private List<String> contentOne = new ArrayList<>();
    private List<String> categoryOne = new ArrayList<>();

    private List<String> contentTwo = new ArrayList<>();
    private List<String> categoryTwo = new ArrayList<>();

    @Mock
    private Session session;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Query query;

    @Mock
    private SearchResult searchResult;
    @Mock
    private SearchResult searchResultNone;

    @Mock
    private Iterator<Node> nodeIterators;

    @Mock
    private ValueMap valueMap;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    private String json;
    @Mock
    private NodeIterator nodeIterator;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page page;

    @Mock
    private Node node;

    protected static final String TAGSPROPERTY = "jcr:content/@" + NameConstants.PN_TAGS;
    protected static final String GROUP = "group.";
    protected static final String ARTICLECATEGORY = "article-category";
    protected static final String VALUE = "_value";
    protected static final String TRUE = "true";

    protected static final String FILEREFERNCE = "fileReference";
    protected static final String ARTICLEDATE = "@jcr:content/articleCreationDate";

    protected static final String PROPERTYONE = "1_property";
    protected static final String GROUPONE = "group.1_property";
    private String json1;
    protected static final String PATH = "path";
    protected static final String TYPE = "type";
    protected static final String ORDERBY = "orderby";
    protected static final String ORDERBYSORT = "orderby.sort";
    protected static final String OFFSET = "p.offset";
    protected static final String LIMIT = "p.limit";
    protected static final String PROPERTY = "_property";
    protected static final String PROPERTYZERO = "_property.0_value";
    protected static final String AND = ".and";
    protected static final String OR = ".or";
    protected static final String OLDEST = "oldest";
    protected static final String ASC = "asc";
    protected static final String DSC = "desc";
    protected static final String GROUPOR = "group.p.or";
    protected static final String ZEROVALUE = ".0_value";
    protected static final String VIDEO = "techcombank:articles/article-type/video";
    protected static final String ARTICLE = "techcombank:articles/article-type/article";
    protected static final String PROMOTIONS = "techcombank:articles/article-category/promotions";
    protected static final String TWO = "2";
    protected static final String PERSONALDEVELOPMENT = "techcombank:articles/article-category/personaldevelopment";

    protected static final String CATEGORYTAG = "jcr:content/@categoryTag";
    protected static final String ONE = "1";
    protected static final String TEN = "10";
    protected static final String ZERO = "0";

    protected static final String CATEGORYPROPERTY = "categoryTag";
    protected static final String GROUPP = "group.p";
    protected static final String TOTAL = "total";
    protected static final String RESULTS = "results";

    @BeforeEach
    void setUp() {
        aemContext.load().json("/content/techcombank/web/vn/en/articles.json", "/article");
        aemContext.addModelsForClasses(StoryListingServiceImpl.class);
        nodeIterator = mock(NodeIterator.class);
        node = mock(Node.class);
        StoryListingBean storyListingBean = mock(StoryListingBean.class);
        List<StoryListingBean> listingBeanList = new ArrayList<>();
        queryBuilder = mock(QueryBuilder.class);
        session = mock(Session.class);
        query = mock(Query.class);
        searchResult = mock(SearchResult.class);
        aemContext.registerAdapter(ResourceResolver.class, QueryBuilder.class, queryBuilder);
    }

    @Test
    void isPageLoad() {
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put(PATH, "/content/techcombank/web/vn/en/articlepages");
        queryMap.put(TYPE, NameConstants.NT_PAGE);
        queryMap.put(PROPERTYONE, TAGSPROPERTY);
        queryMap.put(GROUPOR, TRUE);

        content = new ArrayList<>();
        content.add(VIDEO);
        category = new ArrayList<>();
        category.add(PROMOTIONS);

        Map<String, String> expectedQueryMap = new TreeMap<>();

        expectedQueryMap.put(PATH, "/content/techcombank/web/vn/en/articlepages");
        expectedQueryMap.put(TYPE, NameConstants.NT_PAGE);
        expectedQueryMap.put(PROPERTYONE, TAGSPROPERTY);
        expectedQueryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                + GROUP + ONE + PROPERTY, "jcr:content/@categoryTag");
        expectedQueryMap.put(GROUP + ONE + TCBConstants.UNDERSCORE
                + GROUPONE + ZEROVALUE, "techcombank:articles/article-type/video");
        expectedQueryMap.put(GROUP + TWO + TCBConstants.UNDERSCORE
                + GROUP + ONE + PROPERTY, "jcr:content/@cq:tags");
        expectedQueryMap.put(GROUP + TWO + TCBConstants.UNDERSCORE
                + GROUPONE + ZEROVALUE, "techcombank:articles/article-category/promotions");
        expectedQueryMap.put(ORDERBY, ARTICLEDATE);
        expectedQueryMap.put(GROUPP + OR, TRUE);
        expectedQueryMap.put(ORDERBYSORT, DSC);
        expectedQueryMap.put(OFFSET, "0");
        expectedQueryMap.put(LIMIT, "10");

        Map<String, String> actualQueryMap =
                storyListing.isPageLoad("/content/techcombank/web/vn/en/articlepages",
                        content, category, queryMap, "10", "0");

        assertEquals(expectedQueryMap, actualQueryMap);

    }

    @Test
    void articleFilter() {
        content = Arrays.asList("techcombank:articles/article-type/video",
                "techcombank:articles/article-type/article");
        category = Arrays.asList("techcombank:articles/article-category/promotions",
                "techcombank:articles/article-category/promotions1");

        contentOne = Arrays.asList("techcombank:articles/article-type/video");
        categoryOne = new ArrayList<>();

        contentTwo = new ArrayList<>();
        categoryTwo = Arrays.asList("techcombank:articles/article-category/promotions",
                "techcombank:articles/article-category/promotions1");


        Map<String, String> expectedMap = new HashMap<>();
        Map<String, String> expectedMap2 = new HashMap<>();
        expectedMap.put(PATH, rootPath);
        expectedMap.put(TYPE, NameConstants.NT_PAGE);
        expectedMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY, CATEGORYTAG);
        expectedMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY
                + TCBConstants.DOT + 0 + TCBConstants.UNDERSCORE
                + VALUE, category.get(0).trim());
        expectedMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + GROUP
                + TWO + TCBConstants.UNDERSCORE + PROPERTY, TAGSPROPERTY);
        expectedMap.put(GROUP + ONE + TCBConstants.UNDERSCORE + GROUP
                + TWO + TCBConstants.UNDERSCORE + PROPERTY + TCBConstants.DOT
                + 1 + TCBConstants.UNDERSCORE + VALUE, content.get(0).trim());
        expectedMap.put(1 + TCBConstants.UNDERSCORE + GROUP + "p" + AND, TRUE);
        expectedMap.put(ORDERBY, ARTICLEDATE);
        expectedMap.put(ORDERBYSORT, ASC);
        expectedMap.put(OFFSET, "0");
        expectedMap.put(LIMIT, "10");

        Map<String, String> actualMap = storyListing.articleFilter(rootPath,
                content, category, expectedMap, "10", "0");

        assertEquals(expectedMap, actualMap);
        Map<String, String> actualMapZero = storyListing.articleFilter(rootPath,
                contentOne, categoryOne, expectedMap, "10", "0");
        assertEquals(actualMapZero, actualMap);

        Map<String, String> actualMapOne = storyListing.articleFilter(rootPath,
                contentTwo, categoryTwo, expectedMap, "10", "0");
        assertEquals(actualMapOne, actualMap);
        // Test case for when content is empty
        content = Arrays.asList("");
        expectedMap = new HashMap<>();
        expectedMap.put(PATH, rootPath);
        expectedMap.put(TYPE, NameConstants.NT_PAGE);
        expectedMap.put(GROUPP + OR, TRUE);
        actualMap = storyListing.articleFilter(rootPath, content, category, expectedMap, "10", "0");
        assertEquals(expectedMap, actualMap);

        // Test case for when category is empty
        category = Arrays.asList("");
        expectedMap = new HashMap<>();
        expectedMap.put(PATH, rootPath);
        expectedMap.put(TYPE, NameConstants.NT_PAGE);
        expectedMap.put(GROUPP + OR, TRUE);
        actualMap = storyListing.articleFilter(rootPath, content, category, expectedMap, "10", "0");
        assertEquals(expectedMap, actualMap);

        // Test case for when both content and category are empty
        content = Arrays.asList("");
        category = Arrays.asList("");
        expectedMap = new HashMap<>();
        expectedMap.put(TYPE, NameConstants.NT_PAGE);
        expectedMap.put("group.p" + OR, TRUE);
        actualMap = storyListing.articleFilter(rootPath, content, category, expectedMap, "10", "0");
        assertEquals(expectedMap, actualMap);

        // Test case for when both content and category are not empty  is used
        expectedMap = new HashMap<>();
        expectedMap.put(PATH, rootPath);
        expectedMap.put(TYPE, NameConstants.NT_PAGE);
        expectedMap.put(GROUPP + OR, TRUE);
        actualMap = storyListing.articleFilter(rootPath, content, category, expectedMap, "10", "0");
        assertEquals(expectedMap, actualMap);

        //Test case when content have only one content and category
        List<String> content2 = new ArrayList<>();
        List<String> category2 = new ArrayList<>();
        content2.add(VIDEO);
        category2.add(PROMOTIONS);
        expectedMap2 = new HashMap<>();
        if ((!content2.isEmpty() && content2.size() == 1) && (!category2.isEmpty()
                && category2.size() == 1)) {
            expectedMap2.put(GROUP + ONE + TCBConstants.UNDERSCORE + PROPERTY, CATEGORYTAG);
            expectedMap2.put(GROUP + ONE + TCBConstants.UNDERSCORE
                    + PROPERTY + TCBConstants.DOT + VALUE, category2.get(0));
            expectedMap2.put(GROUP + TWO + TCBConstants.UNDERSCORE
                    + PROPERTY, TAGSPROPERTY);
            expectedMap2.put(GROUP + TWO + TCBConstants.UNDERSCORE
                    + PROPERTY + TCBConstants.DOT + VALUE, content2.get(0));
            expectedMap2.put(GROUPP + AND, TRUE);
            expectedMap2.put(PATH, rootPath);
            expectedMap2.put(TYPE, NameConstants.NT_PAGE);
            expectedMap2.put(GROUPP + OR, TRUE);
            actualMap = storyListing.articleFilter(rootPath, content2, category2, expectedMap2, "10", "0");
            assertEquals(expectedMap2, actualMap);

        }

        // Test case when content and category with multiple values
        List<String> content1 = new ArrayList<>();
        List<String> category1 = new ArrayList<>();
        content1.add(VIDEO);
        content1.add(ARTICLE);
        category1.add(PROMOTIONS);
        category1.add(PERSONALDEVELOPMENT);

        int counter = 0;
        for (int contentType = 0; contentType < content1.size(); contentType++) {
            int propertyCounter = 0;
            for (int categoryType = 0; categoryType < category1.size(); categoryType++) {
                expectedMap.put(GROUP + counter + TCBConstants.UNDERSCORE
                        + GROUP + propertyCounter + TCBConstants.UNDERSCORE
                        + PROPERTY, TAGSPROPERTY);
                expectedMap.put(GROUP + counter + TCBConstants.UNDERSCORE
                        + GROUP + propertyCounter + TCBConstants.UNDERSCORE + PROPERTY + TCBConstants.DOT
                        + propertyCounter + TCBConstants.UNDERSCORE
                        + VALUE, content1.get(contentType).trim());
                expectedMap.put(GROUP + counter + TCBConstants.UNDERSCORE
                        + GROUP + (propertyCounter + 1) + TCBConstants.UNDERSCORE
                        + PROPERTY, CATEGORYTAG);
                expectedMap.put(GROUP + counter + TCBConstants.UNDERSCORE
                        + GROUP + (propertyCounter + 1) + TCBConstants.UNDERSCORE
                        + PROPERTY + TCBConstants.DOT + (propertyCounter + 1) + TCBConstants.UNDERSCORE
                        + VALUE, category1.get(categoryType).trim());
                expectedMap.put(counter + TCBConstants.UNDERSCORE + GROUP + "p" + AND, TRUE);
                counter++;
            }
        }
        actualMap = storyListing.articleFilter(rootPath, content, category, expectedMap, "10", "0");
        assertEquals(expectedMap, actualMap);

    }

    @Test
    void generateQuery() {
        String resultCount = TEN;
        String offset = ZERO;
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put(PATH, rootPath);
        queryMap.put(TYPE, NameConstants.NT_PAGE);
        Map<String, String> expectedMap = storyListing.isPageLoad(rootPath,
                content, category, queryMap, resultCount, offset);
        Map<String, String> actualMap = storyListing.generateQuery(rootPath,
                content, category, resultCount, offset);

        Map<String, String> expectedFilterMap = storyListing.articleFilter(rootPath,
                content, category, queryMap, resultCount, offset);
        Map<String, String> actualFilterMap = storyListing.generateQuery(rootPath,
                content, category, resultCount, offset);
        assertEquals(expectedMap, actualMap);
        assertEquals(expectedFilterMap, actualFilterMap);
    }

    private StoryListingServiceImpl storyListingService = new StoryListingServiceImpl();

    @Test
    void getResult() throws RepositoryException {
        category = Arrays.asList("techcombank:articles/article-category/promotions",
                "techcombank:articles/article-category/promotions1");
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put(TOTAL, "0");
        jsonMap.put(RESULTS, new ArrayList<>());
        resourceResolver = mock(ResourceResolver.class);
        session = mock(Session.class);
        queryBuilder = mock(QueryBuilder.class);
        query = mock(Query.class);
        searchResult = mock(SearchResult.class);
        searchResultNone = mock(SearchResult.class);
        node = mock(Node.class);
        nodeIterators = mock(Iterator.class);
        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        if (!Objects.isNull(query)) {
            lenient().when(searchResult.getTotalMatches()).thenReturn(1L);
            lenient().when(searchResultNone.getTotalMatches()).thenReturn(0L);
            lenient().when(nodeIterator.hasNext()).thenReturn(true, false);
            lenient().when(nodeIterator.next()).thenReturn(node);
            List<StoryListingBean> listingBeanList = new ArrayList<>();
            StoryListingBean storyListingBean = new StoryListingBean();
            storyListingBean.setPath("/content/techcombank/web/article");
            List<String> secondaryTags = new ArrayList<>();
            storyListingBean.setArticleCategory(secondaryTags);
            listingBeanList.add(storyListingBean);
            lenient().when(query.getResult()).thenReturn(searchResult);
            lenient().when(searchResultNone.getTotalMatches()).thenReturn(0L);
            Map<String, String> queryMapEmpty = new HashMap<>();
            json = storyListing.getResult(resourceResolver, queryMapEmpty);
            assertEquals(new Gson().toJson(jsonMap), json);
            Map<String, String> queryMap = new TreeMap<>();
            queryMap.put(PATH, "/content/techcombank/web/vn/en/articlepages");
            queryMap.put(TYPE, NameConstants.NT_PAGE);
            queryMap.put(PROPERTYONE, TAGSPROPERTY);
            queryMap.put(GROUPOR, TRUE);
            Node firstNode = mock(Node.class);
            Node secondNode = mock(Node.class);
            String[] arr = new String[category.size()];
            lenient().when(valueMap.get("categoryTag", String[].class))
                    .thenReturn(category.toArray(arr));
            lenient().when(PlatformUtils.isInternalLink(resourceResolver, "content/techcombank/en/sample")).thenReturn("content/techcombank/en/sample.html");
            lenient().when(searchResultNone.getTotalMatches()).thenReturn(1L);
            lenient().when(queryBuilder.createQuery(PredicateGroup.create(queryMap), session)).thenReturn(query);
            Iterator<Node> nodeIteratorOne = mock(Iterator.class);
            lenient().when(nodeIteratorOne.hasNext()).thenReturn(true, true, false);
            lenient().when(nodeIteratorOne.next()).thenReturn(firstNode, secondNode);
            lenient().when(searchResult.getNodes()).thenReturn(nodeIteratorOne);
            lenient().when(resourceResolver.getResource(node.getPath()
                    + TCBConstants.SLASH + JcrConstants.JCR_CONTENT)).thenReturn(resource);
            lenient().when(resourceResolver.getResource(node.getPath()
                    + TCBConstants.SLASH + JcrConstants.JCR_CONTENT
                    + TCBConstants.SLASH + "image")).thenReturn(resource);
            lenient().when(resource.adaptTo(StoryListingBean.class)).thenReturn(storyListingBean);
            storyListingBean.setImage("/content/dam/sample.jpg");
            lenient().when(resource.getValueMap()).thenReturn(valueMap);
            lenient().when(valueMap.get("/content/dam/sample.jpg", String.class))
                    .thenReturn("/content/dam/sample.jpg");
            lenient().when(valueMap.get(ARTICLE_CREATION_DATE, String.class))
                    .thenReturn("2022-10-19T23:00:00.000+05:30");
            LocalDateTime dateTime = LocalDateTime.parse("2022-10-19T23:00:00.000+05:30",
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            lenient().when(resource.getPath()).thenReturn("/content/techcombank/en/storylisting");
            lenient().when(page.getLanguage()).thenReturn(Locale.forLanguageTag("en"));
            lenient().when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
            lenient().when(tagManager.resolve(Mockito.any())).thenReturn(tag);
            lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
            lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
            lenient().when(pageManager.getContainingPage(resource)).thenReturn(page);
            lenient().when(page.getLanguage()).thenReturn(Locale.forLanguageTag("en"));
            lenient().when(page.getLanguage(true)).thenReturn(Locale.forLanguageTag("en"));
            json = storyListing.getResult(resourceResolver, queryMap);
        }
        String jsonString = "{\"total\":\"1\",\"results\":[{\"articleCategory\":[null,null],"
                + "\"image\":\"\",\"articleCreationDate\":\"19 Oct 2022\"},{\"articleCategory\":[null,null],"
                + "\"image\":\"\",\"articleCreationDate\":\"19 Oct 2022\"}]}";
        assertEquals(jsonString, json);
    }
}
