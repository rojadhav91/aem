
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.lenient;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.mockito.Mockito;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import java.util.HashMap;

/**
 * The Class MastHeadSpecialBannerModelTest to perform code coverage test for
 * MastHeadSpecialBannerModel Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MastHeadSpecialBannerModelTest {

    private final AemContext ctx = new AemContext();

    private MastHeadSpecialBannerModel specialBannerModel;
    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.SPECIAL_BANNER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(MastHeadSpecialBannerModel.class);
        specialBannerModel = getModel("/content/jcr:content/root/container/mastheadspecialbanne");
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        valueMap.put("jcr:title", "Masthead Special Banner");
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, Mockito.any())).
                thenReturn("/content/techcombank/web/vn/en/homev2");
    }

    private MastHeadSpecialBannerModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        MastHeadSpecialBannerModel specialbanner = res.adaptTo(MastHeadSpecialBannerModel.class);
        return specialbanner;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);

    }

    @Test
    void testBackgroundBannerimage() {
        String expected = "/content/dam/techcombank/images/SpecialBannerBG.jpg";
        assertEquals(expected, specialBannerModel.getBgBannerImage());
    }

    @Test
    void testMobileBackgroundBannerimage() {
        String expected = "/content/dam/techcombank/images/SpecialBannerBGmob.jpg";
        assertEquals(expected, specialBannerModel.getBgBannerMobileImage());
    }

    @Test
    void testBackgroundBannerImageAltText() {
        String expected = "BgBannerLogo";
        assertEquals(expected, specialBannerModel.getBgBannerImgAlt());
    }

    @Test
    void testBannerImage() {
        String expected = "/content/dam/techcombank/images/creditcard.png";
        assertEquals(expected, specialBannerModel.getBannerImage());
    }

    @Test
    void testBannerImageAltText() {
        String expected = "BannerLog";
        assertEquals(expected, specialBannerModel.getBannerImgAlt());
    }

    @Test
    void testHighlitedText() {
        String expected = "CREDIT CARD";
        assertEquals(expected, specialBannerModel.getHighlightedtext());
    }

    @Test
    void testBannerTitle() {
        String expected = "Vietnam Airlines Techcombank Visa Classic";
        assertEquals(expected, specialBannerModel.getBannerTitle());
    }

    @Test
    void testBannerSubTitle() {
        String expected = "Live every moment to the fullest";
        assertEquals(expected, specialBannerModel.getBannerSubTitle());
    }

    @Test
    void testBannerDescription() {
        String expected = "Who can register People who have income from 6,000,000 VND/month or more";
        assertEquals(expected, specialBannerModel.getBannerDesc());
    }

    @Test
    void testButtonLabel() {
        String expected = "Register Now";
        assertEquals(expected, specialBannerModel.getButtonLabel());
    }

    @Test
    void testButtonlinkURL() {
        String expected = "/content/techcombank/web/vn/en/homev2";
        assertEquals(expected, specialBannerModel.getButtonlinkURL());
    }

    @Test
    void testButtonTarget() {
        String expected = "_blank";
        assertEquals(expected, specialBannerModel.getButtonTarget());
    }

    @Test
    void testButtonNoFollow() {
        assertTrue(specialBannerModel.isButtonNoFollow());
    }

    @Test
    void testHideButtonArrow() {
        assertTrue(specialBannerModel.isHideButtonArrow());
    }

    @Test
    void testRegisterContactCTA() {
        assertTrue(specialBannerModel.isRegisterContactCta());
    }

    @Test
    void testWebInteractionValue() {
        assertEquals("{'webInteractions': {'name': 'Masthead Special Banner','type': 'other'}}",
                specialBannerModel.getWebInteractionValue());
    }

    @Test
    void testRegisterContactClickInfoValue() {
        assertEquals("{'cta' : 'Register Now'}",
                specialBannerModel.getRegisterContactClickInfoValue());
    }

    @Test
    void testDefaultClickInfoValue() {
        assertEquals("{'linkClick' : 'Register Now'}",
                specialBannerModel.getDefaultClickInfoValue());
    }

}
