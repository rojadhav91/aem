package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListRowModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListRowModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListRowModel.class);
        aemContext.load().json(TestConstants.LISTROW, "/listRow");
        aemContext.currentResource(TestConstants.LISTROWCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListRowModel.class);
    }

    @Test
    void getTitleText() {
        final String expected = "Title";
        assertEquals(expected, model.getTitleText());
    }

    @Test
    void getRowNumber() {
        final String expected = "3";
        assertEquals(expected, model.getRowNumber());
    }

    @Test
    void getColumnNumber() {
        final String expected = "1";
        assertEquals(expected, model.getColumnNumber());
    }

    @Test
    void getListRowItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getListRowItems().size());
    }

}
