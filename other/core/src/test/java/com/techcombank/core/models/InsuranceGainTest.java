package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.InsuranceGainService;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class InsuranceGainTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    @Mock
    private InsuranceGainService insuranceGainService;

    private InsuranceGain insuranceGain;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private Resource resource;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        mockObjects();
        aemContext.addModelsForClasses(InsuranceGain.class);
        aemContext.load().json(TestConstants.INSURANCE_GAIN, "/insurance-gain");
        aemContext.currentResource(TestConstants.INSURANCE_GAIN_COMP);
        Resource myResource = aemContext.resourceResolver().getResource(TestConstants.INSURANCE_GAIN_COMP);
        insuranceGain = myResource.adaptTo(InsuranceGain.class);
        PrivateAccessor.setField(insuranceGain, "insuranceGainService", insuranceGainService);
    }
    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        aemContext.registerService(ResourceResolver.class, resourceResolver);
        resource = mock(Resource.class);
        aemContext.registerService(Resource.class, resource);
        resourceResolverService = mock(ResourceResolverService.class);
        aemContext.registerService(ResourceResolverService.class, resourceResolverService);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }



    @Test
    void getBackground() {
        final String expected = "/content/dam/techcombank/compare_saving_bg3_541ff3a195.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, insuranceGain.getBackground());
    }

    @Test
    void getBackgroundMobile() {
        final String expected = "/content/dam/techcombank/compare_saving_bg3_541ff3a195.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, insuranceGain.getBackgroundMobile());
    }

    @Test
    void getIcon() {
        final String expected = "/content/dam/techcombank/money_0f9fe50e94.svg";
        assertEquals(expected, insuranceGain.getIcon());
    }

    @Test
    void getIconMobile() {
        final String expected = "/content/dam/techcombank/money_0f9fe50e94.svg";
        assertEquals(expected, insuranceGain.getIconMobile());
    }

    @Test
    void getToolTipIcon() {
        final String expected = "/content/dam/techcombank/icon_8c14fabb1a.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, insuranceGain.getToolTipIcon());
    }

    @Test
    void getToolTipIconMobile() {
        final String expected = "/content/dam/techcombank/icon_8c14fabb1a.png/"
                + "jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, insuranceGain.getToolTipIconMobile());
    }



    @Test
    void getDob() {
        final String expected = "Date of birth";
        assertEquals(expected, insuranceGain.getDob());
    }


    @Test
    void getGenderLabel() {
        final String expected = "Gender";
        assertEquals(expected, insuranceGain.getGenderLabel());
    }

    @Test
    void getMale() {
        final String expected = "Male";
        assertEquals(expected, insuranceGain.getMale());
    }

    @Test
    void getFemale() {
        final String expected = "Female";
        assertEquals(expected, insuranceGain.getFemale());
    }

    @Test
    void getAmount() {
        final String expected = "Amount you want to be insured for";
        assertEquals(expected, insuranceGain.getAmount());
    }

    @Test
    void getAmountInput() {
        final String expected = "VND";
        assertEquals(expected, insuranceGain.getAmountInput());
    }

    @Test
    void getAlt() {
        final String expected = "test";
        assertEquals(expected, insuranceGain.getAlt());
    }

    @Test
    void getAltIcon() {
        final String expected = "test";
        assertEquals(expected, insuranceGain.getAltIcon());
    }

    @Test
    void getInterest() {
        final String expected = "Amount of annual premium payment";
        assertEquals(expected, insuranceGain.getInterest());
    }

    @Test
    void getCta() {
        final String expected = "true";
        assertEquals(expected, insuranceGain.getCta());
    }

    @Test
    void getButton() {
        final String expected = "Book for consultation";
        assertEquals(expected, insuranceGain.getButton());
    }

    @Test
    void getButtonImage() {
        final String expected = "/content/dam/techcombank/in_339d409eb6.svg";
        assertEquals(expected, insuranceGain.getButtonImage());
    }

    @Test
    void getButtonImageMobile() {
        final String expected = "/content/dam/techcombank/in_339d409eb6.svg";
        assertEquals(expected, insuranceGain.getButtonImageMobile());
    }

    @Test
    void getAltButton() {
        final String expected = "alte text for icon";
        assertEquals(expected, insuranceGain.getAltButton());
    }



    @Test
    void getOpenInNewTab() {
        final String expected = "true";
        assertEquals(expected, insuranceGain.getOpenInNewTab());
    }

    @Test
    void getNofollow() {
        final String expected = "true";
        assertEquals(expected, insuranceGain.getNofollow());
    }

    @Test
    void getErrorDate() {
        final String expected = "invalidate date";
        assertEquals(expected, insuranceGain.getErrorDate());
    }

    @Test
    void getErrorAge() {
        final String expected = "invalidate age";
        assertEquals(expected, insuranceGain.getErrorAge());
    }

    @Test
    void getType() {
        final String expected = "superLink";
        assertEquals(expected, insuranceGain.getType());
    }

    @Test
    void getToolTipAltText() {
        final String expected = "tool tip text";
        assertEquals(expected, insuranceGain.getToolTipAltText());
    }

    @Test
    void getCurrencyText() {
        final String expected = "VND";
        assertEquals(expected, insuranceGain.getCurrencyText());
    }

    @Test
    void getLinkandList() {
        assertNull(insuranceGain.getGenericList());
        assertNull(insuranceGain.getLink());
        assertEquals("other", insuranceGain.getInterationType());

    }
}
