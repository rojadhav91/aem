package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The Class EnhancedFAQPanelModelTest to perform junit for EnhancedFAQPanelModel
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class EnhancedFAQPanelModelTest {

    private final AemContext ctx = new AemContext();

    private EnhancedFAQPanelModel enhancedFAQPanel;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.ENHANCED_FAQ_PANEL_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(EnhancedFAQPanelModel.class);
        enhancedFAQPanel = getModel("/content/jcr:content/root/container/enhancedfaqpanel");
    }

    private EnhancedFAQPanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        EnhancedFAQPanelModel faqPanel = res.adaptTo(EnhancedFAQPanelModel.class);
        return faqPanel;
    }

    @Test
    void testEnhancedFAQPanelFields() {
        assertEquals("View More", enhancedFAQPanel.getViewMoreLabel());
    }

}
