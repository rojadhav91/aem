package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The Class SectionContainerModelTest.
 */
@ExtendWith(AemContextExtension.class)
class SectionContainerModelTest {

    /**
     * The resource.
     */
    @Mock
    private Resource resource;

    /**
     * The sling model.
     */
    @InjectMocks
    private SectionContainerModel slingModel;

    /**
     * The context.
     */
    private final AemContext context = AppAemContext.newAemContext();

    /**
     * The page.
     */
    private Page page;

    private static final String LLSS_CONSTANT = "ll-ll:sl-sl";

    private static final String LLSS_BGCOLOR_CONSTANT = "ll-bgColor:sl-sl";

    private static final String LLSSLL_BGCOLOR_CONSTANT = "ll-ll:sl-ll";

    /**
     * Sets the up.
     */
    @BeforeEach
    void setUp() {
        page = context.create().page(TestConstants.MYPAGE);
        resource = context.create().resource(page, TestConstants.SCTION_CONTAINER,
                TestConstants.SLING_RESOURCETYPE, TestConstants.SECTION_CONTAINER_RESOURCE);

        slingModel = resource.adaptTo(SectionContainerModel.class);

        slingModel.setBottomPadding("20");
        slingModel.setLargeBGImageReference(TestConstants.ASSECT);
        slingModel.setSmallBGImageReference(TestConstants.ASSECT);
        slingModel.setTopPadding("20");
        slingModel.setBackgroundColor("Blue");
        slingModel.setTitleText("Section Container");
        slingModel.setTextColor("Yellow");
        slingModel.setLeftPadding("0");
        slingModel.setRightPadding("0");
    }

    /**
     * Test sling model.
     */
    @Test
    void testSlingModel() {
        /* Test the bottom padding */
        String bottomPadding = slingModel.getBottomPadding();
        assertEquals("20", bottomPadding);

        /* Test the desktop image */
        String largeBGImage = slingModel.getLargeBGImageReference();
        assertEquals(TestConstants.ASSECT, largeBGImage);
        assertEquals(TestConstants.ASSECT + TestConstants.JPEG_WEB_IMAGE_RENDITION_PATH,
                slingModel.getWebImagePath());

        /* Test the mobile image */
        String smallBGImage = slingModel.getSmallBGImageReference();
        assertEquals(TestConstants.ASSECT, smallBGImage);
        assertEquals(TestConstants.ASSECT + TestConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH,
                slingModel.getMobileImagePath());

        /* Test the Top padding */
        String topPadding = slingModel.getTopPadding();
        assertEquals("20", topPadding);

        /* Test the left padding */
        String leftPadding = slingModel.getLeftPadding();
        assertEquals("0", leftPadding);

        /* Test the right padding */
        String rightPadding = slingModel.getRightPadding();
        assertEquals("0", rightPadding);

        String bgColor = slingModel.getBackgroundColor();
        assertEquals("Blue", bgColor);

        slingModel.setContainerAction(LLSS_CONSTANT);
        assertEquals(LLSS_CONSTANT, slingModel.getContainerAction());

        slingModel.setContainerAction(LLSS_BGCOLOR_CONSTANT);
        assertEquals(LLSS_BGCOLOR_CONSTANT, slingModel.getContainerAction());

        slingModel.setContainerAction("bgColor");
        assertEquals("bgColor", slingModel.getContainerAction());

        slingModel.setContainerAction(LLSSLL_BGCOLOR_CONSTANT);
        assertEquals(LLSSLL_BGCOLOR_CONSTANT, slingModel.getContainerAction());

        slingModel.setContainerAction(StringUtils.EMPTY);
        assertEquals(StringUtils.EMPTY, slingModel.getContainerAction());

        String textColor = slingModel.getTextColor();
        assertEquals("Yellow", textColor);

        String titleText = slingModel.getTitleText();
        assertEquals("Section Container", titleText);

        slingModel.setBackgroundImageRounded(true);
        assertTrue(slingModel.isBackgroundImageRounded());
    }
}
