package com.techcombank.core.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import com.techcombank.core.service.impl.StockChartServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StockChartServiceTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private StockChartServiceImpl stockChartService = new StockChartServiceImpl();

    @Mock
    private HttpClientExecutorService httpExecutor;

    @Mock
    private JwtTokenService jwtTokenService;

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.registerService(HttpClientExecutorService.class, httpExecutor);
        ctx.registerService(JwtTokenService.class, jwtTokenService);
        ctx.registerInjectActivateService(stockChartService);
        ctx.addModelsForClasses(StockChartServiceImpl.class);
    }

    @Test
    void testBranchList() throws UnsupportedEncodingException {

        JsonArray stockChartDataArray = new JsonArray();
        JsonObject stockChartData = new JsonObject();
        stockChartData.addProperty("open", "33550.00");
        stockChartData.addProperty("high", "33900.00");
        stockChartDataArray.add(stockChartData);
        JsonObject stockChart = new JsonObject();
        stockChart.add("data", stockChartDataArray);
        lenient().when(httpExecutor.executeHttpGet(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(stockChart);
        JsonObject responseData = stockChartService.fetchStockChartData(false);
        JsonObject stockChartArray = responseData.get("data").getAsJsonArray().get(0).getAsJsonObject();
        assertEquals("33550.00", stockChartArray.get("open").getAsString());
    }
}
