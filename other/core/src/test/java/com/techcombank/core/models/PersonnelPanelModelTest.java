package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class PersonnelPanelModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private PersonnelPanelModel model;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.PERSONNELPANEL, "/personnelPanel");
        ctx.addModelsForClasses(PersonnelPanelModel.class);
        model = getModel(TestConstants.PERSONNELPANELCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private PersonnelPanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(PersonnelPanelModel.class);
    }

    private void mockObjects() {

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);
    }

    @Test
    void getPersonnelDescription() {
        final String expected = "a";
        assertEquals(expected, model.getPersonnelDescription());
    }

    @Test
    void getPersonnelName() {
        final String expected = "a";
        assertEquals(expected, model.getPersonnelName());
    }

    @Test
    void getPersonnelPosition() {
        final String expected = "a";
        assertEquals(expected, model.getPersonnelPosition());
    }

    @Test
    void getPanelButtonText() {
        final String expected = "a";
        assertEquals(expected, model.getPanelButtonText());
    }

    @Test
    void getPanelButtonLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getPanelButtonLinkUrl());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = false;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void getRightPanelBackgroundColor() {
        final String expected = "a";
        assertEquals(expected, model.getRightPanelBackgroundColor());
    }

    @Test
    void getPersonnelImage() {
        final String expected = "a";
        assertEquals(expected, model.getPersonnelImage());
    }

    @Test
    void getImageAltText() {
        final String expected = "a";
        assertEquals(expected, model.getImageAltText());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit",
            model.getWebInteractionType());
    }

}
