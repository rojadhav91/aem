package com.techcombank.core.pojo;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.StoryListingModel;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StoryListingBeanTest {

    private final AemContext aemContext = TCBAemContext.newAemContext();

    private StoryListingBean storyListingBean;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(StoryListingModel.class);
        aemContext.load().json(TestConstants.STORYLISTING, TestConstants.STORYLISTINGCOMP);
        aemContext.currentResource(TestConstants.STORYLISTINGCOMP + "/storylistingBean");
        storyListingBean = aemContext.currentResource().adaptTo(StoryListingBean.class);
    }

    @Test
    void getArticleCreationDate() {
        assertEquals("2023-01-01T00:00:00Z", storyListingBean.getArticleCreationDate());
    }

    @Test
    void getTitle() {
        StoryListingBean bean = new StoryListingBean();
        bean.setTitle("Article one");
        assertEquals("Article one", bean.getTitle());
    }

    @Test
    void getDescription() {
        StoryListingBean bean = new StoryListingBean();
        bean.setDescription("This is article one");
        assertEquals("This is article one", bean.getDescription());
    }

    @Test
    void getImage() {
        StoryListingBean bean = new StoryListingBean();
        bean.setImage("/path/to/image.jpg");
        assertEquals("/path/to/image.jpg", bean.getImage());
    }

    @Test
    void getPath() {
        StoryListingBean bean = new StoryListingBean();
        bean.setPath("/content/techcombank/articleone.html");
        assertEquals("/content/techcombank/articleone.html", bean.getPath());
    }

    @Test
    void getArticleCategory() {
        StoryListingBean bean = new StoryListingBean();
        List<String> articleList = new ArrayList<>();
        articleList.add("Tips & Tricks");
        articleList.add("Updates");
        articleList.add("Fraud warning");
        storyListingBean.setArticleCategory(articleList);
        assertEquals("Tips & Tricks", storyListingBean.getArticleCategory().get(0));
    }
}
