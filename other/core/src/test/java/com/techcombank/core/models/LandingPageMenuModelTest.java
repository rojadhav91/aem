
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.day.cq.wcm.api.WCMMode;
import com.techcombank.core.constants.TestConstants;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class LandingPageMenuTest to perform junit for LandingPageMenu Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class LandingPageMenuModelTest {

    private final AemContext ctx = new AemContext();

    private LandingPageMenuModel landingPageMenu;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.HEADER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(LandingPageMenuModel.class);
        landingPageMenu = getModel(
                "/content/techcombank/web/vn/vi/personal/jcr:content/root/container/header/landingPageMenu/item0");
    }

    private LandingPageMenuModel getModel(final String currentResource) {
        ctx.request().setAttribute(WCMMode.class.getName(), WCMMode.EDIT);
        ctx.currentResource(currentResource);
        LandingPageMenuModel landingPage = ctx.currentResource().adaptTo(LandingPageMenuModel.class);
        return landingPage;
    }

    @Test
    void testLandingPageURL() {
        String expected = "/content/techcombank/web/vn/vi.html";
        assertEquals(expected, landingPageMenu.getLandingPageURL());
    }

    @Test
    void testLandingMenuTitle() {
        String expected = "Personal";
        assertEquals(expected, landingPageMenu.getLandingMenuTitle());
    }

    @Test
    void testLandingTarget() {
        String expected = "_self";
        assertEquals(expected, landingPageMenu.getLandingTarget());
    }

}
