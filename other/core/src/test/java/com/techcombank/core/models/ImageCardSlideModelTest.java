package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ImageCardSlideModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ImageCardSlideModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ImageCardSlideModel.class);
        aemContext.load().json(TestConstants.IMAGECARDSLIDE, "/imagecardslide");
        aemContext.currentResource(TestConstants.IMAGECARDSLIDECOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ImageCardSlideModel.class);
    }

    @Test
    void getCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCardImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getCardImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCardImagePath());
        assertEquals(model.getCardImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCardImagePath());
    }

    @Test
    void getCardImageAltText() {
        final String expected = "Simplify your banking experience";
        assertEquals(expected, model.getCardImageAltText());
    }

    @Test
    void getCardTitle() {
        final String expected = "Simplify your banking experience";
        assertEquals(expected, model.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "Banking transactions become simpler for all users";
        assertEquals(expected, model.getCardDescription());
    }

    @Test
    void getCardBackgroundColour() {
        final String expected = "#FFFFFF";
        assertEquals(expected, model.getCardBackgroundColour());
    }

    @Test
    void getTextAlignmentOnCard() {
        final String expected = "left";
        assertEquals(expected, model.getTextAlignmentOnCard());
    }

}
