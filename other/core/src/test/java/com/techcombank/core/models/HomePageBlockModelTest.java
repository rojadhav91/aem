package com.techcombank.core.models;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class HomePageBlockModelTest {
    private final AemContext aemContext = TCBAemContext.newAemContext();
    private HomePageBlockModel homePageBlockModel;
    private List<HomePageContact> homePageContacts;
    private List<HomePageBlockNavigation> homePageBlockNavigations;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        aemContext.addModelsForClasses(HomePageBlockModel.class);
        aemContext.load().json(TestConstants.HOMEPAGEBLOCK, "/homepageBlock");
        aemContext.currentResource(TestConstants.HOMEPAGEBLOCKCOMP);
        resource = aemContext.request().getResource();
        homePageBlockModel = resource.adaptTo(HomePageBlockModel.class);
        if (null != homePageBlockModel.getContact()) {
            homePageContacts = homePageBlockModel.getContact().size() != 0
                    ? homePageBlockModel.getContact() : Collections.EMPTY_LIST;
        }
        homePageBlockNavigations = homePageBlockModel.getNavigation();
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        aemContext.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        aemContext.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        aemContext.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);

        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        valueMap.put(TCBConstants.JCR_TITLE, "HomePage");
        lenient().when(resource.getValueMap()).thenReturn(valueMap);

    }

    @Test
    void getTitle() {
        final String expected = "Welcome to Techcombank";
        assertEquals(expected, homePageBlockModel.getTitle());
    }


    @Test
    void getLabel() {
        final String expected = "Contact Us";
        assertEquals(expected, homePageBlockModel.getLabel());
    }

    @Test
    void getLinkUrl() {
        final String expected = TestConstants.COMMONURL;
        assertEquals(expected, homePageBlockModel.getLinkUrl());
    }


    @Test
    void getButtonText() {
        final String expected = "Exchange Rates";
        assertEquals(expected, homePageBlockModel.getButtonText());
    }

    @Test
    void getButtonUrl() {
        final String expected = TestConstants.COMMONURL;
        assertEquals(expected, homePageBlockModel.getButtonUrl());
        assertEquals("{'webInteractions': {'name': 'HomePage','type': 'other'}}",
                homePageBlockModel.getButtonInteraction());
    }

    @Test
    void getOpenNewTab() {
        final String expected = "true";
        assertEquals(expected, homePageBlockModel.getOpenInNewTab());
    }

    @Test
    void getOpenNewTabContact() {
        final String expected = "true";
        assertEquals(expected, homePageBlockModel.getOpenInNewTabContact());
    }

    @Test
    void getNoFollowContact() {
        final String expected = "true";
        assertEquals(expected, homePageBlockModel.getNofollowContact());
    }

    @Test
    void getNoFollow() {
        final String expected = "true";
        assertEquals(expected, homePageBlockModel.getNofollow());
    }

    @Test
    void getNavigation() {
        ValueMap valueMapValue = mock(ValueMap.class);
        String baseImagePath = "/content/dam/techcombank/fb_87a424484b.svg";
        assertEquals(2, homePageBlockNavigations.size());
        assertEquals(TestConstants.COMMONURL, homePageBlockNavigations.get(0).getHomeLink());
        assertEquals("{'webInteractions': {'name': 'HomePage','type': 'other'}}",
                homePageBlockNavigations.get(0).getInteraction());
        assertEquals(baseImagePath, homePageBlockNavigations.get(0).getIcon());
        assertEquals("personal Banking", homePageBlockNavigations.get(0).getIconText());
        assertEquals("Personal Banking", homePageBlockNavigations.get(0).getText());
        assertEquals("true", homePageBlockNavigations.get(0).getOpenInNewTabNavigation());
        assertEquals("true", homePageBlockNavigations.get(0).getNofollow());
        assertEquals(baseImagePath, homePageBlockNavigations.get(0).getIconWebImagePath());
        assertEquals(baseImagePath, homePageBlockNavigations.get(0).getIconMobileImagePath());
        lenient().when(resourceResolver.getResource("techcombank/components/homepageblock")).thenReturn(resource);
        lenient().when(valueMapValue.containsKey(Mockito.eq(TCBConstants.JCR_TITLE))).thenReturn(true);
        lenient().when(valueMapValue.get(Mockito.eq(TCBConstants.JCR_TITLE), Mockito.eq(String.class)))
                .thenReturn("jcrTitle");
        assertEquals("{'webInteractions': {'name': 'homepageblock','type': 'exit'}}", homePageBlockNavigations.get(0)
                .getWebInteractionValue("personal Banking"));
    }

    @Test
    void getContact() {
        assertEquals(2, homePageContacts.size());
        assertEquals("1234567890", homePageContacts.get(0).getNumber());
        assertEquals("Personal Customer", homePageContacts.get(0).getContactText());
    }

    @Test
    void getBgColor() {
        final String expected = "#001F3F";
        assertEquals(expected, homePageBlockModel.getBgColor());
    }

    @Test
    void getImageMobile() {
        final String expected = "/content/dam/techcombank/fb_87a424484b.jpeg";
        assertEquals(expected, homePageBlockModel.getImageMobile());
        assertEquals(expected + TestConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH,
                homePageBlockModel.getImageMobilePath());
    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/fb_87a424484b.jpeg";
        assertEquals(expected, homePageBlockModel.getImage());
        assertEquals(expected + TestConstants.JPEG_WEB_IMAGE_RENDITION_PATH,
                homePageBlockModel.getWebImagePath());
    }

    @Test
    void getLinkInteraction() {
        lenient().when(resourceResolver.getResource("techcombank/components/homepageblock")).thenReturn(resource);
        String linkeType = homePageBlockModel.getWebInteractionValue("/content/techcombank/web/vn/en/homepageblock");
        assertEquals(linkeType, homePageBlockModel.getLinkInteraction());
    }

    @Test
    void getWebInteractionValue() {
        String expectedInteraction = "{'webInteractions': {'name': 'homepageblock','type': 'other'}}";
        lenient().when(resourceResolver.getResource("techcombank/components/homepageblock")).thenReturn(resource);
        assertEquals(expectedInteraction,
        homePageBlockModel.getWebInteractionValue("/content/techcombank/web/kr/en/homepage-block-component"));
    }
}
