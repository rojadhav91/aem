package com.techcombank.core.pojo;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


    @ExtendWith({AemContextExtension.class, MockitoExtension.class})
    class FAQTest {

        @Test
        void testFAQ() {
            FAQ faq = new FAQ();
            faq.setFaqTitle("FAQTitle");
            faq.setFaqDescription("FAQDescription");

            assertEquals("FAQTitle", faq.getFaqTitle());
            assertEquals("FAQDescription", faq.getFaqDescription());
        }
    }
