
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.util.List;
import static org.mockito.Mockito.lenient;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TestConstants;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.mockito.Mockito;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class MastHeadSimpleBannerModelTest to perform code coverage test for
 * MastHeadSimpleBannerModel Model
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class MastHeadSimpleBannerModelTest {

    private final AemContext ctx = new AemContext();

    private MastHeadSimpleBannerModel simpleBannerModel;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private Page currentPage;

    private PageManager pageManager;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.SIMPLE_BANNER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(MastHeadSimpleBannerModel.class);
        simpleBannerModel = getModel(
                "/content/techcombank/web/vn/vi/simplebanner/jcr:content/root/container/mastheadsimplebanner");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, Mockito.any())).
                thenReturn("/content/techcombank/web/vn/vi/personal");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "https://www.google.com")).
                thenReturn("https://www.google.com");
    }

    private MastHeadSimpleBannerModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        MastHeadSimpleBannerModel simplebanner = res.adaptTo(MastHeadSimpleBannerModel.class);
        return simplebanner;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        currentPage = mock(Page.class);
        ctx.registerService(Page.class, currentPage);

        pageManager = mock(PageManager.class);
        ctx.registerService(PageManager.class, pageManager);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(currentPage.getPath()).thenReturn("/content/techcombank/web/vn/vi/simplebanner");

    }

    @Test
    void testBackgroundBannerimage() {
        String expected = "/content/dam/techcombank/images/Frame.svg";
        assertEquals(expected, simpleBannerModel.getBgBannerImage());
    }

    @Test
    void testMobileBackgroundBannerimage() {
        String expected = "/content/dam/techcombank/images/Framemob.svg";
        assertEquals(expected, simpleBannerModel.getBgBannerMobileImage());
    }

    @Test
    void testBackgroundBannerimageAltText() {
        String expected = "BannerLogo";
        assertEquals(expected, simpleBannerModel.getBgBannerImgAlt());
    }

    @Test
    void testHighlitedText() {
        String expected = "Comprehensive payment";
        assertEquals(expected, simpleBannerModel.getHighlightedtext());
    }

    @Test
    void testBannerTitle() {
        String expected = "Comprehensive payment solution";
        assertEquals(expected, simpleBannerModel.getBannerTitle());
    }

    @Test
    void testBannerDescription() {
        String expected = "Comprehensive payment solution helps you enjoy life to the fullest";
        assertEquals(expected, simpleBannerModel.getBannerDesc());
    }

    @Test
    void testWrapBannerTitle() {
        assertTrue(simpleBannerModel.isWrapBannerTitle());
    }

    @Test
    void testButtonLabel() {
        String expected = "Open an account now";
        assertEquals(expected, simpleBannerModel.getButtonLabel());
    }

    @Test
    void testButtonlinkURL() {
        String expected = "/content/techcombank/web/vn/vi/personal";
        assertEquals(expected, simpleBannerModel.getButtonlinkURL());
    }

    @Test
    void testButtonTarget() {
        String expected = "_blank";
        assertEquals(expected, simpleBannerModel.getButtonTarget());
    }

    @Test
    void testButtonNoFollow() {
        assertTrue(simpleBannerModel.isButtonNoFollow());
    }

    @Test
    void testDisplaySearchBox() {
        assertTrue(simpleBannerModel.isDisplaySearchBox());
    }

    @Test
    void testCurrentActivePage() {
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        String expected = "/content/techcombank/web/vn/vi/personal";
        assertEquals(expected, simpleBannerModel.getCurrentActivePage());
    }

    @Test
    void bannerTypeTest() {
        String expected = "large";
        assertEquals(expected, simpleBannerModel.getBannerType());
    }

    @Test
    void textAlignmentTest() {
        final String expected = "top";
        assertEquals(expected, simpleBannerModel.getTextAlignment());
    }

    @Test
    void testTabSection() {
        List<SimpleBannerTabSectionModel> tabSection = simpleBannerModel.getTabSection();
        assertEquals("/content/dam/techcombank/images/Western_Union_tab.svg", tabSection.get(0).getTabIcon());
        assertEquals("OverviewTabLogo", tabSection.get(0).getTabIconAlt());
        assertEquals("Overview", tabSection.get(0).getTabName());
        assertEquals("/content/techcombank/web/vn/vi/personal", tabSection.get(0).getTabLinkURL());
        assertEquals("_blank", tabSection.get(0).getTabTarget());
        assertTrue(tabSection.get(0).isTabNoFollow());
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(null);
        assertEquals("https://www.google.com", tabSection.get(1).getTabLinkURL());
    }
}
