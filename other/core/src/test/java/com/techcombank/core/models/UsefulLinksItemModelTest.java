package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class UsefulLinksItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private UsefulLinksItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(UsefulLinksItemModel.class);
        aemContext.load().json(TestConstants.USEFULLINKSITEM, "/usefulLinksItem");
        aemContext.currentResource(TestConstants.USEFULLINKSITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(UsefulLinksItemModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getBanner() {
        final String expected = "path";
        assertEquals(expected, model.getBanner());
    }

    @Test
    void getSlug() {
        final String expected = "path";
        assertEquals(expected, model.getSlug());
    }

}
