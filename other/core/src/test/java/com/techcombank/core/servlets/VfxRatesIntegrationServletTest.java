package com.techcombank.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletRequest;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.gson.JsonObject;
import com.techcombank.core.service.VfxRatesJobService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class VfxRatesIntegrationServletTest {
    @InjectMocks
    private VfxRatesIntegrationServlet vfxRatesIntegrationServlet = new VfxRatesIntegrationServlet();

    @Mock
    private SlingHttpServletRequest req;

    @Mock
    private SlingHttpServletResponse res;

    @Mock
    private PrintWriter out;

    @Mock
    private VfxRatesJobService vfxRatesJobService;

    private final AemContext ctx = new AemContext();

    private static final int STATUS = 200;

    @BeforeEach
    void setUp() throws Exception {
        ctx.addModelsForClasses(VfxRatesIntegrationServlet.class);
    }

    @Test
    void testPostMethod(final AemContext context) throws ServletException, IOException {

        MockSlingHttpServletRequest request = ctx.request();
        MockSlingHttpServletResponse response = ctx.response();
        JsonObject dateCreated = new JsonObject();
        JsonObject vfxData = new JsonObject();
        dateCreated.addProperty("input_date", "2021-07-04T21:00:0");
        vfxData.add("data", dateCreated);
        request.setContent(vfxData.toString().getBytes(StandardCharsets.UTF_8));
        lenient().when(req.getInputStream()).thenReturn(request.getInputStream());
        lenient().when(res.getWriter()).thenReturn(out);
        vfxRatesIntegrationServlet.doPost(req, response);
        assertEquals(STATUS, response.getStatus());
        vfxData.remove("data");
        request.setContent(vfxData.toString().getBytes(StandardCharsets.UTF_8));
        lenient().when(req.getInputStream()).thenReturn(request.getInputStream());
        vfxRatesIntegrationServlet.doPost(req, response);
        assertEquals(STATUS, response.getStatus());
    }
}
