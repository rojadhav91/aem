package com.techcombank.core.utils;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import static junitx.framework.ComparableAssert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleUtilsTest {

    private AemContext aemContext = TCBAemContext.newAemContext();

    @Mock
    private SearchResult searchResult;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Hit hit1;

    @Mock
    private Hit hit2;

    @Mock
    private Hit hit3;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @Mock
    private GregorianCalendar date;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetArticles() throws Exception {
        lenient().when(searchResult.getHits()).thenReturn(Arrays.asList(hit1, hit2));
        lenient().when(resourceResolver.getResource(any())).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(valueMap.get("articleImagePath", StringUtils.EMPTY)).thenReturn("");
        lenient().when(valueMap.get("articleImageAltText", StringUtils.EMPTY)).thenReturn("altText");
        lenient().when(valueMap.get("articleCreationDate")).thenReturn(date);

        String currentPagePath = "/content/sample/current-page/jcr:content";

        List<Articles> result = ArticleUtils.getArticles(searchResult, currentPagePath, resourceResolver, "someValue");

        lenient().when(searchResult.getHits()).thenReturn(Arrays.asList(hit1, hit2, hit3));
        List<Articles> result2 =
                ArticleUtils.getArticles(searchResult, currentPagePath, resourceResolver, "relatedPost");

        assertEquals(TestConstants.LIST_SIZE_TWO, result.size());
        assertEquals(TestConstants.LIST_SIZE_THREE, result2.size());

    }
}
