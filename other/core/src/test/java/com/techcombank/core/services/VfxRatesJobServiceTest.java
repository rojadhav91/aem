package com.techcombank.core.services;

import com.google.gson.JsonObject;
import com.techcombank.core.service.impl.VfxRatesJobServiceImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class VfxRatesJobServiceTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private VfxRatesJobServiceImpl vfxRatesJobServiceImpl = new VfxRatesJobServiceImpl();

    @Mock
    private JobManager jobManager;

    @Mock
    private Job job;

    @Mock
    private Calendar calender;

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.addModelsForClasses(VfxRatesJobServiceImpl.class);
    }

    @Test
    void testCreateVfxIntegrationJob() throws UnsupportedEncodingException {
        JsonObject vfxData = new JsonObject();
        JsonObject dateCreated = new JsonObject();
        dateCreated.addProperty("input_date", "2021-07-04T21:00:0");
        vfxData.add("data", dateCreated);
        lenient().when(jobManager.addJob(Mockito.any(), Mockito.any())).thenReturn(job);
        lenient().when(job.getTopic()).thenReturn("VFXIntegration");
        lenient().when(job.getCreated()).thenReturn(calender);
        vfxRatesJobServiceImpl.createVfxIntegrationJob(vfxData.toString());
        VfxRatesJobServiceImpl mockInstance = mock(VfxRatesJobServiceImpl.class);
        mockInstance.createVfxIntegrationJob(vfxData.toString());
        Mockito.verify(mockInstance).createVfxIntegrationJob(vfxData.toString());
    }
}
