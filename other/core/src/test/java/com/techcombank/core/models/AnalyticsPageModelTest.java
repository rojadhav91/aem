package com.techcombank.core.models;

import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class AnalyticsPageModelTest {

    private AemContext context = TCBAemContext.newAemContext();

    @BeforeEach
    public void setup() {
        context.load().json(TestConstants.JSON.PERSONAL_BANKING_PAGE,
                TestConstants.CONTENT.PERSONAL_BANKING_PAGE);
        context.currentResource(TestConstants.CONTENT.PERSONAL_BANKING_PAGE
                + TestConstants.SLASH + TestConstants.JCR_CONTENT);

    }

    @Test
    void testModelVariables() {
        AnalyticsPageModel model = context.request().adaptTo(AnalyticsPageModel.class);
        assertEquals("Business Unit", model.getAnalyticsObj().getBusinessUnit());
        assertEquals("vn", model.getAnalyticsObj().getCountry());
        assertEquals("env", model.getAnalyticsObj().getEnvironment());
        assertEquals("Journey", model.getAnalyticsObj().getJourney());
        assertEquals("Plat", model.getAnalyticsObj().getPlatform());
        assertEquals("PN 1", model.getAnalyticsObj().getProductName());
        assertEquals("SS 1", model.getAnalyticsObj().getSiteSection());
    }

}
