package com.techcombank.core.services;

import com.adobe.acs.commons.genericlists.GenericList;
import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.ExchangeRate;
import com.techcombank.core.models.FixingRate;
import com.techcombank.core.models.GoldRate;
import com.techcombank.core.models.OtherRate;
import com.techcombank.core.models.TenorIntRate;
import com.techcombank.core.models.TenorRate;
import com.techcombank.core.service.impl.VfxRatesDataServiceImpl;
import com.techcombank.core.utils.PlatformUtils;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.jackrabbit.vault.packaging.JcrPackageManager;
import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.apache.jackrabbit.vault.packaging.JcrPackageDefinition;
import org.apache.jackrabbit.vault.packaging.Packaging;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class VfxRatesDataServiceTest {

    @InjectMocks
    private VfxRatesDataServiceImpl vfxRatesDataService = new VfxRatesDataServiceImpl();

    private final AemContext ctx = new AemContext();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PageManager pageManager;

    @Mock
    private Replicator replicator;

    @Mock
    private Packaging packaging;

    @Mock
    private Page page;

    @Mock
    private GenericList genericList;

    @Mock
    private FragmentTemplate exchangeRatefragmentTemplate;

    @Mock
    private FragmentTemplate tenorRatefragmentTemplate;

    @Mock
    private FragmentTemplate otherRatefragmentTemplate;

    @Mock
    private FragmentTemplate goldRatefragmentTemplate;

    @Mock
    private FragmentTemplate tenorIntRatefragmentTemplate;

    @Mock
    private Resource exchangeRateResource;

    @Mock
    private Resource goldRateResource;

    @Mock
    private Resource otherRateResource;

    @Mock
    private Resource tenorRateResource;

    @Mock
    private Resource tenorIntRateResource;

    @Mock
    private Resource resource;

    @Mock
    private ContentFragment exchangeRateFragment;

    @Mock
    private ContentFragment goldRateFragment;

    @Mock
    private ContentFragment otherRateFragment;

    @Mock
    private ContentFragment tenorRateFragment;

    @Mock
    private ContentFragment tenorIntRateFragment;

    @Mock
    private ContentElement ce1;

    @Mock
    private ContentElement ce2;

    @Mock
    private ContentElement ce3;

    @Mock
    private ContentElement ce4;

    @Mock
    private ContentElement ce5;

    @Mock
    private ContentElement ce6;

    @Mock
    private ContentElement ce7;

    @Mock
    private ContentElement ce8;

    @Mock
    private ContentElement ce9;

    @Mock
    private ContentElement ce10;

    @Mock
    private ContentElement ce11;

    @Mock
    private ContentElement ce12;

    @Mock
    private ContentElement ce13;

    @Mock
    private ContentElement ce14;

    @Mock
    private ContentElement ce15;

    @Mock
    private ModifiableValueMap modifiableValueMap;

    @Mock
    private Session session;

    @Mock
    private JcrPackageManager jcrPackageManager;

    @Mock
    private Node node;

    @Mock
    private JcrPackage jcrPackage;

    @Mock
    private JcrPackageDefinition jcrPackageDefinition;

    private JsonObject vfxRateDataJson;

    @BeforeEach
    public void setup() throws IOException {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CURRENCY_CONVERTOR_SERVICE_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(VfxRatesDataServiceImpl.class);
        ctx.registerService(Replicator.class, replicator);
        ctx.registerService(Packaging.class, packaging);
        ctx.registerInjectActivateService(vfxRatesDataService);

        String file = TestConstants.CONTENT_ROOT_SRC_PATH + TestConstants.CONTENT_BASE_PATH
                + TestConstants.VFX_RATES_JSON;
        JsonReader reader = new JsonReader(new FileReader(file));
        Gson gsonObj = new Gson();
        Map<String, String> vfxRatesData = gsonObj.fromJson(reader, Map.class);
        JsonElement vfxRates = gsonObj.toJsonTree(vfxRatesData);
        vfxRateDataJson = vfxRates.getAsJsonObject();
    }

    @Test
    void testExchangeRate() throws IOException {
        Resource baseResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/exchange-rates");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/exchange-rates"))
                .thenReturn(baseResource);
        Resource latestExResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00");
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00"))
                .thenReturn(latestExResource);
        String vfxRateType = TestConstants.EXCHANGE_RATE;
        List<String> exchangeRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, true);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver,
                exchangeRateBasepath.get(0));
        assertEquals("2023-07-11 23:30:00", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource latestExchangeRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                exchangeRateBasepath.get(0));
        assertEquals("23-30-00", latestExchangeRateRes.getName());

        Resource audResource = ctx.resourceResolver().getResource(
                "/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00/aud/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(
                "/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00/aud/jcr:content/data/master"))
                .thenReturn(audResource);
        Resource cadResource = ctx.resourceResolver().getResource(
                "/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00/cad/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(
                "/content/dam/techcombank/master-data/exchange-rates/2023/07/11/23-30-00/cad/jcr:content/data/master"))
                .thenReturn(cadResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11");
        Resource timeResource1115 = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/11-15-00");
        Resource timeResource1230 = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/12-30-00");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11"))
                .thenReturn(timeResource);
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/11-15-00"))
                .thenReturn(timeResource1115);
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/exchange-rates/2023/07/11/12-30-00"))
                .thenReturn(timeResource1230);
        List<String> rateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                exchangeRateBasepath.get(0));
        assertEquals("23:30:00", rateTimeStamp.get(0));
        JsonObject exchangeRateJson = vfxRatesDataService.getVfxRateData(latestExchangeRateRes, resourceResolver,
                rateTimeStamp, vfxRateType);
        JsonArray exRate = exchangeRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = exRate.get(0);
        JsonObject exchangeRate = data.getAsJsonObject();

        assertEquals("AUD", exchangeRate.get("label").getAsString());
        assertEquals("AUD", exchangeRate.get("sourceCurrency").getAsString());
        assertEquals("166280", exchangeRate.get("itemId").getAsString());
        assertEquals("16417", exchangeRate.get("askRate").getAsString());
        assertEquals("15785", exchangeRate.get("bidRateCK").getAsString());
        assertEquals("15521", exchangeRate.get("bidRateTM").getAsString());

        ExchangeRate exchangeRateData = cadResource.adaptTo(ExchangeRate.class);
        assertEquals("166281", exchangeRateData.getItemId());
        assertEquals("CAD", exchangeRateData.getLabel());
        assertEquals("17.703", exchangeRateData.getAskRate());
        assertEquals("17.083", exchangeRateData.getBidRateCK());
        assertEquals("16.813", exchangeRateData.getBidRateTM());
        assertEquals("19.813", exchangeRateData.getAskRateTM());
        assertEquals("CAD", exchangeRateData.getSourceCurrency());
        assertEquals("VND", exchangeRateData.getTargetCurrency());

        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        lenient().when(pageManager.getPage(Mockito.any())).thenReturn(page);
        lenient().when(page.adaptTo(GenericList.class)).thenReturn(genericList);
        lenient().when(genericList.lookupTitle(Mockito.any())).thenReturn("CFlag.svg");
        List<ExchangeRate> exchangeRateList = vfxRatesDataService.getExchangeRateDataList(latestExchangeRateRes,
                resourceResolver);
        assertEquals("2", String.valueOf(exchangeRateList.size()));
        assertEquals("/content/dam/techcombank/flags/CFlag.svg", exchangeRateData.getFlag());
    }

    @Test
    void testGoldRate() throws IOException {
        Resource baseResource = ctx.resourceResolver().getResource("/content/dam/techcombank/master-data/gold-rates");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/gold-rates"))
                .thenReturn(baseResource);
        Resource goldRes = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/gold-rates/2023/08/11/23-16-01");
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/gold-rates/2023/08/11/23-16-01"))
                .thenReturn(goldRes);
        String vfxRateType = TestConstants.GOLD_RATE;
        List<String> goldRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, goldRateBasepath.get(0));
        assertEquals("2023-08-11 23:16:01", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource goldRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                goldRateBasepath.get(0));
        assertEquals("23-16-01", goldRateRes.getName());

        Resource goldResource = ctx.resourceResolver().getResource(
                "/content/dam/techcombank/master-data/gold-rates/2023/08/11/23-16-01/12196/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(
                "/content/dam/techcombank/master-data/gold-rates/2023/08/11/23-16-01/12196/jcr:content/data/master"))
                .thenReturn(goldResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/gold-rates/2023/08/11");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/gold-rates/2023/08/11"))
                .thenReturn(timeResource);
        List<String> vfxRateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                goldRateBasepath.get(0));
        JsonObject goldRateJson = vfxRatesDataService.getVfxRateData(goldRateRes, resourceResolver, vfxRateTimeStamp,
                vfxRateType);
        JsonArray gdRate = goldRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = gdRate.get(0);
        JsonObject goldRate = data.getAsJsonObject();

        assertEquals("SJC 99.99", goldRate.get("label").getAsString());
        assertEquals("12196", goldRate.get("itemId").getAsString());
        assertEquals("6700000", goldRate.get("askRate").getAsString());
        assertEquals("6555000", goldRate.get("bidRate").getAsString());

        GoldRate goldRates = goldResource.adaptTo(GoldRate.class);
        assertEquals("12196", goldRates.getItemId());
        assertEquals("SJC 99.99", goldRates.getLabel());
        assertEquals("6700000", goldRates.getAskRate());
        assertEquals("6555000", goldRates.getBidRate());

    }

    @Test
    void testOtherRate() throws IOException {
        Resource baseResource = ctx.resourceResolver().getResource("/content/dam/techcombank/master-data/other-rates");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/other-rates"))
                .thenReturn(baseResource);
        Resource otherRes = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/other-rates/2023/08/11/23-16-01");
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/other-rates/2023/08/11/23-16-01"))
                .thenReturn(otherRes);
        String vfxRateType = TestConstants.OTHER_RATE;
        List<String> otherRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, otherRateBasepath.get(0));
        assertEquals("2023-08-11 23:16:01", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource otherRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                otherRateBasepath.get(0));
        assertEquals("23-16-01", otherRateRes.getName());

        Resource otherResource = ctx.resourceResolver().getResource(
                "/content/dam/techcombank/master-data/other-rates/2023/08/11/23-16-01/12186/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(
                "/content/dam/techcombank/master-data/other-rates/2023/08/11/23-16-01/12186/jcr:content/data/master"))
                .thenReturn(otherResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/other-rates/2023/08/11");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/other-rates/2023/08/11"))
                .thenReturn(timeResource);
        List<String> vfxRateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                otherRateBasepath.get(0));
        JsonObject otherRateJson = vfxRatesDataService.getVfxRateData(otherRateRes, resourceResolver, vfxRateTimeStamp,
                vfxRateType);
        JsonArray otdata = otherRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = otdata.get(0);
        JsonObject otherRate = data.getAsJsonObject();

        assertEquals("12186", otherRate.get("itemId").getAsString());
        assertEquals("24786", otherRate.get("ceiling").getAsString());
        assertEquals("23606", otherRate.get("central").getAsString());
        assertEquals("22426", otherRate.get("floor").getAsString());

        OtherRate otherRates = otherResource.adaptTo(OtherRate.class);
        assertEquals("12186", otherRates.getItemId());
        assertEquals("24786", otherRates.getCeiling());
        assertEquals("23606", otherRates.getCentral());
        assertEquals("22426", otherRates.getFloor());

    }

    @Test
    void testTenorRate() throws IOException {
        Resource baseResource = ctx.resourceResolver().getResource("/content/dam/techcombank/master-data/tenor-rates");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/tenor-rates"))
                .thenReturn(baseResource);
        Resource tenorRes = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/tenor-rates/2023/08/11/23-16-01");
        lenient()
                .when(resourceResolver
                        .getResource("/content/dam/techcombank/master-data/tenor-rates/2023/08/11/23-16-01"))
                .thenReturn(tenorRes);
        String vfxRateType = TestConstants.TENOR_RATE;
        List<String> tenorRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, tenorRateBasepath.get(0));
        assertEquals("2023-08-11 23:16:01", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource tenorRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                tenorRateBasepath.get(0));
        assertEquals("23-16-01", tenorRateRes.getName());

        Resource tenorResource = ctx.resourceResolver().getResource(
                "/content/dam/techcombank/master-data/tenor-rates/2023/08/11/23-16-01/2663/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(
                "/content/dam/techcombank/master-data/tenor-rates/2023/08/11/23-16-01/2663/jcr:content/data/master"))
                .thenReturn(tenorResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver()
                .getResource("/content/dam/techcombank/master-data/tenor-rates/2023/08/11");
        lenient().when(resourceResolver.getResource("/content/dam/techcombank/master-data/tenor-rates/2023/08/11"))
                .thenReturn(timeResource);
        List<String> vfxRateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                tenorRateBasepath.get(0));
        JsonObject tenorRateJson = vfxRatesDataService.getVfxRateData(tenorRateRes, resourceResolver, vfxRateTimeStamp,
                vfxRateType);
        JsonArray tenor = tenorRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = tenor.get(0);

        JsonObject tenorRate = data.getAsJsonObject();
        assertEquals("2663", tenorRate.get("itemId").getAsString());
        assertEquals("23455", tenorRate.get("askRate").getAsString());
        assertEquals("23455", tenorRate.get("bidRate").getAsString());
        assertEquals("JPY", tenorRate.get("sourceCurrency").getAsString());
        assertEquals("VND", tenorRate.get("targetCurrency").getAsString());
        assertEquals("5", tenorRate.get("tenor").getAsString());

        TenorRate tenorRates = tenorResource.adaptTo(TenorRate.class);
        assertEquals("2663", tenorRates.getItemId());
        assertEquals("23455", tenorRates.getAskRate());
        assertEquals("23455", tenorRates.getBidRate());
        assertEquals("JPY", tenorRates.getSourceCurrency());
        assertEquals("VND", tenorRates.getTargetCurrency());
        assertEquals("5", tenorRates.getTenor());

    }

    @Test
    void testTenorIntRate() throws IOException {
        String tenorCF = "/content/dam/techcombank/master-data/tenor-int-rates";
        Resource baseResource = ctx.resourceResolver().getResource(tenorCF);
        lenient().when(resourceResolver.getResource(tenorCF)).thenReturn(baseResource);
        Resource tenorIntRes = ctx.resourceResolver().getResource(tenorCF + "/2023/08/11/23-16-01");
        lenient().when(resourceResolver.getResource(tenorCF + "/2023/08/11/23-16-01")).thenReturn(tenorIntRes);
        String vfxRateType = TestConstants.TENOR_INT_RATE;
        List<String> tenorIntRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver,
                tenorIntRateBasepath.get(0));
        assertEquals("2023-08-11 23:16:01", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource tenorIntRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                tenorIntRateBasepath.get(0));
        assertEquals("23-16-01", tenorIntRateRes.getName());

        Resource tenorIntResource = ctx.resourceResolver()
                .getResource(tenorCF + "/2023/08/11/23-16-01/64375/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(tenorCF + "/2023/08/11/23-16-01/64375/jcr:content/data/master"))
                .thenReturn(tenorIntResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver().getResource(tenorCF + "/2023/08/11");
        lenient().when(resourceResolver.getResource(tenorCF + "/2023/08/11")).thenReturn(timeResource);
        List<String> vfxRateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                tenorIntRateBasepath.get(0));
        JsonObject tenorIntRateJson = vfxRatesDataService.getVfxRateData(tenorIntRateRes, resourceResolver,
                vfxRateTimeStamp, vfxRateType);
        JsonArray tenorInt = tenorIntRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = tenorInt.get(0);

        JsonObject tenorIntRate = data.getAsJsonObject();
        assertEquals("64375", tenorIntRate.get("itemId").getAsString());
        assertEquals("LIBOREUR", tenorIntRate.get("rateCD").getAsString());
        assertEquals("LIBOR EUR", tenorIntRate.get("rateLB").getAsString());
        assertEquals("5.40443", tenorIntRate.get("tenorInt").getAsString());
        assertEquals("3", tenorIntRate.get("tenor").getAsString());

        TenorIntRate tenorIntRates = tenorIntResource.adaptTo(TenorIntRate.class);
        assertEquals("64375", tenorIntRates.getItemId());
        assertEquals("LIBOREUR", tenorIntRates.getRateCD());
        assertEquals("LIBOR EUR", tenorIntRates.getRateLB());
        assertEquals("5.40443", tenorIntRates.getTenorInt());
        assertEquals("3", tenorIntRates.getTenor());

    }

    @Test
    void testFixingRate() throws IOException {
        String fixingCF = "/content/dam/techcombank/master-data/fixing-rates";
        Resource baseResource = ctx.resourceResolver().getResource(fixingCF);
        lenient().when(resourceResolver.getResource(fixingCF)).thenReturn(baseResource);
        Resource fixingRes = ctx.resourceResolver().getResource(fixingCF + "/2023/08/11/23-16-01");
        lenient().when(resourceResolver.getResource(fixingCF + "/2023/08/11/23-16-01")).thenReturn(fixingRes);
        String vfxRateType = TestConstants.FIXING_RATE;
        List<String> fixingRateBasepath = vfxRatesDataService.getVfxRateBasePath(vfxRateType, false);
        String vfxRateDateTime = vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, fixingRateBasepath.get(0));
        assertEquals("2023-08-11 23:16:01", vfxRateDateTime);
        String vfxRateDateTimePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, true);
        Resource fixingRateRes = vfxRatesDataService.getVfxRate(resourceResolver, vfxRateDateTimePath,
                fixingRateBasepath.get(0));
        assertEquals("23-16-01", fixingRateRes.getName());

        Resource fixingResource = ctx.resourceResolver()
                .getResource(fixingCF + "/2023/08/11/23-16-01/1075/jcr:content/data/master");
        lenient().when(resourceResolver.getResource(fixingCF + "/2023/08/11/23-16-01/1075/jcr:content/data/master"))
                .thenReturn(fixingResource);
        String vfxRateDatePath = PlatformUtils.getVfxRateDateTimePath(vfxRateDateTime, false);
        Resource timeResource = ctx.resourceResolver().getResource(fixingCF + "/2023/08/11");
        lenient().when(resourceResolver.getResource(fixingCF + "/2023/08/11")).thenReturn(timeResource);
        List<String> rateTimeStamp = vfxRatesDataService.getVfxRateTimeStamp(resourceResolver, vfxRateDatePath,
                fixingRateBasepath.get(0));
        JsonObject fixingRateJson = vfxRatesDataService.getVfxRateData(fixingRateRes, resourceResolver, rateTimeStamp,
                vfxRateType);
        JsonArray fixing = fixingRateJson.getAsJsonArray(TestConstants.VFX_RATE_DATA);
        JsonElement data = fixing.get(0);

        JsonObject fixingRate = data.getAsJsonObject();
        assertEquals("1075", fixingRate.get("itemId").getAsString());
        assertEquals("2023-08-15", fixingRate.get("inputDate").getAsString());
        assertEquals("12:00:00", fixingRate.get("inputTime").getAsString());
        assertEquals("EUR", fixingRate.get("sourceCurrency").getAsString());
        assertEquals("USD", fixingRate.get("targetCurrency").getAsString());
        assertEquals("1.0917", fixingRate.get("vndLoan").getAsString());
        assertNull(fixingRate.get("smartMoney"));

        FixingRate fixingRates = fixingResource.adaptTo(FixingRate.class);
        assertEquals("1075", fixingRates.getItemId());
        assertEquals("2023-08-15", fixingRates.getInputDate());
        assertEquals("12:00:00", fixingRates.getInputTime());
        assertEquals("EUR", fixingRates.getSourceCurrency());
        assertEquals("USD", fixingRates.getTargetCurrency());
        assertEquals("1.0917", fixingRates.getVndLoan());
        assertNull(fixingRates.getSmartMoney());

    }

    //@Test
    void testCreateVfxRateData() throws ContentFragmentException, RepositoryException, IOException {

        String exchnageRateBasePath = "/content/dam/techcombank/master-data/exchange-rates";
        String exchangeRateCFResource = "/conf/techcombank/web/settings/dam/cfm/models/exchange-rate-fragment";
        String tenorRateCFResource = "/conf/techcombank/web/settings/dam/cfm/models/tenor-rate-fragment";
        String tenorIntRateCFResource = "/conf/techcombank/web/settings/dam/cfm/models/tenor-int-rate-fragment";
        String goldRateCFResource = "/conf/techcombank/web/settings/dam/cfm/models/gold-rate-fragment";
        String otherRateCFResource = "/conf/techcombank/web/settings/dam/cfm/models/other-rate-fragment";
        String exchangeRatePackagePath = "/content/dam/techcombank/master-data/exchange-rates/2022/07/09/12-44-42";
        String goldRatePackagePath = "/content/dam/techcombank/master-data/gold-rates/2022/07/09";
        JsonObject vfxData = vfxRateDataJson.get("data").getAsJsonObject();

        lenient().when(resourceResolver.getResource(exchnageRateBasePath)).thenReturn(resource);

        lenient().when(resource.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        lenient().when(resourceResolver.getResource(exchangeRateCFResource)).thenReturn(exchangeRateResource);
        lenient().when(resourceResolver.getResource(tenorRateCFResource)).thenReturn(tenorRateResource);
        lenient().when(resourceResolver.getResource(goldRateCFResource)).thenReturn(goldRateResource);
        lenient().when(resourceResolver.getResource(otherRateCFResource)).thenReturn(otherRateResource);
        lenient().when(resourceResolver.getResource(tenorIntRateCFResource)).thenReturn(tenorIntRateResource);

        lenient().when(exchangeRateResource.adaptTo(FragmentTemplate.class)).thenReturn(exchangeRatefragmentTemplate);
        lenient().when(goldRateResource.adaptTo(FragmentTemplate.class)).thenReturn(goldRatefragmentTemplate);
        lenient().when(otherRateResource.adaptTo(FragmentTemplate.class)).thenReturn(otherRatefragmentTemplate);
        lenient().when(tenorRateResource.adaptTo(FragmentTemplate.class)).thenReturn(tenorRatefragmentTemplate);
        lenient().when(tenorIntRateResource.adaptTo(FragmentTemplate.class)).thenReturn(tenorIntRatefragmentTemplate);

        lenient().when(exchangeRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(exchangeRateFragment);

        lenient().when(tenorRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(tenorRateFragment);
        lenient().when(otherRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(otherRateFragment);
        lenient().when(goldRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(goldRateFragment);
        lenient().when(tenorIntRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(tenorIntRateFragment);

        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        String mypackageName = "my_packages";
        String timePath = "VFX-Rates-2022-07-09-12-44-42";
        lenient().when(packaging.getPackageManager(session)).thenReturn(jcrPackageManager);
        lenient().when(jcrPackageManager.create(mypackageName, timePath)).thenReturn(jcrPackage);
        lenient().when(jcrPackage.getDefinition()).thenReturn(jcrPackageDefinition);

        lenient().when(resourceResolver.getResource(exchangeRatePackagePath)).thenReturn(resource);
        lenient().when(resourceResolver.getResource(goldRatePackagePath)).thenReturn(resource);
        lenient().when(jcrPackage.getNode()).thenReturn(node);
        lenient().when(node.getPath()).thenReturn("/etc/packages/my_packages/VFX-Rates-2022-07-09-12-44-42.zip");

        List<ContentElement> exchangeRateCE = new ArrayList<>();
        exchangeRateCE.add(ce1);
        exchangeRateCE.add(ce2);
        exchangeRateCE.add(ce3);
        exchangeRateCE.add(ce4);
        exchangeRateCE.add(ce5);
        exchangeRateCE.add(ce6);
        exchangeRateCE.add(ce15);
        lenient().when(exchangeRateFragment.getElements()).thenReturn(exchangeRateCE.iterator());
        lenient().when(ce1.getName()).thenReturn("ItemID");
        lenient().when(ce2.getName()).thenReturn("label");
        lenient().when(ce3.getName()).thenReturn("sourceCurrency");
        lenient().when(ce4.getName()).thenReturn("bidRateTM");
        lenient().when(ce5.getName()).thenReturn("bidRateCK");
        lenient().when(ce6.getName()).thenReturn("askRate");
        lenient().when(ce15.getName()).thenReturn("askRateTM");

        List<ContentElement> goldRateCE = new ArrayList<>();
        goldRateCE.add(ce1);
        goldRateCE.add(ce2);
        goldRateCE.add(ce6);
        goldRateCE.add(ce7);
        lenient().when(goldRateFragment.getElements()).thenReturn(goldRateCE.iterator());
        lenient().when(ce1.getName()).thenReturn("ItemID");
        lenient().when(ce2.getName()).thenReturn("label");
        lenient().when(ce6.getName()).thenReturn("askRate");
        lenient().when(ce7.getName()).thenReturn("bidRate");

        List<ContentElement> otherRateCE = new ArrayList<>();
        otherRateCE.add(ce1);
        otherRateCE.add(ce8);
        otherRateCE.add(ce9);
        otherRateCE.add(ce10);
        lenient().when(otherRateFragment.getElements()).thenReturn(otherRateCE.iterator());
        lenient().when(ce1.getName()).thenReturn("ItemID");
        lenient().when(ce8.getName()).thenReturn("central");
        lenient().when(ce9.getName()).thenReturn("floor");
        lenient().when(ce10.getName()).thenReturn("ceiling");

        List<ContentElement> tenorRateCE = new ArrayList<>();
        tenorRateCE.add(ce1);
        tenorRateCE.add(ce3);
        tenorRateCE.add(ce7);
        tenorRateCE.add(ce6);
        tenorRateCE.add(ce11);
        lenient().when(tenorRateFragment.getElements()).thenReturn(tenorRateCE.iterator());
        lenient().when(ce1.getName()).thenReturn("ItemID");
        lenient().when(ce3.getName()).thenReturn("sourceCurrency");
        lenient().when(ce6.getName()).thenReturn("askRate");
        lenient().when(ce7.getName()).thenReturn("bidRate");
        lenient().when(ce11.getName()).thenReturn("tenor");

        List<ContentElement> tenorIntRateCE = new ArrayList<>();
        tenorIntRateCE.add(ce1);
        tenorIntRateCE.add(ce11);
        tenorIntRateCE.add(ce12);
        tenorIntRateCE.add(ce13);
        tenorIntRateCE.add(ce14);
        lenient().when(tenorIntRateFragment.getElements()).thenReturn(tenorIntRateCE.iterator());
        lenient().when(ce1.getName()).thenReturn("ItemID");
        lenient().when(ce11.getName()).thenReturn("tenor");
        lenient().when(ce12.getName()).thenReturn("rateCD");
        lenient().when(ce13.getName()).thenReturn("rateLB");
        lenient().when(ce14.getName()).thenReturn("tenorInt");

        assertTrue(vfxRatesDataService.createVfxRateData(resourceResolver, vfxData));

        lenient().when(exchangeRatefragmentTemplate.createFragment(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenThrow(ContentFragmentException.class);

        assertFalse(vfxRatesDataService.createVfxRateData(resourceResolver, vfxData));
    }

}

