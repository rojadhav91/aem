package com.techcombank.core.servlets;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.VfxRatesDataService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class VfxRatesDataServletTest {
    @InjectMocks
    private VfxRatesDataServlet currencyConvertor = new VfxRatesDataServlet();

    @Mock
    private Resource resource;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private VfxRatesDataService vfxRatesDataService;

    @Mock
    private PrintWriter out;

    @Mock
    private RequestPathInfo requestPathInfo;

    private final AemContext ctx = new AemContext();

    private JsonObject exchangeRateDataJson;

    private static final int STATUS = 200;

    @BeforeEach
    void setUp() throws Exception {
        String file = TestConstants.CONTENT_ROOT_SRC_PATH + TestConstants.CONTENT_BASE_PATH
                + TestConstants.EXCHANGE_RATE_JSON;
        ctx.addModelsForClasses(VfxRatesDataServlet.class);
        JsonReader reader = new JsonReader(new FileReader(file));
        Gson gsonObj = new Gson();
        Map<String, String> exchangeRateData = gsonObj.fromJson(reader, Map.class);
        JsonElement exchangeRate = gsonObj.toJsonTree(exchangeRateData);
        exchangeRateDataJson = exchangeRate.getAsJsonObject();
    }

    @Test
    void doGet(final AemContext context) throws ServletException, IOException {
        String[] selector1 = {"exchange-rates", "2023-07-13", "23-16-01", "integration"};
        String[] selector2 = {"exchange-rates", "integration"};
        String[] selector3 = {"exchange-rates", "2023-07-13", "integration"};
        List<String> basePaths = new ArrayList<>();
        basePaths.add("/content/dam/techcombank/master-data/exchange-rates");
        basePaths.add("/content/dam/techcombank/master-data/gold-rates");
        basePaths.add("/content/dam/techcombank/master-data/other-rates");
        basePaths.add("/content/dam/techcombank/master-data/tenor-rates");
        basePaths.add("/content/dam/techcombank/master-data/tenor-int-rates");
        basePaths.add("/content/dam/techcombank/master-data/fixing-rates");
        List<String> vfxRateTimeStamp = new ArrayList<>();
        vfxRateTimeStamp.add("23-16-01");
        vfxRateTimeStamp.add("10-30-01");
        vfxRateTimeStamp.add("12-30-30");
        lenient().when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        lenient().when(requestPathInfo.getSelectors()).thenReturn(selector1);

        lenient().when(vfxRatesDataService.getVfxRateBasePath(TestConstants.EXCHANGE_RATE, true)).thenReturn(basePaths);
        lenient().when(vfxRatesDataService.getVfxRateTimeStamp(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(vfxRateTimeStamp);
        MockSlingHttpServletResponse res = ctx.response();
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(vfxRatesDataService.getVfxRate(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(resource);
        lenient().when(vfxRatesDataService.getVfxRateData(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(exchangeRateDataJson);
        lenient().when(response.getWriter()).thenReturn(out);
        currencyConvertor.doGet(request, res);
        assertEquals(STATUS, res.getStatus());
        lenient().when(requestPathInfo.getSelectors()).thenReturn(selector2);
        lenient().when(vfxRatesDataService.getVfxRateUpdateDate(Mockito.any(), Mockito.any()))
                .thenReturn("2023-07-13 23:16:00");
        currencyConvertor.doGet(request, res);
        assertEquals(STATUS, res.getStatus());

        lenient().when(requestPathInfo.getSelectors()).thenReturn(selector3);
        currencyConvertor.doGet(request, res);
        assertEquals(STATUS, res.getStatus());
    }
}
