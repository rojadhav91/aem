
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.GoldRateModelImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class GoldRateModelTest to perform junit for GoldRateModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class GoldRateModelTest {

    private final AemContext ctx = new AemContext();

    private GoldRateModelImpl goldRateModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.GOLD_RATE_CMP_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(GoldRateModelImpl.class);
        goldRateModel = getModel("/content/jcr:content/root/container/goldrate");

    }

    private GoldRateModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        GoldRateModelImpl goldRate = res.adaptTo(GoldRateModelImpl.class);
        return goldRate;
    }

    @Test
    void testGoldHeaderSection() {
        assertEquals("Gold rate", goldRateModel.getGoldRateTitle());
        assertEquals("Day", goldRateModel.getDayLabel());
        assertEquals("Updated time", goldRateModel.getTimeLabel());
        assertEquals("Buy", goldRateModel.getBuyLabel());
        assertEquals("Sell", goldRateModel.getSellLabel());
    }

    @Test
    void testIcon() {
        assertEquals("/content/dam/techcombank/calendar.svg", goldRateModel.getDayIcon());
        assertEquals("DayAlt", goldRateModel.getDayIconAlt());
        assertEquals("/content/dam/techcombank/calendar.svg", goldRateModel.getDayIconWebImagePath());
        assertEquals("/content/dam/techcombank/calendar.svg", goldRateModel.getDayIconMobileImagePath());
        assertEquals("/content/dam/techcombank/clock.svg", goldRateModel.getTimeIcon());
        assertEquals("timeAlt", goldRateModel.getTimeIconAlt());
        assertEquals("/content/dam/techcombank/clock.svg", goldRateModel.getTimeIconWebImagePath());
        assertEquals("/content/dam/techcombank/clock.svg", goldRateModel.getTimeIconMobileImagePath());
    }

}
