package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class StaticGridPanelModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private StaticGridPanelModel staticGridPanelModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(StaticGridPanelModel.class);
        aemContext.load().json(TestConstants.STATICGRIDPANEL, "/staticGridPanel");
        aemContext.currentResource(TestConstants.STATICGRIDPANELCOMP);
        Resource resource = aemContext.request().getResource();
        staticGridPanelModel = resource.adaptTo(StaticGridPanelModel.class);
    }

    @Test
    void getHeading() {
        final String expected = "Heading";
        assertEquals(expected, staticGridPanelModel.getHeading());
    }

    @Test
    void getLayout() {
        final String expected = "3 per row";
        assertEquals(expected, staticGridPanelModel.getLayout());
    }

    @Test
    void getIconImageAlignment() {
        final String expected = "icon-left";
        assertEquals(expected, staticGridPanelModel.getIconImageAlignment());
    }

    @Test
    void testStaticFridPanelItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, staticGridPanelModel.getStaticGridPanelItems().size());
    }
}
