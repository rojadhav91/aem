package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.utils.PlatformUtils;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ColumnPanelCardListModelTest {

    private final AemContext ctx = new AemContext();

    private ColumnPanelCardListModel columnPanelCardListModel;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.COLUMNPANELCARDLISTITEM, "/columnPanelCardList");
        ctx.addModelsForClasses(ColumnPanelCardListModel.class);
        columnPanelCardListModel = getModel("/columnPanelCardList/columnpanel");
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        valueMap.put("jcr:title", "Column Panel");
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(resourceResolver.getResource("https://www.google.com")).thenReturn(null);
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "https://www.google.com")).
                thenReturn("https://www.google.com");
    }

    private ColumnPanelCardListModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        ColumnPanelCardListModel columnpanel = res.adaptTo(ColumnPanelCardListModel.class);
        return columnpanel;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getCardLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, columnPanelCardListModel.getCardLinkUrl());
    }

    @Test
    void getCardTitle() {
        final String expected = "Song Khoe Moi Ngay";
        assertEquals(expected, columnPanelCardListModel.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "Excellent option for anyone looking for health insurance.";
        assertEquals(expected, columnPanelCardListModel.getCardDescription());
    }

    @Test
    void isCardTarget() {
        final String expected = "_blank";
        assertEquals(expected, columnPanelCardListModel.getCardTarget());
    }

    @Test
    void isCardNoFollow() {
        final boolean expected = true;
        assertEquals(expected, columnPanelCardListModel.isCardNoFollow());
    }

    @Test
    void isCardArrow() {
        final boolean expected = false;
        assertEquals(expected, columnPanelCardListModel.isCardArrow());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit",
                columnPanelCardListModel.getWebInteractionType());
    }

}
