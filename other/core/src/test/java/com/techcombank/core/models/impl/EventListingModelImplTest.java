package com.techcombank.core.models.impl;

import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.InvalidTagFormatException;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class EventListingModelImplTest {

    @Mock
    private ResourceResolver resolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @Mock
    private RangeIterator<Resource> rangeItr;

    @InjectMocks
    private EventListingModelImpl eventListingModel;

    private final AemContext aemContext = AppAemContext.newAemContext();

    private EventListingModelImpl model;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty() {
        eventListingModel.init();
        assertTrue(!eventListingModel.getTags().isEmpty());
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty2() throws InvalidTagFormatException {
        aemContext.addModelsForClasses(EventListingModelImpl.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.EVENT_LISTING_JSON, "/eventlist");
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.EVENT_LISTING_JSON,
                TestConstants.CONTENT_EVENTS_TAG);
        aemContext.currentResource(TestConstants.EVENT_LIST);
        Resource resource = aemContext.request().getResource();
        tag = Mockito.mock(Tag.class);
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);

        lenient().when(tagManager.resolve("techcombank:event-type")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/event-type/eventlist")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/event-type/tags/agm")).thenReturn(tag);
        lenient().when(tag.getTagID()).thenReturn("tag");
        lenient().when(tag.getPath()).thenReturn("/content/cq:tags/techcombank/event-type/tags");
        lenient().when(tagManager.find("/content/dam/techcombank/master-data/events", new String[] {"tag"}))
                .thenReturn(rangeItr);
        lenient().when(rangeItr.getSize()).thenReturn(1L);

        model = resource.adaptTo(EventListingModelImpl.class);
        assertEquals("/content/dam/techcombank/master-data/events",
                Objects.requireNonNull(model).getEventsCFRootPath());
        assertNotNull(model.getResolver());
        assertEquals("eventTypeLabel", model.getEventTypeLabel());
        assertEquals("showMeLabel", model.getShowMeLabel());
        assertEquals("viewMoreLabel", model.getViewMoreLabel());
        assertEquals("techcombank:event-type", model.getTagRootPath());
        assertEquals("selectDateLabel", model.getSelectDateLabel());
        assertEquals("filterLabel", model.getFilterLabel());
        assertEquals("applyFilterLabel", model.getApplyFilterLabel());
        assertEquals("upcomingEventsLabel", model.getUpcomingEventsLabel());
        assertEquals("pastEventsLabel", model.getPastEventsLabel());
        assertNotNull(model.getGsonObj());
        assertNotNull(model.getJsonAry());
    }
}
