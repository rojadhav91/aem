package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The Class AppointmentBookingModelTest to perform junit for
 * AppointmentBookingModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class AppointmentBookingModelTest {

    private final AemContext ctx = new AemContext();

    private AppointmentBookingModel appointmentBookingModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.APPOINTMENT_BOOKING_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(AppointmentBookingModel.class);
        appointmentBookingModel = getModel("/content/jcr:content/root/container/appointmentbooking");

    }

    private AppointmentBookingModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        AppointmentBookingModel appointmentBooking = res.adaptTo(AppointmentBookingModel.class);
        return appointmentBooking;
    }

    @Test
    void testCustomerSection() {
        assertTrue(appointmentBookingModel.isEnableTabs());
        assertEquals("You are:", appointmentBookingModel.getIdentifierLabel());
        assertEquals("Personal Customer", appointmentBookingModel.getPersonalLabel());
        assertEquals("Business Customer", appointmentBookingModel.getBusinessLabel());
    }

    @Test
    void testBranchSection() {
        assertEquals("What would you want to do?", appointmentBookingModel.getActionLabel());
        assertEquals("At which branch?", appointmentBookingModel.getBranchLabel());
        assertEquals("City/Province", appointmentBookingModel.getCityLabel());
        assertEquals("District", appointmentBookingModel.getDistrictLabel());
        assertEquals("Branch list:", appointmentBookingModel.getBranchListLabel());
    }

    @Test
    void testSlotSection() {
        assertEquals("Today", appointmentBookingModel.getTodayLabel());
        assertEquals("Tomorrow", appointmentBookingModel.getTomorrowLabel());
        assertEquals("What time can we meet?", appointmentBookingModel.getTimeSlotLabel());
    }

    @Test
    void testDetailsSection() {
        assertEquals("Please provide your details to complete the booking", appointmentBookingModel.getTitle());
        assertEquals("Full name", appointmentBookingModel.getNameLabel());
        assertEquals("Full name", appointmentBookingModel.getNamePlaceHolder());
        assertEquals("Email *", appointmentBookingModel.getEmailLabel());
        assertEquals("Email", appointmentBookingModel.getEmailPlaceHolder());
        assertEquals("Phone *", appointmentBookingModel.getPhoneLabel());
        assertEquals("Phone", appointmentBookingModel.getPhonePlaceHolder());
        assertEquals("Submit", appointmentBookingModel.getCtaLabel());
        assertEquals("AccNo/TaxCod", appointmentBookingModel.getAccTaxLabel());
        assertEquals("012345678", appointmentBookingModel.getAccTaxPlaceHolder());
        assertTrue(appointmentBookingModel.isEnableRecaptcha());
        assertEquals("Email is required.", appointmentBookingModel.getEmailRequired());
        assertEquals("Email is invalid.", appointmentBookingModel.getEmailInvalid());
        assertEquals("Phone is required.", appointmentBookingModel.getPhoneRequired());
        assertEquals("Phone is invalid.", appointmentBookingModel.getPhoneInvalid());
        assertEquals("AccNo/TaxCode is required.", appointmentBookingModel.getAccTaxRequired());
    }

    @Test
    void testConfirmationSection() {
        String expConfirmationIcon = "/content/dam/techcombank/calendar.svg";
        assertEquals(expConfirmationIcon, appointmentBookingModel.getConfirmationIcon());
        assertEquals(expConfirmationIcon, appointmentBookingModel.getConfirmationIconWebImagePath());
        assertEquals(expConfirmationIcon, appointmentBookingModel.getConfirmationIconMobileImagePath());
        assertEquals("confirmation alt", appointmentBookingModel.getConfirmationIconAlt());
        assertEquals("Your Ticket is confirmed", appointmentBookingModel.getConfirmationMessage());
        assertEquals("See you on Saturday, 12 August, 09:00", appointmentBookingModel.getBookingMessage());
        assertEquals("Ticket Number:", appointmentBookingModel.getTicketNumberLabel());
        assertEquals("Ticket will expire 10 minutes after appointment time.", appointmentBookingModel.getTicketNotes());

    }

}
