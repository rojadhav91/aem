package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Locale;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(AemContextExtension.class)
class OfferListingModelImplTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private OfferListingModelImpl model;

    @Mock
    private Page currentPage;

    @BeforeEach
    void setUp() {
        currentPage = mock(Page.class);
        aemContext.addModelsForClasses(OfferListingModelImpl.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.OFFERLISTING_JSON, "/offerListing");
        aemContext.currentResource(TestConstants.OFFERLISTINGCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(OfferListingModelImpl.class);
    }

    @Test
    void testLeftPanelSection() {
        assertEquals("Category", model.getCategoryFilter());
        assertEquals("Products", model.getCardFilter());
        assertEquals("techcombank:promotions/promotion-categories/healthcare", model.getTagFilter()[0]);
        assertEquals("techcombank:promotions/product-types/debit-card", model.getCardType()[0]);
        assertEquals("Select merchant", model.getMerchantFilter());
        assertEquals("techcombank:promotions/merchant/farfetch", model.getMerchantType()[0]);
        assertEquals("Select merchant placeholder", model.getMerchantPlaceholder());
        assertEquals("Select all", model.getSelectAllMerchant());
    }

    @Test
    void testSortSectionFields() {
        assertEquals("Sort", model.getSortFilter());
        assertEquals("Most Popular", model.getMostPopular());
        assertEquals("Latest", model.getLatest());
        assertEquals("More time to consider", model.getMoreTime());
        assertEquals("Expiring soon", model.getExpiringSoon());
        assertEquals("Filter", model.getFilter());
        assertEquals("Apply filter", model.getApply());
        assertEquals("View Result", model.getViewResult());
        assertEquals("View More", model.getViewMore());
        assertEquals("Showing %count promotions", model.getResultCountText());
        assertEquals("Search for Promotions", model.getSearchPlaceHolderText());
        assertEquals("No offers can be found", model.getEmptyPromotionLabel());
        assertEquals("Day", model.getExpiryDayLabel());
        assertEquals("Expires on", model.getExpiryLabel());
    }

    @Test
    void testFilterTags() throws NoSuchFieldException {
        PrivateAccessor.setField(model, "currentPage", currentPage);
        String[] emptyTagList = {"techcombank:card-types/debit-card"};
        TagManager tagManager = mock(TagManager.class);
        Tag tag = mock(Tag.class);
        when(tagManager.resolve("techcombank:card-types/debit-card")).thenReturn(tag);
        Map<String, String> filterTags = model.getFilters(emptyTagList);
        assertEquals(filterTags, model.getFilterTags());
        assertEquals(filterTags, model.getCardTypeTags());
        assertEquals(filterTags, model.getMerchantTypeTags());

        when(currentPage.getLanguage()).thenReturn(Locale.ENGLISH);
        assertEquals(Locale.ENGLISH.toString(), model.getLocaleLanguage());
    }

}
