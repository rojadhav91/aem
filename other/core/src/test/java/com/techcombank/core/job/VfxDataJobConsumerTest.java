package com.techcombank.core.job;

import com.google.gson.JsonObject;
import com.techcombank.core.jobs.VfxDataJobConsumer;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.VfxRatesDataService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.event.jobs.Job;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class VfxDataJobConsumerTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private VfxDataJobConsumer vfxRateJob = new VfxDataJobConsumer();

    @Mock
    private VfxRatesDataService vfxRatesDataService;

    @Mock
    private Job job;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.addModelsForClasses(VfxDataJobConsumer.class);
    }

    @Test
    void testProcess() {
        JsonObject dateCreated = new JsonObject();
        JsonObject vfxData = new JsonObject();
        dateCreated.addProperty("input_date", "2021-07-04T21:00:0");
        dateCreated.addProperty("id", "VFX.20210617.135422");
        vfxData.add("data", dateCreated);
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        lenient().when(job.getProperty("vfxData")).thenReturn(vfxData.toString());
        lenient().when(vfxRatesDataService.createVfxRateData(Mockito.any(), Mockito.any())).thenReturn(true);
        assertEquals("OK", vfxRateJob.process(job).toString());
        lenient().when(vfxRatesDataService.createVfxRateData(Mockito.any(), Mockito.any())).thenReturn(false);
        assertEquals("FAILED", vfxRateJob.process(job).toString());
    }
}
