package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CreditRatingTabModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CreditRatingTabModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CreditRatingTabModel.class);
        aemContext.load().json(TestConstants.CREDITRATINGTAB, "/creditRatingTab");
        aemContext.currentResource(TestConstants.CREDITRATINGTABCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CreditRatingTabModel.class);
    }

    @Test
    void getFormattedDate() {
        final String expected = "01/01/2023";
        assertEquals(expected, model.getFormattedDate());
    }


    @Test
    void getCategoryHeader() {
        final String expected = "text";
        assertEquals(expected, model.getCategoryHeader());
    }

    @Test
    void getRatingHeader() {
        final String expected = "text";
        assertEquals(expected, model.getRatingHeader());
    }

    @Test
    void getCreditRatingDataList() {
        assert model.getCreditRatingDataList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getCreditRatingDataList().size());
    }
}
