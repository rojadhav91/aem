package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class LinkTextItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private LinkTextItemModel linkTextItemModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(LinkTextItemModel.class);
        aemContext.load().json(TestConstants.LINKTEXTITEM, "/linkTextItem");
        aemContext.currentResource(TestConstants.LINKTEXTITEMCOMP);
        Resource resource = aemContext.request().getResource();
        linkTextItemModel = resource.adaptTo(LinkTextItemModel.class);
    }

    @Test
    void testIconImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, linkTextItemModel.getIconImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(linkTextItemModel.getIconImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                linkTextItemModel.getWebIconImagePath());
        assertEquals(linkTextItemModel.getIconImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                linkTextItemModel.getMobileIconImagePath());
    }

    @Test
    void testIconImageAltText() {
        final String expected = "icon1";
        assertEquals(expected, linkTextItemModel.getIconImageAltText());
    }

    @Test
    void testTitleText() {
        final String expected = "Digital Bank";
        assertEquals(expected, linkTextItemModel.getTitleText());
    }

    @Test
    void testDescription() {
        final String expected = "Experience Vietnam's leading digital banking";
        assertEquals(expected, linkTextItemModel.getDescription());
    }

    @Test
    void testLinkUrl() {
        final String expected = "/en/personal/digital-services"
                + "/digital-banking/techcombank-mobile";
        assertEquals(expected, linkTextItemModel.getLinkUrl());
    }

    @Test
    void testLinkUrlType() {
        final String expected = "redirection";
        assertEquals(expected, linkTextItemModel.getLinkUrlType());
    }

    @Test
    void testOpenInNewTab() {
        final Boolean expected = false;
        assertEquals(expected, linkTextItemModel.getOpenInNewTab());
    }

    @Test
    void testNoFollow() {
        final Boolean expected = true;
        assertEquals(expected, linkTextItemModel.getNoFollow());
    }

    @Test
    void testRow() {
        final int expected = TestConstants.LINK_TEXT_ITEM_INDEX;
        assertEquals(expected, linkTextItemModel.getRow());
    }

    @Test
    void testColumn() {
        final int expected = TestConstants.LINK_TEXT_ITEM_INDEX;
        assertEquals(expected, linkTextItemModel.getColumn());
    }
}
