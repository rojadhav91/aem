package com.techcombank.core.services;

import com.day.cq.wcm.api.Page;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.JwtTokenService;
import com.techcombank.core.service.RecaptchaService;
import com.techcombank.core.service.impl.QmsIntegrationServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class QmsIntegrationServiceTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private QmsIntegrationServiceImpl qmsIntegrationService = new QmsIntegrationServiceImpl();

    @Mock
    private HttpClientExecutorService httpExecutor;

    @Mock
    private RecaptchaService recaptchaService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private Page page;

    @Mock
    private JwtTokenService jwtTokenService;


    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.APPOINTMENT_BOOKING_JSON,
                TestConstants.CONTENTPATH);
        ctx.registerService(HttpClientExecutorService.class, httpExecutor);
        ctx.registerService(JwtTokenService.class, jwtTokenService);
        ctx.registerService(RecaptchaService.class, recaptchaService);
        ctx.registerInjectActivateService(qmsIntegrationService);
        ctx.addModelsForClasses(QmsIntegrationServiceImpl.class);
    }

    @Test
    void testBranchSlot() throws UnsupportedEncodingException {
        JsonArray branchSlotDataArray = new JsonArray();
        JsonObject branchSlotData = new JsonObject();
        branchSlotData.addProperty("slotType", "PM");
        branchSlotData.addProperty("slotTime", "15:30");
        branchSlotDataArray.add(branchSlotData);
        JsonObject branchSlot = new JsonObject();
        branchSlot.add("data", branchSlotDataArray);
        JsonObject reqObj = new JsonObject();
        reqObj.addProperty("branchID", "78");
        reqObj.addProperty("srvID", "71");
        lenient().when(request.getResource()).thenReturn(resource);
        lenient().when(resource.getPath()).thenReturn("/content/techcombank/web/vn/en/appoinment-booking");
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(httpExecutor.executeHttpPost(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(branchSlot);
        JsonObject responseData = qmsIntegrationService.fetchQMSBranchSlot(false, request, reqObj);
        JsonObject stockChartArray = responseData.get("data").getAsJsonArray().get(0).getAsJsonObject();
        assertEquals("PM", stockChartArray.get("slotType").getAsString());
        assertEquals("15:30", stockChartArray.get("slotTime").getAsString());
    }


    @Test
    void testBranchList() throws UnsupportedEncodingException {

        JsonArray branchListArray = new JsonArray();
        JsonObject branchDetails = new JsonObject();
        branchDetails.addProperty("branchId", "105");
        branchDetails.addProperty("branchCd", "HCB");
        branchListArray.add(branchDetails);
        JsonObject branchData = new JsonObject();
        branchData.add("branchList", branchListArray);
        JsonObject reqObj = new JsonObject();
        reqObj.addProperty("search", "Dong");
        reqObj.addProperty("lat", "10.223341");
        reqObj.addProperty("long", "10.223341");
        reqObj.addProperty("srvIdent", "79");
        lenient().when(request.getResource()).thenReturn(resource);
        lenient().when(resource.getPath()).thenReturn("/content/techcombank/web/vn/en/appoinment-booking");
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(httpExecutor.executeHttpPost(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(branchData);
        JsonObject responseData = qmsIntegrationService.fetchQMSBranchList(false, request, reqObj);
        JsonObject branchArray = responseData.get("branchList").getAsJsonArray().get(0).getAsJsonObject();
        assertEquals("105", branchArray.get("branchId").getAsString());
    }


    @Test
    void testServiceList() throws UnsupportedEncodingException {

        JsonArray serviceListArray = new JsonArray();
        JsonObject serviceListDetails = new JsonObject();
        serviceListDetails.addProperty("srvID", "79");
        serviceListArray.add(serviceListDetails);
        JsonObject serviceListData = new JsonObject();
        serviceListData.add("serviceList", serviceListArray);
        JsonObject reqObj = new JsonObject();
        reqObj.addProperty("type", "11");
        lenient().when(request.getResource()).thenReturn(resource);
        lenient().when(resource.getPath()).thenReturn("/content/techcombank/web/vn/en/appoinment-booking");
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(httpExecutor.executeHttpPost(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(serviceListData);
        JsonObject responseData = qmsIntegrationService.fetchQMSServiceList(false, request, reqObj);
        JsonObject serviceArray = responseData.get("serviceList").getAsJsonArray().get(0).getAsJsonObject();
        assertEquals("79", serviceArray.get("srvID").getAsString());
    }


    @Test
    void testBookingSubmit() throws UnsupportedEncodingException {
        ctx.currentResource("/content/jcr:content");
        Resource res = ctx.request().getResource();
        Optional<String> siteKeyValue = Optional.of("XXXXXX");
        JsonArray bookingSubmitArray = new JsonArray();
        JsonObject bookingSubmitDetails = new JsonObject();
        bookingSubmitDetails.addProperty("apmtQr", "830728");
        bookingSubmitDetails.addProperty("bkId", "16017");
        bookingSubmitArray.add(bookingSubmitDetails);
        JsonObject bookingSubmitData = new JsonObject();
        bookingSubmitData.add("bookingSubmit", bookingSubmitArray);
        JsonObject reqObj = new JsonObject();
        reqObj.addProperty("type", "11");
        reqObj.addProperty("iDType", "9");
        reqObj.addProperty("branchId", "79");
        reqObj.addProperty("bkDt", "2023-11-17");
        reqObj.addProperty("bkTm", "21:20");
        reqObj.addProperty("customerNm", "test");
        reqObj.addProperty("phneNb", "76545545");
        reqObj.addProperty("accno", "459439");
        reqObj.addProperty("emailAdr", "test@gmail.com");
        reqObj.addProperty("servid", "459439");
        reqObj.addProperty("servnm", "459439");
        lenient().when(request.getResource()).thenReturn(res);
        lenient().when(resource.getPath()).thenReturn("/content/techcombank/web/vn/en/appoinment-booking");
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(recaptchaService.getSiteKey(Mockito.any())).thenReturn(siteKeyValue);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(httpExecutor.executeHttpPost(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(bookingSubmitData);
        JsonObject responseData = qmsIntegrationService.qmsSubmitBooking(false, request, reqObj);
        JsonObject submitBooking = responseData.get("bookingSubmit").getAsJsonArray().get(0).getAsJsonObject();
        assertEquals("830728", submitBooking.get("apmtQr").getAsString());
        assertEquals("16017", submitBooking.get("bkId").getAsString());

    }
}
