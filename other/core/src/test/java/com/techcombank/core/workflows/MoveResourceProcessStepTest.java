package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Workspace;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.techcombank.core.constants.TCBConstants.VAR_FILTER;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class MoveResourceProcessStepTest {

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkItem workItem;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private Node destinationNode;

    @Mock
    private WorkflowData workflowData;

    @InjectMocks
    private MoveResourceProcessStep moveResourceProcessStep;
    @Mock
    private Resource sourceResource;

    @Mock
    private Resource destinationResource;

    @Mock
    private Node node;

    @Mock
    private Asset sourceAsset;

    @Mock
    private Replicator replicator;

    @Mock
    private ResourceResolver resolver;

    private AemContext context = TCBAemContext.newAemContext();

    @Mock
    private Session session;

    @Mock
    private Workspace workspace;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @Mock
    private Iterable<Resource> resItr;

    public static final String VARPATH = "/var/workflow/packages/generated-package12";

    @BeforeEach
    void setUp() {
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflowData().getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE);
        when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowSession.adaptTo(ResourceResolver.class)).thenReturn(resolver);
        when(resolver.getResource(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE)).thenReturn(sourceResource);
        session = mock(Session.class);
        when(resolver.adaptTo(Session.class)).thenReturn(session);
    }

    @Test
    void testExecuteWithValidPayloadAndDestination() throws Exception {
        when(resolver.getResource(TestConstants.DESTINATION_PATH)).thenReturn(destinationResource);
        when(destinationResource.adaptTo(Node.class)).thenReturn(node);
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn(TestConstants.DESTINATION_PATH);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }

    @Test
    void testExecuteWithValidPayloadAndDestinationAsset() throws Exception {
        ReplicationStatus replicationStatus = mock(ReplicationStatus.class);
        when(sourceResource.getResourceType()).thenReturn("dam:Asset");
        when(sourceResource.adaptTo(Asset.class)).thenReturn(sourceAsset);
        when(sourceResource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
        when(replicationStatus.isActivated()).thenReturn(true);
        when(sourceAsset.adaptTo(Node.class)).thenReturn(node);
        when(session.getWorkspace()).thenReturn(workspace);
        when(resolver.getResource(TestConstants.DESTINATION_PATH)).thenReturn(destinationResource);
        when(destinationResource.adaptTo(Node.class)).thenReturn(node);
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn(TestConstants.DESTINATION_PATH);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }

    @Test
    void testExecuteWithBulk() throws Exception {

        when(sourceResource.getPath()).thenReturn(VARPATH);
        when(resolver.getResource(VARPATH + VAR_FILTER)).thenReturn(resource);
        List<Resource> resourceList = new ArrayList<>();
        resourceList.add(resource);
        when(resource.getChildren()).thenReturn(resItr);
        Iterator<Resource> itrRes = resourceList.iterator();
        when(resItr.iterator()).thenReturn(itrRes);
        when(resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.get("root", String.class)).thenReturn("/content/techcombank");
        when(resolver.getResource(TestConstants.DESTINATION_PATH)).thenReturn(destinationResource);
        when(destinationResource.adaptTo(Node.class)).thenReturn(node);
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn(TestConstants.DESTINATION_PATH);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }

    @Test
    void testExecuteWithValidSourceEmpty() throws Exception {
        Resource sourceResource2 = null;
        lenient().when(resolver.getResource(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE))
                .thenReturn(sourceResource2);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }

    @Test
    void testExecuteWithValidDestinationEmpty() throws Exception {
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn("");
        destinationResource = null;
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }
    @Test
    void testExecuteWithInvalidDestination() throws Exception {
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn("invalid/destination/path");
        when(resolver.getResource("invalid/destination/path")).thenReturn(null);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(metaDataMap).put(TCBConstants.STATUS, TCBConstants.ERROR);
        verify(metaDataMap).put(TCBConstants.MESSAGE, "Invalid Target Path : invalid/destination/path");
    }
    @Test
    void testExecuteSourceAndDestinationSame() throws Exception {
        ReplicationStatus replicationStatus = mock(ReplicationStatus.class);
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).
                thenReturn(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE);
        when(resolver.getResource(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE)).thenReturn(destinationResource);
        when(destinationResource.adaptTo(Node.class)).thenReturn(destinationNode);
        when(destinationResource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
        when(destinationResource.getName()).thenReturn(TestConstants.NODE);
        lenient().when(destinationNode.hasNode(TestConstants.NODE)).thenReturn(true);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }

    @Test
    void testExecuteException() throws Exception {
        ReplicationStatus replicationStatus = mock(ReplicationStatus.class);
        when(metaDataMap.get(TCBConstants.TARGET_PATH, String.class)).
                thenReturn(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE);
        when(resolver.getResource(TestConstants.CONTENT.DOWNLOAD_CONTENT_BLOCK_PAGE)).thenReturn(destinationResource);
        when(destinationResource.adaptTo(Node.class)).thenReturn(destinationNode);
        when(destinationResource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
        when(destinationResource.getName()).thenReturn(TestConstants.NODE);
        lenient().when(destinationNode.hasNode(TestConstants.NODE)).thenReturn(true);
        lenient().when(destinationNode.hasNode(TestConstants.NODE)).thenThrow(RepositoryException.class);
        moveResourceProcessStep.execute(workItem, workflowSession, metaDataMap);
        assertNotNull(moveResourceProcessStep);
    }
}
