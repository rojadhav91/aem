package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MastheadExtendModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private MastheadExtendModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(MastheadExtendModel.class);
        aemContext.load().json(TestConstants.MASTHEADEXTEND, "/mastheadExtend");
        aemContext.currentResource(TestConstants.MASTHEADEXTENDCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(MastheadExtendModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getDescription() {
        final String expected = "text";
        assertEquals(expected, model.getDescription());
    }

    @Test
    void getBannerTopBackgroundColor() {
        final String expected = "color-code";
        assertEquals(expected, model.getBannerTopBackgroundColor());
    }

    @Test
    void getBannerBottomBackgroundColor() {
        final String expected = "color-code";
        assertEquals(expected, model.getBannerBottomBackgroundColor());
    }

    @Test
    void isApplyGradientOverBackgroundColor() {
        final boolean expected = true;
        assertEquals(expected, model.isApplyGradientOverBackgroundColor());
    }

    @Test
    void isCtaButtonRequired() {
        final boolean expected = true;
        assertEquals(expected, model.isCtaButtonRequired());
    }

    @Test
    void getPhoneScreenshotImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getPhoneScreenshotImage());
    }

    @Test
    void getPhoneScreenshotImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getPhoneScreenshotImageAltText());
    }

    @Test
    void getMastheadExtendImageList() {
        assert model.getMastheadExtendImageList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getMastheadExtendImageList().size());
    }

    @Test
    void getMastheadExtendCardArticleImageList() {
        assert model.getMastheadExtendCardArticleImageList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getMastheadExtendCardArticleImageList().size());
    }

    @Test
    void getMobilePhoneScreenShotImagePath() {
        String expected = "/content/dam/techcombank/images/test/test.png/"
                + "jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, model.getMobilePhoneScreenShotImagePath());
    }

    @Test
    void getWebPhoneScreenShotImagePath() {
        String expected = "/content/dam/techcombank/images/test/test.png/"
                + "jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, model.getWebPhoneScreenShotImagePath());
    }
}
