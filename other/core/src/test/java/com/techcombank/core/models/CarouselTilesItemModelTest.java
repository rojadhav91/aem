package com.techcombank.core.models;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CarouselTilesItemModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private CarouselTilesItemModel model;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.CAROUSELTILESITEM, "/carouselTilesItem");
        ctx.addModelsForClasses(CarouselTilesItemModel.class);
        model = getModel(TestConstants.CAROUSELTILESITEMCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private CarouselTilesItemModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(CarouselTilesItemModel.class);
    }

    private void mockObjects() {

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);
    }

    @Test
    void getCardImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCardImage());
    }

    @Test
    void getCardImageType() {
        final String expected = "background";
        assertEquals(expected, model.getCardImageType());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getCardImage() + TCBConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCardImagePath());
        assertEquals(model.getCardImage() + TCBConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCardImagePath());
    }

    @Test
    void getCardImageAltText() {
        final String expected = "a";
        assertEquals(expected, model.getCardImageAltText());
    }

    @Test
    void getTheme() {
        final String expected = "dark";
        assertEquals(expected, model.getTheme());
    }

    @Test
    void getNudgeText() {
        final String expected = "a";
        assertEquals(expected, model.getNudgeText());
    }

    @Test
    void getCardTitle() {
        final String expected = "a";
        assertEquals(expected, model.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "a";
        assertEquals(expected, model.getCardDescription());
    }

    @Test
    void getLinkText() {
        final String expected = "a";
        assertEquals(expected, model.getLinkText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getLinkUrl());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = false;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = true;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit",
            model.getWebInteractionType());
    }

}
