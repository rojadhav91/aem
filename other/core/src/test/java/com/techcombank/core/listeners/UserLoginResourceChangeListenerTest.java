package com.techcombank.core.listeners;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.TestConstants;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class UserLoginResourceChangeListenerTest {

    private final AemContext ctx = new AemContext();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceChange resourceChange;

    @Mock
    private Resource resource;

    @Mock
    private Group group;

    @Mock
    private Authorizable authorizable;

    @Mock
    private UserManager userManager;

    @Mock
    private User user;

    @Mock
    private ResourceResolverService resourceResolverService;

    @InjectMocks
    private UserLoginResourceChangeListener userLoginResourceChangeListener = new UserLoginResourceChangeListener();

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.registerService(ResourceResolverService.class, resourceResolverService);
        ctx.registerInjectActivateService(userLoginResourceChangeListener);
        ctx.addModelsForClasses(UserLoginResourceChangeListener.class);
    }

    @Test
    void testonChange() throws UnsupportedEncodingException, RepositoryException {

        List<ResourceChange> resList = new ArrayList<>();
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);

        lenient().when(resourceChange.getType()).thenReturn(ResourceChange.ChangeType.ADDED);
        userLoginResourceChangeListener.onChange(resList);
        resList.add(resourceChange);
        lenient().when(resourceChange.getPath()).thenReturn("/home/users/.tokens/");
        userLoginResourceChangeListener.onChange(resList);
        lenient().when(resourceResolver.getResource(any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Group.class)).thenReturn(group);
        lenient().when(group.isMember(authorizable)).thenReturn(true);
        lenient().when(resourceResolver.adaptTo(UserManager.class)).thenReturn(userManager);
        lenient().when(authorizable.getID()).thenReturn("admin");
        lenient().when((User) userManager.getAuthorizable(Mockito.anyString())).thenReturn(user);
        userLoginResourceChangeListener.onChange(resList);
        lenient().when(resource.adaptTo(Authorizable.class)).thenReturn(authorizable);
        userLoginResourceChangeListener.onChange(resList);
        lenient().when(group.isMember(authorizable)).thenReturn(false);
        userLoginResourceChangeListener.onChange(resList);
        lenient().when(group.isMember(authorizable)).thenThrow(RepositoryException.class);
        userLoginResourceChangeListener.onChange(resList);

        verify(resourceChange, times(TestConstants.CALL_COUNT_15)).getType();

    }
}
