package com.techcombank.core.models;

import com.day.cq.wcm.api.WCMMode;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })

class ArticleAndEventsModelTest {
    private final AemContext ctx = new AemContext();
    private ArticleAndEventsModel eventsModel;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json("/content/techcombank/web/vn/en/articleandevents.json", "/content");
        ctx.addModelsForClasses(ArticleAndEventsModel.class);
        eventsModel = getModel("/content/jcr:content/root/articleandevents");
    }

    @Test
    void articleImagePathTest() {
        final String expected = "/content/dam/techcombank/asset.jpg";
        assertEquals(expected, eventsModel.getArticleImagePath());
        assertEquals(expected + TestConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH,
                eventsModel.getArticleMobileImagePath());
        assertEquals(expected + TestConstants.JPEG_WEB_IMAGE_RENDITION_PATH,
                eventsModel.getArticleWebImagePath());
    }

    @Test
    void articleTitleTest() {
        final String expected = "Page Title goes here";
        assertEquals(expected, eventsModel.getArticleTitle());
    }

    @Test
    void articleImageAltTextTest() {
        final String expected = "Image Alt Text";
        assertEquals(expected, eventsModel.getArticleImageAltText());
    }

    @Test
    void articleCreationDateTest() {
        final String expected = "10/06/2023";
        assertEquals(expected, eventsModel.getArticleCreationDate());
    }

    @Test
    void articleDescriptionTest() {
        final String expected = "article description article description article description article";
        assertEquals(expected, eventsModel.getArticleDescription());
    }

    private ArticleAndEventsModel getModel(final String currentResource) {
        ctx.request().setAttribute(WCMMode.class.getName(), WCMMode.EDIT);
        ctx.currentResource(currentResource);
        ArticleAndEventsModel eventModel = ctx.request().adaptTo(ArticleAndEventsModel.class);
        return eventModel;
    }
}
