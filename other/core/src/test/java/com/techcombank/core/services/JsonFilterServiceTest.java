package com.techcombank.core.services;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techcombank.core.service.impl.JsonFilterServiceImpl;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.sling.settings.SlingSettingsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class JsonFilterServiceTest {

    @InjectMocks
    private JsonFilterServiceImpl service = new JsonFilterServiceImpl();
    private JsonFilterServiceImpl.Config config;

    @Mock
    private SlingSettingsService slingSettings;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeEach
    public void setup() throws IOException {
        this.config = Mockito.mock(JsonFilterServiceImpl.Config.class);
        lenient().when(config.getJsonFilter()).thenReturn(TestConstants.FILTER_KEY_ARRAY);
        lenient().when(config.getAuthorizationToken()).thenReturn(TestConstants.TOKEN);
        service.activate(config);
    }

    @Test
    void testProcessJSON() throws IOException {
        Gson gsonObj = new Gson();
        JsonElement element = gsonObj.fromJson(TestConstants.JSON_DATA, JsonElement.class);
        JsonElement element1 = service.processJSON(element, TestConstants.FILTER_KEY_ARRAY, TestConstants.APP_DOMAIN,
                "");
        JsonObject obj = element1.getAsJsonObject();
        assertFalse(obj.has(TestConstants.FILTER_KEY));

        String jsonData = "{'item1':{'value':'/content/dam/techcombank','item2':'2','item3':'3'}}";

        element = gsonObj.fromJson(jsonData, JsonElement.class);
        JsonElement element2 = service.processJSON(element, TestConstants.FILTER_KEY_ARRAY, TestConstants.APP_DOMAIN,
                "");
        obj = element2.getAsJsonObject();
        assertFalse(obj.has(TestConstants.FILTER_KEY));

        jsonData = "{'item1':{'value':['1','2','3'],'item2':'2',':type':'content-fragment'}}";
        element = gsonObj.fromJson(jsonData, JsonElement.class);
        Set<String> runModes = new HashSet<String>();
        runModes.add("author");
        lenient().when(slingSettings.getRunModes()).thenReturn(runModes);
        JsonElement element3 = service.processJSON(element, TestConstants.FILTER_KEY_ARRAY, TestConstants.APP_DOMAIN,
                "http://localhost:4502/");
        obj = element3.getAsJsonObject();
        assertFalse(obj.has(TestConstants.FILTER_KEY));
    }

    @Test
    void testProcessJSONArray() throws IOException {
        Gson gsonObj = new Gson();
        JsonElement element = gsonObj.fromJson(TestConstants.JSON_ARRAY_DATA, JsonElement.class);
        JsonElement element1 = service.processJSON(element,
                TestConstants.FILTER_KEY_ARRAY, TestConstants.APP_DOMAIN, "");
        JsonArray obj = element1.getAsJsonArray();
        assertTrue(obj.isJsonArray());
    }

    @Test
    void testJsonFilter() {
        assertEquals(service.getJsonFilter()[0], TestConstants.FILTER_KEY_ARRAY[0]);
    }

    @Test
    void testAuthorizationToken() {
        assertEquals(TestConstants.TOKEN, service.getAuthorizationToken());
    }

    @Test
    void testRemoveUnwantedKeysFromMap() {
        try {
            Method method = JsonFilterServiceImpl.class.getDeclaredMethod("removeUnwantedKeysFromMap", Map.class,
                    String[].class);
            method.setAccessible(true);
            Map<String, Object> map = new HashMap<>();
            map.put("@", "item1");
            map.put("jcr:", "jcr");
            map.put("item2", "item2");
            map.put("item3", "item3");
            Map<String, Object> map2 = new HashMap<>();
            map2.put("item1", "item1");
            map.put("item1", map2);
            method.invoke(service, map, TestConstants.FILTER_KEY_ARRAY);
            assertFalse(map.containsKey(TestConstants.FILTER_KEY));
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                 | SecurityException e) {
            logger.error("Exception {} {}", e.getMessage(), e);
        }
    }

    @Test
    void testConvertRelToAbsPath() {
        try {
            Method method = JsonFilterServiceImpl.class.getDeclaredMethod("convertRelToAbsPath", String.class,
                    Map.class);
            method.setAccessible(true);
            Map<String, Object> map = new HashMap<>();
            map.put("item3", "/content/dam/techcombank");
            Map<String, Object> map2 = new HashMap<>();
            map2.put("item4", "/content/techcombank");
            map.put("item1", map2);
            method.invoke(service, "localhost", map);
            assertEquals("localhost/content/dam/techcombank", map.get("item3"));
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                 | SecurityException e) {
            logger.error("Exception {} {}", e.getMessage(), e);
        }
    }

}
