
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.FixingRateModelImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class FixingRateModelTest to perform junit for FixingRateModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class FixingRateModelTest {

    private final AemContext ctx = new AemContext();

    private FixingRateModelImpl exchangeRateModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.FIXING_RATE_CMP_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(FixingRateModelImpl.class);
        exchangeRateModel = getModel("/content/jcr:content/root/container/fixingrate");

    }

    private FixingRateModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        FixingRateModelImpl fixingRate = res.adaptTo(FixingRateModelImpl.class);
        return fixingRate;
    }

    @Test
    void testHeaderSection() {
        assertEquals("Fixing rate", exchangeRateModel.getFixingRateTitle());
        assertEquals("Day", exchangeRateModel.getDayLabel());
        assertEquals("Updated time", exchangeRateModel.getTimeLabel());
        assertEquals("Currency pair", exchangeRateModel.getCurrencyLabel());
        assertEquals("Fixing rate", exchangeRateModel.getColOneHeading());
        assertEquals("Update time", exchangeRateModel.getColTwoHeading());
        assertEquals("Borrow VND*", exchangeRateModel.getColSubHeading());
    }

    @Test
    void testIcon() {
        assertEquals("/content/dam/techcombank/calendar.svg", exchangeRateModel.getDayIcon());
        assertEquals("DayAlt", exchangeRateModel.getDayIconAlt());
        assertEquals("/content/dam/techcombank/calendar.svg", exchangeRateModel.getDayIconWebImagePath());
        assertEquals("/content/dam/techcombank/calendar.svg", exchangeRateModel.getDayIconMobileImagePath());
        assertEquals("/content/dam/techcombank/clock.svg", exchangeRateModel.getTimeIcon());
        assertEquals("timeAlt", exchangeRateModel.getTimeIconAlt());
        assertEquals("/content/dam/techcombank/clock.svg", exchangeRateModel.getTimeIconWebImagePath());
        assertEquals("/content/dam/techcombank/clock.svg", exchangeRateModel.getTimeIconMobileImagePath());
    }
}
