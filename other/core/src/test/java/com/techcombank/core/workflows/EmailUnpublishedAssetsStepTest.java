package com.techcombank.core.workflows;

import com.adobe.acs.commons.notifications.InboxNotification;
import com.adobe.acs.commons.notifications.InboxNotificationSender;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class EmailUnpublishedAssetsStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private Workflow workflow;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @Mock
    private InboxNotificationSender inboxNotificationSender;

    @Mock
    private InboxNotification inboxNotification;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private EmailUnpublishedAssetsStep emailUnpublishedAssetsStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workItem.getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn("/content/sample-page");
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class)).
                thenReturn("approver=unpublish,title=Sample,subject=Sample");

        when(workflow.getInitiator()).thenReturn("initiator@example.com");
        when(workflowMetadataMap.get(TCBConstants.FIRST_APPORVER, String.class)).thenReturn("First Approver");
        when(workflowMetadataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("sample@example.com");
        when(workflowMetadataMap.containsKey(TCBConstants.EMAIL_ENABLE)).thenReturn(true);
        when(workflowMetadataMap.containsKey(TCBConstants.WORKFLOW_TITLE)).thenReturn(true);
        when(workflowMetadataMap.get(TCBConstants.WORKFLOW_TITLE, String.class)).thenReturn("Email Unpublish Process");
        when(workflowMetadataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(workflowMetadataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("sample@example.com");
        when(inboxNotificationSender.buildInboxNotification()).thenReturn(inboxNotification);
        when(inboxNotification.getTitle()).thenReturn("Approval Title");
        when(metaDataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);

        String[] assetPathArr = {"/content/assets/image1.jpg", "/content/assets/image2.jpg"};
        when(workflowMetadataMap.get(TCBConstants.ASSET_REFERENCE, String[].class))
                .thenReturn(assetPathArr);
    }

    @Test
    void testExecuteMailEnabled() throws WorkflowException {
        emailUnpublishedAssetsStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_2)).getWorkflowData();
    }

}

