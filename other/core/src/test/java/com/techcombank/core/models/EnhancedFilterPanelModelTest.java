package com.techcombank.core.models;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Session;

/**
 * The Class EnhancedFilterPanelModelTest to perform junit for EnhancedFilterPanelModel
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class EnhancedFilterPanelModelTest {

    private final AemContext ctx = new AemContext();

    private EnhancedFilterPanelModel enhancedFilterPanel;

    private ResourceResolver resourceResolver;

    private Session session;

    private QueryBuilder queryBuilder;

    private Query query;

    private TagManager tagManager;

    private SearchResult searchResult;

    private Hit hit;

    private Tag tag;

    private Resource resource;

    private Node node;

    private Property property;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.ENHANCED_FILTER_PANEL_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(EnhancedFilterPanelModel.class);
        mockObjects();
        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        lenient().when(queryBuilder.createQuery(Mockito.any(), Mockito.any())).thenReturn(query);
        lenient().when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
        lenient().when(query.getResult()).thenReturn(searchResult);
        List<Hit> hits = new ArrayList<>();
        hits.add(hit);
        lenient().when(searchResult.getHits()).thenReturn(hits);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Node.class)).thenReturn(node);
        lenient().when(node.hasProperty(TCBConstants.CQ_FILTERPANELTAG)).thenReturn(true);
        lenient().when(node.getProperty(TCBConstants.CQ_FILTERPANELTAG)).thenReturn(property);
        lenient().when(property.getString()).thenReturn("enhanced-filter-panel");
        lenient().when(tagManager.resolve(Mockito.any())).thenReturn(tag);
        lenient().when(tag.getTitle()).thenReturn("EnhancedFilterPanel");
        enhancedFilterPanel = getModel("/content/jcr:content/root/container/enhancedfilterpanel");
    }

    private EnhancedFilterPanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        EnhancedFilterPanelModel filterPanel = res.adaptTo(EnhancedFilterPanelModel.class);
        return filterPanel;
    }

    private void mockObjects() {
        session = mock(Session.class);
        ctx.registerService(Session.class, session);
        queryBuilder = mock(QueryBuilder.class);
        ctx.registerService(QueryBuilder.class, queryBuilder);
        query = mock(Query.class);
        ctx.registerService(Query.class, query);
        tagManager = mock(TagManager.class);
        ctx.registerService(TagManager.class, tagManager);
        searchResult = mock(SearchResult.class);
        ctx.registerService(SearchResult.class, searchResult);
        hit = mock(Hit.class);
        ctx.registerService(Hit.class, hit);
        tag = mock(Tag.class);
        ctx.registerService(Tag.class, tag);
        node = mock(Node.class);
        ctx.registerService(Node.class, node);
        property = mock(Property.class);
        ctx.registerService(Property.class, property);
        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);
    }

    @Test
    void testEnhancedFilterPanelFields() {
        assertEquals("Category", enhancedFilterPanel.getCategoryLabel());
        assertEquals("View all", enhancedFilterPanel.getViewAll());
        assertEquals("EnhancedFilterPanel", enhancedFilterPanel.getTags().iterator().next());
        assertEquals("/content/experience-fragments/techcombank/web/vn/en/footer/footer",
                enhancedFilterPanel.getExpFragments().get(0).getPath());

    }

}
