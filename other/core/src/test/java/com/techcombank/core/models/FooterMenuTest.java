package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class FooterMenuTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private FooterMenu footerMenu;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(ModalModel.class);
        aemContext.load().json(TestConstants.FOOTER_MENU, "/footermenu");
        aemContext.currentResource(TestConstants.FOOTERMENUCOMP);
        Resource resource = aemContext.request().getResource();
        footerMenu = resource.adaptTo(FooterMenu.class);
    }

    @Test
    void getFooterMenuHeader() {
        final String expected = "Footer Menu Header";
        assertEquals(expected, footerMenu.getFooterMenuHeader());
        assertNull(footerMenu.getSubMenuList());
    }

    @Test
    void getFooterMenuUrl() {
        final String expected = "/content/techcombank/web/vn/en/footer.html";
        assertEquals(expected, footerMenu.getFooterMenuUrl());
    }

    @Test
    void isFooterMenuUrlExternal() {
        final String expected = "other";
        assertEquals(expected, footerMenu.getIsFooterMenuUrlExternal());
    }

    @Test
    void getOpenInNewTab() {
        final String expected = "true";
        assertEquals(expected, footerMenu.getOpenInNewTab());
    }

}
