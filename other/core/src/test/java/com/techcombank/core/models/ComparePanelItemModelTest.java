package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ComparePanelItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ComparePanelItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ComparePanelItemModel.class);
        aemContext.load().json(TestConstants.COMPAREPANELITEM, "/comparePanelItem");
        aemContext.currentResource(TestConstants.COMPAREPANELITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ComparePanelItemModel.class);
    }

    @Test
    void getProjectName() {
        final String expected = "text-value";
        assertEquals(expected, model.getProjectName());
    }
    @Test
    void getLoanRateOnCapitalNeed() {
        final String expected = "text-value";
        assertEquals(expected, model.getLoanRateOnCapitalNeed());
    }
    @Test
    void getLoanRateOnCollateralValue() {
        final String expected = "text-value";
        assertEquals(expected, model.getLoanRateOnCollateralValue());
    }
    @Test
    void getLoanTerm() {
        final String expected = "text-value";
        assertEquals(expected, model.getLoanTerm());
    }
    @Test
    void getCollateral() {
        final String expected = "text-value";
        assertEquals(expected, model.getCollateral());
    }
    @Test
    void getTag() {
        final String expected = "text-value";
        assertEquals(expected, model.getTag());
    }
}
