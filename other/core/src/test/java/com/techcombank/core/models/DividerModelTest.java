package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class DividerModelTest.
 */
@ExtendWith(AemContextExtension.class)
class DividerModelTest {

    /**
     * The resource.
     */
    @Mock
    private Resource resource;

    /**
     * The sling model.
     */
    @InjectMocks
    private DividerModel slingModel;

    /**
     * The context.
     */
    private final AemContext context = AppAemContext.newAemContext();

    /**
     * The page.
     */
    private Page page;

    /**
     * Sets the up.
     */
    @BeforeEach
    void setUp() {
        page = context.create().page(TestConstants.MYPAGE);
        resource = context.create().resource(page, TestConstants.DIVIDER, TestConstants.SLING_RESOURCETYPE,
                TestConstants.DIVIDER_RESOURCE);
        slingModel = resource.adaptTo(DividerModel.class);
        slingModel.setDividerStyle("Dotted");
        slingModel.setLabelText("Hello");
    }

    /**
     * Test sling model.
     */
    @Test
    void testSlingModel() {
        /* Test the Divider style */
        String dividerStyle = slingModel.getDividerStyle();
        assertEquals("Dotted", dividerStyle);
    }

    @Test
    void testLabelText() {
        String labelText = slingModel.getLabelText();
        assertEquals("Hello", labelText);
    }
}
