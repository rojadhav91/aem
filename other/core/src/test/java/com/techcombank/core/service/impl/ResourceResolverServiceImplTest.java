package com.techcombank.core.service.impl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static junit.framework.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ResourceResolverServiceImplTest {

    private final AemContext ctx = new AemContext();
    @InjectMocks
    private ResourceResolverServiceImpl resourceResolverService = new ResourceResolverServiceImpl();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;



    @BeforeEach
    void setUp() throws Exception {
        resourceResolver = ctx.resourceResolver();
        lenient().when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).thenReturn(resourceResolver);
    }

    @Test
    void testGetReadResourceResolver() throws LoginException {
        assertNotNull(resourceResolverService.getReadResourceResolver());
       }

    @Test
    void testGetWriteResourceResolver() throws LoginException {
        assertNotNull(resourceResolverService.getWriteResourceResolver());
        }

    @Test
    void testCatch() throws LoginException {
        lenient().when(resourceResolverFactory.getServiceResourceResolver(Mockito.any())).
                thenThrow(LoginException.class);
        assertNull(resourceResolverService.getReadResourceResolver());
        assertNull(resourceResolverService.getWriteResourceResolver());
    }
}
