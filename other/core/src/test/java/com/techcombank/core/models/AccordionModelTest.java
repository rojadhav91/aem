package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class AccordionModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private AccordionModel accordionModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(AccordionModel.class);
        aemContext.load().json(TestConstants.ACCORDION, "/accordion");
        aemContext.currentResource(TestConstants.ACCORDIONCOMP);
        Resource resource = aemContext.request().getResource();
        accordionModel = resource.adaptTo(AccordionModel.class);
    }

    @Test
    void testIconImages() {
        assertEquals(TestConstants.LIST_SIZE_THREE, accordionModel.getIconImages().size());
        assertEquals("/content/dam/techcombank/images/test/test.png",
                accordionModel.getIconImages().get(0).getIconSrcOn());
        assertEquals("/content/dam/techcombank/images/test/test.png",
                accordionModel.getIconImages().get(0).getIconSrcOff());
    }

}
