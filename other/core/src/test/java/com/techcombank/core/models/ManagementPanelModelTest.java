package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.ManagementPanel;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ManagementPanelModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();

    private ManagementPanelModel managementPanelModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ManagementPanelModel.class);
        aemContext.load().json(TestConstants.MANAGEMENTPANEL, "/management-panel");
        aemContext.currentResource(TestConstants.MANAGEMENT);
        Resource resource = aemContext.request().getResource();
        managementPanelModel = resource.adaptTo(ManagementPanelModel.class);
    }

    @Test
    void getPanelBgImage() {
        String expected = "/content/dam/techcombank/managemnet-panel/Management_Team_"
                + "Panel_bg_dialog_9a9aa14ba4.png/jcr:content/renditions/cq5dam.web.1280.1280.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, managementPanelModel.getPanelBgImage());
    }

    @Test
    void getCardText() {
        String expected = "Read more";
        assertEquals(expected, managementPanelModel.getCardText());
    }

    @Test
    void getTabText() {
        String expected = "view more";
        assertEquals(expected, managementPanelModel.getViewMore());
    }

    @Test
    void getProfileMultiField() {
        List<ManagementPanel> managmentPanelList = managementPanelModel.getProfileItem();
        String expecedDescription =
                "Graduated from Electronics Engineering in Russia,"
                        + " Mr. Ho Hung Anh joined the executive management of the";
        String expecedprofileImageLarge =
                "/content/dam/techcombank/managemnet-panel"
                        + "/Mr-Bui-Thi-Hong-Mai-mb-01c9b74677-07dcb8b358.jpg/jcr:content/renditions"
                        + "/cq5dam.web.1280.1280.jpeg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
        String expectedprofileImageSmall =
                "/content/dam/techcombank/managemnet-panel"
                        + "/Mr-Bui-Thi-Hong-Mai-mb-01c9b74677-07dcb8b358.jpg";
        String expectedDesignation = "CHAIRMAN";
        String expectedName = "Mr. Ho Hung Anh";
        String expectedImage = "large image";
        String expectedAlt = "small imgae";
        assertEquals(expecedDescription, managmentPanelList.get(0).getDescription());
        assertEquals(expecedprofileImageLarge, managmentPanelList.get(0).getProfileImage());
        assertEquals(expectedDesignation, managmentPanelList.get(0).getDesignation());
        assertEquals(expectedName, managmentPanelList.get(0).getName());
        assertEquals(expectedImage, managmentPanelList.get(0).getAlt());
    }

    @Test
    void getPanelImageMobile() {
        String expected = "/content/dam/techcombank/managemnet-panel/Management_Team_"
                + "Panel_bg_dialog_9a9aa14ba4.png/jcr:content/renditions/cq5dam.web.1280.1280.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, managementPanelModel.getPanelImageMobile());
    }

    @Test
    void getPanelBgMobile() {
        String expected = "/content/dam/techcombank/managemnet-panel/Management_Team_"
                + "Panel_bg_dialog_9a9aa14ba4.png/jcr:content/renditions/"
                + "cq5dam.web.640.640.png/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, managementPanelModel.getPanelBgMobile());
    }
}
