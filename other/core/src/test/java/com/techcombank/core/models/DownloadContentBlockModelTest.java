package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class DownloadContentBlockModelTest {

    private DownloadContentBlockModel downloadContentBlockModel;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setup() throws NoSuchFieldException {
        MockitoAnnotations.openMocks(this);
        downloadContentBlockModel = new DownloadContentBlockModel();
        PrivateAccessor.setField(downloadContentBlockModel, "imagePosition", "left");
        PrivateAccessor.setField(downloadContentBlockModel, "imageAlignment", "right");
        PrivateAccessor.setField(downloadContentBlockModel, "imageAltText", "Test");
        PrivateAccessor.setField(downloadContentBlockModel, "imagePath", "/content/dam/techcombank/images/content-block-download/frame1.png");
        PrivateAccessor.setField(downloadContentBlockModel, "title", "Start your experience journey at Techcombank");
        PrivateAccessor.setField(downloadContentBlockModel, "description", "Get an account");
        PrivateAccessor.setField(downloadContentBlockModel, "descriptionPosition", "Top");
        PrivateAccessor.setField(downloadContentBlockModel, "showScanner", "true");
        PrivateAccessor.setField(downloadContentBlockModel, "scannerText", "Download Techcombank Mobile app");
        PrivateAccessor.setField(downloadContentBlockModel, "scannerImage", "/content/dam/techcombank/images/content-block-download/scanner.png");
        PrivateAccessor.setField(downloadContentBlockModel, "scannerLink", "/content/techcombank/web/vn/vi/personal-banking-page");
        PrivateAccessor.setField(downloadContentBlockModel, "buttonText", "Button Text");
        PrivateAccessor.setField(downloadContentBlockModel, "buttonLink", "/content/techcombank/web/vn/vi/personal-banking-page");
        PrivateAccessor.setField(downloadContentBlockModel, "buttonFullWidth", "true");
        PrivateAccessor.setField(downloadContentBlockModel, "openInNewTabButton", "_blank");
        PrivateAccessor.setField(downloadContentBlockModel, "noFollowButton", "nofollow");
        PrivateAccessor.setField(downloadContentBlockModel, "linkText", "Find out more about Techcombank account");
        PrivateAccessor.setField(downloadContentBlockModel, "linkUrl", "/content/techcombank/web/vn/vi/personal-banking-page");
        PrivateAccessor.setField(downloadContentBlockModel, "openInNewTabLink", "_blank");
        PrivateAccessor.setField(downloadContentBlockModel, "noFollowLink", "nofollow");
        PrivateAccessor.setField(downloadContentBlockModel, "id", "modalId");
        PrivateAccessor.setField(downloadContentBlockModel, "scannerLinkWebInteractionValue", "{'webInteractions': {'name': 'Download content block','type': 'exit'}}");
        PrivateAccessor.setField(downloadContentBlockModel, "linkUrlWebInteractionValue", "{'webInteractions': {'name': 'Download content block','type': 'other'}}");

        PrivateAccessor.setField(downloadContentBlockModel, "resourceResolverService", resourceResolverService);
    }

    @Test
    void testInit() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
        final String expected = "init";
        Method initMethod = DownloadContentBlockModel.class.getDeclaredMethod("init");
        initMethod.setAccessible(true);
        initMethod.invoke(downloadContentBlockModel);
        assertEquals("init", expected);
    }

    @Test
    void testImagePosition() {
        assertEquals("left", downloadContentBlockModel.getImagePosition());
    }

    @Test
    void testImageAlignment() {
        assertEquals("right", downloadContentBlockModel.getImageAlignment());
    }

    @Test
    void testImageAltText() {
        assertEquals("Test", downloadContentBlockModel.getImageAltText());
    }


    @Test
    void testImagePath() {
        assertEquals("/content/dam/techcombank/images/content-block-download/frame1.png",
                downloadContentBlockModel.getImagePath());
    }

    @Test
    void testTitle() {
        assertEquals("Start your experience journey at Techcombank", downloadContentBlockModel.getTitle());
    }

    @Test
    void testDescription() {
        assertEquals("Get an account", downloadContentBlockModel.getDescription());
    }

    @Test
    void testDescriptionPosition() {
        assertEquals("Top", downloadContentBlockModel.getDescriptionPosition());
    }

    @Test
    void testShowScanner() {
        assertEquals("true", downloadContentBlockModel.getShowScanner());
    }

    @Test
    void testScannerText() {
        assertEquals("Download Techcombank Mobile app", downloadContentBlockModel.getScannerText());
    }

    @Test
    void testScannerLink() {
        assertEquals("/content/techcombank/web/vn/vi/personal-banking-page",
                downloadContentBlockModel.getScannerLink());
    }
    @Test
    void testScannerImage() {
        assertEquals("/content/dam/techcombank/images/content-block-download/scanner.png",
                downloadContentBlockModel.getScannerImage());
    }

    @Test
    void testButtonText() {
        assertEquals("Button Text", downloadContentBlockModel.getButtonText());
    }

    @Test
    void testButtonLink() {
        assertEquals("/content/techcombank/web/vn/vi/personal-banking-page", downloadContentBlockModel.getButtonLink());
    }

    @Test
    void testButtonFullWidth() {
        assertEquals("true", downloadContentBlockModel.getButtonFullWidth());
    }


    @Test
    void testNoFollowButton() {
        assertEquals("nofollow", downloadContentBlockModel.getNoFollowButton());
    }

    @Test
    void testOpenInNewTabButton() {
        assertEquals("_blank", downloadContentBlockModel.getOpenInNewTabButton());
    }


    @Test
    void testLinkText() {
        assertEquals("Find out more about Techcombank account", downloadContentBlockModel.getLinkText());
    }

    @Test
    void testLinkUrl() {
        assertEquals("/content/techcombank/web/vn/vi/personal-banking-page", downloadContentBlockModel.getLinkUrl());
    }

    @Test
    void testOpenInNewTabLink() {
        assertEquals("_blank", downloadContentBlockModel.getOpenInNewTabLink());
    }

    @Test
    void testNoFollowLink() {
        assertEquals("nofollow", downloadContentBlockModel.getNoFollowLink());
    }

    @Test
    void testId() {
        assertEquals("modalId", downloadContentBlockModel.getId());
    }

    @Test
    void testImageOptimizationPath() {
        String basepath = "/content/dam/techcombank/images/content-block-download";
        assertEquals(basepath + "/scanner.png" + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                downloadContentBlockModel.getScannerWebImagePath());
        assertEquals(basepath + "/scanner.png" + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                downloadContentBlockModel.getScannerMobileImagePath());

        assertEquals(basepath + "/frame1.png" + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                downloadContentBlockModel.getWebImagePath());
        assertEquals(basepath + "/frame1.png" + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                downloadContentBlockModel.getMobileImagePath());
    }

    @Test
    void testScannerLinkWebInteractionValue() {
        assertEquals("{'webInteractions': {'name': 'Download content block','type': 'exit'}}",
                downloadContentBlockModel.getScannerLinkWebInteractionValue());
    }

    @Test
    void testLinkUrlWebInteractionValue() {
        assertEquals("{'webInteractions': {'name': 'Download content block','type': 'other'}}",
                downloadContentBlockModel.getLinkUrlWebInteractionValue());
    }

}
