package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.TestConstants;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ApprovedEmailProcessStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private Workflow workflow;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private WorkflowProcess workflowProcess;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private ApprovedEmailProcessStep approvedEmailProcessStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn("/content/sample/page");
        when(metaDataMap.containsKey(TCBConstants.EMAIL_ENABLE)).thenReturn(true);
        when(metaDataMap.containsKey(TCBConstants.WORKFLOW_TITLE)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.WORKFLOW_TITLE, String.class)).thenReturn("Email Approval Process");
        when(metaDataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("sample@example.com");
        when(metaDataMap.get(TCBConstants.FIRST_APPORVER, String.class)).thenReturn("First Approver");
        when(workflow.getInitiator()).thenReturn("initiator@example.com");
    }

    @Test
    void testExecuteEmailEnabled() throws WorkflowException {
        when(metaDataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class)).
                thenReturn("FIRST_APPROVER=firstApprover,title=Sample,subject=Sample");
        approvedEmailProcessStep.execute(workItem, workflowSession, metaDataMap);
        when(metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class)).
                thenReturn("SECOND_APPROVER=secondApprover,title=Sample,subject=Sample");
        approvedEmailProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_4)).getWorkflowData();
    }
}
