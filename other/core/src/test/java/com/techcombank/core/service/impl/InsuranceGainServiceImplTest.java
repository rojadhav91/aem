package com.techcombank.core.service.impl;
import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class InsuranceGainServiceImplTest {

    @Mock
    private ResourceResolver resolver;

    @Mock
    private PageManager pageManager;

    @Mock
    private Resource resource;

    @Mock
    private Iterator<Resource> resourceIterator;

    @Mock
    private Resource pages;

    @Mock
    private Resource pageResource;

    @Mock
    private Page gainPage;

    @Mock
    private GenericList flagList;

    @InjectMocks
    private InsuranceGainServiceImpl service = new InsuranceGainServiceImpl();

    private InsuranceGainServiceImpl.InsuranceGainConfig config;

    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp(final AemContext context) {
        resourceResolver = mock(ResourceResolver.class);
        this.config = mock(InsuranceGainServiceImpl.InsuranceGainConfig.class);
        lenient().when(config.getInsuranceGainList()).thenReturn("/insurance-gain");
        service.activate(config);
        context.registerService(PageManager.class, pageManager);
        context.registerInjectActivateService(service);
    }

    @Test
    void testGetGenericList() {
        when(resolver.getResource("/etc/acs-commons/lists/insuranceGain")).thenReturn(resource);
        when(resource.listChildren()).thenReturn(resourceIterator);
        when(resourceIterator.hasNext()).thenReturn(true, false);
        when(resourceIterator.next()).thenReturn(pages);
        when(pages.getName()).thenReturn("SomeName");
        when(resolver.getResource(pages.getPath())).thenReturn(pageResource);
        when(pageResource.adaptTo(Page.class)).thenReturn(gainPage);
        when(gainPage.adaptTo(GenericList.class)).thenReturn(flagList);

        List<GenericList.Item> itemList = new ArrayList<>();
        GenericList.Item item1 = mock(GenericList.Item.class);
        GenericList.Item item2 = mock(GenericList.Item.class);
        lenient().when(item1.getValue()).thenReturn("value1");
        lenient().when(item1.getTitle()).thenReturn("title1");
        when(item2.getValue()).thenReturn("value2");
        when(item2.getTitle()).thenReturn("title2");

        itemList.add(item1);
        itemList.add(item2);
        when(flagList.getItems()).thenReturn(itemList);
        String result = service.getGenericList(resolver);
        String expected = "[{ age: value2,title2 }, { age: value1,title1 }]";
        assertEquals(expected, result);
        verify(resolver, times(1)).getResource("/etc/acs-commons/lists/insuranceGain");
    }
}
