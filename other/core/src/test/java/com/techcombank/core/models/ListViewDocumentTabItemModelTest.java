package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListViewDocumentTabItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListViewDocumentTabItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListRowModel.class);
        aemContext.load().json(TestConstants.LISTVIEWDOCUMENTTABITEM, "/tabItems");
        aemContext.currentResource(TestConstants.LISTVIEWDOCUMENTTABITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListViewDocumentTabItemModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "Tab1";
        assertEquals(expected, model.getTabTitle());
    }

    @Test
    void getListViewDocumentItemsParent() {
        assertNotNull(model.getListViewDocumentItemsFirstLevel());
    }
}
