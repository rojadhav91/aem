package com.techcombank.core.services;

import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.impl.RecaptchaServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class RecaptchaServiceTest {

    @InjectMocks
    private RecaptchaServiceImpl service = new RecaptchaServiceImpl();

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ResourceResolver resolver;

    private final AemContext ctx = new AemContext();

    @BeforeEach
    public void setup() throws Exception {

        ctx.addModelsForClasses(RecaptchaServiceImpl.class);
        when(resourceResolverService.getReadResourceResolver()).thenReturn(resolver);
    }

    @Test
    void testSiteNSecretKeys() {
        Optional<String> siteKey = Optional.of("configPath");
        Optional<String> secretKey = Optional.of("configPath");
        assertEquals(Optional.empty(), service.getSiteKey("configPath"));
        assertEquals(Optional.empty(), service.getSecretKey("configPath"));
    }

}
