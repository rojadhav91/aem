package com.techcombank.core.pojo;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.ManagementPanelModel;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ManagementPanelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private ManagementPanel managmentPanel;

    private ManagementPanelModel managmentPanelModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ManagementPanelModel.class);
        aemContext.load().json(TestConstants.MANAGEMENTPANEL, "/management-panel");
        aemContext.currentResource(TestConstants.MANAGEMENT);
        Resource resource = aemContext.request().getResource();
        managmentPanelModel = resource.adaptTo(ManagementPanelModel.class);
        managmentPanel = managmentPanelModel.getProfileItem().get(0);
    }

    @Test
    void getProfileImage() {
        String expected = "/content/dam/techcombank/managemnet-panel"
                + "/Mr-Bui-Thi-Hong-Mai-mb-01c9b74677-07dcb8b358.jpg"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg/jcr:content"
                + "/renditions/cq5dam.web.1280.1280.jpeg";
        assertEquals(expected, managmentPanel.getProfileImage());
    }

    @Test
    void getAlt() {
        String expected = "large image";
        assertEquals(expected, managmentPanel.getAlt());
    }

    @Test
    void getName() {
        String expected = "Mr. Ho Hung Anh";
        assertEquals(expected, managmentPanel.getName());
    }

    @Test
    void getDesignation() {
        String expected = "CHAIRMAN";
        assertEquals(expected, managmentPanel.getDesignation());
    }

    @Test
    void getDescription() {
        String expected = "Graduated from Electronics Engineering in Russia,"
                + " Mr. Ho Hung Anh joined the executive management of the";
        assertEquals(expected, managmentPanel.getDescription());
    }


    @Test
    void getProfileImageMobile() {
        String expected = "/content/dam/techcombank/managemnet-panel"
                + "/Mr-Bui-Thi-Hong-Mai-mb-01c9b74677-07dcb8b358.jpg/jcr:content"
                + "/renditions/cq5dam.web.1280.1280.jpeg/jcr:content/renditions/cq5dam.web.640.640.jpeg";
        assertEquals(expected, managmentPanel.getProfileImageMobile());
    }
}
