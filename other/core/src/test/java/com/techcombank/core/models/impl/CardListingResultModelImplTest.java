package com.techcombank.core.models.impl;

import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class CardListingResultModelImplTest {

    @Mock
    private ResourceResolver resolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @Mock
    private RangeIterator<Resource> rangeItr;

    @InjectMocks
    private CardListingResultModelImpl cardListingResultModel;
    private CardListingResultModelImpl model;

    private final AemContext aemContext = AppAemContext.newAemContext();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty() {
        cardListingResultModel.init();
        assertNull(cardListingResultModel.getNudgeTags());
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty2() {
        aemContext.addModelsForClasses(CardListingResultModelImpl.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CARD_LISTING_JSON, "/cardlist");
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CARD_LISTING_JSON,
                TestConstants.CONTENT_CARD_TAG);
        aemContext.currentResource(TestConstants.CARD_LIST);
        Resource resource = aemContext.request().getResource();
        tag = Mockito.mock(Tag.class);
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);

        lenient().when(tagManager.resolve("techcombank:card-types/credit-card")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/card-types/credit-card")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/card-types/credit-card/tags/cashback"))
                .thenReturn(tag);
        lenient().when(tag.getTagID()).thenReturn("tag");
        lenient().when(tag.getPath()).thenReturn("/content/cq:tags/techcombank/card-types/credit-card/tags");
        lenient().when(tagManager.find("/content/dam/techcombank/master-data/cards/credit-card", new String[] {"tag"}))
                .thenReturn(rangeItr);
        lenient().when(rangeItr.getSize()).thenReturn(1L);
        model = resource.adaptTo(CardListingResultModelImpl.class);
        assert model != null;
        modelTestAssertions();
    }

    private void modelTestAssertions() {
        assertNotNull(model.getGsonObj());
        assertNotNull(model.getResolver());
        assertNotNull(model.getJsonAry());
        assertNotNull(model.getCardOffersLabel());
        assertNotNull(model.getForLabel());
        assertNotNull(model.getCardCFRootPath());
        assertNotNull(model.getFeesLabel());
        assertNotNull(model.getNudgeTagRoot());
        assertNotNull(model.getConditionsLabel());
        assertNotNull(model.getOffersLabel());
        assertNotNull(model.getRatesLabel());
        assertNotNull(model.getOutstandingLabel());
        assertNotNull(model.getUtilitiesLabel());
        assertNotNull(model.getResolver());
        assertNotNull(model.getListingPage());
        assertNotNull(model.getSteps());
        assertNotNull(model.getSteps().get(0).getStepTitle());
        assertNotNull(model.getSteps().get(0).getImageStpAlt());
        assertNotNull(model.getSteps().get(0).getStepImg());
        assertNotNull(model.getSteps().get(0).getStepImgMobile());
        assertNotNull(model.getBottomText());
        assertNotNull(model.getBottomLink());
        assertNotNull(model.getBottomLinkLabel());
        assertNotNull(model.getCtaBtmTarget());
        assertTrue(model.isCtaBtmFollow());
        assertNotNull(model.getRegImg());
        assertNotNull(model.getImageRegAlt());
        assertNotNull(model.getRegCtaMobile());
        assertNotNull(model.getRegImgMobile());
        assertNotNull(model.getRegTitle());
        assertNotNull(model.getRegSubTitle());
    }
}
