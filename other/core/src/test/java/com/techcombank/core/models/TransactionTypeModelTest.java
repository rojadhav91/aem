package com.techcombank.core.models;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class TransactionTypeModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private TransactionTypeModel transactionTypeModel;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(TransactionTypeModel.class);
        aemContext.load().json(TestConstants.TRANSACTION_JSON, "/transactiontypemodel");
        aemContext.currentResource(TestConstants.TRANSACTIONTYPE_COMP);
        Resource resource = aemContext.request().getResource();
        transactionTypeModel = resource.adaptTo(TransactionTypeModel.class);
    }
    @Test
    void getTransactionType() {
        final String expected = "online payment";
        assertEquals(expected, transactionTypeModel.getTransactionType());
    }
    @Test
    void getTransactionLabel() {
        final String expected = "credit card";
        assertEquals(expected, transactionTypeModel.getTransactionLabel());
    }
}
