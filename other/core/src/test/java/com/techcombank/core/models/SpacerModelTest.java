package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class SpacerModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private SpacerModel spacerModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(SpacerModel.class);
        aemContext.load().json(TestConstants.SPACER, "/spacer");
        aemContext.currentResource(TestConstants.SPACERCOMP);
        Resource resource = aemContext.request().getResource();
        spacerModel = resource.adaptTo(SpacerModel.class);
    }

    @Test
    void getDesktopPadding() {
        final int expected = 200;
        assertEquals(expected, spacerModel.getDesktopPadding());
    }

    @Test
    void getMobilePadding() {
        final int expected = 50;
        assertEquals(expected, spacerModel.getMobilePadding());
    }
}
