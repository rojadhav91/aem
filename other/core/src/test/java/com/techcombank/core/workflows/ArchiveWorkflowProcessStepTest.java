package com.techcombank.core.workflows;

import com.adobe.acs.commons.workflow.synthetic.impl.SyntheticMetaDataMap;
import com.adobe.granite.asset.api.AssetManager;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.service.ResourceResolverService;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArchiveWorkflowProcessStepTest {

    @Mock
    private ResourceResolver resolver;
    @Mock
    private Resource resource;
    @Mock
    private Asset asset;
    @Mock
    private ValueMap valueMap;
    @Mock
    private AssetManager assetMgr;
    @Mock
    private Workflow workflow;
    @Mock
    private WorkItem workItem;
    @Mock
    private Session session;
    @Mock
    private WorkflowData workflowData;

    @Mock
    private ResourceResolverService resourceResolverService;
    @Mock
    private MetaDataMap workflowMetadataMap;
    @Mock
    private Node node;
    @Mock
    private PageManager pgMgr;
    @Test
    void testExecute() throws WorkflowException, NoSuchFieldException, RepositoryException {

        ArchiveWorkflowProcessStep archiveWorkflowProcessStep = new ArchiveWorkflowProcessStep();
        session = mock(Session.class);

        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getPayload()).thenReturn("/var/techcombank/dam/rdb-app/asset");
        lenient().when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        lenient().when(resolver.getResource("/var/techcombank/dam/rdb-app/asset")).thenReturn(resource);
        lenient().when(resolver.getResource("/var/techcombank/dam/rdb-app/asset/jcr:content/vlt:definition/filter"))
                .thenReturn(resource);
        lenient().when(resolver.getResource("/content/techcombank/dam/archive")).thenReturn(null);
        when(workItem.getWorkflow()).thenReturn(workflow);
        PrivateAccessor.setField(archiveWorkflowProcessStep, "resourceResolverService",
                resourceResolverService);
        when(resourceResolverService.getWriteResourceResolver()).thenReturn(resolver);
        when(resolver.adaptTo(PageManager.class)).thenReturn(pgMgr);
        lenient().when(resolver.adaptTo(Session.class)).thenReturn(session);
        ArrayList<Resource> list = new ArrayList<>();
        list.add(resource);
        when(resource.getChildren()).thenReturn(list);
        when(resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.get("root", String.class)).thenReturn("/content/techcombank/dam/rdb-app/asset");
        doNothing().when(resolver).close();
        WorkflowSession workflowSession = mock(WorkflowSession.class);
        when(workflowSession.adaptTo(Mockito.<Class<ResourceResolver>>any()))
                .thenReturn(resolver);
        when(resource.adaptTo(ValueMap.class))
                .thenReturn(valueMap);

        archiveWorkflowProcessStep.execute(workItem, workflowSession, new SyntheticMetaDataMap());
        verify(workItem).getWorkflowData();
        verify(workflowSession).adaptTo(Mockito.<Class<ResourceResolver>>any());

    }

    @Test
    void testExecute2() throws WorkflowException, NoSuchFieldException, RepositoryException {

        ArchiveWorkflowProcessStep archiveWorkflowProcessStep = new ArchiveWorkflowProcessStep();
        session = mock(Session.class);

        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getPayload()).thenReturn("/etc/techcombank/dam/rdb-app/asset");
        lenient().when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        lenient().when(resolver.getResource("/etc/techcombank/dam/rdb-app/asset")).thenReturn(resource);
        lenient().when(resolver.getResource("/etc/techcombank/dam/rdb-app/asset/jcr:content/vlt:definition/filter"))
                .thenReturn(resource);
        lenient().when(resolver.getResource("/content/techcombank/dam/archive")).thenReturn(null);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(session.getRootNode()).thenReturn(node);
        lenient().when(node.hasNode("content/dam/techcombank/archive/rdb-app/"
                + String.valueOf(LocalDate.now().getYear()))).thenReturn(true);
        PrivateAccessor.setField(archiveWorkflowProcessStep, "resourceResolverService",
                resourceResolverService);
        when(resourceResolverService.getWriteResourceResolver()).thenReturn(resolver);
        when(resolver.adaptTo(AssetManager.class)).thenReturn(assetMgr);
        lenient().when(resolver.adaptTo(Session.class)).thenReturn(session);
        ArrayList<Resource> list = new ArrayList<>();
        list.add(resource);
        when(resource.getChildren()).thenReturn(list);
        when(resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.get("root", String.class)).thenReturn("/content/dam/techcombank/rdb-app/asset");
        lenient().when(resolver.getResource("/content/dam/techcombank/archive/rdb-app/2023/asset"))
                .thenReturn(resource);
        lenient().when(resource.getName()).thenReturn("asset");
        doNothing().when(resolver).close();
        WorkflowSession workflowSession = mock(WorkflowSession.class);
        when(workflowSession.adaptTo(Mockito.<Class<ResourceResolver>>any()))
                .thenReturn(resolver);
        when(resource.adaptTo(ValueMap.class))
                .thenReturn(valueMap);

        archiveWorkflowProcessStep.execute(workItem, workflowSession, new SyntheticMetaDataMap());
        verify(workItem).getWorkflowData();
        verify(workflowSession).adaptTo(Mockito.<Class<ResourceResolver>>any());

    }

    @Test
    void testExecute3() throws WorkflowException, NoSuchFieldException, RepositoryException {

        ArchiveWorkflowProcessStep archiveWorkflowProcessStep = new ArchiveWorkflowProcessStep();
        session = mock(Session.class);

        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getPayload()).thenReturn("/content/techcombank/dam/rdb-app/asset");
        lenient().when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        lenient().when(resolver.getResource("/content/techcombank/dam/rdb-app/asset")).thenReturn(resource);
        lenient().when(
                resolver.getResource("/content/techcombank/dam/rdb-app/asset/jcr:content/vlt:definition/filter"))
                .thenReturn(resource);
        lenient().when(resolver.getResource("/content/techcombank/dam/archive")).thenReturn(null);
        when(workItem.getWorkflow()).thenReturn(workflow);
        lenient().when(resource.getName()).thenReturn("asset");
        lenient().when(node.hasNode("content/techcombank/dam/rdb-app")).thenReturn(true);
        PrivateAccessor.setField(archiveWorkflowProcessStep, "resourceResolverService",
                resourceResolverService);
        when(resourceResolverService.getWriteResourceResolver()).thenReturn(resolver);
        when(resolver.adaptTo(PageManager.class)).thenReturn(pgMgr);
        lenient().when(resolver.adaptTo(Session.class)).thenReturn(session);
        doNothing().when(resolver).close();
        WorkflowSession workflowSession = mock(WorkflowSession.class);
        when(workflowSession.adaptTo(Mockito.<Class<ResourceResolver>>any()))
                .thenReturn(resolver);
        archiveWorkflowProcessStep.execute(workItem, workflowSession, new SyntheticMetaDataMap());
        verify(workItem).getWorkflowData();
        verify(workflowSession).adaptTo(Mockito.<Class<ResourceResolver>>any());
    }
    @Test
    void testExecute4() throws WorkflowException, NoSuchFieldException, RepositoryException {

        ArchiveWorkflowProcessStep archiveWorkflowProcessStep = new ArchiveWorkflowProcessStep();
        session = mock(Session.class);

        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getPayload()).thenReturn("/content/techcombank/web/vn/en/personal");
        lenient().when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        lenient().when(resolver.getResource("/content/techcombank/web/vn/en/personal")).thenReturn(resource);
        when(workItem.getWorkflow()).thenReturn(workflow);
        PrivateAccessor.setField(archiveWorkflowProcessStep, "resourceResolverService",
                resourceResolverService);
        when(resourceResolverService.getWriteResourceResolver()).thenReturn(resolver);
        when(resolver.adaptTo(PageManager.class)).thenReturn(pgMgr);

        lenient().when(resolver.adaptTo(Session.class)).thenReturn(session);
        doNothing().when(resolver).close();
        WorkflowSession workflowSession = mock(WorkflowSession.class);
        when(workflowSession.adaptTo(Mockito.<Class<ResourceResolver>>any()))
                .thenReturn(resolver);
        archiveWorkflowProcessStep.execute(workItem, workflowSession, new SyntheticMetaDataMap());
        verify(workItem).getWorkflowData();
        verify(workflowSession).adaptTo(Mockito.<Class<ResourceResolver>>any());
    }
}
