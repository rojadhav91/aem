package com.techcombank.core.models.impl;

import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class CardListingModelImplTest {

    @Mock
    private ResourceResolver resolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @Mock
    private RangeIterator<Resource> rangeItr;

    @InjectMocks
    private CardListingModelImpl cardListingModel;

    private final AemContext aemContext = AppAemContext.newAemContext();

    private CardListingModelImpl model;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty() {
        cardListingModel.init();
        assertNull(cardListingModel.getCardTags());
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty2() {
        aemContext.addModelsForClasses(CardListingModelImpl.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CARD_LISTING_JSON, "/cardlist");
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.CARD_LISTING_JSON,
                TestConstants.CONTENT_CARD_TAG);
        aemContext.currentResource(TestConstants.CARD_LIST);
        Resource resource = aemContext.request().getResource();
        tag = Mockito.mock(Tag.class);
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);

        lenient().when(tagManager.resolve("techcombank:card-types/credit-card")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/card-types/credit-card")).thenReturn(tag);
        lenient().when(tagManager.resolve("/content/cq:tags/techcombank/card-types/credit-card/tags/cashback"))
                .thenReturn(tag);
        lenient().when(tag.getTagID()).thenReturn("tag");
        lenient().when(tag.getPath()).thenReturn("/content/cq:tags/techcombank/card-types/credit-card/tags");
        lenient().when(tagManager.find("/content/dam/techcombank/master-data/cards/credit-card", new String[] {"tag"}))
                .thenReturn(rangeItr);
        lenient().when(rangeItr.getSize()).thenReturn(1L);
        model = resource.adaptTo(CardListingModelImpl.class);
        assert model != null;
        modelTestAssertions();
    }

    private void modelTestAssertions() {
        assertNotNull(model.getGsonObj());
        assertNotNull(model.getResolver());
        assertNotNull(model.getJsonAry());
        assertNotNull(model.getCardTagRoot());
        assertNotNull(model.getCardCFRootPath());
        assertNotNull(model.getNudgeTagRoot());
        assertNotNull(model.getBannerTitle());
        assertNotNull(model.getBannerImg());
        assertNotNull(model.getBannerDesc());
        assertNotNull(model.getBannerLink());
        assertTrue(model.isBannerTarget());
        assertTrue(model.isBannerFollow());
        assertNotNull(model.getCardTypeLabel());
        assertNotNull(model.getImageAlt());
        assertNotNull(model.getRegCtaMobile());
        assertNotNull(model.getSteps());
        assertNotNull(model.getSteps().get(0).getStepTitle());
        assertNotNull(model.getSteps().get(0).getImageStpAlt());
        assertNotNull(model.getSteps().get(0).getStepImg());
        assertNotNull(model.getSteps().get(0).getStepImgMobile());
        assertNotNull(model.getBottomText());
        assertNotNull(model.getBottomLink());
        assertNotNull(model.getBottomLinkLabel());
        assertNotNull(model.getCtaBtmTarget());
        assertTrue(model.isCtaBtmFollow());
        assertNotNull(model.getCtaCompLink());
        assertNotNull(model.getCtaCompLabel());
        assertNotNull(model.getRegImg());
        assertNotNull(model.getRegImgMobile());
        assertNotNull(model.getNudgeTags());
        assertNotNull(model.getImageRegAlt());
        assertNotNull(model.getRegTitle());
        assertNotNull(model.getCtaCompTarget());
        assertNotNull(model.getRegSubTitle());
        assertNotNull(model.getCardTags());
        assertTrue(model.isCtaCompFollow());
    }
}
