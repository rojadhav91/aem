package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The Class ErrorComponentModelTest to perform junit for Error component Model
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ErrorComponentModelTest {

    private final AemContext ctx = new AemContext();

    private ErrorComponentModel errorComponentModel;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.ERROR_MODEL_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(ErrorComponentModel.class);
        errorComponentModel = getModel("/content/jcr:content/root/container/error");
    }

    private ErrorComponentModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        ErrorComponentModel errormodel = res.adaptTo(ErrorComponentModel.class);
        return errormodel;
    }

    @Test
    void testErrorMoelFields() {
        assertEquals("500", errorComponentModel.getErrorCode());
        assertEquals("Internal service error", errorComponentModel.getErrorMessage());
    }

}
