package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The Class FormHiddenFieldModelTest to perform code coverage test for
 * FormHiddenFieldModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class FormHiddenFieldModelTest {

    private final AemContext ctx = new AemContext();

    private FormHiddenFieldModel formHiddenFieldModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.FORM_HIDDEN_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(FormHiddenFieldModel.class);
        formHiddenFieldModel = getModel("/content/hidden");
    }

    private FormHiddenFieldModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        FormHiddenFieldModel formHiddenModel = res.adaptTo(FormHiddenFieldModel.class);
        return formHiddenModel;
    }

    @Test
    void testValueText() {
        assertEquals("CRM001", formHiddenFieldModel.getValueText());
    }

    @Test
    void testCheckboxValueText() {
        assertEquals(true, formHiddenFieldModel.isCheckboxValueText());
    }

    @Test
    void testId() {
        assertEquals("hidden-id", formHiddenFieldModel.getId());
    }

    @Test
    void testName() {
        assertEquals("Campaign", formHiddenFieldModel.getName());
    }

    @Test
    void testValue() {
        assertEquals("cmp", formHiddenFieldModel.getValue());
    }
}
