package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class NestedDropdownModelTest {

    private final AemContext ctx = new AemContext();

    private NestedDropdownModel model;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.NESTEDDROPDOWN, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(NestedDropdownModel.class);
        ctx.currentResource(TestConstants.NESTEDDROPDOWNCOMP);
        model = ctx.currentResource().adaptTo(NestedDropdownModel.class);
    }

    @Test
    void testgetCityLabel() {
        String expected = "fieldLabel";
        assertEquals(expected, model.getFieldLabel());
    }

    @Test
    void testgetFieldName() {
        String expected = "fieldName";
        assertEquals(expected, model.getFieldName());
    }

    @Test
    void testgetPlaceholderText() {
        String expected = "placeholderText";
        assertEquals(expected, model.getPlaceholderText());
    }

    @Test
    void testgetRequiredMessage() {
        String expected = "requiredMessage";
        assertEquals(expected, model.getRequiredMessage());
    }

    @Test
    void testgetItem() {
        String expected = "item";
        assertEquals(expected, model.getItem());
    }
    @Test
    void testisAnalytics() {
        boolean expected = true;
        assertEquals(expected, model.isAnalytics());
    }

}
