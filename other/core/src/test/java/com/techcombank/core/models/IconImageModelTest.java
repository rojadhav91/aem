package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })

class IconImageModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private IconImageModel iconImageModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(IconImageModel.class);
        aemContext.load().json(TestConstants.ICONIMAGE, "/iconImage");
        aemContext.currentResource(TestConstants.ICONIMAGECOMP);
        Resource resource = aemContext.request().getResource();
        iconImageModel = resource.adaptTo(IconImageModel.class);
    }

    @Test
    void getIconSrcOn() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, iconImageModel.getIconSrcOn());
    }

    @Test
    void getIconSrcOff() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, iconImageModel.getIconSrcOff());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(iconImageModel.getIconSrcOn() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                iconImageModel.getWebIconSrcOn());
        assertEquals(iconImageModel.getIconSrcOn() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                iconImageModel.getMobileIconSrcOn());
        assertEquals(iconImageModel.getIconSrcOff() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                iconImageModel.getWebIconSrcOff());
        assertEquals(iconImageModel.getIconSrcOff() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                iconImageModel.getMobileIconSrcOff());
    }
}
