package com.techcombank.core.service.impl;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.Articles;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleListingServiceImplTest {

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Session session;

    @Mock
    private SlingHttpServletRequest request;

    @InjectMocks
    private ArticleListingServiceImpl articleListingService = new ArticleListingServiceImpl();

    @Mock
    private ValueMap valueMap;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Query query;

    private List<Hit> hits = new ArrayList<>();

    @Mock
    private Hit hit1;

    @Mock
    private Hit hit2;

    @Mock
    private Hit hit3;

    @Mock
    private Hit hit4;

    @Mock
    private GregorianCalendar date;

    @Mock
    private SearchResult searchResult;

    @Mock
    private TagManager tagManager;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private Tag tag;

    @BeforeEach
    void setUp(final AemContext context) {
        MockitoAnnotations.openMocks(this);
        String cqtags = "techcombank:articles/article-keyword-tag/blog";
        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(currentPage.getContentResource()).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(currentPage.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(valueMap.get(any(), any())).thenReturn(cqtags);
        lenient().when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        lenient().when(queryBuilder.createQuery(any(), any())).thenReturn(query);
        lenient().when(query.getResult()).thenReturn(searchResult);
    }

    @Test
    void testGetArticleList() throws RepositoryException {
        hits.add(hit1); hits.add(hit2); hits.add(hit3); hits.add(hit4);
        lenient().when(searchResult.getHits()).thenReturn(hits);
        lenient().when(request.getLocale()).thenReturn(Locale.ENGLISH);
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        lenient().when(request.getQueryString()).thenReturn("queryString");
        lenient().when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
        lenient().when(tagManager.resolve(any())).thenReturn(tag);
        lenient().when(tag.getTitle()).thenReturn("Hello");
        lenient().when(hit1.getPath()).thenReturn("/article-page1");
        lenient().when(hit2.getPath()).thenReturn("/article-page2");
        lenient().when(hit3.getPath()).thenReturn("/article-page3");
        lenient().when(hit4.getPath()).thenReturn("/article-page4");
        lenient().when(resourceResolver.getResource(any())).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(valueMap.get("bgBannerImage", StringUtils.EMPTY)).thenReturn("abc.png");
        lenient().when(valueMap.get("bgBannerImgAlt", StringUtils.EMPTY)).thenReturn("altText");
        lenient().when(valueMap.get("articleCreationDate")).thenReturn(date);
        lenient().when(valueMap.get("jcr:description", StringUtils.EMPTY)).thenReturn("desc");
        lenient().when(valueMap.get("pageTitle", StringUtils.EMPTY)).thenReturn("title");
        List<Articles> articlesList = articleListingService.getArticlesList(request, Locale.ENGLISH);
        Assert.assertEquals(TestConstants.LIST_SIZE_FOUR, articlesList.size());
    }
}
