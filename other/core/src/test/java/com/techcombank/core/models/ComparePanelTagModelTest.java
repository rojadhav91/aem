package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ComparePanelTagModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ComparePanelTagModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ComparePanelTagModel.class);
        aemContext.load().json(TestConstants.COMPAREPANELTAG, "/comparePanelTag");
        aemContext.currentResource(TestConstants.COMPAREPANELTAGCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ComparePanelTagModel.class);
    }

    @Test
    void getTagId() {
        final String expected = "text-value";
        assertEquals(expected, model.getTagId());
    }

    @Test
    void getTagTitle() {
        final String expected = "text-value";
        assertEquals(expected, model.getTagTitle());
    }

    @Test
    void getTagPath() {
        final String expected = "text-value";
        assertEquals(expected, model.getTagPath());
    }

}
