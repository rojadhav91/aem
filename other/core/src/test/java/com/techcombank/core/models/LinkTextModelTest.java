package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class LinkTextModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private LinkTextModel linkTextModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(LinkTextModel.class);
        aemContext.load().json(TestConstants.LINKTEXT, "/linkText");
        aemContext.currentResource(TestConstants.LINKTEXTCOMP);
        Resource resource = aemContext.request().getResource();
        linkTextModel = resource.adaptTo(LinkTextModel.class);
    }

    void setUpHorizontalLinkText() {
        aemContext.currentResource(TestConstants.LINKTEXTCOMPHOR);
        Resource resourceHorizontal = aemContext.request().getResource();
        linkTextModel = resourceHorizontal.adaptTo(LinkTextModel.class);
    }

    @Test
    void testLinkTextModalFields() {
        assertEquals("top", linkTextModel.getImagePosition());
        assertEquals("vertical", linkTextModel.getLinkTextView());
        assertEquals(1, linkTextModel.getCardsNumber());
        assertTrue(linkTextModel.isArrow());
        assertEquals("view more", linkTextModel.getViewMoreLabel());
        assertEquals(TestConstants.LIST_SIZE_THREE, linkTextModel.getLinkTextItems().size());
        assertEquals(TestConstants.LINK_TEXT_COLUMNS_NUMBER, linkTextModel.calculateColumnsNumber());
        assertTrue(linkTextModel.calculatePosition());
        setUpHorizontalLinkText();
        assertEquals(TestConstants.LINK_TEXT_COLUMNS_NUMBER_ONE, linkTextModel.calculateColumnsNumber());
        assertTrue(linkTextModel.calculatePosition());
    }
}
