package com.techcombank.core.testcontext;

public final class TestConstants {

    private TestConstants() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static final String TCB_SLING_MODELS = "com.techcombank.core.models";

    public static final String SLASH = "/";
    public static final String PERIOD = ".";

    public static final String JSON_FILE = "json";

    public static final String JSON_ARRAY_DATA = "[{'item1':'1','item2':'2','item3':'3'}]";
    public static final String JCR_CONTENT = "jcr:content";
    public static final String ROOT_PATH = "/jcr:content/root";
    public static final String ROOT_CONTAINER_PATH = ROOT_PATH + "/container";
    public static final String JSON_DATA = "{'item1':'1','item2':'2','item3':'3'}";
    public static final String[] FILTER_KEY_ARRAY = {"item2"};
    public static final String FILTER_KEY = "item2";
    public static final int CALL_COUNT_2 = 2;
    public static final int CALL_COUNT_3 = 3;
    public static final int CALL_COUNT_4 = 4;
    public static final int CALL_COUNT_5 = 5;
    public static final int CALL_COUNT_6 = 6;
    public static final int CALL_COUNT_17 = 17;
    public static final int CALL_COUNT_15 = 15;
    public static final String TOKEN = "token";
    public static final String APP_DOMAIN = "http://aemapp-qa.tcb.com";
    public static final String SOURCE_APP = "app";
    public static final String DESTINATION_PATH = "/content/target/page1";
    public static final String NODE = "node";
    public static final String PERSONAL_BANKING_PAGE_DOMAIN_PATH =
            "http://localhost:4503/content/techcombank/web/vn/en/personal-page";

    // Image Optimization
    public static final String PNG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.png";
    public static final String JPEG_WEB_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
    public static final String PNG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.png";
    public static final String JPEG_MOBILE_IMAGE_RENDITION_PATH = "/jcr:content/renditions/cq5dam.web.640.640.jpeg";

    public final class JSON {
        private JSON() {
            throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
        }

        public static final String PERSONAL_BANKING_PAGE = "/content/techcombank/web/vn/en/personal-page/content.json";

        public static final String DOWNLOAD_CONTENT_BLOCK_PAGE =
                "/content/techcombank/web/vn/en/download-content-block/content.json";
       public static final String BULK_CONTENT_PAGE =
                "/content/techcombank/web/vn/en/bulkpagepublish.json";

        public static final String APP_CONTENT_PAGE =
                "/content/techcombank/rdb-app/v1/vn/en/mobile-app-data/content.json";

        public static final String PERSONAL_BANKING_XF_PAGE =
                "/content/techcombank/web/vn/en/personal-page-xf/content.json";

    }

    public final class CONTENT {
        private CONTENT() {
            throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
        }

        public static final String PERSONAL_BANKING_PAGE = "/content/techcombank/web/vn/en/personal-page";
        public static final String DOWNLOAD_CONTENT_BLOCK_PAGE =
                "/content/techcombank/web/vn/en/download-content-block";
        public static final String PERSONAL_BANKING_XF_PAGE = "/content/techcombank/web/vn/en/personal-page-xf";

        public static final String APP_CONTENT_PAGE = "/content/techcombank/rdb-app/v1/vn/en/mobile-app-data";

    }
}
