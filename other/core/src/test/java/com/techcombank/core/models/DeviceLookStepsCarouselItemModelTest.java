package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class DeviceLookStepsCarouselItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private DeviceLookStepsCarouselItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(DeviceLookStepsCarouselItemModel.class);
        aemContext.load().json(TestConstants.DEVICELOOKSTEPSCAROUSELITEM, "/deviceLookStepsCarouselItem");
        aemContext.currentResource(TestConstants.DEVICELOOKSTEPSCAROUSELITEMCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(DeviceLookStepsCarouselItemModel.class);
    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImagePath());
        assertEquals(model.getImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImagePath());
    }

    @Test
    void getAltText() {
        final String expected = "a";
        assertEquals(expected, model.getAltText());
    }

    @Test
    void getStepNumber() {
        final String expected = "1";
        assertEquals(expected, model.getStepNumber());
    }

    @Test
    void getDescription() {
        final String expected = "a";
        assertEquals(expected, model.getDescription());
    }

}
