package com.techcombank.core.servlets;

import com.day.cq.commons.Externalizer;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.JsonFilterService;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.testcontext.TestConstants;
import com.techcombank.core.utils.PlatformUtils;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.settings.SlingSettingsService;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.net.ssl.HttpsURLConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class AppPageProxyServletTest {

    @InjectMocks
    private final AppPageProxyServlet appPageProxyServlet = new AppPageProxyServlet();

    private  AemContext context = TCBAemContext.newAemContext();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private Externalizer externalizer;

    @Mock
    private SlingSettingsService slingSettings;

    @Mock
    private JsonFilterService jsonFilterService;

    private static final int STATUS = 200;

    private static final int SC_BAD_REQUEST = 400;

    @BeforeEach
    public void setup() throws IOException {
        Resource pageResource = Mockito.mock(Resource.class);
        PrintWriter out = Mockito.mock(PrintWriter.class);
        lenient().when(request.getResource()).thenReturn(pageResource);
        lenient().when(pageResource.getPath()).thenReturn(TestConstants.CONTENT.APP_CONTENT_PAGE);

        lenient().when(pageResource.isResourceType(TCBConstants.CQ_PAGE)).thenReturn(true);
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(response.getWriter()).thenReturn(out);
        Set<String> runModes = new HashSet<String>();
        runModes.add(TCBConstants.AUTHOR);
        lenient().when(slingSettings.getRunModes()).thenReturn(runModes);
        lenient().when(PlatformUtils.getExternalizer(externalizer, resourceResolver, TestConstants.SOURCE_APP))
                .thenReturn("http://localhost:4502");
        lenient().when(jsonFilterService.getAuthorizationToken()).thenReturn(TestConstants.TOKEN);
        InputStream inputStream = new ByteArrayInputStream(TestConstants.JSON_DATA.getBytes());
        HttpsURLConnection con = Mockito.mock(HttpsURLConnection.class);
        lenient().when(con.getInputStream()).thenReturn(inputStream);
    }

    @Test
    void testGetAssetMap() throws IOException {
        MockSlingHttpServletResponse res = context.response();
        appPageProxyServlet.doGet(request, res);
        assertEquals(STATUS, res.getStatus());
    }
    @Test
    void testInvalidResourceType() throws IOException {
        Resource pageResource = Mockito.mock(Resource.class);
        lenient().when(request.getResource()).thenReturn(pageResource);
        lenient().when(pageResource.isResourceType("cq/Page")).thenReturn(false);
        MockSlingHttpServletResponse res = context.response();
        appPageProxyServlet.doGet(request, res);
        assertEquals(SC_BAD_REQUEST, res.getStatus());
    }

}
