package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static junitx.framework.ComparableAssert.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ListEventTileModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private ListEventTileModel listEventTileModel;

    @Mock
    private ListEventTileItem listEventTileModel1;

    @Mock
    private  ListEventTileItem listEventTileModel2;

    private List<ListEventTileItem> listEventTileItems = new ArrayList<>();

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        listEventTileModel = new ListEventTileModel();
        listEventTileItems.add(listEventTileModel1);
        listEventTileItems.add(listEventTileModel2);
        PrivateAccessor.setField(listEventTileModel, "eventTileItem", listEventTileItems);
    }

    @Test
    void getEventTileItem() {
        assertEquals(TestConstants.LIST_SIZE_TWO, listEventTileModel.getEventTileItem().size());
        assertEquals(TestConstants.LIST_SIZE_TWO, 2);
    }

}
