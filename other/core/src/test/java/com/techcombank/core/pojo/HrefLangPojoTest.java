package com.techcombank.core.pojo;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class HrefLangPojoTest {
    @Test
    void testHrefGetterSetter() {
        HrefLangPojo hrefLangPojo = new HrefLangPojo();
        hrefLangPojo.setRel("English");
        hrefLangPojo.setHref("/content/techcombank/web/vn/en");
        hrefLangPojo.setHrefLang("vn");
        assertEquals("English", hrefLangPojo.getRel());
        assertEquals("/content/techcombank/web/vn/en", hrefLangPojo.getHref());
        assertEquals("vn", hrefLangPojo.getHrefLang());
    }
}
