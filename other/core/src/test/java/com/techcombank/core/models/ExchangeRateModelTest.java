
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.ExchangeRateModelImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class ExchangeRateModelTest to perform junit for ExchangeRateModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ExchangeRateModelTest {

    private final AemContext ctx = new AemContext();

    private ExchangeRateModelImpl exchangeRateModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.EXCHANGE_RATE_CMP_JSON,
                TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(ExchangeRateModelImpl.class);
        exchangeRateModel = getModel("/content/jcr:content/root/container/exchangerate");

    }

    private ExchangeRateModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        ExchangeRateModelImpl exchangeRate = res.adaptTo(ExchangeRateModelImpl.class);
        return exchangeRate;
    }

    @Test
    void testHeaderSection() {
        assertEquals("Exchange rate", exchangeRateModel.getExchangeRateTitle());
        assertEquals("Day", exchangeRateModel.getDayLabel());
        assertEquals("Updated time", exchangeRateModel.getTimeLabel());
        assertEquals("CURRENCY", exchangeRateModel.getCurrencyLabel());
        assertEquals("PURCHASE RATE", exchangeRateModel.getPurchaseLabel());
        assertEquals("Cash / Cheque", exchangeRateModel.getCashLabel());
        assertEquals("Transfer", exchangeRateModel.getTransferLabel());
        assertEquals("SELLING RATE", exchangeRateModel.getSellLabel());
    }

    @Test
    void testIcon() {
        assertEquals("/content/dam/techcombank/images/GiftbOX.svg", exchangeRateModel.getDayIcon());
        assertEquals("DayAlt", exchangeRateModel.getDayIconAlt());
        assertEquals("/content/dam/techcombank/images/GiftbOX.svg", exchangeRateModel.getDayIconWebImagePath());
        assertEquals("/content/dam/techcombank/images/GiftbOX.svg", exchangeRateModel.getDayIconMobileImagePath());
        assertEquals("/content/dam/techcombank/download.svg", exchangeRateModel.getTimeIcon());
        assertEquals("timeAlt", exchangeRateModel.getTimeIconAlt());
        assertEquals("/content/dam/techcombank/download.svg", exchangeRateModel.getTimeIconWebImagePath());
        assertEquals("/content/dam/techcombank/download.svg", exchangeRateModel.getTimeIconMobileImagePath());
    }

    @Test
    void testMiddleSection() {
        assertEquals("<p>Source from Bloomberg</p>", exchangeRateModel.getSummaryDesc());
        assertEquals("CENTRAL RATES SBV ANNOUNCED", exchangeRateModel.getCentralLabel());
        assertEquals("FLOOR RATE USD VND", exchangeRateModel.getFloorLabel());
        assertEquals("CEILING RATE USD VND", exchangeRateModel.getCeilingLabel());
        assertEquals("USD forward rate", exchangeRateModel.getUsdLabel());
        assertEquals("Reference", exchangeRateModel.getRefLabel());
        assertEquals("Term (days)", exchangeRateModel.getTermLabel());
        assertEquals("Gold rate", exchangeRateModel.getGoldLabel());
        assertEquals("See More", exchangeRateModel.getSeeMoreLabel());
    }

    @Test
    void testFooterSection() {
        assertEquals("Download", exchangeRateModel.getCtaLabel());
        assertEquals("No data is available", exchangeRateModel.getErrorMessage());
        assertEquals("Exchange rates", exchangeRateModel.getFooterLabelOne());
        assertEquals("<p>Hotline (24/7): <b>1800 588 823 +84 (24) 3944 9626</b></p>",
                exchangeRateModel.getFooterLabelTwo());
        assertEquals("www.techcombank.com", exchangeRateModel.getFooterLabelThree());
    }
}
