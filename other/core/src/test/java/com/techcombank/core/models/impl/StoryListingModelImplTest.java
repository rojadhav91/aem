package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.StoryListingModel;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.utils.PlatformUtils;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.WordUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StoryListingModelImplTest {

    private final AemContext aemContext = TCBAemContext.newAemContext();
    private StoryListingModelImpl storyListingModel;


    @Mock
    private TagManager tagManager;


    private Tag tag;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(StoryListingModel.class);
        aemContext.load().json(TestConstants.STORYLISTING, TestConstants.STORYLISTINGCOMP);
        aemContext.currentResource(TestConstants.STORYLISTINGCOMP + TestConstants.STORYLISTINGCOMP);
        storyListingModel = aemContext.currentResource().adaptTo(StoryListingModelImpl.class);
        tagManager = aemContext.resourceResolver().adaptTo(TagManager.class);
    }

    @Test
    void getViewMore() {
        String getViewMoreButtonLabel = storyListingModel.getViewMore();
        assertEquals("view more", getViewMoreButtonLabel);
    }

    @Test
    void getPrimaryTypeFilterLabel() {
        String contentFilterlabel = storyListingModel.getArticleCategoryLabel();
        assertEquals("content type", contentFilterlabel);
    }

    @Test
    void getSecondaryFilterLabel() {
        String categoryLabel = storyListingModel.getArticleTagLabel();
        assertEquals("Category filter", categoryLabel);
    }

    @Test
    void getResult() {
        String result = storyListingModel.getResult();
        assertEquals("5", result);
    }

    @Test
    void getCtaLabel() {
        String ctalabel = storyListingModel.getCtaLabel();
        assertEquals("viwe more", ctalabel);
    }


    @Test
    void getNofollow() {
        String nofollow = storyListingModel.getNofollow();
        assertEquals("true", nofollow);
    }

    @Test
    void getRootPath() {
        String rootPath = storyListingModel.getRootPath();
        assertEquals("/content/techcombank/web/vn/en/articles", rootPath);
    }

    @Test
    void getSecondary() {
        String[] category = storyListingModel.getArticleTag();
        assertEquals("techcombank:articles/article-category/businesspromotion", category[0]);

    }

    @Test
    void getNewTab() {
        String openInNewTab = storyListingModel.getNewTab();
        assertEquals("true", openInNewTab);
    }

    @Test
    void testPrimary() {
        String[] content = {"techcombank:articles/article-type/video",
                "techcombank:articles/article-type/article"};
        assertEquals(2, storyListingModel.getArticleCategory().length);
        for (int i = 0; i < content.length; i++) {
            assertEquals(content[i], storyListingModel.getArticleCategory()[i]);
        }
    }

    @Test
    void getErrorMsg() {
        String erroMsg = storyListingModel.getErrorMsg();
        assertEquals("Network Error", erroMsg);
    }

    @Test
    void getAllFilter() {
        String all = storyListingModel.getAllFilter();
        assertEquals("All", all);
    }

    @Test
    void getApply() {
        String apply = storyListingModel.getApply();
        assertEquals("Apply", apply);
    }

    @Test
    void getImage() {
        String image = storyListingModel.getImage();
        assertEquals("/content/dam/techcombank/sample.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png", image);
    }

    @Test
    void getInteraction() {
        String viewMore = "view more";
        String expectedInteraction = "{'webInteractions': {'name': 'view more','type': 'other'}}";
        String actualInteraction = PlatformUtils.getInteractionType(viewMore, TCBConstants.OTHER);
        assertEquals(expectedInteraction, storyListingModel.getInteraction());
    }

    @Test
    void getCategoryTags() {
        String[] category = {"Promotions", "Businesspromotion"};
        Map<String, String> conentMap = storyListingModel.getContentTagMap(category);
        assertEquals(conentMap.isEmpty(), storyListingModel.getArticleTags().isEmpty());
    }

    @Test
    void getFilter() {
        String filter = storyListingModel.getFilter();
        assertEquals("Filter", filter);
    }

    @Test
    void getAlt() {
        String alt = storyListingModel.getAlt();
        assertEquals("ALT Text", alt);
    }

    private Map<String, String> getTagMap() {
        String[] tagList = {"techcombank:articles/article-category/businesspromotion",
                "techcombank:articles/article-category/promotions"};
        Map<String, String> conentMap = new HashMap<>();
        if (!ArrayUtils.isEmpty(tagList)) {
            for (String content : tagList) {
                tag = tagManager.resolve(content);
                conentMap.put(WordUtils.capitalize(tag.getName()), content);
            }
        }
        return conentMap;
    }

    @Test
    void getContentTagMap() {
        String[] tagList = {"techcombank:articles/article-category/article-primary-category/promotions",
                "techcombank:articles/article-category/article-primary-category/professional-development"};
        Map<String, String> conentMap = storyListingModel.getContentTagMap(tagList);
        assertTrue(conentMap.isEmpty());
    }

    @Test
    void getArticleCategoryTags() {
        Map<String, String> contentMap = new HashMap<>();
        contentMap.put("Video", "techcombank:articles/article-type/video");
        contentMap.put("Article", "techcombank:articles/article-type/article");
        Map<String, String> conentMap = storyListingModel.getArticleCategoryTags();
        assertTrue(conentMap.isEmpty());
    }
}
