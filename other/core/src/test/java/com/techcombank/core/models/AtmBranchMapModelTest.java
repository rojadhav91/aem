
package com.techcombank.core.models;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.RunmodeService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.techcombank.core.models.AtmBranchMapModel.PROD_API_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

/**
 * The Class AtmBranchMapModelTest to perform junit for AtmBranchMapModel
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class AtmBranchMapModelTest {

    private final AemContext ctx = new AemContext();

    private AtmBranchMapModel atmBranchMapModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private RunmodeService runmodeService;

    @Mock
    private Resource resource;

    @Mock
    private Page page;

    @Mock
    private GenericList genericList;

    @Mock
    private ResourceResolverService resourceResolverService;

    protected static final String GENERIC_LIST_PATH = "/acs-commons/lists/system-configs/google-map";
    protected static final String ETC = "/etc";
    protected static final String PROD = "prod";

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.ATM_BRANCH_MAP_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(AtmBranchMapModel.class);
        atmBranchMapModel = getModel("/content/jcr:content/root/container/atmbranchmap");
        PrivateAccessor.setField(atmBranchMapModel, "runmodeService", runmodeService);
        PrivateAccessor.setField(atmBranchMapModel, "resourceResolverService", resourceResolverService);
    }

    private AtmBranchMapModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        AtmBranchMapModel atmBranchMap = res.adaptTo(AtmBranchMapModel.class);
        return atmBranchMap;
    }

    @Test
    void testFindBranchSection() {
        assertEquals("Personal Customer", atmBranchMapModel.getPersonalLabel());
        String expPersonalIcon = "/content/dam/techcombank/images/ATM.svg";
        assertEquals(expPersonalIcon, atmBranchMapModel.getPersonalIcon());
        assertEquals(expPersonalIcon, atmBranchMapModel.getPersonalIconWebImagePath());
        assertEquals(expPersonalIcon, atmBranchMapModel.getPersonalIconMobileImagePath());
        assertEquals("personalAlt", atmBranchMapModel.getPersonalIconAlt());
        assertEquals("Business Customer", atmBranchMapModel.getBusinessLabel());
        String expBusinessIcon = "/content/dam/techcombank/images/Money.svg";
        assertEquals(expBusinessIcon, atmBranchMapModel.getBusinessIcon());
        assertEquals(expBusinessIcon, atmBranchMapModel.getBusinessIconWebImagePath());
        assertEquals(expBusinessIcon, atmBranchMapModel.getBusinessIconMobileImagePath());
        assertEquals("businessAlt", atmBranchMapModel.getBusinessIconAlt());
        assertEquals("Locate your nearest branch and book a visit", atmBranchMapModel.getTitle());
    }

    @Test
    void testSearchSection() {
        String expSearchIcon = "/content/dam/techcombank/images/Frame.svg";
        assertEquals(expSearchIcon, atmBranchMapModel.getSearchIcon());
        assertEquals(expSearchIcon, atmBranchMapModel.getSearchIconWebImagePath());
        assertEquals(expSearchIcon, atmBranchMapModel.getSearchIconMobileImagePath());
        assertEquals("searchAlt", atmBranchMapModel.getSearchIconAlt());
        assertEquals("Enter the address to search", atmBranchMapModel.getSearchPlaceHolder());
        assertEquals("Select all", atmBranchMapModel.getSelectDropdown());
        assertEquals("Your nearest branch", atmBranchMapModel.getBranchDropdown());
        assertEquals("District", atmBranchMapModel.getDistrictLabel());
    }

    @Test
    void testATMCDMBranchSection() {
        assertEquals("ATM", atmBranchMapModel.getAtmLabel());
        String expAtmIcon = "/content/dam/techcombank/images/Western_Union_tab.svg";
        assertEquals(expAtmIcon, atmBranchMapModel.getAtmIcon());
        assertEquals(expAtmIcon, atmBranchMapModel.getAtmIconWebImagePath());
        assertEquals(expAtmIcon, atmBranchMapModel.getAtmIconMobileImagePath());
        assertEquals("atmAlt", atmBranchMapModel.getAtmIconAlt());
        assertEquals("CDM", atmBranchMapModel.getCdmLabel());
        String expCdmIcon = "/content/dam/techcombank/images/bank.svg";
        assertEquals(expCdmIcon, atmBranchMapModel.getCdmIcon());
        assertEquals(expCdmIcon, atmBranchMapModel.getCdmIconWebImagePath());
        assertEquals(expCdmIcon, atmBranchMapModel.getCdmIconMobileImagePath());
        assertEquals("cdmAlt", atmBranchMapModel.getCdmIconAlt());
        assertEquals("Branch", atmBranchMapModel.getBranchLabel());
        String expBranchIcon = "/content/dam/techcombank/images/Credit_Card.svg";
        assertEquals(expBranchIcon, atmBranchMapModel.getBranchIcon());
        assertEquals(expBranchIcon, atmBranchMapModel.getBranchIconWebImagePath());
        assertEquals(expBranchIcon, atmBranchMapModel.getBranchIconMobileImagePath());
        assertEquals("branchAlt", atmBranchMapModel.getBranchIconAlt());
    }

    @Test
    void testResultSection() {
        assertEquals("Found 241 ATM suitable for you", atmBranchMapModel.getResultMessage());
        assertEquals("No Options", atmBranchMapModel.getNoOptionMessage());
        assertEquals("Back to the list", atmBranchMapModel.getBackLabel());
        assertEquals("Hotline", atmBranchMapModel.getHotlineLabel());
        assertEquals("Address:", atmBranchMapModel.getAddressLabel());
        assertEquals("Get Direction", atmBranchMapModel.getDirectionLabel());
    }

    @Test
    void testGoogleMapValue() {
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        List<GenericList.Item> mockItems = new ArrayList<>();
        GenericList.Item mockItem = mock(GenericList.Item.class);
        lenient().when(mockItem.getValue()).thenReturn("ItemValue");
        mockItems.add(mockItem);
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.ATM_BRANCH_MAP_JSON, "/etc");
        ctx.addModelsForClasses(AtmBranchMapModel.class);
        lenient().when(resourceResolver.getResource(ETC + GENERIC_LIST_PATH)).thenReturn(resource);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.adaptTo(GenericList.class)).thenReturn(genericList);
        lenient().when(runmodeService.getEnvironmentName()).thenReturn(PROD);
        lenient().when(page.adaptTo(GenericList.class).getItems()).thenReturn(mockItems);
        lenient().when(mockItems.get(0).getTitle()).thenReturn(PROD_API_KEY);
        assertTrue(StringUtils.isNotEmpty(atmBranchMapModel.getGoogleMapValue()));
    }

}
