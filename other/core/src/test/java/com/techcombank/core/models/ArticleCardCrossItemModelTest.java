package com.techcombank.core.models;


import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ArticleCardCrossItemModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private ArticleCardCrossItemModel model;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.ARTICLECARDCROSSITEM, "/articleCardCrossItem");
        ctx.addModelsForClasses(ArticleCardCrossItemModel.class);
        model = getModel(TestConstants.ARTICLECARDCROSSITEMCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private ArticleCardCrossItemModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(ArticleCardCrossItemModel.class);
    }

    private void mockObjects() {

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImagePath());
        assertEquals(model.getImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImagePath());
    }

    @Test
    void getImageAlt() {
        final String expected = "text";
        assertEquals(expected, model.getImageAlt());
    }

    @Test
    void getCardTitle() {
        final String expected = "text";
        assertEquals(expected, model.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "richtext";
        assertEquals(expected, model.getCardDescription());
    }

    @Test
    void getCtaLink() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getCtaLink());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = true;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit",
            model.getWebInteractionType());
    }

}
