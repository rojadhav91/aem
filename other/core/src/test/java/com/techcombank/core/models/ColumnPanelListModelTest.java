package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ColumnPanelListModelTest {

    private final AemContext ctx = new AemContext();

    private ColumnPanelListModel columnPanelListModel;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.COLUMNPANELLISTITEM, "/columnpanellist");
        ctx.addModelsForClasses(ColumnPanelListModel.class);
        columnPanelListModel = getModel("/columnpanellist/columnpanel/cardLists");
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        valueMap.put("jcr:title", "Column Control");
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private ColumnPanelListModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        ColumnPanelListModel columnpanel = res.adaptTo(ColumnPanelListModel.class);
        return columnpanel;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getIconImage() {
        final String expected = "/content/dam/techcombank/dev/images/Frame_46457_2a550f867f_HI.svg";
        assertEquals(expected, columnPanelListModel.getIconImage());
    }

    @Test
    void getIconImageAltText() {
        final String expected = "health-insurance";
        assertEquals(expected, columnPanelListModel.getIconImageAltText());
    }

    @Test
    void getCategoryTitle() {
        final String expected = "Health Insurence";
        assertEquals(expected, columnPanelListModel.getCategoryTitle());
    }

    @Test
    void getCardItems() {
        assertEquals(TestConstants.LIST_SIZE_TWO, columnPanelListModel.getCardItems().size());

    }
}
