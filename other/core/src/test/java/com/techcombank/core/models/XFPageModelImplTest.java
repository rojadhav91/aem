package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.models.impl.XFPageModelImpl;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class XFPageModelImplTest {

    private AemContext context = TCBAemContext.newAemContext();
    private XFPageModelImpl xfPageModel;
    private Page currentPage;
    private PageManager pageManager;
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setup() {
        context.load().json(TestConstants.JSON.PERSONAL_BANKING_XF_PAGE,
                TestConstants.CONTENT.PERSONAL_BANKING_XF_PAGE);
        context.currentResource(
                TestConstants.CONTENT.PERSONAL_BANKING_XF_PAGE + TestConstants.SLASH + TestConstants.JCR_CONTENT);
        xfPageModel = context.request().adaptTo(XFPageModelImpl.class);
    }

    private void mockObjectsUpdateResource() {

        context.currentResource(TestConstants.CONTENT.PERSONAL_BANKING_XF_PAGE + "/insider" + TestConstants.SLASH
                + TestConstants.JCR_CONTENT);

        currentPage = mock(Page.class);
        context.registerService(Page.class, currentPage);

        pageManager = mock(PageManager.class);
        context.registerService(PageManager.class, pageManager);

        resourceResolver = mock(ResourceResolver.class);
        context.registerService(ResourceResolver.class, resourceResolver);

        xfPageModel = context.request().adaptTo(XFPageModelImpl.class);
        lenient().when(pageManager.getPage(Mockito.any())).thenReturn(currentPage);
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
    }

    @Test
    void testXfHeader() {
        String expected = "/content/experience-fragments/techcombank/us/en/site/header/master";
        assertEquals(expected, xfPageModel.getHeaderXF());
    }

    @Test
    void testXfFooter() {
        String expected = "/content/experience-fragments/techcombank/us/en/site/footer/master";
        assertEquals(expected, xfPageModel.getFooterXF());
    }

    @Test
    void testXfHeaderParentPage() {
        mockObjectsUpdateResource();
        String expected = "/content/experience-fragments/techcombank/us/en/site/header/master";
        assertEquals(expected, xfPageModel.getHeaderXF());
    }

    @Test
    void testXfFooterParentPage() {
        mockObjectsUpdateResource();
        String expected = "/content/experience-fragments/techcombank/us/en/site/footer/master";
        assertEquals(expected, xfPageModel.getFooterXF());
    }

    @Test
    void testXfEmptyPath() {
        mockObjectsUpdateResource();
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(null);
        String expected = StringUtils.EMPTY;
        assertEquals(expected, xfPageModel.getFooterXF());
        assertEquals(expected, xfPageModel.getHeaderXF());
    }

}
