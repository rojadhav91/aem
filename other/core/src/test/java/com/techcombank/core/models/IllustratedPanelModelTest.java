package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static junitx.framework.Assert.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
 class IllustratedPanelModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    private IllustratedPanelModel illustratedPanelModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(IllustratedPanelModel.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH
                + TestConstants.ILLUSTRATED_PANEL_BENEFITS_JSON, "/illustratedPanel");
        aemContext.currentResource(TestConstants.ILLUSTRATED_PANEL_BENEFITS_COMP);
        Resource resource = aemContext.request().getResource();
        illustratedPanelModel = resource.adaptTo(IllustratedPanelModel.class);
    }

    @Test
    void getBeforeText() {
        final String expected = "This is before text";
        assertEquals(expected, illustratedPanelModel.getBeforeText());
    }

    @Test
    void getCenterImage() {
        final String expected = "/content/dam/techcombank/brand-assets/images/TCB image.png/"
                + "jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, illustratedPanelModel.getCenterImage());
    }

    @Test
    void getALtText() {
        final String expected = "Image alt text";
        assertEquals(expected, illustratedPanelModel.getAltText());
    }

    @Test
    void getAfterText() {
        final String expected = "This is after text";
        assertEquals(expected, illustratedPanelModel.getAfterText());
    }

    @Test
    void getBeforeLabel() {
        final String expected = "Panel before rate label";
        assertEquals(expected, illustratedPanelModel.getBeforeLabel());
    }

    @Test
    void getAfterLabel() {
        final String expected = "Panel after rate label";
        assertEquals(expected, illustratedPanelModel.getAfterLabel());
    }

    @Test
    void getBottomAlignment() {
        final String expected = "center";
        assertEquals(expected, illustratedPanelModel.getBottomAlignment());
    }

    @Test
    void getIcon() {
        final String expected = "/content/dam/techcombank/brand-assets/images/money_0f9fe50e94.svg";
        assertEquals(expected, illustratedPanelModel.getIcon());
    }

    @Test
    void getIconAlt() {
        final String expected = "Icon alternate text";
        assertEquals(expected, illustratedPanelModel.getIconAlt());
    }

    @Test
    void getBottomText() {
        final String expected = "Bottom description text";
        assertEquals(expected, illustratedPanelModel.getBottomText());
    }

    @Test
    void getValueText() {
        final String expected = "Value label text";
        assertEquals(expected, illustratedPanelModel.getValueText());
    }

    @Test
    void getValueAmount() {
        final String expected = "1234";
        assertEquals(expected, illustratedPanelModel.getValueAmount());
    }

    @Test
    void getCenterImageMobile() {
        final String expected = "/content/dam/techcombank/brand-assets/images/TCB image.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, illustratedPanelModel.getCenterImageMobile());
    }

    @Test
    void getIconMobile() {
        final String expected = "/content/dam/techcombank/brand-assets/images/money_0f9fe50e94.svg";
        assertEquals(expected, illustratedPanelModel.getIconMobile());
    }
}
