package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ExtendedTitleTest {

    private final AemContext aemContext = TCBAemContext.newAemContext();
    private ExtendedTitle title;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ExtendedTitle.class);
        aemContext.load().json(TestConstants.TITLE, "/title");
        aemContext.currentResource(TestConstants.TITLECOMPONENT);
        Resource resource = aemContext.request().getResource();
        title = resource.adaptTo(ExtendedTitle.class);
    }

    @Test
    void getId() {
        final String expected = "titleComponent";
        assertEquals(expected, title.getId());
    }

    @Test
    void getText() {
        final String expected = "Techcombank";
        assertEquals(expected, title.getText());
    }

    @Test
    void getType() {
        final String expected = "H2";
        assertEquals(expected, title.getType());
    }

    @Test
    void getAlignment() {
        final String expected = "Left";
        assertEquals(expected, title.getAlignment());
    }

    @Test
    void getSeparatorStyle() {
        final String expected = "true";
        assertEquals(expected, title.getSeparatorStyle());
    }

    @Test
    void getFontColorStyle() {
        final String expected = "#ED1C24";
        assertEquals(expected, title.getFontColorStyle());
    }
}
