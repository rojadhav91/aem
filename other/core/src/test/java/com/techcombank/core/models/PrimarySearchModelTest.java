package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class PrimarySearchModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private PrimarySearchModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(PrimarySearchModel.class);
        aemContext.load().json(TestConstants.PRIMARYSEARCH, "/primarySearch");
        aemContext.currentResource(TestConstants.PRIMARYSEARCHCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(PrimarySearchModel.class);
    }

    @Test
    void getResultSlug() {
        final String expected = "text";
        assertEquals(expected, model.getResultSlug());
    }

    @Test
    void getInputPlaceHolder() {
        final String expected = "text";
        assertEquals(expected, model.getInputPlaceHolder());
    }

    @Test
    void getInputSearchIcon() {
        final String expected = "path";
        assertEquals(expected, model.getInputSearchIcon());
    }

    @Test
    void getHistoryLabel() {
        final String expected = "text";
        assertEquals(expected, model.getHistoryLabel());
    }

    @Test
    void getUsefulLinksTitle() {
        final String expected = "text";
        assertEquals(expected, model.getUsefulLinksTitle());
    }

    @Test
    void getUsefulLinksItemList() {
        assert model.getUsefulLinksItemList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getUsefulLinksItemList().size());
    }

}
