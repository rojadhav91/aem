package com.techcombank.core.models.impl;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class FinancialFiltersModelImplTest {
    @Mock
    private ResourceResolver resolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @InjectMocks
    private FinancialFiltersModelImpl financialFiltersModelImpl;

    private FinancialFiltersModelImpl model;

    private final AemContext aemContext = AppAemContext.newAemContext();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInitWhenFindThrowsExceptionThenTagsMapIsEmpty2() {
        aemContext.addModelsForClasses(FinancialFiltersModelImpl.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.FINANCIALHIGHLIGHTS_JSON, "/content");
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.FINANCIALHIGHLIGHTS_JSON,
                TestConstants.CONTENT_QUARTER_TAG);
        aemContext.currentResource(TestConstants.HIGHLIGHTS);
        Resource resource = aemContext.request().getResource();
        tag = Mockito.mock(Tag.class);
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);

        lenient().when(tagManager.resolve("techcombank:quarters/q1")).thenReturn(tag);
        lenient().when(tag.getName()).thenReturn("q1");

        model = resource.adaptTo(FinancialFiltersModelImpl.class);
        assert model != null;
        assertNotNull(model.getHeading());
        assertNotNull(model.getQuarters());
        assertNotNull(model.getYearLabel());
        assertNotNull(model.getQuarterLabel());
        assertNotNull(model.getFiltersLabel());
        assertNotNull(model.getSelectLabel());
        assertNotNull(model.getApplyFiltersLabel());
        assertNotNull(model.getHighlights());
        assertNotNull(model.getHighlights().get(0).getYear());
        assertNotNull(model.getHighlights().get(0).getPath());

    }
}

