package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static junitx.framework.ComparableAssert.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class SocialShareInfoModelTest {

    private SocialShareInfoModel socialShareInfoModel;

    @Mock
    private SocialShareInfoItemModel socialShareInfoItemModel1;

    @Mock
    private SocialShareInfoItemModel socialShareInfoItemModel2;

    private List<SocialShareInfoItemModel> socialShareItems = new ArrayList<>();

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        socialShareInfoModel = new SocialShareInfoModel();
        socialShareItems.add(socialShareInfoItemModel1);
        socialShareItems.add(socialShareInfoItemModel2);
        PrivateAccessor.setField(socialShareInfoModel, "socialShareItems", socialShareItems);
    }

    @Test
    void getSocialShareItems() {
        assertEquals(TestConstants.LIST_SIZE_TWO, socialShareInfoModel.getSocialShareItems().size());
    }
}
