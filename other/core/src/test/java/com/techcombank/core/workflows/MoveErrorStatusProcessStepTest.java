package com.techcombank.core.workflows;

import com.adobe.acs.commons.notifications.InboxNotification;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MoveErrorStatusProcessStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private Workflow workflow;

    @Mock
    private InboxNotification inboxNotification;

    @Mock
    private EmailService emailService;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @InjectMocks
    private MoveErrorStatusProcessStep moveErrorStatusProcessStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workflowData.getPayload()).thenReturn("/content/sample/page");
        when(workflowMetadataMap.containsKey(TCBConstants.EMAIL_ENABLE)).thenReturn(true);
        when(workflowMetadataMap.containsKey(TCBConstants.MESSAGE)).thenReturn(true);
        when(workflowMetadataMap.get(TCBConstants.MESSAGE, String.class)).thenReturn("Move Error Status");
        when(workflowMetadataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(workflowMetadataMap.containsKey(TCBConstants.WORKFLOW_TITLE)).thenReturn(true);
        when(workflowMetadataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("sample@example.com");
        when(workflow.getInitiator()).thenReturn("initiator@example.com");
        when(inboxNotification.getTitle()).thenReturn("Approval Title");
    }

    @Test
    void testExecuteEmailEnabled() throws WorkflowException {
        moveErrorStatusProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_4)).getWorkflowData();
    }

}
