package com.techcombank.core.models;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MediaCarouselItemModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private MediaCarouselItemModel model;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.MEDIACAROUSELITEM, "/mediaCarouselItem");
        ctx.addModelsForClasses(MediaCarouselItemModel.class);
        model = getModel(TestConstants.MEDIACAROUSELITEMCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private MediaCarouselItemModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(MediaCarouselItemModel.class);
    }

    private void mockObjects() {

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);
    }

    @Test
    void getMediaType() {
        final String expected = "select-item";
        assertEquals(expected, model.getMediaType());
    }

    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImagePath());
        assertEquals(model.getImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImagePath());
    }

    @Test
    void getAltText() {
        final String expected = "text";
        assertEquals(expected, model.getAltText());
    }

    @Test
    void getVideoLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getVideoLinkUrl());
    }

    @Test
    void getVideoLinkWebInteractionType() {
        assertEquals("exit",
            model.getVideoLinkWebInteractionType());
    }

    @Test
    void getCarouselTitleText1() {
        final String expected = "text";
        assertEquals(expected, model.getCarouselTitleText1());
    }

    @Test
    void getCarouselTitleText2() {
        final String expected = "text";
        assertEquals(expected, model.getCarouselTitleText2());
    }

    @Test
    void getSubTitleText() {
        final String expected = "text";
        assertEquals(expected, model.getSubTitleText());
    }

    @Test
    void getDescriptionText() {
        final String expected = "text";
        assertEquals(expected, model.getDescriptionText());
    }

    @Test
    void getLinkText() {
        final String expected = "text";
        assertEquals(expected, model.getLinkText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "https://www.google.com";
        assertEquals(expected, model.getLinkUrl());
    }

    @Test
    void getLinkUrlWebInteractionType() {
        assertEquals("exit",
            model.getLinkUrlWebInteractionType());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = true;
        assertEquals(expected, model.isNoFollow());
    }

    @Test
    void isUseAsHighlight() {
        final boolean expected = true;
        assertEquals(expected, model.isUseAsHighlight());
    }

}
