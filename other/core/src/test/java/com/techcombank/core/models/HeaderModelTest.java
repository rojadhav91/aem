package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import java.util.List;

import static org.mockito.Mockito.lenient;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMMode;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.utils.PlatformUtils;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class PrimaryNavigationModelTest to perform junit for
 * HeaderNavigationModel Model
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class HeaderModelTest {

    private final AemContext ctx = new AemContext();

    private HeaderModel headerNavigation;

    private Resource resource;

    private ResourceResolver resourceResolver;

    private Page currentPage;

    private PageManager pageManager;

    private SlingHttpServletRequest request;

    @BeforeEach
    void setUp() throws Exception {

        ctx.load().json(TestConstants.HEADER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(HeaderModel.class);
        headerNavigation = getModel("/content/techcombank/web/vn/vi/personal/jcr:content/root/container/header");
    }

    private HeaderModel getModel(final String currentResource) {
        ctx.request().setAttribute(WCMMode.class.getName(), WCMMode.EDIT);
        ctx.currentResource(currentResource);
        HeaderModel headerNav = ctx.request().adaptTo(HeaderModel.class);
        return headerNav;
    }

    private void mockObjects() {
        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        currentPage = mock(Page.class);
        ctx.registerService(Page.class, currentPage);

        pageManager = mock(PageManager.class);
        ctx.registerService(PageManager.class, pageManager);

        request = mock(SlingHttpServletRequest.class);
        ctx.registerService(SlingHttpServletRequest.class, request);
    }

    private void mockPageObjects(final String pagePath) {
        Resource pageResource = ctx.resourceResolver().getResource(pagePath);
        Page page = pageResource.adaptTo(Page.class);
        ctx.currentPage(page);
    }

    @Test
    void testGetAllProductsURL() {
        String expected = "/content/techcombank/web.html";
        assertEquals(expected, headerNavigation.getAllProductsURL());
        assertEquals("exit", headerNavigation.getAllProductsURLInteractionType());
    }

    @Test
    void testTurnOnSearchOnNavigation() {
        boolean expected = true;
        assertEquals(expected, headerNavigation.isTurnOnSearchOnNavigation());
    }

    @Test
    void testLandingPageMenu() {
        List<LandingPageMenuModel> landingPageMenu = headerNavigation.getLandingPageMenu();
        assertEquals("/content/techcombank/web/vn/vi.html", landingPageMenu.get(0).getLandingPageURL());
        assertEquals("other", landingPageMenu.get(0).getLandingPageURLInteractionType());
        assertEquals("Personal", landingPageMenu.get(0).getLandingMenuTitle());
        assertEquals("_self", landingPageMenu.get(0).getLandingTarget());
    }

    @Test
    void testLoginData() {
        List<DropDownLoginModel> loginMenu = headerNavigation.getLoginData();
        assertEquals("/content/techcombank/web/vn/testpageed.html", loginMenu.get(0).getDropdownLoginURL());
        assertEquals("other", loginMenu.get(0).getDropdownLoginURLInteractionType());
        assertEquals("Personal Banking", loginMenu.get(0).getDropdownLoginText());
    }

    @Test
    void testDiscoverMenu() {
        List<DiscoverLabelMenuModel> discoverMenu = headerNavigation.getDiscoverMenu();
        assertEquals("/content/we-retail/ch", discoverMenu.get(0).getDiscoverMenuPageURL());
        assertEquals("exit", discoverMenu.get(0).getDiscoverMenuPageURLInteractionType());
        assertEquals("INVESTORS", discoverMenu.get(0).getDiscoverMenuLabel());
        assertEquals("_blank", discoverMenu.get(0).getDiscoverTarget());
    }

    @Test
    void testRightNavigationMenu() {
        List<NavigationMenuModel> navigationMenu = headerNavigation.getRightNavigationMenu();
        assertEquals("/content/techcombank/web/vn/vi.html", navigationMenu.get(0).getNavigationPageURL());
        assertEquals("other", navigationMenu.get(0).getNavigationPageURLInteractionType());
        assertEquals("Toools & Utilities", navigationMenu.get(0).getNavigationLabel());
        assertEquals("_self", navigationMenu.get(0).getNavTarget());
    }

    @Test
    void testDiscoverLabel() {
        String expected = "Explore";
        assertEquals(expected, headerNavigation.getDiscoverLabel());
    }

    @Test
    void testDefaultMenuLabel() {
        String expected = "Go To";
        assertEquals(expected, headerNavigation.getDefaultMenuLabel());
    }

    @Test
    void testAllProductsLink() {
        String expected = "all Products Links";
        assertEquals(expected, headerNavigation.getAllProductsLink());
    }

    @Test
    void testEnableHomePage() {
        assertTrue(headerNavigation.isEnableHomePage());
    }

    @Test
    void testGetLanguageDetails() {
        headerNavigation = getModel("/content/techcombank/web/vn/vi/personal");
        mockObjects();
        String expectedVIURL = "/content/techcombank/web/vn/vi/vitalias.html";
        String expectedENURL = "/content/techcombank/web/vn/en/personal.html";
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        assertEquals(TestConstants.LANG_VI, headerNavigation.getLanguageDetails().get(0).getLanguage());
        assertEquals(TestConstants.LANG_EN, headerNavigation.getLanguageDetails().get(1).getLanguage());
        assertEquals(expectedVIURL, headerNavigation.getLanguageDetails().get(0).getLanguageUrl());
        assertEquals(expectedENURL, headerNavigation.getLanguageDetails().get(1).getLanguageUrl());
    }

    @Test
    void testPageTiltle() {
        headerNavigation = getModel("/content/techcombank/web/vn/vi/personal");
        mockObjects();
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        String languagePath = "/content/techcombank/web/vn/vi";
        lenient().when(resource.getPath()).thenReturn(languagePath);
        lenient().when(currentPage.getTitle()).thenReturn(TestConstants.LANG_VI);
        lenient().when(pageManager.getContainingPage(languagePath)).thenReturn(currentPage);
        assertEquals(TestConstants.LANG_VI, PlatformUtils.getLanguageTitle(resource, false, resourceResolver));
    }

    @Test
    void testAllProductEnabledPersonalTemp() {
        mockObjects();
        String path = "/content/techcombank/web/vn/vi/personal";
        mockPageObjects(path);
        headerNavigation = getModel("/content/techcombank/web/vn/vi/personal");
        assertTrue(headerNavigation.isAllProductEnabled());
    }

    @Test
    void testAllProductEnabledBusinessTemp() {
        mockObjects();
        String path = "/content/techcombank/web/vn/en/personal";
        mockPageObjects(path);
        headerNavigation = getModel("/content/techcombank/web/vn/en/personal");
        assertFalse(headerNavigation.isAllProductEnabled());
    }

    @Test
    void testAllProductEnabledContentTemp() {
        mockObjects();
        String path = "/content/techcombank/web/vn/us";
        mockPageObjects(path);
        headerNavigation = getModel("/content/techcombank/web/vn/us");
        assertTrue(headerNavigation.isAllProductEnabled());
    }

    @Test
    void testAllProductEnabledOtherTemp() {
        mockObjects();
        String path = "/content/techcombank/web/vn/jp";
        mockPageObjects(path);
        headerNavigation = getModel("/content/techcombank/web/vn/jp");

        assertFalse(headerNavigation.isAllProductEnabled());
    }

    /* Secondary Nav test */
    @Test
    void testGetLogoImage() {
        String getLogoImage = "/content/dam/techcombank/images/TechComBankLogoRedColored.svg";
        assertEquals(getLogoImage, headerNavigation.getLogoImage());
        assertEquals(getLogoImage, headerNavigation.getLogoWebImagePath());
    }

    @Test
    void testGetLogoMobileImage() {
        String getLogoMobileImage = "/content/dam/techcombank/images/Western_Union_tab.svg";
        assertEquals(getLogoMobileImage, headerNavigation.getLogoMobileImage());
        assertEquals(getLogoMobileImage, headerNavigation.getLogoMobileImagePath());
    }

    @Test
    void testGetLogoImageLink() {
        String getLogoImageLink = headerNavigation.getLogoImageLink();
        assertEquals("/content/techcombank/web/vn/vi.html", getLogoImageLink);
        assertEquals("other", headerNavigation.getLogoImageLinkInteractionType());
    }

    @Test
    void testGetLogoImageAltText() {
        String getLogoImageAltText = headerNavigation.getLogoImageAltText();
        assertEquals("TechcomBank Logo", getLogoImageAltText);
    }

    @Test
    void testGetLoginButtonColour() {
        String getLoginButtonColour = headerNavigation.getLoginButtonColour();
        assertEquals("rgb(242,239,255)", getLoginButtonColour);
    }

    @Test
    void testGetLoginLabelText() {
        String getLoginLabelText = headerNavigation.getLoginLabelText();
        assertEquals("Log in", getLoginLabelText);
    }

    @Test
    void testGetLoginLabelLinkUrl() {
        String getLoginLabelLinkUrl = headerNavigation.getLoginLabelLinkUrl();
        assertEquals("/content/techcombank/web/vn/vi.html", getLoginLabelLinkUrl);
        assertEquals("other", headerNavigation.getLoginLabelLinkUrlInteractionType());
    }

    @Test
    void testGetOpeninnewtab() {
        String getOpeninnewtab = headerNavigation.getOpeninnewtab();
        assertEquals("_blank", getOpeninnewtab);
    }

    @Test
    void testSecondaryMenuNavLevel1() {
        List<SecondaryMenuNavLevel1> secondaryMenuNavLevel1 = headerNavigation.getSecondaryMenuNavLevel1();
        assertEquals("Spend", secondaryMenuNavLevel1.get(0).getMenuLabelText());
        assertEquals("/content/techcombank/web/vn/vi.html", secondaryMenuNavLevel1.get(0).getMenuLinkUrl());
        assertEquals("other", secondaryMenuNavLevel1.get(0).getMenuLinkUrlInteractionType());
        assertEquals("_blank", secondaryMenuNavLevel1.get(0).getOpeninnewtab());

        List<SecondaryMenuNavLevel2> secondaryMenuNavLevel2 = secondaryMenuNavLevel1.get(0).getSecondaryMenuNavLevel2();
        assertEquals("Accounts", secondaryMenuNavLevel2.get(0).getSubTabMenuLabelText());
        assertEquals("/content/techcombank/web/vn/vi.html", secondaryMenuNavLevel2.get(0).getSubTabMenuLinkUrl());
        assertEquals("other", secondaryMenuNavLevel2.get(0).getSubTabMenuLinkUrlInteractionType());
        assertEquals("_blank", secondaryMenuNavLevel2.get(0).getOpeninnewtab());
        assertEquals("_blank", secondaryMenuNavLevel2.get(0).getSubMenuTargetLeft());
        assertEquals("_blank", secondaryMenuNavLevel2.get(0).getSubMenuTargetRight());
        assertEquals("View Less", secondaryMenuNavLevel2.get(0).getViewLessSubTab());
        assertEquals("View All", secondaryMenuNavLevel2.get(0).getViewAllSubTab());
        assertEquals("/content/dam/techcombank/images/Business_b2d68e7312.svg",
                secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkIcon());
        assertEquals("/content/dam/techcombank/images/Business_b2d68e7312.svg",
                secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkMobileIconPath());
        assertEquals("/content/dam/techcombank/images/Business_b2d68e7312.svg",
                secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkWebIconPath());
        assertEquals("abc", secondaryMenuNavLevel2.get(0).getIconImageAltText());
        assertEquals("Promotion", secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkText());
        assertEquals("/content/techcombank/web/vn/vi.html", secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkUrl());
        assertEquals("other", secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkUrllInteractionType());
        assertEquals("Demo", secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkTextRight());
        assertEquals("/content/techcombank/web/vn/vi.html",
                secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkUrlRight());
        assertEquals("other", secondaryMenuNavLevel2.get(0).getSubMenuBottomLinkUrlRightInteractionType());

    }

    @Test
    void testSecondaryMenuNavLevel3() {
        List<SecondaryMenuNavLevel1> secondaryMenuNavLevel1 = headerNavigation.getSecondaryMenuNavLevel1();
        List<SecondaryMenuNavLevel2> secondaryMenuNavLevel2 = secondaryMenuNavLevel1.get(0).getSecondaryMenuNavLevel2();
        List<SecondaryMenuNavLevel3> secondaryMenuNavLevel3 = secondaryMenuNavLevel2.get(0).getSecondaryMenuNavLevel3();
        assertEquals("Current account", secondaryMenuNavLevel3.get(0).getSubMenuLabelText());
        assertEquals("/content/techcombank/web/vn/vi.html", secondaryMenuNavLevel3.get(0).getSubMenuLinkUrl());
        assertEquals("other", secondaryMenuNavLevel3.get(0).getSubMenuLinkUrlInteractionType());
        assertEquals("Allow Drawing money directly from current account",
                secondaryMenuNavLevel3.get(0).getSubTabMenuLabelDesc());
        assertEquals("/content/dam/techcombank/images/Frame.svg", secondaryMenuNavLevel3.get(0).getSubMenuIconImage());
        assertEquals("-", secondaryMenuNavLevel3.get(0).getImageAltText());
        assertEquals("_blank", secondaryMenuNavLevel3.get(0).getOpeninnewtab());
        assertEquals("/content/dam/techcombank/images/Frame.svg",
                secondaryMenuNavLevel3.get(0).getSubMenuWebIconImagePath());
        assertEquals("/content/dam/techcombank/images/Frame.svg",
                secondaryMenuNavLevel3.get(0).getSubMenuIconMobileImagePath());
    }

    @Test
    void testGetCurrentActivePage() {
        String expected = "/content/techcombank/web/vn/vi/personal.html";
        assertEquals(expected, headerNavigation.getCurrentActivePage());
    }


    @Test
    void testActivePageMenuListed() {
        assertTrue(headerNavigation.isActivePageMenuListed());
    }

    @Test
    void testGetCurrentActiveAliasPage() {
        String expected = "/content/techcombank/web/vn/vi/vitalias.html";
        assertEquals(expected, headerNavigation.getCurrentAliasActivePage());
    }

    @Test
    void testActiveSecondaryMenuPagePath() {
        mockObjects();
        String path = "/content/techcombank/web/vn/en/personal/borrow/real-home-loan";
        String primaryMenuPath = "/content/techcombank/web/vn/en/personal/borrow";
        String secondaryMenuPath = "/content/techcombank/web/vn/en/personal";
        mockPageObjects(path);
        headerNavigation = getModel("/content/techcombank/web/vn/us");
        lenient().when(resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        lenient().when(resourceResolver.map(primaryMenuPath + TCBConstants.HTMLEXTENSION))
                .thenReturn(primaryMenuPath + TCBConstants.HTMLEXTENSION);
        lenient().when(resourceResolver.map(secondaryMenuPath + TCBConstants.HTMLEXTENSION))
                .thenReturn(secondaryMenuPath + TCBConstants.HTMLEXTENSION);
        assertEquals(primaryMenuPath + TCBConstants.HTMLEXTENSION,
                headerNavigation.getActiveSecondaryMenuPagePath());
        assertEquals(secondaryMenuPath + TCBConstants.HTMLEXTENSION,
                headerNavigation.getActivePrimaryMenuPagePath());
    }
}
