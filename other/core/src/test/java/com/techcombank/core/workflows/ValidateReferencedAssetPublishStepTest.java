package com.techcombank.core.workflows;

import com.adobe.granite.workflow.PayloadMap;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.PageManager;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.TestConstants;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PropertyIterator;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ValidateReferencedAssetPublishStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private Workflow workflow;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private Asset asset;

    @Mock
    private ReplicationStatus replicationStatus;

    @Mock
    private UserManager userManager;

    @Mock
    private Node node;

    @Mock
    private PayloadMap payloadMap;

    @Mock
    private Iterable<Resource> resItr;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @InjectMocks
    private AssetReferenceSearch assetReferenceSearch;

    @Mock
    private ValueMap valueMap;

    @Mock
    private List<String> unPublishedAsset;

    @Mock
    private PageManager pgMgr;

    @InjectMocks
    private ValidateReferencedAssetPublishStep validateReferencedAssetPublishStep;

    public static final String NODEPATH = "/content/techcombank/sample";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        List<Workflow> workflowList = new ArrayList<Workflow>();
        when(workItem.getMetaDataMap()).thenReturn(metaDataMap);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflowData.getPayload()).thenReturn(NODEPATH);
        when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workflowSession.adaptTo(ResourceResolver.class)).thenReturn(resolver);
        when(resolver.getResource(anyString())).thenReturn(resource);
        when(asset.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
        when(resource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
        when(resource.adaptTo(Asset.class)).thenReturn(asset);
        when(resource.adaptTo(Node.class)).thenReturn(node);
        when(workflowMetadataMap.get(TCBConstants.USER_ID, String.class)).thenReturn("userId");
        when(resolver.adaptTo(UserManager.class)).thenReturn(userManager);
        when(workflowData.getPayload()).thenReturn(NODEPATH);
        when(resolver.adaptTo(PayloadMap.class)).thenReturn(payloadMap);
        when(payloadMap.getWorkflowInstances("", true)).thenReturn(workflowList);
        PropertyIterator pIter = mock(PropertyIterator.class);
        NodeIterator nItr = mock(NodeIterator.class);
        when(node.getProperties()).thenReturn(pIter);
        when(node.getNodes()).thenReturn(nItr);
        PrivateAccessor.setField(validateReferencedAssetPublishStep, "workflowNotificationService",
                workflowNotificationService);
    }

    @Test
    void testUnpublishedReferences() {
        try {
            Method method = ValidateReferencedAssetPublishStep.class.getDeclaredMethod("getUnpublishedReferences",
                    ResourceResolver.class, Map.class);
            method.setAccessible(true);
            Asset asset1 = mock(Asset.class);
            Asset asset2 = mock(Asset.class);
            Map<String, Asset> allref = new HashMap<>();
            allref.put("asset1", asset1);
            allref.put("asset2", asset2);
            when(resource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
            method.invoke(validateReferencedAssetPublishStep, resolver, allref);

            allref = new HashMap<>();
            allref.put("/content/dam/techcombank/rdb-app/data/asset1", asset1);
            allref.put("/content/dam/techcombank/cdb-app/data/asset2", asset2);
            method.invoke(validateReferencedAssetPublishStep, resolver, allref);
            verify(resolver, times(TestConstants.CALL_COUNT_6)).getResource(Mockito.anyString());
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                 | SecurityException e) {
            logger.error("Exception {} {}", e.getMessage(), e);
        }
    }

    @Test
    void testProcessUnPublishedAssets() {
        try {
            Method method = ValidateReferencedAssetPublishStep.class.getDeclaredMethod("processUnPublishedAssets",
                    MetaDataMap.class);
            method.setAccessible(true);
            when(unPublishedAsset.isEmpty()).thenReturn(false);
            when(resource.adaptTo(ReplicationStatus.class)).thenReturn(replicationStatus);
            method.invoke(validateReferencedAssetPublishStep, workflowMetadataMap);
            verify(workflowMetadataMap).put(TCBConstants.ASSET_PUBLISHED, TCBConstants.FALSE);

        } catch (IllegalAccessException | NoSuchMethodException | SecurityException | IllegalArgumentException
                 | InvocationTargetException e) {
            logger.error("Exception {} {}", e.getMessage(), e);
        }
    }

    @Test
    void testExecuteWithPublishedAsset() throws Exception {
        Asset asset1 = mock(Asset.class);
        Asset asset2 = mock(Asset.class);
        Map<String, Asset> allref = new HashMap<>();
        allref.put("asset1", asset1);
        allref.put("asset2", asset2);
        when(replicationStatus.isActivated()).thenReturn(true);

        when(unPublishedAsset.isEmpty()).thenReturn(false);
        List<Resource> resourceList = new ArrayList<>();
        resourceList.add(resource);
        when(resource.getChildren()).thenReturn(resItr);
        Iterator<Resource> itrRes = resourceList.iterator();
        when(resItr.iterator()).thenReturn(itrRes);
        when(resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        List<Workflow> workflowList = new ArrayList<Workflow>();
        workflowList.add(workflow);
        when(workflowData.getPayload()).thenReturn("/var/techcombank/sample");
        when(valueMap.get("root", String.class)).thenReturn("/content/techcombank");
        when(payloadMap.getWorkflowInstances("/content/techcombank", true)).thenReturn(workflowList);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);

        when(valueMap.get("root", String.class)).thenReturn("/content/dam/techcombank/test/data");
        when(payloadMap.getWorkflowInstances("/content/dam/techcombank/test/data", true))
                .thenReturn(workflowList);
        Iterator<Resource> itrRes1 = resourceList.iterator();
        when(resItr.iterator()).thenReturn(itrRes1);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);

        when(workflowData.getPayload()).thenReturn("/content/dam/techcombank/rdb-app/data/asset2-image-new.png");
        when(valueMap.get("root", String.class))
                .thenReturn("/content/dam/techcombank/rdb-app/data/asset2-image-new.png");
        when(payloadMap.getWorkflowInstances("/content/dam/techcombank/rdb-app/data/asset2-image-new.png", true))
                .thenReturn(workflowList);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);

        workflowList.add(workflow);
        when(workflowData.getPayload()).thenReturn("/etc/techcombank/sample");
        Iterator<Resource> itrRes2 = resourceList.iterator();
        when(resItr.iterator()).thenReturn(itrRes2);
        when(valueMap.get("root", String.class)).thenReturn("/content/dam/techcombank/rdb-app/data");
        when(payloadMap.getWorkflowInstances("/content/dam/techcombank/rdb-app/data", true))
                .thenReturn(workflowList);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);

        workflowList.add(workflow);
        when(workflowData.getPayload()).thenReturn("/content/dam/techcombank/test/data");
        when(valueMap.get("root", String.class)).thenReturn("/content/dam/techcombank/test/data");
        when(payloadMap.getWorkflowInstances("/content/dam/techcombank/test/data", true))
                .thenReturn(workflowList);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);

        List<Workflow> workflowList1 = new ArrayList<Workflow>();
        when(workflowData.getPayload()).thenReturn("/content/dam/techcombank/test/data1");
        when(resolver.adaptTo(PageManager.class)).thenReturn(pgMgr);
        when(resolver.findResources("//*[jcr:contains(., '\"/content/dam/techcombank/test/data1\"')]", "xpath"))
                .thenReturn(Collections.emptyIterator());
        when(valueMap.get("root", String.class)).thenReturn("/content/dam/techcombank/test/data1");
        when(metaDataMap.get(TCBConstants.PROCESS_ARGS, String.class)).thenReturn("workflow=unpublish");
        when(payloadMap.getWorkflowInstances("/content/dam/techcombank/test/data1", true))
                .thenReturn(workflowList1);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);
        when(Objects.requireNonNull(replicationStatus).isActivated()).thenReturn(false);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_17)).getWorkflowData();


    }

    @Test
    void testEmptyPayload() throws Exception {
        String payload = StringUtils.EMPTY;
        when(workflowData.getPayload()).thenReturn(payload);
        validateReferencedAssetPublishStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_2)).getWorkflowData();
    }
}
