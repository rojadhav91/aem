package com.techcombank.core.service.impl;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.Articles;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticlesRelatedPostServiceImplTest {

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Session session;

    @InjectMocks
    private ArticlesRelatedPostServiceImpl articleRelatedPostService = new ArticlesRelatedPostServiceImpl();

    private ArticlesRelatedPostServiceImpl.ArticleRelatedPostConfig config;

    @Mock
    private ValueMap valueMap;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Query query;

    private List<Hit> hits = new ArrayList<>();

    @Mock
    private Hit hit1;

    @Mock
    private Hit hit2;

    @Mock
    private Hit hit3;

    @Mock
    private Hit hit4;

    @Mock
    private GregorianCalendar date;

    @Mock
    private SearchResult searchResult;

    @BeforeEach
    void setUp(final AemContext context) {
        MockitoAnnotations.openMocks(this);
        String[] cqtags = {"techcombank:articles/article-type/video", "techcombank:articles/article-type/article"};
        this.config = mock(ArticlesRelatedPostServiceImpl.ArticleRelatedPostConfig.class);
        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(currentPage.getContentResource()).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(currentPage.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(valueMap.get(any(), any())).thenReturn(cqtags);
        lenient().when(valueMap.containsKey(TCBConstants.CQ_TAGS)).thenReturn(true);
        lenient().when(config.getDefaultPath()).thenReturn("/article-page");
        lenient().when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        lenient().when(queryBuilder.createQuery(any(), any())).thenReturn(query);
        lenient().when(query.getResult()).thenReturn(searchResult);
        articleRelatedPostService.activate(config);
    }

    @Test
    void testGetRelatedPostIfPart() throws RepositoryException {
        hits.add(hit1);
        hits.add(hit2);
        hits.add(hit3);
        hits.add(hit4);
        lenient().when(searchResult.getHits()).thenReturn(hits);
        lenient().when(hit1.getPath()).thenReturn("/article-page1");
        lenient().when(hit2.getPath()).thenReturn("/article-page2");
        lenient().when(hit3.getPath()).thenReturn("/article-page3");
        lenient().when(hit4.getPath()).thenReturn("/article-page4");
        lenient().when(resourceResolver.getResource(any())).thenReturn(resource);
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
        lenient().when(valueMap.get("bgBannerImage", StringUtils.EMPTY)).thenReturn("abc.png");
        lenient().when(valueMap.get("bgBannerImgAlt", StringUtils.EMPTY)).thenReturn("altText");
        lenient().when(valueMap.get("articleCreationDate")).thenReturn(date);
        lenient().when(valueMap.get("jcr:description", StringUtils.EMPTY)).thenReturn("desc");
        lenient().when(valueMap.get("pageTitle", StringUtils.EMPTY)).thenReturn("title");
        List<Articles> articlesList =
                articleRelatedPostService.getArticlesRelatedPosts(resource, currentPage, resourceResolver);
        Assert.assertEquals(TestConstants.LIST_SIZE_THREE, articlesList.size());
    }

    @Test
    void testGetRelatedPostElsePart() {
        List<Articles> articlesList =
                articleRelatedPostService.getArticlesRelatedPosts(resource, currentPage, resourceResolver);
        Assert.assertEquals(TestConstants.LIST_SIZE_ZERO, articlesList.size());
    }
}
