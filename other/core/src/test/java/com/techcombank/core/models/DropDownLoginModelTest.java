
package com.techcombank.core.models;

/**
 * The Class DropDownLoginDetailsTest to perform junit for DropDownLoginDetails Model
 */
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.day.cq.wcm.api.WCMMode;
import com.techcombank.core.constants.TestConstants;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class DropDownLoginModelTest {

    private final AemContext ctx = new AemContext();

    private DropDownLoginModel dropDownLoginDetails;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.HEADER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(DropDownLoginModel.class);
        dropDownLoginDetails = getModel(
                "/content/techcombank/web/vn/vi/personal/jcr:content/root/container/header/loginData/item1");
    }

    private DropDownLoginModel getModel(final String currentResource) {
        ctx.request().setAttribute(WCMMode.class.getName(), WCMMode.EDIT);
        ctx.currentResource(currentResource);
        DropDownLoginModel loginDetails = ctx.currentResource().adaptTo(DropDownLoginModel.class);
        return loginDetails;
    }

    @Test
    void testDropdownLoginText() {
        String expected = "Business Banking";
        assertEquals(expected, dropDownLoginDetails.getDropdownLoginText());
    }

    @Test
    void testDropdownLoginURL() {
        String expected = "/content/techcombank/web/vn/vi.html";
        assertEquals(expected, dropDownLoginDetails.getDropdownLoginURL());
        assertEquals("other", dropDownLoginDetails.getDropdownLoginURLInteractionType());
    }

}
