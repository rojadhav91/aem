package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ExtendedEmbedTest {

    private final AemContext aemContext = TCBAemContext.newAemContext();
    private ExtendedEmbed extendedEmbed;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ExtendedEmbed.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.EXTENDEDEMBED, "/extendedEmbed");
        aemContext.currentResource(TestConstants.EXTENDEDEMBEDCOMPONENT);
        Resource resource = aemContext.request().getResource();
        extendedEmbed = resource.adaptTo(ExtendedEmbed.class);
    }

    @Test
    void youtubIdTest() {
        final String expected = "KV7cbL3j1ic";
        assertEquals(expected, extendedEmbed.getYoutubeVideoId());
    }

    @Test
    void youtubWidthTest() {
        final String expected = "300";
        assertEquals(expected, extendedEmbed.getYoutubeWidth());
    }

    @Test
    void youtubHeightTest() {
        final String expected = "300";
        assertEquals(expected, extendedEmbed.getYoutubeHeight());
    }

    @Test
    void youtubAspectRatioTest() {
        final String expected = "5";
        assertEquals(expected, extendedEmbed.getYoutubeAspectRatio());
    }
}
