package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ListViewDocumentLevelOneItemModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    @InjectMocks
    private ListViewDocumentLevelOneItemModel model;


    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(ListViewDocumentLevelOneItemModel.class);
        aemContext.load().json(TestConstants.LISTVIEWDOCUMENTLEVELONE, "/listViewDocumentItemsFirstLevel");
        aemContext.currentResource(TestConstants.LISTVIEWDOCUMENTEVELONECOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListViewDocumentLevelOneItemModel.class);
    }

    @Test
    void getDocumentTitle() {
        final String expected = "Title";
        assertEquals(expected, model.getDocumentTitle());
    }

    @Test
    void getDocumentPath() {
        final String expected = "/listViewDocumentItemsFirstLevel/listViewDocumentItemsFirstLevel.html";
        assertEquals(expected, model.getDocumentPath());
    }

    @Test
    void getLabel() {
        final String expected = "View";
        assertEquals(expected, model.getLabel());
    }

    @Test
    void getIcon() {
        final String expected = "/content/dam/techcombank/adobe-qa/2002.jpg";
        assertEquals(expected, model.getIcon());
    }

    @Test
    void getYear() {
        final String expected = "2093";
        assertEquals(expected, model.getYear());
    }

    @Test
    void getDocumentDate() {
        assertNotNull(model.getDocumentDate());
    }


    @Test
    void getTypeOfOutput() {
        final String expected = "pdf";
        assertEquals(expected, model.getTypeOfOutput());
    }

    @Test
    void getMobileIcon() {
        final String expected =
                "/content/dam/techcombank/adobe-qa/2002.jpg/jcr:content/renditions/cq5dam.web.640.640.jpeg";
        assertEquals(expected, model.getMobileIcon());
    }

    @Test
    void getWebIcon() {
        final String expected =
                "/content/dam/techcombank/adobe-qa/2002.jpg/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
        assertEquals(expected, model.getWebIcon());
    }

    @Test
    void getWebInteractionType() {
        final String expected = "exit";
        assertEquals(expected, model.getWebInteractionType());
    }

    @Test
    void getFormattedDate() {
        final String expected = "01/10/2093";
        assertEquals(expected, model.getFormattedDate());
    }
}
