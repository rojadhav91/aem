package com.techcombank.core.models;

import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class FAQPanelModelTest {

    public static final String CONTENT_DAM_TECHCOMBANK_CF_FAQ = "/content/dam/techcombank/cf/faq";
    public static final String FAQPANEL_JSON = "/content/techcombank/web/vn/en/faqpanel.json";
    public static final String FAQCF_JSON = "/content/techcombank/web/vn/en/faqcf.json";
    public static final String FAQPANEL = "/faqpanel";
    public static final String FAQPANEL_FAQPANEL = "/faqpanel/faqpanel";

    private ResourceResolver resourceResolver;

    @Mock
    private Iterable<Resource> childResources;
    @Mock
    private Iterator<Resource> mockIterator;

    private AemContext aemContext = TCBAemContext.newAemContext();
    private FAQPanelModel faqPanelModel;

    private Resource cfResource;

    private Resource resource;

    private Iterator<Resource> iterator;

    @BeforeEach
    public void setup() {
        aemContext.addModelsForClasses(FAQPanelModel.class);
        aemContext.load().json(FAQPANEL_JSON, FAQPANEL);
        aemContext.load().json(FAQCF_JSON, CONTENT_DAM_TECHCOMBANK_CF_FAQ);
        aemContext.currentResource(FAQPANEL_FAQPANEL);
        resource = aemContext.request().getResource();
        resourceResolver = mock(ResourceResolver.class);
        cfResource = mock(Resource.class);
        iterator = mock(Iterator.class);
    }

    @Test
    void testInit() {
        lenient().when(resourceResolver.resolve(CONTENT_DAM_TECHCOMBANK_CF_FAQ)).thenReturn(cfResource);
        lenient().when(cfResource.getChildren()).thenReturn(childResources);
        lenient().when(childResources.iterator()).thenReturn(iterator);
        lenient().when(iterator.hasNext()).thenReturn(true, false);
        lenient().when(iterator.next()).thenReturn(mock(Resource.class));
        aemContext.registerAdapter(ResourceResolver.class, Resource.class, cfResource);
        faqPanelModel = resource.adaptTo(FAQPanelModel.class);
        assertEquals("View More", faqPanelModel.getViewMoreLabel());
        assertEquals("FAQ 2 Title", faqPanelModel.getFaqCFList().get(0).getFaqTitle());
        assertEquals("FAQ 2 Description", faqPanelModel.getFaqCFList().get(0).getFaqDescription());
    }
}
