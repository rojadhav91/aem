
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import java.util.List;

import static org.mockito.Mockito.lenient;

import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.NavPanelModelImpl;

import static org.junit.jupiter.api.Assertions.assertTrue;
import org.mockito.Mockito;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class NavEqualPanelModelTest to perform code coverage test for
 * NavEqualPanelModelTest Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class NavEqualPanelModelTest {

    private final AemContext ctx = new AemContext();

    private NavEqualPanelModel navEqualPanelModel;

    private ResourceResolver resourceResolver;

    private Resource resource;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.NAV_EQUAL_PANEL_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(NavEqualPanelModel.class);
        navEqualPanelModel = getModel("/content/jcr:content/root/container/navequalpanel");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver,
                        "/content/techcombank/web/vn/en/currencyconvertornew.html")).
                thenReturn("/content/techcombank/web/vn/en/currencyconvertornew");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "/content/dam/techcombank/images/loginguide.pdf")).
                thenReturn("/content/dam/techcombank/images/loginguide.pdf");
    }

    private NavEqualPanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        NavEqualPanelModel navPanel = res.adaptTo(NavEqualPanelModel.class);
        return navPanel;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
    }

    @Test
    void testNavPannelSection() {
        List<NavPanelModelImpl> navSection = navEqualPanelModel.getNavPanelSection();
        assertEquals("Login to experience Bao Loc Certificate of Deposit.", navSection.get(0).getDescription());
        assertEquals("arrowAlt", navSection.get(0).getCtaTypeOneAltText());
        assertEquals("Watch instruction for transaction", navSection.get(0).getCtaTypeOneLabel());
        assertEquals("/content/techcombank/web/vn/en/currencyconvertornew",
                navSection.get(0).getCtaTypeOneLinkURL());
        assertTrue(navSection.get(0).isCtaTypeOneNoFollow());
        assertEquals("_self", navSection.get(0).getCtaTypeOneTarget());
        assertEquals("videoAlt", navSection.get(0).getCtaTypeTwoAltText());
        assertEquals("Join transaction now", navSection.get(0).getCtaTypeTwoLabel());
        assertEquals("/content/dam/techcombank/images/loginguide.pdf", navSection.get(0).getCtaTypeTwoLinkURL());
        assertEquals("_blank", navSection.get(0).getCtaTypeTwoTarget());
        assertTrue(navSection.get(0).isCtaTypeTwoNoFollow());
        String otherType = "{'webInteractions': {'name': 'Nav Equal Panel','type': 'other'}}";
        assertEquals(otherType, navSection.get(0).getCtaTypeOneLinkURLType());
        String downLoadType = "{'webInteractions': {'name': 'Nav Equal Panel','type': 'download'}}";
        assertEquals(downLoadType, navSection.get(0).getCtaTypeTwoLinkURLType());
        String exitType = "{'webInteractions': {'name': 'Nav Equal Panel','type': 'exit'}}";
        assertEquals(exitType, navSection.get(1).getCtaTypeTwoLinkURLType());
        String emptyType = "{'webInteractions': {'name': 'Nav Equal Panel','type': ''}}";
        assertEquals(emptyType, navSection.get(1).getCtaTypeOneLinkURLType());
    }

    @Test
    void testNavPannelImagesOptimazation() {
        List<NavPanelModelImpl> navSection = navEqualPanelModel.getNavPanelSection();
        String ctaTypeOneImage = "/content/dam/techcombank/images/Western_Union_tab.svg";
        String ctaTypeTwoImage = "/content/dam/techcombank/images/Credit_Card.svg";
        assertEquals(ctaTypeOneImage, navSection.get(0).getCtaTypeOneIcon());
        assertEquals(ctaTypeOneImage, navSection.get(0).getCtaTypeOneIconWebImagePath());
        assertEquals(ctaTypeOneImage, navSection.get(0).getCtaTypeOneIconMobileImagePath());
        assertEquals(ctaTypeTwoImage, navSection.get(0).getCtaTypeTwoIcon());
        assertEquals(ctaTypeTwoImage, navSection.get(0).getCtaTypeTwoIconWebImagePath());
        assertEquals(ctaTypeTwoImage, navSection.get(0).getCtaTypeTwoIconMobileImagePath());
    }

}
