package com.techcombank.core.pojo;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class TabEventCardPojoTest {

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    private TabEventCard tabEventCard = new TabEventCard();

    @Test
    void testTabEventCardPojo() {
        lenient().when(tag.getTitle()).thenReturn("Institutional");
        String[] tags = {"techcombank:event-type/institutional"};
        tabEventCard.setImageForEvent("/content/dam/techcombank/public-site/29216_1_69264953d7.png");
        tabEventCard.setEventHeading("Regular");
        tabEventCard.setEventSubHeading("AGM");
        tabEventCard.setEventDesription("This is regular event");
        tabEventCard.setAwardLabelText("Business");
        tabEventCard.setCtaButtonLabel("Read more");
        tabEventCard.setCtaButtonLink("/content/techcombank/web/kr/en/dividerComponent.html");
        tabEventCard.setCtaTarget("_blank");
        tabEventCard.setCtaNoFollow("true");
        tabEventCard.setEventMonth("2022-06-07");
        tabEventCard.setEventDate("2022-06-07");
        tabEventCard.setEventTime("2022-06-07");
        tabEventCard.setEventTag(tags, tagManager);

        assertEquals("/content/dam/techcombank/public-site/29216_1_69264953d7.png", tabEventCard.getImageForEvent());
        assertEquals("Regular", tabEventCard.getEventHeading());
        assertEquals("AGM", tabEventCard.getEventSubHeading());
        assertEquals("This is regular event", tabEventCard.getEventDesription());
        assertEquals("Business", tabEventCard.getAwardLabelText());
        assertEquals("Read more", tabEventCard.getCtaButtonLabel());
        assertEquals("/content/techcombank/web/kr/en/dividerComponent.html", tabEventCard.getCtaButtonLink());
        assertEquals("_blank", tabEventCard.getCtaTarget());
        assertEquals("true", tabEventCard.getCtaNoFollow());
        assertEquals("June", tabEventCard.getEventMonth());
        assertEquals("07", tabEventCard.getEventDate());
        assertEquals("", tabEventCard.getEventTime());
    }
}
