package com.techcombank.core.services;

import com.adobe.acs.commons.notifications.InboxNotification;
import com.adobe.acs.commons.notifications.InboxNotificationSender;
import com.adobe.granite.taskmanagement.TaskManagerException;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.service.impl.WorkflowNotificationServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class WorkflowNotificationServiceTest {

    @Mock
    private InboxNotificationSender inboxNotificationSender;

    @Mock
    private InboxNotification inboxNotification;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private UserManager userManager;

    @Mock
    private Authorizable authorizable;
    @Mock
    private EmailService emailService;

    @Mock
    private HistoryItem historyItem;

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private MetaDataMap metaDataMap;
    private Value[] values;

    @InjectMocks
    private WorkflowNotificationServiceImpl workflowNotificationService = new WorkflowNotificationServiceImpl();

    @BeforeEach
    void setup() {
        lenient().when(inboxNotificationSender.buildInboxNotification()).thenReturn(inboxNotification);
        lenient().when(inboxNotification.getTitle()).thenReturn("Approval Title");
    }

    @Test
    void testInboxNotification() throws TaskManagerException {
        String initiator = "initiator@example.com";
        String emailTitle = "Title";
        String emailBody = "Body";
        String payload = "/content";
        workflowNotificationService.inboxNotification(resourceResolver, initiator, emailTitle, emailBody, payload);
        assertEquals("Approval Title", inboxNotification.getTitle());
    }

    @Test
    void testEmailNotifications() throws TaskManagerException {
        String email = "initiator@example.com";
        String emailTitle = "Title";
        String subject = "Body";
        String workflowTitle = "Workflow Title";
        String approverName = "user2";
        String payload = "/content";
        workflowNotificationService.emailNotifications(payload, workflowTitle,
                approverName, subject, emailTitle, email);
        assertEquals("Approval Title", inboxNotification.getTitle());
    }

    @Test
    void testExecuteComment() throws WorkflowException {
        List<HistoryItem> historyList = new ArrayList<>();
        historyList.add(historyItem);
        when(workflowSession.getHistory(Mockito.any())).thenReturn(historyList);
        when(historyItem.getComment()).thenReturn("comment");
        String comment = workflowNotificationService.getUserComment(workItem, workflowSession);
        assertEquals("comment", comment);
    }

    @Test
    void testSendNotificationAndEmail() throws RepositoryException {
        String email = "initiator@example.com";
        String payLoad = "/content";
        String initator = "abc@xyz.com";
        String approverName = "admin";
        String subject = "This is subject";
        String title = "Email notification";
        Value value = Mockito.mock(Value.class);
        Mockito.lenient().when(value.toString()).thenReturn("test@test.com");
        values = new Value[]{value};
        authorizable = mock(Authorizable.class);
        userManager = mock(UserManager.class);
        when(resourceResolver.adaptTo(UserManager.class)).thenReturn(userManager);
        when(userManager.getAuthorizable(initator)).thenReturn(authorizable);
        when(metaDataMap.containsKey(TCBConstants.EMAIL_ENABLE)).thenReturn(true);
        when(metaDataMap.containsKey(TCBConstants.WORKFLOW_TITLE)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.EMAIL_ENABLE, Boolean.class)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn("");
        when(metaDataMap.get(TCBConstants.WORKFLOW_TITLE, String.class)).thenReturn("workflowTitle");
        Mockito.lenient().when(authorizable.hasProperty("./profile/email")).thenReturn(true);
        Mockito.lenient().when(authorizable.getProperty("./profile/email")).thenReturn(values);
        workflowNotificationService.sendNotificationAndEmail(resourceResolver, metaDataMap,
                payLoad, initator, approverName, subject, title);
        assertEquals("Approval Title", inboxNotification.getTitle());
        when(metaDataMap.get(TCBConstants.USER_EMAIL, String.class)).thenReturn(email);
        workflowNotificationService.sendNotificationAndEmail(resourceResolver, metaDataMap,
                payLoad, initator, approverName, subject, title);
        assertEquals("Approval Title", inboxNotification.getTitle());
    }
}
