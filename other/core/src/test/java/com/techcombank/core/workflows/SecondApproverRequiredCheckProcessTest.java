package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.testcontext.TestConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SecondApproverRequiredCheckProcessTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @InjectMocks
    private SecondApproverRequiredCheckProcess secondApproverRequiredCheckProcess;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflowData().getMetaDataMap()).thenReturn(metaDataMap);
        when(workItem.getWorkflowData().getPayload()).thenReturn("/content/documents/sample");
    }

    @Test
    void testExecuteWithSecondApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(true);
        when(metaDataMap.get(TCBConstants.SECOND_APPORVER, String.class)).thenReturn("second.approver@example.com");
        secondApproverRequiredCheckProcess.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_5)).getWorkflowData();
    }

    @Test
    void testExecuteWithoutSecondApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(false);
        secondApproverRequiredCheckProcess.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_4)).getWorkflowData();
    }
}
