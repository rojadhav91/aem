package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CreditRatingDataModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CreditRatingDataModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CreditRatingDataModel.class);
        aemContext.load().json(TestConstants.CREDITRATINGDATA, "/creditRatingData");
        aemContext.currentResource(TestConstants.CREDITRATINGDATACOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CreditRatingDataModel.class);
    }

    @Test
    void getCategoryData() {
        final String expected = "text";
        assertEquals(expected, model.getCategoryData());
    }

    @Test
    void getRatingData() {
        final String expected = "text";
        assertEquals(expected, model.getRatingData());
    }

}
