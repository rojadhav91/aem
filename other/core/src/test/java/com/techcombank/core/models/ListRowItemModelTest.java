package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListRowItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListRowItemModel model;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    public static final String ARTICLE = "Article";
    public static final String TAG_NAME = "techcombank:promotion-categories/cuisine";

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListRowItemModel.class);
        aemContext.load().json(TestConstants.LISTROWITEM, "/listRowItem");
        aemContext.currentResource(TestConstants.LISTROWITEMCOMP);
        Resource resource = aemContext.request().getResource();
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);
        when(tagManager.resolve(TAG_NAME)).thenReturn(tag);
        when(tag.getTitle()).thenReturn(ARTICLE);
        model = resource.adaptTo(ListRowItemModel.class);
    }

    @Test
    void getRowTitle() {
        final String expected = "Text Title";
        assertEquals(expected, model.getRowTitle());
    }

    @Test
    void getTagLabel() {
        assertEquals(ARTICLE, model.getTagLabel());
    }

    @Test
    void getFormattedDate() {
        final String expected = "09/10/2023";
        assertEquals(expected, model.getFormattedDate());
    }

    @Test
    void getIconImage() {
        final String expected = "/content/dam/techcombank/tcb_qa/folder-for-test/red_arrow_54cb6ae59e.svg";
        assertEquals(expected, model.getIconImage());
    }

    @Test
    void getAltText() {
        final String expected = "alt";
        assertEquals(expected, model.getAltText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "/content/techcombank/rdb-app/v1/vn/vi";
        assertEquals(expected, model.getLinkUrl());
    }

    @Test
    void isOpenInNewTab() {
        final boolean expected = true;
        assertEquals(expected, model.isOpenInNewTab());
    }

    @Test
    void isNoFollow() {
        final boolean expected = false;
        assertEquals(expected, model.isNoFollow());
    }

     @Test
    void getWebInteractionType() {
        final String expected = "exit";
        assertEquals(expected, model.getWebInteractionType());

    }

}
