package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class TableCompareColumnHeaderModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private TableCompareColumnHeaderModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TableCompareColumnHeaderModel.class);
        aemContext.load().json(TestConstants.TABLECOMPARECOLUMNHEADER, "/tableCompareColumnHeader");
        aemContext.currentResource(TestConstants.TABLECOMPARECOLUMNHEADERCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(TableCompareColumnHeaderModel.class);
    }

    @Test
    void getColumnHeader() {
        final String expected = "text";
        assertEquals(expected, model.getColumnHeader());
    }

}
