package com.techcombank.core.services;

import com.google.gson.JsonObject;
import com.techcombank.core.service.HttpClientExecutorService;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.impl.JwtTokenServiceImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class JwtTokenServiceTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private JwtTokenServiceImpl jwtTokenService = new JwtTokenServiceImpl();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private JwtTokenServiceImpl.JwtTokenConfig jwtTokenConfig;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ModifiableValueMap modifiableValueMap;

    @Mock
    private Resource resource;

    @Mock
    private HttpClientExecutorService httpExecutor;

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.registerService(ResourceResolverService.class, resourceResolverService);
        ctx.registerService(HttpClientExecutorService.class, httpExecutor);
        ctx.addModelsForClasses(JwtTokenServiceImpl.class);
    }

    @Test
    void testJwtTokenExpired() {
        JsonObject tokenDetails = new JsonObject();
        String expectedToken = "ejk_ijhtyjdhgfdjj";
        tokenDetails.addProperty("token", expectedToken);
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resource.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        lenient().when(modifiableValueMap.get("token"))
                .thenReturn("5C8oGB+BN4TCWk+clOSOdMIGXXA94s7uiPgmQZQprXKjDnIAxzGHROK+1VPE");
        lenient().when(jwtTokenConfig.getJwtTokenPath()).thenReturn("/var/tokens");
        lenient().when(jwtTokenConfig.getEncKey()).thenReturn("mGu/UqEfuZoWAQsWG9rVFQ==");
        lenient().when(resourceResolver.getResource("/var/tokens")).thenReturn(resource);
        lenient().when(httpExecutor.executeHttpGet(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(tokenDetails);
        String actualToken = jwtTokenService.getJwtToken(true);
        assertEquals(expectedToken, actualToken);
    }

    @Test
    void testJwtTokenNotAvailable() {
        JsonObject tokenDetails = new JsonObject();
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resource.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        lenient().when(jwtTokenConfig.getJwtTokenPath()).thenReturn("/var/tokens");
        lenient().when(jwtTokenConfig.getEncKey()).thenReturn("mGu/UqEfuZoWAQsWG9rVFQ==");
        lenient().when(resourceResolver.getResource("/var/tokens")).thenReturn(resource);
        lenient().when(httpExecutor.executeHttpGet(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(tokenDetails);
        String actualToken = jwtTokenService.getJwtToken(false);
        assertTrue(actualToken.isEmpty());
    }

    @Test
    void testCreateJwtTokenFolder() {
        JsonObject tokenDetails = new JsonObject();
        String expectedToken = "ejk_ijhtyjdhgfdjj";
        tokenDetails.addProperty("token", expectedToken);
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resource.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        lenient().when(jwtTokenConfig.getJwtTokenPath()).thenReturn("/var/tokens");
        lenient().when(jwtTokenConfig.getEncKey()).thenReturn("mGu/UqEfuZoWAQsWG9rVFQ==");
        lenient().when(httpExecutor.executeHttpGet(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(tokenDetails);
        String actualToken = jwtTokenService.getJwtToken(false);
        assertTrue(actualToken.isEmpty());
    }
}
