package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class TableCompareModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private TableCompareModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TableCompareModel.class);
        aemContext.load().json(TestConstants.TABLECOMPARE, "/tableCompare");
        aemContext.currentResource(TestConstants.TABLECOMPARECOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(TableCompareModel.class);
    }

    @Test
    void getColumnHeaderTitle() {
        final String expected = "text";
        assertEquals(expected, model.getColumnHeaderTitle());
    }

    @Test
    void getTableCompareColumnHeaderList() {
        assert model.getTableCompareColumnHeaderList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getTableCompareColumnHeaderList().size());
    }

    @Test
    void getTableCompareCardList() {
        assert model.getTableCompareCardList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getTableCompareCardList().size());
    }

}
