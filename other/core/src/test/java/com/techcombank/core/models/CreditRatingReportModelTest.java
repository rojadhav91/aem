package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CreditRatingReportModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CreditRatingReportModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CreditRatingReportModel.class);
        aemContext.load().json(TestConstants.CREDITRATINGREPORT, "/creditRatingReport");
        aemContext.currentResource(TestConstants.CREDITRATINGREPORTCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CreditRatingReportModel.class);
    }

    @Test
    void getCreditRatingLogoImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCreditRatingLogoImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getCreditRatingLogoImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCreditRatingLogoImagePath());
        assertEquals(model.getCreditRatingLogoImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCreditRatingLogoImagePath());
    }

    @Test
    void getLogoImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getLogoImageAltText());
    }

    @Test
    void getCreditRatingTabList() {
        assert model.getCreditRatingTabList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getCreditRatingTabList().size());
    }
}
