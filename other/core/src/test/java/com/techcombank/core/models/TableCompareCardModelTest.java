package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class TableCompareCardModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private TableCompareCardModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TableCompareCardModel.class);
        aemContext.load().json(TestConstants.TABLECOMPARECARD, "/tableCompareCard");
        aemContext.currentResource(TestConstants.TABLECOMPARECARDCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(TableCompareCardModel.class);
    }

    @Test
    void getCardTitle() {
        final String expected = "text";
        assertEquals(expected, model.getCardTitle());
    }

    @Test
    void getCardTitleImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCardTitleImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getCardTitleImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCardTitleImagePath());
        assertEquals(model.getCardTitleImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCardTitleImagePath());
    }

    @Test
    void getCardBackgroundColour() {
        final String expected = "color-code";
        assertEquals(expected, model.getCardBackgroundColour());
    }

    @Test
    void getTableCompareRowList() {
        TableCompareRowModel tableCompareRowModel = model.getTableCompareRowList().get(0);
        String expected = "text";
        assertEquals(expected, tableCompareRowModel.getRowValue());
    }
}
