package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class StepAccordionModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();
    private StepAccordionModel stepAccordionModel;

    @BeforeEach
    void setUp() throws NoSuchFieldException {
        aemContext.addModelsForClasses(StepAccordionModel.class);
        aemContext.load().json(TestConstants.STEP_ACCORDION, "/stepaccordion");
        aemContext.currentResource(TestConstants.STEP_ACCORDION_COMP);
        Resource resource = aemContext.request().getResource();
        stepAccordionModel = resource.adaptTo(StepAccordionModel.class);
    }


    @Test
    void getStepAccordionItemList() {
        List<StepAccordionItem> stepAccordionItemList = stepAccordionModel.getStepAccordionItemList();
        assertEquals("Title 1", stepAccordionItemList.get(0).getAccordionItemTitle());
    }

}
