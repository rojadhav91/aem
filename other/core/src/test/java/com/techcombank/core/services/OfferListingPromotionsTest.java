package com.techcombank.core.services;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.impl.OfferListingPromotionsImpl;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class OfferListingPromotionsTest {

    private final AemContext ctx = new AemContext();

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Page currentPage;

    @Mock
    private Session session;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Query query;

    @Mock
    private TagManager tagManager;

    @Mock
    private SearchResult searchResult;

    @Mock
    private Node node;

    @Mock
    private Tag tag1;

    @Mock
    private Tag tag2;

    @Mock
    private Tag tag3;

    @Mock
    private Iterator<Node> nodeIterator;

    @InjectMocks
    private OfferListingPromotionsImpl offerListingPromotions = new OfferListingPromotionsImpl();

    @BeforeEach
    public void setup() throws IOException, NoSuchFieldException {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.OFFER_LISTING_PROMOTIONS_CARD,
                TestConstants.CONTENTPATH);
        ctx.currentResource("/content/offerListingCard");
        ctx.addModelsForClasses(OfferListingPromotionsImpl.class);
        ctx.registerInjectActivateService(offerListingPromotions);
    }

    @Test
    void testPromotionsCard() throws UnsupportedEncodingException {
        Map<String, String> requestQueryParam = new HashMap<>();
        requestQueryParam.put("limit", "10");
        requestQueryParam.put("offset", "0");
        requestQueryParam.put("products", "debit-card,credit-card");
        requestQueryParam.put("partner", "avis,hertz");
        requestQueryParam.put("types", "shopping,education");
        requestQueryParam.put("sort", "most-popular");
        requestQueryParam.put("isSearch", "true");
        requestQueryParam.put("searchText", "10%");
        String cfPath = "/content/dam/techcombank/content-fragments/promotions/en";
        Map<String, String> queryMap = offerListingPromotions.generatePromotionsQuery(requestQueryParam, cfPath);
        assertEquals("@jcr:content/data/master/favourite", queryMap.get("orderby"));
        assertEquals("@jcr:content/data/master/ranking", queryMap.get("1_orderby"));

        requestQueryParam.put("sort", "latest");
        queryMap = offerListingPromotions.generatePromotionsQuery(requestQueryParam, cfPath);
        assertEquals("@jcr:created", queryMap.get("orderby"));

        requestQueryParam.put("sort", "more-time-to-consider");
        queryMap = offerListingPromotions.generatePromotionsQuery(requestQueryParam, cfPath);
        assertEquals("@jcr:content/data/master/expiryDate", queryMap.get("orderby"));
        assertEquals("desc", queryMap.get("orderby.sort"));

        requestQueryParam.put("sort", "expiring-soon");
        queryMap = offerListingPromotions.generatePromotionsQuery(requestQueryParam, cfPath);
        assertEquals("@jcr:content/data/master/expiryDate", queryMap.get("orderby"));
        assertEquals("asc", queryMap.get("orderby.sort"));

        Map<String, Object> response;

        lenient().when(resourceResolver.adaptTo(Session.class)).thenReturn(session);
        lenient().when(resourceResolver.adaptTo(QueryBuilder.class)).thenReturn(queryBuilder);
        lenient().when(queryBuilder.createQuery(Mockito.any(), Mockito.any())).thenReturn(query);
        lenient().when(currentPage.getLanguage()).thenReturn(Locale.ENGLISH);
        lenient().when(resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
        lenient().when(query.getResult()).thenReturn(searchResult);
        lenient().when(searchResult.getTotalMatches()).thenReturn((long) 0);
        response = offerListingPromotions.generateOffers(resourceResolver, queryMap, currentPage);
        assertEquals("0", response.get("total").toString());

        lenient().when(searchResult.getTotalMatches()).thenReturn((long) 1);
        lenient().when(searchResult.getNodes()).thenReturn(Collections.singleton(node).iterator());
        lenient().when(nodeIterator.next()).thenReturn(node);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(ctx.currentResource());
        lenient().when(tagManager.resolve("techcombank:promotions/promotion-categories/healthcare")).thenReturn(tag1);
        lenient().when(tag1.getTitle(Mockito.any())).thenReturn("Healthcare");
        lenient().when(tagManager.resolve("techcombank:promotions/product-types/debit-card")).thenReturn(tag2);
        lenient().when(tag2.getTitle(Mockito.any())).thenReturn("Debit card");
        lenient().when(tagManager.resolve("techcombank:promotions/merchant/farfetch")).thenReturn(tag3);
        lenient().when(tag3.getTitle(Mockito.any())).thenReturn("Farfetch");
        response = offerListingPromotions.generateOffers(resourceResolver, queryMap, currentPage);
        assertEquals("1", response.get("total").toString());
    }
}
