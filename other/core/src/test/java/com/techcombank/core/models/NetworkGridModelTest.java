package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class NetworkGridModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private NetworkGridModel networkGridModel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(NetworkGridModel.class);
        aemContext.load().json(TestConstants.NETWORKGRID, "/networkGrid");
        aemContext.currentResource(TestConstants.NETWORKGRIDCOMP);
        Resource resource = aemContext.request().getResource();
        networkGridModel = resource.adaptTo(NetworkGridModel.class);
    }

    @Test
    void testNetworkGridItems() {
        assertEquals(TestConstants.LIST_SIZE_THREE, networkGridModel.getNetworkGridItems().size());
    }
}
