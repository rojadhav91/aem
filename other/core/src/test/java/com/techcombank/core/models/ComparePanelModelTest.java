package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ComparePanelModelTest {
    private final AemContext ctx = AppAemContext.newAemContext();
    private ComparePanelModel model;

    private ResourceResolverService resourceResolverService;

    private ResourceResolver resourceResolver;

    private Resource resource;

    private ModifiableValueMap valueMap;

    @BeforeEach
    void setUp() {
        mockObjects();
        ctx.load().json(TestConstants.COMPAREPANEL, "/comparePanel");
        ctx.addModelsForClasses(ComparePanelModel.class);
        model = getModel(TestConstants.COMPAREPANELCOMP);
        valueMap = new ModifiableValueMapDecorator(new HashMap<String, Object>());
        lenient().when(resource.getValueMap()).thenReturn(valueMap);
    }

    private ComparePanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        return res.adaptTo(ComparePanelModel.class);
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void getTagFolder() {
        final String expected = "path";
        assertEquals(expected, model.getTagFolder());
    }

    @Test
    void getContentFragmentFolder() {
        final String expected = "path";
        assertEquals(expected, model.getContentFragmentFolder());
    }

    @Test
    void getCustomerTypePlaceholderLabel() {
        final String expected = "text";
        assertEquals(expected, model.getCustomerTypePlaceholderLabel());
    }

    @Test
    void getHouseTypePlaceholderLabel() {
        final String expected = "text";
        assertEquals(expected, model.getHouseTypePlaceholderLabel());
    }

    @Test
    void getInvestorTypePlaceholderLabel() {
        final String expected = "text";
        assertEquals(expected, model.getInvestorTypePlaceholderLabel());
    }

    @Test
    void getProjectNameLabel() {
        final String expected = "text";
        assertEquals(expected, model.getProjectNameLabel());
    }

    @Test
    void getLoanRateOnCapitalNeedLabel() {
        final String expected = "text";
        assertEquals(expected, model.getLoanRateOnCapitalNeedLabel());
    }

    @Test
    void getToolTipLoanRateOnCapitalNeed() {
        final String expected = "text";
        assertEquals(expected, model.getToolTipLoanRateOnCapitalNeed());
    }

    @Test
    void getLoanRateOnCollateralValueLabel() {
        final String expected = "text";
        assertEquals(expected, model.getLoanRateOnCollateralValueLabel());
    }

    @Test
    void getToolTipLoanRateOnCollateralValueLabel() {
        final String expected = "text";
        assertEquals(expected, model.getToolTipLoanRateOnCollateralValueLabel());
    }

    @Test
    void getLoanTermLabel() {
        final String expected = "text";
        assertEquals(expected, model.getLoanTermLabel());
    }

    @Test
    void getToolTipLoanTermLabel() {
        final String expected = "text";
        assertEquals(expected, model.getToolTipLoanTermLabel());
    }

    @Test
    void getCollateralLabel() {
        final String expected = "text";
        assertEquals(expected, model.getCollateralLabel());
    }

    @Test
    void getToolTipCollateral() {
        final String expected = "text";
        assertEquals(expected, model.getToolTipCollateral());
    }

    @Test
    void getToolTipIcon() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getToolTipIcon());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getToolTipIcon() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebTooltipIconPath());
        assertEquals(model.getToolTipIcon() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileTooltipIconPath());
    }

    @Test
    void getViewResultCtaLabel() {
        final String expected = "text";
        assertEquals(expected, model.getViewResultCtaLabel());
    }

}
