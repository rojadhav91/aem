package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.pojo.Articles;
import com.techcombank.core.service.ArticlesRelatedPostService;
import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleRelatedPostModelTest {

    private AemContext aemContext = TCBAemContext.newAemContext();

    private ArticlesRelatedPostModel articlesRelatedPostModel;

   @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ArticlesRelatedPostService articlesRelatedPostService;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    void setUp() {
        aemContext.load().json(TestConstants.ARTICLE_RELATEDPOST, "/articlerelatedpost");
        aemContext.currentResource(TestConstants.ARTICLERELATEDPOST);
        Resource pageResource = aemContext.request().getResource();
        articlesRelatedPostModel = pageResource.adaptTo(ArticlesRelatedPostModel.class);
    }

    @Test
    void getTitleText() {
        final String expected = "Related Posts";
        assertEquals(expected, articlesRelatedPostModel.getTitleText());
    }

    @Test
    void getViewMore() {
        final String expected = "View More";
        assertEquals(expected, articlesRelatedPostModel.getViewMore());
    }

    @Test
    void getNewTab() {
        final String expected = "true";
        assertEquals(expected, articlesRelatedPostModel.getNewTab());
    }
    @Test
    void getImage() {
        final String expected = "/content/dam/techcombank/Techcombank-thumbnail-58d4c61434.jpg";
        assertEquals(expected, articlesRelatedPostModel.getDefaultThumbnailImage());
    }

    @Test
    void getRelatedPost() throws NoSuchFieldException {
        PrivateAccessor.setField(articlesRelatedPostModel, "resource", resource);
        PrivateAccessor.setField(articlesRelatedPostModel, "currentPage", currentPage);
        PrivateAccessor.setField(articlesRelatedPostModel, "resourceResolver", resourceResolver);
        PrivateAccessor.setField(articlesRelatedPostModel, "articlesRelatedPostService", articlesRelatedPostService);
        Articles art1 = new Articles();
        art1.setMobileImage(StringUtils.EMPTY);
        art1.setWebImage(StringUtils.EMPTY);
        List<Articles> relatedArticles = new ArrayList<>();
        relatedArticles.add(art1);
        lenient().when(articlesRelatedPostService.getArticlesRelatedPosts(resource, currentPage, resourceResolver))
                .thenReturn(relatedArticles);
        articlesRelatedPostModel.getRelatedPost();
        assertEquals(1, relatedArticles.size());
    }

    @Test
    void getHideStatus() throws NoSuchFieldException {
        PrivateAccessor.setField(articlesRelatedPostModel, "currentPage", currentPage);
        lenient().when(currentPage.getProperties()).thenReturn(valueMap);
        lenient().when(valueMap.get("hideArticleRelatedPost", String.class)).thenReturn("hello");
        String hideStatus = articlesRelatedPostModel.getHideStatus();
        assertEquals("hello", hideStatus);
    }
}
