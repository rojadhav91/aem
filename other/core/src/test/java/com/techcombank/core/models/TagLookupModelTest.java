package com.techcombank.core.models;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class TagLookupModelTest {

    public static final String FILTERPANELCONTAINER_JSON = "/content/techcombank/web/vn/en/filterpanelcontainer.json";
    public static final String FILTERPANELCONTAINER = "/filterpanelcontainer";
    public static final String ARTICLE = "Article";
    public static final String TAG_NAME = "techcombank:articles/article-type";
    private final AemContext aemContext = AppAemContext.newAemContext();

    private TagLookupModel lookupModel;

    @Mock
    private ResourceResolver resolver;
    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TagLookupModel.class);
        aemContext.load().json(FILTERPANELCONTAINER_JSON, FILTERPANELCONTAINER);
        aemContext.currentResource(FILTERPANELCONTAINER + FILTERPANELCONTAINER);
        Resource resource = aemContext.request().getResource();
        aemContext.registerAdapter(ResourceResolver.class, TagManager.class, tagManager);
        when(tagManager.resolve(TAG_NAME)).thenReturn(tag);
        when(tag.getTitle()).thenReturn(ARTICLE);
        lookupModel = resource.adaptTo(TagLookupModel.class);
    }

    @Test
    void testInit() {
        assertEquals("Article", lookupModel.getTagLabel());
    }
}
