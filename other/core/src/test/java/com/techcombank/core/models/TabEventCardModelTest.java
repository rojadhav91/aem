package com.techcombank.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
public class TabEventCardModelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();
    public static final String CONTENT_DAM_TECHCOMBANK_CF_FAQ =
            "/content/dam/techcombank/rdb-app/data/content-fragment/data";

    private ResourceResolver resourceResolver;
    private TabEventCardModel model;

    private Resource cfResource;

    private Resource resource;

    private ContentFragment contentFragment;
    private List<TabEventCardPaths> paths;


    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(TabEventCardModel.class);
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH
                + TestConstants.TAB_EVENT_CARD_JSON, "/tabEventCard");
        aemContext.load().json(TestConstants.CONTENT_BASE_PATH
                + TestConstants.TABEVENTCF_JSON, CONTENT_DAM_TECHCOMBANK_CF_FAQ);
        aemContext.currentResource(TestConstants.TAB_EVENT_CARD_COMP);
        resource = aemContext.request().getResource();
        resourceResolver = mock(ResourceResolver.class);
        cfResource = mock(Resource.class);
        aemContext.registerAdapter(ResourceResolver.class, Resource.class, cfResource);
        contentFragment = mock(ContentFragment.class);
        model = resource.adaptTo(TabEventCardModel.class);
        paths = model.getPaths();
    }

    @Test
    void getViewAllLabel() {
        final String expected = "View all label";
        assertEquals(expected, model.getViewAll());
    }

    @Test
    void getViewAllLink() {
        final String expected = "/content/techcombank/cdb-app";
        assertEquals(expected, model.getLink());
    }

    @Test
    void getNewTab() {
        final String expected = "true";
        assertEquals(expected, model.getNewTab());
    }

    @Test
    void getNoFollow() {
        final String expected = "true";
        assertEquals(expected, model.getNoFollow());
    }

    @Test
    void getVariationType() {
        final String expected = "awards";
        assertEquals(expected, model.getVariationType());
    }

    @Test
    void getPaths() {
        final String expected = "/content/dam/techcombank/rdb-app/data/content-fragment/data";
        assertEquals(1, paths.size());
        assertEquals(expected, paths.get(0).getRootPath());
    }

    @Test
    void testInit() {
        assertEquals("/content/dam/techcombank/tabevent/tabevent-10.png",
                model.getTabEventList().get(0).getImageForEvent());
        assertEquals("true", model.getTabEventList().get(0).getCtaNoFollow());
        assertEquals("/content/techcombank/web/vn/en/test-page0.html",
                model.getTabEventList().get(0).getCtaButtonLink());
        assertEquals("event sub heading", model.getTabEventList().get(0).getEventSubHeading());
        assertEquals("This livestream event is a quarterly virtual meeting",
                model.getTabEventList().get(0).getEventDesription());
        assertEquals("09", model.getTabEventList().get(0).getEventDate());
        assertEquals("05:05 AM", model.getTabEventList().get(0).getEventTime());
        assertEquals("true", model.getTabEventList().get(0).getCtaTarget());
        assertEquals("Analyst Presentation - FY2022", model.getTabEventList().get(0).getEventHeading());
        assertEquals("Awarded by", model.getTabEventList().get(0).getAwardLabelText());
        assertEquals("Read more", model.getTabEventList().get(0).getCtaButtonLabel());

    }

}
