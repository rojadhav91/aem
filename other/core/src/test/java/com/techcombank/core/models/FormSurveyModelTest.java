package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class FormSurveyModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private FormSurveyModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(FormSurveyModel.class);
        aemContext.load().json(TestConstants.FORMSURVEY, "/formSurvey");
        aemContext.currentResource(TestConstants.FORMSURVEYCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(FormSurveyModel.class);
    }

    @Test
    void getSurveyType() {
        final String expected = "select-item";
        assertEquals(expected, model.getSurveyType());
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getCaption() {
        final String expected = "text";
        assertEquals(expected, model.getCaption());
    }

    @Test
    void getName() {
        final String expected = "text";
        assertEquals(expected, model.getName());
    }

    @Test
    void getAnswerDisplayDirection() {
        final String expected = "select-item";
        assertEquals(expected, model.getAnswerDisplayDirection());
    }
    @Test
    void getMinimumNumberOfAnswers() {
        final int expected = 1;
        assertEquals(expected, model.getMinimumNumberOfAnswers());
    }

    @Test
    void getMinimumNumberOfAnswersAlert() {
        final String expected = "text";
        assertEquals(expected, model.getMinimumNumberOfAnswersAlert());
    }

    @Test
    void getAnswerType() {
        final String expected = "select-item";
        assertEquals(expected, model.getAnswerType());
    }

    @Test
    void getFormSurveyItemList() {
        assert model.getFormSurveyItemList() != null;
        assertEquals(TestConstants.LIST_SIZE_THREE, model.getFormSurveyItemList().size());
    }

}
