package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The Class MilestonePanelModelTest to perform code coverage test for
 * MilestonePanelModel Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MilestonePanelModelTest {

    private final AemContext ctx = new AemContext();

    private MilestonePanelModel milestonePanelModel;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.MILESTONE_PANEL_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(MilestonePanelModel.class);
        milestonePanelModel = getModel(
                "/content/milestonepanel/jcr:content/root/container/milestonepanel");
    }

    private MilestonePanelModel getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        MilestonePanelModel milestonePanel = res.adaptTo(MilestonePanelModel.class);
        return milestonePanel;
    }

    @Test
    void testTimelines() {
        List<TimelineModel> timelines = milestonePanelModel.getTimelines();
        assertEquals("1993", timelines.get(0).getTimelineYear());
        assertEquals("Outstanding achievements in 1993", timelines.get(0).getTimelineSliderTitle());
        assertEquals("<p>Established with the charter capital of 20 billion VND</p>",
                timelines.get(0).getTimelineSliderDescriptionText());
        String expImage = "/content/dam/techcombank/qa/image.jpeg";
        assertEquals(expImage, timelines.get(0).getTimelineImage());
        assertEquals("Alt Text",
                timelines.get(0).getTimelineImageAltText());
        assertEquals(expImage + TestConstants.JPEG_WEB_IMAGE_RENDITION_PATH,
                timelines.get(0).getWebImagePath());
        assertEquals(expImage + TestConstants.JPEG_MOBILE_IMAGE_RENDITION_PATH,
                timelines.get(0).getMobileImagePath());
    }

    @Test
    void testWebInteractionValue() {
        assertEquals("{'webInteractions': {'name': 'Milestone Panel','type': 'other'}}",
                milestonePanelModel.getWebInteractionValue());
    }
}
