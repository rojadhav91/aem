package com.techcombank.core.servlets;

import com.google.gson.JsonObject;
import com.techcombank.core.service.QmsIntegrationService;
import com.techcombank.core.service.StockChartService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.http.HttpStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.testing.mock.sling.servlet.MockSlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class IntegrationDataServletTest {
    private static final int STATUS_OK = 200;
    private static final int STATUS_ERROR = 500;
    private static final String ERROR_CODE = "errorCode";
    private static final String ERROR_MSG = "errorMsg";
    private final AemContext ctx = new AemContext();
    private final JsonObject integrationDataJsonFailure = new JsonObject();
    @InjectMocks
    private IntegrationDataServlet integrationData = new IntegrationDataServlet();
    @Mock
    private SlingHttpServletRequest request;
    @Mock
    private SlingHttpServletResponse response;
    @Mock
    private StockChartService stockChartService;
    @Mock
    private QmsIntegrationService qmsIntegrationService;
    @Mock
    private PrintWriter out;
    @Mock
    private RequestPathInfo requestPathInfo;

    @BeforeEach
    void setUp() throws Exception {
        ctx.addModelsForClasses(IntegrationDataServlet.class);
        lenient().when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        lenient().when(response.getWriter()).thenReturn(out);
        integrationDataJsonFailure.addProperty(ERROR_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        integrationDataJsonFailure.addProperty(ERROR_MSG, HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    void doPostQMSIntegrationService(final AemContext context) throws ServletException, IOException {
        String[] branchSelector = {"branchlist"};
        String[] branchSlotSelector = {"branchslot"};
        String[] branchServiceSelector = {"servicelist"};
        String[] branchBookingConfirmSelector = {"confirmbooking"};
        JsonObject reqObj = new JsonObject();
        JsonObject integrationDataJson = new JsonObject();
        lenient().when(requestPathInfo.getSelectors()).thenReturn(branchSelector);
        MockSlingHttpServletResponse res = ctx.response();
        lenient().when(qmsIntegrationService.fetchQMSBranchList(false, request, reqObj))
        .thenReturn(integrationDataJson);
        integrationData.doPost(request, res);
        assertEquals(STATUS_OK, res.getStatus());

        integrationDataJson.addProperty("error", "Invalid token.");
        lenient().when(qmsIntegrationService.fetchQMSBranchList(false, request, reqObj))
        .thenReturn(integrationDataJson);
        lenient().when(qmsIntegrationService.fetchQMSBranchList(true, request, reqObj))
        .thenReturn(integrationDataJsonFailure);
        integrationData.doPost(request, res);
        assertEquals(STATUS_ERROR, res.getStatus());

        res.setStatus(STATUS_OK);

        integrationDataJson = new JsonObject();
        lenient().when(requestPathInfo.getSelectors()).thenReturn(branchSlotSelector);
        lenient().when(qmsIntegrationService.fetchQMSBranchSlot(false, request, reqObj))
        .thenReturn(integrationDataJson);
        integrationData.doPost(request, res);
        assertEquals(STATUS_OK, res.getStatus());

        integrationDataJson.addProperty("error", "Invalid token.");
        lenient().when(qmsIntegrationService.fetchQMSBranchSlot(false, request, reqObj))
        .thenReturn(integrationDataJson);
        lenient().when(qmsIntegrationService.fetchQMSBranchSlot(true, request, reqObj))
        .thenReturn(integrationDataJsonFailure);
        integrationData.doPost(request, res);
        assertEquals(STATUS_ERROR, res.getStatus());

        res.setStatus(STATUS_OK);

        integrationDataJson = new JsonObject();
        lenient().when(requestPathInfo.getSelectors()).thenReturn(branchServiceSelector);
        lenient().when(qmsIntegrationService.fetchQMSServiceList(false, request, reqObj))
        .thenReturn(integrationDataJson);
        integrationData.doPost(request, res);
        assertEquals(STATUS_OK, res.getStatus());

        integrationDataJson.addProperty("error", "Invalid token.");
        lenient().when(qmsIntegrationService.fetchQMSServiceList(false, request, reqObj))
        .thenReturn(integrationDataJson);
        lenient().when(qmsIntegrationService.fetchQMSServiceList(true, request, reqObj))
        .thenReturn(integrationDataJsonFailure);
        integrationData.doPost(request, res);
        assertEquals(STATUS_ERROR, res.getStatus());

        res.setStatus(STATUS_OK);

        integrationDataJson = new JsonObject();
        lenient().when(requestPathInfo.getSelectors()).thenReturn(branchBookingConfirmSelector);
        lenient().when(qmsIntegrationService.qmsSubmitBooking(false, request, reqObj))
        .thenReturn(integrationDataJson);
        integrationData.doPost(request, res);
        assertEquals(STATUS_OK, res.getStatus());

        integrationDataJson.addProperty("error", "Invalid token.");
        lenient().when(qmsIntegrationService.qmsSubmitBooking(false, request, reqObj))
        .thenReturn(integrationDataJson);
        lenient().when(qmsIntegrationService.qmsSubmitBooking(true, request, reqObj))
        .thenReturn(integrationDataJsonFailure);
        integrationData.doPost(request, res);
        assertEquals(STATUS_ERROR, res.getStatus());

    }

    @Test
    void doGetStockStartService(final AemContext context) throws ServletException, IOException {
        String[] stockChartselector = {"stockchart"};
        JsonObject integrationDataJson = new JsonObject();
        MockSlingHttpServletResponse res = ctx.response();

        lenient().when(requestPathInfo.getSelectors()).thenReturn(stockChartselector);
        integrationDataJson = new JsonObject();
        lenient().when(stockChartService.fetchStockChartData(false)).thenReturn(integrationDataJson);
        integrationData.doGet(request, res);
        assertEquals(STATUS_OK, res.getStatus());

        integrationDataJson.addProperty("error", "Invalid token.");
        lenient().when(stockChartService.fetchStockChartData(false)).thenReturn(integrationDataJson);
        lenient().when(stockChartService.fetchStockChartData(true)).thenReturn(integrationDataJsonFailure);
        integrationData.doGet(request, res);
        assertEquals(STATUS_ERROR, res.getStatus());
    }

}
