package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MastheadExtendCardArticleImageModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private MastheadExtendCardArticleImageModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(MastheadExtendCardArticleImageModel.class);
        aemContext.load().json(TestConstants.MASTHEADEXTENDCARDARTICLEIMAGE, "/mastheadExtendCardArticleImage");
        aemContext.currentResource(TestConstants.MASTHEADEXTENDCARDARTICLEIMAGECOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(MastheadExtendCardArticleImageModel.class);
    }

    @Test
    void getCardArticleImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCardArticleImage());
    }

    @Test
    void testImageOptimizationPath() {
        assertEquals(model.getCardArticleImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCardArticleImagePath());
        assertEquals(model.getCardArticleImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCardArticleImagePath());
    }

    @Test
    void getCardArticleImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getCardArticleImageAltText());
    }

    @Test
    void getCardArticleTitle() {
        final String expected = "text";
        assertEquals(expected, model.getCardArticleTitle());
    }

    @Test
    void getCardArticleDescrition() {
        final String expected = "text";
        assertEquals(expected, model.getCardArticleDescrition());
    }

}
