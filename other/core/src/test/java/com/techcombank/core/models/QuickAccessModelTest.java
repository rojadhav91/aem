
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.utils.PlatformUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.QuickAccessFileModelImpl;
import com.techcombank.core.models.impl.QuickAccessModelImpl;
import com.techcombank.core.service.VfxRatesDataService;
import com.techcombank.core.service.ResourceResolverService;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;

/**
 * The Class QuickAccessModelTest to perform junit for QuickAccessModel
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class QuickAccessModelTest {

    private final AemContext ctx = new AemContext();

    private QuickAccessModelImpl quickAccessModel;

    protected static final int MAX_INDEX = 6;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page page;

    @Mock
    private Resource resource;

    @Mock
    private VfxRatesDataService vfxRatesDataService;

    private JsonObject exchangeRateDataJson;

    private Page currentPage;

    @BeforeEach
    void setUp() throws Exception {
        mockObjects();
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.QUICK_ACCESS_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(QuickAccessModelImpl.class);
        currentPage = mock(Page.class);
        ctx.registerService(Page.class, currentPage);
        quickAccessModel = getModel("/content/jcr:content/root/container/quickaccess");
        PrivateAccessor.setField(quickAccessModel, "vfxRatesDataService", vfxRatesDataService);
        PrivateAccessor.setField(quickAccessModel, "resourceResolver", resourceResolver);
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "/content/techcombank/web/vn/vi/business.html"))
                .thenReturn("/content/techcombank/web/vn/vi/business");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "/content/techcombank/web/vn/en/business.html"))
                .thenReturn("/content/techcombank/web/vn/en/business");
        lenient().when(PlatformUtils.getMapUrl(resourceResolver, "https://www.google.com"))
                .thenReturn("https://www.google.com");
        lenient().when(resourceResolver.getResource("/content/techcombank/web/vn/vi/business")).thenReturn(resource);
        lenient().when(resourceResolver.getResource("/content/techcombank/web/vn/en/business")).thenReturn(resource);
    }

    private QuickAccessModelImpl getModel(final String currentResource) {
        ctx.currentResource(currentResource);
        Resource res = ctx.request().getResource();
        QuickAccessModelImpl quickAccess = res.adaptTo(QuickAccessModelImpl.class);
        return quickAccess;
    }

    private void mockObjects() {
        resourceResolver = mock(ResourceResolver.class);
        ctx.registerService(ResourceResolver.class, resourceResolver);

        resource = mock(Resource.class);
        ctx.registerService(Resource.class, resource);

        resourceResolverService = mock(ResourceResolverService.class);
        ctx.registerService(ResourceResolverService.class, resourceResolverService);

        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
    }

    @Test
    void testQuickAccessHeading() {
        assertEquals("Exchange Rate", quickAccessModel.getTabOneHeading());
        assertEquals("Service Fees", quickAccessModel.getTabTwoHeading());
        assertEquals("Downlaod Forms", quickAccessModel.getTabThreeHeading());
        assertEquals("BUY", quickAccessModel.getSubbHeadingOne());
        assertEquals("TRANSFER", quickAccessModel.getSubbHeadingTwo());
        assertEquals("SELL", quickAccessModel.getSubbHeadingThree());
        assertEquals("(Cash)", quickAccessModel.getSubbHeadingColOne());
        assertEquals("(Transfer)", quickAccessModel.getSubbHeadingColTwo());
        assertEquals("(sell)", quickAccessModel.getSubbHeadingColThree());
    }

    @Test
    void testQuickAccessLinks() {
        assertEquals("View All", quickAccessModel.getTabOneViewAll());
        assertEquals("View All", quickAccessModel.getTabTwoViewAll());
        assertEquals("View All", quickAccessModel.getTabThreeViewAll());
        assertEquals("/content/techcombank/web/vn/vi/business", quickAccessModel.getTabOneLink());
        assertEquals("/content/techcombank/web/vn/en/business", quickAccessModel.getTabTwoLink());
        assertEquals("https://www.google.com", quickAccessModel.getTabThreeLink());
        assertEquals("other", quickAccessModel.getTabOneLinkInteractionType());
        assertEquals("other", quickAccessModel.getTabTwoLinkInteractionType());
        assertEquals("exit", quickAccessModel.getTabThreeLinkInteractionType());
    }

    @Test
    void testTabIcon() {
        String tabIcon = "/content/dam/techcombank/red-arrow-icon.svg";
        assertEquals(tabIcon, quickAccessModel.getTabOneIcon());
        assertEquals("exchangeAlt", quickAccessModel.getTabOneIconAlt());
        assertEquals("_blank", quickAccessModel.getTabOneTarget());
        assertTrue(quickAccessModel.isTabOneNoFollow());
        assertEquals(tabIcon, quickAccessModel.getTabOneIconWebImagePath());
        assertEquals(tabIcon, quickAccessModel.getTabOneIconMobileImagePath());

        assertEquals(tabIcon, quickAccessModel.getTabTwoIcon());
        assertEquals("ServieAlt", quickAccessModel.getTabTwoIconAlt());
        assertEquals("_blank", quickAccessModel.getTabTwoTarget());
        assertFalse(quickAccessModel.isTabTwoNoFollow());
        assertEquals(tabIcon, quickAccessModel.getTabTwoIconWebImagePath());
        assertEquals(tabIcon, quickAccessModel.getTabTwoIconMobileImagePath());

        assertEquals(tabIcon, quickAccessModel.getTabThreeIcon());
        assertEquals("Downlaod Alt", quickAccessModel.getTabThreeIconAlt());
        assertEquals("_blank", quickAccessModel.getTabThreeTarget());
        assertTrue(quickAccessModel.isTabThreeNoFollow());
        assertEquals(tabIcon, quickAccessModel.getTabThreeIconWebImagePath());
        assertEquals(tabIcon, quickAccessModel.getTabThreeIconMobileImagePath());
    }

    @Test
    void testTabFileSection() {
        List<QuickAccessFileModelImpl> tabTwoFileSection = quickAccessModel.getTabTwoFileSection();
        String file = "/content/dam/techcombank/AWSloginguide.pdf";
        String fileIcon = "/content/dam/techcombank/download.svg";
        lenient().when(resourceResolver.map("/content/dam/techcombank/AWSloginguide.pdf"))
                .thenReturn("/content/dam/techcombank/AWSloginguide.pdf");
        lenient().when(resourceResolver.map("/content/dam/techcombank/download.svg"))
                .thenReturn("/content/dam/techcombank/download.svg");
        assertEquals("Account service fee", tabTwoFileSection.get(0).getFileName());
        assertEquals(file, tabTwoFileSection.get(0).getFilePath());
        assertEquals("download", tabTwoFileSection.get(0).getFileType());
        assertEquals(fileIcon, tabTwoFileSection.get(0).getFileIcon());
        assertEquals("AccountAlt", tabTwoFileSection.get(0).getFileIconAlt());
        assertEquals(fileIcon, tabTwoFileSection.get(0).getFileIconWebImagePath());
        assertEquals(fileIcon, tabTwoFileSection.get(0).getFileIconMobileImagePath());

        List<QuickAccessFileModelImpl> tabThreeFileSection = quickAccessModel.getTabThreeFileSection();
        assertEquals("Open a personal account", tabThreeFileSection.get(0).getFileName());
        assertEquals(file, tabThreeFileSection.get(0).getFilePath());
        assertEquals("download", tabThreeFileSection.get(0).getFileType());
        assertEquals(fileIcon, tabThreeFileSection.get(0).getFileIcon());
        assertEquals("persoanlAlt", tabThreeFileSection.get(0).getFileIconAlt());
        assertEquals(fileIcon, tabThreeFileSection.get(0).getFileIconWebImagePath());
        assertEquals(fileIcon, tabThreeFileSection.get(0).getFileIconMobileImagePath());

    }

    @Test
    void testDateChangeNote() {
        String expected = "As of 11 July";
        List<String> basePath = new ArrayList<>();
        basePath.add("/content/dam/techcombank/master-data/exchange-rates");
        lenient().when(vfxRatesDataService.getVfxRateBasePath(TestConstants.EXCHANGE_RATE, false))
                .thenReturn(basePath);
        lenient().when(vfxRatesDataService.getVfxRateUpdateDate(resourceResolver, basePath.get(0)))
                .thenReturn("2023-07-11 23:30:00");
        lenient().when(request.getResource()).thenReturn(resource);
        lenient().when(resource.getPath()).thenReturn("/content/techcombank/web/vn/en/appoinment-booking");
        lenient().when(request.getResourceResolver()).thenReturn(resourceResolver);
        lenient().when(resourceResolver.getResource(Mockito.any())).thenReturn(resource);
        lenient().when(resource.adaptTo(Page.class)).thenReturn(page);
        lenient().when(page.getLanguage()).thenReturn(Locale.ENGLISH);
        assertEquals(expected, quickAccessModel.getDateChangeNote());
    }

    @Test
    void testExchangeRateList() throws Exception {
        String file = TestConstants.CONTENT_ROOT_SRC_PATH + TestConstants.CONTENT_BASE_PATH
                + TestConstants.EXCHANGE_RATE_JSON;
        JsonReader reader = new JsonReader(new FileReader(file));
        Gson gsonObj = new Gson();
        Map<String, String> exchangeRateData = gsonObj.fromJson(reader, Map.class);
        JsonElement exchangeRate = gsonObj.toJsonTree(exchangeRateData);
        exchangeRateDataJson = exchangeRate.getAsJsonObject();
        JsonArray exchangeData = exchangeRateDataJson.get("exchangeRate").getAsJsonArray();
        List<ExchangeRate> exchangeRateOne = new ArrayList<>();
        List<ExchangeRate> exchangeRateTwo = new ArrayList<>();
        for (int i = 0; exchangeData.size() > i; i++) {
            JsonObject data = exchangeData.get(i).getAsJsonObject();
            ExchangeRate exRate = new ExchangeRate();
            exRate.setAskRate(data.get("askRate").toString());
            exRate.setBidRateCK(data.get("bidRateCK").toString());
            exRate.setBidRateTM(data.get("bidRateTM").toString());
            exRate.setFlag("/content/dam/techcombank/flags/flag-aud.svg");
            exchangeRateOne.add(exRate);

            if (exchangeRateTwo.size() < MAX_INDEX) {
                exchangeRateTwo.add(exRate);
            }

        }

        List<String> basePath = new ArrayList<>();
        basePath.add("/content/dam/techcombank/master-data/exchange-rates");
        lenient().when(vfxRatesDataService.getVfxRateBasePath(TestConstants.EXCHANGE_RATE, false))
                .thenReturn(basePath);
        lenient().when(vfxRatesDataService.getVfxRateUpdateDate(Mockito.any(), Mockito.any()))
                .thenReturn("2023-07-11 23:30:00");
        lenient().when(vfxRatesDataService.getVfxRate(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(resource);
        lenient().when(vfxRatesDataService.getExchangeRateDataList(resource, resourceResolver))
                .thenReturn(exchangeRateOne);
        assertEquals(exchangeRateOne.size() - 1, quickAccessModel.getExchangeRateList().size());
        assertEquals("/content/dam/techcombank/flags/flag-aud.svg", exchangeRateOne.get(0).getFlag());

        lenient().when(vfxRatesDataService.getExchangeRateDataList(resource, resourceResolver))
                .thenReturn(exchangeRateTwo);
        assertEquals(exchangeRateTwo.size(), quickAccessModel.getExchangeRateList().size());
    }
}
