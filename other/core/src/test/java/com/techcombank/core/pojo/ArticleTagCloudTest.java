package com.techcombank.core.pojo;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class ArticleTagCloudTest {
    @Test
    void testArticleTagCloudGetterSetter() {

        ArticleTagCloud articleTagCloud = new ArticleTagCloud();

        articleTagCloud.setName("name");
        articleTagCloud.setTitle("title");
        articleTagCloud.setPath("/content/articleTagCloud");
        articleTagCloud.setOpenInNewTab("true");


        assertEquals("title", articleTagCloud.getTitle());
        assertEquals("/content/articleTagCloud", articleTagCloud.getPath());
        assertEquals("true", articleTagCloud.getOpenInNewTab());
        assertEquals("name", articleTagCloud.getName());

    }

}
