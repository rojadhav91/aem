package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.TermsAndConditionsModelImpl;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith({AemContextExtension.class})
class TermsAndConditionsModelTest {

    private final AemContext context = new AemContext();

    private TermsAndConditionsModelImpl termsAndConditionsModel;

    @BeforeEach
    void setUp() throws Exception {
        context.load().json(TestConstants.TERMS, TestConstants.CONTENTPATH);
        context.addModelsForClasses(TermsAndConditionsModelImpl.class);
        context.currentResource(TestConstants.TERMSCOMP);
        termsAndConditionsModel =
                context.currentResource(TestConstants.TERMSCOMP).adaptTo(TermsAndConditionsModelImpl.class);
    }

    @Test
    void testGetFieldTitle() {
        assertEquals("fieldTitle", termsAndConditionsModel.getFieldTitle());
    }

    @Test
    void testGetFieldName() {
        assertEquals("fieldName", termsAndConditionsModel.getFieldName());
    }

    @Test
    void testGetFieldValue() {
        assertEquals("fieldValue", termsAndConditionsModel.getFieldValue());
    }

    @Test
    void testGetFieldId() {
        assertEquals("fieldId", termsAndConditionsModel.getFieldId());
    }

    @Test
    void testGetRequiredMessage() {
        assertEquals("requiredMessage", termsAndConditionsModel.getRequiredMessage());
    }

    @Test
    void testIsRequired() {
        assertTrue(termsAndConditionsModel.isRequired());
    }

    @Test
    void testIsAnalytics() {
        assertTrue(termsAndConditionsModel.isAnalytics());
    }
}
