package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class SecondApproverParticipantStepTest {
    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private MetaDataMap metaDataMap;

    @InjectMocks
    private SecondApproverParticipantStep secondApproverParticipantStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflowData().getMetaDataMap()).thenReturn(metaDataMap);
        when(metaDataMap.get(TCBConstants.SECOND_APPORVER, String.class)).thenReturn("second.approver@example.com");
    }

    @Test
    void testGetParticipantWithSecondApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(true);
        String participant = secondApproverParticipantStep.getParticipant(workItem, workflowSession, metaDataMap);
        assertEquals("second.approver@example.com", participant);
    }

    @Test
    void testGetParticipantWithoutSecondApprover() throws WorkflowException {
        when(metaDataMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(false);
        String participant = secondApproverParticipantStep.getParticipant(workItem, workflowSession, metaDataMap);
        assertEquals(StringUtils.EMPTY, participant);
    }
}
