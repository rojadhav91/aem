package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CCashScoreBoardModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CCashScoreBoardModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CCashScoreBoardModel.class);
        aemContext.load().json(TestConstants.CCASHSCOREBOARD, "/cCashScoreBoard");
        aemContext.currentResource(TestConstants.CCASHSCOREBOARDCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CCashScoreBoardModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getCriteriaTitle() {
        final String expected = "text";
        assertEquals(expected, model.getCriteriaTitle());
    }

    @Test
    void getAverageScoreTitle() {
        final String expected = "text";
        assertEquals(expected, model.getAverageScoreTitle());
    }

    @Test
    void getTotalScoreTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTotalScoreTitle());
    }

    @Test
    void getCaption() {
        final String expected = "text";
        assertEquals(expected, model.getCaption());
    }

}
