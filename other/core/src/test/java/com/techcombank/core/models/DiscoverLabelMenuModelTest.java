
package com.techcombank.core.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.techcombank.core.constants.TestConstants;
import com.day.cq.wcm.api.WCMMode;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

/**
 * The Class DiscoverLabelMenuTest to perform junit for DiscoverLabelMenu Model
 */
@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class DiscoverLabelMenuModelTest {

    private final AemContext ctx = new AemContext();

    private DiscoverLabelMenuModel discoverLabelMenu;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.HEADER_MODEL_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(NavigationMenuModel.class);
        discoverLabelMenu = getModel(
                "/content/techcombank/web/vn/vi/personal/jcr:content/root/container/header/discoverMenu/item1");
    }

    private DiscoverLabelMenuModel getModel(final String currentResource) {
        ctx.request().setAttribute(WCMMode.class.getName(), WCMMode.EDIT);
        ctx.currentResource(currentResource);
        DiscoverLabelMenuModel discoverLabel = ctx.currentResource().adaptTo(DiscoverLabelMenuModel.class);
        return discoverLabel;
    }

    @Test
    void testDiscoverMenuPageURL() {

        String expected = "/content/techcombank/web/vn/vi.html";
        assertEquals(expected, discoverLabelMenu.getDiscoverMenuPageURL());
        assertEquals("other", discoverLabelMenu.getDiscoverMenuPageURLInteractionType());
    }

    @Test
    void testDiscoverMenuLabel() {
        String expected = "ABOUT US";
        assertEquals(expected, discoverLabelMenu.getDiscoverMenuLabel());
    }

    @Test
    void testDiscoverTarget() {
        String expected = "_self";
        assertEquals(expected, discoverLabelMenu.getDiscoverTarget());
    }
}
