package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.pojo.HrefLangPojo;
import com.techcombank.core.service.ResourceResolverService;
import com.techcombank.core.service.SeoHref;
import com.techcombank.core.testcontext.TCBAemContext;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class SEOPageModelTest {

    private AemContext context = TCBAemContext.newAemContext();
    public static final String PAGE_TITLE = "Personal Page";
    public static final String PAGE_DESCRIPTION = "This is Description for Page";

    @InjectMocks
    private SEOPageModel seoPageModel;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private Page currentPage;

    @Mock
    private SeoHref seoHref;

    @Mock
    private ResourceResolver resourceResolver;


    @BeforeEach
    public void setup() throws NoSuchFieldException {
        context.load().json(TestConstants.JSON.PERSONAL_BANKING_PAGE, TestConstants.CONTENT.PERSONAL_BANKING_PAGE);
        context.currentResource(TestConstants.CONTENT.PERSONAL_BANKING_PAGE + TestConstants.SLASH
                + TestConstants.JCR_CONTENT);
        context.registerService(ResourceResolverService.class, resourceResolverService);
        seoPageModel = context.request().adaptTo(SEOPageModel.class);
        PrivateAccessor.setField(seoPageModel, "seoHref", seoHref);
        PrivateAccessor.setField(seoPageModel, "resourceResolverService", resourceResolverService);
    }

    @Test
    void testSEOPageModelFields() {
        lenient().when(resourceResolverService.getWriteResourceResolver()).thenReturn(resourceResolver);
        assertEquals("SEO Title", seoPageModel.getSeoObj().getSeoTitle());
        assertEquals(TestConstants.PERSONAL_BANKING_PAGE_DOMAIN_PATH, seoPageModel.getPagePath());
        assertEquals(PAGE_TITLE, seoPageModel.getPageTitle());
        assertEquals(PAGE_DESCRIPTION, seoPageModel.getPageDescription());

        List<HrefLangPojo> hrefLangList = new ArrayList<>();
        HrefLangPojo hrefLang = new HrefLangPojo();
        hrefLang.setRel("alternate");
        hrefLang.setHrefLang("vn_en");
        hrefLang.setHref("/content/techcombank/web/vn/en/personal/list-view.html");
        hrefLangList.add(hrefLang);
        lenient().when(seoHref.getPageList(Mockito.any(), Mockito.any()))
                .thenReturn(hrefLangList);
        assertEquals("alternate", seoPageModel.getHrefLangList().get(0).getRel());

    }

}
