package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
 class DebtPanelTest {

    private final AemContext aemContext = AppAemContext.newAemContext();
    private DebtPanel debtPanel;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(DebtPanel.class);
        aemContext.load().json(TestConstants.DEBTPANEL, "/debtPanel");
        aemContext.currentResource(TestConstants.DEBTPANELCOMP);
        Resource resource = aemContext.request().getResource();
        debtPanel = resource.adaptTo(DebtPanel.class);
    }
    @Test
    void depositAmountTest() {
        final String expected = "Deposit Amount";
        assertEquals(expected, debtPanel.getDepositAmountLabel());
    }
    @Test
    void depositAmountToolTest() {
        final String expected = "Deposit smount tool tip";
        assertEquals(expected, debtPanel.getDepositAmountTool());
    }

    @Test
    void depositAmountInputTest() {
        final String expected = "VND";
        assertEquals(expected, debtPanel.getDepositAmountInput());
    }

    @Test
    void intrestRateTest() {
        final String expected = "5";
        assertEquals(expected, debtPanel.getInterestRate());
    }

    @Test
    void intrestRateToolTest() {
        final String expected = "Interest rate (%/year)";
        assertEquals(expected, debtPanel.getInterestRateTool());
    }

    @Test
    void intrestRateSliderTest() {
        final String expected = "4";
        assertEquals(expected, debtPanel.getInterestRateSlider());
    }

    @Test
    void termLabelTextTest() {
        final String expected = "Term";
        assertEquals(expected, debtPanel.getTermLabelText());
    }

    @Test
    void termToolTipTest() {
        final String expected = "Term";
        assertEquals(expected, debtPanel.getTermToolTip());
    }

    @Test
    void toolTipIconTest() {
        final String expected = "/content/dam/techcombank/icon_8c14fabb1a.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, debtPanel.getToolTipIcon());
    }

    @Test
    void currencyTextTest() {
        final String expected = "VND";
        assertEquals(expected, debtPanel.getCurrencyText());
    }

    @Test
    void monthTextTest() {
        final String expected = "Month";
        assertEquals(expected, debtPanel.getMonthText());
    }

    @Test
    void outputPanelBgTest() {
        final String expected = "/content/dam/techcombank/rdb-app"
                + "/images/img2.png/jcr:content/renditions/cq5dam.web.1280.1280.png"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        assertEquals(expected, debtPanel.getOutputPanelBg());
    }

    @Test
    void iconImageTest() {
        final String expected = "/content/dam/techcombank/rdb-app/images"
                + "/sample-2.bmp/jcr:content/renditions/cq5dam.web.1280.1280.jpeg"
                + "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
        assertEquals(expected, debtPanel.getIconImage());
    }

    @Test
    void iconImageAltTest() {
        final String expected = "alt text";
        assertEquals(expected, debtPanel.getIconImageAlt());
    }

    @Test
    void interestPaidLabelTest() {
        final String expected = "Interest paid at maturity";
        assertEquals(expected, debtPanel.getInterestPaidLabel());
    }

    @Test
    void interestPaidOutputAmountTest() {
        final String expected = "VND";
        assertEquals(expected, debtPanel.getInterestPaidOutputAmount());
    }

    @Test
    void totalRecievedLabelTest() {
        final String expected = "Total Received";
        assertEquals(expected, debtPanel.getTotalReceivedLabel());
    }

    @Test
    void totalRecievedOutputAmount() {
        final String expected = "VND";
        assertEquals(expected, debtPanel.getTotalReceivedOutputAmount());
    }


    @Test
    void getOutputPanelBgMobile() {
        final String expected = "/content/dam/techcombank/rdb-app/images"
                + "/img2.png/jcr:content/renditions/cq5dam.web.1280.1280.png"
                + "/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, debtPanel.getOutputPanelBgMobile());
    }

    @Test
    void getIconImageMobile() {
        final String expected = "/content/dam/techcombank/rdb-app"
                + "/images/sample-2.bmp/jcr:content/renditions/cq5dam.web.1280.1280.jpeg"
                + "/jcr:content/renditions/cq5dam.web.640.640.jpeg";
        assertEquals(expected, debtPanel.getIconImageMobile());
    }

    @Test
    void getToolTipIconMobile() {
        final String expected = "/content/dam/techcombank"
                + "/icon_8c14fabb1a.png/jcr:content/renditions/cq5dam.web.640.640.png";
        assertEquals(expected, debtPanel.getToolTipIconMobile());
    }

    @Test
    void getAlt() {
        final String expected = "alt text";
        assertEquals(expected, debtPanel.getBgImageAltText());
    }

    @Test
    void getToolTipAltText() {
        final String expected = "alt text";
        assertEquals(expected, debtPanel.getToolTipAltText());
    }
}
