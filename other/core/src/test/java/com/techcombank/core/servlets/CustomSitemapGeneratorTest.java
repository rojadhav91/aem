package com.techcombank.core.servlets;

import com.techcombank.core.testcontext.TCBAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.sitemap.SitemapException;
import org.apache.sling.sitemap.builder.Sitemap;
import org.apache.sling.sitemap.builder.Url;
import org.apache.sling.sitemap.spi.common.SitemapLinkExternalizer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class CustomSitemapGeneratorTest {

    private AemContext aemContext = TCBAemContext.newAemContext();

    @Mock
    private SitemapLinkExternalizer sitemapLinkExternalizer;

    @Mock
    private Url url;

    @Mock
    private Sitemap sitemap;

    @InjectMocks
    private CustomSitemapGenerator customSitemapGenerator;

    @BeforeEach
    void setUp() throws SitemapException {
        MockitoAnnotations.openMocks(this);
        aemContext.load().json("/content/techcombank/web/vn/en/sitemap.json", "/sitemap");
        lenient().when(sitemap.addUrl(any())).thenReturn(url);
    }

    @Test
    void testCustomSiteMapGenerator() throws Exception {
        //Always
        aemContext.currentResource("/sitemap/always");
        Resource resource = aemContext.resourceResolver().resolve("/sitemap/always");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //Never-Hourly
        aemContext.currentResource("/sitemap/hourly");
        resource = aemContext.resourceResolver().resolve("/sitemap/hourly");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //Daily
        aemContext.currentResource("/sitemap/daily");
        resource = aemContext.resourceResolver().resolve("/sitemap/daily");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //Weekly
        aemContext.currentResource("/sitemap/weekly");
        resource = aemContext.resourceResolver().resolve("/sitemap/weekly");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //Monthly
        aemContext.currentResource("/sitemap/monthly");
        resource = aemContext.resourceResolver().resolve("/sitemap/monthly");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //Yearly
        aemContext.currentResource("/sitemap/yearly");
        resource = aemContext.resourceResolver().resolve("/sitemap/yearly");
        customSitemapGenerator.addResource("testPage", sitemap, resource);
        assertNotNull(customSitemapGenerator);

        //ShouldInclude
        aemContext.currentResource("/sitemap/robots");
        resource = aemContext.resourceResolver().resolve("/sitemap/robots");
        customSitemapGenerator.shouldInclude(resource);
        assertNotNull(customSitemapGenerator);


    }
}
