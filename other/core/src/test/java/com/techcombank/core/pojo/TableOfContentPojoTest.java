package com.techcombank.core.pojo;

import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class TableOfContentPojoTest {

    @Test
    void testTableOfContentPojo() {
        TableOfContentPojo tableOfContentPojo = new TableOfContentPojo();

        tableOfContentPojo.setId("id");
        tableOfContentPojo.setValue("value");

        assertEquals("id", tableOfContentPojo.getId());
        assertEquals("value", tableOfContentPojo.getValue());
    }
}
