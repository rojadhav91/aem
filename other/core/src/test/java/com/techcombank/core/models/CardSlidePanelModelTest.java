package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class CardSlidePanelModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private CardSlidePanelModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(CardSlidePanelModel.class);
        aemContext.load().json(TestConstants.CARDSLIDEPANEL, "/cardSlidePanel");
        aemContext.currentResource(TestConstants.CARDSLIDEPANELCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(CardSlidePanelModel.class);
    }

    @Test
    void getTitle() {
        final String expected = "text";
        assertEquals(expected, model.getTitle());
    }

    @Test
    void getDescription() {
        final String expected = "richtext";
        assertEquals(expected, model.getDescription());
    }

    @Test
    void getBackgroundColour() {
        final String expected = "color-code";
        assertEquals(expected, model.getBackgroundColour());
    }

    @Test
    void getImage1() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage1());
    }

    @Test
    void testImage1OptimizationPath() {
        assertEquals(model.getImage1() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImage1Path());
        assertEquals(model.getImage1() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImage1Path());
    }

    @Test
    void getImageAltText1() {
        final String expected = "text";
        assertEquals(expected, model.getImageAltText1());
    }

    @Test
    void getImage2() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage2());
    }

    @Test
    void testImage2OptimizationPath() {
        assertEquals(model.getImage2() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImage2Path());
        assertEquals(model.getImage2() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImage2Path());
    }

    @Test
    void getImageAltText2() {
        final String expected = "text";
        assertEquals(expected, model.getImageAltText2());
    }

    @Test
    void getCenterImage() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getCenterImage());
    }

    @Test
    void testCenterImageOptimizationPath() {
        assertEquals(model.getCenterImage() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebCenterImagePath());
        assertEquals(model.getCenterImage() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileCenterImagePath());
    }

    @Test
    void getCenterImageAltText() {
        final String expected = "text";
        assertEquals(expected, model.getCenterImageAltText());
    }

    @Test
    void getImage4() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage4());
    }

    @Test
    void testImage4OptimizationPath() {
        assertEquals(model.getImage4() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImage4Path());
        assertEquals(model.getImage4() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImage4Path());
    }

    @Test
    void getImageAltText4() {
        final String expected = "text";
        assertEquals(expected, model.getImageAltText4());
    }

    @Test
    void getImage5() {
        final String expected = "/content/dam/techcombank/images/test/test.png";
        assertEquals(expected, model.getImage5());
    }

    @Test
    void testImage5OptimizationPath() {
        assertEquals(model.getImage5() + TestConstants.PNG_WEB_IMAGE_RENDITION_PATH,
                model.getWebImage5Path());
        assertEquals(model.getImage5() + TestConstants.PNG_MOBILE_IMAGE_RENDITION_PATH,
                model.getMobileImage5Path());
    }

    @Test
    void getImageAltText5() {
        final String expected = "text";
        assertEquals(expected, model.getImageAltText5());
    }

}
