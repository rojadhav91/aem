package com.techcombank.core.models;

import com.day.cq.wcm.api.Page;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.models.impl.RecaptchaModelImpl;
import com.techcombank.core.service.RecaptchaService;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.util.Optional;

/**
 * The Class RecaptchaModelTest to perform junit for RecaptchaModel
 */
@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class RecaptchaModelTest {

    private final AemContext ctx = new AemContext();

    @InjectMocks
    private RecaptchaModelImpl recaptchaModel;

    @Mock
    private RecaptchaService recaptchaService;
    @Mock
    private Page currentPage;

    @BeforeEach
    void setUp() throws Exception {
        ctx.load().json(TestConstants.CONTENT_BASE_PATH + TestConstants.RECAPTCHA_JSON, TestConstants.CONTENTPATH);
        ctx.addModelsForClasses(RecaptchaModelImpl.class);
        ctx.currentResource("/content/jcr:content/root/container/recaptcha");
        recaptchaService = mock(RecaptchaService.class);
        ctx.registerService(RecaptchaService.class, recaptchaService);

    }

    @Test
    void testKeys() {
        Optional<String> siteKeyValue = Optional.of("XXsite%XX&Key");
        Optional<String> secretKeyValue = Optional.of("XXsecret%XX&Key");
        lenient().when(recaptchaService.getSiteKey(Mockito.any())).thenReturn(siteKeyValue);
        lenient().when(recaptchaService.getSecretKey(Mockito.any())).thenReturn(secretKeyValue);
        recaptchaModel = ctx.request().adaptTo(RecaptchaModelImpl.class);
        assertEquals("XXsite%XX&Key", recaptchaModel.getSiteKey());
        assertEquals("XXsecret%XX&Key", recaptchaModel.getSecretKey());
        assertEquals("Captcha Verification is Required", recaptchaModel.getRequiredMessage());
        assertTrue(recaptchaModel.isRequired());

        siteKeyValue = Optional.empty();
        secretKeyValue = Optional.empty();
        lenient().when(recaptchaService.getSiteKey(Mockito.any())).thenReturn(siteKeyValue);
        lenient().when(recaptchaService.getSecretKey(Mockito.any())).thenReturn(secretKeyValue);

        recaptchaModel = ctx.request().adaptTo(RecaptchaModelImpl.class);
        assertEquals(StringUtils.EMPTY, recaptchaModel.getSiteKey());
        assertEquals(StringUtils.EMPTY, recaptchaModel.getSecretKey());

    }


}
