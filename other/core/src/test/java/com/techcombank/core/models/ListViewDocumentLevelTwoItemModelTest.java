package com.techcombank.core.models;

import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListViewDocumentLevelTwoItemModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private ListViewDocumentLevelTwoItemModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(ListRowModel.class);
        aemContext.load().json(TestConstants.LISTVIEWDOCUMENTLEVELTWO, "/listViewDocumentItemsSecondLevel");
        aemContext.currentResource(TestConstants.LISTVIEWDOCUMENTLEVELTWOCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(ListViewDocumentLevelTwoItemModel.class);
    }

    @Test
    void getDocumentTitleChild() {
        final String expected = "Text Title";
        assertEquals(expected, model.getDocumentTitleChild());
    }

    @Test
    void getLabel() {
        final String expected = "Download";
        assertEquals(expected, model.getLabel());
    }

    @Test
    void getIcon() {
        final String expected = "/content/dam/techcombank/tcb_qa/folder-for-test/red_arrow_54cb6ae59e.svg";
        assertEquals(expected, model.getIcon());
    }

    @Test
    void getTypeOfOutput() {
        final String expected = "download";
        assertEquals(expected, model.getTypeOfOutput());
    }

    @Test
    void getUrl() {
        final String expected = "/content/techcombank/tcb_qa";
        assertEquals(expected, model.getUrl());
    }

    @Test
    void getWebIcon() {
        final String expected = "/content/dam/techcombank/tcb_qa/folder-for-test/red_arrow_54cb6ae59e.svg";
        assertEquals(expected, model.getWebIcon());
    }


    @Test
    void getMobileIcon() {
        final String expected = "/content/dam/techcombank/tcb_qa/folder-for-test/red_arrow_54cb6ae59e.svg";
        assertEquals(expected, model.getMobileIcon());
    }

    @Test
    void getWebInteractionType() {
        assertEquals("exit", model.getWebInteractionType());
    }

}
