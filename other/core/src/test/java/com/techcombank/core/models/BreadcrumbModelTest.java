package com.techcombank.core.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.techcombank.core.constants.TestConstants;
import com.techcombank.core.testcontext.AppAemContext;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class BreadcrumbModelTest {
    private final AemContext aemContext = AppAemContext.newAemContext();
    private BreadcrumbModel model;

    @BeforeEach
    void setUp() {
        aemContext.addModelsForClasses(BreadcrumbModel.class);
        aemContext.load().json(TestConstants.BREADCRUMB, "/breadcrumb");
        aemContext.currentResource(TestConstants.BREADCRUMBCOMP);
        Resource resource = aemContext.request().getResource();
        model = resource.adaptTo(BreadcrumbModel.class);
    }

    @Test
    void isHideBreadcrumb() {
        final boolean expected = true;
        assertEquals(expected, model.isHideBreadcrumb());
    }

}
