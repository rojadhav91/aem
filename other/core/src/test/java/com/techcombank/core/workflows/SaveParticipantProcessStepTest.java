package com.techcombank.core.workflows;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.EmailService;
import com.techcombank.core.testcontext.TestConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SaveParticipantProcessStepTest {

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private Workflow workflow;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private ValueMap valueMap;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private SaveParticipantProcessStep saveParticipantProcessStep;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workItem.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflow.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn("/content/sample/page");
        when(workflowSession.adaptTo(ResourceResolver.class)).thenReturn(resolver);
        when(resolver.getResource("/content/documents/history/" + TCBConstants.WORKITEM_METADATA)).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(valueMap);
        when(metaDataMap.containsKey(TCBConstants.FIRST_APPORVER)).thenReturn(true);
        when(valueMap.get(TCBConstants.FIRST_APPORVER, String.class)).thenReturn("FirstApprover");
        when(metaDataMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(true);
        when(valueMap.get(TCBConstants.SECOND_APPORVER, String.class)).thenReturn("SecondApprover");
        when(metaDataMap.containsKey(TCBConstants.TARGET_PATH)).thenReturn(true);
        when(valueMap.get(TCBConstants.TARGET_PATH, String.class)).thenReturn("TargetPath");
        when(workItem.getWorkflowData().getPayload()).thenReturn("/content/documents/payload");
        when(workflowMetadataMap.get(TCBConstants.HISTORY_ENTRY_PATH, String.class)).
                thenReturn("/content/documents/history/");
    }

    @Test
    void testExecuteWithApprovers() throws WorkflowException {
        when(valueMap.containsKey(TCBConstants.FIRST_APPORVER)).thenReturn(true);
        when(valueMap.containsKey(TCBConstants.SECOND_APPORVER)).thenReturn(true);
        when(valueMap.containsKey(TCBConstants.TARGET_PATH)).thenReturn(true);
        saveParticipantProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_2)).getWorkflowData();
    }

    @Test
    void testExecuteWithoutApprovers() throws WorkflowException {
        when(workflowMetadataMap.get(TCBConstants.HISTORY_ENTRY_PATH, String.class)).thenReturn(StringUtils.EMPTY);
        saveParticipantProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_2)).getWorkflowData();
    }
}
