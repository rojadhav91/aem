package com.techcombank.core.workflows;

import com.adobe.granite.workflow.PayloadMap;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.techcombank.core.constants.TCBConstants;
import com.techcombank.core.service.WorkflowNotificationService;
import com.techcombank.core.testcontext.AppAemContext;
import com.techcombank.core.testcontext.TestConstants;
import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({AemContextExtension.class, MockitoExtension.class})
class CheckDuplicateWorkflowProcessStepTest {

    private final AemContext aemContext = AppAemContext.newAemContext();

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private MetaDataMap metaDataMap;

    @Mock
    private MetaDataMap workflowMetadataMap;

    @Mock
    private Workflow workflow;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PayloadMap payloadMap;

    @Mock
    private ValueMap valueMap;

    @Mock
    private Resource resource;

    @Mock
    private WorkflowNotificationService workflowNotificationService;

    @InjectMocks
    private CheckDuplicateWorkflowProcessStep checkDuplicateWorkflowProcessStep;

    public static final String VARPATH = "/var/workflow/packages/generated-package12";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(workflowData.getMetaDataMap()).thenReturn(workflowMetadataMap);
        when(workItem.getWorkflowData()).thenReturn(workflowData);
        when(workItem.getWorkflow()).thenReturn(workflow);
        when(workflow.getInitiator()).thenReturn("user");
        when(workflowData.getMetaDataMap()).thenReturn(metaDataMap);
        when(workflowData.getPayload()).thenReturn("/var/sample/page");
        when(workflowSession.adaptTo(ResourceResolver.class)).thenReturn(resourceResolver);
        when(resourceResolver.adaptTo(PayloadMap.class)).thenReturn(payloadMap);
    }

    @Test
    void testExecuteWithDuplicateWorkflows() throws WorkflowException {
        String payload = workItem.getWorkflowData().getPayload().toString();
        List<Workflow> workflowInstances = Collections.singletonList(workflow);
        when(payloadMap.getWorkflowInstances(payload, true)).thenReturn(workflowInstances);
        String resPath = payload.toString().concat(TCBConstants.VAR_FILTER);
        resource = aemContext.create().resource("/var/sample/page", "sling:resourceType", "yourApp/components/page");
        aemContext.create().resource(resource, "child1", "root", VARPATH);
        aemContext.create().resource(resource, "child2", "sling:resourceType", "yourApp/components/page");
        List<Workflow> workflowList = new ArrayList<Workflow>();
        Workflow w1 = mock(Workflow.class);
        Workflow w2 = mock(Workflow.class);
        workflowList.add(w1);
        workflowList.add(w2);
        when(payloadMap.getWorkflowInstances(VARPATH, true)).thenReturn(workflowList);
        aemContext.load().json(TestConstants.JSON.BULK_CONTENT_PAGE,
                VARPATH);
        when(resourceResolver.getResource(resPath)).thenReturn(resource);
        checkDuplicateWorkflowProcessStep.execute(workItem, workflowSession, metaDataMap);
        verify(workItem, times(TestConstants.CALL_COUNT_3)).getWorkflowData();
    }
}
