package com.techcombank.core.models;

import com.techcombank.core.service.ResourceResolverService;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;
import junitx.util.PrivateAccessor;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class ListEventTileItemTest {

    private ListEventTileItem listEventTileItem;

    @Mock
    private ResourceResolverService resourceResolverService;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        listEventTileItem = new ListEventTileItem();
        PrivateAccessor.setField(listEventTileItem, "cardImage", "/content/dam/techcombank/brand-assets/images/icon_fb_40684f140a.png");
        PrivateAccessor.setField(listEventTileItem, "cardImageAltText", "CardImageAltText");
        PrivateAccessor.setField(listEventTileItem, "cardLabelText", "CardLabelText");
        PrivateAccessor.setField(listEventTileItem, "cardTitle", "Card Title");
        PrivateAccessor.setField(listEventTileItem, "cardDescription", "Card Description");
        PrivateAccessor.setField(listEventTileItem, "linkIcon", "/content/dam/techcombank/brand-assets/images/icon_fb_40684f140a.png");
        PrivateAccessor.setField(listEventTileItem, "linkIconAltText", "linkIconAltText");
        PrivateAccessor.setField(listEventTileItem, "linkText", "Link Text");
        PrivateAccessor.setField(listEventTileItem, "linkUrl", "/content/techcombank/web/vn/en/abc.html");
        PrivateAccessor.setField(listEventTileItem, "eventDate", "2023-12-27T10:15:30Z");
        PrivateAccessor.setField(listEventTileItem, "openInNewTab", "true");
        PrivateAccessor.setField(listEventTileItem, "noFollow", "true");
        PrivateAccessor.setField(listEventTileItem, "linkInteraction", "{'webInteractions': {'name': 'Social Share','type': 'exit'}}");
        PrivateAccessor.setField(listEventTileItem, "resourceResolverService", resourceResolverService);
    }

    @Test
    void testInit() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        lenient().when(resourceResolverService.getReadResourceResolver()).thenReturn(resourceResolver);
        final String expected = "init";
        Method initMethod = ListEventTileItem.class.getDeclaredMethod("init");
        initMethod.setAccessible(true);
        initMethod.invoke(listEventTileItem);
        assertEquals("init", expected);
    }

    @Test
    void getCardImage() {
        final String expected = "/content/dam/techcombank/brand-assets/images/icon_fb_40684f140a.png";
        assertEquals(expected, listEventTileItem.getCardImage());
    }

    @Test
    void getCardImageAltText() {
        final String expected = "CardImageAltText";
        assertEquals(expected, listEventTileItem.getCardImageAltText());
    }

    @Test
    void getCardLabelText() {
        final String expected = "CardLabelText";
        assertEquals(expected, listEventTileItem.getCardLabelText());
    }

    @Test
    void getCardTitle() {
        final String expected = "Card Title";
        assertEquals(expected, listEventTileItem.getCardTitle());
    }

    @Test
    void getCardDescription() {
        final String expected = "Card Description";
        assertEquals(expected, listEventTileItem.getCardDescription());
    }

    @Test
    void getLinkIcon() {
        final String expected = "/content/dam/techcombank/brand-assets/images/icon_fb_40684f140a.png";
        assertEquals(expected, listEventTileItem.getLinkIcon());
    }

    @Test
    void getLinkIconAltText() {
        final String expected = "linkIconAltText";
        assertEquals(expected, listEventTileItem.getLinkIconAltText());
    }

    @Test
    void getLinkText() {
        final String expected = "Link Text";
        assertEquals(expected, listEventTileItem.getLinkText());
    }

    @Test
    void getLinkUrl() {
        final String expected = "/content/techcombank/web/vn/en/abc.html";
        assertEquals(expected, listEventTileItem.getLinkUrl());
    }

    @Test
    void getEventDate() {
        Instant instant = Instant.parse(listEventTileItem.getEventDate());
        LocalDateTime localDateTime = instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
        assertEquals("27/12/2023", localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }

    @Test
    void getOpenInNewTab() {
        final String expected = "true";
        assertEquals(expected, listEventTileItem.getOpenInNewTab());
    }

    @Test
    void getNoFollow() {
        final String expected = "true";
        assertEquals(expected, listEventTileItem.getNoFollow());
    }

    @Test
    void getLinkInteraction() {
        assertEquals("{'webInteractions': {'name': 'Social Share','type': 'exit'}}",
                listEventTileItem.getLinkInteraction());
    }

}