$(document).ready(function () {
  var insuranceOption = $('.column-panel-component');
  var insuranceReadMore = insuranceOption.find('.read-more');
  var insuranceReadMoreBtn = insuranceReadMore.find('button');
  var insuranceExpand = insuranceReadMore.find('.expand');
  var listInsurance = insuranceOption.find('.list-option');
  var listImageTitle = insuranceOption[0] ? [...insuranceOption[0].querySelectorAll('.image-title')] : [];
  var maxHeight = Math.max(...listImageTitle.map(item => item.clientHeight));
  var listDescription = insuranceOption.find('.content-description');

  var itemPanel = insuranceOption.find('.item-panel');
  var itemOptions0 = $(itemPanel[0]).find('.option');
  var itemOptions01 = $(itemPanel[1]).find('.option');
  var itemOptions02 = $(itemPanel[2]).find('.option');

  var numberOfitemOptions0 = itemOptions0.length;
  var numberOfitemOptions01 = itemOptions01.length;
  var numberOfitemOptions02 = itemOptions02.length;
  var numberOfOptions = numberOfitemOptions0 + numberOfitemOptions01 + numberOfitemOptions02;
  
  var viewMoreLabel = $(".read-more").data('viewmore'); // changed by adobe
  var viewLessLabel = $(".read-more").data('viewless'); // changed by adobe

  updateHeightTitle();
  updateToolTipText();


  function updateHeightTitle() {
    if($(window).width() > 767 ) {
      listImageTitle.forEach(item => {
        item.style.height = maxHeight + 'px';
      });
    } else {
      $(listImageTitle).removeAttr('style');
    }
  }

  function updateToolTipText() {
    updateToolTipTextTitle();
    updateToolTipTextDes();
  }

  function updateToolTipTextDes() {
    listDescription.each(function() {
      $(this).find('.tooltiptext').remove();
      if( ($(this)[0].offsetHeight < $(this)[0].scrollHeight) && !$(this).find('.tooltiptext').length ) {
        const toolTipText = $(`<span class="tooltiptext"></span>`);
        toolTipText.text($(this).text());
        $(this).append(toolTipText);
      }
    });
  }

  function updateToolTipTextTitle() {
    $(listImageTitle).each(function() {
      $(this).find('.tooltiptext').remove();
      if( ($(this)[0].offsetHeight < $(this)[0].scrollHeight) && !$(this).find('.tooltiptext').length ) {
        const toolTipText = $(`<span class="tooltiptext"></span>`);
        toolTipText.text($(this).text());
        $(this).append(toolTipText);
      }
    });
  }

  insuranceOption.each(function () {
    var showedItem = countShowedItem(itemOptions0) + countShowedItem(itemOptions01) + countShowedItem(itemOptions02);
    var i = getListStartPoint(listInsurance);
    var temp = showedItem;
    var step = [3, 3, 3];

    // hide read-more button if no-content to load
    if (numberOfitemOptions0 <= 2 && numberOfitemOptions01 <= 2 && numberOfitemOptions02 <= 2) {
      insuranceReadMore.css('display', 'none');
    }

    // click readmore button
    insuranceReadMore.click(function () {
      // function use in screen > 767px
      if ($(window).width() >= 767) {
        // back to original state when all items be showed
        if (checkAllItemBeShowed(listInsurance, i)) {
          for (n = 0; n < listInsurance.length; n++) {
            var listOption = $(listInsurance[n]).find('.option');
            var numberOfOption = listOption.length;

            for (j = getStartPoint(listOption); j < numberOfOption; j++) {
              listOption[j].style.display = 'none';
            }
          }

          // change btn + set initial value
          insuranceReadMoreBtn.text(viewMoreLabel); // changed by adobe
          insuranceExpand.css('transform', 'rotate(360deg)');
          i = getListStartPoint(listInsurance);
          step = [3, 3, 3];
          showedItem = temp;
        } else {
          // load items
          for (m = 0; m < listInsurance.length; m++) {
            var listOption = $(listInsurance[m]).find('.option');
            var numberOfOption = listOption.length;

            if (step[m] >= numberOfOption) {
              for (i[m]; i[m] < numberOfOption; i[m]++) {
                listOption[i[m]].style.display = 'flex';
                showedItem++;
              }
            } else {
              for (i[m]; i[m] <= step[m]; i[m]++) {
                listOption[i[m]].style.display = 'flex';
                showedItem++;
              }
            }

            step[m] = i[m] + 1;
          }

          changedBtn(showedItem, numberOfOptions);
        }
      }
      updateToolTipTextDes();
    });

    // change btn
    function changedBtn(showedItem, numberOfItem) {
      if (showedItem == numberOfItem) {
        insuranceReadMoreBtn.text(viewLessLabel); // changed by adobe
        insuranceExpand.css('transform', 'rotate(180deg)');
      }
    }

    // return number of showed item
    function countShowedItem(listOfOptions) {
      var count = 0;
      for (i = 0; i < listOfOptions.length; i++) {
        var option = $(listOfOptions[i]);
        if (option.css('display') == 'flex') {
          count++;
        }
      }
      return count;
    }

    // get start point of every list to loop
    function getStartPoint(listOfOptions) {
      if (countShowedItem(listOfOptions) > 2) {
        return 2;
      }
      return countShowedItem(listOfOptions);
    }

    // return list of start point
    function getListStartPoint(listInsurance) {
      var i = [];
      for (j = 0; j < listInsurance.length; j++) {
        var listOptions = $(listInsurance[j]).find('.option');
        i.push(getStartPoint(listOptions));
      }
      return i;
    }

    // check if all item be showed
    function checkAllItemBeShowed(listInsurance, i) {
      var result = 0;
      for (j = 0; j < listInsurance.length; j++) {
        var listOptions = $(listInsurance[j]).find('.option');
        if (i[j] == listOptions.length) {
          result++;
        }
      }
      if (result == listInsurance.length) {
        return true;
      }
      return false;
    }
  });
  $(window).resize(()=> {
    updateHeightTitle();
    updateToolTipText();
  });
});