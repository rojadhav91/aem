// Inspire Exclusive Component
import Swiper, { Mousewheel, Navigation, Pagination, FreeMode, Virtual, Autoplay } from 'swiper';

const inspireExclusive = $('.inspire-exclusive');
if (inspireExclusive.length) {
  const iETabMenuItem = inspireExclusive.find('.inspire-exclusive__tab-menu .tab-menu-item');
  const iETabList = inspireExclusive.find('.inspire-exclusive__tab-list');
  const multiPictures = inspireExclusive.find('.content-item__multi-picture');
  const currentLanguage = inspireExclusive.attr('lang');
  const inspireExclusiveConMobile = inspireExclusive.find('.inspire-exclusive__container[responsive-type="mobile"]');
  const tabMenuMobileItem = inspireExclusiveConMobile.find('.tab-menu-mobile-item');
  const tabAllMenuContent = tabMenuMobileItem.find('.tab-menu-mobile-content');
  const allTitleIconText = tabAllMenuContent.find('.dropdown-title-icon p');

  // First time load page on desktop
  refreshTabMenuColor();
  const iETabMenuFirItem = "#DF7F45";
  const iETabFirListItems = iETabList.eq(0).find('.tab-list__content-item:not(:last-child)');
  iETabMenuItem.eq(0).addClass('active');
  iETabMenuItem.eq(0).css('background-color', iETabMenuFirItem);
  iETabMenuItem.eq(0).find('h3').css('color', '');

  iETabList.eq(0).addClass('active');
  iETabFirListItems.css('border-bottom', '1px solid ' + iETabMenuFirItem);

  // Load background-color for tab-menu and
  // border-bottom-color for each list items content except last item on mobile
  tabMenuMobileItem.each(function () {
    var tabMobileColorIndex = $(this).attr('color-tab');
    var tabMobileColor = getTabColor(tabMobileColorIndex, "active");
    $(this).find('.tab-menu-mobile-content').css('background-color', tabMobileColor);
    $(this)
      .find('.tab-list-mobile__content-item:not(:last-child) .content-item__container')
      .css('border-bottom', '1px solid ' + tabMobileColor);
  });

  // Click event handle for tab-menu-mobile
  tabMenuMobileItem.each(function (i) {
    var tabMobileItem = $(this);
    var tabMenuMobileContentItem = tabMobileItem.find('.tab-menu-mobile-content');
    var currentMenuCollapIconSvg = tabMobileItem.find('.collapse-icon-content svg');

    tabMenuMobileContentItem.click(function (event) {
      activeTabMenuMobile(event, tabMobileItem, currentLanguage);
    });

    currentMenuCollapIconSvg.click(function (event) {
      activeTabMenuMobile(event, tabMobileItem, currentLanguage);
    });
  });

  // Handle color for tabs
  function getTabColor(tabIndex, tabStatus) {
    if(tabStatus == 'active') {
      switch (Number(tabIndex) % 4) {
        case 0:
          return "#DF7F45";
        case 1:
          return "#3E8E87";
        case 2:
          return "#9B7C29";
        case 3:
          return "#14254C";
      }
    }
    else {
      switch (Number(tabIndex) % 4) {
        case 0:
          return "#FCF1ED";
        case 1:
          return "#DBF4F2";
        case 2:
          return "#F5EEE1";
        case 3:
          return "#DFE3EA";
      }
    }
  }

  function getTabTitleColor(tabIndex) {
    switch (Number(tabIndex) % 4) {
      case 0:
        return "#EBB28F";
      case 1:
        return "#61BBB6";
      case 2:
        return "#C2B17F";
      case 3:
        return "#727D94";
    }
  }

  // Handle event active tab menu mobile
  function activeTabMenuMobile(event, tabMobileItem, currentLanguage) {
    if (tabMobileItem.hasClass('active')) {
      tabMobileItem.toggleClass('active');
      if ($(event.currentTarget).is('.tab-menu-mobile-content')) {
        $(event.currentTarget).find('.dropdown-title-icon p').text(currentLanguage == 'Vietnamese' ? 'Xem thêm' : 'View more');
      } else {
        $(event.currentTarget)
          .parents()
          .siblings('.tab-menu-mobile-content')
          .find('.dropdown-title-icon p')
          .text(currentLanguage == 'Vietnamese' ? 'Xem thêm' : 'View more');
      }
    } else {
      tabMenuMobileItem.removeClass('active');
      allTitleIconText.each(function () {
        $(this).text(currentLanguage == 'Vietnamese' ? 'Xem thêm' : 'View more');
      });
      tabMobileItem.toggleClass('active');
      if ($(event.currentTarget).is('.tab-menu-mobile-content')) {
        $(event.currentTarget).find('.dropdown-title-icon p').text(currentLanguage == 'Vietnamese' ? 'Thu gọn' : 'View less');
      } else {
        $(event.currentTarget)
          .parents()
          .siblings('.tab-menu-mobile-content')
          .find('.dropdown-title-icon p')
          .text(currentLanguage == 'Vietnamese' ? 'Thu gọn' : 'View less');
      }
      //scroll to selected content
      if($('.tab-menu-mobile-item.active').prev('div').length) {
        $('.tab-menu-mobile-item.active').prev('div')[0].scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'center'
        })
      }
    }
  }

  // Click event handle for tab-menu
  iETabMenuItem.each(function (index) {
    var tabMenuItem = $(this);
    var tabMenuItemTitle = tabMenuItem.find('h3');
    var colorTabActiveIndex = tabMenuItem.attr('color-tab');
    var colorTabActive = getTabColor(colorTabActiveIndex, 'active');

    tabMenuItem.click(function () {
      iETabMenuItem.removeClass('active');
      iETabList.removeClass('active');
      refreshTabMenuColor();
      tabMenuItem.toggleClass('active');
      iETabList.eq(index).toggleClass('active');
      var tabListContentItems = iETabList.eq(index).find('.tab-list__content-item:not(:last-child)');
      if ($(this).hasClass('active')) {
        tabMenuItem.css('background-color', colorTabActive);
        tabMenuItemTitle.css('color', '');
      }

      if (iETabList.eq(index).hasClass('active')) {
        tabListContentItems.css('border-bottom', '1px solid ' + colorTabActive);
      }
    });
  });

  // Refresh color for tab-menu
  function refreshTabMenuColor() {
    iETabMenuItem.each(function (index) {
      var tabMenuItem = $(this);
      var tabMenuItemTitle = tabMenuItem.find('h3');
      var colorTabUnactiveIndex = tabMenuItem.attr('color-tab');
      var colorTabUnactive = getTabColor(colorTabUnactiveIndex, 'unactive');
      var colorTitleUnactiveIndex = tabMenuItemTitle.attr('color-title');
      var colorTitleUnactive = getTabTitleColor(colorTitleUnactiveIndex);

      tabMenuItem.css('background-color', colorTabUnactive);
      tabMenuItemTitle.css('color', colorTitleUnactive);
    });
  }

  $('.content-item__multi-picture').each(function() {
    $(this).find('.swiper2:nth-child(even)').attr('dir', 'rtl');
  });

  $('.content-item__multi-picture').each(function() {
    $(this).find('.swiper2').each(function(indexKey, element) {
      let swiperScrolling = new Swiper(element, {
        speed: 1000,
        modules: [FreeMode, Autoplay],
        // Optional parameters
        slidesPerView: 'auto',
        spaceBetween: 8,
        loop: true,
        loopedSlides: 5,
        loopedSlidesLimit: true,
        maxBackfaceHiddenSlides: 10,
        freeMode: {
          enabled: true,
          minimumVelocity: 0.02
        }
      });

      let getTranslate2 = swiperScrolling.translate;
      swiperScrolling.setTranslate(getTranslate2 + 135 / indexKey);
      if (!getTranslate2) {
        getTranslate2 = 135/4;
      }
      swiperScrolling.initialTranslate = getTranslate2;

      document.body.addEventListener('scroll', function(event) {
        var st = $(this).scrollTop();
        let currentProgress = st / $(document).innerHeight();
        if (indexKey % 2 === 0) {
          let progress = Math.min(Math.max(currentProgress, 0), 1);
          const max2 = swiperScrolling.snapGrid[parseInt(swiperScrolling.snapGrid.length / 2)];
          const current = getTranslate2 + max2 * progress;
          swiperScrolling.translateTo(current, 500);
        } else {
          let progress = Math.min(Math.max(currentProgress, 0), 1);
          const max2 = swiperScrolling.snapGrid[parseInt(swiperScrolling.snapGrid.length / 2)];
          const current = (getTranslate2 - max2 * progress) * -1;
          swiperScrolling.translateTo(current, 500);
        }
      });
    });
  });
}
