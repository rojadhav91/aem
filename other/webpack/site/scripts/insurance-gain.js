$(document).ready(function () {
  $('.insurance-gain__panel .input-items').click(function(){
    let date = $(this).find('input[name=day]').val() +'-'+ $(this).find('input[name=month]').val() +'-'+ $(this).find('input[name=year]').val();
    let gender = $(this).find('.input-fields__drop-down .select-option .gender.selected').text();
    let moneyVal = $(this).find('input[name=moneyValue]').val() 
    let inputInfoValue = $(this).data().trackingClickInfoValue
    let inputJsonStr = inputInfoValue.replace(/'/g, '"');
    let inputJson = JSON.parse(inputJsonStr);
    inputJson.calculatorFields = `${date} | ${gender} | ${moneyVal}`;
    let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
        $(this).attr("data-tracking-click-info-value", updatedValues);
  })
  
  $('.insurance-gain__panel .input-items input[name=moneyValue]').keyup(function(){
    let date = $(this).parents('.input-items').find('input[name=day]').val() +'-'+ $(this).parents('.input-items').find('input[name=month]').val() +'-'+ $(this).parents('.input-items').find('input[name=year]').val();
    let gender = $(this).parents('.input-items').find('.input-fields__drop-down .select-option .gender.selected').text();
    let moneyVal = $(this).val() 
    let inputInfoValue = $(this).parents('.input-items').data().trackingClickInfoValue
    let inputJsonStr = inputInfoValue.replace(/'/g, '"');
    let inputJson = JSON.parse(inputJsonStr);
    inputJson.calculatorFields = `${date} | ${gender} | ${moneyVal}`;
    let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
        $(this).parents('.input-items').attr("data-tracking-click-info-value", updatedValues);
       
  })
  
  $('.insurance-gain__panel .input-items input').keyup(function(){
    $(this).parents('.input-items').trigger('click')
  })
});
