$(document).ready(function () {
  const lookingForInput = document.querySelector('.tcb-hero-banner_looking-for-wrapper [name="search-document"]');
  const lookingForSubmit = document.querySelector('.looking-for_submit');
  const lookingForOptions = document.querySelector('.looking-for_input-options-roots');
  const lookingForSubmitActiveClass = 'active';
  const breadcrumb = $('.breadcrumb').find('.hero-breadcrumb-container');
  const content = $('.mastheadsimplebanner').find('.external-component');

  if(!breadcrumb.length) {
    content.addClass("external-component_p-0");
  }

  const showOptions = (isShow = true) => {
    if (!lookingForOptions) {
      return;
    }

    lookingForOptions.style.height = isShow
      ? `${document.querySelector('.looking-for_input-options-content').clientHeight}px`
      : 0;
  };

  if (lookingForInput) {
    lookingForInput.addEventListener('focus', function () {
      if (lookingForSubmit) {
        lookingForSubmit.classList.add(lookingForSubmitActiveClass);
        if (!lookingForInput.value) {
          showOptions();
        }
      }
    });
    lookingForInput.addEventListener('keyup', function () {
      if (lookingForInput.value) {
        showOptions(false);
      }
    });
    lookingForInput.addEventListener('blur', function () {
      if (!lookingForInput.value && lookingForSubmit) {
        lookingForSubmit.classList.remove(lookingForSubmitActiveClass);
        showOptions(false);
      }
    });
  }
});
