window.TcbDropdown = class TcbDropdown {
  wrapper;
  dropdownLabel;
  dropdownList;
  onSelectItem = () => {};
  backdrop;
  placeholder;
  viewValue;
  constructor(element, onSelect) {
    this.wrapper = element;
    this.onSelectItem = onSelect ? onSelect : () => {};
    const wrap = $(this.wrapper);
    this.dropdownLabel = wrap.find('.dropdown-inputbase')[0];
    this.dropdownList = wrap.find('.list-dropdown')[0];
    const selectedText = $(this.dropdownList).find('.dropdown-item').filter('[data-selected]').find('span').text()
    this.backdrop = $('<div class="dropdown-backdrop" hidden></div>')[0];
    this.placeholder = $(`<span class="dropdown-placeholder">${this.wrapper.dataset.placeholder || ''}</span>`)[0];
    this.viewValue = $(`<span class="dropdown-viewValue">${this.wrapper.dataset.selected || selectedText}</span>`)[0];
    $(this.wrapper).append(this.backdrop);
    if (!selectedText) $(this.dropdownLabel).prepend(this.placeholder);
    $(this.dropdownLabel).prepend(this.viewValue);

    if (this.wrapper.dataset.selected) {
      this.placeholder.classList.add('hidden');
    } else {
      this.viewValue.classList.add('hidden');
    }

    this.listenEvents(true);

    this.wrapper.setValue = this.setValue.bind(this);
  }

  listenEvents(isFirstInit ) {
    if (isFirstInit) {
      this.dropdownLabel.addEventListener('click', this.showDropdown.bind(this));
      this.backdrop.addEventListener('click', this.handleClickOutSideToClose.bind(this));
    }

    $(this.dropdownList)
      .find('.dropdown-item')
      .each((index, item) => {
        $(item).on('click', (event) => this.handleItemClick(event));
      });
  }

  handleItemClick(event) {
    this.onSelectItem(event);
    const value = event.currentTarget.dataset.value;
    const text = event.currentTarget.innerText;
    this.setValue(text);
    this.wrapper.dispatchEvent(new CustomEvent('select', { detail: value }));
    this.closeDropdown();
  }

  setValue(value) {
    $(this.viewValue).text(value);
    const input = $(this.wrapper).find('input');
    input.val(value);
    input[0].dispatchEvent(new Event('input'));
    if (value) {
      this.placeholder.classList.add('hidden');
      this.viewValue.classList.remove('hidden');
    } else {
      this.placeholder.classList.remove('hidden');
      this.viewValue.classList.add('hidden');
    }
  }

  clearValue() {
    $(this.viewValue).text('');
    const input = $(this.wrapper).find('input');
    input.val('');
    this.placeholder.classList.remove('hidden');
    this.viewValue.classList.add('hidden');

  }

  handleClickOutSideToClose() {
    this.closeDropdown();
  }

  showDropdown(event) {
    if (event.which === 1) {
      let lengthToBottom = window.innerHeight - this.wrapper.getBoundingClientRect().bottom;
      // this.handleClickOutSideToClose();
      this.dropdownList.removeAttribute('hidden');
      if(lengthToBottom < (this.dropdownList.getBoundingClientRect().height - 30 )) {
        let heightOfDropDown = lengthToBottom - this.dropdownList.getBoundingClientRect().height;
        this.dropdownList.style.top = heightOfDropDown + 40 + 'px';
      } else {
        this.dropdownList.style.top = 0;
      }
      this.backdrop.removeAttribute('hidden');
      $('body').addClass('dropdown-overflow-hidden');
    }
  }

  closeDropdown() {
    this.dropdownList.setAttribute('hidden', true);
    this.backdrop.setAttribute('hidden', true);
    $('body').removeClass('dropdown-overflow-hidden');
  }

  cleanup() {
    // Remove all event listeners
    // Destroy everything to cleanup
    this.dropdownLabel.removeEventListener('mousedown', this.showDropdown.bind(this));
  }
  // receive array with value and text
  setData(data) {
    this.dropdownList.textContent = '';
    data.forEach(item => {
      const dropdownItem = document.createElement('li');
      dropdownItem.textContent = item.text;
      dropdownItem.setAttribute('data-value', item.value);
      dropdownItem.classList.add('dropdown-item');
      this.dropdownList.appendChild(dropdownItem);
    })
    this.listenEvents(false);
  }
};
