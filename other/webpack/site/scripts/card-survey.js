function loadSurveyCard(surveyPanel){
  const Q1 = {
    id: 1,
    questionCountText: surveyPanel.q1QuestionCountText,
    title: surveyPanel.question1,
    options: [
      {
        id: 1,
        content: surveyPanel.q1AnswerOption1,
      },
      {
        id: 2,
        content: surveyPanel.q1AnswerOption2,
      },
      {
        id: 3,
        content: surveyPanel.q1AnswerOption3,
      },
      {
        id: 4,
        content: surveyPanel.q1AnswerOption4,
      },
    ],
    progressBarText: surveyPanel.q1ProgressBarDescriptionText
  };
  
  const Q2 = {
    id: 2,
    questionCountText: surveyPanel.q2QuestionCountText,
    title: surveyPanel.question2,
    options: [
      {
        id: 5,
        content: surveyPanel.q2AnswerOption1,
      },
      {
        id: 6,
        content: surveyPanel.q2AnswerOption2,
      },
      {
        id: 7,
        content: surveyPanel.q2AnswerOption3,
      },
      {
        id: 8,
        content: surveyPanel.q2AnswerOption4,
      },
      {
        id: 9,
        content: surveyPanel.q2AnswerOption5,
      },
    ],
    progressBarText: surveyPanel.q2ProgressBarDescriptionText
  };
  
  const Q3 = {
    id: 3,
    questionCountText: surveyPanel.q3QuestionCountText,
    title: surveyPanel.question3,
    options: [
      {
        id: 10,
        content: surveyPanel.q3AnswerOption1,
      },
      {
        id: 11,
        content: surveyPanel.q3AnswerOption2,
      },
      {
        id: 12,
        content: surveyPanel.q3AnswerOption3,
      },
    ],
    progressBarText: surveyPanel.q3ProgressBarDescriptionText
  };
  
  const Cards = [
    {
      id: 1,
      imageUrl: surveyPanel.everydayCardImage,
      webImageUrl: surveyPanel.webEverydayCardImage,
      mobileImageUrl: surveyPanel.mobileEverydayCardImage,
      imageAlt: surveyPanel.everydayCardImageAltText,
      url: surveyPanel.everydayCardUrl,
      webInteractionType: surveyPanel.everydayCardWebInteractionType
    },
    {
      id: 2,
      imageUrl: surveyPanel.styleCardImage,
      webImageUrl: surveyPanel.webStyleCardImage,
      mobileImageUrl: surveyPanel.mobileStyleCardImage,
      imageAlt: surveyPanel.styleCardImageAltText,
      url: surveyPanel.styleCardUrl,
      webInteractionType: surveyPanel.styleCardWebInteractionType
    },
    {
      id: 3,
      imageUrl: surveyPanel.sparkCardImage,
      webImageUrl: surveyPanel.webSparkCardImage,
      mobileImageUrl: surveyPanel.mobileSparkCardImage,
      imageAlt: surveyPanel.sparkCardImageAltText,
      url: surveyPanel.sparkCardUrl,
      webInteractionType: surveyPanel.sparkCardWebInteractionType
    },
    {
      id: 4,
      imageUrl: surveyPanel.signatureCardImage,
      webImageUrl: surveyPanel.webSignatureCardImage,
      mobileImageUrl: surveyPanel.mobileSignatureCardImage,
      imageAlt: surveyPanel.signatureCardImageAltText,
      url: surveyPanel.signatureCardUrl,
      webInteractionType: surveyPanel.signatureCardWebInteractionType
    },
    {
      id: 5,
      imageUrl: surveyPanel.vietnamAirlineCardImage,
      webImageUrl: surveyPanel.webVietnamAirlineCardImage,
      mobileImageUrl: surveyPanel.mobileVietnamAirlineCardImage,
      imageAlt: surveyPanel.vietnamAirlineCardImageAltText,
      url: surveyPanel.vietnamAirlineUrl,
      webInteractionType: surveyPanel.vietnamAirlineCardWebInteractionType
    },
  ];
  
  const TCB_EVR_ID = 1;
  const TCB_STY_ID = 2;
  const TCB_SPK_ID = 3;
  const TCB_SIG_ID = 4;
  const TCB_VNA_ID = 5;
  
  let counter = 1;
  let a1 = 0;
  let a2 = 0;
  let a3 = 0;
  let selectedValue = -1;
  
  const iconPlusPathHTML = `<img class="add-icon" src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg" />`;
  const iconArrowRightPathHTML = `<img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg">`;
  
  const inputOptionTemplate = (name, value) => `
  <div class="survey-panel__question-radio-wrapper">
  <div class="survey-panel__question-input-radio">
    <input
      name="question-input-radio"
      type="radio"
      value="${value}"
    />
    <div>${name}</div>
  </div>
  </div>
  `;
  
  const renderQuestion = (questionObj) => {
    $('.survey-panel__question-label').text(questionObj.questionCountText);
    $('.question-radio-container').html('');
    $('.survey-panel__question-title').text(questionObj.title);
    $(questionObj.options).each(function (index, element) {
      $('.question-radio-container').append(inputOptionTemplate(element.content, element.id));
    });
  };
  
  const disableBtnNextQuestion = () => {
    $('.survey-panel__question-button').addClass('btn--disabled');
    $('.button-icon path').addClass('btn__icon--disabled');
  };
  
  const enableBtnNextQuestion = () => {
    $('.survey-panel__question-button').removeClass('btn--disabled');
    $('.button-icon path').removeClass('btn__icon--disabled');
  };
  
  /**
   * Rule 1: 1-3 & 2-5 & 3 any → Everyday card (Gold)
   * Rule 2: 1-3 & 2-6 & 3 any → Style card (Platinum)
   * Rule 3: 1-3 & 2-7 & 3 any → Spark card
   * Rule 4: 1-3 & 2-8 & 3 any → Spark card
   * Rule 5: 1-3 & 2-9 & 3 any → Everyday card (Gold)
   * Rule 6: 1-2 & 2 any & 3-10 → Style card (Platinum)
   * Rule 7: 1-2 & 2 any & 3-11 → Style card (Platinum)
   * Rule 8: 1-2 & 2 any & 3-12 → Signature card
   * Rule 9: 1-1 & 2 any & 3 any → Spark card
   * Rule 10: 1-4 & 2 any & 3 any → Viet Nam Airline card
   *
   * @param {Number} a [1, 2, 3, 4]
   * @param {Number} b [5, 6, 7, 8, 9]
   * @param {Number} c [10, 11, 12]
   * @returns Card ID
   */
  const checkingResultMetrix = (a, b, c) => {
    switch (a) {
      case 1:
        // * Rule 9: 1-1 & 2 any & 3 any → Spark card
        return TCB_SPK_ID;
      case 2:
        // * Rule 6: 1-2 & 2 any & 3-10 → Style card (Platinum)
        // * Rule 7: 1-2 & 2 any & 3-11 → Style card (Platinum)
        // * Rule 8: 1-2 & 2 any & 3-12 → Signature card
        if (c === 10 || c === 11) {
          return TCB_STY_ID;
        }
        if (c === 12) {
          return TCB_SIG_ID;
        }
      case 3:
        // * Rule 1: 1-3 & 2-5 & 3 any → Everyday card (Gold)
        // * Rule 2: 1-3 & 2-6 & 3 any → Style card (Platinum)
        // * Rule 3: 1-3 & 2-7 & 3 any → Spark card
        // * Rule 4: 1-3 & 2-8 & 3 any → Spark card
        // * Rule 5: 1-3 & 2-9 & 3 any → Everyday card (Gold)
        if (b === 5 || b === 9) {
          return TCB_EVR_ID;
        }
        if (b === 7 || b === 8) {
          return TCB_SPK_ID;
        }
        if (b === 6) {
          return TCB_STY_ID;
        }
      case 4:
        // * Rule 10: 1-4 & 2 any & 3 any → Viet Nam Airline card
        return TCB_VNA_ID;
      default:
        break;
    }
    return 0;
  };
  
  const setPercentComplete = (percentComplete, progressBarText) => {
    let transform = 100 - percentComplete;
    $('.linear-progress-bar').css('transform', `translateX(-${transform}%)`);
    $('.survey-panel__process-label').text(progressBarText);
  };
  
  const displayCardImageSuggestion = (card) => {
    $('.survey-panel__process-image, .survey-panel__progress-bar, .survey-panel__process-label').hide();

    let viewPortWidth = window.innerWidth;
    let imgSrc = card.webImageUrl;
    if (viewPortWidth <= 576) {
      imgSrc = card.mobileImageUrl;
    } else {
      imgSrc = card.webImageUrl;
    }
    $('.survey-panel__card-image img').attr({
      src: imgSrc,
      websrc: card.webImageUrl,
      mobilesrc: card.mobileImageUrl,
      alt: card.imageAlt,
    });
    $('.btn-learn-more .survey-panel__question-button').attr("href", card.url);
    $('.survey-panel__card-image, .btn-learn-more').show();
  };

  const setDataTrackingValue = (card, learnMoreCta) => {
    $('.survey-panel__question-button.btn-learn-more').attr("data-tracking-click-event", "linkClick");
    $('.survey-panel__question-button.btn-learn-more').attr("data-tracking-click-info-value", "{'linkClick' : '" + learnMoreCta + "'}");
    $('.survey-panel__question-button.btn-learn-more').attr(
      "data-tracking-web-interaction-value", "{'webInteractions': {'name': 'Survey Panel','type': '" + card.webInteractionType + "'}}"
    );
  };
  
  const displayProcessImage = () => {
    $('.survey-panel__process-image, .survey-panel__progress-bar, .survey-panel__process-label').show();
  
    $('.survey-panel__card-image, .btn-learn-more').hide();
  };
  
  const setTryAgainButton = (flag, message) => {
    if (flag) {
      $('.btn-next-question .button-icon').html(iconPlusPathHTML);
      $('.btn-next-question .button-text').text(message);
    } else {
      $('.btn-next-question .button-icon').html(iconArrowRightPathHTML);
      $('.btn-next-question .button-text').text(message);
    }
  };
  
  const handleButtonNextQuestion = () => {
    let selectedValue = 0;
    let radioChecked = $("input[name='question-input-radio']:checked");
    if (radioChecked.length > 0) {
      selectedValue = parseInt(radioChecked.val());
    }
  
    if (!selectedValue) {
      return;
    }
  
    switch (counter) {
      case 1:
        // answered for question 1
        a1 = selectedValue;
        counter++;
        // render next question
        renderQuestion(Q2);
        // disable button next
        disableBtnNextQuestion();
        setPercentComplete(100 / 3, Q2.progressBarText);
        break;
      case 2:
        // answered for question 2
        a2 = selectedValue;
        counter++;
        // render next question
        renderQuestion(Q3);
        // disable button next
        disableBtnNextQuestion();
        setPercentComplete(((100 / 3) * 2), Q3.progressBarText);
        break;
      case 3:
        // answer for question 3
        a3 = selectedValue;
        counter++;
        const idCard = checkingResultMetrix(a1, a2, a3);
        const suggestedCard = Cards.find((card) => card.id == idCard);
        setPercentComplete(100, "");
        displayCardImageSuggestion(suggestedCard);
        setDataTrackingValue(suggestedCard, surveyPanel.learnMoreCta);
        setTryAgainButton(true, surveyPanel.tryAgainCta);
        break;
      case 4:
        // reset all values
        a1 = 0;
        a2 = 0;
        a3 = 0;
        counter = 1;
        // render the first question
        renderQuestion(Q1);
        disableBtnNextQuestion();
        setPercentComplete(0, Q1.progressBarText);
        displayProcessImage();
        setTryAgainButton(false, surveyPanel.nextCta);
        break;
      default:
        break;
    }
  };
  
  $('.btn-next-question').click(handleButtonNextQuestion);
  
  $('.survey-panel__question').on('change', 'input[type=radio][name=question-input-radio]', function (e) {
    if (this.value && this.value > 0) {
      enableBtnNextQuestion();
    }
  });
  
  $(function () {
    // init style for button next
    disableBtnNextQuestion();
    // check the checked value radio button
    renderQuestion(Q1);
  });
}


$(document).ready(function () {
  try{
    let jsonString = $(".survey-panel-data-container").attr("surveyPanel");
    if(!jsonString){
      return
    }
    let surveyPanel = JSON.parse(jsonString);
    loadSurveyCard(surveyPanel);
  }
  catch(err){
    return
  }
});

$(window).resize(function () {
  let viewPortWidth = window.innerWidth;
  if (viewPortWidth <= 576) {
    $('.survey-panel__card-image img').each(function () {
      $(this).attr('src', $(this).attr('mobilesrc'));
    });
  } else {
    $('.survey-panel__card-image img').each(function () {
      $(this).attr('src', $(this).attr('websrc'));
    });
  }    
});