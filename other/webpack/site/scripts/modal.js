$(document).on('click', 'a[href^="#"]', function (e) {

    let target = $(this).attr('href');

    const targetElement = document.getElementById(target.slice(1))
    const modalTarget = targetElement && targetElement.matches('.popup') ? targetElement : null;
    if (modalTarget) {
        e.preventDefault();
        modalTarget.style.display = 'flex';

        window.addEventListener('click', function (event) {
            if (event.target == modalTarget) {
                modalTarget.style.display = 'none';
            }
        });

        $(document).on('click', '.popup .close', function (e) {
            modalTarget.style.display = 'none';
        })
    }
});

//changed by adobe
$(".close.popup__close-icon").on("click", function () { // to close modal popup of Form & others
    let modal = $(this).parents('.modal .popup');
    if (modal.css('display') == 'flex') {
        modal.hide();
    }
});
