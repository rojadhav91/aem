function loadLinkText(){
    // Set unique ids for link texts and view more buttons
    $(".link-text.link-text-component").each(function(index) {
        $(this).attr("id", index);
    });
    $(".link-text-btn").each(function(index, el) {
        $(this).attr("id", index);
    });

    // Show/hide view more button base on number of link texts
    let totalLinkTextComps = $(".link-text.link-text-component").length;
    for(let index = 0; index < totalLinkTextComps; index++){
        let cardsNumber = $(".link-text.link-text-component#" + index + " .ticket-item").length;
        if(window.innerWidth > 1024){
            if(cardsNumber <= 12){
                $("#" + index + ".link-text-btn").toggle();
            }
        }
        else{
            if(cardsNumber <= 3){
                $("#" + index + ".link-text-btn").toggle();
            }
        }
    }

    // Show hidden items on click
    $(".link-text-btn").click(function(){
        let linkTextId = $(this).attr("id");
        $(".link-text.link-text-component#" + linkTextId + " .ticket-item").attr("hiddenInDesktop", "");
        $(".link-text.link-text-component#" + linkTextId + " .ticket-item").attr("hiddenInMobile", "");
        $(this).toggle();
    })
}

$(document).ready(function () {
    loadLinkText();
    window.addEventListener("message", (e) => {
      if (e.data === "reloadLinkText") {
        loadLinkText();
      }
    }, false);
});