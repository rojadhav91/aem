(function ($) {
    window.Position = class Position {
      x;
      y;

      constructor(position = {}) {
        this.x = position.x || 0;
        this.y = position.y || 0;
      }
    };

    window.MouseUtil = class MouseUtil {
      start = new Position();
      last = new Position();
      current = new Position();

      setStart(position) {
        this.start = position;
      }

      updateLast(position) {
        this.last = { ...this.current };
      }

      update(position) {
        this.current = position;
      }

      getDistance() {
        return this.current.x - this.start.x;
      }
    };

    window.TcbInputRange = class TcbInputRange {
      wrapper;
      thumb;
      bar;
      labelWrapper;
      labelStart;
      labelEnd;

      min;
      max;
      unit;
      useLabel;
      useValuePopover;
      rounded;

      mouse = new MouseUtil();
      startLeft = 0;
      left = 0;
      value = 0;
      constructor(element) {
        this.wrapper = element;
        const data = element.dataset;
        this.min = Number(data.min) || 0;
        this.max = Number(data.max) || 100;
        this.unit = data.unit || '';
        this.useLabel = data.useLabel && data.useLabel === 'true' ? true : false;
        this.useValuePopover = data.useValuePopover && data.useValuePopover === 'true' ? true : false;
        this.rounded = data.rounded && data.rounded === 'false' ? false : true;

        this.value = Number(data.value) || this.min;

        this.init();
      }

      init() {
        const barWrapper = $('<div class="tcb-input-range_bar-wrapper"></div>');
        this.bar = $('<div class="tcb-input-range_bar"></div>');
        this.thumb = $('<div class="tcb-input-range_thumb"></div>');
        $(this.bar).append(this.thumb);
        $(barWrapper).append(this.bar);
        $(this.wrapper).append(barWrapper);

        if (this.useLabel) {
          this.labelWrapper = $('<div class="tcb-input-range_labels"></div>');
          this.labelStart = $(`<div class="tcb-input-range_label-start">${this.min + this.unit}</div>`);
          this.labelEnd = $(`<div class="tcb-input-range_label-end">${this.max + this.unit}</div>`);
          this.labelWrapper.append(this.labelStart).append(this.labelEnd);
          $(this.wrapper).append(this.labelWrapper);
        }

        if (this.useValuePopover) {
          this.inlineValue = $(`<div class="tcb-input-range_inline-value">${this.value + this.unit}</div>`);
          $(this.thumb).append(this.inlineValue);
        }

        if (this.value) {
          this.setValue(this.value);
        }

        $(this.wrapper).on('setValue', this.handleSetValue.bind(this));
        this.thumb.on('mousedown touchstart', this.handleMouseDown.bind(this));
        $(window).on('resize', this.recalculateView.bind(this));
        $(this.wrapper).on('click', (event)=> {
          var leftClick = event.clientX - this.wrapper.getBoundingClientRect().left;
          const barWidth = this.wrapper.clientWidth;
          const valueDistance = this.max - this.min;
          const stepDistance = (barWidth - 24) / valueDistance;
          const newWidth = this.updateWidth(leftClick);
          this.value = this.rounded
          ? Math.round((newWidth - 12) / stepDistance) + this.min
          : (newWidth - 12) / stepDistance + this.min;
          this.mouse.setStart(this.getPosition(event));
          this.bar.css({ width: newWidth + 'px' });
          this.startLeft = newWidth;
          if (this.useValuePopover) {
            this.inlineValue.text(this.value + this.unit);
          }
          this.wrapper.dispatchEvent(new CustomEvent('input', { detail: this.value }));
        });
      }

      handleMouseDown(event) {
        event.preventDefault();
        $(window)
          .on('mousemove touchmove', this.handleMouseMove.bind(this))
          .on('mouseup touchend', this.handleMouseUp.bind(this))
          .on('mousecancel touchcancel', this.handleMouseUp.bind(this));
        this.mouse.setStart(this.getPosition(event));
        this.startLeft = this.left;
      }

      handleMouseMove(event) {
        event.preventDefault();
        this.mouse.update(this.getPosition(event));
        const barWidth = this.wrapper.clientWidth;
        const leftClick = event.clientX ? event.clientX - this.wrapper.getBoundingClientRect().left : event.touches[0].pageX - this.wrapper.getBoundingClientRect().left;
        this.left = leftClick < 0 ? 0 : leftClick > this.wrapper.offsetWidth ? this.wrapper.offsetWidth : leftClick;
        this.updateView();
        const valueDistance = this.max - this.min;
        const stepDistance = (barWidth - 24) / valueDistance;
        const newWidth = this.updateWidth();
        this.value = this.rounded
          ? Math.round((newWidth - 12) / stepDistance) + this.min
          : (newWidth - 12) / stepDistance + this.min;

        if (this.useValuePopover) {
          this.inlineValue.text(this.value + this.unit);
        }

        this.wrapper.dispatchEvent(new CustomEvent('input', { detail: this.value }));
      }

      handleSetValue(event) {
        this.setValue(event.detail);
      }

      setValue(value) {
        this.value = value;
        this.recalculateView();
        if (this.useValuePopover) {
          this.inlineValue.text(this.value + this.unit);
        }
        this.wrapper.dispatchEvent(new CustomEvent('input', { detail: this.value }));
      }

      recalculateView() {
        const barWidth = this.wrapper.clientWidth;
        const valueDistance = this.max - this.min;
        // To centralize the thumb
        // Without 12px of padding, the thumb will displays outside of the slider
        const stepDistance = this.value / this.max > 0.9 ? (barWidth - 12) / valueDistance : barWidth / valueDistance;
        const stepsCount = this.value - this.min;
        this.left = stepsCount * stepDistance;
        this.updateView();
      }

      handleMouseUp(event) {
        event.preventDefault();
        this.mouse.updateLast();
        $(window).off('mousemove mouseup mousemove mousecancel touchmove touchend touchcancel');
      }

      updateWidth(width) {
        const numSegments = this.max - this.min; 
        const segmentWidth = (this.wrapper.offsetWidth - 24 ) / numSegments;
        const currentPosition = width || this.left;
        const nearestSegment = Math.round(currentPosition / segmentWidth);
        const result = (nearestSegment * segmentWidth) + 12;
        const maxAllowedResult = this.wrapper.offsetWidth - 12;
        return Math.min(result, maxAllowedResult);
      }

      updateView() {
        this.bar.css({ width: this.updateWidth() + 'px' });
      }

      getPosition(event) {
        const position = new Position();
        if (event.type.includes('mouse') || event.type.includes('click')) {
          position.x = event.pageX;
          position.y = event.pageY;
        } else {
          position.x = event.touches[0].pageX;
          position.y = event.touches[0].pageY;
        }
        return position;
      }
    };

    $('.tcb-input-range,.tcb-input--save-calc').each((index, item) => {
      new TcbInputRange(item);
    });
}) (jQuery);
