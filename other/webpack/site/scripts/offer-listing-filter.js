$(function () {
  const offerListingComponent = $(".offer-listing-filter__body");
  if (offerListingComponent.length === 0) {
    return;
  }

  let promotionData;
  const url = new URL(window.location);
  let totalPage;
  let totalItem;
  let dataSearchCount = "";
  let dataViewMore = "";
  let dataURL = "";
  let dataDay = "";
  let dataExpiryLabel = "";
  let dataComponentName = "";
  let dataLanguageLabel = "";
  let dataEmptyPromotion = "";
  let currentPage = 1;
  const MOBILE_SIZE = 768;
  const DEFAULT_SORT_PARAMS = 'most popular';
  const paramsName = {
    sort: 'sort',
    products: 'products',
    types: 'types',
    partner: 'partner',
    limit: 'limit',
    offset: 'offset'
  };
  const paramsValue = {
    limit: 12,
    offset: 0,
    sort: DEFAULT_SORT_PARAMS,
    products: '',
    types: ''
  };

  const dataElement = offerListingComponent.find(".offer-listing-promotions");

  const sortDesktopElement = $(".offer-listing-promotions__wrapper input.input__radio");
  const productDesktopElement = $(".offer-listing-promotions__wrapper div.offer-filter__checkbox.product-checkbox");
  const categoryDesktopElement = $(".offer-listing-promotions__wrapper div.offer-filter__checkbox.category-checkbox");
  const merchantDesktopElement = $(".offer-listing-promotions__wrapper div.offer-filter__dropdown.merchant-desktop-element");

  const sortMobileElement = $(".offer-listing-promotions__wrapper div.offer-filter__dropdown.sort-mobile-element");
  const productMobileElement = $(".offer-listing-promotions__wrapper div.offer-filter__checkbox.product-checkbox-mobile");
  const categoryMobileElement = $(".offer-listing-promotions__wrapper div.offer-filter__checkbox.category-checkbox-mobile");
  const merchantMobileElement = $(".offer-listing-promotions__wrapper div.offer-filter__dropdown.merchant-mobile-element");

  initDataType();
  initSearchParams();
  displayElementByScreen();
  handlePromotionFilterOnMobile();
  getPromotions();

  $(window).resize(displayElementByScreen);

  function handlePromotionFilterOnMobile() {
    if (dataElement.length > 0) {
      let btnOpenFilter = dataElement.find('.btn-open-filter');
      let buttonFilter = dataElement.find('.tcb-apply-filter-button');
      btnOpenFilter.on('click', function () {
        dataElement.addClass('open');
        $('body').addClass('modal-showing');
      });
      buttonFilter.on('click', function () {
        dataElement.removeClass('open');
        $('body').removeClass('modal-showing');
        $(".offer-listing-filter__button").show();
      });
    }
  }

  function displayElementByScreen() {
    let width = $(window).width();
    if (width < MOBILE_SIZE) {
      $(".offer-listing-promotions__wrapper .offer-listing-filter__button").show();
      $(".offer-listing-promotions__wrapper .offer-filter__container").hide();
      $(".offer-listing-promotions__wrapper .offer-cards__container .offer-cards__wrapper .search-area").hide();
    } else {
      $(".offer-listing-promotions__wrapper .offer-listing-filter__button").hide();
      $(".offer-listing-promotions__wrapper .offer-filter__container").show();
      $(".offer-listing-promotions__wrapper .offer-cards__container .offer-cards__wrapper .search-area").show();
    }
  }

  function initDataType() {
    dataURL = dataElement?.attr("data-url");
    dataSearchCount = dataElement?.attr("data-search-count");
    dataViewMore = dataElement?.attr("data-view-more");
    dataDay = dataElement?.attr("data-day-label");
    dataExpiryLabel = dataElement?.attr("data-expiry-label");
    dataEmptyPromotion = dataElement?.attr("data-empty-promotion-label");
    dataLanguageLabel = dataElement?.attr("data-language-label");
    dataComponentName = dataElement?.attr("data-component-name");
  }

  function initSearchParams() {
    if (url.searchParams.size === 0) {
      paramsValue.sort = DEFAULT_SORT_PARAMS;
    } else {
      if ($(window).width() > MOBILE_SIZE) {
        if (url.searchParams.has(paramsName.sort)) {
          const sort = url.searchParams.get(paramsName.sort);
          sortDesktopElement.each(function() {
            let elementValue = $(this).attr('data-sort');
            if (elementValue.toLowerCase() === DEFAULT_SORT_PARAMS) {
              elementValue = 'most popular';
            }
            if (sort.toLowerCase() === elementValue.toLowerCase()) {
              $(this).attr('checked', 'checked');
            }
          });
        } else {
          paramsValue.sort = DEFAULT_SORT_PARAMS;
        }

        if (url.searchParams.has(paramsName.products)) {
          const products = url.searchParams.get(paramsName.products);
          const inputElement = productDesktopElement?.find($("input.input__checkbox"));
          inputElement.each(function() {
            const elementValue = $(this).attr('data-product');
            if (products.includes(elementValue.toLowerCase())) {
              $(this).prop('checked', true);
            }
          });
        }

        if (url.searchParams.has(paramsName.types)) {
          const types = url.searchParams.get(paramsName.types);
          const inputElement = categoryDesktopElement?.find($("input.input__checkbox"));
          inputElement.each(function() {
            const elementValue = $(this).attr('data-category');
            if (types.includes(elementValue.toLowerCase())) {
              $(this).prop('checked', true);
            }
          });
        }

        if (url.searchParams.has(paramsName.partner)) {
          const partner = url.searchParams.get(paramsName.partner);
          merchantDesktopElement?.find($("span.display__text"))?.text(capitalizeFirstLetter(partner));
        }
      } else {
        if (url.searchParams.has(paramsName.sort)) {
          const sort = url.searchParams.get(paramsName.sort);
          setTimeout(() => {
            sortMobileElement?.find($("span.display__text"))?.text(capitalizeFirstLetter(sort));
          }, 500);
        } else {
          paramsValue.sort = DEFAULT_SORT_PARAMS;
        }

        if (url.searchParams.has(paramsName.products)) {
          const products = url.searchParams.get(paramsName.products);
          const inputElement = productMobileElement?.find($("input.input__checkbox"));
          inputElement.each(function() {
            const elementValue = $(this).attr('data-product');
            if (products.includes(elementValue.toLowerCase())) {
              $(this).prop('checked', true);
            }
          });
        }

        if (url.searchParams.has(paramsName.types)) {
          const types = url.searchParams.get(paramsName.types);
          const inputElement = categoryMobileElement?.find($("input.input__checkbox"));
          inputElement.each(function() {
            const elementValue = $(this).attr('data-category');
            if (types.includes(elementValue.toLowerCase())) {
              $(this).prop('checked', true);
            }
          });
        }

        if (url.searchParams.has(paramsName.partner)) {
          const partner = url.searchParams.get(paramsName.partner);
          merchantMobileElement?.find($("span.display__text"))?.text(capitalizeFirstLetter(partner));
        }
      }
    }
  }

  function capitalizeFirstLetter(text) {
    return `${text.charAt(0).toUpperCase()}${text.slice(1)}`;
  }

  function getEndpoint() {
    let endpoint = `${dataURL}.offerlisting.json?${paramsName.limit}=${paramsValue.limit}&${paramsName.offset}=${paramsValue.offset}`;

    if (url.searchParams.has(paramsName.sort)) {
      endpoint += `&${paramsName.sort}=${url.searchParams.get(paramsName.sort).toString()}`;
    } else if (paramsValue.sort) {
      endpoint += `&${paramsName.sort}=${paramsValue.sort.split(' ').join('+')}`;
    }

    if (url.searchParams.has(paramsName.products)) {
      endpoint += `&${paramsName.products}=${url.searchParams.get(paramsName.products).toString()}`;
    } else if (paramsValue.products) {
      endpoint += `&${paramsName.products}=${paramsValue.products}`;
    }

    if (url.searchParams.has(paramsName.types)) {
      endpoint += `&${paramsName.types}=${url.searchParams.get(paramsName.types).toString()}`;
    } else if (paramsValue.types) {
      endpoint += `&${paramsName.types}=${paramsValue.types}`;
    }

    if (url.searchParams.has(paramsName.partner)) {
      endpoint += `&${paramsName.partner}=${url.searchParams.get(paramsName.partner).toString()}`;
    }

    return endpoint;
  }

  function getPromotions() {
    const api = getEndpoint();
    fetchData(api).then((response) => {
      promotionData = response;
      if (promotionData.total && Number(promotionData.total) > 0) {
        totalPage = Math.ceil(promotionData.total / paramsValue.limit);
        totalItem = promotionData.total;
        const sort = url.searchParams.get(paramsName.sort);
        let promotions = [];

        if (!sort && promotionData.results.length > 6) {
          let favoriteItem = promotionData.results.splice(0,6);
          const nonFavoriteItemIndex = promotionData.results.findIndex(item => /false/.test(item?.favourite));
          const nonFavoriteItem = promotionData.results.splice(nonFavoriteItemIndex, 1)[0];
          promotionData.results.unshift(nonFavoriteItem);
          favoriteItem = favoriteItem.map(item => ({...item, isFavorite: true}));
          promotions = [...favoriteItem, ...promotionData.results];
        } else {
          promotions = promotionData.results;
        }
        renderPromotionCard(promotions);
      } else {
        renderEmptyPromotionCard();
      }
    });
  }

  function fetchData(url) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: url,
        method: "get",
        contentType: "application/json",
        success: function (response) {
          resolve(response);
        },
        error: function (xhr, status, error) {
          reject(error);
        },
      });
    });
  }

  function renderPromotionCard(response) {
    const cardPromotionList = $(".card-promotion__list");
    cardPromotionList.empty();
    response.forEach((item) => {
      const element = renderPromotionCardItem(item);
      cardPromotionList.append(element);
    });
    renderExpiryDate();
    renderPromotionCount();
    renderPagination();
  }

  function renderPromotionCardItem(item) {
    return `
            <div class="card-promotion__item-wrapper">
                <div class="card-promotion__item">
                    <div class="card-offer__image">
                        <span class="card-offer__image--wrapper">
                            <span class="image-card-offer">
                                <img alt="" aria-hidden="true" src="data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27620%27%20height=%27348%27/%3e">    
                            </span>
                            <img class="card-offer__image--thumbnail" src="${item?.thumbnail || ""}" alt="card-image"/>
                        </span>
                    </div>
                    <div class="card-offer__content">
                        <span class="card-content__label">
                            ${item?.products?.join() || ""}
                        </span>
                        <span class="card-content__title">
                            ${item?.partner?.join() || ""}
                        </span>  
                        <span class="card-content__description">
                            ${item?.description || ""}
                        </span>
                        <div class="expiry-date-section" data-expiry="${item?.expiryDate}"></div>
                        <div class="card-content__link">
                            <a href="${item?.url || ""}"
                                target="${item?.openInNewTab === "true" ? "_blank" : "_self"}" rel="noopener noreferrer"
                                class="card-content__link--button" data-tracking-click-event="viewOfferCTA"
                                data-tracking-click-info-value="{'viewOfferTitle':'${item?.partner?.join()}'}"
                                data-tracking-web-interaction-value="{'webInteractions': {'name': '${dataComponentName}','type': '${item?.interactionType}'}}">
                                    <span class="button--text">${dataViewMore || ""}</span>
                                    <div class="button--icon">
                                        <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow.svg"
                                            alt="red-arrow"/>
                                    </div>
                            </a>
                            
                        </div>
                        <div class="card-content__favorite-promo ${(item?.favourite === "true" && item?.isFavorite) ? "display-unset" : "display-none"}">
                            <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/fav-icon.png"
                                 alt="fav-icon"
                            />
                        </div>
                    </div>
                </div>
            </div>
        `;
  }

  function renderEmptyPromotionCard() {
    const promotionTotalCount = $(".promotion-total-count");
    const countElement = `Showing <b>0</b> promotions`;
    promotionTotalCount.empty();
    promotionTotalCount.append(countElement);

    const cardPromotionList = $(".card-promotion__list");
    const emptyElement = `
            <div class="card-promotion__item-wrapper">
                <span>${dataEmptyPromotion}</span>
            </div>
        `;
    cardPromotionList.empty();
    cardPromotionList.append(emptyElement);

    const pagination = $(".pagination");
    pagination.empty();
  }

  function renderExpiryDate() {
    const expiryDateElement = $(".expiry-date-section");
    expiryDateElement.each(function () {
      const dataExpiryDate = $(this).attr('data-expiry'); // en is 'dd/MM/yyyy', vi is dd/MMM/yyyy
      let expiredDate;
      switch (dataLanguageLabel) {
        case 'en':
          expiredDate = new Date(Date.parse(`${dataExpiryDate} 17:00:00 GMT`));
          break;

        case 'vi':
          const dateParts = dataExpiryDate.split("/");
          expiredDate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0], 23,59,59);
          break;
      }

      const duration = expiredDate.getTime() - Date.now();
      const durationOf7Days = 604_800_000;
      const showExpiryCountdownBar = duration < durationOf7Days;

      if (showExpiryCountdownBar) {
        const intervalID = setInterval(() => {
          const remainingTime = expiredDate.getTime() - Date.now();
          const days = Math.floor(remainingTime / (24 * 60 * 60 * 1000));
          const daysms = remainingTime % (24 * 60 * 60 * 1000);
          const hours = Math.floor(daysms / (60 * 60 * 1000));
          const hoursms = remainingTime % (60 * 60 * 1000);
          const minutes = Math.floor(hoursms / (60 * 1000));
          const minutesms = remainingTime % (60 * 1000);
          const seconds = Math.floor(minutesms / 1000);
          const remainingData = {
            day: days,
            time: `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds }`,
          };
          const remainingText = `${remainingData.day} ${dataDay} ${remainingData.time}`;
          const progressBarWidth = Number(100 - (remainingTime / durationOf7Days) * 100).toFixed();
          const countdownElement = `
            <div class="card-content__expired-date-count-down">
                <div class="progress-bar">
                    <div class="progress-bar__inner" style="width: ${progressBarWidth}%"></div>
                </div>
                <div class="countdown-time">${remainingText}</div>
            </div>
          `;

          if (remainingTime <= 0) {
            clearInterval(intervalID);
          }

          $(this).empty();
          $(this).append(countdownElement);
        }, 1000);
      } else {
        const expiredDateElement = `
            <div class="card-content__expired-date">
                <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/time-icon.svg"
                    alt="time-icon"
                />
                <span class="card-content__expired-date--date-time">${dataExpiryLabel} ${dataExpiryDate || ''}</span>
            </div>
        `;

        $(this).empty();
        $(this).append(expiredDateElement);
      }
    });
  }

  function renderPromotionCount() {
    const promotionTotalCount = $(".promotion-total-count");
    promotionTotalCount.empty();
    const searchCountLabel = dataSearchCount.replace("%count", `<b>${totalItem}</b>`);
    promotionTotalCount.append(searchCountLabel);
  }

  function renderPagination() {
    if (totalPage < 1) {
      return;
    }

    const pagination = $(".pagination");
    const previous = `<label class="pag-container--previous">Previous</label>`;
    const next = `<label class="pag-container--next">Next</label>`;
    pagination.empty();

    const pagContainer = `<div class="pag-container"></div>`;
    pagination.append(pagContainer);

    const pagContainerElement = $(".pagination .pag-container");

    if (currentPage !== 1) {
      pagContainerElement.append(previous);
    }

    for (let i = 1; i <= totalPage; i++) {
      const element = `
                <a class="${i === currentPage ? "active" : ""}">${i}</a>
            `;
      pagContainerElement.append(element);
    }

    if (currentPage !== totalPage) {
      pagContainerElement.append(next);
    }

    const aTagElement = $(".pagination .pag-container a");
    const previousElement = $(".pagination .pag-container label.pag-container--previous");
    const nextElement = $(".pagination .pag-container label.pag-container--next");

    aTagElement.click(function () {
      if ($(this).length > 0) {
        currentPage = Number($(this).text());
        onPageChange(currentPage);
      }
    });

    previousElement.click(function () {
      if ($(this).length > 0) {
        if (currentPage && Number(currentPage) > 1) {
          currentPage -= 1;
          onPageChange(currentPage);
        }
      }
    });

    nextElement.click(function () {
      if ($(this).length > 0) {
        if (currentPage && Number(currentPage) < totalPage) {
          currentPage += 1;
          onPageChange(currentPage);
        }
      }
    });
  }

  function onPageChange(changedPage) {
    paramsValue.offset = (changedPage - 1) * paramsValue.limit;
    $('body').animate({scrollTop: 0}, 500);
    getPromotions();
  }

  function getSelectedCheckbox(element, paramName) {
    const selectedElement = element.find($("input.input__checkbox:checked"));
    if (selectedElement.length === 0) {
      removeURLParams(paramName, true);
      paramsValue[paramName] = '';
      return;
    }

    const selectedItem = selectedElement
      .map(function () {
        return $(this)?.val()?.toLowerCase()?.split('-')?.join(' ');
      })
      .get();

    if (selectedItem.length === element.children().length) {
      removeURLParams(paramName, true);
      paramsValue[paramName] = selectedItem.join().split('-').join('+').split(',').join('%2C');
    } else {
      setURLParams(paramName, selectedItem.join(), true);
      paramsValue[paramName] = '';
    }
  }

  function setURLParams(name, params, replaceState) {
    url.searchParams.set(name, params);
    url.searchParams.sort();
    if (replaceState) {
      window.history.replaceState({}, "", url);
    }
  }

  function removeURLParams(name, replaceState) {
    url.searchParams.delete(name);
    url.searchParams.sort();
    if (replaceState) {
      window.history.replaceState({}, "", url);
    }
  }

  sortDesktopElement.click(function () {
    if ($(this).length === 0) {
      return;
    }

    const selectedItem = $(this)?.attr('data-value');
    switch (selectedItem) {
      case 'most-popular':
        removeURLParams(paramsName.sort, true);
        paramsValue.sort = DEFAULT_SORT_PARAMS;
        break;

      default:
        setURLParams(paramsName.sort, selectedItem?.split('-')?.join(' '), true);
        paramsValue.sort = '';
        break;
    }
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  productDesktopElement.click(function () {
    if ($(this).length === 0) {
      return;
    }

    getSelectedCheckbox($(this), paramsName.products);
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  categoryDesktopElement.click(function () {
    if ($(this).length === 0) {
      return;
    }

    getSelectedCheckbox($(this), paramsName.types);
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  merchantDesktopElement.change(function () {
    if ($(this).length === 0) {
      return;
    }

    const selectedItem = $(this)
      ?.find($("input.dropdown-selected-value.merchant-desktop-element"))
      ?.val()
      ?.toLowerCase()?.split('-')?.join(' ');
    switch (selectedItem) {
      case "all":
        removeURLParams(paramsName.partner, true);
        break;

      default:
        setURLParams(paramsName.partner, selectedItem, true);
        break;
    }
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  sortMobileElement.change(function () {
    if ($(this).length === 0) {
      return;
    }

    const selectedItem = $(this)?.find($("input.dropdown-selected-value.sort-mobile-element"))?.val()?.toLowerCase();
    switch (selectedItem) {
      case "most-popular":
        removeURLParams(paramsName.sort, true);
        paramsValue.sort = DEFAULT_SORT_PARAMS;
        break;

      default:
        setURLParams(paramsName.sort, selectedItem?.split('-')?.join(' '), true);
        paramsValue.sort = '';
        break;
    }
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  productMobileElement.click(function () {
    if ($(this).length === 0) {
      return;
    }

    getSelectedCheckbox($(this), paramsName.products);
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  categoryMobileElement.click(function () {
    if ($(this).length === 0) {
      return;
    }

    getSelectedCheckbox($(this), paramsName.types);
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });

  merchantMobileElement.change(function () {
    if ($(this).length === 0) {
      return;
    }

    const selectedItem = $(this)
        ?.find($("input.dropdown-selected-value.merchant-mobile-element"))
        ?.val()
        ?.toLowerCase()?.split('-')?.join(' ');
    switch (selectedItem) {
      case "all":
        removeURLParams(paramsName.partner, true);
        break;

      default:
        setURLParams(paramsName.partner, selectedItem, true);
        break;
    }
    paramsValue.offset = 0;
    currentPage = 1;
    getPromotions();
  });
});


$(document).ready(function () {
  let sortText = $('.analytics-ol-sort-mobile .dropdown__list').find('li:first-child').text();
  $('.analytics-ol-sort-mobile .display__text').text(sortText);
  $('.analytics-ol-sort-mobile input').val(sortText);
    $(".enhanced-offer-filter__container").click(function () {
      if($(this).hasClass('enhanced-offer-filter__container')){
        captureOLFilters()
      }
    });

    $(".offer-listing-enhance-mobile,.analytics-ol-sort-mobile,.analytics-ol-merchant-mobile").click(function () {
      captureOLFilters();
    //   $(".enhanced-offer-filter__container").trigger('click');
    });
  });

  //analytics : start
  let sortedOfferListCategories;
  let sortedOfferListProduct;
  let sortedOfferListMerchant;
  let sortedOfferListSort;
  let updatedOfferListFilters = "";
  let merchantPlaceholder=''

  //capturing Categories
  function captureOLCategories(){
    let updatedOLCategories = [];
    if ($(window).width() > 767) {
      $('.analytics-ol-categories .offer-filter__checkbox-item input[type="checkbox"]:checked').each(function () {
        updatedOLCategories.push($(this).next("span").text());
      });
      sortedOfferListCategories =
      updatedOLCategories && updatedOLCategories.length > 0 ? updatedOLCategories.join(" | ") : "";
    } else {
      $('.offer-listing-enhance-mobile .category_filter .offer-filter__checkbox-item-mobile input[type="checkbox"]:checked').each(function () {
        updatedOLCategories.push($(this).next("span").text());
      });
      sortedOfferListCategories =
      updatedOLCategories && updatedOLCategories.length > 0 ? updatedOLCategories.join(" | ") : "";
    }
  }

  function captureProductFilter(){
    let updatedProductFilter = [];
    if ($(window).width() > 767) {
      $('.analytics-product-type .offer-filter__checkbox-item input[type="checkbox"]:checked').each(function () {
        updatedProductFilter.push($(this).next("span").text());
      });
      sortedOfferListProduct =
      updatedProductFilter && updatedProductFilter.length > 0 ? updatedProductFilter.join(" | ") : "";
    } else {
      $('.offer-listing-enhance-mobile .product_filter .offer-filter__checkbox-item-mobile input[type="checkbox"]:checked').each(function () {
        updatedProductFilter.push($(this).next("span").text());
      });
      sortedOfferListProduct =
      updatedProductFilter && updatedProductFilter.length > 0 ? updatedProductFilter.join(" | ") : "";
    }
  }

  // capturing Sort
  function captureOLSort() {
    let updatedOLSort = [];
    if ($(window).width() > 767) {
      $('.analytics-ol-sort .offer-filter__checkbox-item .checkbox-item__wrapper input[type="radio"]:checked').each(function () {
        updatedOLSort.push($(this).next("div").text());
      });
      sortedOfferListSort = updatedOLSort && updatedOLSort.length > 0 ? updatedOLSort.join(" | ") : "";
    } else {
      let updatedOLSortMobile = $('.analytics-ol-sort-mobile input').val();
      sortedOfferListSort = updatedOLSortMobile ? updatedOLSortMobile : "";
    }
  }
    // capturing Merchant
    function captureOLMerchant(){
      merchantPlaceholder = $('.analytics-ol-merchant .display__text').attr('data-placeholder')
      if ($(window).width() > 767) {
        let updatedOLMerchant = '';
        if(updatedOLMerchant!='-1' && merchantPlaceholder==$('.analytics-ol-merchant .display__text').text()){
          updatedOLMerchant = $('.analytics-ol-merchant input').val();
        }else{
          updatedOLMerchant = $('.analytics-ol-merchant .display__text').text();
        }
        sortedOfferListMerchant = updatedOLMerchant && updatedOLMerchant.length > 0 ? updatedOLMerchant : "";
      }else{
        let updatedOLMerchant = $('.analytics-ol-merchant-mobile input').val();
        sortedOfferListMerchant = updatedOLMerchant && updatedOLMerchant.length > 0 ? updatedOLMerchant : "";
      }

      }

  function captureOLFilters(){
    captureOLSort();
    captureProductFilter();
    captureOLCategories();
    captureOLMerchant();

    let categoriesClick = sortedOfferListCategories;
    let merchantClick = sortedOfferListMerchant;
    let SortClick = sortedOfferListSort;
    let cardFilterClick = sortedOfferListProduct;
    let updatedOfferListFilters = "";

   if (SortClick && SortClick.trim().length > 0) {
    if (updatedOfferListFilters.length > 0) {
       updatedOfferListFilters += " | " + SortClick.trim();
    } else {
       updatedOfferListFilters += SortClick.trim();
    }
  }
  if (cardFilterClick && cardFilterClick.trim().length > 0) {
    if (updatedOfferListFilters.length > 0) {
       updatedOfferListFilters += " | " + cardFilterClick.trim();
    } else {
       updatedOfferListFilters += cardFilterClick.trim();
    }
  }
  if (categoriesClick && categoriesClick.trim().length > 0) {
    if (updatedOfferListFilters.length > 0) {
       updatedOfferListFilters += " | " + categoriesClick.trim();
    } else {
       updatedOfferListFilters += categoriesClick.trim();
    }
 }
  if (merchantClick && merchantClick.trim().length > 0 && merchantClick!='-1') {
    if (updatedOfferListFilters.length > 0) {
       updatedOfferListFilters += " | " + merchantClick.trim();
    } else {
       updatedOfferListFilters += merchantClick.trim();
    }
  }
   //updated dataLayerObject
   var calendarEventsDataValues = $(".enhanced-offer-filter__container").data().trackingClickInfoValue;
   if (calendarEventsDataValues) {
    var jsonStr = calendarEventsDataValues.replace(/'/g, '"');
    var json = JSON.parse(jsonStr);
    json.articleFilter = updatedOfferListFilters;
    var updatedValues = JSON.stringify(json).replace(/"/g, "'");
    $(".enhanced-offer-filter__container").attr("data-tracking-click-info-value", updatedValues);
   }
  }

  //analytics : end