import moment from 'moment';
import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/ui/widgets/selectmenu.js';
import"@gabrielfins/ripple-effect";

$(document).ready(function () {
  function updatePosition(button) {
    let popup = button.next('.calendar-popup');
    let offset = button.offset();
    let parentHeight = button.outerHeight();
    let height = 305;
    let windowHeight = $(window).height();
    let top = parentHeight/2.0 + 8;
    if (offset.top + height > windowHeight) {
      top = (windowHeight - (offset.top + height)) - 20;
    } else if (offset.top < 0) {
      top = offset.top + 48;
    }
    popup.css({'top': top});
  }
  let calendarButton = $('.calendar_real_estate');
  calendarButton.each((calendarIndex, elementCalendar) => {    
    let popup = $(elementCalendar).next('.calendar-popup');
    const acceptAllDate = $(elementCalendar).attr("data-acceptAllDate");
    popup.empty();
    popup.addClass('tcb-date-picker');
    let calendarInputfield = popup.siblings('.calendar_real_estate').find('.calendar__input-field');
    calendarInputfield.val(moment(new Date()).format('DD/MM/yy'));
    popup.on('ready', function() {
      popup.datepicker('option', {
        dateFormat: 'dd/mm/yy',
        maxDate: acceptAllDate ? null : new Date(),
        selectOtherMonths: true,
        onSelect: function(date) {
          calendarInputfield.text(date);
          calendarInputfield.val(date);
          $('body').toggleClass('calendar-showing');
          $(elementCalendar).next('.calendar-popup').toggleClass('active');
          calendarInputfield.trigger('updateCal', date);
        }
      });
    });
    $(elementCalendar).on('click', function (event) {
      $('body').toggleClass('calendar-showing');
      updatePosition($(elementCalendar));
      if ($(event.target).parents('.ui-datepicker').length == 0) {
        $(elementCalendar).next('.calendar-popup').toggleClass('active');
      }
    });
  });

  // datepicker'
  let datepicker = $('.datepicker');

  datepicker.each(function () {
    let datepickerSection = $(this).find('.datepicker-section');
    let datepickerInp = datepickerSection.find('.datepicker-input');
    let changeYearType = $(this).find('.datepicker-input').data('change-year') ?? true;
    let acceptAllDate = $(this).find('.datepicker-input').data('accept-all-date') ?? false;
    let yearTitle = $(this).find('.datepicker-input').data('year-title');

    function convertToLists(inst) {
      function handle() {
        let yearSelect = inst.dpDiv.find('select');
        yearSelect.selectmenu({ 
          position: {my: "left top", at: "left top", collision: 'none'}, 
          appendTo: '.ui-datepicker-title', 
          classes: {'ui-selectmenu-menu': "select-year-dropdown", 'ui-selectmenu-button': 'select-year-button'},
          width: 278,
          select: function() {
            $(this).trigger('change');
            handle();
          }
        });
      }
      handle();
    }

    datepickerInp.datepicker({
      dateFormat: 'yy-mm-dd',
      autoSize: true,
      maxDate: acceptAllDate ? null : 0,
      duration: 'fast',
      beforeShowDay: function(date) {
        return [true, 'md-ripples']
      },
      onUpdateDatepicker: function(inst) {
        convertToLists(inst);
        inst.dpDiv.find('.ui-datepicker-prev, .ui-datepicker-next').addClass('md-ripples');
      },
      onSelect: function(dateText, inst) {
        datepickerInp.trigger('date-change', dateText);
        $('body').removeClass('datepicker-showing');
      }
    });

    let date = new Date();
    let yearNow = date.getFullYear();

    // set year range datepicker
    datepickerInp.datepicker('option', 'yearRange', '1900:' + yearNow);

    let picker = $('#ui-datepicker-div');
    picker.wrap('<div id="datepicker-container"></div>');

    datepickerSection.on('click', function (e) {
      e.stopPropagation();
      $('body').addClass('datepicker-showing');
      $(this).datepicker('show');
    });

    if (changeYearType) {
      picker.addClass('change-year');
      datepickerInp.datepicker('option', 'changeYear', true);
      let element = $('#ui-datepicker-div.change-year');
      element.css("--yearVar", `" ${new Date().getFullYear()}"`);
      element.css("--yearTitle", `"${yearTitle}"`);
      datepickerInp.datepicker('option', 'onChangeMonthYear', function(year, month, inst) {
        element.css("--yearVar", `" ${year}"`);
        convertToLists(inst);
      });
    }

    $(document).click(function (event) {
      if ((!$(event.target).is(picker) && !$(event.target).parents().is(picker)) && ($(event.target).parents('.select-year-dropdown').length == 0)) {
        $('body').removeClass('datepicker-showing');
      }
    });

    picker.on('click', (event) => {
      event.stopPropagation();
      if ($(event.target).hasClass('select-year-button') || $(event.target).parents('.select-year-button').length != 0) {
        $('.ui-datepicker-year').selectmenu('open');
      }
    });

    datepickerInp.change(function () {
      $('body').removeClass('datepicker-showing');
    });
  });
});
