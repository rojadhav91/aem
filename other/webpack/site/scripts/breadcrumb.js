$(function () {
    // remove section container contains breadcrumb if breadcrumb is hidden
    if ($('.hero-breadcrumb-container').length == 0) {
        $('.breadcrumb').closest('.sectioncontainer').remove();        
    }
    
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $('.aem-Grid:has(.breadcrumb)').css('position', 'relative');
        $('.article-content--wrapper:has(.breadcrumb)').css('position', 'relative');
    }

    const olElement = $('ol.cmp-breadcrumb__list');
    const mediumBreakpoint = 768;

    if (olElement?.length > 0 && window.innerWidth <= mediumBreakpoint) {
        const scrollWidth = document.getElementsByClassName('cmp-breadcrumb__list')[0].scrollWidth;
        olElement.scrollLeft(scrollWidth);
    }
});