import 'jquery-ui/ui/widgets/autocomplete';
import * as constants from './atm-mock-data';

const itemSection = (type, name, address, src, phone, lat, long, id) => `
<div class="item">
    <div class="image">
        <img
        src="${src}"
        alt />
    </div>
    <div class="content">
        <p class="type">${type}</p>
        <p class="name">${name}</p>
        <p class="address">${address}</p>
        <p class="phone">${phone}</p>
        <input type="hidden" class="lattitude" value="${lat}"/>
        <input type="hidden" class="longtitude" value="${long}"/>
        <input type="hidden" class="atmBranchItemId" value="${id}"/>
    </div>
</div>
`;

const atmIcon = 'https://d1kndcit1zrj97.cloudfront.net/uploads/icon_atm_2dd4ca616f.png?w=1920&q=75';
const cdmIcon =
  'https://dehs0b8bq571d.cloudfront.net/uploads/Property_1_Active_1_b881a6b4f8_118c494fc0.png?w=1080&q=75';
const branchicon = 'https://d1kndcit1zrj97.cloudfront.net/uploads/icon_branch_e24da9e40d.png?w=1920&q=75';

var srcGoogleMap = 'https://www.google.com/maps/embed/v1/place?key=';
const googleHref = 'https://www.google.com/maps/search/?api=1&query=';

const date = new Date();
let timeList = [
];

const timeNow = date.getHours() + ':' + date.getSeconds();
const dayOfWeek = date.getDay();

const itemTimeSelect = (setTime, className, itemId) => `
<div class="item">
    <span class="${className ?? ''}"  itemId="${itemId ?? ''}">${setTime}</span>
    <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg">
</div>
`;


let onlyATM = [];
let onlyCDM = [];
let branchList = [];

const EMAIL = 'emailAddress';
const PHONE = 'phone';
const TAXCODE = 'taxCode';
let CityList = [];

const appendItems = (element, type, arr) => {
  var icon;
  if (type == 'ATM') {
    icon = atmIcon;
  } else if (type == 'CDM') {
    icon = cdmIcon;
  } else {
    icon = branchicon;
  }

  if (arr && arr.length > 0) {
    for (let i = 0; i < arr.length; i++) {
      const latTitude = arr[i].lat ?? arr[i].atmLatitude;
      const longTidude = arr[i].long ?? arr[i].atmLongitude;
      const id = arr[i].atmId ?? arr[i].branchId;
      var itemHtml = itemSection(type, arr[i].atmName ?? arr[i].branchNm, arr[i].atmAddress ?? arr[i].pstlAdr?.adrLine, icon, arr[i].phone, latTitude, longTidude, id);
      element.append(itemHtml);
    }
  }
};

const removeAccents = (str) => {
  return str
    ?.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/đ/g, 'd')
    .replace(/Đ/g, 'D');
};

const changeStrToSearch = (str) => {
  return removeAccents(str?.toLowerCase().replace(/\s/g, ''));
};

const searchAddress = (searchTag, arr, mode) => {
  var newArr = [];

  for (let i = 0; i < arr.length; i++) {
    if(searchTag) {
      for (let j = 0; j < searchTag.length; j++) {
        var searchValue = changeStrToSearch(searchTag[j]);
        if (mode == 'name') {
          var add = changeStrToSearch(arr[i].atmName ?? arr[i].branchNm);
        } else {
          var add = changeStrToSearch(arr[i].atmAddress ?? arr[i]?.pstlAdr?.adrLine);
        }
  
        if (add.indexOf(searchValue) != -1) {
          newArr.push(arr[i]);
          break;
        }
      }
    }
  }

  return newArr;
};

const filterNameOrAddress = (searchInput, dataInput, mode) => {
  const searInputTmp = changeStrToSearch(searchInput);
  return (dataInput && dataInput.length > 0) ? dataInput.filter((item) => {
    if (mode === 'name') {
      const itemTmp = changeStrToSearch(item.atmName ?? item?.branchNm);
      return itemTmp?.includes(searInputTmp);
    } else {
      const itemTmp = changeStrToSearch(item.atmAddress ?? item?.pstlAdr?.adrLine);
      return itemTmp?.includes(searchInput);
    }
  }) : [];
}

const getAutocompleteList = (arr) => {
  var newArr = [];
  for (let i = 0; i < arr.length; i++) {
    newArr.push(arr[i].atmName ?? arr[i].branchNm);
  }

  return newArr;
};

const appendResults = (atmResults, cdmResults, branchResults, resultEl, searchType, message, branchLabel) => {
  // append atm list
  resultEl.find('.atm-list').empty();
  appendItems(resultEl.find('.atm-list'), 'ATM', atmResults);

  // append cdm list
  resultEl.find('.cdm-list').empty();
  appendItems(resultEl.find('.cdm-list'), 'CDM', cdmResults);


  // append branch list
  resultEl.find('.branch-list').empty();
  appendItems(resultEl.find('.branch-list'), branchLabel, branchResults);

  let count = searchType == 'ATM' ? atmResults.length + ' '+ searchType: searchType == 'CDM' ? cdmResults.length + ' '+  searchType : branchResults.length + ' '+  branchLabel;
  const regex = /\$count/g;
  const replaceMessage = '<span class="count">' + count + '</span>';
  resultEl.find('.result-message').html(message.replace(regex, replaceMessage));
};

const appendFilterResults = (searChName, selectedCityindex, selectedDistrictIndex, searchResult, searchType, message, branchLabel) => {
  var atmResults;
  var cdmResults;
  var branchResults;

  if (selectedCityindex) {

    // filter by city
    var atmCityFilter = onlyATM.filter((atm) => atm.cityCode == selectedCityindex);
    var cdmCityFilter = onlyCDM.filter((cdm) => cdm.cityCode == selectedCityindex);
    var branchCityFilter = branchList.filter((branch) => branch.cityId == selectedCityindex);

    // filter by district
    if (selectedDistrictIndex) {
      atmResults = onlyATM.filter((atm) => atm.cityCode == selectedCityindex && atm.districtCode == selectedDistrictIndex);
      cdmResults = onlyCDM.filter((cdm) => cdm.cityCode == selectedCityindex && cdm.districtCode == selectedDistrictIndex);
      branchResults = branchList.filter((branch) => branch.cityId == selectedCityindex && branch.districtId == selectedDistrictIndex);
    } else {
      atmResults = atmCityFilter;
      cdmResults = cdmCityFilter;
      branchResults = branchCityFilter;
    }
  } else {
    atmResults = onlyATM;
    cdmResults = onlyCDM;
    branchResults = branchList;
  }
  atmResults = filterNameOrAddress(searChName, atmResults, 'name');
  cdmResults = filterNameOrAddress(searChName, cdmResults, 'name');
  branchResults = filterNameOrAddress(searChName, branchResults, 'name');
  appendResults(atmResults, cdmResults, branchResults, searchResult, searchType, message, branchLabel);
};

const setMap = (map, item) => {
  var location = item.find('.address').text();
  map.attr('src', srcGoogleMap + location);
};

$(document).ready(function () {
  $('.branches-atm-locate input[name=youAre]').on('change',function(){
    
    let typeCUstomer = $('input[name=youAre]:checked').parent().find('.branches-radio-checkmark span:first-child').text();
    let types=[".atm-type",".branch-type",".cdm-type"];
    let bracnhValue='';
    let branchValueStr='';
    let branchValueJson={};
    let updatedValues = '';
    $.each(types,(i,v)=>{
      if($(v).data().trackingClickInfoValue) {
        bracnhValue = $(v).data().trackingClickInfoValue
        branchValueStr = bracnhValue.replace(/'/g, '"');
        branchValueJson = JSON.parse(branchValueStr);
        branchValueJson.customerType = typeCUstomer;
        updatedValues = JSON.stringify(branchValueJson).replace(/"/g, "'");
        $(v).attr("data-tracking-click-info-value", updatedValues);
      }
    });
    $('input[name="youAre"]').prop('checked', false);
    $(this).prop('checked', true);
  })

  $('.atm-map-component').each(function (i, element) {
    srcGoogleMap += $(this).attr("data-map-key") + '&q=';
    const api = $(this).attr("data-api");
  });
});

// ATM Map component
$(document).ready(function () {
  var atmMap = $('.atm-map-component');

  atmMap.each(function (i, element) {
    var typeSearchMapBtn = $(this).find('.search-type button');
    var ItemSection = $(this).find('.show-list');
    var atmListItem = ItemSection.find('.atm-list');
    var branchListItem = ItemSection.find('.branch-list');
    var cdmListItem = ItemSection.find('.cdm-list');
    var mapIframe = $(this).find('#atm-map');
    var atmDetail = $(this).find('.detail-item');
    var itemImageDetail = atmDetail.find('.atm img');
    var itemNameDetail = atmDetail.find('.name');
    var itemAddressDetail = atmDetail.find('.address');
    var expandCardsPhone = $(this).find('.expand_cards_phone');
    var itemContactNumber = $(expandCardsPhone).find('.contact-number');

    var comebackBtn = atmDetail.find('.comeback');
    var direction = atmDetail.find('.direction');
    var branchBtn = atmDetail.find('.branch-btn');

    var noOption = $(this).attr("data-no-option");
    var resultMessage = $(this).attr("data-result-message");
    var placeHolder = $(this).attr("data-district-place-holder");

    var searchFilter = $(this).find('.search-filter');
    var cityFilter = searchFilter.find('.city');
    var districtFilter = searchFilter.find('.district');
    var citySelect = cityFilter.find('.option');
    var cityOptions = cityFilter.find('.select-options');
    var districtSelect = districtFilter.find('.option');
    var districtOptions = districtFilter.find('.select-options');

    var searchInput = $(this).find('.search-input');
    var searchResult = $(this).find('.search-result');

    var searchMapState = 1;
    var timeSelected = '';
    let atmCdmCitiesOrigin = [];
    let branchAddressOrigin = [];
    const api = $(this).attr("data-api");
    const apiBranch = $(this).attr("data-api-branch");
    const apiBrahchSuffix = $(this).attr("data-api-branch-suffix");
    const dataBranchLabel = $(this).attr("data-branch-label");
    let defaultLat = 21.025529328035024;
    let defaultLong = 105.84885245019137;
    
    getAtmCdmList(api, '', '', '', null).then(function (dataResponse) {
      onlyATM = dataResponse;
      onlyCDM = dataResponse.filter(item => item.isCDM == true);
      // init items
      appendResults(dataResponse, onlyCDM, [], $(element).find('.search-result'), 'ATM', resultMessage, dataBranchLabel);
      // init map
      setMap($(element).find('#atm-map'), $(element).find('.search-result .atm-list').children().first());
      // autocomplete input
      autocompleteType('ATM', searchInput.find('input'), dataResponse, api, '', '', searchResult, null);

      // select search result for detail
      $(element).find('.show-list').on('click', '.item', function () {
        showDetail($(this));
      });
    });
    getCurrentPosition(function(message){
      getBranchList(apiBranch, apiBrahchSuffix, '', defaultLat, defaultLong).then(function (dataResponse) {
        branchList = dataResponse.branches ?? [];
        branchAddressOrigin = dataResponse.cityList ?? [];
        // append branch list
        $(element).find('.search-result .branch-list').find('.branch-list').empty();
        appendItems($(element).find('.search-result .branch-list'), dataBranchLabel, branchList);
      });
    });
    

    getCityList(api).then(function (dataResponse) {
      CityList = dataResponse;
      atmCdmCitiesOrigin = [...dataResponse];
      cityRendering(CityList);
      const nearestOption = cityOptions.find('li[value="nearest"]');
        if (nearestOption.length) {
          nearestOption.click();
        }
    }).catch(function (error) {
      console.log(error);
    })

    // style focus search input
    searchInput.find('input').focus(function () {
      searchInput.find('.search').addClass('focus');
    });

    searchInput.find('input').focusout(function () {
      searchInput.find('.search').removeClass('focus');
    });

    // open city select-options
    citySelect.click(function () {
      if ($(this).hasClass('showed')) {
        $(this).removeClass('showed');
      } else {
        $(this).addClass('showed');
      }

      districtSelect.removeClass('showed');
    });

    // open district select-options
    districtSelect.click(function () {

      if ($(this).hasClass('showed')) {
        $(this).removeClass('showed');
      } else {
        $(this).addClass('showed');
      }
      citySelect.removeClass('showed');

      // add district options base on selected city
      const currentType = $(element).find('.search-type button.clicked').find('span').text();
      getDistrictByType(currentType);
    });

    function getDistrictByType(type) {
      var selectedCityIndex = cityOptions.find('li.selected').attr('city');
      if(type == 'ATM' || type == 'CDM') {
        getDistrictList(api, selectedCityIndex).then(function (dataResponse) {
          const districts = dataResponse;
          districtRendering(districts);
        });
      }else {
        districtRendering(filterDistrictsByCityCode(selectedCityIndex, branchAddressOrigin));
      }
    }

    // close select-option box
    $(document).click(function (event) {
      if (
        !$(event.target).is(cityOptions) &&
        !$(event.target).parents().is(cityOptions) &&
        !$(event.target).is(citySelect) &&
        !$(event.target).is(districtSelect)
      ) {
        citySelect.removeClass('showed');
        districtSelect.removeClass('showed');
      }
    });

    // select type of search results
    typeSearchMapBtn.click(function () {
      const prevDistrictSelected = $(districtOptions).find('.selected').text();
      
      searchInput.find('input').autocomplete('close').val('');
      searchInput.find('input').attr('selected-value', '');
      searchInput.find('input').autocomplete('option', 'source', []);
      searchInput.val('');
      typeSearchMapBtn.removeClass('clicked');
      $(this).addClass('clicked');
      var type = $(this).find('span').text();
      const cityData = (type == 'ATM' || type == 'CDM')? atmCdmCitiesOrigin : branchAddressOrigin;
      cityRendering(cityData);
      getDistrictByType(type);
      const selectedCityindex = $(cityOptions).find('.selected').attr('city');
      let btnBracnhValue = $(this).data().trackingClickInfoValue;
      if(btnBracnhValue) {
        let btnJsonStr = btnBracnhValue.replace(/'/g, '"');
        let btnJson = JSON.parse(btnJsonStr);
        let bracnhValue='';
        let branchValueStr='';
        let branchValueJson={};
        let updatedValues = '';
        $('.branches-radio-container').each((i,v)=>{
          bracnhValue = $(v).data().trackingClickInfoValue
          branchValueStr = bracnhValue.replace(/'/g, '"');
          branchValueJson = JSON.parse(branchValueStr);
          branchValueJson.branchSearchType = btnJson.branchSearchType;        
          updatedValues = JSON.stringify(branchValueJson).replace(/"/g, "'");
          $(v).attr("data-tracking-click-info-value", updatedValues);
        });
      }
      // show search results
      if (type == 'ATM') {
        atmListItem.css('display', 'block');
        cdmListItem.css('display', 'none');
        branchListItem.css('display', 'none');
        expandCardsPhone.css('display', 'none');

        
        searchMapState = 1;
      } else if (type == 'CDM') {
        atmListItem.css('display', 'none');
        cdmListItem.css('display', 'block');
        branchListItem.css('display', 'none');
        expandCardsPhone.css('display', 'none');

        searchMapState = 2;
      } else {
        atmListItem.css('display', 'none');
        cdmListItem.css('display', 'none');
        branchListItem.css('display', 'block');
        expandCardsPhone.css('display', 'block');

        searchMapState = 3;
      }

      ItemSection.css('display', 'block');
      atmDetail.css('display', 'none');
    });

    // show detail item
    function showDetail(item) {
      
      ItemSection.css('display', 'none');
      atmDetail.css('display', 'block');

      // branch type
      if (searchMapState == 3) {
        branchBtn.css('display', 'flex');
      } else {
        branchBtn.css('display', 'none');
      }

      // set item detail info
      var itemImageSrc = item.find('.image img').attr('src');
      var itemName = item.find('.name').text();
      var itemAddress = item.find('.address').text();
      var phone = item.find('.phone').text();
      var lat = item.find('.lattitude').val();
      var long = item.find('.longtitude').val();
      var currentBranchSelect = item.find('.atmBranchItemId').val();
      
      itemImageDetail.attr('src', itemImageSrc);
      itemNameDetail.text(itemName);
      itemAddressDetail.text(itemAddress);
      atmDetail.find('.btn-contactus .cta-button').attr('currentBranchSelect', currentBranchSelect);
      if (phone != 'undefined') {
        itemContactNumber.text(phone);
        expandCardsPhone.css('display', 'block');
      } else {
        expandCardsPhone.css('display', 'none');
      }
      
      // init map
      mapIframe.attr('src', srcGoogleMap + lat+','+long);
      direction.attr('href', googleHref + lat+','+long);
    }

    // comeback to list result
    comebackBtn.click(function () {
      atmDetail.css('display', 'none');
      ItemSection.css('display', 'block');
    });

    function districtRendering(districts) {
      const prevSelected = $(districtOptions).find('.selected').text();
      districtOptions.find('ul li:not([value="all"])').remove();
      const currentType = $(element).find('.search-type button.clicked').find('span').text();
      
      for (let i = 0; i < districts.length; i++) {
        const disTrictCode = districts[i].districtCode ?? districts[i].districtCd;
        const disTrictName = districts[i].districtName ?? districts[i].districtNm;
        if(disTrictCode && disTrictName) {
          districtOptions.find('ul').append(`<li district=${disTrictCode} class="${prevSelected? changeStrToSearch(prevSelected) == changeStrToSearch(disTrictName) ? 'selected' : '' :''}" >${disTrictName}</li>`);
        }
      }

      // when select districts
      districtOptions.find('li').click(function () {
        districtSelect.removeClass('showed');
        districtSelect.find('input').attr('placeholder', $(this).text());

        const selectedCityindex = $(cityOptions).find('.selected').attr('city');
        const selectedDistrict = $(this).attr('district');
       
        const searchValue = searchInput.val();
        appendFilterResults(searchValue, selectedCityindex, selectedDistrict, searchResult, currentType, resultMessage, dataBranchLabel);

        districtOptions.find('li').removeClass('selected');
        $(this).addClass('selected');

        // reset map
        if (searchMapState == 1) {
          setMap(mapIframe, atmListItem.children().first());
        } else if (searchMapState == 2) {
          setMap(mapIframe, cdmListItem.children().first());
        } else {
          setMap(mapIframe, branchListItem.children().first());
        }

        comebackBtn.click();      
      });

      const selectedCity = $(cityOptions).find('.selected').attr('city');
      const selectedDistricts = $(districtOptions).find('.selected').attr('district');
      const searchData = searchInput.val();
      const selectAll = cityOptions.find('li[value="all"]').text();
      if(currentType == 'ATM' || currentType == 'CDM') {
        const cityCodes = atmCdmCitiesOrigin.filter((city) => city.cityCode != null).map((city) => city.cityCode.toString());
        const disStrictCode =  districts.filter((district) => district.districtCode != null).map((district) => district.districtCode.toString());
        if(!(selectedCity && cityCodes.includes(selectedCity))){
		      citySelect.find('input').attr('placeholder', selectAll);
        }
        if(!(selectedDistricts && disStrictCode.includes(selectedDistricts))) {
          districtSelect.find('input').attr('placeholder', selectAll);
        }
      }else {
        const cityCodes = branchAddressOrigin.map((barnch) => barnch.city.cityCd.toString());
        const disStrictCode =  districts.filter((district) => district.districtCd != null).map((district) => district.districtCd.toString());
        if(!(selectedCity && cityCodes.includes(selectedCity))){
		      citySelect.find('input').attr('placeholder', selectAll);
        }
        if(!(selectedDistricts && disStrictCode.includes(selectedDistricts))) {
          districtSelect.find('input').attr('placeholder', selectAll);
        }
      }

      appendFilterResults(searchData, selectedCity, selectedDistricts, searchResult, currentType, resultMessage, dataBranchLabel);
      const data = currentType === 'ATM' ? onlyATM : currentType === 'CDM' ? onlyCDM : branchList;
      autocompleteType(currentType, searchInput.find('input'), data, api, selectedCity, selectedDistricts, searchResult, currentType);
      setMap(mapIframe, atmListItem.children().first());
    }
    
    function cityRendering(CityList) {
      const prevSelected = $(cityOptions).find('.selected').text();
      cityOptions.find('ul li:not([value="all"]):not([value="nearest"])').remove();
      for (let i = 0; i < CityList.length; i++) {
        const cityCode = CityList[i].cityCode ?? CityList[i]?.city?.cityCd;
        const cityName = CityList[i].cityName ?? CityList[i]?.city?.cityNm;
        if (cityCode && cityName) {
          $(element)
            .find('.search-filter .city .select-options ul')
            .append(`<li city='${cityCode}' class="${prevSelected? changeStrToSearch(prevSelected) == changeStrToSearch(cityName) ? 'selected' : '' :'' }">${cityName}</li>`);
        }

        // when select cities
        cityOptions.find('li').off('click').click(function () {
          citySelect.removeClass('showed');
          var city = $(this).text();
          const selectedCityindex = $(this).attr('city');
          const currentType = $(element).find('.search-type button.clicked').find('span').text();
          const searchValue = searchInput.val();
          appendFilterResults(searchValue, selectedCityindex, '', searchResult, currentType, resultMessage, dataBranchLabel);
          // disable/enable district select-options
          if ($(this).attr('value') != 'all' && $(this).attr('value') != 'nearest') {
            districtFilter.removeClass('disable');
          } else {
            districtFilter.addClass('disable');
          }

          // reset selected district when re-select cities
          citySelect.find('input').attr('placeholder', city);
          districtSelect.find('input').attr('placeholder', placeHolder);

          cityOptions.find('li').removeClass('selected');
          districtOptions.find('li').removeClass('selected');

          $(this).addClass('selected');

          // reset map
          if ($(this).attr('value') === 'nearest') {
            getNearestAtmBranch(function(error){
              const selectAll = cityOptions.find('li[value="all"]');
              if (selectAll.length) {
                selectAll.click();
              }
            });
          } else if (searchMapState == 1) {
            setMap(mapIframe, atmListItem.children().first());
          } else if (searchMapState == 2) {
            setMap(mapIframe, cdmListItem.children().first());
          } else {
            setMap(mapIframe, branchListItem.children().first());
          }

          // reset detail item
          comebackBtn.click();
        });
      }
    }
    // autocomplete
    function initAutocomplete(inp, arr, api, cityCode, districtCode, searchResultelement, typeSelected, isCDMApi) {
      inp.autocomplete({
        appendTo: '#atm-map-autocomplete',
        minLength: 0,
        source: function (request, response) {
          const userInput = changeStrToSearch(request.term);
          const filteredItems = arr.filter(tag =>
            changeStrToSearch(tag).includes(userInput)
          );
          if (filteredItems.length === 0) {
            response([
              {
                label: noOption,
                value: "",
                disabled: true,
              },
            ]);
          } else {
            response(filteredItems);
          }
        },

        select: function (event, ui) {
          if (ui.item.disabled) {
            event.preventDefault();
            return;
          }
          $(this).attr('selected-value', ui.item.label);
          var name = [ui.item.label];
          appendSelectedAutocompleteItem(name);
          // reset detail item
          comebackBtn.click();

          if (searchMapState == 1) {
            searchResult.find('.result-message .count').text(atmListItem.children().length);
          } else if (searchMapState == 2) {
            searchResult.find('.result-message .count').text(cdmListItem.children().length);
          } else {
            searchResult.find('.result-message .count').text(branchListItem.children().length);
          }
          searchInput.val(name[0]);
          const selectedCityindex = $(cityOptions).find('.selected').attr('city');
          const selectedDistrict = $(districtOptions).find('.selected').attr('district');
          const currentType = isCDMApi ?? $(element).find('.search-type button.clicked').find('span').text();
          appendFilterResults(name[0], selectedCityindex, selectedDistrict, searchResult, currentType, resultMessage, dataBranchLabel);
        },
        search: function (event, ui) {
          const currentType = isCDMApi ?? $(element).find('.search-type button.clicked').find('span').text();
          appendFilterResults($(this).val(), cityCode, districtCode, searchResultelement, currentType, resultMessage, dataBranchLabel);
        },
        create: function () {
          $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
            return $('<li>').append(item.label).append('</li>').appendTo(ul);
          };
        },

        open: function () {
          $('#atm-map-autocomplete > ul').css('top', '-20px');
        },
      });
    }

    function autocompleteType(type, inp, lstAtmCdmBranch, api, cityCode, districtCode, searchResultelement, isCDMApi) {
      const arr = getAutocompleteList(lstAtmCdmBranch)
      initAutocomplete(inp, arr, api, cityCode, districtCode, searchResultelement, type, isCDMApi);
    }

    function appendSelectedAutocompleteItem(name) {
      var arr;

      atmListItem.empty();
      cdmListItem.empty();
      branchListItem.empty();

      if (searchMapState == 1) {
        arr = searchAddress(name, onlyATM, 'name');
        appendItems(atmListItem, 'ATM', arr);
        setMap(mapIframe, atmListItem.children().first());
      } else if (searchMapState == 2) {
        arr = searchAddress(name, onlyCDM, 'name');
        appendItems(cdmListItem, 'CDM', arr);
        setMap(mapIframe, cdmListItem.children().first());
      } else {
        arr = searchAddress(name, branchList, 'name');
        appendItems(branchListItem, dataBranchLabel, arr);
        setMap(mapIframe, branchListItem.children().first());
      }
    }

    function getNearestAtmBranch(errorCallBack) {
      atmListItem.empty();
      cdmListItem.empty();
      branchListItem.empty();
      if( !defaultLat || !defaultLong) {
        errorCallBack('Not allow get position');
      }

      if(searchMapState == 1 || searchMapState == 2) {
        let data = searchMapState === 1 ? onlyATM : onlyCDM;
        
       if(data) {
        const sortedData = sortByDistance(defaultLat, defaultLong, [...data]);
        
        if (searchMapState == 1) {
          appendItems(atmListItem, 'ATM', sortedData);
        } else if (searchMapState == 2) {
          appendItems(cdmListItem, 'CDM', sortedData);
        }
        
       if(sortedData[0]) {
        mapIframe.attr('src', srcGoogleMap + sortedData[0].atmLatitude + ',' + sortedData[0].atmLongitude);
        direction.attr('href', googleHref + sortedData[0].atmLatitude + ',' + sortedData[0].atmLongitude);
       }
       }
      }else {
        appendItems(branchListItem, dataBranchLabel, branchList);
        mapIframe.attr('src', srcGoogleMap + branchList[0].lat + ',' + branchList[0].long);
        direction.attr('href', googleHref + branchList[0].lat + ',' + branchList[0].long);
      }
    }

    function getCurrentPosition(callBackFn) {
      navigator.geolocation.getCurrentPosition(function (position) {
        defaultLat = position.coords.latitude;
        defaultLong = position.coords.longitude;
        callBackFn('Allow get position');
      }, function(error) {
        callBackFn(error);
      });
    }

    function distance(lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1 / 180
      var radlat2 = Math.PI * lat2 / 180
      var theta = lon1 - lon2
      var radtheta = Math.PI * theta / 180
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist)
      dist = dist * 180 / Math.PI
      dist = dist * 60 * 1.1515
      if (unit == "K") { dist = dist * 1.609344 }
      if (unit == "N") { dist = dist * 0.8684 }
      return dist
    }

    function sortByDistance(lat, long, data) {
      return data.sort((a,b) => {
        const distanceA = distance(lat, long, a.atmLatitude, a.atmLongitude, "K");
        const distanceB = distance(lat, long, b.atmLatitude, b.atmLongitude, "K");
        return distanceA - distanceB
      })
    }
  });
});

const appendAddress = (CityList, branchList, selectedCityindex, selectedDistrictIndex, searchResult) => {
  var branchResults;
  if (selectedCityindex != -1) {
    var branchCityFilter = branchList.filter((branch) => branch.cityId == selectedCityindex);
    if (selectedDistrictIndex) {
      branchResults = branchList.filter((branch) => branch.cityId == selectedCityindex && branch.districtId == selectedDistrictIndex);
    } else {
      branchResults = [...branchCityFilter];
    }
  } else {
    branchResults = [...branchList];
  }
  searchResult.find('.branch .address .address-list').empty();
  for (let i = 0; i < branchResults.length; i++) {
    var setTime = branchResults[i];
    searchResult.find('.branch .address .address-list').append(itemTimeSelect(setTime?.pstlAdr?.adrLine ?? setTime.atmAddress, '', setTime.branchId));
  }
};

//appointment-booking

$(document).ready(function () {
  var btnContactUs = $(this).find('.btn-contactus');
  var popUpBookingContents = $(this).find('.booking');
  popUpBookingContents.each(function () {
    const popUpBookingContent = $(this);
    const apiBranch = $(this).attr("data-api-branch");
    const apiBrahchSuffix = $(this).attr("data-api-branch-suffix");
    const apiServices = $(this).attr("data-api-service");
    const apiBookingSlot = $(this).attr("data-api-booking-slot");
    const apiBookingConfirm = $(this).attr("data-api-booking-confirm");
    const serviceIdFromAtm = $(this).attr("data-serviceId");
    let CityList = [];
    let branchList = [];

    var btnSubmit = popUpBookingContent.find('.btn-submit');
    var searchFilter = popUpBookingContent.find('.search-filter');
    var cityFilter = searchFilter.find('.city');
    var districtFilter = searchFilter.find('.district');
    var citySelect = cityFilter.find('.option');
    var cityOptions = cityFilter.find('.select-options');
    var districtSelect = districtFilter.find('.option');
    var districtOptions = districtFilter.find('.select-options');
    var addresses = popUpBookingContent.find('.address-list');
    var closeButton = popUpBookingContent.find('.popup__close-icon');
    var scheduleBookingDate = popUpBookingContent.find('.schedule .date');
    var scheduleBookingTime = popUpBookingContent.find('.schedule .time');
    var isPersonal = true;
    var timeSelected = '';
    var todayLabel = popUpBookingContent.attr('data-today-label');
    var tomorrowLabel = popUpBookingContent.attr('data-tomorrow-label');
    const defaultLabelConfirmDate = popUpBookingContent.find('.confirm-appointment .date-appointment').text();
    let defaultLat = 21.025529328035024;
    let defaultLong = 105.84885245019137;
    let typeCustomerFromATM = undefined;
    if(btnContactUs) {
      var modal = $(btnContactUs).find('a.cta-button').attr('href');
      if(modal) {
        var popUpBooking = $("#"+modal.slice(1));
        btnContactUs.click(function (e) { 
          popUpBooking.removeClass('hide');
          popUpBooking.addClass('show');
          popUpBookingContent.find('.booking-container').removeClass('active');
          popUpBookingContent.find('.confirm-appointment').removeClass('active');
          clearForm(popUpBookingContent);
          e.preventDefault();
          
    
          if(popUpBookingContent.find('.client').length > 0) {
            popUpBookingContent.find('.client').css('display', 'block');
            popUpBookingContent.find('.client').addClass('active');
            popUpBookingContent.find('.service').addClass('hidden');
            popUpBookingContent.find('.branch').addClass('hidden');
            popUpBookingContent.find('.schedule').addClass('hidden');
            popUpBookingContent.find('.form').addClass('hidden');
            popUpBookingContent.find('.confirm-appointment').addClass('hidden');
          }else {
            typeCustomerFromATM = $('input[name=youAre]:checked').val();
            const branchIdFromATM = btnContactUs.find('.cta-button').attr('currentBranchSelect');
            popUpBookingContent.find('input[name="selectedBranch"]').val(branchIdFromATM);
            popUpBookingContent.find('input[name="selectedService"]').val(serviceIdFromAtm);
            showLoading();
            getBookingSlot(apiBookingSlot, serviceIdFromAtm, branchIdFromATM).then((timeLots) => {
              bookingTimeRendering(timeLots.data);
              popUpBookingContent.find('.schedule').css('display', 'block');
              popUpBookingContent.find('.schedule').addClass('active');
              popUpBookingContent.find('.form').addClass('hidden');
              popUpBookingContent.find('.confirm-appointment').addClass('hidden');
              selectedDateCSS(scheduleBookingDate.find('div').first());
              initTimeSchedule(scheduleBookingDate.find('div').first());
              hideLoading();
            }).catch ( error => {
              hideLoading();
              popUpBookingContent.find('.schedule').css('display', 'block');
              popUpBookingContent.find('.schedule').addClass('active');
              popUpBookingContent.find('.form').addClass('hidden');
              popUpBookingContent.find('.confirm-appointment').addClass('hidden');
              const errorMess = `<span class='error-response'>${error.errorMessage}</span>`
              popUpBookingContent.find('.schedule .time .time-list').html(errorMess);
            });
          }
         
          $('body').addClass('overflow-hidden'); // prevent scroll
        });

        closeButton.click(function () {
          popUpBooking.removeClass('show');
          popUpBooking.addClass('hide');
          $('body').removeClass('overflow-hidden');
        });
      }  
    }
    
    function cityRendering(CityList) {
      for (let i = 0; i < CityList.length; i++) {
        const cityCode = CityList[i].cityCode ?? CityList[i]?.city?.cityCd;
        const cityName = CityList[i].cityName ?? CityList[i]?.city?.cityNm;
        if (cityCode != null) {
          $(popUpBookingContent)
          .find('.search-filter .city .select-options ul')
          .append('<li city=' + cityCode + '>' + cityName + '</li>');
        }
      }

       // open city select-options
      citySelect.click(function () {
        if (!$(this).hasClass('showed')) {
          $(this).addClass('showed');
          districtSelect.removeClass('showed');
        } else {
          $(this).removeClass('showed');
        }
      });

      districtSelect.click(function () {
        $(this).addClass('showed');
        citySelect.removeClass('showed');

        var selectedCityIndex = cityOptions.find('li.selected').attr('city');
        disTrictRendering(filterDistrictsByCityCode(selectedCityIndex, CityList));
      });

      // when select cities
      cityOptions.find('li').click(function () {
        citySelect.removeClass('showed');
        var city = $(this).text();

        // reset selected district when re-select cities
        citySelect.addClass('selected');
        citySelect.find('.dropdown-body').text(city);
        districtSelect.find('.dropdown-body').text('');

        cityOptions.find('li').removeClass('selected');
        districtOptions.find('li').removeClass('selected');

        $(this).addClass('selected');

        // get filter results by selected city
        var selectedCityindex = $(this).attr('city');
        appendAddress(CityList, branchList, selectedCityindex, '', popUpBookingContent);
        registerAddressClick(addresses);
      });
    }

    function disTrictRendering(districts) {
      
      districtOptions.find('ul li:not([value="all"])').remove();
      for (let i = 0; i < districts.length; i++) {
        const disTrictCode = districts[i].districtCode ?? districts[i].districtCd;
        const disTrictName = districts[i].districtName ?? districts[i].districtNm;
        districtOptions.find('ul').append('<li district=' + disTrictCode + '>' + disTrictName + '</li>');
      }

      // when select districts
      districtOptions.find('li').click(function () {
        districtSelect.removeClass('showed');

        districtOptions.find('li').removeClass('selected');
        $(this).addClass('selected');

        // get filter results by selected district
        var selectedCityindex = cityOptions.find('li.selected').attr('city');
        var selectedDistrictIndex = $(this).attr('district');
        districtSelect.find('.dropdown-body').text($(this).text());
        appendAddress(CityList, branchList, selectedCityindex, selectedDistrictIndex, popUpBookingContent);
        registerAddressClick(addresses);
      });
    }
    var listClient = popUpBookingContent.find('.client-list .item');
    var listService = popUpBookingContent.find('.service-list .item');

    
    hideElementAfterAnimation(popUpBookingContent.find('.booking-container'));

    // close select-option box
    $(document).click(function (event) {
      if (
        !$(event.target).is(cityOptions) &&
        !$(event.target).parents().is(cityOptions) &&
        !$(event.target).is(citySelect) &&
        !$(event.target).is(districtSelect) &&
        !$(event.target).parents().is(citySelect) &&
        !$(event.target).parents().is(districtSelect)
      ) {
        citySelect.removeClass('showed');
        districtSelect.removeClass('showed');
      }
    });

    listClient.click(function () {
      popUpBookingContent.find('.booking-container').removeClass('active');
      isPersonal = $(this).find('.personal').length > 0 ? true : false;
      $(this).closest('.booking-container-wrapper').addClass(isPersonal ? 'selected-personal' : 'selected-bussiness');
      showLoading();
      getServices(apiServices, isPersonal).then((result) => {
        servicesRendering(result.data);
      }).catch ( error => {
        const errorMess = `<span class='error-response'>${error.errorMessage}</span>`
        popUpBookingContent.find('.service .service-list').html(errorMess);
      }).finally(() => {
        popUpBookingContent.find('.client').addClass('hidden');
        popUpBookingContent.find('.service').css('display', 'block');
        popUpBookingContent.find('.service').addClass('active');
        hideLoading();
      });
    });

    function servicesRendering(services) {
      popUpBookingContent.find('input[name="selectedService"]').val('');
      popUpBookingContent.find('.service-list').empty();
      for (let i = 0; i < services.length; i++) {
        var serviceItems = services[i];
        popUpBookingContent.find('.service-list').append(itemTimeSelect(serviceItems.srvName, '', serviceItems.srvIdent));
      }
      
      const itemService = popUpBookingContent.find('.service-list .item');
      itemService.click(function () {
        popUpBookingContent.find('input[name="selectedService"]').val($(this).find('span').attr('itemid'));        
        popUpBookingContent.find('.booking-container').removeClass('active');
        if(popUpBookingContent.find('.client').length > 0) { 
          const serviceId = popUpBookingContent.find('input[name="selectedService"]').val();
          showLoading();
          getBranchList(apiBranch, apiBrahchSuffix,'',defaultLat, defaultLong, serviceId).then(function (dataResponse) {
            branchList = dataResponse.branches ?? [];
            CityList = dataResponse.cityList ?? [];
            appendAddress(CityList, branchList, -1, '', $(popUpBookingContent));
            registerAddressClick(addresses);
            cityRendering(CityList);
            popUpBookingContent.find('.service').addClass('hidden');
            popUpBookingContent.find('.branch').css('display', 'block');
            popUpBookingContent.find('.branch').addClass('active');
            hideLoading();
          }).catch ( error => {
            popUpBookingContent.find('.service').addClass('hidden');
            popUpBookingContent.find('.branch').css('display', 'block');
            popUpBookingContent.find('.branch').addClass('active');
            const errorMess = `<span class='error-response'>${error.errorMessage}</span>`
            popUpBookingContent.find('.branch .address .address-list').html(errorMess);
            hideLoading();
          });
        }
      });
    }

    function bookingTimeRendering(timeList) {
      // set schedule booking
      const toDayAndTomorrow = getTodayAndTomorrow();
      $(popUpBookingContent).find('.schedule .time .time-list').empty();
      const todayDate = toDayAndTomorrow.today;
      const tomorrow = toDayAndTomorrow.tomorrow;
      $(popUpBookingContent)
        .find('.schedule .date .today')
        .html(todayLabel + ' ' + date.getDate() + '/' + (date.getMonth() + 1));
      $(popUpBookingContent)
        .find('.schedule .date .tomorrow')
        .html(tomorrowLabel +' ' + (date.getDate() + 1) + '/' + (date.getMonth() + 1));
      renderTimeByDate(todayDate, timeList);
      registerClickEvent();
      // click today/tomorrow schedule
      scheduleBookingDate.find('div').click(function () {
        $(popUpBookingContent).find('.schedule .time .time-list').empty();
        selectedDateCSS($(this));
        const todayEnable = scheduleBookingDate.find('.today.enable');
        if(todayEnable.length > 0 ) {
          renderTimeByDate(todayDate, timeList);
        }
        else {
          renderTimeByDate(tomorrow, timeList);
        }
        registerClickEvent();
      });
      selectedDateCSS(scheduleBookingDate.find('div').first());
    }
    


    function registerClickEvent() {
       // enable clicking function for available time to select
       scheduleBookingTime.find('.time-list .item').click(function () {
        if ($(this).attr('class').includes('enable')) {
          popUpBookingContent.find('.booking-container').removeClass('active');
          timeSelected = $(this).text();
          
          let checkPersonalBaseOnBookingVisit = isPersonal;
          if(!popUpBookingContent.find('.client').length > 0) { 
            checkPersonalBaseOnBookingVisit = !!(typeCustomerFromATM == '11');
          }

          if (!checkPersonalBaseOnBookingVisit) {
            popUpBookingContent.find('#' + TAXCODE).on('input', function () {
              validateTaxCode(popUpBookingContent);
            });
          }
          checkPersonalBaseOnBookingVisit ? popUpBookingContent.find('.taxt-code').css('display', 'none') : popUpBookingContent.find('.taxt-code').css('display', 'block');
          // popUpBookingContent.find('.schedule').css('display', 'none');
          popUpBookingContent.find('.schedule').addClass('hidden');
          popUpBookingContent.find('.form').css('display', 'flex'); // show submit form
          popUpBookingContent.find('.form').addClass('active');
        }
      });
    }
    function renderTimeByDate(seletedDate, dataRender) {
      const times = dataRender.filter((item) => item.slotDate == seletedDate)[0]?.slotData ?? [];
      timeList = times;
      for (let i = 0; i < times.length; i++) {
        var setTime = times[i];
        const enableTime = setTime.enable == 1? 'enable-time' : '';
        $(popUpBookingContent).find('.schedule .time .time-list').append(itemTimeSelect(setTime.slotTime, enableTime, ''));
      }
      changeTimeCSS();
    }

    btnSubmit.click(function (event) {    
      
      let checkPersonalBaseOnBookingVisit = isPersonal;
      if(!popUpBookingContent.find('.client').length > 0) { 
        checkPersonalBaseOnBookingVisit = !!(typeCustomerFromATM == '11');
      }

      if (!checkPersonalBaseOnBookingVisit) {
        validateTaxCode(popUpBookingContent);
      }
      validateEmail(popUpBookingContent);
      validatePhone(popUpBookingContent);
      recaptchaValidation(this); // changed by adobe
      const errors = popUpBookingContent.find('.form-error-message').filter(function(){
        return $(this).css("display") !== "none";
      });
      if (errors.length > 0) {
        event.preventDefault();
      } else {
       
       let type = '';
       if(popUpBookingContent.find('.client').length > 0) {
        type = isPersonal? "11" : "12";
       }else {
         type = typeCustomerFromATM
       }
       const name = popUpBookingContent.find('#name').val();   
       const tax = popUpBookingContent.find('#' + TAXCODE).val();
       const email = popUpBookingContent.find('#' + EMAIL).val();
       const phone = popUpBookingContent.find('#' + PHONE).val();
       const todayAndTomorrow = getTodayAndTomorrow();
       let iDType = '';
       if(type == "11") {
         iDType = "4";
       }else {
         const taxLength = tax.replace(/-/g, '').length;
         iDType = (taxLength == 10 || taxLength == 13) ? "6" : "7";
       }
       const serviceId = popUpBookingContent.find('input[name="selectedService"]').val();
       const branchId = popUpBookingContent.find('input[name="selectedBranch"]').val();
       const todaySelect = popUpBookingContent.find('.today.enable');
       const bkDt = (todaySelect.length > 0)? todayAndTomorrow.today : todayAndTomorrow.tomorrow;
       const serviceName = popUpBookingContent.find('.service-list [itemid="' +serviceId +'"]').text();
       const options = {
          weekday: 'long',
          day: '2-digit',
          month: 'long'
        };
       const requestObject = {
          "type": type,
          "iDType": iDType,
          "branchId": branchId,
          "bkDt":bkDt,
          "bkTm":timeSelected.trim(),
          "customerNm":name,
          "phneNb":phone,
          "accno":tax,
          "emailAdr": email,
          "servid": serviceId,
          "servnm": serviceName
      }
      showLoading();
      confirmSubmit(apiBookingConfirm, requestObject).then((res) => {
          popUpBookingContent.find('.booking-container').removeClass('active');
          popUpBookingContent.find('.form').addClass('hidden');
          popUpBookingContent.find('.confirm-appointment').css('display', 'block');
          popUpBookingContent.find('.confirm-appointment').addClass('active');
          const confirmAppointment = popUpBookingContent.find('.confirm-appointment');
          const formatDate = (res?.data?.bkDt)? new Date(res?.data?.bkDt) :  undefined;
          const confirmDateValue = defaultLabelConfirmDate + ' ' + formatDateForConfirm(formatDate) + ', ' + (res?.data?.bkTm ?? '');
          confirmAppointment.find('.date-appointment').text(confirmDateValue);
          confirmAppointment.find('.confirm-ticket-number').text(res?.data?.apmtQr ?? '');
          hideLoading();
        }).catch ( error => {
          hideLoading();
          const errorMess = `<br><span class='form-error-message'>${error.errorMessage}</span>`
          popUpBookingContent.find('.form .captcha').append(errorMess);
        });
        bookVisitAnalytics(); //book a visit analytics call
        event.preventDefault();
      }
    });
    
    popUpBookingContent.find('#' + EMAIL).on('input change', function () {
      validateEmail(popUpBookingContent);
    });
    popUpBookingContent.find('#' + PHONE).on('input change', function () {
      const currentInput = this.value.replace(/\D/g,'');
      if(/^\d+$/.test(this.value)) {
        validatePhone(popUpBookingContent);
      }else {
        this.value = currentInput;
      }
      if(!this.value) {
        validatePhone(popUpBookingContent);
      }
    });
    
    function formatDateForConfirm(dateInput) {

      if(!dateInput) {
        return '';
      }
      const day = dateInput.toLocaleDateString('en-US', { weekday: 'long'});
      const dayNumber = dateInput.toLocaleDateString('en-US', { day: '2-digit'});
      const month = dateInput.toLocaleDateString('en-Us', { month: 'long' });

      return `${day}, ${dayNumber} ${month}`;
    }

    function bookVisitAnalytics() {
      //hashed Email converter  
      let hashEmail = $("#emailAddress").val();
      if (hashEmail) {
         let hash = 0;
         for (let i = 0; i < hashEmail.length; i++) {
            const char = hashEmail.charCodeAt(i);
            hash = (hash << 5) - hash + char;
         }
         hashEmail = hash.toString(16);
      }
      //update analytics values
      if ($("#book-visit-analytics").length > 0) {
         var bookVisitValues = $("#book-visit-analytics").data().trackingFormInfoValue;
         var jsonBookVisitInfoStr = bookVisitValues.replace(/'/g, '"');
         var jsonBookVisitInfo = JSON.parse(jsonBookVisitInfoStr);
         jsonBookVisitInfo.hashedEmail = hashEmail;
         var updatedBookVisitValues = JSON.stringify(jsonBookVisitInfo).replace(/"/g, "'");
         $("#book-visit-analytics").attr("data-tracking-form-info-value", updatedBookVisitValues);
         $("#book-visit-analytics").trigger('click');
      }
    }

    function validateTaxCode(element) {
      const taxCode = element.find('#' + TAXCODE).val().trim();
      if (taxCode === '') {
        showError(TAXCODE+'-error');
      } else {
        hideError(TAXCODE);
      }
    }

    function validateEmail(element) {
      const emailValue = element.find('#' + EMAIL).val().trim();
      hideError(EMAIL);
      if (emailValue === '') {
        showError(EMAIL+'-error'+'-required');
      } else if (!isValidEmail(emailValue)) {
        showError(EMAIL+'-error'+'-invalid');
      } else {
        hideError(EMAIL);
      }
    }

    function validatePhone(element) {
      hideError(PHONE);
      const phoneValue = element.find('#' + PHONE).val().trim();
      if (phoneValue === '') {
        showError(PHONE+'-error');
      } else if(phoneValue.length < 10) {
        showError(PHONE+'-error'+'-invalid');
      }
      else {
        hideError(PHONE);
      }
    }

    function showError(inputId) {
      // const errorElement = $('<p>').addClass('form-error-message').text(message).attr('id', inputId + '-error');
      // $('#' + inputId).parent().append(errorElement);
      popUpBookingContent.find('#' + inputId).show();
      popUpBookingContent.find('#' + inputId).parent().addClass('error');
    }

    function hideError(inputId) {
      popUpBookingContent.find('#' + inputId).parent().removeClass('error');
      popUpBookingContent.find('#' + inputId).parent().find('.form-error-message').hide();
    }

    function isValidEmail(email) {
      const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
      return emailRegex.test(email);
    }

    // change selected date css
    function selectedDateCSS(el) {
      scheduleBookingDate.find('div').css('background-color', 'unset');
      scheduleBookingDate.find('div').css('color', '#616161');
      scheduleBookingDate.find('div').removeClass('enable');

      el.css('background-color', '#212121');
      el.css('color', '#fff');
      el.addClass('enable');
    }

    // set time schedule css
    function initTimeSchedule(el) {
      switch (dayOfWeek) {
        case 0:
          if (el.index() == 0) {
            unavailableAllTime();
          } else {
            oriTimeCSS();
          }
          break;
        case 5:
          if (el.index() == 0) {
            changeTimeCSS();
          } else {
            unavailableAllTime();
          }
          break;
        case 6:
          unavailableAllTime();
          break;
        default:
          if (el.index() == 0) {
            changeTimeCSS();
          } else {
            oriTimeCSS();
          }
          break;
      }
    }

    // change css for available time to select
    function changeTimeCSS() {
      const itemsList = scheduleBookingTime.find('.time-list .item');
      itemsList.each(function(i, element) {
        if($(element).find('.enable-time').length > 0){
          // tim kiem class parrent and add class enable
          $(scheduleBookingTime.find('.time-list .item')[i]).addClass('enable');
          $(scheduleBookingTime.find('.time-list .item')[i]).css('color', '#616161');
          $(scheduleBookingTime.find('.time-list .item img')[i]).attr('src', '/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg');
          $(scheduleBookingTime.find('.time-list .item svg path')[i]).attr('fill', '#ED1B24');
        } else {
          $(scheduleBookingTime.find('.time-list .item')[i]).css('color', '#a2a2a2');
          $(scheduleBookingTime.find('.time-list .item img')[i]).attr('src', '/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/grey-cheron-right-large.svg');
          $(scheduleBookingTime.find('.time-list .item')[i]).removeClass('enable');
        }
      });
    }

    // original css for time to select
    function oriTimeCSS() {
      for (let i = 0; i < timeList.length; i++) {
        $(scheduleBookingTime.find('.time-list .item')[i]).css('color', '#616161');
        $(scheduleBookingTime.find('.time-list .item img')[i]).attr('src', '/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-chevron-right.svg');
        $(scheduleBookingTime.find('.time-list .item')[i]).addClass('enable');
      }
    }

    // change css for unavailable time
    function unavailableAllTime() {
      $(scheduleBookingTime.find('.time-list .item')).css('color', '#a2a2a2');
      $(scheduleBookingTime.find('.time-list .item svg path')).attr('fill', '#a2a2a2');
      $(scheduleBookingTime.find('.time-list .item')).removeClass('enable');
    }

    function registerAddressClick(el) {
      popUpBookingContent.find('input[name="selectedBranch"]').val();
      el.find('.item').click(function () {
        popUpBookingContent.find('input[name="selectedBranch"]').val($(this).find('span').attr('itemid'));
        const serviceId = popUpBookingContent.find('input[name="selectedService"]').val();
        const branchId = popUpBookingContent.find('input[name="selectedBranch"]').val();
        showLoading();
        getBookingSlot(apiBookingSlot, serviceId, branchId).then((timeLots) => {
          bookingTimeRendering(timeLots.data);
          popUpBookingContent.find('.booking-container').removeClass('active');
          popUpBookingContent.find('.client').addClass('hidden');
          popUpBookingContent.find('.service').addClass('hidden');
          popUpBookingContent.find('.branch').addClass('hidden');
          popUpBookingContent.find('.schedule').css('display', 'block');
          popUpBookingContent.find('.schedule').addClass('active');
          popUpBookingContent.find('.form').addClass('hidden');
          hideLoading();
        }).catch ( error => {
          hideLoading();
          popUpBookingContent.find('.booking-container').removeClass('active');
          popUpBookingContent.find('.client').addClass('hidden');
          popUpBookingContent.find('.service').addClass('hidden');
          popUpBookingContent.find('.branch').addClass('hidden');
          popUpBookingContent.find('.schedule').css('display', 'block');
          popUpBookingContent.find('.schedule').addClass('active');
          popUpBookingContent.find('.form').addClass('hidden');
          const errorMess = `<span class='error-response'>${error.errorMessage}</span>`
          popUpBookingContent.find('.schedule .time .time-list').html(errorMess);
        });
      });
    }

    function showLoading() {
      popUpBookingContent.find('.loading').css('display', 'inline-block');
      popUpBookingContent.find('.loading_backdrop').css('display', 'block');
    }

    function hideLoading() {
      popUpBookingContent.find('.loading').css('display', 'none');
      popUpBookingContent.find('.loading_backdrop').css('display', 'none');
    }
  });
});

function getCityList(api) {
  api += 'atmCdmCityList';
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "get",
      success: function (response) {
        resolve(removeDuplicates(response?.data?.atmCdmFragmentList?.items, 'cityCode'));
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function getDistrictList(api, cityCode) {
  api += `atmCdmDistrictList;cityCode=${cityCode}`;
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "get",
      success: function (response) {
        resolve(removeDuplicates(response?.data?.atmCdmFragmentList?.items, 'districtCode'));
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function generateAtmCdmApi(api) {
  return `${api}atmcdmSearch`;
}

function getAtmCdmList(api, name, cityCode, districtCode, isCDM) {
  const apiUpdated = generateAtmCdmApi(api);
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: apiUpdated,
      method: "get",
      success: function (response) {
        let dataResponse = response?.data?.atmCdmFragmentList?.items ?? [];
        dataResponse = filterNameOrAddress(name, dataResponse, 'name');
        resolve(dataResponse);
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function getBranchList(api, apiSuffixes, search, lat, long, serviceId) {
  api += apiSuffixes ? apiSuffixes : '/_jcr_content.branchlist.json';
  const dataRequest = {
    "search": search ?? '',
    "lat": lat ?? 0,
    "long": long ?? 0,
    "srvIdent": serviceId ?? '',
  }
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(dataRequest),
      success: function (response) {
        let dataResponse = response?.branchList ?? [];
        dataResponse = filterNameOrAddress('', dataResponse, 'name');
        resolve({
          branches: dataResponse,
          cityList: response?.pstlAdr
        });
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function getServices(api, isPersonal) {
  api +='/_jcr_content.servicelist.json';
  const requestData = {
    "type": isPersonal? "11" : "12"  
  }
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(requestData),
      success: function (response) {
          resolve({
            data: response? getAllSubServiceInf(response) : []
          });
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function getBookingSlot(api, serviceId, branchId) {
  api +='/_jcr_content.branchslot.json';
  const requestData = 
  {
      "branchID": `${branchId}`,
      "srvID": `${serviceId}`
  }
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(requestData),
      success: function (response) {
          resolve({
            data: response?.slotList ?? []
          });
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function confirmSubmit(api, confirmInformation) {
  api +='/_jcr_content.confirmbooking.json';
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: api,
      method: "POST",
      contentType: 'application/json',
      data: JSON.stringify(confirmInformation),
      success: function (response) {
          resolve({
            data: response
          });
      },
      error: function (xhr, status, error) {
        reject(error)
      },
    });
  });
}

function filterDistrictsByCityCode(cityCd, branchAddress) {
  const cityData = branchAddress.find(item => item.city.cityCd === cityCd);
  return cityData? cityData.city.district : [];
}

function removeDuplicates(arr, prop) {
  if (!arr) {
    return [];
  }
  const objTmp = new Map();
  return arr.filter((item) => {
    const key = item[prop];
    if (!objTmp.has(key)) {
      objTmp.set(key, true);
      return true;
    }
    return false;
  });
}

function hideElementAfterAnimation(element) {
  element.each(function(index,item) {
    $(item).on('transitionend', function(event) {
      $(item).hide();
    })
  });
}

function clearForm(element) {
  const inputs = element.find('.input');
  inputs.each(function(index, item) {
    $(item).removeClass('error');
    $(item).find('input').val('');
    $(item).find('.form-error-message').hide();
  });
}

function formatDate(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() +1).padStart(2,'0');
  const day = String(date.getDate()).padStart(2,'0');
  return `${year}-${month}-${day}`;
}

function getTodayAndTomorrow() {
  const today = new Date();
  const tomorrow = new Date(today);
  tomorrow.setDate(today.getDate() +1);
  return {
    today: formatDate(today),
    tomorrow: formatDate(tomorrow)
  }
}
function getAllSubServiceInf(dataInput) {
  if(dataInput.servicesList) {
    return dataInput.servicesList
    .flatMap(service => service.subServiceInf || [])
    .filter(subService => subService !== null && typeof subService === 'object');
  }else {
    return [];
  }
}