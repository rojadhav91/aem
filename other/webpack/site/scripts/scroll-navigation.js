window.ScrollNavigation = class ScrollNavigation {
  wrapper;
  control;
  scroller;
  next;
  prev;
  constructor(element) {
    this.wrapper = element;
    this.init();
  }

  init() {
    const wrap = $(this.wrapper);
    this.scroller = wrap.find('.scroller')[0];
    this.prev = $('<div class="tcb-scroll-control tcb-scroll-control_prev"></div>')[0];
    this.next = $('<div class="tcb-scroll-control tcb-scroll-control_next"></div>')[0];
    this.control = $('<div class="tcb-scroll-controls"></div>')[0];
    $(this.control).append(this.prev);
    $(this.control).append(this.next);
    wrap.append(this.control);

    this.checkScrollable();

    $(this.scroller).on('scroll', this.checkScrollable.bind(this));

    $(window).on('resize', this.checkScrollable.bind(this));

    $(this.next).on('click', this.handleNext.bind(this));
    $(this.prev).on('click', this.handlePrev.bind(this));
  }

  checkScrollable() {
    if (this.scroller.scrollWidth > this.scroller.clientWidth) {
      if (this.scroller.scrollLeft > 0) {
        this.wrapper.classList.add('can-prev');
      } else {
        this.wrapper.classList.remove('can-prev');
      }
      if (this.scroller.scrollLeft < this.scroller.scrollWidth - this.scroller.clientWidth) {
        this.wrapper.classList.add('can-next');
      } else {
        this.wrapper.classList.remove('can-next');
      }
    }
  }

  handleNext() {
    const scrollWidth = (this.scroller.children[0] && this.scroller.children[0].clientWidth) + 24 || 0; // 24 is for gap
    $(this.scroller).animate(
      {
        scrollLeft: this.scroller.scrollLeft + scrollWidth,
      },
      300
    );
  }

  handlePrev() {
    const scrollWidth = (this.scroller.children[0] && this.scroller.children[0].clientWidth) + 24 || 0;
    $(this.scroller).animate(
      {
        scrollLeft: this.scroller.scrollLeft - scrollWidth,
      },
      300
    );
  }
};
