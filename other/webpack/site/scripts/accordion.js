$(document).ready(function() {
    let accordion = $('.accordion-component');

    accordion.each(function() {
        // Effects on open/close panels
        $(this).find('.card-product-feature__item').each(function() {
            $(this)
                .find('.card-product-feature__item__content__wrapper')
                .click(function() {
                    let description = $(this).parent().parent().find('.card-product-feature__item__description');
                    let descriptionText = description.find('.text');
                    let imgOn = $(this).parent().parent().find('.card-product-feature__item__img__on');
                    let imgOff = $(this).parent().parent().find('.card-product-feature__item__img__off');
                    let iconOn = $(this).find('.card-product-feature__item__content__icon__on');
                    let iconOff = $(this).find('.card-product-feature__item__content__icon__off');
                    let item = $(this).parent().parent();
                    let itemContainer = item.parent();
                    if (descriptionText.css('visibility') == 'hidden') {
                        description.css('max-height', itemContainer.height());
                        descriptionText.css('visibility', 'visible');
                        imgOff.hide();
                        imgOn.show();
                        iconOn.hide();
                        iconOff.show();
                        item.css('background', 'white');
                    } else {
                        description.css('max-height', 0);
                        imgOff.show();
                        imgOn.hide();
                        iconOff.hide();
                        iconOn.show();
                        item.css('background', '#f5f6f8');
                        descriptionText.css('visibility', 'hidden');
                    }
                });
        });
        // AA integration
        $(this).find('.card-product-feature__item__content__wrapper').each(function() {
            let accordionTitle = $(this).find('.card-product-feature__item__content__title').text();
            $(this).data("trackingClickInfoKey", "offer");
            $(this).attr("data-tracking-click-event", "accordions");
            $(this).attr("data-tracking-click-info-value", "{'accordionTitle' : '" + accordionTitle + "'}");
            $(this).attr(
                "data-tracking-web-interaction-value", "{'webInteractions': {'name': 'Accordion','type': 'other'}}"
            );

        });
    });
});