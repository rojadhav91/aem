$(document).ready(function () {
  $('.card-list')
    .not('.slick-initialized')
    .slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      infinite: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

  const input = $('.input-container').find('.search-input');
  const closeSearchIcon = $('.input-container').find('.close-icon');
  const tilteReason = $('.search-primary-container').find('.search-box_title');
  const keyWordList = $('.search-primary-container').find('.key-word-list');
  var historyKeyWord = JSON.parse(localStorage.getItem('historyKeyWord')) ?? [];

  if (!historyKeyWord.length) {
    tilteReason.hide();
  }

  historyKeyWord.forEach((key) => {
    keyWordList.append(renderKeywordList(key));
  });

  function renderKeywordList(item) {
    const resultSlug = keyWordList.attr('data-primary-search-result-slug');
    return `
      <div class="key-word-container">
        <a class="key-word-item" href="https://techcombank.com/${resultSlug}?q=${item}">${item}</a>
        <div class="close-icon">
          <img
            src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/icon-close-grey.svg" />
        </div>
      </div>
    `;
  }
  input.on('keyup', function () {
    if (input.val().length) {
      closeSearchIcon.css({ display: 'block' });
    } else {
      closeSearchIcon.css({ display: 'none' });
    }
  });

  input.on('keyup', function (e) {
    if (e.keyCode == 13) {
      const keyWord = e.target.value?.trim();
      if (!keyWord) {
        return;
      }

      const index = historyKeyWord.indexOf(keyWord);
      if (index < 0) {
        historyKeyWord.unshift(keyWord);
        if (historyKeyWord.length > 5) {
          historyKeyWord.pop();
        }
        localStorage.setItem('historyKeyWord', JSON.stringify(historyKeyWord));
      }

      const resultSlug = keyWordList.attr('data-primary-search-result-slug');
      window.location.href = `https://techcombank.com/${resultSlug}?q=${keyWord}`;
    }
  });

  closeSearchIcon.on('click', function () {
    $(this).css({ display: 'none' });
    input.val('');
  });

  const closeReasonIcon = $('.key-word-list').find('.close-icon');
  closeReasonIcon.each(function () {
    $(this).click(function () {
      const keyWord = $(this).closest('.key-word-container').find('.key-word-item').text();
      const index = historyKeyWord.indexOf(keyWord);
      if (index > -1) {
        historyKeyWord.splice(index, 1);
      }
      localStorage.setItem('historyKeyWord', JSON.stringify(historyKeyWord));
      $(this).parent().remove();
      if (keyWordList.children().length === 0) {
        tilteReason.hide();
      }
    });
  });

  const keyWordButton = $('.search-primary-container').find('.key-word-item');
  keyWordButton.each(function () {
    $(this).click(function () {
      const url = `https://techcombank.com/tim-kiem?q=${$(this).text()}`;
      window.location.href = url;
    });
  });
});
