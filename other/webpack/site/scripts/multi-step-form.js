$(document).ready(function () {
  $('.multi-step-form').each((_, formElement) => {
    const formId = formElement.id;
    // action event
    $(`a[id^=${formId}-step]`).each((_, actionElement) => {
      $(actionElement).on('click', () => {
        $('.multi-step-form .multi-step-form__step').each((_, stepElement) => {
          stepElement.id === actionElement.id
            ? stepElement.classList.add('active')
            : stepElement.classList.remove('active');
        });
        $('body').animate({ scrollTop: formElement.offsetTop }, 200);
      });
    });
  });
});
