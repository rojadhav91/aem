import { readPdfFile, renderYoutube } from './service-fee.js';
import moment from 'moment';
import _ from 'lodash';

let dateFormated = 'DD/MM/YYYY';

function sortDocuments(tabItem) {
  const documents = tabItem.find('.row').toArray();
  const sortedDocs = _.orderBy(documents, (o) => {
    return moment($(o).find('.date span').text(), dateFormated).format('YYYYMMDD');
  }, ['desc']);
  tabItem.empty();
  tabItem.append(sortedDocs);
}

function getYearRange(listViewElement) {
  const yearCheckboxs = listViewElement.find('.checkbox-list .year ul');
  const dates = listViewElement.find('.row .date span').map(function() { return $(this).text() }).get();
  const years = dates.map(e => moment(e, dateFormated).get('year')).filter(e => !isNaN(e));
  const minYear = Math.min(...years);
  const maxYear = Math.max(...years);
  const arr = Array.from({length: maxYear-minYear+1}, (_, i) => maxYear - i);
  yearCheckboxs.empty();
  arr.forEach(e => {
    yearCheckboxs.append(`
    <li>
      <label>
        <input type="checkbox" value="${e}" />
        <div>${e}</div>
      </label>
    </li>
    `);
  });
}

function regisSearchBoxes($searchBox, $searchBtn) {
  $searchBox.keypress(function (e) {
    let key = e.which;
    if(key == 13) { // the enter key code
       // trigger show/hide LVD Items by Filter
       showHideByFilter($searchBox.val());
     }
   });
  $searchBtn.on('click', function () {
    showHideByFilter($searchBox.val());
  });
}

function showHideByFilter(filterTxt) {
  // search across all LVDs on the page
  let $lvds = $('.list-view-documents');
  $lvds.each(function () {
    let $lvd = $(this);
    let $lvItems = $lvd.find('.list-items .row');
    $lvItems.each(function () {
      let $lvItem = $(this);
      let itemContent = $lvItem.find('.content p').html();
      if (!filterTxt || (filterTxt && itemContent && itemContent.indexOf(filterTxt) > -1)) {
        $lvItem.removeClass('filter-hide');
      } else {
        $lvItem.addClass('filter-hide');
      }
    });
  });
}

function setupFilter(element, placeHolderText) {
  let filterSelect = element.find('.select-checkbox-filter');
  let selectBtn = element.find('.select');
  let placeHolder = selectBtn.find('span')
  // desktop options
  let showedCheckbox = element.find('.checkbox-list');
  let options = filterSelect.find('.checkbox .type input');

  // mobile options
  let popupContent = filterSelect.find('.popup-content');
  let mobileOptions = popupContent.find('.type input');
  let mobileFilterContent = popupContent.find('.content');
  let applyButton = popupContent.find('.btn button');
  // variables
  const currentYear = new Date().getFullYear();
  let availableYears = showedCheckbox.find('.year input').map(function() { return $(this).val() }).get();
  let selectedYears = availableYears.slice(0, 1);
  let selectedQuaters = ['Q1', 'Q2', 'Q3', 'Q4'];
  let tmpYears = selectedYears;
  let tmpQuaters = selectedQuaters;

  const onOptionsChanged = (_options, oldYears) => {
    let yearOptions = _options.filter(function() { return $(this).parents('.year').length > 0; });
    let quarterOptions = _options.filter(function() { return $(this).parents('.quarter').length > 0; });

    if (oldYears.length == 0) {
      quarterOptions.prop('checked', true);
      quarterOptions.prop('disabled', false);
    }
    let newSelectedYears = yearOptions.filter(':checkbox:checked').map(function() { return $(this).val() }).get();
    if (newSelectedYears.length == 0) {
      quarterOptions.prop('checked', false);
      quarterOptions.prop('disabled', true);
    }
    let newSelectedQuarters = quarterOptions.filter(':checkbox:checked').map(function() { return $(this).val() }).get();
    return [newSelectedYears, newSelectedQuarters];
  }

  const cloneToMobile = () => {
    mobileFilterContent.empty();
    showedCheckbox.clone().appendTo(mobileFilterContent);
    mobileOptions = popupContent.find('.type input');
  }

  const updateFilters = () => {
    let allOptions = options.add(mobileOptions);
    allOptions.filter(function() { return $(this).parents('.year').length > 0 }).each(function() {
      $(this).prop('checked', false);
      if (selectedYears.includes($(this).val())) {
        $(this).prop('checked', true);
      }
    });
    allOptions.filter(function() { return $(this).parents('.quarter').length > 0 }).each(function() {
      $(this).prop('checked', false);
      if (selectedQuaters.includes($(this).val())) {
        $(this).prop('checked', true);
      }
    });
    if (selectedYears.length == 0) {
      placeHolder.text(placeHolderText);
    } else {
      placeHolder.text(selectedYears.join(','));
    }
  };

  updateFilters();
  cloneToMobile();
  // Desktop change options
  options.on('click', function() {
    let [years, quarters] = onOptionsChanged(options, selectedYears);
    selectedYears = years;
    selectedQuaters = quarters;
    element.trigger('updateFilter', [selectedYears, selectedQuaters]);
    if (selectedYears.length == 0) {
      placeHolder.text(placeHolderText);
    } else {
      placeHolder.text(selectedYears.join(','));
    }
  });

  // Mobile filter clicked
  selectBtn.on('click', function (e) {
    e.stopPropagation();

    if ($(window).width() > 767) {
      $(this).toggleClass('showed');
    } else {
      $(this).parent().toggleClass('mobile');
      $('body').addClass('overflow-hidden');

      updateFilters();
      tmpYears = selectedYears;
      tmpQuaters = selectedQuaters;
    }
  });

  $(document).on('click', function (event) {
    if (!$(event.target).is(showedCheckbox) && !$(event.target).parents().is(showedCheckbox)) {
      selectBtn.removeClass('showed');
    }

    if (!$(event.target).is(popupContent) && !$(event.target).parents().is(popupContent)) {
      selectBtn.parent().removeClass('mobile');
      $('body').removeClass('overflow-hidden');
    }
  });

  popupContent.on('click', (event) => {
    event.stopPropagation();
  });

  mobileOptions.on('click', function() {
    let [years, quarters] = onOptionsChanged(mobileOptions, tmpYears);
    tmpYears = years;
    tmpQuaters = quarters;
  });

  applyButton.on('click', function () {
    selectBtn.parent().removeClass('mobile');
    $('body').removeClass('overflow-hidden');
    selectedYears = tmpYears;
    selectedQuaters = tmpQuaters;
    updateFilters();
    element.trigger('updateFilter', [selectedYears, selectedQuaters]);
  });

  element.trigger('updateFilter', [selectedYears, selectedQuaters]);
}

$(document).ready(function () {
  if (document.documentElement.lang === 'en') {
    dateFormated = "DD MMM yyyy";
    let dateSpan = $('.row .date span');
    dateSpan.each(function () {
      const englishDate = moment($(this).html(),"DD/MM/yyyy").format(dateFormated);
      $(this).html(englishDate);
    });
  }
  let serviceFee = $('.list-view-documents');
  // configCheckboxInfoTable();
  serviceFee.each(function () {
    let menuService = $(this).find('.menu');
    let tabServiceFeeItem = $(this).find('.list-items .tab');
    let viewMore = $(this).find('.view-more');
    let viewMoreBtn = viewMore.find('.cta-button');
    const viewMoreNumber = Number($(this).attr('data-viewmore-number')) ?? 10;
    const filterPlaceholder = $(this).attr('data-filter-placeholder') ?? 'Select';
    let tabServiceState = 0;
    let selectedYears = null;
    let selectedQuaters = null;

    getYearRange($(this));
    tabServiceFeeItem.each(function() {
      sortDocuments($(this));
    });

    // click tab title
    menuService.on('click', '.tcb-tabs_item', function () {
      tabServiceState = $(this).data('tabindex');
      // initialSetup();

      tabServiceFeeItem.css('display', 'none');
      $(tabServiceFeeItem[tabServiceState]).css('display', 'block');
      if ($(tabServiceFeeItem[tabServiceState]).find('.row[data-hidden="false"]:hidden').length > 0) {
        viewMore.css('display', 'flex');
      } else {
        viewMore.css('display', 'none');
      }
    });

    setTimeout(() => {
      menuService.find('.tcb-tabs_item').addClass('md-ripples');
    });

    // add even listener

    let downloadBtn = $(this).find('.file-download .btn .link');
    let serviceFeePopup = $(this).find('.popup-download');
    let serviceFeePopupContent = serviceFeePopup.find('.popup-content');
    let closeBtn = serviceFeePopupContent.find('.close-btn');
    let linkDownloadBtn = serviceFeePopupContent.find('.foot a');

    downloadBtn.click(function (e) {
      $(this).parent().find('input').trigger("click")
      e.stopPropagation();
      let url = $(this).data('file-link');
      let type = $(this).data('file-type');
      switch (type) {
        case 'pdf':
          readPdfFile(serviceFeePopup, linkDownloadBtn, url);
          break;
        case 'seeMore':
          window.open(url, '_blank');
          break;
        case 'youtube':
          let title = $(this).parents('.row').find('.content p').text();
          renderYoutube(serviceFeePopup, title, url);
          break;
        default:
          window.open(url, '_blank');
          break;
      }
      return false;
    });

    let showedItem = [];
    let step = [];

    $(this).on('updateFilter', function(event, years, quarters) {
      selectedQuaters = quarters;
      selectedYears = years;
      initialSetup();
    });

    if ($(this).find('.select-options').length > 0) {
      setupFilter($(this), filterPlaceholder);
    } else {
      initialSetup();
    }

    // initial set up
    function initialSetup() {
      tabServiceFeeItem.each(function() {
        showedItem.push(viewMoreNumber);
        step.push(2 * viewMoreNumber - 1);
      })
      if (selectedYears && selectedQuaters) {
        tabServiceFeeItem.find('.row').each(function() {
          let date = moment($(this).find('.date span').text(), dateFormated);
          if (selectedYears.includes(`${date.year()}`) && selectedQuaters.includes(`Q${date.quarter()}`)) {
            $(this).attr('data-hidden', false);
          } else {
            $(this).attr('data-hidden', true);
          }
        });
      } else {
        tabServiceFeeItem.find('.row').attr('data-hidden', false);
      }

      tabServiceFeeItem.each(function () {
        $(this).find('.row[data-hidden="true"]').css('display', 'none');
        $(this).find('.row[data-hidden="false"]').each(function(i, e) {
          if (i < viewMoreNumber) {
            $(e).css('display', 'flex');
          } else {
            $(e).css('display', 'none');
          }
        });

        if ($(tabServiceFeeItem[tabServiceState]).find('.row[data-hidden="false"]:hidden').length > 0) {
        viewMore.css('display', 'flex');
      } else {
        viewMore.css('display', 'none');
      }
      });
    }

    // view more btn
    viewMoreBtn.click(function () {
      let rowItem = $(tabServiceFeeItem[tabServiceState]).find('.row[data-hidden="false"]');

      // Get the showedItem value of the current tab
      let tabShowedItem = 0;
      if (tabServiceState < showedItem.length) {
        tabShowedItem = showedItem[tabServiceState];
      }

      // Get the step value of the current tab
      let tabStep = 0;
      if (tabServiceState < step.length) {
        tabStep = step[tabServiceState];
      }

      if (tabStep >= rowItem.length) {
        for (tabShowedItem; tabShowedItem <= rowItem.length; tabShowedItem++) {
          $(rowItem[tabShowedItem]).css('display', 'flex');
        }

        viewMore.css('display', 'none');
      } else {
        for (tabShowedItem; tabShowedItem <= tabStep; tabShowedItem++) {
          $(rowItem[tabShowedItem]).css('display', 'flex');
        }
      }

      step[tabServiceState] = tabShowedItem + viewMoreNumber - 1;
    });

    closeBtn.click(function () {
      serviceFeePopup.css('display', 'none');
      $('body').removeClass('overflow-hidden');
    });

    $(document).click(function (event) {
      if (!$(event.target).is(serviceFeePopupContent) && !$(event.target).parents().is(serviceFeePopupContent)) {
        serviceFeePopup.css('display', 'none');
        $('body').removeClass('overflow-hidden');
      }
    });

    serviceFeePopupContent.on('click', (event) => {
      event.stopPropagation();
    });
  });

  // register for search box
  regisSearchBoxes($('input[trigger-search-to="list-view-document"]'), $('button[trigger-search-to="list-view-document"]'));
});
