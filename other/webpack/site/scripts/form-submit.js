var formTitle = $("body").attr("data-tracking-page-name");
$(document).ready(function () {
  $(".tcbformcontainer form.cmp-form").submit(function (event) {
    var currentObject = $(this);

    //form validation check
    event.preventDefault();
    if (!validateForm(event)) {
      errorAnalytics(currentObject);
      $("#form-error-analytics").trigger("click");
      return;
    }

    // capture form success analytics
    processAnalyticsFields(currentObject);

    //form submit ajax call : start
    var form = $(this);
    var ajaxPage = $(this).attr("action");
    var modalTargetId = $(form).attr("data-success-modalId");
    var failureModalTargetId = $(form).attr("data-failure-modalId");
    let targetModalId;
    $.ajax({
      url: ajaxPage,
      method: "POST",
      data: $(form).serialize(), // Serialize form data
      success: function (data) {
        var returnedData = JSON.parse(JSON.stringify(data));
        if (returnedData.Success == 200) {
          showModal(modalTargetId);
        }
        resetForm();
      },
      error: function (xhr, status, error) {
        showModal(failureModalTargetId);
      },
    });

    //form reset function
    function resetForm() {
      $(form).trigger("reset");
      $(form)
        .find(".dropdown-wrapper .dropdown-inputbase .dropdown-viewValue")
        .text("")
        .addClass("hidden");
      $(form)
        .find(".dropdown-wrapper .dropdown-inputbase .dropdown-placeholder")
        .removeClass("hidden");
      var siteKeyElement = document.getElementById("siteKey");
      if (siteKeyElement !== null && siteKeyElement.value !== "") {
        grecaptcha.reset();
      }
    }

    //form show modal box
    function showModal(modalId) {
      targetModalId = modalId;
      $(".modal .popup").each(function () {
        var modalID = $(this).attr("id");
        if (targetModalId === "#" + modalID) {
          // Check if the current div's class matches the class of the first div
          $(this).css("display", "flex");
        }
      });
    }
  });
  //form submit ajax call : end

  //dateClick function
  function dateClickAnalytics() {
    if ($(".tcbformcontainer form.cmp-form input").length > 0) {
      $(".tcbformcontainer form.cmp-form input").eq(0).trigger("click");
    }
  }

  // analytics for start filling the form : start
  $(".tcbformcontainer form.cmp-form input").one("click", function () {
    if ($(this).hasClass("datepicker-input")) {
      dateClickAnalytics();
    }
    var formStartDataValues = $(this)
      .parents(".tcb-form-wrapper")
      .find(".cmp-form")
      .data().trackingClickInfoValue;
    var submitBtn = $(this)
      .parents(".tcb-form-wrapper")
      .find(".cmp-form-button")
      .text()
      .trim();
    var webInteractionDataValues = $(this)
      .parents(".tcb-form-wrapper")
      .find(".cmp-form")
      .data().trackingWebInteractionValue;
    if (formStartDataValues) {
      var jsonStr = formStartDataValues.replace(/'/g, '"');
      var json = JSON.parse(jsonStr);
      json.formName = formTitle;
      var updatedValues = JSON.stringify(json).replace(/"/g, "'");
      var webInteractionStr = webInteractionDataValues.replace(/'/g, '"');
      var webInteractionjson = JSON.parse(webInteractionStr);
      webInteractionjson.webInteractions.name = submitBtn;
      var updatedWebInteractionValues = JSON.stringify(
        webInteractionjson
      ).replace(/"/g, "'");
      $(this)
        .parents(".tcb-form-wrapper")
        .find(".cmp-form")
        .attr("data-tracking-click-info-value", updatedValues);
      $(this)
        .parents(".tcb-form-wrapper")
        .find(".cmp-form")
        .attr(
          "data-tracking-web-interaction-value",
          updatedWebInteractionValues
        );
      $(this)
        .parents(".tcb-form-wrapper")
        .find(".cmp-form")
        .data("trackingWebInteractionValue", updatedWebInteractionValues);
    }
    var formStartClick = $(this);
    setTimeout(function () {
      $(formStartClick).removeAttr("data-tracking-click-event");
    }, 1000);
  });
  // analytics for start filling the form : end
});

// process Analytics Fields function
function processAnalyticsFields(currentObject) {
  var analyticsFields = [];
  var hasedEmailId = "";
  var formBranchName = "";
  $(currentObject)
    .find(".analytics-form-field input")
    .each(function () {
      if (this.type === "radio" || this.type === "checkbox") {
        if (this.checked) {
          var value = $(this).val().trim();
          if (value !== "") {
            analyticsFields.push(value);
          }
        }
      } else if ($(this).data("type") === "email") {
        var value = $(this).val().trim();
        if (value !== "") {
          let emailVal = value;
          const hashEmail = hashEmailValue(emailVal);
          value = hashEmail;
          hasedEmailId = hashEmail;
          analyticsFields.push(value);
        }
      } else {
        var value = $(this).val().trim();
        if (value !== "") {
          analyticsFields.push(value);
        }
      }
    });

  var analyticsFieldsString = analyticsFields.join(" | ");
  // Call the function to update the dataLayer object
  updateDataLayerObject(currentObject, analyticsFieldsString, hasedEmailId);
}

//update DataLayerObject
function updateDataLayerObject(
  currentObject,
  analyticsFieldsString,
  hasedEmailId
) {
  var submitBtn = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find(".cmp-form-button")
    .text()
    .trim();
  formBranchName = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find(".analytics-form-field .branch-list .dropdown-viewValue")
    .text()
    .trim();
  var formSubmitDataValues = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-submit-analytics")
    .data().trackingClickInfoValue;
  var webInteractionDataValues = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-submit-analytics")
    .data().trackingWebInteractionValue;

  if (formSubmitDataValues) {
    var jsonStr = formSubmitDataValues.replace(/'/g, '"');
    var json = JSON.parse(jsonStr);
    json.formName = formTitle;
    json.formDetails = analyticsFieldsString;
    json.hashedEmail = hasedEmailId;
    if (hasedEmailId !== "") {
      json.hashedEmail = hasedEmailId;
    } else {
      delete json.hashedEmail;
    }
    if (!$(".branch-list").hasClass("disabled") && formBranchName !== "") {
      json.branchLocation = formBranchName;
    } else {
      delete json.branchLocation;
    }
    var updatedValues = JSON.stringify(json).replace(/"/g, "'");

    var webInteractionStr = webInteractionDataValues.replace(/'/g, '"');
    var webInteractionjson = JSON.parse(webInteractionStr);
    webInteractionjson.webInteractions.name = submitBtn;
    var updatedWebInteractionValues = JSON.stringify(
      webInteractionjson
    ).replace(/"/g, "'");
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-submit-analytics")
      .attr("data-tracking-click-info-value", updatedValues);
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-submit-analytics")
      .attr("data-tracking-web-interaction-value", updatedWebInteractionValues);
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-submit-analytics")
      .data("trackingWebInteractionValue", updatedWebInteractionValues);
  }
  $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-submit-analytics")
    .trigger("click");
}

// covert hash email value for analytics
function hashEmailValue(emailVal) {
  let hashEmail = "";
  if (emailVal) {
    let hash = 0;
    for (let i = 0; i < emailVal.length; i++) {
      const char = emailVal.charCodeAt(i);
      hash = (hash << 5) - hash + char;
    }
    hashEmail = hash.toString(16);
  }
  return hashEmail;
}

// capture form error analytics
function errorAnalytics(currentObject) {
  var analyticsErrorFields = [];
  var analyticsDataErrorFields = [];
  var hashErrorEmailID;
  var radioErrorProcessed = false;
  var checkboxErrorProcessed = false;
  formBranchName = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find(".analytics-form-field .branch-list .dropdown-viewValue")
    .text()
    .trim();
  $(currentObject)
    .find(".analytics-form-field input")
    .each(function () {
      if (this.type === "radio") {
        if (!this.checked && !radioErrorProcessed) {
          var radioValue = $(this)
            .parents(".analytics-form-field")
            .find(".form-error-message")
            .text()
            .trim();
          analyticsErrorFields.push(radioValue);
          radioErrorProcessed = true;
        } else if (this.checked) {
          var value = $(this).val().trim();
          if (value !== "") {
            analyticsDataErrorFields.push(value);
          }
        }
      } else if (this.type === "checkbox") {
        if (!this.checked && !checkboxErrorProcessed) {
          var radioValue = $(this)
            .parents(".analytics-form-field")
            .find(".form-error-message")
            .text()
            .trim();
          analyticsErrorFields.push(radioValue);
          checkboxErrorProcessed = true;
        } else if (this.checked) {
          var value = $(this).val().trim();
          if (value !== "") {
            analyticsDataErrorFields.push(value);
          }
        }
      } else if ($(this).data("type") === "email") {
        var value = $(this).val().trim();
        if (value !== "") {
          let emailVal = value;
          const hashErrorEmail = hashEmailValue(emailVal);
          value = hashErrorEmail;
          hashErrorEmailID = hashErrorEmail;
        } else {
          var value = $(this)
            .parents(".analytics-form-field")
            .find(".form-error-message")
            .text()
            .trim();
          if (value !== "") {
            analyticsErrorFields.push(value);
          }
          hashErrorEmailID = "";
        }
      } else {
        var value = $(this)
          .parents(".analytics-form-field")
          .find(".form-error-message")
          .text()
          .trim();
        analyticsErrorFields.push(value);
        var inputValue = $(this).val().trim();
        if (inputValue !== "") {
          analyticsDataErrorFields.push(inputValue);
        }
      }
    });

  var analyticsErrorFieldsString = analyticsErrorFields
    .map((error) => error.replace(/\*/g, ""))
    .filter((error) => error !== "")
    .join(" | ");

  var analyticsDataErrorFieldsString = analyticsDataErrorFields.join(" | ");

  // update form error datalayer object
  var formErrorDataValues = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-error-analytics")
    .data().trackingClickInfoValue;
  var formInfoValues = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-error-analytics")
    .data().trackingFormInfoValue;
  var userInfoValues = $(currentObject)
    .parents(".tcb-form-wrapper")
    .find("#form-error-analytics")
    .data().trackingUserInfoValue;
  if (formErrorDataValues) {
    var jsonStr = formErrorDataValues.replace(/'/g, '"');
    var jsonFormInfoStr = formInfoValues.replace(/'/g, '"');
    var jsonUserInfoStr = userInfoValues.replace(/'/g, '"');
    var json = JSON.parse(jsonStr);
    var jsonFormInfo = JSON.parse(jsonFormInfoStr);
    var jsonUserInfo = JSON.parse(jsonUserInfoStr);
    json.errorMessage = analyticsErrorFieldsString;
    jsonFormInfo.formName = formTitle;
    jsonFormInfo.formDetails = analyticsDataErrorFieldsString;
    jsonFormInfo.hashedEmail = hashErrorEmailID;
    jsonFormInfo.branchLocation = formBranchName;
    jsonUserInfo.gaid = "test";
    var updatedValues = JSON.stringify(json).replace(/"/g, "'");
    var updatedFormValues = JSON.stringify(jsonFormInfo).replace(/"/g, "'");
    var updatedUserValues = JSON.stringify(jsonUserInfo).replace(/"/g, "'");
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-error-analytics")
      .attr("data-tracking-click-info-value", updatedValues);
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-error-analytics")
      .attr("data-tracking-form-info-value", updatedFormValues);
    $(currentObject)
      .parents(".tcb-form-wrapper")
      .find("#form-error-analytics")
      .attr("data-tracking-user-info", updatedUserValues);
  }
}
