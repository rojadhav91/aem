import 'slick-carousel';

$(document).ready(function () {
  var sectionFastAccess = $('section.section__fast-access');

  sectionFastAccess.each(function () {
    var fastAccessContent = $(this).find('.fast-access__contents');
    var fastAccessTable = fastAccessContent.find('.fast-access__table');
    var fastAccessTableHeaderItems = fastAccessTable.find('.table-header__items');
    var fastAccessHeaderUnderline = fastAccessTable.find('.header__underline');
    var headerButton = fastAccessTableHeaderItems.find('.header__button');
    var tableRecordsInner = fastAccessTable.find('.table-records__inner');
    var headerItemMobile = $(this).find('.header-item');
    var currencyPopup = $(this).find('.pop-up');
    var currencyPopupContent = currencyPopup.find('.pop-up-content');
    var currencyPopUpTitle = currencyPopup.find('.title h6');

    headerButton.each(function (i) {
      var itemTemp = $(this);
      if (i == 0) {
        $(this).addClass('header__selected');
        tableRecordsInner.each(function (j) {
          if (j == i) {
            $(this).addClass('record__show');
          }
        });
      }
      itemTemp.click(function () {
        headerButton.removeClass('header__selected');
        $(this).addClass('header__selected');
        headerButton.scrollCenter('.header__selected', 300);
        var headerTileWidth = headerButton.parent().width();
        tableRecordsInner.each(function (j) {
          $(this).removeClass('record__show');
          if (j == i) {
            $(this).addClass('record__show');
          }

          headerButton.css('border-bottom', 'unset');
          $(itemTemp).css('border-bottom', '4px solid #ed1c24');
        });
      });
    });

    if ($(window).width() <= 767) {
      headerItemMobile.click(function (e) {
        e.stopPropagation();
        showCurExPopUp($(this));
        $('body').addClass('overflow-hidden');
      });
    }

    $(window).resize(function () {
      if ($(window).width() <= 767) {
        headerItemMobile.click(function (e) {
          e.stopPropagation();
          showCurExPopUp($(this));
          $('body').addClass('overflow-hidden');
        });
      }
    });

    function showCurExPopUp(element) {
      // remove old content
      var oldContent = currencyPopupContent.find('.table-records__inner');
      oldContent.remove();

      // add new content
      currencyPopup.css('display', 'block');
      currencyPopUpTitle.text(element.find('span').text());
      $(tableRecordsInner[element.index()]).clone().appendTo(currencyPopupContent);
    }

    $(document).click(function (event) {
      if (!$(event.target).is(currencyPopupContent) && !$(event.target).parents().is(currencyPopupContent)) {
        currencyPopup.css('display', 'none');
        $('body').removeClass('overflow-hidden');
      }
    });

    jQuery.fn.scrollCenter = function (elem, speed) {
      var active = $(this).parent().find(elem); // find the active element
      var activeWidth = active.width() / 2; // get active width center

      var pos = active.position().left + activeWidth; //get left position of active li + center position
      var elpos = active.scrollLeft(); // get current scroll position
      var elW = active.width(); //get div width

      pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

      $(this).parent().animate({ scrollLeft: pos }, speed);

      return pos;
    };
  });
});

// List Card
$(document).ready(function () {
  $('.list-card').each(function () {
    if ($(window).width() <= 767) {
      $('.list-card__list-item').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: false,
        dots: true,
        infinite: false,
      });
    } else {
      $('.list-card__list-item').filter('.slick-initialized').slick('unslick');
    }
    $(window).on('resize', function () {
      if ($(window).width() <= 767) {
        $('.list-card__list-item').not('.slick-initialized').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false,
          autoplaySpeed: 2000,
          arrows: false,
          dots: true,
          infinite: false,
        });
      } else {
        $('.list-card__list-item').filter('.slick-initialized').slick('unslick');
      }
    });
  });
});
// End of List Card

// End Card Product Filter component

//Document download slider
$(document).ready(function () {
  $('.list-document-download').each(function () {
    if ($(window).width() <= 1334) {
      $('.list-item-document-download__body')
        .not('.slick-initialized')
        .slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: false,
          autoplaySpeed: 2000,
          arrows: true,
          dots: false,
          infinite: false,
          centerMode: false,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                arrows: false,
              },
            },
          ],
        });
    } else {
      $('.list-item-document-download__body').filter('.slick-initialized').slick('unslick');
    }
    $(window).on('resize', function () {
      if ($(window).width() <= 1334) {
        $('.list-item-document-download__body')
          .not('.slick-initialized')
          .slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            dots: false,
            infinite: false,
            centerMode: false,
            responsive: [
              {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  arrows: false,
                },
              },
            ],
          });
      } else {
        $('.list-item-document-download__body').filter('.slick-initialized').slick('unslick');
      }
    });
    $('.list-document-download .list-document-download__container .list-item-document-download__body .slick-track').css(
      'cssText',
      'margin-left: -20px;'
    );
    $('.list-item-document-download__body').on('afterChange', function (event, slick, currentSlide) {
      if (slick.$slides.length - 1 == currentSlide) {
        $(
          '.list-document-download .list-document-download__container .list-item-document-download__body .slick-track'
        ).css('cssText', 'margin-left: 0;');
      } else {
        $(
          '.list-document-download .list-document-download__container .list-item-document-download__body .slick-track'
        ).css('cssText', 'margin-left: -20px;');
      }
    });

    $('.list-item-document-download__body .item-document-download_content')
      .first()
      .addClass('radius-top-bottom-left-8');
    $('.list-item-document-download__body .item-document-download_content')
      .last()
      .addClass('radius-top-bottom-right-8');

    $('.list-item-document-download__body .slick-next').text('');
    $('.list-item-document-download__body .slick-prev').text('');

    $('.list-item-document-download__body .slick-prev').css('cssText', 'display: none !important');
    $('.list-item-document-download__body .slick-next').css('cssText', 'display: block !important');

    $('.list-item-document-download__body').on('afterChange', function (event, slick, currentSlide, nextSlide) {
      if ($(window).width() <= 1334 && $(window).width() >= 992) {
        if (currentSlide == 0) {
          $('.list-item-document-download__body .slick-prev').css('cssText', 'display: none !important');
          $('.list-item-document-download__body .slick-next').css('cssText', 'display: block !important');
        } else if (slick.$slides.length - 3 == currentSlide) {
          $('.list-item-document-download__body .slick-prev').css('cssText', 'display: block !important');
          $('.list-item-document-download__body .slick-next').css('cssText', 'display: none !important');
        } else {
          $('.list-item-document-download__body .slick-prev').css('cssText', 'display: block !important');
          $('.list-item-document-download__body .slick-next').css('cssText', 'display: block !important');
        }
      }
    });
  });
});

//end document download slider

// tab-vertical-qa Component
$(document).ready(function () {
  $('.tab-vertical-qa').each(function () {
    $(this)
      .find('.tab-vertical-qa__tab-control-btn')
      .click(function () {
        if ($(window).width() >= 992) {
          activeTab(this);
        } else {
          showpopup(this);
        }
      });
    if ($(window).width() < 992) {
      $('.tab-vertical-qa__tab-control-btn').removeClass('tab-active');
      $('.tab-vertical-qa .tab-vertical-qa__article-content').css('display', 'none');
    } else {
      activeTab($('.tab-vertical-qa__tab-control-btn:first-child'));
    }
    $(window).resize(function () {
      if ($(window).width() < 992) {
        $('.tab-vertical-qa__tab-control-btn').removeClass('tab-active');
        $('.tab-vertical-qa .tab-vertical-qa__article-content').css('display', 'none');
        $('.tab-vertical-qa .tab-vertical-qa__tab-content').css('display', 'none');
      } else {
        activeTab($('.tab-vertical-qa__tab-control-btn:first-child'));
        $('.tab-vertical-qa .tab-vertical-qa__tab-content').css('display', 'block');
      }
    });
    $(this)
      .find('.tab-vertical-qa__tab-content')
      .click(function () {
        $('.tab-vertical-qa__tab-content').hide();
      });
    $(this)
      .find('.tab-vertical-qa__modal-close')
      .click(function () {
        $('.tab-vertical-qa__article-content').hide();
        $('.tab-vertical-qa__tab').css('cssText', 'background-color:unset');
      });
    function activeTab(obj) {
      $('.tab-vertical-qa .tab-vertical-qa__tab-control-btn').removeClass('tab-active');
      $(obj).addClass('tab-active');
      var id = $(obj).data('tab-control');
      $('.tab-vertical-qa__article-content').css('display', 'none');
      $('.tab-vertical-qa__article-content').each(function () {
        if ($(this).data('tab-content') == id) {
          $(this).css('display', 'block');
        }
      });
    }
    function showpopup(obj) {
      $('.tab-vertical-qa .tab-vertical-qa__tab-control-btn').removeClass('tab-active');
      $(obj).addClass('tab-active');
      var id = $(obj).data('tab-control');
      $('.tab-vertical-qa__article-content').hide();
      $('.tab-vertical-qa__tab-content').hide();
      $('.tab-vertical-qa__article-content').each(function () {
        if ($(this).data('tab-content') == id) {
          $(this).show();
          $('.tab-vertical-qa__tab-content').show();
          $('.tab-vertical-qa__tab-content').css(
            'cssText',
            'background: #00000069; position: absolute; width: 100vw; height: 100vh; top: 0; left: 0; z-index: 2; max-width: unset;'
          );
        }
      });
    }
  });
});

// End tab-vertical-qa Component

//Tab Horizontal Report Component
$(document).ready(function () {
  $('.tab-horizontal-report').each(function () {
    $(this)
      .find('.tab-horizontal-report__tab-control-btn')
      .click(function () {
        activeTabHorizontalReport(this);
      });
    activeTabHorizontalReport($(this).find('.tab-horizontal-report__tab-control-btn:first-child'));

    function activeTabHorizontalReport(obj) {
      $(obj)
        .parent()
        .parent()
        .parent()
        .parent()
        .find('.tab-horizontal-report__tab-control-btn')
        .removeClass('tab-horizontal-report__tab-active');
      $(obj).addClass('tab-horizontal-report__tab-active');
      var id = $(obj).data('tab-control');
      $(obj)
        .parent()
        .parent()
        .parent()
        .parent()
        .find('.tab-horizontal-report__tab-content-card')
        .css('display', 'none');
      $('.tab-horizontal-report__tab-content-card').each(function () {
        if ($(this).data('tab-content') == id) {
          $(this).css('display', 'flex');
          if ($(this).find('.tab-horizontal__tab-content-item').length > 4) {
            $('.tab-horizontal__tab-content-list-item').filter('.slick-initialized').slick('unslick');
            $(this)
              .find('.tab-horizontal__tab-content-list-item')
              .not('.slick-initialized')
              .slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                accessibility: true,
                arrows: true,
                infinite: false,
                nextArrow: '<button class="slick-next slick-arrow slick-disabled"></button>',
                prevArrow: '<button class="slick-prev slick-arrow"></button>',
                responsive: [
                  {
                    breakpoint: 1335,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                      arrows: false,
                    },
                  },
                ],
              });
            $('.tab-horizontal .slick-prev').css('cssText', 'display: none !important');
            $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
            $('.tab-horizontal__tab-content-list-item').on(
              'afterChange',
              function (event, slick, currentSlide, nextSlide) {
                if ($(window).width() <= 1334) {
                  if (currentSlide == 0) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: none !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
                  } else if (currentSlide == 2) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: block !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: none !important');
                  } else {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: block !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
                  }
                } else {
                  if (currentSlide == 0) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: none !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
                  } else if (currentSlide == 1) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: block !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: none !important');
                  }
                }
              }
            );
          } else if ($(this).find('.tab-horizontal__tab-content-item').length == 4) {
            $('.tab-horizontal__tab-content-list-item').filter('.slick-initialized').slick('unslick');
            $('.tab-horizontal__tab-content-list-item').filter('.slick-initialized').slick('unslick');
            $(this)
              .find('.tab-horizontal__tab-content-list-item')
              .not('.slick-initialized')
              .slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                accessibility: true,
                arrows: true,
                infinite: false,
                nextArrow: '<button class="slick-next slick-arrow slick-disabled"></button>',
                prevArrow: '<button class="slick-prev slick-arrow"></button>',
                responsive: [
                  {
                    breakpoint: 1335,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    },
                  },
                  {
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                      arrows: false,
                    },
                  },
                ],
              });
            $('.tab-horizontal .slick-prev').css('cssText', 'display: none !important');
            $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
            $('.tab-horizontal__tab-content-list-item').on(
              'afterChange',
              function (event, slick, currentSlide, nextSlide) {
                if ($(window).width() <= 1334) {
                  if (currentSlide == 0) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: none !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: block !important');
                  } else if (currentSlide == 1) {
                    $('.tab-horizontal .slick-prev').css('cssText', 'display: block !important');
                    $('.tab-horizontal .slick-next').css('cssText', 'display: none !important');
                  }
                }
              }
            );
          } else if ($(this).find('.tab-horizontal__tab-content-item').length == 3) {
            $('.tab-horizontal__tab-content-list-item').filter('.slick-initialized').slick('unslick');
            $(this)
              .find('.tab-horizontal__tab-content-list-item')
              .not('.slick-initialized')
              .slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                accessibility: true,
                arrows: false,
                infinite: false,
                responsive: [
                  {
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    },
                  },
                ],
              });
          }
        }
      });
    }
  });
});

// Tab Slider component

//Card list Component
$(document).ready(function () {
  $('.card-list').each(function () {
    var cardListContainer = $(this).find('.card-list__container');
    var cardListItem = cardListContainer.find('.card-list__list-item');
    if ($(window).width() < 767 && !$(this).hasClass('card-list-stack') && !cardListItem.hasClass('digital')) {
      slickCardlist();
    } else if ($(window).width() < 767 && cardListItem.hasClass('digital')) {
      cardListItem.filter('.slick-initialized').slick('unslick');
    } else {
      $('.card-list__list-item').filter('.slick-initialized').slick('unslick');
    }
    $(window).resize(function () {
      if ($(window).width() < 767 && !$('.card-list').hasClass('card-list-stack')) {
        slickCardlist();
      } else {
        $('.card-list__list-item').filter('.slick-initialized').slick('unslick');
      }
    });

    $(window).resize(function () {
      if ($(window).width() < 767 && cardListItem.hasClass('digital')) {
        cardListItem.filter('.slick-initialized').slick('unslick');
      }
    });

    function slickCardlist() {
      $('.card-list__list-item').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: false,
        dots: true,
        infinite: false,
      });
    }
  });
});
// End of Card list Component

// Tab Vertical Features component
$(document).ready(function () {
  var tabVerticalFeature = $('.tab-vertical-features-component');

  tabVerticalFeature.each(function () {
    var menuItem = $(this).find('.menu .menu-item');
    var tabItem = $(this).find('.list-items .tab');
    var viewMoreTabVertical = $(this).find('.view-more');
    var tabVerticalState = 0;

    // set up first state
    initialSetup();

    // click menu
    menuItem.click(function () {
      if ($(window).width() >= 992) {
        menuItem.css('border-bottom', '1px solid rgb(222, 222, 222)');
      } else {
        menuItem.css('border-bottom', 'none');
      }
      menuItem.css('color', 'rgb(97, 97, 97)');
      menuItem.css('font-weight', 'unset');

      $(this).css('border-bottom', '4px solid rgb(237, 27, 36)');
      $(this).css('color', 'black');
      $(this).css('font-weight', 'bolder');

      tabItem.css('display', 'none');
      if ($(window).width() >= 992) {
        $(tabItem[$(this).index()]).css('display', 'grid');
      } else {
        $(tabItem[$(this).index()]).css('display', 'flex');
      }

      tabVerticalState = $(this).index();

      initialSetup();

      if ($(window).width() < 992) {
        menuItem.scrollCenter($(this), 300);
      }
    });

    // ripple clicked effect
    function ripple(e) {
      // Setup
      var posX = this.offsetLeft;
      var posY = this.offsetTop;
      var buttonWidth = this.offsetWidth;
      var buttonHeight = this.offsetHeight;

      // Add the element
      var ripple = document.createElement('span');
      ripple.classList.add('ripple-btn-tab-vertical');
      this.appendChild(ripple);

      // Make it round!
      if (buttonWidth >= buttonHeight) {
        buttonHeight = buttonWidth;
      } else {
        buttonWidth = buttonHeight;
      }

      // Get the center of the element
      var x = e.pageX - posX - buttonWidth / 2;
      var y = e.pageY - posY - buttonHeight / 2;

      ripple.style.width = `${buttonWidth}px`;
      ripple.style.height = `${buttonHeight}px`;
      ripple.style.top = `${y}px`;
      ripple.style.left = `${x}px`;

      if ($(window).width() >= 992) {
        ripple.classList.add('ripple-animation-tab-vertical');
      }
      setTimeout(() => {
        this.removeChild(ripple);
      }, 1000);
    }

    // add even listener
    menuItem.on('click', ripple);

    var i = 3;
    var step = i + 2;

    // initial set up
    function initialSetup() {
      i = 3;
      step = i + 2;

      tabItem.each(function () {
        if ($(window).width() < 992) {
          $(this).find('.item:nth-child(n+4)').css('display', 'none');

          if ($(tabItem[tabVerticalState]).find('.item').length > 3) {
            viewMoreTabVertical.css('display', 'block');
          } else {
            viewMoreTabVertical.css('display', 'none');
          }
        } else {
          $(this).find('.item').css('display', 'flex');
          viewMoreTabVertical.css('display', 'none');
        }
      });

      // resize
      $(window).resize(function () {
        tabItem.each(function () {
          if ($(window).width() < 992) {
            $(tabItem[tabVerticalState]).css('display', 'flex');
            $(this).find('.item:nth-child(n+4)').css('display', 'none');

            if ($(tabItem[tabVerticalState]).find('.item').length > 3) {
              viewMoreTabVertical.css('display', 'block');
            } else {
              viewMoreTabVertical.css('display', 'none');
            }
          } else {
            $(this).find('.item').css('display', 'flex');
            viewMoreTabVertical.css('display', 'none');
            $(tabItem[tabVerticalState]).css('display', 'grid');
          }
        });
      });
    }

    // view more btn
    viewMoreTabVertical.click(function () {
      var listItem = $(tabItem[tabVerticalState]).find('.item');

      if (step >= listItem.length) {
        for (i; i < listItem.length; i++) {
          listItem[i].style.display = 'flex';

          if ((i = listItem.length)) {
            viewMoreTabVertical.css('display', 'none');
          }
        }
      } else {
        for (i; i <= step; i++) {
          listItem[i].style.display = 'flex';
        }
      }
      step = i + 2;
    });

    // scroll center function
    jQuery.fn.scrollCenter = function (elem, speed) {
      var activeWidth = elem.width() / 2; // get active width center

      var pos = elem.position().left + activeWidth; //get left position of active li + center position
      var elpos = elem.scrollLeft(); // get current scroll position
      var elW = elem.width(); //get div width

      pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

      $(this).parent().animate({ scrollLeft: pos }, speed);

      return pos;
    };
  });
});

// card-center-container mid long term loan
$(document).ready(function () {
  var cardCenterContainer = $('section.card-center-container');
  var cardCenterContainerLarge = cardCenterContainer.find('.large');
  var cardCenterListCard = cardCenterContainerLarge.find('.card-center-listcard');

  //Adobe: return if element not found
  if (cardCenterListCard.length == 0) return;

  if (cardCenterListCard.hasClass('no-slick-mobile')) {
    cardCenterListCard.not('.slick-initialized').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      arrows: true,
      dots: true,
      infinite: false,
      prevArrow:
        '<div data-role="none" class="slick-arrow slick-prev"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
      nextArrow:
        '<div data-role="none" class="slick-arrow slick-next"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
      responsive: [
        {
          breakpoint: 1201,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 768,
          settings: 'unslick',
        },
      ],
    });
  } else {
    cardCenterListCard.not('.slick-initialized').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: false,
      arrows: true,
      dots: true,
      infinite: false,
      prevArrow:
        '<div data-role="none" class="slick-arrow slick-prev"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
      nextArrow:
        '<div data-role="none" class="slick-arrow slick-next"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
      responsive: [
        {
          breakpoint: 1201,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          },
        },
      ],
    });
  }
  $(window).resize(function () {

    //Adobe: return if element not found
    if (cardCenterListCard.length == 0) return;

    if (cardCenterListCard.hasClass('no-slick-mobile')) {
      cardCenterListCard.not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        dots: true,
        infinite: false,
        prevArrow:
          '<div data-role="none" class="slick-arrow slick-prev"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
        nextArrow:
          '<div data-role="none" class="slick-arrow slick-next"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
        responsive: [
          {
            breakpoint: 1201,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 768,
            settings: 'unslick',
          },
        ],
      });
    } else {
      cardCenterListCard.not('.slick-initialized').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        dots: true,
        infinite: false,
        prevArrow:
          '<div data-role="none" class="slick-arrow slick-prev"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
        nextArrow:
          '<div data-role="none" class="slick-arrow slick-next"><svg focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z"></path></svg></div>',
        responsive: [
          {
            breakpoint: 1201,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
            },
          },
        ],
      });
    }
  });
});

// table component
$(document).ready(function () {
  var tableCMP = $('.table-container');

  if ($(window).width() <= 767) {
    tableMoveContent();
  } else {
    tableCMP.find('.moved-content').remove();
  }

  $(window).resize(function () {
    if ($(window).width() <= 767) {
      tableMoveContent();
    } else {
      tableCMP.find('.moved-content').remove();
    }
  });

  function tableMoveContent() {
    tableCMP.each(function () {
      var movedContent = $(this)
        .find('.no-scroll .table-body-row:first-child .table-cell-innercontent p')
        .last()
        .text();
      $(this).find('.moved-content').remove();
      $(this)
        .find('.no-scroll .table-cell-innercontent>div>div:last-child p')
        .before('<p class="moved-content">' + movedContent + '</p>');
    });
  }
});

// ripple clicked effect
function ripple(e) {
  // Setup
  var posX = this.offsetLeft;
  var posY = this.offsetTop;
  var buttonWidth = this.offsetWidth;
  var buttonHeight = this.offsetHeight;

  // Add the element
  var ripple = document.createElement('span');
  ripple.classList.add('ripple-btn-service-fee');
  this.appendChild(ripple);

  // Make it round!
  if (buttonWidth >= buttonHeight) {
    buttonHeight = buttonWidth;
  } else {
    buttonWidth = buttonHeight;
  }

  // Get the center of the element
  var x = e.pageX - posX - buttonWidth / 2;
  var y = e.pageY - posY - buttonHeight / 2;

  ripple.style.width = `${buttonWidth}px`;
  ripple.style.height = `${buttonHeight}px`;
  ripple.style.top = `${y}px`;
  ripple.style.left = `${x}px`;

  // if ($(window).width() >= 992) {
  ripple.classList.add('ripple-animation-service-fee');
  // }
  setTimeout(() => {
    this.removeChild(ripple);
  }, 1000);
}

// Select Filter
$(document).ready(function () {
  var selectFilter = $('.select-filter');

  selectFilter.each(function () {
    var select = $(this).find('.select');
    var options = $(this).find('.options li');
    var oriSelectedText = select.find('span').text();
    var targetOption = $(this).find('.options>div');

    // show options to select
    select.click(function (e) {
      e.stopPropagation();
      $(this).toggleClass('showed');
    });

    // change placeholder
    options.click(function () {
      select.removeClass('showed');
      options.removeClass('selected');
      $(this).addClass('selected');

      if ($(this).index() == 0) {
        select.find('span').text(oriSelectedText);
      } else {
        select.find('span').text($(this).text());
      }
    });

    $(document).click(function (event) {
      if (!$(event.target).is(targetOption) && !$(event.target).parents().is(targetOption)) {
        select.removeClass('showed');
      }
    });
  });
});

// Floating Banner
$(document).ready(function () {
  $('.floating-banner').each(function () {
    $(this)
      .find('.floating-banner__btn-close')
      .click(function () {
        $(this).parent().parent().parent().remove();
      });
    setTimeout(function () {
      $('.floating-banner').remove();
    }, $(this).find('.floating-banner__container').attr('auto-hide') * 1000);
  });
});
// End of Floating Banner

// Scroll to top
$(document).ready(function () {
  const scrollToTopButton = $('.scroll-to-top');
  $('body').scroll(function () {
    if (document.body.scrollTop > 300) {
      scrollToTopButton.addClass('show');
    } else {
      scrollToTopButton.removeClass('show');
    }
  });

  scrollToTopButton.click(function () {
    $('html, body').animate({ scrollTop: 0 }, 500);
  });
});
// End of Scroll to top
// End of 

// calendar and real-estate-calc general code
$(document).ready(function () {
  $('body').on('click', function (event) {
    // if you click somewhere else not calendar-popup or not calendar-icon, it will hide the calendar-popup
    // if you click somewhere else not time__dropdown or not time-select, it will hide the time__dropdown
    // if you click somewhere else not modal active or not button-link, it will hide the modal
    if (
      $(event.target).parents('.calendar-popup').length == 0 &&
      $(event.target).parents('.ui-datepicker-header').length == 0 &&
      $(event.target).parents('.date-time-wrapper__input-extra').length == 0 &&
      !$(event.target).is('.calulator-analytics') &&
      !$(event.target).is('.calendar__input-field') &&
      $(event.target).parents('.calendar__input-field').length == 0
    ) {
      $('body').removeClass('calendar-showing');
      $('.calendar-popup.active').removeClass('active');
      $('.calendar_real_estate').removeClass('no-pointer');
    }
    if ($(event.target).parents('.time__dropdown').length == 0 && $(event.target).parents('.time-select').length == 0) {
      $('body').removeClass('time-showing');
      $('.time__dropdown.active').removeClass('active');
      $('.time-select .header-select__data-input').removeClass('no-pointer');
    }
    if (
      !$(event.target).is('.content-button__link.loan-calc') &&
      $(event.target).parents('.modal.active').length == 0
    ) {
      $('body').removeClass('overflow-hidden');
      $('.modal.active').removeClass('active');
    }
  });
});

const filterButton = $('.news_open-filter-button');
if (filterButton.length) {
  $(document.body).on('scroll', () => {
    if (filterButton.offset().top < 0 ) {
      filterButton.addClass('sticky');
    } else {
      filterButton.removeClass('sticky');
    }
  });
}

// accordion inspire
$(document).ready(function () {
  var accordionInspire = $('.accordion-inspire-component');

  accordionInspire.each(function () {
    var accordion = $(this);
    var itemTitle = accordion.find('.item-title');

    itemTitle.each(function () {
      $(this).click(function () {
        var itemDesc = $(this).parent();

        accordion.find('.item').not(itemDesc).addClass('hide');
        accordion.find('.item').not(itemDesc).removeClass('show');
        itemDesc.toggleClass('hide');
        itemDesc.toggleClass('show');
      });
    });

  });
});

// all-awards.html
let currentTab = '';
let currentSelectedTab = 0;
$(`.tab-item`).addClass('hidden', true);
const handleTabClick = (tabIndex, tab, element) => {
  const tabs = $(element).closest(".tabs-control-wrapper").find(".tab-item");
  var tabsControlIndex = $(tabs[0]).attr("tabs-control-index");
  if ($(`.tab-item`).length) {
    if (tab === currentTab) {
      return;
    }
    $(`:not(.hidden)[tabs-control-index="${tabsControlIndex}"]`).addClass('hidden', true);
    currentTab = tab;
    $(`[data-year="${tab}"]`).removeClass('hidden');
  } 
  else if ($('.tab-horizontal-report__tab-content-card').length) {
    const tabs = $('.tab-horizontal-report__tab-content-card');
    tabs[currentSelectedTab].classList.add('hidden');
    currentSelectedTab = tabIndex;
    tabs[currentSelectedTab].classList.remove('hidden');
  }
};

if ($('.tcb-tabs').length) {
  for (let i = 0; i < $('.tcb-tabs').length; ++i) {
    new TcbTab($('.tcb-tabs')[i], handleTabClick);
  }
}

// other-events.html; update.html
let openFilterButton = $('.news_open-filter-button');
if (openFilterButton.length) {
  let title = openFilterButton.attr('data-title');
  let action = openFilterButton.attr('data-action');
}

const typeSection = $('#newsTypes');
const typeSectionOther = $('#newsTypes-1');
const checkboxes = typeSection.find('input[type="checkbox"]');
const checkboxesOther = typeSectionOther.find('input[type="checkbox"]');
const checkboxAll = $('#allTypePost');
const checkboxAllOther = $('#allTypePost-1');

window.toggleAll = (event) => {
  const checked = event.target.checked;
  checkboxes.prop('checked', checked);
  checkboxesOther.prop('checked', checked);
};

window.typeCheckboxChange = (event) => {
  const checked = event.target.checked;
  if (!checked) {
    checkboxAll.prop('checked', false);
  } else {
    var all = true;
    checkboxes.each((index, element) => {
      if (index !== 0 && !element.checked) {
        all = false;
      }
    });
    checkboxAll.prop('checked', all);
    checkboxAllOther.prop('checked', all);
  }
  checkboxesOther.each((i, e) => {
    if(event.target.name == e.name) {
      e.checked = event.target.checked;
    }
  });
};

window.typeCheckboxChangeOther = (event) => {
  const checked = event.target.checked;
  if (!checked) {
    checkboxAllOther.prop('checked', false);
  } else {
    var all = true;
    checkboxesOther.each((index, element) => {
      if (index !== 0 && !element.checked) {
        all = false;
      }
    });
    checkboxAll.prop('checked', all);
    checkboxAllOther.prop('checked', all);
  }
  checkboxes.each((i, e) => {
    if(event.target.name == e.name) {
      e.checked = event.target.checked;
    }
  });
};

window.toggleRadios = (groupName,selectedRadio) => {
  var radios = document.querySelectorAll('input[name="' + groupName + '"]');
  for (var i = 0; i < radios.length; i++) {
    if(selectedRadio.value === radios[i].value) {
      radios[i].checked = true;
    } 
  }
};

window.toggleRadios = (groupName,selectedRadio) => {
  var radios = document.querySelectorAll('input[name="' + groupName + '"]');
  for (var i = 0; i < radios.length; i++) {
    if(selectedRadio.value === radios[i].value) {
      radios[i].checked = true;
    } 
  }
};


// TCB modal for grid pagination


new GridPagination($('.tcb-grid-card_list'), '.tcb-grid-card_item');

