import { createChart } from 'lightweight-charts';

const formatPrice = (price) => {
  return parseFloat(price)
    .toFixed(2)
    .replace(/\d(?=(\d{3})+\.)/g, '$&,');
};

const formatPriceChart = (price) => {
  const val = price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  return val.substr(0, val.length - 3);
};

function percentage(percent, total) {
  return ((percent * 100) / total)?.toFixed(2);
}

const STOCK_CHART_HEIGHT = 300;
const STOCK_CHART_WIDTH = null;
const chartConstants = {
  textColor: '#616161',
  priceScaleBorderColor: '#dedede',
  timeScaleBorderColor: '#dedede',
  gridVertLinesColor: '#ffffff',
  areaSeriesTopColor: '#84c1ff',
  areaSeriesBottomColor: '#f5faff',
  areaSeriesLineColor: '#0A84FF',
  areaSeriesPriceLineColor: '#000000',
  areaSeriesLastPriceLineColor: 'rgba(33, 33, 33, 0)',
};

const stockInformation = $('.stock-information');
const asOfLabel = stockInformation.attr('data-current-date-label');
const asOfDate = $('.stock-dateLastPrice');
const currentUrl = window.location.href;

let lastPrice,
  priceWithPercent,
  percent,
  isTallestPrice = false;
let dataChart;

document.addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'hidden') {
    initChart();
  }
});

const initChart = () => $.ajax({
  url: stockInformation.data('url'),
  type: 'GET',
  dataType: 'json',
  success: function (response) {
    dataChart = response.data.map(({ tradingDate, close }) => ({
      time: new Date(tradingDate).toISOString().substr(0, 10),
      value: close !== null ? close : null,
    }));

    if (dataChart && dataChart.length) {
      lastPrice = parseFloat(dataChart[dataChart.length - 1].value);
      if (dataChart.length > 1) {
        let yesterdayPrice = parseFloat(dataChart[dataChart.length - 2].value);
        if (lastPrice !== yesterdayPrice) {
          if (lastPrice > yesterdayPrice) priceWithPercent += `+`;
          priceWithPercent = formatPrice(lastPrice - yesterdayPrice);
          percent = percentage(lastPrice - yesterdayPrice, yesterdayPrice);
          priceWithPercent += ` (${(percent > 0 && '+') || ''}${percent}%)`;
        }
      }
      isTallestPrice =
        dataChart.filter((item) => item.value > lastPrice).length === 0;
    }

    
    let locale = 'en';
    if (currentUrl.includes('vi')) {
      locale = 'vi';
    }

    const stockPrice = stockInformation.find('.stock-information_price .value');

    const stockChange = stockInformation.find(
      '.stock-information_price .stock-information_changes'
    );

    stockPrice.text(formatPrice(lastPrice));

    stockChange.text(priceWithPercent);

    if (percent < 0) {
      stockInformation
        .find('.stock-information_price .tcb-icon')
        .addClass('decrease');
      stockChange.css('color', '#ff3a2f');
    } else {
      stockInformation
        .find('.stock-information_price .tcb-icon')
        .addClass('increase');
    }

    const formatDate = (timestamp, locale) => {
      const time = new Date(timestamp);
      return `${time.getDate()} ${time.toLocaleString(locale || 'default', {
        month: locale === 'vi' ? 'long' : 'short',
      })} ${time.getFullYear()}`;
    };
    asOfDate.text(asOfLabel + ' ' + formatDate(new Date(), locale));

    const chartWrapper = $('#stock-chart');
    if (chartWrapper.length) {
      const toolTip = document.createElement('div');
      toolTip.className = 'stock-chart_floating-tooltip';
      chartWrapper.append(toolTip);
      if ($('.tv-lightweight-charts').length === 0) {
        const chart = createChart(chartWrapper[0], {
          width: STOCK_CHART_WIDTH,
          height: STOCK_CHART_HEIGHT,
          layout: {
            background: { type: 'solid', color: '#ffffff' },
            textColor: '#616161',
          },
          grid: {
            vertLines: {
              visible: true,
              color: chartConstants.gridVertLinesColor,
            },
            horzLines: {
              visible: false,
            },
          },
          localization: {
            locale: locale,
          },
  
          rightPriceScale: {
            scaleMargins: {
              top: 0.1,
              bottom: 0.05,
            },
            borderColor: chartConstants.priceScaleBorderColor,
            ticksVisible: true,
          },
          handleScroll: false,
          handleScale: false,
          timeScale: {
            borderColor: chartConstants.timeScaleBorderColor,
            timeVisible: true,
            secondsVisible: true,
            rightOffset: -0.6,
            ticksVisible: true,
          },
          crosshair: {
            horzLine: {
              visible: false,
              labelVisible: false,
            },
            vertLine: {
              visible: false,
              labelVisible: false,
            },
          },
        });
  
        const toolTipWidth = 80;
        const toolTipHeight = 80;
        const toolTipMargin = 10;
  
        // update tooltip
        chart.subscribeCrosshairMove((param) => {
          if (
            !param.time ||
            param.point.x < 0 ||
            param.point.x > chartWrapper.width() ||
            param.point.y < 0 ||
            param.point.y > STOCK_CHART_HEIGHT
          ) {
            toolTip.style.display = 'none';
            return;
          }
          const inputDate = param.time;
          const parsedDate = new Date(inputDate);
          const day = parsedDate.getDate().toString().padStart(2, '0');
          const month = (parsedDate.getMonth() + 1).toString().padStart(2, '0');
          const year = parsedDate.getFullYear();
          const formattedDate = day + '/' + month + '/' + year;
          toolTip.style.display = 'block';
          const seriesData = param.seriesData.get(areaSeries);
          const price =
            seriesData.value !== undefined ? seriesData.value : seriesData.close;
          toolTip.innerHTML =
            '<div class="stock-chart_total-price">' +
            formatPrice(price) +
            '</div>' +
            '<div class="stock-chart_tooltip-date">' +
            formattedDate +
            '</div>';
  
          const coordinate = areaSeries.priceToCoordinate(price);
  
          let shiftedCoordinate = param.point.x - 35;
          if (coordinate === null) {
            return;
          }
          shiftedCoordinate = Math.max(
            0,
            Math.min(
              chartWrapper[0].clientWidth - toolTipWidth,
              shiftedCoordinate
            )
          );
          const coordinateY =
            coordinate - toolTipHeight - toolTipMargin > 0
              ? coordinate - toolTipHeight - toolTipMargin
              : Math.max(
                  0,
                  Math.min(
                    chartWrapper[0].clientHeight - toolTipHeight - toolTipMargin,
                    coordinate + toolTipMargin
                  )
                );
          toolTip.style.left = shiftedCoordinate + 'px';
          toolTip.style.top = coordinateY + 'px';
        });
  
        chart.timeScale().fitContent();
        chart.timeScale().scrollToPosition(-1, false);
        const areaSeries = chart.addAreaSeries({
          topColor: chartConstants.areaSeriesTopColor,
          bottomColor: chartConstants.areaSeriesBottomColor,
          lineColor: chartConstants.areaSeriesLineColor,
          lineWidth: 3,
        });
  
        areaSeries.applyOptions({
          priceFormat: {
            type: 'custom',
            formatter: formatPriceChart,
          },
          lastValueVisible: false,
          priceLineColor: chartConstants.areaSeriesPriceLineColor,
          priceLineStyle: 2,
          priceLineWidth: 1,
        });
  
        areaSeries.createPriceLine({
          price: lastPrice,
          color: chartConstants.areaSeriesLastPriceLineColor,
          lineWidth: 1,
          lineStyle: 2,
          axisLabelVisible: true,
        });
  
        areaSeries.setData(dataChart);
      }
      
    }
  },
  error: function (error) {
  },
});

initChart();
