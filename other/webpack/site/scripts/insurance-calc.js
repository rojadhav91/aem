import './currency-helper';
import CurrencyHelper from './currency-helper';
$(document).ready(function () {
  $('.insurance-gain').each(function() {
    let insuarnceGain = $(this);
    var insuranceValue = []
    try {
      insuranceValue = eval(insuarnceGain.attr('data-insurance-gain')) ?? [];
    } catch {

    }

    var ageValidate, defaulAgeValidation =  {
      insuranceSL: { min: 0, max: 69},
      insuranceAG: { min: 18, max: 50},
      insuranceBL: { min: 0, max: 65},
    };
    try {
      ageValidate = eval(insuarnceGain.attr('data-age-validate')) ?? defaulAgeValidation;
    } catch {
      ageValidate = defaulAgeValidation;
    }

    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();

    function numberWithComma(x) {
      return x
        .toString()
        .replaceAll(',', '')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function numberWithDot(x) {
      return x
        .toString()
        .replaceAll('.', '')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

    insuarnceGain.find('.date-time-wrapper__input').keypress(function (e) {
      var charCode = e.which ? e.which : e.keyCode;
      if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
        return false;
      }
      var preValue = e.target.value;
      var currentValue = String.fromCharCode(charCode);
      var finalValue = preValue+currentValue; 
      if(e.target.getAttribute('name') == 'day' && finalValue > 31 && finalValue.length < 3) {
        return false;
      }
      if(e.target.getAttribute('name') == 'month' && finalValue > 12 && finalValue.length < 3) {
        return false;
      }
      if(e.target.getAttribute('name') == 'year' && finalValue > currentYear && !(e.target.selectionStart == 0) && finalValue.length < 5) {
        return false;
      }
    });

    function calcAge() {
      var day = insuarnceGain.find(".date-time-wrapper__input[name='day']").val();
      var month = insuarnceGain.find(".date-time-wrapper__input[name='month']").val();
      var year = insuarnceGain.find(".date-time-wrapper__input[name='year']").val();
      var d = new Date(year, month - 1, day);
      var today = new Date();
      var age = -1;
      if (
        d.getDate() == parseInt(day) &&
        d.getMonth() + 1 == parseInt(month) &&
        year == d.getFullYear() &&
        year <= new Date().getFullYear() &&
        year > 999 && 
        (today > d)
      ) {
        age = Math.floor((today - d) / (365.25 * 24 * 60 * 60 * 1000));
        insuarnceGain.find('.input-fields__error-msg').text('');
        insuarnceGain.find('.date-time-wrapper__input:not(.money__input-field)').css('border-color', '');
      } else {
        insuarnceGain.find('.input-fields__error-msg').text(insuarnceGain.attr('data-invalid-date'));
        insuarnceGain.find('.date-time-wrapper__input:not(.money__input-field)').css('border-color', '#f44336');
        insuarnceGain.find('.insurance-fee').html(0 + ` ${insuarnceGain.attr('data-currency-input')}`);
      }
      return age;
    }

    function calcMoney(age, money) {
      if (age > -1) {
        var gender = insuarnceGain.find('.option.selected.gender').attr('value') === '0' ? 0 : 1;
        var inputId = insuarnceGain.find('.date-time-wrapper__input.money__input-field').attr('id');
        money = money ? parseInt(money.replaceAll(',', '')) : 0;
        // var validateMoney = moneyValidate[inputId]
        let ageValidation = ageValidate?.[inputId] || {min: 0, max: 0};
        if (age >= ageValidation.min && age <= ageValidation.max) {
          var insuranceItem = insuranceValue.find((x) => x.age === age);
          if (!insuranceItem) {
            return;
          }
          var premiumIndex = insuranceItem?.[inputId]?.[gender] || 0;
          const newResult = (premiumIndex * money) / 1000;
          insuarnceGain.find('.insurance-fee').html(CurrencyHelper.numberWithCommas(Math.round(newResult / 1000) * 1000) + ` ${insuarnceGain.attr('data-currency-input')}`);
        } else {
          insuarnceGain.find('.input-fields__error-msg').text(insuarnceGain.attr('data-invalid-age'));
          insuarnceGain.find('.date-time-wrapper__input:not(.money__input-field)').css('border-color', '#f44336');
        }
      }
    }

    insuarnceGain.find("[name='moneyValue'], [name='year'], [name='month'], [name='day']").keyup(function () {
      var money = insuarnceGain.find('.date-time-wrapper__input.money__input-field').val();
      if(money) {
        var age = calcAge();
        insuarnceGain.find('.date-time-wrapper__input.money__input-field').val(numberWithComma(money));
        calcMoney(age, money);
      }
    });
    insuarnceGain.find("[name='moneyValue']").on('paste', function(event) {
      if (event.originalEvent.clipboardData.getData('Text').match(/[^\d\.\,]/)) {
        event.preventDefault();
      }
    });
    insuarnceGain.find('.drop-down__controls').click(function () {
      var option = $(this).next('.select-option');
      $(option).toggleClass('opened');
    });
    insuarnceGain.find('.select-option').click(function (e) {
      var selectOption = $(this);
      if ($(e.target).hasClass('option')) {
        var select = $(this).prev('.drop-down__controls');
        var selectcontrol = $(select).find('.drop-down__select');
        $(selectcontrol).html($(e.target).html());
        selectOption.find('.option').each((index,item) => {
          $(item).removeClass('selected');
          if($(item).html() == $(e.target).html()) {
            $(item).addClass('selected');
          }
        });
        var money = insuarnceGain.find("[name='moneyValue']").val();
        if(money) {
          var age = calcAge();
          calcMoney(age, money);
        }
      }
      selectOption.removeClass('opened');
    });

    $(window).on("click", function (e) {
      if(!$(e.target).hasClass("drop-down__container") && 
          !$(e.target).hasClass("drop-down__controls") &&
          !$(e.target).hasClass("drop-down__select") &&
          !$(e.target).parents().hasClass("drop-down__controls")) {
        $('.insurance-gain').find('.select-option').removeClass('opened');
      }
    });
  });

  $('.insurance-calculation input[name=depositMoney]').keyup(function(){
    let sliderValue = $(this).parents('.insurance-calculation').find('.tcb-input--save-calc .tcb-input-range_inline-value').text();
    let depositeMoney = $(this).val();
    let tenure = $(this).parents('.insurance-calculation').find('input[name=depositMonth]').val();
    let inputInfoValue = $(this).parents('.input-items').data().trackingClickInfoValue
    let inputJsonStr = inputInfoValue.replace(/'/g, '"');
    let inputJson = JSON.parse(inputJsonStr);
    inputJson.calculatorFields = `${depositeMoney} | ${sliderValue} | ${tenure}`;
    let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
    $(this).parents('.input-items').attr("data-tracking-click-info-value", updatedValues);
    
  })
  
  $('.insurance-calculation .tcb-input-range_bar-wrapper').on('mouseleave',function(){
    let sliderValue = $(this).find('.tcb-input-range_inline-value').text();
    let depositeMoney = $(this).parents('.insurance-calculation').find('input[name=depositMoney]').val();
    let tenure = $(this).parents('.insurance-calculation').find('input[name=depositMonth]').val();
    var attrData = $(this).parents('.input-items').attr("data-tracking-click-info-value");

    if (typeof attrData !== typeof undefined && attrData !== false) {
      let inputInfoValue = $(this).parents('.input-items').data().trackingClickInfoValue;
      let inputJsonStr = inputInfoValue.replace(/'/g, '"');
      let inputJson = JSON.parse(inputJsonStr);
      inputJson.calculatorFields = `${depositeMoney} | ${sliderValue} | ${tenure}`;
      let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
      $(this).parents('.input-items').attr("data-tracking-click-info-value", updatedValues);
      $(this).parents('.input-items').trigger('click');
    }
  })

  $('.insurance-calculation input[name=depositMonth]').keyup(function(){
    let sliderValue = $(this).parents('.insurance-calculation').find('.tcb-input--save-calc .tcb-input-range_inline-value').text();
    let depositeMoney = $(this).parents('.insurance-calculation').find('input[name=depositMoney]').val();
    let tenure = $(this).val();

    let inputInfoValue = $(this).parents('.input-items').data().trackingClickInfoValue
    let inputJsonStr = inputInfoValue.replace(/'/g, '"');
    let inputJson = JSON.parse(inputJsonStr);
    inputJson.calculatorFields = `${depositeMoney} | ${sliderValue} | ${tenure}`;
    let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
        $(this).parents('.input-items').attr("data-tracking-click-info-value", updatedValues);
  })
  $('.insurance-calculation input').keyup(function(){
    $(this).parents('.input-items').trigger('click')
  })
});
