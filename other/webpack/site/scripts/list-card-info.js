$(document).ready(function () {
  let listCardInfo = $('.list-card-info');
  let listCardInfoListItem = listCardInfo.find('.list-card-info__list-item');
  let numberOfCardsShownDefault = listCardInfoListItem.length; 
  // switch to carousel on smaller device  
  slickInitial();
  // show and hide in view more
  let viewAllFlag = false;
  showHideCardsInitials();

  $(window).on('resize', function () {
    slickInitial();
    showHideCardsInitials();
  });

  function slickInitial() {

    $('.list-card-info').each(function () {
      var carouselSlicklist = $(this).find('.list-card-info__list-item');
      var ctaLink = carouselSlicklist.find('.list-card-info__item-action');
      if(ctaLink.length > 0){
        carouselSlicklist.addClass('stack-view');
      }

      if ($(window).width() <= 767 && ctaLink.length === 0) {
        carouselSlicklist.not('.list-card-info__item-action').not('.slick-initialized').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false,
          autoplaySpeed: 2000,
          arrows: false,
          dots: true,
          infinite: false,
        });
        let viewMoreBtn = $('.list-card-info .list-card-info__view-more-btn');
        viewMoreBtn.css('display', 'none');
      } else {
        carouselSlicklist.filter('.slick-initialized').slick('unslick');
        let viewMoreBtn = $('.list-card-info .list-card-info__view-more-btn');
        viewMoreBtn.css('display', 'flex');
      }
      
    }); 
  }
  function showHideCardsInitials() {
    //view more view less functionalities
    if(listCardInfoListItem.hasClass('view-more')) {
      //get cols/rows to show number
      let screenWidth = window.innerWidth;
      if(screenWidth > 991) {
        if(listCardInfoListItem.hasClass('column-4')) {
          numberOfCardsShownDefault = 4;
        }
        if(listCardInfoListItem.hasClass('column-3')) {
          numberOfCardsShownDefault = 3;
        }
        if(listCardInfoListItem.hasClass('column-2')) {
          numberOfCardsShownDefault = 2;
        }
      }
      else if(screenWidth > 767) {
        numberOfCardsShownDefault = 4;
      }
      else {
        numberOfCardsShownDefault = 3;
      }
      //hide all rows, only show first rows
      if(viewAllFlag) {
        showHideCards(listCardInfoListItem, numberOfCardsShownDefault, 'show');
      }
      else {
        showHideCards(listCardInfoListItem, numberOfCardsShownDefault, 'hide');
      }
    }
  }

  // view more view less on click
  listCardInfo.each(function () {
    let viewMoreBtnContainer = $(this).find('.list-card-info__view-more-btn');
    let viewMoreBtn = viewMoreBtnContainer.find('.cta-button');
    viewMoreBtn.click(function() {
      if(viewAllFlag) {
        viewMoreBtn.addClass('view-more');
        let viewMoreText = viewMoreBtnContainer.attr('viewMorelabel');
        $(this).find('.cmp-button__text').text(viewMoreText);
        showHideCards(listCardInfoListItem, numberOfCardsShownDefault, 'hide');
        viewAllFlag = false;
      }
      else {
        viewMoreBtn.removeClass('view-more');
        let viewLessText = viewMoreBtnContainer.attr('viewLesslabel');
        $(this).find('.cmp-button__text').text(viewLessText);
        showHideCards(listCardInfoListItem, numberOfCardsShownDefault, 'show');
        viewAllFlag = true;
      }
    })
  }); 

  function showHideCards(listCardInfoListItem, numberOfCardsShownDefault, action) {
    let listCardInfoItem = listCardInfoListItem.find('.list-card-info__item');
    listCardInfoListItem.find('[style*="display: none"]').css('display', 'flex');
    if(action == 'hide') {
      listCardInfoItem.each(function(index) {
        if(index >= numberOfCardsShownDefault) {
          $(this).css('display', 'none');
        }
      })
    }
  }
});