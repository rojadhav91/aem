$(document).ready(function () {
    let mastheadExtendContainer = $('.masthead-extend-component');
    mastheadExtendContainer.each(function () {
        // gradient color background effect
        let mastheadGradientBanner = $('.masthead-extend.gradient');
        if(mastheadGradientBanner.length > 0) {
            let mastheadCardGallery = $(this).find('.masthead-extend__card-gallery');
            let mastheadGradientBannerColor = mastheadGradientBanner.css('background-color');
            let mastheadCardGalleryColor = mastheadCardGallery.css('background-color');
            mastheadGradientBanner.css('background', getTopBannerBackground(mastheadGradientBannerColor));
            mastheadCardGallery.css('background', getBottomBannerBackground(mastheadCardGalleryColor));
        }

        function getTopBannerBackground(rgbString) {
            // create a lighter color for the gradient effect. The ratio between 2 colors equals to the 2 colors on live site
            let rgbColor = rgbString.match(/\d+/g);
            let rSecondary = parseInt(rgbColor[0] / 248 * 255);
            let gSecondary = parseInt(rgbColor[1] / 210 * 245);
            let bSecondary = parseInt(rgbColor[2] / 211 * 235);
            let rgbSecondary = 'rgb(' + rSecondary + ',' + gSecondary + ',' + bSecondary + ')';
            // Returns the gradient effect
            return 'radial-gradient(140.24% 140.24% at 48.72% 71.61%,' + rgbSecondary + ' 0,' + rgbString + ' 100%)';
        }

        function getBottomBannerBackground(rgbString) {
            // create a lighter color for the gradient effect. The ratio between 2 colors equals to the 2 colors on live site
            let rgbColor = rgbString.match(/\d+/g);
            let rSecondary = parseInt(rgbColor[0] / 240 * 255);
            let gSecondary = parseInt(rgbColor[1] / 240 * 255);
            let bSecondary = parseInt(rgbColor[2] / 240 * 255);
            let rgbSecondary = 'rgb(' + rSecondary + ',' + gSecondary + ',' + bSecondary + ')';
            // Returns the gradient effect
            return 'radial-gradient(140.24% 140.24% at 48.72% 71.61%,' + rgbSecondary + ' 0,' + rgbString + ' 100%)';
        }
    });
    
  });