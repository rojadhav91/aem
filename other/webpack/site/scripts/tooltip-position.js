(function($) {
  $.fn.invisible = function() {
      return this.each(function() {
          $(this).css("visibility", "hidden");
          $(this).css("opacity", "0");
      });
  };
  $.fn.visible = function() {
      return this.each(function() {
          $(this).css("visibility", "visible");
          $(this).css("opacity", "1");
      });
  };
  $.fn.setupTooltip = function() {
    let text = $(this).text();
    $(this).empty();
    $(this).append(`<i></i>`);
    $(this).append(`<span class="tooltiptext__content">${text}</span>`);
  }
}(jQuery));

$(function() {
  function calculatePosition(element, tooltip, text) {
    let icon = element;
    let arrowIcon = tooltip.find('i');
    let content = tooltip.find('.tooltiptext__content');
    content.text(text);
    arrowIcon.css({
      position: 'fixed',
      bottom: $(window).height() - icon.offset().top,
      left: icon.offset().left - 4
    });
    let width = content.outerWidth();
    let tmpLeft = arrowIcon.offset().left + arrowIcon.width() / 2 - width / 2;
    let tmpRight = $(window).width() -  tmpLeft - width;
    const minMargin = 24;
    if (tmpRight < minMargin) {
      tmpRight = minMargin;
      tmpLeft = 'auto';
    } else if (tmpLeft < minMargin) {
      tmpLeft = minMargin;
      tmpRight = 'auto';
    }
    content.css({
      position: 'fixed',
      bottom: `${$(window).height() - arrowIcon.offset().top}px`,
      left: tmpLeft,
      right: tmpRight
    });
    
  }

  if ($('body #tooltiptext').length == 0) {
    $('body').append('<div id="tooltiptext"></span></div>');
  }
  const tooltip = $('body #tooltiptext');
  tooltip.empty();
  tooltip.append('<i></i><span class="tooltiptext__content">');

  $('.icon-info,.tcb-tooltip').hover(function() {
    calculatePosition($(this), tooltip, $(this).find('.tooltiptext').text());
    tooltip.visible();
  }, function(){
    tooltip.invisible();
  });
});