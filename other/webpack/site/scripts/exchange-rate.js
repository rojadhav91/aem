import moment from 'moment';
import _ from 'lodash';
import './currency-helper';
import CurrencyHelper from './currency-helper';

$.fn.datetimeAnalyticUpdate = function(selectedDate, selectedTime) {
  let analyticsElement = $(this);
  let selectedDateTime = selectedTime !== null ? selectedDate + '|' + selectedTime : selectedDate; // updated date and time
  let eventTrackingDataValues = analyticsElement.data().trackingClickInfoValue; // data updated

  if(typeof eventTrackingDataValues !== 'undefined'){
    let json = JSON.parse(eventTrackingDataValues.replaceAll('\'', '"'));
    if (typeof json !== 'undefined'){
      json.calculatorField = selectedDateTime;     
      let updatedValue = JSON.stringify(json);
      analyticsElement.attr("data-tracking-click-info-value", updatedValue);
    }
  }
}

// Fixing rate
$(document).ready(function () {
  $('.exchange-rate').each(function() {
    let fixingRate = $(this);
    let timeSelect = fixingRate.find('.time-select');

    let viewMoreButton = fixingRate.find('.exchange-rate__view-more button');
    let fixingRatePopup = fixingRate.find('.exchange-rate__popup');
    let popupCloseButton = fixingRatePopup.find('.popup__close-button');
    let downloadButton = fixingRatePopup.find('.popup__download-button button');
    let tableContainer = fixingRate.find('.table-content-container');
    let emptyLabel = fixingRate.find('.exchange-rate__empty-label');
    let tableElements = fixingRate.find('.exchange-rate-table-content');
    let selectedDateElements = fixingRate.find('.calendar__input-field');

    let currentDate = moment().format('DD/MM/yyyy');
    let currentTime = null;

    fixingRatePopup.css({
      opacity: '0',
      transform: 'scale(0.75, 0.5625)',
      transition:
        'opacity 461ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, transform 307ms cubic-bezier(0.4, 0, 0.2, 1) 154ms',
      visibility: 'hidden',
    });

    viewMoreButton.each(function () {
      $(this).click(function () {
        $('body').addClass('overflow-hidden');
        $(this).parents().siblings('.exchange-rate__popup').css({
          opacity: '1',
          transform: 'none',
          transition:
            'opacity 461ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, transform 307ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
          visibility: '',
        });
      });
    });

    popupCloseButton.click(function () {
      $('body').removeClass('overflow-hidden');
      fixingRatePopup.css({
        opacity: '0',
        transform: 'scale(0.75, 0.5625)',
        transition:
          'opacity 461ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, transform 307ms cubic-bezier(0.4, 0, 0.2, 1) 154ms',
        visibility: 'hidden',
      });
    });

    downloadButton.click(function () {
      let body = $('body');
      body.addClass('print');
      print(fixingRatePopup);
      body.removeClass('print');
    });

    function generateTable(element, items) {
      element.empty();
      items.forEach((item) => {
        element.append(
          `
      <div class='exchange-rate__table-records'>
        <div class='table__first-column left-pos strong-par first-column'>
          <p>${item.label ?? ''}</p>
        </div>
        <div class='table-records__data'>
          <div class='table-records__data-content row-divided'>
            <div class='data-content__item'>
              <p>${CurrencyHelper.numberWithCommas(item.bidRateCK)}</p>
            </div>
            <div class='data-content__item'>
              <p>${CurrencyHelper.numberWithCommas(item.bidRateTM)}</p>
            </div>
          </div>
          <div class='table-records__data-content row-divided last-column'>
            <div class='data-content__item'>
              <p>${CurrencyHelper.numberWithCommas(item.askRateTM)}</p>
            </div>
            <div class='data-content__item last-column'>
              <p>${CurrencyHelper.numberWithCommas(item.askRate)}</p>
            </div>
          </div>
        </div>
      </div>
      `
        );
      });
    }

    function generateTenorIntRate(items) {
      if (items === null || items.length == 0) {
        fixingRate.find('#tenorint-rate').addClass('hidden');
        return;
      }
      fixingRate.find('#tenorint-rate').removeClass('hidden');
      let container = fixingRate.find('#tenorint-rate-content');
      container.empty();
      const map = _.chain(items)
        .groupBy('rateCD')
        .map((rates, rateCD) => ({ rates, label: rateCD }))
        .value();
      let tenors = [...Array.from(new Set(items.map(e => Number(e.tenor) ?? ''))).sort((a, b) => a - b), '', '', ''].slice(0,3);
      container.append(
        `
          <div class='exchange-rate__table-records'>
            <div class='table__first-column left-pos strong-par first-column'>
              <p>${currentDate}</p>
            </div>
            <div class='table-records__data width-66-33'>
              <div class='table-records__data-content row-divided'>
                <div class='data-content__item'>
                  <p>${tenors[0]}</p>
                </div>
                <div class='data-content__item'>
                  <p>${tenors[1]}</p>
                </div>
              </div>
              <div class='table-records__data-content last-column'>
                <p>${tenors[2]}</p>
              </div>
            </div>
          </div>
          `
      );
      map.forEach((item) => {
        container.append(
          `
          <div class='exchange-rate__table-records'>
            <div class='table__first-column left-pos strong-par first-column'>
              <p>${item.label}</p>
            </div>
            <div class='table-records__data width-66-33'>
              <div class='table-records__data-content row-divided'>
                <div class='data-content__item'>
                  <p>${
                    tenors[0] === '' ? '' : CurrencyHelper.numberWithCommas(item.rates.find((e) => Number(e.tenor) === tenors[0])?.tenorInt)
                  }</p>
                </div>
                <div class='data-content__item'>
                  <p>${
                    tenors[1] === '' ? '' : CurrencyHelper.numberWithCommas(item.rates.find((e) => Number(e.tenor) === tenors[1])?.tenorInt)
                  }</p>
                </div>
              </div>
              <div class='table-records__data-content last-column'>
                <p>${
                  tenors[2] === '' ? '' : CurrencyHelper.numberWithCommas(item.rates.find((e) => Number(e.tenor) === tenors[2])?.tenorInt)
                }</p>
              </div>
            </div>
          </div>
          `
        );
      });
    }

    function generateTenorRate(items) {
      let container = fixingRate.find('#tenor-rate-content');
      container.empty();
      let data = items.sort((a,b) => a.tenor - b.tenor);
      data.forEach(e => {
        container.append(
          `
          <div class='exchange-rate__table-records'>
            <div class='table__first-column left-pos strong-par first-column'>
              <p>${e.tenor}</p>
            </div>
            <div class='table-records__data width-66-33'>
            <div class='table-records__data-content row-divided'>
              <div class='data-content__item'>
                <p></p>
              </div>
              <div class='data-content__item'>
                <p>${CurrencyHelper.numberWithCommas(e.bidRate)}</p>
              </div>
            </div>
            <div class='table-records__data-content last-column'>
              <p>${CurrencyHelper.numberWithCommas(e.askRate)}</p>
            </div>
          </div>
          `
        );
      });
    }

    function generatePopupTable(data) {
      let otherRate = data?.otherRate?.data[0];
      fixingRate.find('#central-rate').text(CurrencyHelper.numberWithCommas(otherRate?.central));
      fixingRate.find('#floor-rate').text(CurrencyHelper.numberWithCommas(otherRate?.floor));
      fixingRate.find('#ceiling-rate').text(CurrencyHelper.numberWithCommas(otherRate?.ceiling));

      let goldRates = data?.goldRate?.data;
      if (goldRates.length > 0) {
        let goldRate = goldRates[0];
        fixingRate.find('#gold-bid-rate').text(CurrencyHelper.numberWithCommas(goldRate.bidRate));
        fixingRate.find('#gold-ask-rate').text(CurrencyHelper.numberWithCommas(goldRate.askRate));
      }
      let tenorIntRate = data?.tenorintRate?.data ?? [];
      generateTenorIntRate(tenorIntRate);

      let tenorRate = data?.tenorRate?.data ?? [];
      generateTenorRate(tenorRate);
    }

    selectedDateElements.each((idx, element) => {
      $(element).text(currentDate);
      $(element).on('updateCal', function () {
        if (currentDate === $(element).text()) {
          return;
        }
        currentDate = $(element).text();
        currentTime = null;
        updateData(currentDate, currentTime);
      });
    });

    timeSelect.on('timeChanged', function(event, newValue) {
      currentTime = newValue;
      updateData(currentDate, currentTime);
    });

    updateData(currentDate, null);

    function updateData(date, time) {
      selectedDateElements.each((idx, element) => {
        $(element).text(currentDate);
      });
      let baseUrl = fixingRate.attr('data-url');
      if (baseUrl === undefined) {
        return;
      }
      let url = baseUrl + '/_jcr_content.exchange-rates';
      if (date) {
        url += `.${date.split('/').toReversed().join('-')}`;
        if (time) {
          url += `.${time.replaceAll(':', '-')}`;
        }
      }
      url += '.integration.json';
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
          let exchangeRate = data?.exchangeRate;
          // If not available
          if (exchangeRate === null || exchangeRate.data.length == 0) {
            emptyLabel.removeClass('hidden');
            tableContainer.addClass('hidden');
            timeSelect.changeOptions(exchangeRate.updatedTimes, currentTime);
            return;
          }

          emptyLabel.addClass('hidden');
          tableContainer.removeClass('hidden');

          // generate table
          tableElements.each((idx, element) => {
            generateTable($(element), exchangeRate.data);
          });
          generatePopupTable(data);
          timeSelect.changeOptions(exchangeRate.updatedTimes, currentTime);
          if (currentTime === null && exchangeRate.updatedTimes.length > 0) {
            currentTime = exchangeRate.updatedTimes[0];
          }
          fixingRate.find('.exchange-rate__table-note').each(function () {
            let element = $(this);
            let note = element.attr('note');
            if (note == null) {
              return;
            }
            note = note
              .replace('%updatedDate', currentDate)
              .replace('%updatedTime', currentTime);
            element.html(note);
          });
          upadateDateTimeStamp();
        },
      });
    }
    
    //analytics funtion to capture selected data & time from exchange rate : start
    function upadateDateTimeStamp(){
      fixingRate.find('.analytics-active-link').click();
    }
    
    fixingRate.find('.analytics-active-link').on('click',function(){
        $(this).datetimeAnalyticUpdate(currentDate, currentTime);
    });

    upadateDateTimeStamp(); // analytics function call on DOM load to update value 
    
  });
});
//analytics funtion to capture selected data & time from exchange rate : start