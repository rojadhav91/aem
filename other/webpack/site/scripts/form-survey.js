$(function () {
  const formSurveyElement = $(".c-cash-form-survey");
  const surveyType = formSurveyElement?.attr("data-survey-type");
  if (formSurveyElement.length === 0 || surveyType !== "multi") {
    return;
  }

  const checkboxElement = $(".multiple-choice__container--answer");
  const minimumOfAnswers = checkboxElement?.attr("data-minimum-of-answers");
  const answerAlertElement = $(".multiple-choice__container--answer-alert");

  answerAlertElement?.hide();

  checkboxElement.click(function () {
    const selectedElement = $(this)?.find($("input.input__checkbox:checked"));
    if (selectedElement?.length < Number(minimumOfAnswers)) {
      answerAlertElement?.show();
    } else {
      answerAlertElement?.hide();
    }
  });
});
