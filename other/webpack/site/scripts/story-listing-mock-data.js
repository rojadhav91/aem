export const stories = [
  {
    total: "23",
    results: [
      {
        title: "Video Test",
        description: "Sample description 1",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        image: "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "14 Aug 2023",
        path: "/content/techcombank/web/kr/en/updates/article-test.html",
      },
      {
        title: "Article",
        description: "Sample description 2",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        image:
          "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "08 Aug 2023",
        path: "/content/techcombank/web/kr/en/updates/article.html",
      },
      {
        title: "Practice reasonable spending habits to get richer",
        description: "Sample description 3",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description: "Save money for the rainy days",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "24 Jul 2023",
        path: "/content/techcombank/web/kr/en/updates/practice-reasonable-spending-habits-to-get-richer.html",
      },
      {
        title: "Practice reasonable spending habits to get richer",
        description: "Sample description 4",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description: "Save money for the rainy days",
        image:
          "/content/dam/techcombank/brand-assets/images/FX-masthead-26-4-2023-thumb-f5c0c4bdbc.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "04 Jul 2023",
        path: "/content/techcombank/web/kr/en/updates/practice-reasonable-spending-habits-to-get-richer1.html",
      },
      {
        title:
          'Overseas remittance and foreign exchange offer: "Profitable exchange – zero fee remittance"',
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          'Get full of attractive offers for the " Profitable exchange – zero fee remittance " when doing overseas remittance and exchange foreign currency at Techcombank',
        image:
          "/content/dam/techcombank/brand-assets/images/KV_T_Ien_hon_tien_mat_loi_hon_tien_mat_Thumbnail_Website_1155x657_26_09_ae87512497.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "25 Apr 2023",
        path: "/content/techcombank/web/kr/en/updates/overseas-remittance-and-foreign-exchange-offer---profitable-exch.html",
      },
      {
        title:
          'Overseas remittance and foreign exchange offer: "Profitable exchange – zero fee remittance"',
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          'Get full of attractive offers for the " Profitable exchange – zero fee remittance " when doing overseas remittance and exchange foreign currency at Techcombank',
        image:
          "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "25 Apr 2023",
        path: "/content/techcombank/web/kr/en/updates/overseas-remittance-and-foreign-exchange-offer---profitable-exch1.html",
      },
      {
        title:
          "Change the notification for credit card transaction to Techcombank Mobile app, replacing SMS",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          "Techcombank is pleased to announce that we will stop sending SMS for credit card transaction in March 2023. Instead, Techcombank credit card holders can receive notifications of all transactions for free, more safe and convenient with Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/Noti_giao_dich_the_tin_dung_website_oct31_thumbnail_e99e55c8b1.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "27 Mar 2023",
        path: "/content/techcombank/web/kr/en/updates/change-the-notification-for-credit-card-transaction-to-techcomba0.html",
      },
      {
        title:
          "Change the notification for credit card transaction to Techcombank Mobile app, replacing SMS",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          "Techcombank is pleased to announce that we will stop sending SMS for credit card transaction in March 2023. Instead, Techcombank credit card holders can receive notifications of all transactions for free, more safe and convenient with Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/KV_T_Ien_hon_tien_mat_loi_hon_tien_mat_Thumbnail_Website_1155x657_26_09_ae87512497.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "13 Mar 2023",
        path: "/content/techcombank/web/kr/en/updates/change-the-notification-for-credit-card-transaction-to-techcomba1.html",
      },
      {
        title:
          "Change the notification for credit card transaction to Techcombank Mobile app, replacing SMS",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          "Techcombank is pleased to announce that we will stop sending SMS for credit card transaction in March 2023. Instead, Techcombank credit card holders can receive notifications of all transactions for free, more safe and convenient with Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "10 Mar 2023",
        path: "/content/techcombank/web/kr/en/updates/change-the-notification-for-credit-card-transaction-to-techcomba01.html",
      },
      {
        title:
          "Change the notification for credit card transaction to Techcombank Mobile app, replacing SMS",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          "Techcombank is pleased to announce that we will stop sending SMS for credit card transaction in March 2023. Instead, Techcombank credit card holders can receive notifications of all transactions for free, more safe and convenient with Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/Noti_giao_dich_the_tin_dung_website_oct31_thumbnail_e99e55c8b1.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "02 Mar 2023",
        path: "/content/techcombank/web/kr/en/updates/change-the-notification-for-credit-card-transaction-to-techcomba.html",
      },
      {
        title: "Announcement of updating Techcombank credit card interest rate",
        articleCategory: ["Sản phẩm mới", "Tin tức dành cho Nhà đầu tư"],
        description:
          "From 01/04/2023, Techcombank will update interest rates for some specific credit cards",
        image:
          "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "23 Feb 2023",
        path: "/content/techcombank/web/kr/en/updates/announcement-of-updating-techcombank-credit-card-interest-rate1.html",
      },
      {
        title: "Announcement of updating Techcombank credit card interest rate",
        articleCategory: [
          "Sản phẩm mới",
          "Thông cáo báo chí",
          "Tin tức dành cho Nhà đầu tư",
        ],
        description:
          "From 01/04/2023, Techcombank will update interest rates for some specific credit cards",
        image:
          "/content/dam/techcombank/brand-assets/images/KV_T_Ien_hon_tien_mat_loi_hon_tien_mat_Thumbnail_Website_1155x657_26_09_ae87512497.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "22 Feb 2023",
        path: "/content/techcombank/web/kr/en/updates/announcement-of-updating-techcombank-credit-card-interest-rate.html",
      },
      {
        title:
          "Migration from F@st Mobile/F@st i-Bank to Techcombank Mobile/Techcombank Online Banking",
        articleCategory: ["Sản phẩm mới"],
        description:
          "Enjoy shopping flexibly, convenient payment for credi cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "26 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/migration-from-f-st-mobile-f-st-i-bank-to-techcombank-mobile-tec.html",
      },
      {
        title: "Up to 300,000 VND cashback when paying by credit card",
        articleCategory: ["Tin tức dành cho Nhà đầu tư"],
        description:
          "Enjoy shopping flexibly, convenient payment for crdit cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "20 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/up-to-300-000-vnd-cashback-when-paying-by-credit-card.html",
      },
      {
        title:
          "Top up from Techcombank, pay via ShopeePay with tons of special offers this October",
        articleCategory: ["Sản phẩm mới", "Thông cáo báo chí"],
        description:
          "Enjoy shopping flexibly, convenient payment for credit cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "19 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/top-up-from-techcombank--pay-via-shopeepay-with-tons-of-special-.html",
      },
      {
        title:
          "Top up from Techcombank, pay via ShopeePay with tons of special offers this October",
        articleCategory: ["Thông cáo báo chí", "Tin tức dành cho Nhà đầu tư"],
        description:
          "Enjoy shopping flexibly, convenient payment for credt cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "12 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/top-up-from-techcombank--pay-via-shopeepay-with-tons-of-special-1.html",
      },
      {
        title: "Discount to 1 million when paying at Nguyen Kim by NAPAS card",
        articleCategory: ["Sản phẩm mới", "Tin tức dành cho Nhà đầu tư"],
        description:
          "Sincerely send to you and the Bank informaton about a series of extremely attractive promotions when paying by NAPAS Card at Nguyen Kim's stores.",
        image:
          "/content/dam/techcombank/brand-assets/images/Card_detail_11_12b0ece282.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "11 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/discount-to-1-million-when-paying-at-nguyen-kim-by-napas-card.html",
      },
      {
        title: "Discount to 1 million when paying at Nguyen Kim by NAPAS card",
        articleCategory: ["Sản phẩm mới", "Thông cáo báo chí"],
        description:
          "Sincerely send to you and the Bank information about a series of extremely attractive promotions when paying by NAPAS Card at Nguyen Kim's stores.",
        image:
          "/content/dam/techcombank/brand-assets/images/TC_Bx_Shopeepay2510_Thumbnail_fccaa0b479.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "10 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/discount-to-1-million-when-paying-at-nguyen-kim-by-napas-card1.html",
      },
      {
        title:
          "Migration from F@st Mobile/F@st i-Bank to Techcombank Mobile/Techcombank Online Banking",
        articleCategory: ["Thông cáo báo chí", "Tin tức dành cho Nhà đầu tư"],
        description:
          "Enjoy shopping flexibly, convenien payment for credit cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/FX-masthead-26-4-2023-thumb-f5c0c4bdbc.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "09 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/migration-from-f-st-mobile-f-st-i-bank-to-techcombank-mobile-tec1.html",
      },
      {
        title: "Up to 300,000 VND cashback when paying by credit card",
        articleCategory: ["Sản phẩm mới"],
        description:
          "Enjoy shopping flexibly, convenient payment for credit cardholders with 0% interest installment and up to 300,000 VND cashback when first installment on Techcombank Mobile app.",
        image:
          "/content/dam/techcombank/brand-assets/images/KV_T_Ien_hon_tien_mat_loi_hon_tien_mat_Thumbnail_Website_1155x657_26_09_ae87512497.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "05 Oct 2022",
        path: "/content/techcombank/web/kr/en/updates/up-to-300-000-vnd-cashback-when-paying-by-credit-card1.html",
      },
      {
        title:
          "Reimbursement policy for issuance fee and annual fee in the first year",
        articleCategory: ["Sản phẩm mới", "Thông cáo báo chí"],
        description:
          "From January 1, 2024, TechTNMT adjusted the 1% Cash Back Program policy applicable to payment cards (aka debit cards)",
        image:
          "/content/dam/techcombank/brand-assets/images/KV_T_Ien_hon_tien_mat_loi_hon_tien_mat_Thumbnail_Website_1155x657_26_09_ae87512497.png/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "29 Sep 2022",
        path: "/content/techcombank/web/kr/en/updates/reimbursement-policy-for-issuance-fee-and-annual-fee-in-the-firs.html",
      },
      {
        title:
          "Reimbursement policy for issuance fee and annual fee in the first year",
        articleCategory: ["Thông cáo báo chí", "Tin tức dành cho Nhà đầu tư"],
        description:
          "From January 1, 2022, TechTNMT adjusted the 1% Cash Back Program policy applicable to payment cards (aka debit cards)",
        image:
          "/content/dam/techcombank/brand-assets/images/TCB_Chuyendoiapp_website_oct25_thumb_077de82ec3.jpg/jcr:content/renditions/cq5dam.thumbnail.319.319.png",
        articleCreationDate: "15 Sep 2022",
        path: "/content/techcombank/web/kr/en/updates/reimbursement-policy-for-issuance-fee-and-annual-fee-in-the-firs1.html",
      },
    ],
  },
];
