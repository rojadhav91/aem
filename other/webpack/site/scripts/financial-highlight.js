// Statistics Table Component
$(document).ready(function () {
    var statisticsTable = $('.financialhighlights');
  
    statisticsTable.each(function () {
      var yearTable = $(this).find('.year-item');
      var statisticFilter = $(this).find('.select-checkbox-filter');
      var quarterTable = yearTable.find('.quarter-item');
      var showedOptions = statisticFilter.find('.select-options .checkbox-list input');
      var showedOptionYear = statisticFilter.find('.select-options .year input');
      var showedOptionQuarter = statisticFilter.find('.select-options .quarter input');
      var mobileShowedOptions = statisticFilter.find('.popup-content .checkbox-list input');
      var applyMobileFilter = statisticFilter.find('.popup-content .btn button');
      var showedOptionYearMB = statisticFilter.find('.popup-content .year input');
      var showedOptionQuarterMB = statisticFilter.find('.popup-content .quarter input');
      var selectLabel = $(this).attr("data-select-label");

      var arrYear;
      var arrQuarter;
      var arrYearMB;
      var arrQuarterMB;
      var arr;
  
      // filter by check year
      showedOptionYear.change(function () {
        arrYear = [];
        arrYear = getListOption(arrYear, showedOptionYear);
        yearFilter(arrYear, yearTable); // filter
      });
  
      // filter by check quarter
      showedOptionQuarter.change(function () {
        arrQuarter = [];
        arrQuarter = getListOption(arrQuarter, showedOptionQuarter);
        quarterFilter(arrQuarter, quarterTable); // filter
      });
  
      // when no checked-box
      showedOptions.change(function () {
        arr = [];
        arr = getListOption(arr, showedOptions);
  
        // show all
        // if (arr.length == 0) {
        //   quarterTable.addClass('showed');
        //   yearTable.addClass('showed');
        // }
      });
  
      // get list of checked checkbox
      function getListOption(arr, list) {
        list.each(function () {
          if ($(this).is(':checked')) {
            arr.push($(this).val());
          }
        });
  
        return arr;
      }
  
      // filter by quarter function
      function quarterFilter(arr, tables) {
        tables.removeClass('showed');
  
        if (arr.length == 0) {
         return;
        }
  
        tables.each(function () {
          for (let j = 0; j < arr.length; j++) {
            if ($(this).attr('data-quarter') == arr[j]) {
              $(this).addClass('showed');
            }
          }
        });
      }
  
      // filter by year function
      function yearFilter(arr, tables) {
        tables.removeClass('showed');
        
        if (arr.length == 0) {
          return;
        }
  
        tables.each(function () {
          for (let i = 0; i < arr.length; i++) {
            if ($(this).attr('data-year') == arr[i]) {
              $(this).addClass('showed');
            }
          }
        });
      }
  
      initialSetup();
  
      // initial filter
      function initialSetup() {
        var yearNow = new Date().getFullYear();
  
        // autoclick newest year
        showedOptions.each(function () {
          if ($(this).val() == yearNow) {
            $(this).click();
          }
        });
  
        // in mobile
        mobileShowedOptions.each(function () {
          if ($(this).val() == yearNow) {
            $(this).click();
            $(this).addClass('checked');
          }
        });
  
        // show newest tables
        yearTable.each(function () {
          if ($(this).attr('data-year') == yearNow) {
            $(this).addClass('showed');
          }
  
          quarterTable.addClass('showed');
        });
      }
  
      function filterInMobile() {
        arrYearMB = [];
        arrQuarterMB = [];
  
        arrYearMB = getListOption(arrYearMB, showedOptionYear);
        arrQuarterMB = getListOption(arrQuarterMB, showedOptionQuarter);
        yearFilter(arrYearMB, yearTable);
        quarterFilter(arrQuarterMB, quarterTable);
      }
      if ($(this).find('.select-options').length > 0) {
        setupFilter($(this), selectLabel, filterInMobile);
      }

      function setupFilter(element, defaultSelectLabel, applyFilterFnc) {
        let filterSelect = element.find('.select-checkbox-filter');
        let selectBtn = element.find('.select');
        let placeHolder = selectBtn.find('span')
        // desktop options
        let showedCheckbox = element.find('.checkbox-list');
        let options = filterSelect.find('.checkbox .type input');
      
        // mobile options
        let popupContent = filterSelect.find('.popup-content');
        let mobileOptions = popupContent.find('.type input');
        let mobileFilterContent = popupContent.find('.content');
        let applyButton = popupContent.find('.btn button');
        // variables
        const currentYear = new Date().getFullYear();
        let selectedYears = [currentYear].map(e => `${e}`);
        let selectedQuaters = ['Q1', 'Q2', 'Q3', 'Q4'];
        let tmpYears = selectedYears;
        let tmpQuaters = selectedQuaters;
      
        const onOptionsChanged = (_options, oldYears) => {
          let yearOptions = _options.filter(function() { return $(this).parents('.year').length > 0; });
          let quarterOptions = _options.filter(function() { return $(this).parents('.quarter').length > 0; });
      
          if (oldYears.length == 0) {
            quarterOptions.prop('checked', true);
            quarterOptions.prop('disabled', false);
          }
          let newSelectedYears = yearOptions.filter(':checkbox:checked').map(function() { return $(this).val() }).get();
          if (newSelectedYears.length == 0) {
            quarterOptions.prop('checked', false);
            quarterOptions.prop('disabled', true);
          }
          let newSelectedQuarters = quarterOptions.filter(':checkbox:checked').map(function() { return $(this).val() }).get();
          return [newSelectedYears, newSelectedQuarters];
        }
      
        const cloneToMobile = () => {
          mobileFilterContent.empty();
          showedCheckbox.clone().appendTo(mobileFilterContent);
          mobileOptions = popupContent.find('.type input');
        }
      
        const updateFilters = () => {
          let allOptions = options.add(mobileOptions);
          allOptions.filter(function() { return $(this).parents('.year').length > 0 }).each(function() {
            $(this).prop('checked', false);
            if (selectedYears.includes($(this).val())) {
              $(this).prop('checked', true);
            }
          });
          allOptions.filter(function() { return $(this).parents('.quarter').length > 0 }).each(function() {
            $(this).prop('checked', false);
            if (selectedQuaters.includes($(this).val())) {
              $(this).prop('checked', true);
            }
          });
          
          if(selectedYears.length > 0){
            placeHolder.text(selectedYears.join(','));
            let inputInfoValue = $(filterSelect).find('.dropdownAnalytics').data().trackingClickInfoValue;
            let updatedValues = updateTrackingData(inputInfoValue, selectedYears, selectedQuaters);
            $(filterSelect).find('.dropdownAnalytics').attr("data-tracking-click-info-value", updatedValues);
          } else {
            placeHolder.text(defaultSelectLabel);
          }
        };
    
        function updateTrackingData(inputInfoValue,selectedYears,selectedQuaters){
          let inputJsonStr = inputInfoValue.replace(/'/g, '"');
          let inputJson = JSON.parse(inputJsonStr);
          inputJson.articleFilter = `{${selectedYears.join('|')}|${selectedQuaters.join('|')}}`;
          let updatedValues = JSON.stringify(inputJson).replace(/"/g, "'");
          return updatedValues;
        }
      
        updateFilters();
        cloneToMobile();
        // Desktop change options
        options.on('click', function() {
          let [years, quarters] = onOptionsChanged(options, selectedYears);
          selectedYears = years;
          selectedQuaters = quarters;
          if(selectedYears.length > 0){
            placeHolder.text(selectedYears.join(','));
            let inputInfoValue = $(this).parents(filterSelect).find('.dropdownAnalytics').data().trackingClickInfoValue;
            let updatedValues = updateTrackingData(inputInfoValue, selectedYears, selectedQuaters);
            $(this).parents(filterSelect).find('.dropdownAnalytics').attr("data-tracking-click-info-value", updatedValues);
            $(this).parents(filterSelect).find('.dropdownAnalytics').trigger('click')
          } else {
            placeHolder.text(defaultSelectLabel);
          }
          element.trigger('updateFilter', [selectedYears, selectedQuaters]);
        });
      
        // Mobile filter clicked
        selectBtn.on('click', function (e) {
          e.stopPropagation();
      
          if ($(window).width() > 767) {
            $(this).toggleClass('showed');
          } else {
            $(this).parent().toggleClass('mobile');
            $('body').addClass('overflow-hidden');
      
            updateFilters();
            tmpYears = selectedYears;
            tmpQuaters = selectedQuaters;
          }
        });
      
        $(document).on('click', function (event) {
          if(!event.target.classList.contains('dropdownAnalytics')){
            if (!$(event.target).is(showedCheckbox) && !$(event.target).parents().is(showedCheckbox)) {
              selectBtn.removeClass('showed');
            }
          }
          if (!$(event.target).is(popupContent) && !$(event.target).parents().is(popupContent)) {
            selectBtn.parent().removeClass('mobile');
            $('body').removeClass('overflow-hidden');
          }
        });
      
        popupContent.on('click', (event) => {
          event.stopPropagation();
        });
      
        mobileOptions.on('click', function() {
          let [years, quarters] = onOptionsChanged(mobileOptions, tmpYears);
          tmpYears = years;
          tmpQuaters = quarters;
        });
      
        applyButton.on('click', function () {
          selectBtn.parent().removeClass('mobile');
          $('body').removeClass('overflow-hidden');
          selectedYears = tmpYears;
          selectedQuaters = tmpQuaters;
          updateFilters();
          let inputInfoValue = $(this).parents(filterSelect).find('.dropdownAnalytics').data().trackingClickInfoValue;
          let updatedValues = updateTrackingData(inputInfoValue, selectedYears, selectedQuaters);
          $(this).parents(filterSelect).find('.dropdownAnalytics').attr("data-tracking-click-info-value", updatedValues);
          $(this).parents(filterSelect).find('.dropdownAnalytics').trigger('click')
          element.trigger('updateFilter', [selectedYears, selectedQuaters]);
          applyFilterFnc();
        });
      
        element.trigger('updateFilter', [selectedYears, selectedQuaters]);
      }
    });
  });

 
  