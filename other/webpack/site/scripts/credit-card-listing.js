$(document).ready(function () {
  let section = $('.credit-card-listing-section');
  let creditCardListing = $('.credit-card-listing');
  let creditCardListingContent = creditCardListing.find('.credit-card-listing__content');
  let creditCardListingContainer = creditCardListing.find('.credit-card-listing__container');
  let creditCardListingItems = creditCardListingContainer.find('.credit-card-listing__items');
  let creditCardListingButton = creditCardListingItems.find('.credit-card-listing__button');
  let listCardProduct = creditCardListing.find('.list-card-product__container');
  let listCardGridContainer = listCardProduct.find('.list-card-product__grid-container');
  let compareChoosing = creditCardListing.find('.compare-choosing');
  let listCardProductItem = listCardProduct.find('.list-card-product__grid-item');
  let alertBannerBtn = creditCardListing.find('.card-bank__actions .button');
  let alertBannerPopup = creditCardListing.find('.popup');
  let alertBannerPopupContent = alertBannerPopup.find('.popup-content');
  let closeBtn = alertBannerPopup.find('.close');
  let banner = listCardProduct.find('.card-product-banner');
  let addedLabel = creditCardListingContent.data('addedLabel');
  let addLabel = creditCardListingContent.data('addLabel');
  const compareLink = $(section).find('.compare-button__link').attr('href');
  const filterItems = $(section).attr('data-cardoptions')? JSON.parse($(section).attr('data-cardoptions')) : {};
  const cfPath = $(section).attr('data-cfPath');
  const prefer = $(section).attr('data-nudgeoptions')? JSON.parse($(section).attr('data-nudgeoptions')) : {};
  const dataRegisterLink = $(section).attr('data-registerLink');
  if ($(section).length > 0) {
    cardsRender(cfPath, prefer);
  }
  function cardsRender(cfPath, prefer, carTypes) {
    const cardIds = getCardIdFromUrl();
    getCards(cfPath, carTypes).then(function (dataResponse) {   
      dataResponse.forEach(function(item) {
        listCardGridContainer.append(createHtmlCard(item, prefer));
      });
      handlePopup();
      comareAction(cardIds);
      updateBannerPosition();
    });
  }

  function getCardIdFromUrl() {
    const currentUrl = window.location.href;
    const urlParams = new URLSearchParams(currentUrl.split('?')[1]);
    const cardIds = []; console.log(urlParams.getAll('cardId').length)
    if(urlParams.has('cardId')) {
      
      cardIds.push(...urlParams.getAll('cardId'));
    }
    return cardIds;
  }

  function handlePopup() {
    let alertBannerBtn = $('.credit-card-listing').find('.card-bank__actions .button');
    let alertBannerPopup = $('.credit-card-listing').find('.popup');

    $('.card-bank__actions .cta-button--link').click(function(){
      $(this).parents('.card-bank__actions').find('.analytics-learn-more').trigger("click");
    })
    $('.credit-card-listing .compare-button .compare-button__link').click(function(){
      $(this).parents('.credit-card-listing').find('.analytics-compare').trigger("click");
    })
    $('.credit-card-listing .card-bank__comparing-action .card-bank__button').click(function(){
      $(this).parents('.credit-card-listing').find('.analytics-add-compare').trigger("click");
    })
    alertBannerBtn.click(function (e) {
      $(this).parents('.card-bank__actions').find('.analytics-register').trigger("click");
  
      e.preventDefault();
      e.stopPropagation();
      

      if ($(window).width() <= 768) {
        window.open(dataRegisterLink, '_blank');
      } else {
        alertBannerPopup.addClass('open');
        $('body').addClass('overflow-hidden');
      } 
    });
  
    closeBtn.click(function () {
      alertBannerPopup.removeClass('open');
      $('body').removeClass('overflow-hidden');
    });
  }

  $(document).click(function (event) {
    if (!$(event.target).is(alertBannerPopupContent) && !$(event.target).parents().is(alertBannerPopupContent)) {
      alertBannerPopup.removeClass('open');
      $('body').removeClass('overflow-hidden');
    }
  });

  function filterListCallBack(button) {
    $(button).toggleClass('filter-selected');
    $('.list-card-product__grid-item').hide();
    let selectedButtons = $(creditCardListingItems).find('.filter-selected');
    if (selectedButtons.length === 0) {
      $('.list-card-product__grid-item').show();
    } else {
      selectedButtons.each(function (index, btnFilter) {
        let cardType = $(btnFilter).attr('data-card-type');
        $('.list-card-product__grid-item').each(function(index, element){
          const dataTypes = $(this).data('type').split(',').map(type => type.trim()); 
          if(dataTypes.includes(cardType)) {
            $(element).show(); 
          }
        })
      });
    }
    updateBannerPosition();
  }

  createFilterList(filterItems , creditCardListingItems, filterListCallBack);
  function comareAction(cardIds) {
    if (listCardProduct.length && compareChoosing.length) {
      let listCardProductGridItem = listCardProduct.find('.list-card-product__grid-item');
      let cardBankButtons = listCardProductGridItem.find('.card-bank__button[data-button-compare]');
      let cardBankButtonJsonObject = new Map();
  
      let listCardItem = compareChoosing.find('.list-card__item');
  
      listCardProductGridItem.each(function (index) {
        let cardProductGredItem = $(this);
        let cardBankButtonCurr = cardProductGredItem.find('.card-bank__button[data-button-compare]');
        let imageDataObj = jQuery.parseJSON(cardBankButtonCurr.attr('data-json'));
        cardBankButtonCurr.click(function () {
          if (!cardBankButtonCurr.hasClass('disabled') && !cardBankButtonCurr.hasClass('selected')) {
            if (compareChoosing.hasClass('hidden')) {
              compareChoosing.removeClass('hidden');
            }
            cardBankButtonJsonObject.set(imageDataObj.cardId, cardBankButtonCurr);
            listCardItem.each(function (index) {
              if (!$(this).hasClass('has-card')) {
                let currentImage = $(this).find('.list-card__item-image img');
                currentImage.attr('src', imageDataObj.cardImage);
                currentImage.attr('alt', imageDataObj.cardName);
                $(this).attr('data-card-id', imageDataObj.cardId);
                $(this).addClass('has-card');
                return false;
              } else {
                return true;
              }
            });
            cardBankButtonCurr.find('.card-bank__button__text').text(addedLabel);
            cardBankButtonCurr.addClass('selected');
            checkFullHasCard();
            handleComparingButton();
          }
        });

        if(cardIds.includes(imageDataObj.cardId)) {
          $(cardBankButtonCurr).trigger('click');
        }
      });
  
      listCardItem.each(function (i,cardItem) {
        let itemCard = $(cardItem);
        let removeButton = $(cardItem).find('.remove-button');
  
        removeButton.click(function () {
          let itemCardId = itemCard.attr('data-card-id');
          if (itemCard.next().hasClass('has-card')) {
            let nexCardFirst = itemCard.replaceImg();
            if (nexCardFirst.next().hasClass('has-card')) {
              let nexCardSecond = nexCardFirst.replaceImg();
              if (nexCardSecond.next().hasClass('has-card')) {
                nexCardSecond.replaceImg();
              } else {
                nexCardSecond.removeClass('has-card');
              }
            } else {
              nexCardFirst.removeClass('has-card');
            }
          } else {
            itemCard.removeClass('has-card');
          }
          removeSelectedButton(itemCardId, cardBankButtonJsonObject);
          checkNoHasCard();
          checkFullHasCard();
          handleComparingButton();
        });
      });
      jQuery.fn.replaceImg = function () {
        let elmNex = $(this).next();
        let nextImageSrcnNex2 = elmNex.find('img').attr('src');
        let nextImageAltNex2 = elmNex.find('img').attr('alt');
        let nextDataCardId = elmNex.attr('data-card-id');
        $(this).find('.list-card__item-image img').attr('src', nextImageSrcnNex2);
        $(this).find('img').attr('alt', nextImageAltNex2);
        $(this).attr('data-card-id', nextDataCardId);
        return elmNex;
      };
  
      function handleComparingButton () {
        const listCardItemHasCard = compareChoosing.find('.list-card__item.has-card');
        if (listCardItemHasCard.length > 1) {
          $('.compare-button__link').removeAttr('disabled');
        } else {
          $('.compare-button__link').attr('disabled','');
        }
        const queryStrings = [];
          listCardItemHasCard.each(function(i, comPareCard) {
            const cardId = $(comPareCard).attr('data-card-id');
            queryStrings.push(`cardId=${cardId}`);
          });
          const cardIds =( queryStrings.length > 0 )? `?${queryStrings.join('&')}` : '';
          $('.compare-button__link').attr('href',`${compareLink}${cardIds}`);
      }
      handleComparingButton();
      function checkNoHasCard() {
        if (compareChoosing.find('.list-card__item.has-card').length === 0) {
          compareChoosing.addClass('hidden');
        }
      }
  
      function checkFullHasCard() {
        if (compareChoosing.find('.list-card__item.has-card').length === listCardItem.length) {
          cardBankButtons.each(function () {
            if (!$(this).hasClass('selected')) {
              $(this).addClass('disabled');
            }
          });
        } else {
          cardBankButtons.each(function () {
            $(this).removeClass('disabled');
          });
        }
      }
  
      function removeSelectedButton(itemCardId, cardBankButtonJsonObject) {
        let refreshButton = cardBankButtonJsonObject.get(itemCardId);
        refreshButton?.find('.card-bank__button__text').text(addLabel);
        refreshButton?.removeClass('selected');
      }
    }
  }
  updateBannerPosition();
  function updateBannerPosition() {
    let visibleItems = $('.list-card-product__container').find(".list-card-product__grid-item:visible");
    if ($(window).width() > 992) {
      banner.insertAfter(visibleItems.eq(Math.min(2, visibleItems.length - 1)));
    } else {
      banner.insertAfter(visibleItems.eq(Math.min(3, visibleItems.length - 1)));
    }
  }
  $(window).resize(updateBannerPosition);

  function createFilterList(filterList, element, callback) {
    if(Array.isArray(filterList)){
      filterList?.forEach(function(item) {
        const button = $('<button>')
        .addClass('credit-card-listing__button')
        .attr('type','button')
        .attr('data-card-type', item.id)
        .text(item.title);
  
        button.on('click', function() {
          if(typeof callback === 'function') {
            callback(button);
          }
        });
  
        const div = $('<div>').addClass('filter__item').append(button);
  
        element.append(div);
        
      });
    }
  }

  function getCards(cfPath, carTypes) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/content/_cq_graphql/techcombank/endpoint.json',
        method: 'post',
        data: createQuery(cfPath, carTypes),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
          resolve(response?.data?.credit_debitCardDetailContentFragmentList?.items ?? []);
        },
        error: function (xhr, status, error) {
          reject(error)
        },
      });
    });
  }
});

function createQuery(cfPath, cardType) {
  const query = `query getCreditDebitCardList($cardType: StringArrayFilter, $cardId: IntFilter, $cfPath: ID, $logOp: LogOp) {
      credit_debitCardDetailContentFragmentList(
        filter: {cardType: $cardType, _path: {_expressions: [{value: $cfPath, _operator: STARTS_WITH}]}, _logOp: $logOp, cardId: $cardId}
      ) {
        items {
          cardId
          cardType
          cardNudgeType
          displaySpecialOffer
          cardImage {
            ... on ImageRef {
              _publishUrl
            }
          }
          cardBackgroundColor
          addToCompareLabel
          addedLabelText
          icon {
            ... on ImageRef {
              _publishUrl
            }
            ... on DocumentRef {
              _publishUrl
            }
          }
          iconAtlText
          cardTitle
          notesText
          findoutLabelText
          findoutMoreInternalLink {
            ... on PageRef {
              _publishUrl
            }
          }
          findoutMoreExternalLink
          findoutMoreTarget
          findoutMoreNoFollow
          compareTextDetailsFor {
            html
          }
          outStandingOffers {
            html
          }
          utilities {
            html
          }
          appilicationConditions {
            html
          }
          feeInterest {
            html
          }
          cardOffers {
            html
          }
          learnMoreLabelText
          learnMoreInternalLink {
            ... on PageRef {
              _publishUrl
            }
          }
          learnMoreExternalLink
          learnMoreTarget
          learnMoreNoFollow
          registerNowLabel
          registerNowCTA
          applynowLabelText
        }
      }
    }
    `;
  let body = {
    query,
  }
  const variables = {
    logOp: 'OR',
    cfPath
  };
  if (cardType?.length > 0) {
    variables.cardType = {
      _logOp: 'OR',
      _expressions: cardType.map(e => ({value: e}))
    }
  }
  body.variables = variables;
  return JSON.stringify(body);
}

function modifyLink(link) {
  
  if(!link) {
    return undefined;
  }

  return link.replace(/\.html$/,'');
}

function createHtmlCard(cardItem, prefer) {
  let webInteractionType  = 'Other';
  let section = $('.credit-card-listing-section');
  let isMobile = $(window).width()
  if(isMobile<=768){
    const hrefVal = section.data('registerlink');
    if (hrefVal.includes("techcombank.com")) {
      webInteractionType = 'Other';
    }else if(hrefVal.includes(".pdf")){
      webInteractionType = 'Download';
    } else {
      webInteractionType = 'Exit';
    }
  }
  const preferItem = Array.isArray(prefer)? prefer?.filter((item) => item?.id === cardItem?.cardNudgeType[0]): [];
  const componentName = section.data("componentName");
  const compareText = $(section).find('.compare-button__link').text().trim();
  const registerButton = `
      <div class="button cta-button--light">
        <a class="cta-button" href="#" target="_self">
            <span class="cmp-button__text">${cardItem.registerNowLabel}</span>
            <picture>
                <img aria-hidden="true"
                    class="cmp-button__icon"
                    src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg">
            </picture>
        </a>
      </div>
  `;
  const findOutButton =`
    <a class="cta-button cta-button--link" href="${ modifyLink(cardItem.findoutMoreInternalLink?._publishUrl) ?? cardItem.findoutMoreExternalLink ?? '#'}" target="_self">
      <span class="cmp-button__text">${cardItem.findoutLabelText}</span>
      <picture>
          <img aria-hidden="true"
              class="cmp-button__icon" src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg">
      </picture>
    </a>
  `;

  let applyNowWebInteractionType = cardItem.findoutMoreExternalLink? "exit":"Other";
  let registerNowCtaWebInteractionType = cardItem.registerNowCTA? "exit":"Other";

  const prefix = `<div
      class="list-card-product__grid-item"
      data-id="${cardItem.cardId}"
      data-type=${cardItem.cardType}
      >
    <article class="card-bank__card">`;
    const sufix =`<div class="card-bank__image-wrapper" style="background: ${cardItem.cardBackgroundColor};">
    <div class="card-bank__image">
        <img alt="Thẻ tín dụng Techcombank Visa Signature" class="card-image"
            data-nimg="fixed"
            decoding="async"
            src="${cardItem.cardImage._publishUrl}"/>
    </div>
  </div>
  <div class="card-bank__comparing-action">
    <button class="card-bank__button card-bank__button-white card-bank__button__box-shadow"
            data-button-compare
            data-json='
            {
            "cardId": "${cardItem.cardId}",
            "cardName": "${cardItem.cardTitle}",
            "cardImage": "${cardItem.cardImage._publishUrl}"
            }' 
      type="button">
        <span class="card-bank__button__text"> ${cardItem.addToCompareLabel}</span>
        <div class="button__icon-svg">
            <img alt=""
                class="icon-svg-add"
                src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/add-icon.svg">
            <img alt=""
                class="icon-svg-checked"
                src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/checked-icon.svg">
        </div>
    </button>
  </div>
  <div class="card-bank__info">
    <h6 class="card-bank__name">${cardItem.cardTitle}</h6>
    <div class="card-bank__body-info">
        <div class="card-bank__description">
            <ul>
              ${cardItem.notesText ? cardItem.notesText.map((item) => `<li>${item}</li>`).join('') : ''}
            </ul>
        </div>
        <div class="card-bank__actions">
            ${cardItem.registerNowLabel? registerButton : ''}
            ${(cardItem.findoutMoreInternalLink || cardItem.findoutMoreExternalLink)? findOutButton : ''}
        <input type="hidden" class="analytics-register"  data-tracking-click-event="cta"
        data-tracking-click-info-value="{'cta':'${cardItem.registerNowLabel}'}" data-tracking-web-interaction-value="{'webInteractions': {'name': '${isMobile<=768?componentName:cardItem.cardTitle}','type': '${webInteractionType}'}}" />
        
        <input type="hidden" class="analytics-learn-more" data-tracking-click-event="linkClick"
        data-tracking-click-info-value="{'linkClick':'${cardItem.findoutLabelText}'}" data-tracking-web-interaction-value="{'webInteractions': {'name': '${componentName}','type': '${applyNowWebInteractionType}'}}"/>

        <input type="hidden" class="analytics-add-compare" data-tracking-click-event="linkClick"
        data-tracking-click-info-value="{'linkClick':'${cardItem.addToCompareLabel}'}" data-tracking-web-interaction-value="{'webInteractions': {'name': '${componentName}','type': 'Other'}}"/>

        <input type="hidden" class="analytics-compare" data-tracking-click-event="linkClick"
        data-tracking-click-info-value="{'linkClick':'${compareText}'}" data-tracking-web-interaction-value="{'webInteractions': {'name': '${componentName}','type': 'Other'}}"/>
        </div>
    </div>
  </div>
  </article>
  </div>`;

  if(preferItem.length > 0 && cardItem.displaySpecialOffer){
    return prefix + `<label class="card-bank__label">`+ preferItem[0].title +`</label>` + sufix;
  }else {
    return prefix +sufix;
  }
}