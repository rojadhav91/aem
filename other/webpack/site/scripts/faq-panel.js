// Card Product Filter component
$(document).ready(function () {
  var cardFilter = $('.filter-panel');
  var cardFilterContainer = cardFilter.find('.filter-panel__container');
  var cardFilterItems = cardFilterContainer.find('.filter-panel__items');
  var cardFilterButton = cardFilterItems.find('.filter-panel__button');

  updateBackground();
  cardFilterButton.each(function () {
    $(this).click(function () {
      if ($(this).hasClass('filter-selected')) {
        if (this.getAttribute('data-id') != 'all') {
          $(this).removeClass('filter-selected');
        }
        let hasSelected = false;
        for (let card = 1; card < cardFilterButton.length; card++) {
          if ($(cardFilterButton[card]).hasClass('filter-selected')) {
            hasSelected = true;
          }
        }
        if (!hasSelected) {
          $(cardFilterButton).first().addClass('filter-selected');
        }
      } else {
        $(this).addClass('filter-selected');
      }
      $('.filter-panel-container').hide();
      cardFilterButton.each((index, item) => {
        if (
          this.getAttribute('data-id') != 'all' &&
          item.getAttribute('data-id') == 'all'
        ) {
          let hasSelected = false;
          for (let card = 1; card < cardFilterButton.length; card++) {
            if ($(cardFilterButton[card]).hasClass('filter-selected')) {
              hasSelected = true;
            }
          }
          if (!hasSelected) {
            $(cardFilterButton).first().addClass('filter-selected');
          } else {
            $(item).removeClass('filter-selected');
          }
        } else if (
          this.getAttribute('data-id') == 'all' &&
          item.getAttribute('data-id') != 'all'
        ) {
          $(item).removeClass('filter-selected');
        }
        $('.filter-panel-container').each((i, element) => {
          if (
            item.getAttribute('data-id') == 'all' &&
            $(item).hasClass('filter-selected')
          ) {
            $(element).show();
          }
          if (
            element.getAttribute('data-id') == item.getAttribute('data-id') &&
            $(item).hasClass('filter-selected')
          ) {
            $(element).show();
          }
        });
      });
      if (!cardFilterItems.find('.filter-selected').length) {
        $('.filter-panel-container').show();
      }
      updateBackground();
    });
  });

  function updateBackground() {
    const allItems = $('.filter-panel-container');
    const visibleItems = allItems.filter(function () {
      return $(this).css('display') !== 'none';
    });
    visibleItems.each((index, element) => {
      if (index % 2 !== 0) {
        $(element).css('backgroundColor', '#fff');
      } else {
        $(element).css('backgroundColor', '#f5f6f8');
      }
    });
  }
});
// End Card Product Filter components

// Question Answer component
$(document).ready(function () {
  let questionAnswer = $('.enhancedfaqpanel');
  
  var questionShoweMore = questionAnswer.find("[data-showmore]").attr('data-showmore');
  var questionShow = questionShoweMore;
  
  let faqPanel = $('.faq-panel')
  if(faqPanel.parent().hasClass('faqpanel')){
    questionAnswer = faqPanel
    questionShoweMore = questionAnswer.find('data-showmore');
  }
  
  questionAnswer.each(function () {
    let listQA = $(this).find('.enhancedfaq');
    if(faqPanel.parent().hasClass('faqpanel')){
      listQA = $(this).find('.answer-question');
    }
    let question = $(this).find('.question');
    let questionAnswerBtn = $(this).find('.read-more');
    // function showhide awswer section
    for (let i = 0; i < question.length; i++) {
      question[i].addEventListener('click', function () {
        this.classList.toggle('active');
        let expand = this.lastElementChild;
        let answer = this.nextElementSibling;
        let answerHeight = answer.scrollHeight + 'px';
        if (answer.style.height === answerHeight) {
          answer.style.marginBottom = '0px';
          answer.style.height = '0px';
          expand.style.transform = 'rotate(0deg)';
        } else {
          answer.style.marginBottom = '24px';
          answer.style.height = answerHeight;
          expand.style.transform = 'rotate(-180deg)';
        }
      });
    }

    let j = 5;
    let step = j + 4;

    if (listQA.length <= 5) {
      questionAnswerBtn.css('display', 'none');
    } else {
      $(listQA[4]).css('border', 'none');
    }

    questionAnswerBtn.click(function () {
      if (step >= listQA.length) {
        for (j; j < listQA.length; j++) {
          listQA[j].style.display = 'block';
        }

        questionAnswerBtn.css('display', 'none');
      } else {
        for (j; j <= step; j++) {
          listQA[j].style.display = 'block';
        }
      }
      questionShow += parseInt(questionShoweMore);
      step += parseInt(questionShoweMore);
      listQA.each(function (i) {
        if (i < questionShow) {
          $(this).find('.question').removeClass('active');
          $(this).find('img').css('transform', 'rotate(0deg)');
          $(this).find('.answer').css('marginBottom', '0');
          $(this).css('border-bottom', '1px solid rgba(128, 128, 128, 0.363)');
        }
        if (i == questionShow - 1 || i == listQA.length - 1) {
          $(this).css('border', 'none');
        }
      });
	  
    });
  });
});

$(document).ready(function () {
  $('.card-product-banner').each(function () {
    $(this)
      .find('.card-product-banner__close')
      .click(function () {
        $(this).parent().parent().parent().remove();
      });
  });
});
