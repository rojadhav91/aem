$(document).ready(function () {
  /* Header JS*/
  const headerTabItemActiveIndex = $('.tab_item.active').index();

  $('#viewAllButton').on('click', function () {
    const sublist = $(this).siblings('.navigation_sub_list');
    sublist.toggleClass('expanded');
  });

  $('#login-btn:not(.inspire)').click(function () {
    const dropdown = event.target.closest('#login-btn');
    const listener = (event) => {
      if (!dropdown.contains(event.target)) {
        dropdown.classList.remove('active');
        window.removeEventListener('click', listener);
      }
    };
    if (dropdown.classList.contains('active')) {
      dropdown.classList.remove('active');
      window.removeEventListener('click', listener);
    } else {
      dropdown.classList.add('active');
      window.addEventListener('click', listener);
    }
  });
  $('#login-btn1:not(.inspire)').click(function (event) {
    const dropdown = event.target.closest('#login-btn1');
    const listener = (event) => {
      if (!dropdown.contains(event.target)) {
        dropdown.classList.remove('active');
        window.removeEventListener('click', listener);
      }
    };
    if (dropdown.classList.contains('active')) {
      dropdown.classList.remove('active');
      window.removeEventListener('click', listener);
    } else {
      dropdown.classList.add('active');
      window.addEventListener('click', listener);
    }
  });
  $('.navigation-secondary_item:not(.inspire)').mouseover(function () {
    const index = $(this).index();
    const content = $('.navigation_sub_item')[index];
    $('.navigation_sub_item.active').not(content).removeClass('active');
    const tabItem = $('.tab_item');
    tabItem.removeClass('active');
    if (content && !$(content).hasClass('active')) {
      $(content).addClass('active');
      $(content).find('.tab_content').first().addClass('active');
    }
    const tabActivePar = tabItem.parents('.navigation_sub_item')[0];
    $('.navigation_sub_item.active').find('.tab_item:first-child').addClass('active');
    $($(tabActivePar).find('.tab_content.active')).removeClass('active');
    $($(tabActivePar).find('.tab_content')[headerTabItemActiveIndex]).addClass('active');
  });

  $('body').mouseout(function (e) {
    if (
      !$(e.target).hasClass('navigation-secondary_menu') &&
      $(e.target).parents('.navigation-secondary_menu').length === 0 &&
      !$(e.target).hasClass('navigation_sub') &&
      $(e.target).parents('.navigation_sub').length === 0
    ) {
      $('.navigation_sub_item').removeClass('active');
      $('.tab_content.active').removeClass('active');
      $('.tab_item').removeClass('active');
    }
  });

  $('.tab_item').mouseover(function (event) {
    $('.tab_item.active').removeClass('active');
    const tabItems = $(this).parents('.navigation_sub_item')[0];
    const current = event.target.closest('.tab_item');
    current.classList.add('active');
    const index = $(this).index();
    if (tabItems) {
      const content = $(tabItems).find('.tab_content')[index];
      $('.tab_content.active').not(content).removeClass('active');
      if (content && !$(content).hasClass('active')) {
        $(content).addClass('active');
      }
    }
  });

  $('.language_item .link_component-link').click(function () {
    $('.language_item .link_component-link').removeClass('black_text');
    $(this).addClass('black_text');
  });

  $('.navigation-primary_item-dropdown_list').click(function (event) {
    const dropdown = event.target.closest('.navigation-primary_item-dropdown_list');
    const listener = (event) => {
      if (!dropdown.contains(event.target)) {
        dropdown.classList.remove('open');
        window.removeEventListener('click', listener);
      }
    };
    if (dropdown.classList.contains('open')) {
      dropdown.classList.remove('open');
      window.removeEventListener('click', listener);
    } else {
      dropdown.classList.add('open');
      window.addEventListener('click', listener);
    }
  });

  $('.language_dropdown_item').click(function (event) {
    const dropdown = event.target.closest('.language_dropdown_item');
    const listener = (event) => {
      if (!dropdown.contains(event.target)) {
        dropdown.classList.remove('open');
        window.removeEventListener('click', listener);
      }
    };
    if (dropdown.classList.contains('open')) {
      dropdown.classList.remove('open');
      window.removeEventListener('click', listener);
    } else {
      dropdown.classList.add('open');
      window.addEventListener('click', listener);
    }
  });

  $('.navigation_sub .discover_dropdown_btn').click(function () {
    const dropdown = this.getElementsByClassName('discover_dropdown')[0];
    if ($(dropdown).hasClass('dropdown_open')) {
      $(dropdown).removeClass('dropdown_open');
    } else {
      $(dropdown).addClass('dropdown_open');
    }
  });

  $('.navigation-secondary_item').click(function () {
    $('.navigation-secondary_item').not($(this)).removeClass('active');
    if ($(this) && !$($(this)).hasClass('active')) {
      $($(this)).addClass('active');
    }
  });

  // sidebar menu in inspire
  $('.sidebar-menu_item').click(function () {
    const index = $(this).index();
    $('.navigation-secondary_item.inspire')[index].click();
  });

  // JS for mobile
  $('.mobile-button').click(function () {
    $('.mobile-main-nav').toggleClass('active');
  });

  // JS for mobile
  // $('.mobile-button').click(function () {
  //   $('.mobile-main-nav').toggleClass('active');
  // });
  $('.mobile-expand').click(function () {
    $('.mobile-main-nav').toggleClass('active');
  });
  $('.hambuger-icon').click(function () {
    $('#login-btn').removeClass('active');
    $('.mobile-main-nav').removeClass('active');
    $('.header-navigation').toggleClass('active');
    $('.navigation_sub').toggleClass('active');
    $('.navigation_sub_item').removeClass('active');
    $('.open_submenu').removeClass('open_submenu');
    $('body').removeClass('show-popup');
    if ($('.mobile-menu .navigation-primary_right').length == 0) {
      $('.navigation-secondary_menu:not(.inspire)').clone().appendTo('.mobile-menu-items');
      $('.navigation-primary_right').clone(true).appendTo('.mobile-menu-items');
    }
    $('.tab_item .material-symbols-outlined').html('add');
  });
  $('.mobile-menu').on('click', '.navigation-secondary_item.secondary-nav', function () {
    if ($(this).find('.material-symbols-outlined').length == 0) return;
    $('.navigation_sub').toggleClass('open_submenu');
    $('body').toggleClass('show-popup');
    const index = $(this).index();
    const content = $('.navigation_sub_item')[index];
    const spantext = $(this).find('span')[0];
    $('.back-menu-text').html($(spantext).html());
    $('.navigation_sub_item.active').not(content).removeClass('active');
    if (content && !$(content).hasClass('active')) {
      $(content).addClass('active');
      $(content).find('.tab_content').first().addClass('active');
    }
  });

  $('.navigation_sub_wraper').on('click', '.tab_item', function () {
    const icon = $(this).find('.material-symbols-outlined');
    $('.tab_item .navigation_sub_list').remove();
    $('.tab_item .navigation_tab_bottom').remove();
    if ($(icon).html() === 'add') {
      $('.tab_item .material-symbols-outlined').not(icon).html('add');
      $(icon).html('remove');
      const index = $(this).index();
      const tabItems = $(this).parents('.navigation_sub_item')[0];
      const content = $(tabItems).find('.tab_content')[index];
      const items = $(content).find('.navigation_sub_list')[0];
      const bottom = $(content).find('.navigation_tab_bottom')[0];
      $(items).clone().appendTo(this);
      $(bottom).clone().appendTo(this);
    } else if ($(icon).html() === 'remove') {
      $(icon).html('add');
    }
  });
  $('.navigation_sub_wraper').on('click', '.back_menu', function () {
    $('.open_submenu').removeClass('open_submenu');
    const icon = $(this).find('.material-symbols-outlined');
    $('.tab_item .material-symbols-outlined').not(icon).html('add');
    $('.tab_item .navigation_sub_list').remove();
    $('.tab_item .navigation_tab_bottom').remove();
  });

  $(window).resize(function () {
    $('.mobile-main-nav.active').removeClass('active');
    $('.header-navigation.active').removeClass('active');
    $('.navigation_sub.active').removeClass('active');
    $('.tab_content.active').removeClass('active');
    $('.navigation_sub_item.active').removeClass('active');
    $('.open_submenu').removeClass('open_submenu');
    $('.tab_item .navigation_tab_bottom').remove();
    $('.tab_item .navigation_sub_list').remove();
    $('.mobile-expand .navigation-primary_left').remove();
    $('.mobile-menu-items .navigation-secondary_menu').remove();
    $('.mobile-menu-items .navigation-primary_right').remove();
    $('.tab_item .material-symbols-outlined').html('arrow_forward');
    $('body').removeClass('show-popup');
  });
  /* END Header JS*/

  /* Header inspire */
  $('.navigation-secondary_menu-inspire .link_component-link').click(function (e) {
    e.stopPropagation();

    $('.header-popup').css('display', 'block');
    $('body').addClass('overflow-hidden');

    $('.header-popup-content').addClass('hide-form');
    $('.header-popup-content').removeClass('show-form');

    $('.header-popup-content .radio input[type="radio"]').prop('checked', false);
  });

  $(document).click(function (event) {
    if (!$(event.target).is($('.header-popup-content')) && !$(event.target).parents().is($('.header-popup-content'))) {
      $('.header-popup').css('display', 'none');
      $('body').removeClass('overflow-hidden');
    }
  });

  $('.header-close-form').click(function () {
    $('.header-popup').css('display', 'none');
    $('body').removeClass('overflow-hidden');
  });

  $('.header-popup-content').on('click', (event) => {
    event.stopPropagation();
  });

  $('#header-form-accept_condition').prop('checked', true);

  $('.header-popup-content .radio input[type="radio"]').each(function () {
    $(this).click(function () {
      $('.header-popup-content').addClass('show-form');
      $('.header-popup-content').removeClass('hide-form');

      $('.header-popup-content .step-guide').css('display', 'none');
      $(this).parent().find('.step-guide').css('display', 'flex');
    });
  });

  /* Inspire Sidebar Menu */
  $('.sidebar-menu .sidebar-menu_chat .float-chat-icon').click(function (e) {
    showRegisterModal(e);
  });
  const showRegisterModal = (e) => {
    $('.header-popup').css('display', 'block');
    $('body').addClass('overflow-hidden');

    $('.header-popup-content').addClass('hide-form');
    $('.header-popup-content').removeClass('show-form');

    $('.header-popup-content .radio input[type="radio"]').prop('checked', false);
  };

  /**
   * Footer JS
   */
  let timer;
  $('.footer-expand').on('click', function () {
    var showHide = document.getElementById('footer-links__list');
    var expand = this.lastElementChild;
    if (timer) {
      clearTimeout(timer);
    }
    if (!showHide.classList.contains('expanded')) {
      showHide.classList.add('expanded');
      expand.style.transform = 'rotate(-180deg)';
    } else {
      showHide.classList.remove('expanded');
      expand.style.transform = 'rotate(-360deg)';
    }
    timer = setTimeout(() => {
      $(document.body).animate(
        {
          scrollTop: document.body.scrollHeight,
        },
        400
      );
    }, 300);
  });
});
