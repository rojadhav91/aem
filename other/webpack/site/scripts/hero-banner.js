const heroBanners = [];

window.HeroBanner = class HeroBanner {
  wrapper;

  constructor(banner) {
    this.wrapper = banner;
    this.init();
  }

  init() {
    if (!this.wrapper) {
      return;
    }

    const wrapperElement = this.wrapper;
    const menu = wrapperElement.querySelector('.page-menu');

    const nextButton = wrapperElement.querySelector('.tcb-hero-banner_control--next');

    const prevButton = wrapperElement.querySelector('.tcb-hero-banner_control--prev');
    if (menu === null) {
        return;
    }
    const screenWidth = window.innerWidth;
    if (menu.scrollWidth < screenWidth) {
        nextButton.style.display = 'none';
    }
    menu.addEventListener('scroll',() => {
        const atEnd = menu.scrollLeft >= menu.scrollWidth - menu.offsetWidth - 1;
        if(menu.scrollLeft === 0) {
            prevButton.style.display = 'none';
        } else {
            prevButton.style.display = 'block';
        }
        if(atEnd) {
            nextButton.style.display = 'none';
        } else {
            nextButton.style.display = 'block';
        }
    })

    nextButton.addEventListener('click', () => {
        menu.scrollLeft = menu.scrollWidth;
    });

    prevButton.addEventListener('click', () => {
        menu.scrollLeft = 0;
    });
  }

};
const sliderElements = document.querySelectorAll('.tcb-hero-banner');
sliderElements.forEach((item) => {
  heroBanners.push(new HeroBanner(item));
});


//Apply active class on hero banner tabs if active class is not there
$(document).ready(function () {
  $(window).on('resize', function () {
    const menu = document.querySelector('.page-menu');
    const nextControl = document.querySelector('.tcb-hero-banner_control--next');
    if (menu) {
      menu.scrollLeft = 0;
    }
    menu.scrollWidth < window.innerWidth
      ? nextControl.style.display = 'none'
      : nextControl.style.display = 'block';
  });
  var hasClass = false;
  $(".tcb-hero-banner_bottom-content ul.page-menu li a").each(function () {
    if ($(this).hasClass("page-menu_item--active")) {
      hasClass = true;
      return false; // Exit the loop if the class is found on any <a> tag
    }
  });

  if (!hasClass) {
    // Apply the class to the first <a> tag
    $(".tcb-hero-banner_bottom-content ul.page-menu li:first a").addClass(
      "page-menu_item--active"
    );
  }

  //Apply the active class if tab href starts with #
  $(".tcb-hero-banner_bottom-content ul.page-menu li a").click(function () {
    var href = $(this).attr("href");
    if (typeof href !== "undefined" && href.startsWith("#")) {
      $(".tcb-hero-banner_bottom-content ul.page-menu li a").removeClass(
        "page-menu_item--active"
      );
      $(this).addClass("page-menu_item--active");
    }
  });
});
