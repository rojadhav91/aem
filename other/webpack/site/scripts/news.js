/**
 * Select tab filter event
 */
$(".offer-filter__tab")
  .find("button")
  .each(function () {
    $(this).click(() => {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
      } else {
        $(this).addClass("active");
      }
    });
  });

/**
 * Load more button event
 */
const promotionFilterLarge = $(".news-filter");
const dialogFilter = $(".dialog");
if (promotionFilterLarge.length) {
  var newList = promotionFilterLarge.find(".news-list");
  var loadMoreButton = $(".news-filter").find(".load-more__button");
  var listTag = promotionFilterLarge.find(
    '.news_filter-group input[type="checkbox"]'
  );
  var listTagMobile = promotionFilterLarge.find(
    '.category-filter-mobi input[type="checkbox"]'
  );
  var listRadio = promotionFilterLarge.find(
    '.offer-filter__checkbox-item input[type="radio"]'
  );
  var listCategory = promotionFilterLarge.find(".category-filter button");
  var listCategoryMobile = promotionFilterLarge.find(
    ".category-filter-mobi .btn"
  );
  var stickyOpenFilter = promotionFilterLarge.find("#sticky");
  const btnOpenFilter = promotionFilterLarge.find(".btn-open-filter");

  var btnCloseFilter = promotionFilterLarge.find(".btn-close");
  var content = promotionFilterLarge[0].getAttribute("data-articleCategory")
    ? promotionFilterLarge[0].getAttribute("data-articleCategory")
    : "";
  var category = promotionFilterLarge[0].getAttribute("data-articletag")
    ? promotionFilterLarge[0].getAttribute("data-articletag")
    : "";
  var limit = promotionFilterLarge.attr("data-limit");
  var dataDefaultImage = promotionFilterLarge.attr("data-defaultImage");
  var notFound = promotionFilterLarge.find(".not-found");
  var ctaLabel = promotionFilterLarge.attr("data-ctalabel");
  var newTab =
    promotionFilterLarge.attr("data-newtab") != true ? "_blank" : "_self";
  var noFollow =
    promotionFilterLarge.attr("data-nofollow") != true ? "nofollow" : "";
  var offset = 0;
  var checkFilter = false;
  var showItem = 0;
  var latestQuery = "";
  var dataUrl = promotionFilterLarge.attr("data-url");
  var urlParams = location.search;
  var url = new URL(window.location);
  setFilter();
  getData(dataUrl + `?limit=${limit}&offset=${offset}&` + urlParams)
    .then(function (dataResponse) {
      handleDataResponse(dataResponse);
    })
    .catch(function (error) {
      console.log(error);
    });
  listTag.on("change", function () {
    changeFilter(listTag, listCategory);
	$(".news-filter .category-filter").trigger('click'); //analytics trigger click event
  });
  listTagMobile.on("change", function () {
    changeFilter(listTagMobile, listCategoryMobile);
  });
  listCategory.on("click", function () {
    setTimeout(function () {
      changeFilter(listTag, listCategory);
    }, 100);
  });
  listCategoryMobile.on("click", function () {
    setTimeout(function () {
      changeFilter(listTagMobile, listCategoryMobile);
    }, 100);
  });
  loadMoreButton.on("click", function () {
    let cards = parseInt(promotionFilterLarge.attr("data-limit"), 10);
    limit += cards;
    const api =
      dataUrl +
      `?limit=${limit.split('').reduce((acc, digit) => acc + parseInt(digit, 10), 0)}&offset=${offset}&` +
      latestQuery;
    getData(api)
      .then(function (dataResponse) {
        handleDataResponse(dataResponse);
      })
      .catch(function (error) {
        console.log(error);
      });
  });
  btnOpenFilter.on("click", function () {
    handleOpenModal();
  });
  stickyOpenFilter.on("click", function () {
    handleOpenModal();
  });
  btnCloseFilter.on("click", function () {
    handleRemoveModal();
  });
  $(window).on("click", function (e) {
    if ($(e.target).hasClass("tcb-button")) {
      handleRemoveModal();
      callAPI("");
    }
  });
}

if (dialogFilter.length) {
  let btnCloseFilter = dialogFilter.find(".btn-close");
  btnCloseFilter.on("click", function () {
    handleRemoveModal();
  });
}

//close modal when rotate
$(window).on("orientationchange", function () {
  handleRemoveModal();
});

function handleOpenModal() {
  $("section").css({
    display: "none",
  });
  $(".dialog").css("display", "");
  $(".breadcrumb").css("display", "none");
  $(".mastheadsimplebanner").css("display", "none");
}

function handleRemoveModal() {
  $("section").css("display", "");
  $(".dialog").css({
    display: "none",
  });
  $(".breadcrumb").css("display", "block");
  $(".mastheadsimplebanner").css("display", "");
}

function changeFilter(tags, categories) {
  limit = promotionFilterLarge.attr("data-limit");
  showItem = 0;
  let checkedBox = new Set();
  var articleTag;
  tags.each((index, item) => {
    if ($(item).is(":checked") && item.getAttribute("data-articletag")) {
      var tag = item.getAttribute("data-articletag").replace(/-/g, ' ');
      checkedBox.add(tag);
    }
  });
  articleTag = checkedBox.size > 0 ? Array.from(checkedBox).join() : "";
  let checkedCategory = new Set();
  var articleCategory;
  categories.each((index, item) => {
    if (
      $(item).hasClass("button-active") &&
      item.getAttribute("data-articlecategory")
    ) {
      checkedCategory.add(item.getAttribute("data-articlecategory").replace(/-/g, ' '));
    }
  });
  articleCategory =
    checkedCategory.size > 0 ? Array.from(checkedCategory).join() : "";
  checkFilter = true;
  articleTag
    ? url.searchParams.set("articleTag", articleTag)
    : url.searchParams.delete("articleTag");
  articleCategory
    ? url.searchParams.set("articleCategory", articleCategory)
    : url.searchParams.delete("articleCategory");
  window.history.replaceState({}, "", url);
  const fullApi = dataUrl + `?limit=${limit}&offset=0&` + url.searchParams.toString();
  latestQuery = url.searchParams.toString();
  getData(fullApi)
    .then(function (dataResponse) {
      handleDataResponse(dataResponse);
	  mergeFilters(); //analytics function call
    })
    .catch(function (error) {
      console.log(error);
    });
  mergeFilters(); //analytics function call
}

function handleDataResponse(dataResponse) {
  const total = parseInt(dataResponse.total);
  const stories = dataResponse.results;
  newList.empty();
  if (stories.length === 0) {
    notFound.css("display", "block");
  } else {
    notFound.css("display", "none");
    stories.forEach((item) => {
      newList.append(renderNewsList(item));
    });
  }
  total === stories.length || stories.length < limit.split('').reduce((acc, digit) => acc + parseInt(digit, 10), 0)
    ? loadMoreButton.hide()
    : loadMoreButton.show();
}

function setType(load) {
  var type = "pageLoaded.";
  if (checkFilter) {
    type = "";
  }
  callAPI(type, load);
}

function getData(url) {
  return new Promise(function (resolve, reject) {
    $.ajax({
      url: url,
      method: "get",
      success: function (response) {
        resolve(response);
      },
      error: function (xhr, status, error) {
        reject(error);
      },
    });
  });
}

function checkboxInit(item, checked) {
  listTag.each((index, tag) => {
    if (
      $(tag).hasClass("input__checkbox") &&
      tag.getAttribute("data-articletag") === item.replace(/\s+/g, '-')
    ) {
      $(tag).prop("checked", checked);
    }
  });
  listTagMobile.each((index, tag) => {
    if (
      $(tag).hasClass("input__checkbox") &&
      tag.getAttribute("data-articletag") === item.replace(/\s+/g, '-')
    ) {
      $(tag).prop("checked", checked);
    }
  });
}

function buttonInit(item, button) {
  if (
    $(button).hasClass("btn") &&
    button.getAttribute("data-articlecategory") === item.replace(/\s+/g, '-')
  ) {
    $(button).addClass("button-active");
  }
}

function setFilter() {
  const params = Object.fromEntries(new URLSearchParams(location.search));
  if (!isEmpty(params)) {
    var tagArr = params.articleTag ? params.articleTag.split(",") : "";
    var categoryArr = params.articleCategory
      ? params.articleCategory.split(",")
      : "";
    if (categoryArr !== "") {
      categoryArr.forEach((item) => {
        listCategory.each((index, cat) => {
          buttonInit(item, cat);
        });
        listCategoryMobile.each((index, cat) => {
          buttonInit(item, cat);
        });
      });
    }

    if (tagArr !== "") {
      tagArr.forEach((item) => {
        checkboxInit(item, true);
      });
    } else {
      listTagMobile.each((index, tag) => {
        if ($(tag).hasClass("input__checkbox")) {
          $(tag).prop("checked", false);
        }
      });
    }
  }
}

function isEmpty(obj) {
  return Object.keys(obj).length === 0;
}

function callAPI(load, type) {
  var uri = `${url}`;
  $.ajax({
    url: uri,
    type: "GET",
    dataType: "json",
    success: function (data) {
      if (type != "more") {
        newList.html("");
      }
      newList.removeClass("temp-placeholder");
      var resultTemplate = promotionFilterLarge.find(
        ".template-result .news-item__wrapper"
      );
      if (data.results) {
        data.results.forEach((item) => {
          var resultClone = resultTemplate.clone();
          var defaultThubnailImage = $(
            "section.news-filter.content-wrapper"
          ).attr("data-default-image"); //added default thumbnail image
          item.image
            ? resultClone
                .find(".news-image img")[0]
                .setAttribute("src", item.image)
            : resultClone
                .find(".news-image img")[0]
                .setAttribute("src", defaultThubnailImage);
          item.category
            ? (resultClone.find(
                ".news-content__header .title-header"
              )[0].innerHTML = item.category)
            : "";
          item.articleCreationDate
            ? (resultClone.find(
                ".news-content__header .date-header"
              )[0].innerHTML = item.articleCreationDate)
            : "";
          item.title
            ? (resultClone.find(".news-content__title")[0].innerHTML =
                item.title)
            : "";
          item.description
            ? (resultClone.find(".news-content__description")[0].innerHTML =
                item.description)
            : "";
          item.path
            ? resultClone
                .find(".news-content__link")[0]
                .setAttribute("href", `${item.path}`)
            : "";
          resultClone.appendTo(newList);
           mergeFilters(); //analytics function call
        });
      }
      newList.children().length < 1
        ? promotionFilterLarge.addClass("not-filter")
        : promotionFilterLarge.removeClass("not-filter");
      showItem += data.results ? data.results.length : "";
      if (data.total <= showItem) {
        loadMoreButton.hide();
      } else {
        loadMoreButton.show();
      }
    },
    error: function (error) {
      console.error(error);
      newList.removeClass("temp-placeholder");
    },
  });
   mergeFilters(); // analytics function call
}

// show sticky filter
$(document).ready(function () {
  const offerCardElement = $(".offer-cards__container");
  const stickyElement = $("#sticky");

  if (offerCardElement.length === 0 || stickyElement.length === 0) {
    return;
  }

  if ($(window).width() <= 767) {
    showStickyIcon();
  }
  $(window).on("resize", function () {
    showStickyIcon();
  });

  function showStickyIcon() {
    $("body").scroll(function () {
      if (
        offerCardElement.offset()?.top < 65 &&
        $(".container.news-filter").css("display") !== "none"
      ) {
        stickyElement.css("display", "");
      } else {
        stickyElement.css("display", "none");
      }
    });
  }
});

//click button
const buttonList = promotionFilterLarge.find(".btn");
buttonList.click(function () {
  if ($(this).hasClass("button-active")) {
    $(this).removeClass("button-active");
  } else {
    $(this).addClass("button-active");
  }
  mergeFilters(); //analytics function call
});

function renderNewsList(item) {
  return `
    <div class="news-item__wrapper">
      <div class="news-image">
        <img
          src=${item.image ? item.image : dataDefaultImage}
          alt=""
          srcset=""
        />
      </div>
      <div class="news-content">
        <div>
          <span class="news-content__header">
            ${renderArticleCategory(item.articleCategory)}
            <div></div>
            <div>${
              item.articleCreationDate ? item.articleCreationDate : ""
            }</div>
          </span>
          <span class="news-content__title">${item.title ? item.title : ""}
          </span>
          <div class="news-content__description">${
            item.description ? item.description : ""
          }
          </div>
        </div>
        <a
          href="${item.path}"
          target="${newTab}"
          rel="${noFollow}"
          class="news-content__link"
          data-tracking-click-event="articleView"
          data-tracking-web-interaction-value="{'webInteractions': {'name': '${
            item.title.replace(/["']/g,"")
          } ','type': 'other'}}"
          data-tracking-click-info-value="{'articleFilter':''}">
          <span>${ctaLabel}</span>
          <div class="news-content__link-icon">
            <img
              src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/arrow-right-red.png"
            />
          </div>
        </a>
      </div>
    </div>`;
}

function renderArticleCategory(articleList) {
  var article;
  article = Array.from(articleList).join(", ");
  return `
    <div>${article}</div>
  `;
}
// News Filter List
$(document).ready(function () {
  var d = new Date();
  var today = d.getDate();

  d = new Date(d.getFullYear(), d.getMonth(), 1);
  loadCal(d);

  function loadCal(d) {
    let day = d.getDay() === 0 ? 7 : d.getDay();
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    let lastday = new Date(year, month, 0).getDate();
    let isThisMonth = false;
    if (month === new Date().getMonth() + 1) {
      isThisMonth = true;
    }
    $(".news-filter .month-data").html("Tháng " + month + " " + year);
    let week1 = "";
    let week2 = "";
    let week3 = "";
    let week4 = "";
    let week5 = "";
    let week6 = "";
    for (var i = 1; i < day; i++) {
      week1 += "<span></span>";
    }
    for (var i = 1; i <= lastday; i++) {
      if (i + day <= 8) {
        week1 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      } else if (i <= 15 - day) {
        week2 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      } else if (i <= 22 - day) {
        week3 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      } else if (i <= 29 - day) {
        week4 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      } else if (i <= 36 - day) {
        week5 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      } else {
        week6 +=
          "<span><span class='day-item" +
          (today == i && isThisMonth ? " active" : "") +
          "'>" +
          i +
          "</span></span>";
      }
    }
    $(".news-filter .week.line1").html(week1);
    $(".news-filter .week.line2").html(week2);
    $(".news-filter .week.line3").html(week3);
    $(".news-filter .week.line4").html(week4);
    $(".news-filter .week.line5").html(week5);
    $(".news-filter .week.line6").html(week6);
  }
  $(".news-filter #prev-month").click(function () {
    d = new Date(d.getFullYear(), d.getMonth() - 1, 1);
    loadCal(d);
  });
  $(".news-filter #next-month").click(function () {
    d = new Date(d.getFullYear(), d.getMonth() + 1, 1);
    loadCal(d);
  });
  $(".news-filter .offer-filter__datepicker").on(
    "click",
    ".day-item",
    function (event) {
      today = $(event.target).html();
      $(".news-filter .day-item.active").not(this).removeClass("active");
      $(this).addClass("active");
    }
  );
});

function replaceURL(url) {
  return url.replace(/\//g, "_").replace(/,/g, "~");
}

//analytics - start
let sortedTypes;
let sortedCategories;

function captureNewsType() {
  let updatedType = [];
  if ($(window).width() > 767) {
    $(
      ".news_filter-group .offer-filter__checkbox input[type='checkbox']:checked"
    ).each(function () {
      updatedType.push($(this).next("div").text());
    });
    sortedTypes =
      updatedType && updatedType.length > 0 ? updatedType.join(" | ") : "";
  } else {
    $(
      ".category-filter-mobi .offer-filter__checkbox input[type='checkbox']:checked"
    ).each(function () {
      updatedType.push($(this).next("div").text());
    });
    sortedTypes =
      updatedType && updatedType.length > 0 ? updatedType.join(" | ") : "";
  }
}

function captureCategories() {
  var updatedCategories = [];

  if ($(window).width() > 767) {
    $(".category-filter .slide-button .btn.button-active").each(function () {
      updatedCategories.push($(this).text());
    });
    sortedCategories =
      updatedCategories && updatedCategories.length > 0
        ? updatedCategories.join(" | ")
        : "";
  } else {
    $(".category-filter-mobi .slide-button .btn.button-active").each(
      function () {
        updatedCategories.push($(this).text());
      }
    );
    sortedCategories =
      updatedCategories && updatedCategories.length > 0
        ? updatedCategories.join(" | ")
        : "";
  }
}

function mergeFilters() {
  captureNewsType();
  captureCategories();

  let typeClick = sortedTypes;
  let categoriesClick = sortedCategories;
  let updatedFilters = "";

  if (
    categoriesClick &&
    categoriesClick.trim().length > 0 &&
    typeClick &&
    typeClick.trim().length > 0
  ) {
    updatedFilters += categoriesClick.trim() + " | " + typeClick.trim(); // sequence for updatedFilters
  } else if (categoriesClick && categoriesClick.trim().length > 0) {
    updatedFilters += categoriesClick.trim();
  } else if (typeClick && typeClick.trim().length > 0) {
    updatedFilters += typeClick.trim();
  }

  //update filters in tracking code
  let articleFiltersTrackingDataValues = $(
    ".news-filter .category-filter"
  ).data().trackingClickInfoValue;
  let jsonStr = articleFiltersTrackingDataValues.replace(/'/g, '"');
  let json = JSON.parse(jsonStr);
  json.articleFilter = updatedFilters;
  let updatedValues = JSON.stringify(json).replace(/"/g, "'");

  //desktop interaction with filters
  $(".news-filter .category-filter").attr(
    "data-tracking-click-info-value",
    updatedValues
  );
  $(".news-filter .category-filter").trigger("click");

  //desktop : read more click of artical
  $(".news-item__wrapper .news-content__link").attr(
    "data-tracking-click-info-value",
    updatedValues
  );

  //desktop : view more click of articals
  $(".information-filter__load-more .load-more__button").attr(
    "data-tracking-click-info-value",
    updatedValues
  );

  //Mobile : apply filter click of articals
  $(".category-filter-mobi .dialog-content .btn-close").attr(
    "data-tracking-click-info-value",
    updatedValues
  );
}
//analytics - end
