export const calendarEvents = [
  {
    title: '30th CITIC CLSA Investors Forum',
    description:
      "CLSA annual Asia investor forum is most renowned conference for investors with thousands of institutional investors from around the globe in attendance.\nFor our 30th anniversary, they will bring together over 1,000 senior executives from 320 leading companies for meetings with more than 1,800 investors from around the world.\nTechcombank's representatives: Mr Jens Lottner - CEO \nMr Alex Macaire - CFO\n",
    eventType: 'Conference',
    startDate: '2023-09-11',
    endDate: '2023-09-14',
    openTime: '09:00:00',
    closeTime: '06:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Report-9-CLSA-07436240a2.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Report-9-CLSA-07436240a2.jpg',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
    hostedBy: 'Hosted by CLSA in Hong Kong'
  },
  {
    title: '1H23 Retail Investor Meeting',
    description:
      "A quarterly online event where Techcombank's executive team presents quarterly business results and answers investors' questions about the bank's performance and prospects. Register to attend the event for a chance to win a ticket to run Marathon in Hanoi on the occasion of Techcombank's 30th birthday",
    eventType: 'Analyst Presentation',
    startDate: '2023-07-26',
    endDate: '2023-07-26',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl:
      'https://forms.microsoft.com/Pages/ResponsePage.aspx?id=6A4wK-aYa0C_-NiZmIWkw5RjEdZEQQ9PrUMpdh5si9dUMjREWEZJQjEzUkowWjJYTTNCVllSSzVGUS4u',
    addToCalendarUrl: '',
  },
  {
    title: '1H23 Analyst Presentation',
    description:
      'This event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its financial results, outlook, strategy and business updates. ',
    eventType: 'Analyst Presentation',
    startDate: '2023-07-25',
    endDate: '2023-04-25',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl:
      'https://forms.microsoft.com/Pages/ResponsePage.aspx?id=6A4wK-aYa0C_-NiZmIWkw5RjEdZEQQ9PrUMpdh5si9dUOVlSUDlLQjVJUkJLOURYSUpGUzA0OEVZTy4u',
    addToCalendarUrl: '',
  },
  {
    title: '1H2023 Financial Statements (VAS) Disclosures ',
    description:
      "Techcombank's 1H2023 financial statements in accordance with the Vietnamese Accounting Standards (VAS)",
    eventType: 'Analyst Presentation',
    startDate: '2023-07-24',
    endDate: '2023-07-24',
    openTime: '15:00:00',
    closeTime: '15:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl: null,
    addToCalendarUrl: null,
  },
  {
    title: 'Invest ASEAN 2023 Investor Conference',
    description:
      'Venue: The Fullerton Hotel Singapore || Session: "ASEAN’s Rising Stars" || Time: 9.20am-10.20am || Jens Lottner, Chief Executive Officer as a panelist',
    eventType: 'Conference',
    startDate: '2023-06-20',
    endDate: '2022-06-21',
    openTime: '09:00:00',
    closeTime: '17:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Invest-Asean-2023-Logo-02-df2ee7b2ff.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Invest-Asean-2023-Logo-02-df2ee7b2ff.png',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: 'HSC Emerging Vietnam 2023 Investor Conference',
    description:
      'Venue: Le Méridien Saigon Hotel, 3C Ton Duc Thang Street, District 1, Ho Chi Minh City                   \n|| Session: "Digital transformation (DX) driving growth" || Time: 13.30 – 14.45 on 15 June, 2023 || Pranav Seth, Chief Digital Officer as a panelist',
    eventType: 'Conference',
    startDate: '2023-06-14',
    endDate: '2022-06-16',
    openTime: '09:00:00',
    closeTime: '06:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/anh-c37fc52056.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/anh-c37fc52056.jpg',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: '1Q23 Retail Investor Meeting',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2023-04-27',
    endDate: '2023-04-27',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl:
      'https://forms.office.com/Pages/ResponsePage.aspx?id=6A4wK-aYa0C_-NiZmIWkw5RjEdZEQQ9PrUMpdh5si9dUNUpFNFZVTVdIN05IVDQ0UjRHUkhGRUE5Uy4u',
    addToCalendarUrl: '',
  },
  {
    title: '1Q2023 Analyst Presentation',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2023-04-26',
    endDate: '2023-04-26',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl:
      'https://forms.office.com/Pages/ResponsePage.aspx?id=6A4wK-aYa0C_-NiZmIWkw5RjEdZEQQ9PrUMpdh5si9dUNDI4T0RQUldEVDlZR1BQSlZNMzM0VjFKTC4u',
    addToCalendarUrl: '',
  },
  {
    title: '1Q2023 Financial Statements (VAS) Disclosures ',
    description:
      "Techcombank's 1Q2023 financial statements in accordance with the Vietnamese Accounting Standards (VAS)",
    eventType: 'Analyst Presentation',
    startDate: '2023-04-25',
    endDate: '2023-04-25',
    openTime: '15:00:00',
    closeTime: '15:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Nha-dau-tu-e827562d86-87743bb467.png',
    viewMoreUrl: '',
    registerUrl: null,
    addToCalendarUrl: null,
  },
  {
    title: '2023 Annual General Meeting, 22/04/2023',
    description:
      'Techcombank will organise 2023 Annual General Meeting on the 22rd April 2023.',
    eventType: 'AGM',
    startDate: '2023-04-22',
    endDate: '2023-04-22',
    openTime: '09:00:00',
    closeTime: '09:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl: '',
    registerUrl: 'https://www.youtube.com/watch?v=URsxRH9Lvn0',
    addToCalendarUrl: null,
  },
  {
    title: '2023 Vietcap Taiwan Conference ',
    description:
      '2023 Vietcap Taiwan Conference connects Vietnam companies with many big institution and high net-worth individual investors in Taiwan who are increasingly investing in Vietnam market the past year. ',
    eventType: 'Conference',
    startDate: '2023-03-16',
    endDate: '2022-03-16',
    openTime: '09:00:00',
    closeTime: '06:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: 'Vietnam Access Days 2023',
    description:
      "Vietnam Access Days (VAD) is Vietnam's largest investment conference that connects the nation’s leading enterprises and global investors to discuss the latest market trends and potential investment opportunities\n",
    eventType: 'Conference',
    startDate: '2023-03-02',
    endDate: '2022-03-02',
    openTime: '09:00:00',
    closeTime: '06:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: 'Vietnam C-Suite Forum 2023',
    description:
      'Citi and SSI Securities Corporation will host the “Vietnam C-Suite Forum 2023”, which will be held from Wednesday, February 15th through Friday, February 17th, 2023. This is the first in person event in Vietnam in three years, featuring C-level management of the leading listed and unlisted companies across sectors. Furthermore, the event attracts over hundreds of high-quality institutional investors worldwide\n',
    eventType: 'Conference',
    startDate: '2023-02-17',
    endDate: '2022-02-17',
    openTime: '09:00:00',
    closeTime: '06:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    viewMoreUrl: '',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: 'FY2022 Retail Investor Meeting',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2023-02-02',
    endDate: '2023-02-02',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=4',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: 'FY2022 Analyst Presentation',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2023-02-01',
    endDate: '2023-02-01',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=4',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: 'Financial Statements (VAS) Disclosures ',
    description:
      "Techcombank's FY2022 financial statements in accordance with the Vietnamese Accounting Standards (VAS)",
    eventType: 'Analyst Presentation',
    startDate: '2023-01-30',
    endDate: '2023-01-30',
    openTime: '15:00:00',
    closeTime: '15:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/financial-statements-vas?year=2022&quarter=4',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '9M2022 Retail Investor Meeting',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-10-24',
    endDate: '2022-10-24',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=3',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '9M2022 Analyst Presentation',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-10-21',
    endDate: '2022-09-21',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=3',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '1H2022 Retail Investor Meeting',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-07-25',
    endDate: '2022-07-25',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=2',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '1H2022 Analyst Presentation',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-07-22',
    endDate: '2022-07-22',
    openTime: '14:45:00',
    closeTime: '16:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/Q2_2022_final_op_1_bc85048dc8.png',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=2',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '1Q2022 Retail Investor Meeting',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-04-28',
    endDate: '2022-04-28',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=1',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '1Q2022 Analyst Presentation',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Analyst Presentation',
    startDate: '2022-04-27',
    endDate: '2022-04-27',
    openTime: '14:45:00',
    closeTime: '16:30:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2022&quarter=1',
    registerUrl: '',
    addToCalendarUrl: '',
  },
  {
    title: '2022 Annual General Meeting, 23/04/2022',
    description:
      'Techcombank will organise 2022 Annual General Meeting on the 23rd April 2022.',
    eventType: 'AGM',
    startDate: '2022-04-23',
    endDate: '2022-04-23',
    openTime: '09:00:00',
    closeTime: '09:00:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl: '/investors/agm',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: '4Q21 Retail Investor Meeting: 26 January 2022',
    description:
      'This livestream event is a quarterly virtual meeting designed for individual investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details. Kindly note that the meeting is only in Vietnamese.',
    eventType: 'Individual',
    startDate: '2022-01-26',
    endDate: '2022-01-26',
    openTime: '14:45:00',
    closeTime: '14:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=4',
    registerUrl: 'https://www.surveymonkey.com/r/LVCHQDH',
    addToCalendarUrl:
      'https://calendar.google.com/event?action=TEMPLATE&text=Cuộc%20họp%20với%20nhà%20đầu%20tư%20cá%20nhân%204Q21&dates=20220126T074500Z/20220126T094500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20cá%20nhân.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích.%20Vui%20lòng%20lưu%20ý%20cuộc%20họp%20sẽ%20trao%20đổi%20bằng%20Tiếng%20Việt.',
  },
  {
    title: '4Q21 Analyst Presentation: 25 January 2022',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Institutional',
    startDate: '2022-01-25',
    endDate: '2022-01-25',
    openTime: '14:45:00',
    closeTime: '14:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=4',
    registerUrl: 'https://www.surveymonkey.com/r/LVBW3YP',
    addToCalendarUrl:
      'https://calendar.google.com/event?action=TEMPLATE&text=Công%20bố%20KQKD%204Q21&dates=20220125T074500Z/20220125T094500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.',
  },
  {
    title: '3Q21 Retail Investor Meeting: 22 October 2021',
    description:
      'This livestream event is a quarterly virtual meeting designed for individual investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details. Kindly note that the meeting is only in Vietnamese.',
    eventType: 'Individual',
    startDate: '2021-10-22',
    endDate: '2021-10-22',
    openTime: '08:45:00',
    closeTime: '08:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=3',
    registerUrl: 'https://www.surveymonkey.com/r/W2RYY8T',
    addToCalendarUrl:
      'https://calendar.google.com/event?action=TEMPLATE&text=Cuộc%20họp%20với%20nhà%20đầu%20tư%20cá%20nhân%203Q21&dates=20211022T014500Z/20211022T014500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20cá%20nhân.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích.%20Vui%20lòng%20lưu%20ý%20cuộc%20họp%20sẽ%20trao%20đổi%20bằng%20Tiếng%20Việt.',
  },
  {
    title: '3Q21 Analyst Presentation: 21 October',
    description:
      'This livestream event is a quarterly virtual meeting designed for analysts and institutional investors, where Techcombank announces its remarkable business performance results. Techcombank’s executive team will deliver a comprehensive presentation on financial results, outlook, strategy and business updates. The event also includes Q&A section in which speakers will answer audiences’ questions in details.',
    eventType: 'Institutional',
    startDate: '2021-10-21',
    endDate: '2021-10-21',
    openTime: '08:45:00',
    closeTime: '08:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=3',
    registerUrl: 'https://www.surveymonkey.com/r/WDB98C2',
    addToCalendarUrl:
      '"https://calendar.google.com/event?action=TEMPLATE&text=Công%20bố%20KQKD%203Q21&dates=20211021T014500Z/20211021T014500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.',
  },
  {
    title: 'Jefferies Asia Forum: 8-10 September',
    description:
      'This virtual three-day event will bring together institutional investors, leading public and private companies from around the region as well as pre-eminent thought leaders and innovative thinkers, offering original content, comprehensive corporate access and actionable investment insights.\n',
    eventType: 'Conference',
    startDate: '2021-09-10',
    endDate: '2021-09-10',
    openTime: '08:45:00',
    closeTime: '08:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/jefferies_conference_d1b436bac4.jpg',
    viewMoreUrl:
      '/investors/financial-information/documents?year=2021&category=FINANCIAL+STATEMENTS+VAS',
    registerUrl: '',
    addToCalendarUrl: null,
  },
  {
    title: '2Q21 Analyst Presentation',
    description:
      'Techcombank reported 1H21 resilient results amidst a cautious economic recovery.',
    eventType: 'Analyst Presentation',
    startDate: '2021-07-21',
    endDate: '2021-07-21',
    openTime: '14:45:00',
    closeTime: '14:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=2',
    registerUrl: 'https://www.surveymonkey.com/r/LVCHQDH',
    addToCalendarUrl:
      '"https://calendar.google.com/event?action=TEMPLATE&text=Công%20bố%20KQKD%203Q21&dates=20211021T014500Z/20211021T014500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.',
  },
  {
    title: '1Q21 Analyst Presentation',
    description:
      'Techcombank continues strong 2020 momentum with record 1Q21 financial results',
    eventType: 'Analyst Presentation',
    startDate: '2021-04-27',
    endDate: '2021-04-27',
    openTime: '14:45:00',
    closeTime: '14:45:00',
    thumbnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    mobileThumnail:
      'https://d1kndcit1zrj97.cloudfront.net/uploads/2q21_analyst_presentation_3edcd2e88a.jpg',
    viewMoreUrl:
      'https://techcombank.com/en/investors/financial-information/investor-presentations?year=2021&quarter=1',
    registerUrl: null,
    addToCalendarUrl:
      '"https://calendar.google.com/event?action=TEMPLATE&text=Công%20bố%20KQKD%201Q21&dates=20210427T014500Z/20210427T014500Z&location=Home&details=Sự%20kiện%20trực%20tuyến%20được%20tổ%20chức%20hàng%20quý%20bởi%20Techcombank%20nhằm%20công%20bố%20các%20kết%20quả%20đáng%20chú%20ý%20trong%20hoạt%20động%20kinh%20doanh.%20Sự%20kiện%20hướng%20tới%20đối%20tượng%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.%20Đội%20ngũ%20điều%20hành%20của%20Techcombank%20sẽ%20trình%20bày%20về%20kết%20quả%20tài%20chính,%20những%20triển%20vọng%20và%20chiến%20lược%20của%20công%20ty,%20đồng%20thời%20cập%20nhật%20các%20hoạt%20động%20kinh%20doanh%20nổi%20bật%20trong%20quý.%20Sự%20kiện%20bao%20gồm%20phần%20Hỏi%20đáp,%20trong%20đó%20các%20diễn%20giả%20sẽ%20trả%20lời%20chi%20tiết%20các%20thắc%20mắc%20đến%20từ%20các%20nhà%20đầu%20tư%20và%20phân%20tích%20tài%20chính.',
  },
].sort((a, b) => (a.startDate < b.startDate ? 1 : -1));
