export const STOCK_CHART_DATA = {
  "ticker": "TCB",
  "data": [
    {
      "open": 33000,
      "high": 33200,
      "low": 32200,
      "close": 32500,
      "volume": 5241159,
      "tradingDate": "2022-09-30T00:00:00.000Z"
    },
    {
      "open": 32500,
      "high": 32600,
      "low": 30250,
      "close": 30250,
      "volume": 6078214,
      "tradingDate": "2022-10-03T00:00:00.000Z"
    },
    {
      "open": 30800,
      "high": 31000,
      "low": 30000,
      "close": 30300,
      "volume": 5968534,
      "tradingDate": "2022-10-04T00:00:00.000Z"
    },
    {
      "open": 31000,
      "high": 31050,
      "low": 30500,
      "close": 30500,
      "volume": 3987890,
      "tradingDate": "2022-10-05T00:00:00.000Z"
    },
    {
      "open": 30500,
      "high": 30800,
      "low": 29300,
      "close": 29300,
      "volume": 5576309,
      "tradingDate": "2022-10-06T00:00:00.000Z"
    },
    {
      "open": 28900,
      "high": 28900,
      "low": 27250,
      "close": 27250,
      "volume": 14353686,
      "tradingDate": "2022-10-07T00:00:00.000Z"
    },
    {
      "open": 25800,
      "high": 26950,
      "low": 25350,
      "close": 25800,
      "volume": 18278239,
      "tradingDate": "2022-10-10T00:00:00.000Z"
    },
    {
      "open": 25800,
      "high": 25800,
      "low": 24000,
      "close": 24000,
      "volume": 21192923,
      "tradingDate": "2022-10-11T00:00:00.000Z"
    },
    {
      "open": 24000,
      "high": 25400,
      "low": 23550,
      "close": 24800,
      "volume": 9607074,
      "tradingDate": "2022-10-12T00:00:00.000Z"
    },
    {
      "open": 24800,
      "high": 25300,
      "low": 24100,
      "close": 25300,
      "volume": 9388012,
      "tradingDate": "2022-10-13T00:00:00.000Z"
    },
    {
      "open": 26500,
      "high": 26500,
      "low": 25500,
      "close": 25700,
      "volume": 6335997,
      "tradingDate": "2022-10-14T00:00:00.000Z"
    },
    {
      "open": 25500,
      "high": 25500,
      "low": 24350,
      "close": 25100,
      "volume": 7762786,
      "tradingDate": "2022-10-17T00:00:00.000Z"
    },
    {
      "open": 25600,
      "high": 25900,
      "low": 25150,
      "close": 25300,
      "volume": 5031198,
      "tradingDate": "2022-10-18T00:00:00.000Z"
    },
    {
      "open": 25300,
      "high": 25500,
      "low": 24600,
      "close": 25200,
      "volume": 4573133,
      "tradingDate": "2022-10-19T00:00:00.000Z"
    },
    {
      "open": 25100,
      "high": 25100,
      "low": 24550,
      "close": 24600,
      "volume": 5211003,
      "tradingDate": "2022-10-20T00:00:00.000Z"
    },
    {
      "open": 24850,
      "high": 24850,
      "low": 22900,
      "close": 22900,
      "volume": 9636932,
      "tradingDate": "2022-10-21T00:00:00.000Z"
    },
    {
      "open": 23050,
      "high": 23350,
      "low": 21300,
      "close": 21300,
      "volume": 11803332,
      "tradingDate": "2022-10-24T00:00:00.000Z"
    },
    {
      "open": 21300,
      "high": 22450,
      "low": 20000,
      "close": 21300,
      "volume": 9596304,
      "tradingDate": "2022-10-25T00:00:00.000Z"
    },
    {
      "open": 21700,
      "high": 21800,
      "low": 21050,
      "close": 21400,
      "volume": 3154614,
      "tradingDate": "2022-10-26T00:00:00.000Z"
    },
    {
      "open": 21500,
      "high": 22850,
      "low": 21500,
      "close": 22850,
      "volume": 5224376,
      "tradingDate": "2022-10-27T00:00:00.000Z"
    },
    {
      "open": 23600,
      "high": 24400,
      "low": 23500,
      "close": 24400,
      "volume": 12486279,
      "tradingDate": "2022-10-28T00:00:00.000Z"
    },
    {
      "open": 24600,
      "high": 24800,
      "low": 23500,
      "close": 24600,
      "volume": 7287089,
      "tradingDate": "2022-10-31T00:00:00.000Z"
    },
    {
      "open": 25000,
      "high": 26000,
      "low": 25000,
      "close": 25650,
      "volume": 8604891,
      "tradingDate": "2022-11-01T00:00:00.000Z"
    },
    {
      "open": 25700,
      "high": 25800,
      "low": 25050,
      "close": 25200,
      "volume": 5443799,
      "tradingDate": "2022-11-02T00:00:00.000Z"
    },
    {
      "open": 24650,
      "high": 25850,
      "low": 24650,
      "close": 25850,
      "volume": 6664096,
      "tradingDate": "2022-11-03T00:00:00.000Z"
    },
    {
      "open": 25000,
      "high": 25500,
      "low": 24050,
      "close": 25500,
      "volume": 12628815,
      "tradingDate": "2022-11-04T00:00:00.000Z"
    },
    {
      "open": 25450,
      "high": 25800,
      "low": 23750,
      "close": 23750,
      "volume": 6327819,
      "tradingDate": "2022-11-07T00:00:00.000Z"
    },
    {
      "open": 23750,
      "high": 24900,
      "low": 23000,
      "close": 24150,
      "volume": 5355201,
      "tradingDate": "2022-11-08T00:00:00.000Z"
    },
    {
      "open": 24500,
      "high": 25100,
      "low": 24200,
      "close": 24350,
      "volume": 3646436,
      "tradingDate": "2022-11-09T00:00:00.000Z"
    },
    {
      "open": 24000,
      "high": 24100,
      "low": 22650,
      "close": 22750,
      "volume": 7582667,
      "tradingDate": "2022-11-10T00:00:00.000Z"
    },
    {
      "open": 23500,
      "high": 23950,
      "low": 22800,
      "close": 22800,
      "volume": 5480773,
      "tradingDate": "2022-11-11T00:00:00.000Z"
    },
    {
      "open": 22200,
      "high": 22400,
      "low": 21300,
      "close": 22200,
      "volume": 5830557,
      "tradingDate": "2022-11-14T00:00:00.000Z"
    },
    {
      "open": 22050,
      "high": 22050,
      "low": 20650,
      "close": 20700,
      "volume": 10945570,
      "tradingDate": "2022-11-15T00:00:00.000Z"
    },
    {
      "open": 19300,
      "high": 22100,
      "low": 19300,
      "close": 22100,
      "volume": 18064365,
      "tradingDate": "2022-11-16T00:00:00.000Z"
    },
    {
      "open": 22700,
      "high": 23050,
      "low": 22200,
      "close": 22950,
      "volume": 5978687,
      "tradingDate": "2022-11-17T00:00:00.000Z"
    },
    {
      "open": 22850,
      "high": 23350,
      "low": 21800,
      "close": 22700,
      "volume": 6392039,
      "tradingDate": "2022-11-18T00:00:00.000Z"
    },
    {
      "open": 22650,
      "high": 23100,
      "low": 22250,
      "close": 22250,
      "volume": 4509809,
      "tradingDate": "2022-11-21T00:00:00.000Z"
    },
    {
      "open": 22050,
      "high": 23050,
      "low": 21850,
      "close": 21850,
      "volume": 7550322,
      "tradingDate": "2022-11-22T00:00:00.000Z"
    },
    {
      "open": 22000,
      "high": 22400,
      "low": 21700,
      "close": 22000,
      "volume": 3773424,
      "tradingDate": "2022-11-23T00:00:00.000Z"
    },
    {
      "open": 22000,
      "high": 22200,
      "low": 21500,
      "close": 22150,
      "volume": 3472711,
      "tradingDate": "2022-11-24T00:00:00.000Z"
    },
    {
      "open": 22350,
      "high": 23000,
      "low": 22200,
      "close": 23000,
      "volume": 5209259,
      "tradingDate": "2022-11-25T00:00:00.000Z"
    },
    {
      "open": 23300,
      "high": 24600,
      "low": 23250,
      "close": 24600,
      "volume": 9061731,
      "tradingDate": "2022-11-28T00:00:00.000Z"
    },
    {
      "open": 24950,
      "high": 25600,
      "low": 24350,
      "close": 25200,
      "volume": 7878874,
      "tradingDate": "2022-11-29T00:00:00.000Z"
    },
    {
      "open": 25200,
      "high": 25900,
      "low": 24950,
      "close": 25900,
      "volume": 8862365,
      "tradingDate": "2022-11-30T00:00:00.000Z"
    },
    {
      "open": 26250,
      "high": 27700,
      "low": 26250,
      "close": 27400,
      "volume": 16535529,
      "tradingDate": "2022-12-01T00:00:00.000Z"
    },
    {
      "open": 27000,
      "high": 29000,
      "low": 26850,
      "close": 28800,
      "volume": 9003657,
      "tradingDate": "2022-12-02T00:00:00.000Z"
    },
    {
      "open": 29500,
      "high": 29800,
      "low": 28800,
      "close": 29000,
      "volume": 8447624,
      "tradingDate": "2022-12-05T00:00:00.000Z"
    },
    {
      "open": 28800,
      "high": 29000,
      "low": 27000,
      "close": 27050,
      "volume": 14492964,
      "tradingDate": "2022-12-06T00:00:00.000Z"
    },
    {
      "open": 27000,
      "high": 27150,
      "low": 25950,
      "close": 26400,
      "volume": 7115853,
      "tradingDate": "2022-12-07T00:00:00.000Z"
    },
    {
      "open": 26800,
      "high": 28200,
      "low": 26500,
      "close": 28200,
      "volume": 10146335,
      "tradingDate": "2022-12-08T00:00:00.000Z"
    },
    {
      "open": 28450,
      "high": 28500,
      "low": 27700,
      "close": 28250,
      "volume": 4408627,
      "tradingDate": "2022-12-09T00:00:00.000Z"
    },
    {
      "open": 28250,
      "high": 28550,
      "low": 27200,
      "close": 27200,
      "volume": 6191157,
      "tradingDate": "2022-12-12T00:00:00.000Z"
    },
    {
      "open": 27200,
      "high": 28800,
      "low": 27000,
      "close": 28800,
      "volume": 7317733,
      "tradingDate": "2022-12-13T00:00:00.000Z"
    },
    {
      "open": 29500,
      "high": 29700,
      "low": 28800,
      "close": 28800,
      "volume": 8001168,
      "tradingDate": "2022-12-14T00:00:00.000Z"
    },
    {
      "open": 28800,
      "high": 29550,
      "low": 28600,
      "close": 29500,
      "volume": 7682642,
      "tradingDate": "2022-12-15T00:00:00.000Z"
    },
    {
      "open": 29000,
      "high": 29300,
      "low": 28600,
      "close": 29150,
      "volume": 6470231,
      "tradingDate": "2022-12-16T00:00:00.000Z"
    },
    {
      "open": 29150,
      "high": 30650,
      "low": 29050,
      "close": 29250,
      "volume": 9320936,
      "tradingDate": "2022-12-19T00:00:00.000Z"
    },
    {
      "open": 29000,
      "high": 29250,
      "low": 27250,
      "close": 27700,
      "volume": 11753179,
      "tradingDate": "2022-12-20T00:00:00.000Z"
    },
    {
      "open": 28050,
      "high": 28200,
      "low": 26300,
      "close": 27700,
      "volume": 6561737,
      "tradingDate": "2022-12-21T00:00:00.000Z"
    },
    {
      "open": 27750,
      "high": 28400,
      "low": 27250,
      "close": 27700,
      "volume": 3605685,
      "tradingDate": "2022-12-22T00:00:00.000Z"
    },
    {
      "open": 27700,
      "high": 27900,
      "low": 26950,
      "close": 27600,
      "volume": 2879544,
      "tradingDate": "2022-12-23T00:00:00.000Z"
    },
    {
      "open": 27250,
      "high": 27400,
      "low": 25700,
      "close": 25700,
      "volume": 7545177,
      "tradingDate": "2022-12-26T00:00:00.000Z"
    },
    {
      "open": 25600,
      "high": 26300,
      "low": 25400,
      "close": 26300,
      "volume": 4427020,
      "tradingDate": "2022-12-27T00:00:00.000Z"
    },
    {
      "open": 26100,
      "high": 26450,
      "low": 25750,
      "close": 26150,
      "volume": 3299598,
      "tradingDate": "2022-12-28T00:00:00.000Z"
    },
    {
      "open": 26100,
      "high": 26300,
      "low": 25750,
      "close": 25950,
      "volume": 2935675,
      "tradingDate": "2022-12-29T00:00:00.000Z"
    },
    {
      "open": 25950,
      "high": 26100,
      "low": 25850,
      "close": 25850,
      "volume": 2182511,
      "tradingDate": "2022-12-30T00:00:00.000Z"
    },
    {
      "open": 25750,
      "high": 27450,
      "low": 25750,
      "close": 27450,
      "volume": 3787630,
      "tradingDate": "2023-01-03T00:00:00.000Z"
    },
    {
      "open": 27450,
      "high": 27800,
      "low": 27250,
      "close": 27300,
      "volume": 3186578,
      "tradingDate": "2023-01-04T00:00:00.000Z"
    },
    {
      "open": 27300,
      "high": 27800,
      "low": 27150,
      "close": 27650,
      "volume": 2717668,
      "tradingDate": "2023-01-05T00:00:00.000Z"
    },
    {
      "open": 27450,
      "high": 28400,
      "low": 27300,
      "close": 27700,
      "volume": 4805646,
      "tradingDate": "2023-01-06T00:00:00.000Z"
    },
    {
      "open": 27800,
      "high": 27950,
      "low": 27600,
      "close": 27750,
      "volume": 2388393,
      "tradingDate": "2023-01-09T00:00:00.000Z"
    },
    {
      "open": 27750,
      "high": 28000,
      "low": 27300,
      "close": 27450,
      "volume": 2614094,
      "tradingDate": "2023-01-10T00:00:00.000Z"
    },
    {
      "open": 27450,
      "high": 28000,
      "low": 27300,
      "close": 27650,
      "volume": 3137320,
      "tradingDate": "2023-01-11T00:00:00.000Z"
    },
    {
      "open": 27650,
      "high": 27950,
      "low": 27600,
      "close": 27850,
      "volume": 2124636,
      "tradingDate": "2023-01-12T00:00:00.000Z"
    },
    {
      "open": 27900,
      "high": 28300,
      "low": 27800,
      "close": 27800,
      "volume": 2810702,
      "tradingDate": "2023-01-13T00:00:00.000Z"
    },
    {
      "open": 27850,
      "high": 28100,
      "low": 27650,
      "close": 28100,
      "volume": 2493611,
      "tradingDate": "2023-01-16T00:00:00.000Z"
    },
    {
      "open": 28400,
      "high": 29300,
      "low": 28350,
      "close": 29000,
      "volume": 6180297,
      "tradingDate": "2023-01-17T00:00:00.000Z"
    },
    {
      "open": 29100,
      "high": 29150,
      "low": 28900,
      "close": 29100,
      "volume": 3241975,
      "tradingDate": "2023-01-18T00:00:00.000Z"
    },
    {
      "open": 28850,
      "high": 29300,
      "low": 28850,
      "close": 29100,
      "volume": 4743287,
      "tradingDate": "2023-01-19T00:00:00.000Z"
    },
    {
      "open": 29350,
      "high": 29700,
      "low": 29000,
      "close": 29050,
      "volume": 5540997,
      "tradingDate": "2023-01-27T00:00:00.000Z"
    },
    {
      "open": 29050,
      "high": 29050,
      "low": 28550,
      "close": 28700,
      "volume": 5918055,
      "tradingDate": "2023-01-30T00:00:00.000Z"
    },
    {
      "open": 28650,
      "high": 29400,
      "low": 28050,
      "close": 29400,
      "volume": 6149430,
      "tradingDate": "2023-01-31T00:00:00.000Z"
    },
    {
      "open": 29500,
      "high": 29500,
      "low": 28000,
      "close": 28200,
      "volume": 5946569,
      "tradingDate": "2023-02-01T00:00:00.000Z"
    },
    {
      "open": 28200,
      "high": 28400,
      "low": 27600,
      "close": 28000,
      "volume": 3795867,
      "tradingDate": "2023-02-02T00:00:00.000Z"
    },
    {
      "open": 28100,
      "high": 28200,
      "low": 27100,
      "close": 27100,
      "volume": 6449574,
      "tradingDate": "2023-02-03T00:00:00.000Z"
    },
    {
      "open": 27100,
      "high": 28000,
      "low": 27050,
      "close": 27950,
      "volume": 3183964,
      "tradingDate": "2023-02-06T00:00:00.000Z"
    },
    {
      "open": 27950,
      "high": 28050,
      "low": 27400,
      "close": 27400,
      "volume": 3290162,
      "tradingDate": "2023-02-07T00:00:00.000Z"
    },
    {
      "open": 27500,
      "high": 27950,
      "low": 27400,
      "close": 27850,
      "volume": 2709523,
      "tradingDate": "2023-02-08T00:00:00.000Z"
    },
    {
      "open": 27700,
      "high": 27850,
      "low": 27300,
      "close": 27300,
      "volume": 2757691,
      "tradingDate": "2023-02-09T00:00:00.000Z"
    },
    {
      "open": 27350,
      "high": 27500,
      "low": 26900,
      "close": 26950,
      "volume": 2627628,
      "tradingDate": "2023-02-10T00:00:00.000Z"
    },
    {
      "open": 26850,
      "high": 26900,
      "low": 26350,
      "close": 26600,
      "volume": 2898242,
      "tradingDate": "2023-02-13T00:00:00.000Z"
    },
    {
      "open": 26500,
      "high": 27400,
      "low": 26500,
      "close": 26850,
      "volume": 2950560,
      "tradingDate": "2023-02-14T00:00:00.000Z"
    },
    {
      "open": 26850,
      "high": 27600,
      "low": 26600,
      "close": 27400,
      "volume": 2633863,
      "tradingDate": "2023-02-15T00:00:00.000Z"
    },
    {
      "open": 27600,
      "high": 27900,
      "low": 27200,
      "close": 27900,
      "volume": 2248327,
      "tradingDate": "2023-02-16T00:00:00.000Z"
    },
    {
      "open": 27750,
      "high": 27800,
      "low": 27500,
      "close": 27800,
      "volume": 1784647,
      "tradingDate": "2023-02-17T00:00:00.000Z"
    },
    {
      "open": 27750,
      "high": 28600,
      "low": 27700,
      "close": 28600,
      "volume": 2744240,
      "tradingDate": "2023-02-20T00:00:00.000Z"
    },
    {
      "open": 28600,
      "high": 28950,
      "low": 28350,
      "close": 28400,
      "volume": 2591781,
      "tradingDate": "2023-02-21T00:00:00.000Z"
    },
    {
      "open": 28050,
      "high": 28150,
      "low": 27500,
      "close": 27500,
      "volume": 2373872,
      "tradingDate": "2023-02-22T00:00:00.000Z"
    },
    {
      "open": 27500,
      "high": 27950,
      "low": 27000,
      "close": 27950,
      "volume": 3597923,
      "tradingDate": "2023-02-23T00:00:00.000Z"
    },
    {
      "open": 27950,
      "high": 27950,
      "low": 27150,
      "close": 27250,
      "volume": 1973343,
      "tradingDate": "2023-02-24T00:00:00.000Z"
    },
    {
      "open": 27200,
      "high": 27200,
      "low": 26600,
      "close": 27000,
      "volume": 2377696,
      "tradingDate": "2023-02-27T00:00:00.000Z"
    },
    {
      "open": 27000,
      "high": 27200,
      "low": 26500,
      "close": 26500,
      "volume": 2105214,
      "tradingDate": "2023-02-28T00:00:00.000Z"
    },
    {
      "open": 26200,
      "high": 27350,
      "low": 26050,
      "close": 27300,
      "volume": 2204708,
      "tradingDate": "2023-03-01T00:00:00.000Z"
    },
    {
      "open": 27550,
      "high": 27600,
      "low": 27000,
      "close": 27200,
      "volume": 1550415,
      "tradingDate": "2023-03-02T00:00:00.000Z"
    },
    {
      "open": 27200,
      "high": 27350,
      "low": 26550,
      "close": 26800,
      "volume": 1876687,
      "tradingDate": "2023-03-03T00:00:00.000Z"
    },
    {
      "open": 27000,
      "high": 27450,
      "low": 26700,
      "close": 27200,
      "volume": 2569900,
      "tradingDate": "2023-03-06T00:00:00.000Z"
    },
    {
      "open": 27200,
      "high": 27500,
      "low": 27050,
      "close": 27100,
      "volume": 2340737,
      "tradingDate": "2023-03-07T00:00:00.000Z"
    },
    {
      "open": 26900,
      "high": 27400,
      "low": 26800,
      "close": 27300,
      "volume": 1945590,
      "tradingDate": "2023-03-08T00:00:00.000Z"
    },
    {
      "open": 27500,
      "high": 27850,
      "low": 27200,
      "close": 27800,
      "volume": 3286991,
      "tradingDate": "2023-03-09T00:00:00.000Z"
    },
    {
      "open": 27500,
      "high": 27600,
      "low": 27000,
      "close": 27150,
      "volume": 2130732,
      "tradingDate": "2023-03-10T00:00:00.000Z"
    },
    {
      "open": 26550,
      "high": 26950,
      "low": 26550,
      "close": 26750,
      "volume": 3115026,
      "tradingDate": "2023-03-13T00:00:00.000Z"
    },
    {
      "open": 26650,
      "high": 26750,
      "low": 26300,
      "close": 26300,
      "volume": 2471323,
      "tradingDate": "2023-03-14T00:00:00.000Z"
    },
    {
      "open": 26800,
      "high": 27400,
      "low": 26800,
      "close": 27200,
      "volume": 2362248,
      "tradingDate": "2023-03-15T00:00:00.000Z"
    },
    {
      "open": 26900,
      "high": 26950,
      "low": 26350,
      "close": 26750,
      "volume": 2663133,
      "tradingDate": "2023-03-16T00:00:00.000Z"
    },
    {
      "open": 26800,
      "high": 27100,
      "low": 26600,
      "close": 26650,
      "volume": 1200567,
      "tradingDate": "2023-03-17T00:00:00.000Z"
    },
    {
      "open": 26500,
      "high": 26600,
      "low": 26250,
      "close": 26250,
      "volume": 2287264,
      "tradingDate": "2023-03-20T00:00:00.000Z"
    },
    {
      "open": 26500,
      "high": 26600,
      "low": 26100,
      "close": 26350,
      "volume": 1998513,
      "tradingDate": "2023-03-21T00:00:00.000Z"
    },
    {
      "open": 26450,
      "high": 26600,
      "low": 26200,
      "close": 26200,
      "volume": 2472073,
      "tradingDate": "2023-03-22T00:00:00.000Z"
    },
    {
      "open": 26200,
      "high": 26350,
      "low": 25850,
      "close": 26200,
      "volume": 2369382,
      "tradingDate": "2023-03-23T00:00:00.000Z"
    },
    {
      "open": 26400,
      "high": 26700,
      "low": 26300,
      "close": 26450,
      "volume": 1956017,
      "tradingDate": "2023-03-24T00:00:00.000Z"
    },
    {
      "open": 26450,
      "high": 26700,
      "low": 26350,
      "close": 26500,
      "volume": 1537765,
      "tradingDate": "2023-03-27T00:00:00.000Z"
    },
    {
      "open": 26700,
      "high": 27700,
      "low": 26700,
      "close": 27550,
      "volume": 7665446,
      "tradingDate": "2023-03-28T00:00:00.000Z"
    },
    {
      "open": 27550,
      "high": 28300,
      "low": 27500,
      "close": 28000,
      "volume": 5721469,
      "tradingDate": "2023-03-29T00:00:00.000Z"
    },
    {
      "open": 28350,
      "high": 28500,
      "low": 27700,
      "close": 27700,
      "volume": 4899644,
      "tradingDate": "2023-03-30T00:00:00.000Z"
    },
    {
      "open": 27800,
      "high": 28400,
      "low": 27800,
      "close": 28350,
      "volume": 7221142,
      "tradingDate": "2023-03-31T00:00:00.000Z"
    },
    {
      "open": 29000,
      "high": 29350,
      "low": 28700,
      "close": 29300,
      "volume": 12425983,
      "tradingDate": "2023-04-03T00:00:00.000Z"
    },
    {
      "open": 29300,
      "high": 29500,
      "low": 29050,
      "close": 29500,
      "volume": 7304774,
      "tradingDate": "2023-04-04T00:00:00.000Z"
    },
    {
      "open": 29400,
      "high": 29600,
      "low": 29200,
      "close": 29500,
      "volume": 4330672,
      "tradingDate": "2023-04-05T00:00:00.000Z"
    },
    {
      "open": 29350,
      "high": 30200,
      "low": 29350,
      "close": 29600,
      "volume": 7829889,
      "tradingDate": "2023-04-06T00:00:00.000Z"
    },
    {
      "open": 29650,
      "high": 29700,
      "low": 29400,
      "close": 29550,
      "volume": 4095698,
      "tradingDate": "2023-04-07T00:00:00.000Z"
    },
    {
      "open": 29650,
      "high": 30800,
      "low": 29600,
      "close": 30200,
      "volume": 5661649,
      "tradingDate": "2023-04-10T00:00:00.000Z"
    },
    {
      "open": 30200,
      "high": 30400,
      "low": 29600,
      "close": 30400,
      "volume": 5210501,
      "tradingDate": "2023-04-11T00:00:00.000Z"
    },
    {
      "open": 30400,
      "high": 30950,
      "low": 30100,
      "close": 30700,
      "volume": 4506702,
      "tradingDate": "2023-04-12T00:00:00.000Z"
    },
    {
      "open": 30600,
      "high": 30950,
      "low": 30200,
      "close": 30200,
      "volume": 3215262,
      "tradingDate": "2023-04-13T00:00:00.000Z"
    },
    {
      "open": 30200,
      "high": 30400,
      "low": 28900,
      "close": 28900,
      "volume": 9437403,
      "tradingDate": "2023-04-14T00:00:00.000Z"
    },
    {
      "open": 29000,
      "high": 29450,
      "low": 28750,
      "close": 29450,
      "volume": 3014955,
      "tradingDate": "2023-04-17T00:00:00.000Z"
    },
    {
      "open": 29400,
      "high": 29400,
      "low": 28900,
      "close": 29150,
      "volume": 2554618,
      "tradingDate": "2023-04-18T00:00:00.000Z"
    },
    {
      "open": 29200,
      "high": 29400,
      "low": 29000,
      "close": 29000,
      "volume": 2612668,
      "tradingDate": "2023-04-19T00:00:00.000Z"
    },
    {
      "open": 28900,
      "high": 29000,
      "low": 28550,
      "close": 28850,
      "volume": 2314483,
      "tradingDate": "2023-04-20T00:00:00.000Z"
    },
    {
      "open": 29000,
      "high": 29000,
      "low": 28700,
      "close": 28700,
      "volume": 2355180,
      "tradingDate": "2023-04-21T00:00:00.000Z"
    },
    {
      "open": 28700,
      "high": 29750,
      "low": 28650,
      "close": 29500,
      "volume": 3483416,
      "tradingDate": "2023-04-24T00:00:00.000Z"
    },
    {
      "open": 29650,
      "high": 29850,
      "low": 29000,
      "close": 29000,
      "volume": 2086772,
      "tradingDate": "2023-04-25T00:00:00.000Z"
    },
    {
      "open": 29200,
      "high": 30000,
      "low": 28850,
      "close": 30000,
      "volume": 3239742,
      "tradingDate": "2023-04-26T00:00:00.000Z"
    },
    {
      "open": 30200,
      "high": 30200,
      "low": 29850,
      "close": 29900,
      "volume": 2032454,
      "tradingDate": "2023-04-27T00:00:00.000Z"
    },
    {
      "open": 29950,
      "high": 29950,
      "low": 29500,
      "close": 29500,
      "volume": 2478528,
      "tradingDate": "2023-04-28T00:00:00.000Z"
    },
    {
      "open": 29500,
      "high": 29500,
      "low": 28900,
      "close": 29050,
      "volume": 1918703,
      "tradingDate": "2023-05-04T00:00:00.000Z"
    },
    {
      "open": 29050,
      "high": 29200,
      "low": 28450,
      "close": 28700,
      "volume": 3438755,
      "tradingDate": "2023-05-05T00:00:00.000Z"
    },
    {
      "open": 28900,
      "high": 29400,
      "low": 28700,
      "close": 29400,
      "volume": 2847866,
      "tradingDate": "2023-05-08T00:00:00.000Z"
    },
    {
      "open": 29450,
      "high": 29500,
      "low": 29050,
      "close": 29300,
      "volume": 1990748,
      "tradingDate": "2023-05-09T00:00:00.000Z"
    },
    {
      "open": 29350,
      "high": 29400,
      "low": 29100,
      "close": 29300,
      "volume": 2523530,
      "tradingDate": "2023-05-10T00:00:00.000Z"
    },
    {
      "open": 29500,
      "high": 29500,
      "low": 29100,
      "close": 29100,
      "volume": 2515029,
      "tradingDate": "2023-05-11T00:00:00.000Z"
    },
    {
      "open": 29100,
      "high": 29300,
      "low": 29000,
      "close": 29300,
      "volume": 2625716,
      "tradingDate": "2023-05-12T00:00:00.000Z"
    },
    {
      "open": 29450,
      "high": 30150,
      "low": 29400,
      "close": 29900,
      "volume": 7337508,
      "tradingDate": "2023-05-15T00:00:00.000Z"
    },
    {
      "open": 30100,
      "high": 30100,
      "low": 29750,
      "close": 29800,
      "volume": 2076312,
      "tradingDate": "2023-05-16T00:00:00.000Z"
    },
    {
      "open": 29700,
      "high": 29900,
      "low": 29250,
      "close": 29250,
      "volume": 3660203,
      "tradingDate": "2023-05-17T00:00:00.000Z"
    },
    {
      "open": 29250,
      "high": 29700,
      "low": 29250,
      "close": 29550,
      "volume": 3215893,
      "tradingDate": "2023-05-18T00:00:00.000Z"
    },
    {
      "open": 29550,
      "high": 29750,
      "low": 29250,
      "close": 29650,
      "volume": 3398295,
      "tradingDate": "2023-05-19T00:00:00.000Z"
    },
    {
      "open": 29700,
      "high": 30600,
      "low": 29700,
      "close": 30500,
      "volume": 9445317,
      "tradingDate": "2023-05-22T00:00:00.000Z"
    },
    {
      "open": 30700,
      "high": 30700,
      "low": 30000,
      "close": 30350,
      "volume": 4206105,
      "tradingDate": "2023-05-23T00:00:00.000Z"
    },
    {
      "open": 30400,
      "high": 30550,
      "low": 29750,
      "close": 29800,
      "volume": 4819842,
      "tradingDate": "2023-05-24T00:00:00.000Z"
    },
    {
      "open": 29800,
      "high": 29900,
      "low": 29600,
      "close": 29800,
      "volume": 2522163,
      "tradingDate": "2023-05-25T00:00:00.000Z"
    },
    {
      "open": 29900,
      "high": 29950,
      "low": 29650,
      "close": 29900,
      "volume": 1795228,
      "tradingDate": "2023-05-26T00:00:00.000Z"
    },
    {
      "open": 30000,
      "high": 30200,
      "low": 29800,
      "close": 30200,
      "volume": 2766086,
      "tradingDate": "2023-05-29T00:00:00.000Z"
    },
    {
      "open": 30400,
      "high": 30450,
      "low": 30000,
      "close": 30200,
      "volume": 2101794,
      "tradingDate": "2023-05-30T00:00:00.000Z"
    },
    {
      "open": 30200,
      "high": 30300,
      "low": 29900,
      "close": 29950,
      "volume": 3945209,
      "tradingDate": "2023-05-31T00:00:00.000Z"
    },
    {
      "open": 30000,
      "high": 30300,
      "low": 29750,
      "close": 30300,
      "volume": 4776838,
      "tradingDate": "2023-06-01T00:00:00.000Z"
    },
    {
      "open": 30800,
      "high": 32200,
      "low": 30700,
      "close": 32200,
      "volume": 13801388,
      "tradingDate": "2023-06-02T00:00:00.000Z"
    },
    {
      "open": 32450,
      "high": 32800,
      "low": 31900,
      "close": 31900,
      "volume": 5864033,
      "tradingDate": "2023-06-05T00:00:00.000Z"
    },
    {
      "open": 31900,
      "high": 32700,
      "low": 31750,
      "close": 32700,
      "volume": 6001629,
      "tradingDate": "2023-06-06T00:00:00.000Z"
    },
    {
      "open": 32550,
      "high": 32750,
      "low": 32200,
      "close": 32600,
      "volume": 4965709,
      "tradingDate": "2023-06-07T00:00:00.000Z"
    },
    {
      "open": 32700,
      "high": 32750,
      "low": 31750,
      "close": 31750,
      "volume": 5742395,
      "tradingDate": "2023-06-08T00:00:00.000Z"
    },
    {
      "open": 31800,
      "high": 32500,
      "low": 31600,
      "close": 32400,
      "volume": 5316162,
      "tradingDate": "2023-06-09T00:00:00.000Z"
    },
    {
      "open": 32250,
      "high": 32850,
      "low": 31900,
      "close": 32700,
      "volume": 4137740,
      "tradingDate": "2023-06-12T00:00:00.000Z"
    },
    {
      "open": 32700,
      "high": 32900,
      "low": 32400,
      "close": 32800,
      "volume": 3530343,
      "tradingDate": "2023-06-13T00:00:00.000Z"
    },
    {
      "open": 33000,
      "high": 33450,
      "low": 32550,
      "close": 32600,
      "volume": 5375884,
      "tradingDate": "2023-06-14T00:00:00.000Z"
    },
    {
      "open": 32600,
      "high": 32900,
      "low": 32400,
      "close": 32650,
      "volume": 3864497,
      "tradingDate": "2023-06-15T00:00:00.000Z"
    },
    {
      "open": 32650,
      "high": 33450,
      "low": 32500,
      "close": 32550,
      "volume": 6929446,
      "tradingDate": "2023-06-16T00:00:00.000Z"
    },
    {
      "open": 32550,
      "high": 32550,
      "low": 31950,
      "close": 32100,
      "volume": 3675383,
      "tradingDate": "2023-06-19T00:00:00.000Z"
    },
    {
      "open": 32200,
      "high": 32250,
      "low": 31950,
      "close": 32150,
      "volume": 2085025,
      "tradingDate": "2023-06-20T00:00:00.000Z"
    },
    {
      "open": 32150,
      "high": 32500,
      "low": 32100,
      "close": 32500,
      "volume": 2507595,
      "tradingDate": "2023-06-21T00:00:00.000Z"
    },
    {
      "open": 32900,
      "high": 33150,
      "low": 32600,
      "close": 32600,
      "volume": 4396086,
      "tradingDate": "2023-06-22T00:00:00.000Z"
    },
    {
      "open": 32650,
      "high": 33250,
      "low": 32550,
      "close": 32900,
      "volume": 4145302,
      "tradingDate": "2023-06-23T00:00:00.000Z"
    },
    {
      "open": 32900,
      "high": 33300,
      "low": 32550,
      "close": 33300,
      "volume": 4046015,
      "tradingDate": "2023-06-26T00:00:00.000Z"
    },
    {
      "open": 33300,
      "high": 33400,
      "low": 32900,
      "close": 32950,
      "volume": 3666451,
      "tradingDate": "2023-06-27T00:00:00.000Z"
    },
    {
      "open": 32950,
      "high": 33300,
      "low": 32750,
      "close": 33300,
      "volume": 4304207,
      "tradingDate": "2023-06-28T00:00:00.000Z"
    },
    {
      "open": 33300,
      "high": 33400,
      "low": 32650,
      "close": 32650,
      "volume": 3540183,
      "tradingDate": "2023-06-29T00:00:00.000Z"
    },
    {
      "open": 32650,
      "high": 32800,
      "low": 32350,
      "close": 32350,
      "volume": 2449372,
      "tradingDate": "2023-06-30T00:00:00.000Z"
    },
    {
      "open": 32700,
      "high": 32700,
      "low": 32000,
      "close": 32000,
      "volume": 3754857,
      "tradingDate": "2023-07-03T00:00:00.000Z"
    },
    {
      "open": 32000,
      "high": 32100,
      "low": 31850,
      "close": 32000,
      "volume": 4327609,
      "tradingDate": "2023-07-04T00:00:00.000Z"
    },
    {
      "open": 32150,
      "high": 32400,
      "low": 31950,
      "close": 31950,
      "volume": 4324600,
      "tradingDate": "2023-07-05T00:00:00.000Z"
    },
    {
      "open": 31950,
      "high": 32050,
      "low": 31200,
      "close": 31550,
      "volume": 6315105,
      "tradingDate": "2023-07-06T00:00:00.000Z"
    },
    {
      "open": 31500,
      "high": 31800,
      "low": 31300,
      "close": 31550,
      "volume": 3275078,
      "tradingDate": "2023-07-07T00:00:00.000Z"
    },
    {
      "open": 32000,
      "high": 32100,
      "low": 31700,
      "close": 32000,
      "volume": 4983713,
      "tradingDate": "2023-07-10T00:00:00.000Z"
    },
    {
      "open": 32200,
      "high": 32350,
      "low": 31950,
      "close": 31950,
      "volume": 5709541,
      "tradingDate": "2023-07-11T00:00:00.000Z"
    },
    {
      "open": 31900,
      "high": 32000,
      "low": 31600,
      "close": 31650,
      "volume": 4343800,
      "tradingDate": "2023-07-12T00:00:00.000Z"
    },
    {
      "open": 31700,
      "high": 32000,
      "low": 31650,
      "close": 31950,
      "volume": 4413075,
      "tradingDate": "2023-07-13T00:00:00.000Z"
    },
    {
      "open": 32200,
      "high": 32200,
      "low": 31700,
      "close": 31950,
      "volume": 4473993,
      "tradingDate": "2023-07-14T00:00:00.000Z"
    },
    {
      "open": 32100,
      "high": 32100,
      "low": 31800,
      "close": 31900,
      "volume": 3653751,
      "tradingDate": "2023-07-17T00:00:00.000Z"
    },
    {
      "open": 31900,
      "high": 32300,
      "low": 31800,
      "close": 32300,
      "volume": 6509554,
      "tradingDate": "2023-07-18T00:00:00.000Z"
    },
    {
      "open": 32650,
      "high": 32900,
      "low": 32250,
      "close": 32300,
      "volume": 5558188,
      "tradingDate": "2023-07-19T00:00:00.000Z"
    },
    {
      "open": 32400,
      "high": 32400,
      "low": 31800,
      "close": 31900,
      "volume": 5265302,
      "tradingDate": "2023-07-20T00:00:00.000Z"
    },
    {
      "open": 31850,
      "high": 32300,
      "low": 31850,
      "close": 32300,
      "volume": 5206964,
      "tradingDate": "2023-07-21T00:00:00.000Z"
    },
    {
      "open": 32450,
      "high": 32500,
      "low": 32150,
      "close": 32450,
      "volume": 4833172,
      "tradingDate": "2023-07-24T00:00:00.000Z"
    },
    {
      "open": 32800,
      "high": 33800,
      "low": 32700,
      "close": 33500,
      "volume": 13271626,
      "tradingDate": "2023-07-25T00:00:00.000Z"
    },
    {
      "open": 33500,
      "high": 33550,
      "low": 33200,
      "close": 33450,
      "volume": 3414460,
      "tradingDate": "2023-07-26T00:00:00.000Z"
    },
    {
      "open": 33350,
      "high": 33450,
      "low": 32700,
      "close": 33100,
      "volume": 7799655,
      "tradingDate": "2023-07-27T00:00:00.000Z"
    },
    {
      "open": 33050,
      "high": 33800,
      "low": 33000,
      "close": 33800,
      "volume": 5632095,
      "tradingDate": "2023-07-28T00:00:00.000Z"
    },
    {
      "open": 34100,
      "high": 34600,
      "low": 34000,
      "close": 34300,
      "volume": 7650937,
      "tradingDate": "2023-07-31T00:00:00.000Z"
    },
    {
      "open": 34600,
      "high": 34600,
      "low": 34000,
      "close": 34000,
      "volume": 8661063,
      "tradingDate": "2023-08-01T00:00:00.000Z"
    },
    {
      "open": 33900,
      "high": 34100,
      "low": 33750,
      "close": 33900,
      "volume": 4645959,
      "tradingDate": "2023-08-02T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34050,
      "low": 33500,
      "close": 33500,
      "volume": 4172633,
      "tradingDate": "2023-08-03T00:00:00.000Z"
    },
    {
      "open": 33650,
      "high": 34200,
      "low": 33450,
      "close": 33600,
      "volume": 12324057,
      "tradingDate": "2023-08-04T00:00:00.000Z"
    },
    {
      "open": 34200,
      "high": 34450,
      "low": 34000,
      "close": 34350,
      "volume": 5258291,
      "tradingDate": "2023-08-07T00:00:00.000Z"
    },
    {
      "open": 34400,
      "high": 34500,
      "low": 33950,
      "close": 34000,
      "volume": 5540784,
      "tradingDate": "2023-08-08T00:00:00.000Z"
    },
    {
      "open": 34050,
      "high": 34200,
      "low": 33800,
      "close": 34000,
      "volume": 4669626,
      "tradingDate": "2023-08-09T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34050,
      "low": 33500,
      "close": 33500,
      "volume": 6167335,
      "tradingDate": "2023-08-10T00:00:00.000Z"
    },
    {
      "open": 33550,
      "high": 33800,
      "low": 33100,
      "close": 33650,
      "volume": 5908847,
      "tradingDate": "2023-08-11T00:00:00.000Z"
    },
    {
      "open": 33750,
      "high": 33800,
      "low": 33450,
      "close": 33800,
      "volume": 4575286,
      "tradingDate": "2023-08-14T00:00:00.000Z"
    },
    {
      "open": 33900,
      "high": 34400,
      "low": 33850,
      "close": 34050,
      "volume": 4612576,
      "tradingDate": "2023-08-15T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 35350,
      "low": 33950,
      "close": 35300,
      "volume": 13200378,
      "tradingDate": "2023-08-16T00:00:00.000Z"
    },
    {
      "open": 35300,
      "high": 35300,
      "low": 34700,
      "close": 34700,
      "volume": 5309921,
      "tradingDate": "2023-08-17T00:00:00.000Z"
    },
    {
      "open": 34550,
      "high": 34550,
      "low": 32450,
      "close": 32500,
      "volume": 13224652,
      "tradingDate": "2023-08-18T00:00:00.000Z"
    },
    {
      "open": 32500,
      "high": 32950,
      "low": 32100,
      "close": 32750,
      "volume": 5357852,
      "tradingDate": "2023-08-21T00:00:00.000Z"
    },
    {
      "open": 32750,
      "high": 33100,
      "low": 31900,
      "close": 33100,
      "volume": 6718788,
      "tradingDate": "2023-08-22T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34200,
      "low": 33200,
      "close": 33200,
      "volume": 9089727,
      "tradingDate": "2023-08-23T00:00:00.000Z"
    },
    {
      "open": 33500,
      "high": 33950,
      "low": 33400,
      "close": 33800,
      "volume": 4816046,
      "tradingDate": "2023-08-24T00:00:00.000Z"
    },
    {
      "open": 33700,
      "high": 33850,
      "low": 33400,
      "close": 33650,
      "volume": 3638646,
      "tradingDate": "2023-08-25T00:00:00.000Z"
    },
    {
      "open": 33850,
      "high": 34100,
      "low": 33600,
      "close": 34000,
      "volume": 4018726,
      "tradingDate": "2023-08-28T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34250,
      "low": 33750,
      "close": 33900,
      "volume": 3703626,
      "tradingDate": "2023-08-29T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34300,
      "low": 33900,
      "close": 34000,
      "volume": 4268932,
      "tradingDate": "2023-08-30T00:00:00.000Z"
    },
    {
      "open": 34100,
      "high": 34500,
      "low": 34100,
      "close": 34500,
      "volume": 4123034,
      "tradingDate": "2023-08-31T00:00:00.000Z"
    },
    {
      "open": 34800,
      "high": 35150,
      "low": 34750,
      "close": 35000,
      "volume": 8001071,
      "tradingDate": "2023-09-05T00:00:00.000Z"
    },
    {
      "open": 35050,
      "high": 35350,
      "low": 34850,
      "close": 35200,
      "volume": 4654739,
      "tradingDate": "2023-09-06T00:00:00.000Z"
    },
    {
      "open": 35250,
      "high": 36150,
      "low": 35100,
      "close": 35750,
      "volume": 9047381,
      "tradingDate": "2023-09-07T00:00:00.000Z"
    },
    {
      "open": 35850,
      "high": 35900,
      "low": 35350,
      "close": 35350,
      "volume": 4604499,
      "tradingDate": "2023-09-08T00:00:00.000Z"
    },
    {
      "open": 35600,
      "high": 35600,
      "low": 34450,
      "close": 34550,
      "volume": 10815264,
      "tradingDate": "2023-09-11T00:00:00.000Z"
    },
    {
      "open": 34550,
      "high": 35300,
      "low": 34400,
      "close": 35300,
      "volume": 4937282,
      "tradingDate": "2023-09-12T00:00:00.000Z"
    },
    {
      "open": 35400,
      "high": 35450,
      "low": 34850,
      "close": 35000,
      "volume": 4980611,
      "tradingDate": "2023-09-13T00:00:00.000Z"
    },
    {
      "open": 35100,
      "high": 35750,
      "low": 34900,
      "close": 34900,
      "volume": 7644272,
      "tradingDate": "2023-09-14T00:00:00.000Z"
    },
    {
      "open": 35100,
      "high": 35500,
      "low": 34900,
      "close": 34900,
      "volume": 4525539,
      "tradingDate": "2023-09-15T00:00:00.000Z"
    },
    {
      "open": 34850,
      "high": 35000,
      "low": 34000,
      "close": 34100,
      "volume": 7733431,
      "tradingDate": "2023-09-18T00:00:00.000Z"
    },
    {
      "open": 34300,
      "high": 34350,
      "low": 33800,
      "close": 33900,
      "volume": 5865883,
      "tradingDate": "2023-09-19T00:00:00.000Z"
    },
    {
      "open": 33900,
      "high": 34500,
      "low": 33850,
      "close": 34350,
      "volume": 3427408,
      "tradingDate": "2023-09-20T00:00:00.000Z"
    },
    {
      "open": 34350,
      "high": 34500,
      "low": 34000,
      "close": 34300,
      "volume": 3166808,
      "tradingDate": "2023-09-21T00:00:00.000Z"
    },
    {
      "open": 33950,
      "high": 34250,
      "low": 33400,
      "close": 34000,
      "volume": 7543775,
      "tradingDate": "2023-09-22T00:00:00.000Z"
    },
    {
      "open": 34000,
      "high": 34200,
      "low": 32750,
      "close": 32800,
      "volume": 4233432,
      "tradingDate": "2023-09-25T00:00:00.000Z"
    },
    {
      "open": 32800,
      "high": 33000,
      "low": 32400,
      "close": 32400,
      "volume": 4757701,
      "tradingDate": "2023-09-26T00:00:00.000Z"
    },
    {
      "open": 32250,
      "high": 32600,
      "low": 32250,
      "close": 32600,
      "volume": 3554965,
      "tradingDate": "2023-09-27T00:00:00.000Z"
    },
    {
      "open": 32600,
      "high": 33900,
      "low": 31850,
      "close": 33650,
      "volume": 6558467,
      "tradingDate": "2023-09-28T00:00:00.000Z"
    },
    {
      "open": 33500,
      "high": 33700,
      "low": 33100,
      "close": 33550,
      "volume": 1941300,
      "tradingDate": "2023-09-29T00:00:00.000Z"
    }
  ],
  "dataT1": null
}