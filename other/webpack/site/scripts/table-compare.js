$(document).ready(function () {
    let tableBenefits = $('.table-compare-component');
    let listColBenefits = tableBenefits.find('.table-compare-content-col');
    let firstColBenefit = listColBenefits.find('.first-col');
  
    tableBenefits.each(function () {
      let firstCol = $(this).find('.first-col');
      let firstColRows = firstCol.find('.table-compare-item-row:not(.first-row)');
      let headerMsgList = [];
      firstColRows.each(function(){
        headerMsgList.push($(this).find('p:not(:empty)').html());
      })
      let rowsNumber = headerMsgList.length;
      //set moveld title for mobile view
      for(let i = 0; i < rowsNumber; i++){
        let rowItems = $(this).find('.table-compare-item-row:not(.first-col-row)[row-index=' + i + ']');
        let movedTitles = rowItems.find('.moved-title')
        movedTitles.html(headerMsgList[i]);
        if ($(window).width() <= 991) {
          movedTitles.removeClass('moved-title');
        }
      }
      //header fit content
      let maxHeight = -1;
      let firstRows = $(this).find('.first-row');
      firstRows.each(function(){
        let firstRowTexts = $(this).find('span');
        firstRowTexts.each(function() {
          if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
          }
        });
        $(this).height(maxHeight);
        $(this).find('picture').height(maxHeight);
      })
      //set all cells in the same rows to have equal height
      maxHeight = -1;
      for(let i = -1; i < rowsNumber; i++){
        let rowItems = $(this).find('.table-compare-item-row[row-index=' + i + ']')
        rowItems.each(function() {
          if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
          }
        });
        rowItems.height(maxHeight);
        maxHeight = -1;
      }
    })

    listColBenefits.each(function () {
      if ($(window).width() <= 991) {
        firstColBenefit.detach();
        $(this).not('.slick-initialized').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: false,
          arrows: false,
          dots: true,
          infinite: true,
          speed: 500,  
          fade: true,  
          cssEase: 'linear'
        });
      }
    });
  
    $(window).resize(function () {
      listColBenefits.each(function () {
        if ($(window).width() <= 991) {
          firstColBenefit.detach();
          $(this).not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false,
            dots: true,
            infinite: true,
          });
        } else {
          $(this).filter('.slick-initialized').slick('unslick');
          $(this).find('.table-compare-item-col').first().before(firstColBenefit);
        }
      });
    });
  });