$(function () {
    registerTextIn();
    registerTextOfNextSibling();

    function registerTextIn() {
        let expression = '@textIn(';
        $('[data-tracking-click-info-value*="' + expression + '"]').each(function () {
            let $ele = $(this);
            let clickInfoValue = $ele.attr('data-tracking-click-info-value');
            let beginExpression = clickInfoValue.indexOf(expression);
            let eleSelector = clickInfoValue.substring(beginExpression + expression.length).replaceAll(")'}", "");
            let $eleText = eleSelector === 'this' ? $ele : $ele.find(eleSelector);
            let newClickInfoValue = clickInfoValue.substr(0, beginExpression) + $eleText.text().trim() + "'}";
            console.log(newClickInfoValue);
            $ele.attr('data-tracking-click-info-value', newClickInfoValue);
        });
    }

    function registerTextOfNextSibling() {
        let expression = '@textInNextSibling(';
        $('[data-tracking-click-info-value*="' + expression + '"]').each(function () {
            let $ele = $(this);
            let clickInfoValue = $ele.attr('data-tracking-click-info-value');
            let beginExpression = clickInfoValue.indexOf(expression);
            let eleSelector = clickInfoValue.substring(beginExpression + expression.length).replaceAll(")'}", "");
            let $eleText = $ele.parent().find(eleSelector);
            let newClickInfoValue = clickInfoValue.substr(0, beginExpression) + $eleText.text().trim() + "'}";
            console.log(newClickInfoValue);
            $ele.attr('data-tracking-click-info-value', newClickInfoValue);
        });
    }
});