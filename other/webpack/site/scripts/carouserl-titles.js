import 'slick-carousel';

// Card Slider
$(document).ready(function () {
    $('.card-slider-container').each(function () {
        // get the number of carousel items displayed per line
        let $component = $(this);
        let numAlias = $component.attr('num-alias');
        let num = 4;
        if (numAlias) {
            if (numAlias == 'card3') num = 3;
            if (numAlias == 'card4') num = 4;
        } else {
            num = parseInt($component.attr('num'));
        }

        // marked class "mobile-slide": If on mobile viewport, display as carousel
        let isMobileSlide = $component.hasClass('mobile-slide');

        let totalSlickitem = $component.find('.cardslider-carousel-slickitem').length;
        let mobileSetting = isMobileSlide ? { slidesToShow: 1, slidesToScroll: 1 } : 'unslick';
        let tabletSetting = num > 3 ? { slidesToShow: 3, slidesToScroll: 1 } : { slidesToShow: num, slidesToScroll: 1 };
        if ($component.attr('arrow-mobile-show') == 0) {
            mobileSetting = isMobileSlide ? { slidesToShow: 1, slidesToScroll: 1, arrows: false } : 'unslick';
        } else {
            mobileSetting = mobileSetting;
        }
        if ($(window).width() >= 768 || isMobileSlide) {
            $component.find('.show-more').remove();
            configImageSlider($component, num, mobileSetting, tabletSetting);
        } else {
            if(totalSlickitem > 4){
                showMoreButton($component.find('.mobile-unslide-items'), $component.attr('data-viewmore'));
            }
        }

    });

    $(window).resize(function () {
        $('.card-slider-container').each(function () {
            let $component = $(this);
            let num = parseInt($component.attr('num'));
            let isMobileSlide = $component.hasClass('mobile-slide');
            let totalSlickitem = $component.find('.cardslider-carousel-slickitem').length;
            let mobileSetting = isMobileSlide ? { slidesToShow: 1, slidesToScroll: 1 } : 'unslick';
            if ($component.attr('arrow-mobile-show') == 0) {
                mobileSetting = isMobileSlide ? { slidesToShow: 1, slidesToScroll: 1, arrows: false } : 'unslick';
            } else {
                mobileSetting = mobileSetting;
            }
            if ($(window).width() >= 768 || isMobileSlide) {
                $component.find('.show-more').remove();
                configImageSlider($component, num, mobileSetting);
            }else {
                if(totalSlickitem > 4){
                    showMoreButton($component.find('.mobile-unslide-items'), $component.attr('data-viewmore'));
                }
            }
        });
        setTimeout(() => {
            $('.slick-arrow').empty();
        }, 200);
    });

    function configImageSlider(el, num, mobileSetting, tabletSetting) {
        let carouselSlicklist = el.find('.cardslider-carousel-slicklist');
        num = !num ? 3 : num;
        //Adobe: return if element not found
        if (carouselSlicklist.length == 0) return;

        carouselSlicklist
            .not('.slick-initialized')
            .not('.cards-un-carousel')
            .slick({
                slidesToShow: !num ? 3 : num,
                slidesToScroll: 1,
                autoplay: false,
                accessibility: true,
                arrows: true,
                dots: true,
                infinite: false,
                rows: 1,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: mobileSetting,
                    },
                    {
                        breakpoint: 1025,
                        settings: tabletSetting,
                    }
                ],
            });
        setTimeout(() => {
            carouselSlicklist.find('.slick-arrow').empty();
        }, 200);
        carouselSlicklist.find('.slick-prev').css('cssText', 'display: none !important');
        carouselSlicklist.find('.slick-next').css('cssText', 'display: block !important');
        carouselSlicklist.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            if (currentSlide == 0) {
                carouselSlicklist.find('.slick-prev').css('cssText', 'display: none !important');
                carouselSlicklist.find('.slick-next').css('cssText', 'display: block !important');
            } else if (slick.$slides.length - num == currentSlide) {
                carouselSlicklist.find('.slick-prev').css('cssText', 'display: block !important');
                carouselSlicklist.find('.slick-next').css('cssText', 'display: none !important');
            } else {
                carouselSlicklist.find('.slick-prev').css('cssText', 'display: block !important');
                carouselSlicklist.find('.slick-next').css('cssText', 'display: block !important');
            }
        });
    }

    function showMoreButton(el, viewMoreLabel) {
        if (el.find('.show-more').length > 0) return;
        const moreButton = $(
        `<div class="show-more">
            <div class="cta-button--light">
                <a class="cta-button" href="#">
                    <span class="cmp-button__text">${viewMoreLabel}</span>
                    <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-dropdown-arrow.svg">
                </a>
            </div>
        </div>`
        );

        moreButton.find('.cta-button').on('click', function() {
            el.find('.slickitem-hidden').css('display', 'block');
            moreButton.remove();
        });
        el.append(moreButton);
    }

    $('.slide-mobile-only').each(function () {
        if ($(window).width() < 768) {
            configImageSlider();
        }
        $(window).resize(function () {
            configImageSlider();
        });

        function configImageSlider() {

            //Adobe: return if element not found
            if ($('.slide-mobile-only .list__items').length == 0) return;

            $('.slide-mobile-only .list__items')
                .not('.slick-initialized')
                .slick({
                    dots: true,
                    mobileFirst: true,
                    infinite: true,
                    autoplay: false,
                    slidesToShow: 1,
                    adaptiveHeight: true,
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: 'unslick',
                        },
                    ],
                });
        }
    });
});
// End of Card Slider