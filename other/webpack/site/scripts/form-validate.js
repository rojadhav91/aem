
window.validateEmail = (email) => {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};

jQuery.fn.forceNumericOnly = function () {
  return this.each(function () {
    $(this).keydown(function (event) {
      if (event.shiftKey) {
        event.preventDefault();
      }
      var key = event.charCode || event.keyCode || 0;
      // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
      // home, end
      return (
        key == 8 ||
        key == 9 ||
        key == 13 ||
        key == 46 ||
        (key >= 35 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105)
      );
    });
  });
};

window.FormControl = class FormControl {
  options = {
    type: 'string',
    required: {
      value: false,
      message: 'Vui lòng nhập vào trường này',
      valid: true,
      invalid: false,
    },
    format: {
      value: false,
      message: 'Vui lòng nhập vào đúng kiểu',
      valid: true,
      invalid: false,
    },
  };
  element;
  parentElement;
  dirty = false;
  errorCondition = '';
  errorMessage = '';
  errorMessageElement;
  isClickSubmit = false;
  constructor(inputElement, options = {}) {
    this.element = inputElement;
    this.options.type = options.type;
    if (options.required) {
      this.options.required = { ...this.options.required, ...options.required };
    }
    if (options.format) {
      this.options.format = { ...this.options.format, ...options.format };
    }
    if (options.parentElement) {
      this.parentElement = options.parentElement;
    }
    this.listenEvents();
  }

  listenEvents() {
    $(this.element).on('input', this.validate.bind(this));
    $(this.element).on('date-change', this.validate.bind(this));
    if (this.options.type === 'number') {
      $(this.element).forceNumericOnly();
    } else if (this.options.type === 'radio') {
      $(this.element).find('input[type="radio"]').on('change', this.validate.bind(this));
    }
  }

  validate() {
    switch (this.options.type) {
      case 'tel':
      case 'phone':
      case 'password':
      case 'number':
      case 'text':
      case 'currency':
        if (this.options.required.value && !this.element.value && (!this.parentElement || !this.parentElement.querySelector('.disabled'))) {
          this.setRequiredMessage();
        } else {
          this.clear();
        }
        break;

      case 'date':
        if (this.options.required.value && (!this.element.value || new Date(this.element.value) === 'Invalid Date')) {
          this.setRequiredMessage();
        } else {
          this.clear();
        }
        break;

      case 'radio':
        if (this.options.required.value && !$(this.element).find('input[type="radio"]:checked').length) {
          this.setRequiredMessage();
        } else {
          this.clear();
        }
        break;

      case 'checkbox':
        if (this.options.required.value && !$(this.element).find('input[type="checkbox"]:checked').length && this.isClickSubmit) {
          this.setRequiredMessage();
        } else {
          this.clear();
        }
        break;

      case 'email':
        this.clear();
        if (this.options.required.value && !this.element.value) {
          this.checkAndShowMessage();
          this.setRequiredMessage();
        } else if (!validateEmail(this.element.value)) {
          this.checkAndShowMessage();
          this.options.required.valid = false;
          this.options.required.invalid = true;
          this.errorCondition = 'format';
          this.errorMessage = this.options.format.message;
        }
        break;

      default:
        break;
    }

    this.checkAndShowMessage();
  }

  setRequiredMessage() {
    this.options.required.valid = false;
    this.options.required.invalid = true;
    this.errorCondition = 'required';
    this.errorMessage = this.options.required.message;
  }

  clear() {
    this.options.required.valid = true;
    this.options.required.invalid = false;
    this.errorCondition = '';
    this.errorMessage = '';
  }

  checkAndShowMessage() {
    const input = $(this.element)[0];
    const parent = this.parentElement ? $(this.parentElement) : $(input.parentElement);
    if (this.errorMessage) {
      if (this.errorMessageElement) {
        return;
      }
      this.errorMessageElement = $(`<p class="form-error-message">${this.errorMessage}</p>`)[0];
      parent.append(this.errorMessageElement);
      parent.addClass('error');
    } else if (this.errorMessageElement) {
      parent[0].removeChild(this.errorMessageElement);
      parent.removeClass('error');
      this.errorMessageElement = null;
    }
  }
};

window.FormGroup = class FormGroup {
  controls = [];
  registerField(control) {
    this.controls.push(control);
  }

  checkAllFields() {
    this.controls.forEach((control) => {
      control.dirty = true;
      control.isClickSubmit = true;
      control.validate();
    });
    this.scrollToFirstErrorField();
  }

  scrollToFirstErrorField() {
    const control = this.controls.find((control) => !control.valid);
    if (control) {
      const top = $(control.element).offset().top;
      $('html, body').animate({ scrollTop: top + document.body.scrollTop - 140 }, 300);
    }
  }
};
$('.cmp-form-text__text').keypress(function (e) {
  var charCode = e.which ? e.which : e.keyCode;
  var dataType = $(this).attr('type');
  if ((dataType === 'currency' || dataType === 'phone') && String.fromCharCode(charCode).match(/[^0-9]/g)) {
    return false;
  }
});

$('.cmp-form-text__text[type=currency]').on('paste', function(event) {
  if (event.originalEvent.clipboardData.getData('Text').match(/[^\d\.\,]/)) {
    event.preventDefault();
  }
});

$('.cmp-form-text__text').keyup(function (e) {
  if($(this).attr('type') === 'currency') {
    var currency = e.target.value.replaceAll(',', '');
    e.target.value = numberWithCommas(currency);
    
  }
});

function numberWithDot(x) {
  if(x) {
    return x
    .toString()
    .replaceAll('.', '')
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }
  return "";
}

function numberWithCommas(x) {
  return numberWithDot(x).replaceAll('.', ',');
}

$('form').each(function () {
  const formGroup = new FormGroup();
  const listInput = $(this).find('input[type]:not(:radio, :checkbox), textarea');
  const listRadio =$(this).find('.radio');
  const listCheckBox =$(this).find('.checkbox');
  const listDropdown =$(this).find('.dropdown');
  listInput.each(function(i,e) {
    let message,messageFormat;
    message = e.getAttribute('data-required');
    messageFormat = e.getAttribute('data-format');
    let type = e.hasAttribute('data-is-email-type') ? 'email' : e.getAttribute('type');
    const textInput = new FormControl(e, {
      type: type || 'text',
      required: { value: true, message: message || '' },
      format: { message : messageFormat || '' },
    });
    formGroup.registerField(textInput);
  });
  
  listRadio.each(function(i,e) {
    const message = e.getAttribute('data-required') || '';
    const radio = new FormControl(e, {
      type: 'radio',
      required: { value: true, message },
      parentElement: e,
    });
    formGroup.registerField(radio);
  });
  
  listCheckBox.each(function(i,e) {
    const message = e.getAttribute('data-required') || '';
    const checkbox = new FormControl(e, {
      type: 'checkbox',
      required: { value: true, message },
      parentElement: e,
    });
    formGroup.registerField(checkbox);
  });
  
  listDropdown.each(function(i,e) {
    const inputDropdown = e.querySelector('.dropdown-input');
    const dropdown = new FormControl(inputDropdown, {
      type: 'text',
      required: { value: true, message: e.getAttribute('data-required') },
      parentElement: e,
    });
    formGroup.registerField(dropdown);
  });

  window.validateForm = (event) => {
    event.preventDefault();
    formGroup.checkAllFields();
    var checkRecaptcha = document.querySelectorAll(".recaptcha-container");
    if (checkRecaptcha.length) {
      recaptchaValidation(this); // changed by adobe
    }
    if (document.querySelectorAll('.form-error-message').length === 0) { //changed by adobe
      return true;
    }
  };
});
