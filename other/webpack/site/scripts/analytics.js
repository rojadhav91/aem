const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
const splitParams = (pageUrlParams) => {
    if (!pageUrlParams) {
        return;
    }
    var data = {};
    pageUrlParams.split('?')[1].split('&').forEach(function(d) {
        var pair = d.split('=');
        data[pair[0]] = pair[1];
    });

    return data;
};
const getPageUrlParams = (pageUrlParams) => {
    if (!pageUrlParams) {
        return;
    }
    var pageParams = '';
    for (const [key, value] of Object.entries(pageUrlParams)) {
        pageParams += key + ':' + value + '|';
    }
    if (pageParams.length == 0) {
        return null;
    }
    return pageParams.substr(0, pageParams.length - 1);
};
const getPageName = (pageUrl, trackingBusinessUnit, trackingPlatform, trackingCountry, trackingLanguage, pageName) => {
	var values = [trackingBusinessUnit, trackingPlatform, trackingCountry, pageName];
	var page = '';

	for (var i = 0; i < values.length; i++) {
	  if (values[i] !== undefined && values[i] !== null) {
		page += (page === '' ? '' : ':') + values[i];
	  }
	}
    return page;
};
//Method to get the tracking code from the URL params

const getTrackingCode = (pageUrlParams) => {

    const trackingCodeParams = ['utm_medium', 'utm_source', 'utm_campaign', 'utm_content'];
    var trackingCode = '';

    trackingCodeParams.forEach((trackingItem) => {
        if (!pageUrlParams || pageUrlParams == undefined) {
            return trackingCode += ":";
        }
        trackingCode += pageUrlParams[trackingItem] != undefined ? pageUrlParams[trackingItem] + ":" : ':';
    });
    return trackingCode.substr(0, trackingCode.length - 1);
};

const mainElement = $('body');

const pageUrl = window.location.href;
const pageName = mainElement.data('trackingPageName') != undefined ?  formatString(mainElement.data('trackingPageName').toString().normalize('NFD').replace(/[\u0300-\u036f]/g, '')) :  mainElement.data('trackingPageName');
const pageHostName = window.location.hostname;
const pageUrlParamsArray = splitParams(window.location.search);
const trackingSiteSection = mainElement.data('trackingSiteSection') != undefined ? formatString(mainElement.data('trackingSiteSection').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingSiteSection');
const trackingBusinessUnit = mainElement.data('trackingBusinessUnit') != undefined ? formatString(mainElement.data('trackingBusinessUnit').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingBusinessUnit');
const trackingSiteEnv = mainElement.data('trackingSiteEnv') != undefined ? formatString(mainElement.data('trackingSiteEnv').normalize('NFD').replace(/\p{Diacritic}/gu, "")) : mainElement.data('trackingSiteEnv');
const trackingPlatform = mainElement.data('trackingPlatform')  != undefined ? formatString(mainElement.data('trackingPlatform').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingPlatform');
const trackingProduct = mainElement.data('trackingProductName') != undefined ? formatString(mainElement.data('trackingProductName').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingProductName');
const trackingJourney = mainElement.data('trackingJourney') != undefined ? formatString(mainElement.data('trackingJourney').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingJourney');
const trackingCountry = mainElement.data('trackingCountry') != undefined ? formatString(mainElement.data('trackingCountry').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : mainElement.data('trackingCountry');
const trackingLanguage = $('html').attr('lang');
if ($('body').data('trackingEvent')) {
    addDataLayerObject($('body').data('trackingEvent'));
}

$('body').on('click', '[data-tracking-click-event]', (elem) => {
    const element = $(elem.currentTarget);
    const eventName = element.data('trackingClickEvent') != undefined ? formatString(element.data('trackingClickEvent').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : undefined; //data-tracking-click-event
    const trackingWebInteractionKey = "web";
    const trackingWebInteractionValue = element.data('trackingWebInteractionValue') != undefined ? formatString(element.data('trackingWebInteractionValue').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : undefined; //data-tracking-web-interaction-value
    const trackingClickInfoKey = element.data('trackingClickInfoKey') ? element.data('trackingClickInfoKey') : "clickInfo";
    const trackingClickInfoValue = element.attr('data-tracking-click-info-value') != undefined ? formatString(element.attr('data-tracking-click-info-value').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : undefined; //data-tracking-click-info-value
    const trackingFormInfoKey = element.data('trackingFormInfoKey') ? element.data('trackingFormInfoKey') : "formInfo";
    const trackingFormInfoValue = element.attr('data-tracking-form-info-value') != undefined ? formatString(element.attr('data-tracking-form-info-value').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : undefined; //data-tracking-form-info-value
    const trackingUserInfoKey = element.data('trackingUserInfoKey') ? element.data('trackingUserInfoKey') : "userInfo";
    const trackingUserInfoValue = element.attr('data-tracking-user-info-value') != undefined ? formatString(element.attr('data-tracking-user-info-value').normalize('NFD').replace(/[\u0300-\u036f]/g, '')) : undefined; //data-tracking-user-info-value

    var elementDataValues = trackingWebInteractionKey != undefined ? JSON.parse(formatString(trackingWebInteractionValue.replaceAll('\'', '"').normalize('NFD').replace(/[\u0300-\u036f]/g, ''))) : undefined;
    var elementClickValues = trackingClickInfoValue != undefined ? JSON.parse(formatString(trackingClickInfoValue.replaceAll('\'', '"').normalize('NFD').replace(/[\u0300-\u036f]/g, ''))) : undefined;
    var elementFormValues = trackingFormInfoValue != undefined ? JSON.parse(formatString(trackingFormInfoValue.replaceAll('\'', '"').normalize('NFD').replace(/[\u0300-\u036f]/g, ''))) : undefined;
    var elementUserValues = trackingUserInfoValue != undefined ? JSON.parse(formatString(trackingUserInfoValue.replaceAll('\'', '"').normalize('NFD').replace(/[\u0300-\u036f]/g, ''))) : undefined;

    if ((trackingWebInteractionKey != undefined && elementDataValues != undefined) || (trackingClickInfoValue != undefined && trackingClickInfoKey != undefined) || (trackingFormInfoKey != undefined && trackingFormInfoValue != undefined) || (trackingUserInfoKey != undefined && trackingUserInfoValue != undefined)) {
        addDataLayerObject(eventName, trackingWebInteractionKey, elementDataValues, trackingClickInfoKey, elementClickValues, trackingFormInfoKey, elementFormValues, trackingUserInfoKey, elementUserValues);
    } else {
        addDataLayerObject(eventName);
    }

});

function addDataLayerObject(eventName, additionalTrackingDataKey, additionalTrackingDataObject, additionalClickDataKey, additionalClickObject,  additionalFormDataKey, additionalFormObject, additionalUserDataKey, additionalUserObject) {
    dataLayerObject = {
        "event": eventName,
        "_techcombank": {
            "pageName": getPageName(pageUrl != undefined ? pageUrl : '', trackingBusinessUnit != undefined ? trackingBusinessUnit : '', trackingPlatform != undefined ? trackingPlatform : '', trackingCountry != undefined ? trackingCountry : '', trackingLanguage != undefined ? trackingLanguage : '', pageName != undefined ? pageName : ''),
            "pageURL": pageUrl,
            "pageURLParam": getPageUrlParams(pageUrlParamsArray) != undefined ? getPageUrlParams(pageUrlParamsArray) : '',
            "siteSection": trackingSiteSection != undefined ? trackingSiteSection : '',
            "language": trackingLanguage,
            "country": trackingCountry,
            "environment": trackingSiteEnv != undefined ? trackingSiteEnv : '',
            "businessUnit": trackingBusinessUnit != undefined ? trackingBusinessUnit : '',
            "platform": trackingPlatform != undefined ? trackingPlatform : '',
            "productName": trackingProduct != undefined ? trackingProduct : '',
            "journey": trackingJourney != undefined ? trackingJourney : '',
            "dayOfWeek": days[(new Date()).getDay()],
            "timestamp": new Date(new Date().getTime() + 7 * 60 * 60000).toISOString().substring(0, 19) + 'Z',
            "cookiesEnabled": navigator.cookieEnabled ? 'y' : 'n',
            "defaultBrowserLanguage": navigator.language || navigator.userLanguage,
            "userInfo": {
                "status": "not logged-in",
            },
        },
        "marketing": {
            "trackingCode": getTrackingCode(pageUrlParamsArray) != undefined ? getTrackingCode(pageUrlParamsArray) : '',
        }
    };
    if (typeof additionalUserObject == 'undefined') {
        delete dataLayerObject._techcombank.userInfo;
    }
    if (typeof additionalUserDataKey != 'undefined' && typeof additionalUserObject != 'undefined') {
        dataLayerObject._techcombank[additionalUserDataKey] = additionalUserObject;
    }
    if (typeof additionalTrackingDataKey != 'undefined' && typeof additionalTrackingDataObject != 'undefined') {
        dataLayerObject[additionalTrackingDataKey] = additionalTrackingDataObject;
    }
    if (typeof additionalClickDataKey != 'undefined' && typeof additionalClickObject != 'undefined') {
        dataLayerObject._techcombank[additionalClickDataKey] = additionalClickObject;
    }
    if (typeof additionalFormDataKey != 'undefined' && typeof additionalFormObject != 'undefined') {
        dataLayerObject._techcombank[additionalFormDataKey] = additionalFormObject;
    }
    window.adobeDataLayer = window.adobeDataLayer || [];
    window.adobeDataLayer.push(dataLayerObject);
}
//Method to remove accent from text.
function formatString(str) {
    str = str.replace(/ĩ/g, "i");
    str = str.replace(/ø/g, "o");
    str = str.replace(/đ/g, "d");
    str = str.replace(/Đ/g, "D");
    return str;
}

//Error Page Analytics
if ($(".error-page-component").length > 0) {
    let errorMessageVal = $(".error-page-component").data().trackingClickInfoValue;
      if(errorMessageVal) {
        let formattedJson = $(".error-page-component").data().errorMessage.replace(/'/g, '’');
        let jsonStr = errorMessageVal.replace(/'/g, '"');
        let parsedJson = JSON.parse(jsonStr);
        parsedJson.errorMessage = formattedJson;
        let updatedValues = JSON.stringify(parsedJson).replace(/"/g, "'");
        $(".error-page-component").attr("data-tracking-click-info-value", updatedValues);
      }
	$(".error-page-component").trigger("click");
}
