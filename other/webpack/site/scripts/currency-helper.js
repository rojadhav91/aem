export default class CurrencyHelper {
  static numberWithDot(x) {
    return x
      .toString()
      .replaceAll('.', '')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }
  static numberWithCommas(x, locale) {
    if (x || x === 0) {
      let number = new Number(x);
      return number?.toLocaleString(locale ?? 'en-US', {maximumFractionDigits: 17});
    }
    return '';
  }
}