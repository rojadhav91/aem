// Security Slider
$(document).ready(function () {
    if ($(window).width() <= 768) {
      $('.security-slider').each(function () {
        const notSlider = $(this).find('.not-slider-mobile');
        if ($(window).width() <= 768 && notSlider.length === 0) {
          $(this)
            .find('.security-slider-container')
            .not('.slick-initialized')
            .slick({
              slidesToShow: 3,
              slidesToScroll: 3,
              autoplay: false,
              accessibility: true,
              arrows: true,
              infinite: false,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                  },
                },
                {
                  breakpoint: 992,
                  settings: 'unslick',
                },
              ],
            });
          $('.security-slider-container .slick-next').text('');
          $('.security-slider-container .slick-prev').text('');
          $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
          $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
          $('.security-slider-container').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            if ($(window).width() > 1200) {
              if (currentSlide == 0) {
                $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
              } else if (slick.$slides.length - 4 == currentSlide) {
                $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: none !important');
              } else {
                $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
              }
            } else {
              if (currentSlide == 0) {
                $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
              } else if (slick.$slides.length - 3 == currentSlide) {
                $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: none !important');
              } else {
                $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
              }
            }
          });
        }
        $(window).resize(function () {
          if ($(window).width() <= 768 && !$(this).hasClass('not-slider-mobile')) {
            $(this)
              .find('.security-slider-container')
              .not('.slick-initialized')
              .slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                autoplay: false,
                accessibility: true,
                arrows: true,
                infinite: false,
                responsive: [
                  {
                    breakpoint: 768,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                    },
                  },
                  {
                    breakpoint: 992,
                    settings: 'unslick',
                  },
                ],
              });
            $('.security-slider-container .slick-next').text('');
            $('.security-slider-container .slick-prev').text('');
            $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
            $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
            $('.security-slider-container').on('afterChange', function (event, slick, currentSlide, nextSlide) {
              if ($(window).width() > 1200) {
                if (currentSlide == 0) {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
                } else if (slick.$slides.length - 4 == currentSlide) {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: none !important');
                } else {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
                }
              } else {
                if (currentSlide == 0) {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: none !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
                } else if (slick.$slides.length - 3 == currentSlide) {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: none !important');
                } else {
                  $('.security-slider-container .slick-prev').css('cssText', 'display: block !important');
                  $('.security-slider-container .slick-next').css('cssText', 'display: block !important');
                }
              }
            });
          }
        });
      });
    }
  });
  // End of Security Slider