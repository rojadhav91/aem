$.fn.cardMultipleSelectOnChanged = function(values) {
  if (!this.hasClass('card-multiple-select')) {
    throw new Error('This element does not have "card-multiple-select" class');
  }
  let cardFilter = $(this);
  let cardFilterContainer = cardFilter.find('.card-multiple-select__container');
  let cardFilterItems = cardFilterContainer.find('.card-multiple-select__items');
  let cardFilterButton = cardFilterItems.find('.card-multiple-select__button');
  let newValues = Array.from(values);
  cardFilterButton.each(function () {
    let value = $(this).data('id');
    if (newValues.includes(value)) {
      $(this).addClass('filter-selected');
    } else {
      $(this).removeClass('filter-selected');
    }
  });
};
$(document).ready(function() {
  $('.card-multiple-select').each(function() {
    let cardFilter = $(this);
    let options = []
    try {
      options = eval(cardFilter.data('options'));
    } catch {

    }
    let title = cardFilter.data('title');
    let isWrap = cardFilter.data('wrap') ?? true;
    if (isWrap) {
      cardFilter.addClass('wrap');
    }
    cardFilter.empty();
    cardFilter.append(`<div class="card-multiple-select__content"><div class="content-wrapper card-multiple-select__container"><h6 class="card-multiple-select__title">${title}</h6><div class="card-multiple-select__items"></div></div></div>`);
    let cardFilterContainer = cardFilter.find('.card-multiple-select__container');
    let cardFilterItems = cardFilterContainer.find('.card-multiple-select__items');
    options?.forEach((option, index) => {
      cardFilterItems.append(`
      <div class="filter__item">
        <button type="button" class="card-multiple-select__button big-size" data-id="${option.id}">${option.title}</button>
      </div>`
      );
    });
    let cardFilterButton = cardFilterItems.find('.card-multiple-select__button');
    let selectedValues = new Set();

    cardFilterButton.each(function () {
      $(this).click(function () {
        let id = $(this).data('id');
        if ($(this).hasClass('filter-selected')) {
          $(this).removeClass('filter-selected');
          selectedValues.delete(id);
        } else {
          $(this).addClass('filter-selected');
          selectedValues.add(id);
        }
        $(this).trigger('onChanged', selectedValues);
      });
    });
  });
  
});