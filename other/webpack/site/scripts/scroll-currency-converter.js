window.ScrollCurrencyConverter = class ScrollCurrencyConverter {
  wrapper;
  control;
  scroller;
  next;
  prev;
  constructor(element) {
    this.wrapper = element;
    this.init();
  }

  init() {
    const wrap = $(this.wrapper);
    this.scroller = wrap.find('.scroller')[0];
    this.prev = $('<div class="tcb-scroll-control tcb-scroll-control_prev"><span class="material-symbols-outlined chevron_left">chevron_left</span></div>')[0];
    this.next = $('<div class="tcb-scroll-control tcb-scroll-control_next"><span class="material-symbols-outlined">chevron_right</span></div>')[0];
    this.control = $('<div class="tcb-scroll-controls"></div>')[0];
    $(this.control).append(this.prev);
    $(this.control).append(this.next);
    wrap.append(this.control);

    this.checkScrollable();

    $(this.scroller)
      .find('.slick-list')
      .on('mouseup', this.checkScrollable.bind(this, 500));

    $(window).on('resize', this.checkScrollable.bind(this, 300));
    $(this.next).on('click', this.handleNext.bind(this, 1));
    $(this.prev).on('click', this.handlePrev.bind(this, 300));
  }

  checkScrollable(e) {
    setTimeout(() => {
      const slickList = $(this.scroller).find('.slick-track')[0]?.children;
      if (slickList) {
        if (slickList[0].getAttribute('aria-hidden') == 'true') {
          this.wrapper.classList.add('can-prev');
          this.wrapper.classList.remove('track-first');
        } else {
          this.wrapper.classList.remove('can-prev');
          this.wrapper.classList.remove('track-first');
        }
        if (
          $(slickList[slickList.length - 1])
            .get(0)
            .getBoundingClientRect().right >
          this.scroller.getBoundingClientRect().right
        ) {
          this.wrapper.classList.add('can-next');
        } else {
          this.wrapper.classList.remove('can-next');
        }
      }
    }, e);
  }

  handleNext(e) {
    const slickNext = $(this.scroller).find('.slick-next')[0];
    const slickList = $(this.scroller).find('.slick-track')[0]?.children;
    if (
      $(slickList[slickList.length - 1])
        .get(0)
        .getBoundingClientRect().right >
      this.scroller.getBoundingClientRect().right
    ) {
      slickNext.click();
      this.checkScrollable(e);
    }
  }

  handlePrev(e) {
    const slickPrev = $(this.scroller).find('.slick-prev')[0];
    slickPrev.click();
    this.checkScrollable(e);
  }
};
