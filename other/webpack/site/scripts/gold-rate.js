import moment from 'moment';
import './currency-helper';
import CurrencyHelper from './currency-helper';

// Fixing rate
$(document).ready(function () {
  let fixingRate = $('.gold-rate');
  let timeSelect = fixingRate.find('.time-select');

  let tableContainer = fixingRate.find('.table-content-container');
  let emptyLabel = fixingRate.find('.exchange-rate__empty-label');
  let tableElements = fixingRate.find('.exchange-rate-table-content');
  let selectedDateElements = fixingRate.find('.calendar__input-field');
  let defaultGoldLabel = fixingRate.attr('data-label');

  let currentDate = moment().format('DD/MM/yyyy');
  let currentTime = null;

  function generateTable(element, items) {
    element.empty();
    items.forEach((item, idx, array) => {
      let lastRow = (idx === array.length - 1);
      element.append(
        `
      <div class="exchange-rate__table-records">
        <div class="table__first-column first-column ${lastRow ? 'last-row' : ''}">
          <p>${item.label ?? defaultGoldLabel ?? ''}</p>
        </div>
        <div class="table-records__data">
          <div class="table-records__data-content ${lastRow ? 'last-row' : ''}">
            <p>${CurrencyHelper.numberWithCommas(item.bidRate)}</p>
          </div>
          <div class="table-records__data-content ${lastRow ? 'last-row' : ''} last-column">
            <p>${CurrencyHelper.numberWithCommas(item.askRate)}</p>
          </div>
        </div>
      </div>
    `
      );
    });
  }

  selectedDateElements.each((idx, element) => {
    $(element).text(currentDate);
    $(element).on('updateCal', function () {
      if (currentDate === $(element).text()) {
        return;
      }
      currentDate = $(element).text();
      currentTime = null;
      updateData(currentDate, currentTime);
    });
  });

  timeSelect.on('timeChanged', function(event, newValue) {
    currentTime = newValue;
    updateData(currentDate, currentTime);
  });

  updateData(currentDate, null);

  function updateData(date, time) {
    selectedDateElements.each((idx, element) => {
      $(element).text(currentDate);
    });
    let baseUrl = fixingRate.attr('data-url');
    if (baseUrl === undefined) {
      return;
    }
    let url = baseUrl + '/_jcr_content.gold-rates';
    if (date) {
      url += `.${date.split('/').toReversed().join('-')}`;
      if (time) {
        url += `.${time.replaceAll(':', '-')}`;
      }
    }
    url += '.integration.json';
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        let goldRate = data?.goldRate;
        // If not available
        if (goldRate === null || goldRate.data.length === 0) {
          emptyLabel.removeClass('hidden');
          tableContainer.addClass('hidden');
          timeSelect.changeOptions(goldRate.updatedTimes, currentTime);
          return;
        }

        emptyLabel.addClass('hidden');
        tableContainer.removeClass('hidden');

        // generate table
        tableElements.each((idx, element) => {
          generateTable($(element), goldRate.data);
        });
        timeSelect.changeOptions(goldRate.updatedTimes, currentTime);
        if (currentTime === null && goldRate.updatedTimes.length > 0) {
          currentTime = goldRate.updatedTimes[0];
        }
        upadateDateTimeStamp();
      },
    });
  }

  //analytics funtion to capture selected data & time from exchange rate : start
  function upadateDateTimeStamp(){
    fixingRate.find('.analytics-active-link').click();
  }
  
  fixingRate.find('.analytics-active-link').on('click',function(){
      $(this).datetimeAnalyticUpdate(currentDate, currentTime);
  });

  upadateDateTimeStamp(); // analytics function call on DOM load to update value 
});
