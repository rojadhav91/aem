import Swiper, {
  FreeMode,
  Autoplay,
} from "swiper";

// Inspire Privilege Component
const inspirePrivilege = $(".inspire-privilege");
if (inspirePrivilege.length) {
  // Add Swiper for single picture row with arrow button container

  if(window.innerWidth >= 767) {
    $('.swiper1 .swiper-wrapper').not('.slick-initialized').each(function(num, elem) {
      elem = $(elem);
      elem.slick({
        slidesToShow: 1,
        centerMode: true,
        arrows: true,
        dots: false,
        infinite: true,
        variableWidth: true,
        prevArrow: elem.parent().find('.swiper-button-prev'),
        nextArrow: elem.parent().find('.swiper-button-next')
      });
    });
  }
  else{
    // scroll in mobile
    setTimeout(() => {
      $('.inspire-exclusive__tab-menu-mobile .swiper1 .swiper-wrapper').not('.slick-initialized').each(function(num, elem) {
        elem = $(elem);
        elem.slick({
          slidesToShow: 1,
          centerMode: true,
          arrows: true,
          dots: false,
          infinite: true,
          variableWidth: true,
          prevArrow: elem.parent().find('.swiper-button-prev'),
          nextArrow: elem.parent().find('.swiper-button-next')
        });
      });
    }, 1000); 
  }

  $(".content-item__multi-picture").each(function () {
    $(this).find(".swiper2:nth-child(even)").attr("dir", "rtl");
  });

  $(".content-item__multi-picture").each(function () {
    $(this)
      .find(".swiper2")
      .each(function (indexKey, element) {
        let swiperScrolling = new Swiper(element, {
          speed: 1000,
          modules: [FreeMode, Autoplay],
          // Optional parameters
          slidesPerView: "auto",
          spaceBetween: 8,
          loop: true,
          loopedSlides: 5,
          loopedSlidesLimit: true,
          maxBackfaceHiddenSlides: 10,
          freeMode: {
            enabled: true,
            minimumVelocity: 0.02,
          },
        });

        let startTranslate = swiperScrolling.translate;

        document.body.addEventListener("scroll", function (event) {
          var bodyScrolltop = $(this).scrollTop();
          scrollSwiper(bodyScrolltop, indexKey, startTranslate, swiperScrolling);
        });

        // trigger scroll animation if open swiper inside tabs
        var tabClickTime = 0;
        $(element).on('animate-scroll-in-tabs', function () {
          tabClickTime++;
          scrollSwiper((tabClickTime % 2) * 500, indexKey, startTranslate, swiperScrolling);
        });
      });

    function scrollSwiper(bodyScrolltop, indexKey, startTranslate, swiperScrolling) {
      let currentProgress = bodyScrolltop / $(document).innerHeight();
      if (indexKey % 2 === 0) {
        let progress = Math.min(Math.max(currentProgress, 0), 1);
        const max2 =
          swiperScrolling.snapGrid[
          parseInt(swiperScrolling.snapGrid.length / 2)
          ];
        const current = startTranslate + max2 * progress;
        swiperScrolling.translateTo(current, 500);
      } else {
        let progress = Math.min(Math.max(currentProgress, 0), 1);
        const max2 =
          swiperScrolling.snapGrid[
          parseInt(swiperScrolling.snapGrid.length / 2)
          ];
        const current = (startTranslate - max2 * progress) * -1;
        swiperScrolling.translateTo(current, 500);
      }
    }
  });
}
