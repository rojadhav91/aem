// START script for DROPDOWN Component
$(function () {
  let isShowDropdownList = false;

  const rotateIcon = (deg, icon) => {
    (icon ?? $('.dropdown__display .display__icon')).css({
      transform: `rotate(${deg})`,
      'transition-duration': '0.3s',
    });
  };

  const turnOnDropdownList = (dropdown, display, dropdownUl) => {
    const selectedValue = display?.find('span.display__text')?.text();
    if (selectedValue && selectedValue.length && selectedValue.length > 0) {
      dropdownUl?.find('li.dropdown__item')?.each(function() {
        const elementValue = $(this)?.attr('data-merchant');
        if (selectedValue?.toLowerCase() === elementValue?.toLowerCase()) {
          $(this).css('background-color', 'rgba(0, 0, 0, 0.08)');
        } else {
          $(this).css('background-color', '');
        }
      });
    }

    const bodyOuterHeight = $('body').outerHeight();
    const dropdownDisplayOffsetTop = display.offset().top;
    const dropdownDisplayOuterHeight = display.outerHeight();
    const dropdownListOuterHeight = dropdown.outerHeight();

    if (dropdownDisplayOffsetTop < bodyOuterHeight && ((dropdownDisplayOffsetTop + dropdownListOuterHeight) > bodyOuterHeight)) {
      const bottomProperty = (dropdownDisplayOffsetTop + dropdownDisplayOuterHeight + 15) - bodyOuterHeight;
      dropdown.css('bottom', `${bottomProperty}px`);
      dropdown.css('top', 'unset');
      dropdown.css('transform-origin', 'bottom');
    } else {
      dropdown.css('top', '0');
      dropdown.css('transform-origin', 'top');
    }

    dropdown.css({
      opacity: '1',
      transform: 'scaleY(1)',
    });
    isShowDropdownList = true;
    rotateIcon('180deg', dropdown.find('.dropdown__display .display__icon'));
  };

  const turnOffDropdownList = () => {
    const dropdown = $('.dropdown__list');
    const display = $('.dropdown__display');
    const bodyOuterHeight = $('body').outerHeight();
    const dropdownDisplayOffsetTop = display.offset().top;
    const dropdownDisplayOuterHeight = display.outerHeight();
    const dropdownListOuterHeight = dropdown.outerHeight();

    if (dropdownDisplayOffsetTop < bodyOuterHeight && ((dropdownDisplayOffsetTop + dropdownListOuterHeight) > bodyOuterHeight)) {
      const bottomProperty = (dropdownDisplayOffsetTop + dropdownDisplayOuterHeight + 15) - bodyOuterHeight;
      dropdown.css('bottom', `${bottomProperty}px`);
      dropdown.css('top', 'unset');
      dropdown.css('transform-origin', 'bottom');
    } else {
      dropdown.css('top', '0');
      dropdown.css('bottom', '');
      dropdown.css('transform-origin', 'top');
    }

    dropdown.css({
      opacity: '0',
      transform: 'scaleY(0)',
    });
    isShowDropdownList = false;
    rotateIcon('0deg');
  };

  const selectDropdownItemHandler = (displayText, selectedField, element) => {
    displayText.text(element.textContent.replace(/\s+/g, " ").trim());
    selectedField.val(element.getAttribute('value'));
    selectedField.change();
  };

  $('.offer-filter__dropdown').each(function() {
    let dropdown = $(this).find('.dropdown__list');
    let dropdownUl =  dropdown.find('ul');
    let display = $(this).find('.dropdown__display');
    let displayText = $(this).find('.display__text');
    let selectedField = $(this).find('.dropdown-selected-value');
    // Get initial options
    let initialOptions = [];
    dropdownUl
      .find('li.dropdown__item')
      .each(function () {
        initialOptions.push({
          id: $(this).attr('value'),
          title: $(this).text(),
        });
      });
    
    dropdownUl.on('click', 'li', (e) => {
      selectDropdownItemHandler(displayText, selectedField, e.target);
      turnOffDropdownList();
    });
    
    display.click(() => {
      isShowDropdownList ? turnOffDropdownList() : turnOnDropdownList(dropdown, display, dropdownUl);
    });
  });

  $('body').on('scroll', function() {
    if (!isShowDropdownList) {
      $('.dropdown__list').css('bottom', '');
    }
  });

  $('body').on('click', function (event) {
    if ($(event.target).parents('.dropdown__wrapper').length === 0 && !$(event.target).hasClass('offer-filter__container') && isShowDropdownList) {
      turnOffDropdownList();
    }
  });
});
// END script for DROPDOWN Component

// START script for AUTOCOMPLETE Component
$(function () {
  let isShowAutoCompleteList = false;

  const turnOnAutoCompleteList = (target) => {
    target.css({
      opacity: '1',
      transform: 'scaleY(1)',
    });
    isShowAutoCompleteList = true;
  };

  const turnOffAutoCompleteList = (reset=false) => {
    $('.autocomplete__list').css({
      opacity: '0',
      transform: 'scaleY(0)',
    });
    isShowAutoCompleteList = false;
    if (reset) {
      $('.offer-filter__autocomplete').each(function () {
        let inputField = $(this).find('input.autocomplete__input-text');
        let selectedField = $(this).find('input.autocomplete-selected-value');
        inputField.val(selectedField.text());
      });
    }
  };

  const selectAutoCompleteItemHandler = (
    inputField,
    selectedField,
    element
  ) => {
    inputField.val(element.textContent.replace(/\s+/g, " ").trim());
    selectedField.val(element.getAttribute('value'));
    selectedField.text(element.textContent.replace(/\s+/g, " ").trim());
    selectedField.change();
  };

  const autoCompleteMatch = (input, arr) => {
    let reg = new RegExp(input.toLowerCase());
    return arr.filter((item) => item.title.toLowerCase().match(reg));
  };

  const initDataSourceAutoComplete = (_dropdown, data) => {
    let elements = data.map(
      (obj) =>
        `<li class="autocomplete__item" value="${obj.id}">${obj.title}</li>`
    );
    _dropdown.find('ul').html(elements);
  };

  $('.offer-filter__autocomplete').each(function () {
    let inputField = $(this).find('input.autocomplete__input-text');
    let selectedField = $(this).find('input.autocomplete-selected-value');
    let dropdown = $(this).find('.autocomplete__list');
    let currentText = '';

    // Get initial options
    let initialOptions = [];
    $(this)
      .find('.autocomplete__list ul li.autocomplete__item')
      .each(function () {
        initialOptions.push({
          id: $(this).attr('value'),
          title: $(this).text(),
        });
      });

    // on selected
    dropdown
      .find('ul')
      .on('click', 'li', (e) => {
        if ($(e.target).hasClass('disabled')) {
          inputField.val(currentText);
        } else {
          selectAutoCompleteItemHandler(inputField, selectedField, e.target);
          currentText = e.target.textContent.replace(/\s+/g, " ").trim();
        }
        turnOffAutoCompleteList();
        $('.is-banner').hide();
        
      });

    inputField.click(() => {
      if (isShowAutoCompleteList && inputField.val() == '') {
        turnOffAutoCompleteList();
      } else {
        initDataSourceAutoComplete(dropdown, initialOptions);
        turnOnAutoCompleteList(dropdown);
      }
    });

    inputField.on('input', function () {
      if (this.value == '') {
        initDataSourceAutoComplete(dropdown, initialOptions);
        dropdown.removeClass('--fit-content');
        return;
      }
      let results = autoCompleteMatch(this.value, initialOptions);
      let elements = results.map(
        (obj) =>
          `<li class="autocomplete__item" value="${obj.id}">${obj.title}</li>`
      );
      if (elements.length == 0) {
        elements = '<li class="autocomplete__item disabled" value="-1">No options</li>';
      }
      dropdown.find('ul').html(elements);
      dropdown.addClass('--fit-content');
    });
  });

  $('body').on('click', function (event) {
    if ($(event.target).parents('.autocomplete__display').length === 0 && !$(event.target).hasClass('offer-filter__container') && isShowAutoCompleteList) {
      turnOffAutoCompleteList(true);
    }
  });
});
// END script for AUTOCOMPLETE Component
