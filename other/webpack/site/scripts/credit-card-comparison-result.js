// Component Compare Product
$(document).ready(function () {
  $('.credit-card-comparison-result').each(function () {
    const container = $(this).find('.compare-product__container');
    const prefer = $(this).attr('data-nudgeoptions')? JSON.parse($(this).attr('data-nudgeoptions')) : {};
    const cfPath = $(this).data('cfpath');
    const applyNowLink = $(this).data('applynowlink');
    // credit card titles
    const forTitle = $(this).data('forlabel') ?? 'Dành cho';
    const offersTitle = $(this).data('offerslabel') ?? 'Quyền lợi chủ thẻ';
    const utilitiesTitle = $(this).data('utilitieslabel') ?? 'Tiện ích';
    const conditionTitle = $(this).data('conditionslabel') ?? 'Điều kiện mở thẻ';
    const ratesTitle = $(this).data('rateslabel') ?? 'Biểu phí thẻ';
    // debit card titles
    const outstandingTitle = $(this).data('outstandinglabel') ?? 'Tiện ích nổi bật';
    const cardOffersTitle = $(this).data('cardofferslabel') ?? 'Ưu đã thẻ';
    const feeTitle = $(this).data('feeslabel') ?? 'Biểu phí';

    const creditCardTitles = { forTitle, offersTitle, utilitiesTitle, conditionTitle, ratesTitle };
    const debitCardTitles = { outstandingTitle, cardOffersTitle, feeTitle };

    const addCardUrl = $(this).data('addcardurl') ?? '';
    const urlParams = new URLSearchParams(window.location.search);
    const cardIds = urlParams.getAll('cardId');
    const finalAddCardUrl = `${addCardUrl + '?'+ cardIds.map(e => `cardId=${e}`).join('&')}`;
    const alertBannerPopup = $(this).find('.popup');
    const alertBannerPopupContent = alertBannerPopup.find('.popup-content');
    const closeBtn = alertBannerPopup.find('.close');

    $(window).resize(function () {
      $('.compare-product__card').css('height', 'auto');
      $('[class*="compare-product__card-info-content-"]').css('height', 'auto');
      setHeightCard();
      setupSlick(container);
    });
    setHeightCard();
    handlePopup();

    getData(cardIds, cfPath).then(data => {
      if (data.length < 1) {
        container.empty();
        container.append(generateEmptyCardHeader(finalAddCardUrl));
        container.append(generateEmptyCardHeader(finalAddCardUrl));
        container.append(generateEmptyCardHeader(finalAddCardUrl));
        setHeightCard();
        setupSlick(container);
        return;
      }
      container.empty();
      let cards = Array.from(data).concat(...[null, null, null]).slice(0,3);
      let isCredit = cards[0].cardType[0].includes('credit-card');
      cards.forEach(e => {
        container.append(generateCard(e, prefer, { creditCard: creditCardTitles, debitCard: debitCardTitles }, finalAddCardUrl, isCredit ? 'credit-card' : 'debit-card'));
      });
      setHeightCard();
      setupSlick(container);
    });

    function setHeightCard() {
      if ($(window).width() < 767) { 
        return;
      }
      let maxHeightMap = {};
      let contentMap = {};
      container.find('[class*="compare-product__card-info-content-"]').each(function() {
        let className = Array.from(this.classList).find(item => item.includes('compare-product__card-info-content-'));
        maxHeightMap[className] = Math.max(maxHeightMap[className] ?? 0, $(this).innerHeight());
        contentMap[className] = contentMap[className] ?? '' + $(this).text();
      });
      for (const [key, value] of Object.entries(maxHeightMap)) {
        if (contentMap[key].trim() === '') {
          container.find(`.${key}`).parents('.compare-product__card-section').remove();
        } else {
          container.find(`.${key}`).css('height', value);
        }
      }
      var maxHeightCard = Math.max.apply(
        null,
        $('.compare-product__card')
          .map(function () {
            return $(this).innerHeight();
          })
          .get()
      );
      $('.compare-product__card').css('height', maxHeightCard);

      container.find('.compare-product__card-no-item a').attr('href', finalAddCardUrl)
    }

    function handlePopup() {
      container.on('click', '.compare-product__bottom-button button',function (e) {
        $(this).parent().find(`.${$(this).attr('ref-class')}`).trigger('click')
        e.preventDefault();
        e.stopPropagation();
        

        if ($(window).width() <= 768) {
          window.open(applyNowLink, '_blank');
        } else {
          alertBannerPopup.addClass('open');
          $('body').addClass('overflow-hidden');
        } 
      });
    
      closeBtn.click(function () {
        alertBannerPopup.removeClass('open');
        $('body').removeClass('overflow-hidden');
      });

      $(document).click(function (event) {
        if (!$(event.target).is(alertBannerPopupContent) && !$(event.target).parents().is(alertBannerPopupContent)) {
          alertBannerPopup.removeClass('open');
          $('body').removeClass('overflow-hidden');
        }
      });
    }
  });

  function setupSlick(element) {
    if ($(window).width() < 767) {
      element.not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: false,
        dots: true,
        infinite: true,
      });
    } else {
      element.filter('.slick-initialized').slick('unslick');
    }
  }

  function getData(cardIds, cfPath) {
    return new Promise(function (resolve, reject) {
      $.ajax({
        url: '/content/_cq_graphql/techcombank/endpoint.json',
        method: 'post',
        data: createQuery(cardIds, cfPath),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
          resolve(response?.data?.credit_debitCardDetailContentFragmentList?.items ?? []);
        },
        error: function (xhr, status, error) {
          reject(error);
        },
      });
    });
  }

  function createQuery(cardIds, cfPath) {
    const query = `query getCreditDebitCardList($cardType: StringArrayFilter, $cardId: IntFilter, $cfPath: ID, $logOp: LogOp) {
      credit_debitCardDetailContentFragmentList(
        filter: {cardType: $cardType, _path: {_expressions: [{value: $cfPath, _operator: STARTS_WITH}]}, _logOp: $logOp, cardId: $cardId}
      ) {
        items {
          cardId
          cardType
          cardNudgeType
          displaySpecialOffer
          cardImage {
            ... on ImageRef {
              _publishUrl
            }
          }
          cardBackgroundColor
          addToCompareLabel
          addedLabelText
          icon {
            ... on ImageRef {
              _publishUrl
            }
            ... on DocumentRef {
              _publishUrl
            }
          }
          iconAtlText
          cardTitle
          notesText
          findoutLabelText
          findoutMoreInternalLink {
            ... on PageRef {
              _publishUrl
            }
          }
          findoutMoreExternalLink
          findoutMoreTarget
          findoutMoreNoFollow
          compareTextDetailsFor {
            html
          }
          outStandingOffers {
            html
          }
          utilities {
            html
          }
          appilicationConditions {
            html
          }
          feeInterest {
            html
          }
          cardOffers {
            html
          }
          learnMoreLabelText
          learnMoreInternalLink {
            ... on PageRef {
              _publishUrl
            }
          }
          learnMoreExternalLink
          learnMoreTarget
          learnMoreNoFollow
          registerNowLabel
          registerNowCTA
          applynowLabelText
        }
      }
    }
    `;
    let body = {
      query
    }
    const variables = {
      logOp: 'AND',
      cfPath: cfPath,
    };
    variables.cardId = {
      _logOp: 'OR',
      _expressions: cardIds.map(e => ({value: Number(e)}))
    }
    body.variables = variables;
    return JSON.stringify(body);
  }

  function generateSection(title, content, suffix) {
    return `
    <div class="compare-product__card-section">
      <div class="compare-product__card-info-title">
        <p>${title}</p>
      </div>
      <div class="compare-product__card-info-content compare-product__card-info-content-${suffix}">
        ${content}
      </div>
    </div>
    `;
  }

  function generateEmptyCardHeader(addCardUrl) {
    return `<div class="compare-product__card-no-item">
          <div class="compare-product__card-no-item-background-img">
            <img
              src="https://d1kndcit1zrj97.cloudfront.net/uploads/grey_plus_6cf05bd965.svg?w=32&q=75"
              srcset="
                https://d1kndcit1zrj97.cloudfront.net/uploads/grey_plus_6cf05bd965.svg?w=32&q=75 1x,
                https://d1kndcit1zrj97.cloudfront.net/uploads/grey_plus_6cf05bd965.svg?w=64&q=75 2x
              " />
          </div>
          <a href="${addCardUrl}"></a>
        </div>`;
  }

  function creditCardSections(cardItem, titles) {
    return `
      ${generateSection(titles.forTitle, cardItem?.compareTextDetailsFor?.html ?? '', 'for')}
      ${generateSection(titles.offersTitle, cardItem?.outStandingOffers?.html ?? '', 'outstading')}
      ${generateSection(titles.utilitiesTitle, cardItem?.utilities?.html ?? '', 'utilities')}
      ${generateSection(titles.conditionTitle, cardItem?.appilicationConditions?.html ?? '', 'conditions')}
      ${generateSection(titles.ratesTitle, cardItem?.feeInterest?.html ?? '', 'fee')}
    `;
  }

  function debitCardSections(cardItem, titles) {
    return `
      ${generateSection(titles.outstandingTitle, cardItem?.utilities?.html ?? '', 'outstading')}
      ${generateSection(titles.cardOffersTitle, cardItem?.cardOffers?.html ?? '', 'offer')}
      ${generateSection(titles.feeTitle, cardItem?.feeInterest?.html ?? '', 'fee')}
    `;
  }

  function generateCard(cardItem, prefer, titles, addCardUrl, cardType) { 
    const preferItem = Array.isArray(prefer) ? prefer?.find((item) => item?.id === cardItem?.cardNudgeType[0]) : null;
    let section = $('.credit-card-comparison-result');
    const componentName = section.data("componentName");
    let applyNowWebInteractionType = "Other";
    let learnMoreWebInteractionType = cardItem?.learnMoreExternalLink? "exit":"Other";
  if($(window).width()<769){
    const hrefVal = section.data('applynowlink');
    if (hrefVal.includes("techcombank.com")) {
      applyNowWebInteractionType = 'Other';
    }else if(hrefVal.includes(".pdf")){
      applyNowWebInteractionType = 'Download';
    } else {
      applyNowWebInteractionType = 'Exit';
    }
  }
    return `
    <div class="compare-product__item">
      <div class="compare-product__card">
        ${cardItem ? 
        `<p class="compare-product__card-label ${preferItem && cardItem.displaySpecialOffer ? '' : 'no-label'}">${preferItem?.title}</p>
        <div class="compare-product__card-image"><img src="${cardItem?.cardImage?._publishUrl}" /></div>
        <p class="compare-product__card-text black-text">${cardItem?.cardTitle ?? ''}</p>`
        : generateEmptyCardHeader(addCardUrl)}
      </div>
      <div class="compare-product__card-info">
        <div class="compare-product__list-card-info">
          ${cardType === 'credit-card' ? creditCardSections(cardItem, titles.creditCard) : debitCardSections(cardItem, titles.debitCard)}
        </div>
      </div>
      ${cardItem ? `
      <div class="compare-product__bottom">
        <div class="compare-product__bottom-button">
        <input type="hidden" class="applyNowAABtn"
        data-tracking-click-event="linkClick"
          data-tracking-click-info-value="{'linkClick':'${cardItem?.applynowLabelText}'}" 
          data-tracking-web-interaction-value="{'webInteractions': {'name': '${componentName}','type': '${applyNowWebInteractionType}'}}"
        />
          <button type="button" ref-class="applyNowAABtn">
            <div class="Button_text__d9Ltb">${cardItem?.applynowLabelText}</div>
          </button>
        </div>
        <div class="compare-product__bottom-link">
          <a href="${cardItem?.learnMoreInternalLink?._publishUrl?.replace('.html','') ?? cardItem?.learnMoreExternalLink ?? '#'}"
          data-tracking-click-event="linkClick"
          data-tracking-click-info-value="{'linkClick':'${cardItem?.learnMoreLabelText}'}" 
          data-tracking-web-interaction-value="{'webInteractions': {'name': '${componentName}','type': '${learnMoreWebInteractionType}'}}"
          >${cardItem?.learnMoreLabelText}</a>
        </div>
      </div>
      ` : ''}
    </div>
    `;
  }
});
//End Component Compare Product
