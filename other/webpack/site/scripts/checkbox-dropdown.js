// Init dropdowns
$(document).ready(function () {

    const itemDropdown = (data) => `
    <li class="dropdown-item" role="button" data-id="${data.branchId || data.cityCode || data.districtCode}" data-value="${data.fullBranchName || data.cityName || data.districtName}">
      <span>
        <p>${data.fullBranchName || data.cityName || data.districtName}</p>
      </span>
    </li>
    `;

    let selectedItem1 = "";
    let selectedItem2 = "";
    let selectedItem3 = "";

    const selectItem1 = $("form .city-list");
    const selectItem2 = $("form .district-list");
    const selectItem3 = $("form .branch-list");

    const handleSelectItem1 = (event) => {
        selectedItem1 = event.detail;
        if (selectedItem1) {
            selectItem2.removeClass("disabled");
        } else {
            selectItem2.addClass("disabled");
        }

        selectItem3.addClass("disabled");
    };

    const handleSelectItem2 = (event) => {
        selectedItem2 = event.detail;
        if (selectedItem2) {
            selectItem3.removeClass("disabled");
        } else {
            selectItem3.addClass("disabled");
        }
    };

    const handleSelectItem3 = (event) => {
        selectedItem3 = event.detail;
    };

    selectItem1.on("select", handleSelectItem1);
    selectItem2.on("select", handleSelectItem2);
    selectItem3.on("select", handleSelectItem3);

    let uniqueItemsCity, uniqueItemsDistrict, currentCity, currentDistrict,branchDropdown;
    const cityLists = document.querySelectorAll(".dropdown-wrapper.city-list");
    if (cityLists.length > 0) {
        cityLists.forEach(cityList => {
            const cityListDropdown =  new TcbDropdown(cityList);
            $.ajax({
                url: '/graphql/execute.json/techcombank/cityList', type: "GET", dataType: "json", success: function (response) {
                    uniqueItemsCity = response.data.branchFragmentList.items.reduce((acc, item) => {
                        if (!acc.some(i => i.cityCode === item.cityCode)) {
                            acc.push(item);
                        }
                        return acc;
                    }, []);

                    cityListDropdown.setData(uniqueItemsCity.map(item => {
                        return {text: item.cityName, value: item.cityCode}
                    }));
                }, error: function (error) {
                    console.log(error);
                }
            });
        })

    }
    const districtLists = document.querySelectorAll(".dropdown-wrapper.district-list");
    if (districtLists.length > 0) {
        districtLists.forEach(districtList => {
            const districtDropdown = new TcbDropdown(districtList);
            $('form .city-list input').on('input', function (e) {
                currentCity = uniqueItemsCity.find(item => item.cityName === e.target.value);
                $.ajax({
                    url: `/graphql/execute.json/techcombank/districtList;cityCode=${currentCity.cityCode};`,
                    type: "GET",
                    dataType: "json",
                    success: function (response) {
                        uniqueItemsDistrict = response.data.branchFragmentList.items.reduce((acc, item) => {
                            if (!acc.some(i => i.districtCode === item.districtCode)) {
                                acc.push(item);
                            }
                            return acc;
                        }, []);
                        districtDropdown.clearValue();
                        districtDropdown.setData(uniqueItemsDistrict.map(item => {
                            return {value: item.districtCode, text: item.districtName};
                        }));
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
                branchDropdown ? branchDropdown.clearValue() : '';
            });
        })
    }
    const branchLists = document.querySelectorAll(".dropdown-wrapper.branch-list");
    if (branchLists.length > 0) {
        branchLists.forEach(branchList => {
            branchDropdown = new TcbDropdown(branchList);
            $('form .district-list input').on('input', function (e) {
                currentDistrict = uniqueItemsDistrict.find(item => item.districtName === e.target.value);
                $.ajax({
                    url: `/graphql/execute.json/techcombank/branchList;cityCode=${currentCity.cityCode};districtCode=${currentDistrict.districtCode};`,
                    type: "GET",
                    dataType: "json",
                    success: function (response) {
                        const uniqueItems = response.data.branchFragmentList.items.reduce((acc, item) => {
                            if (!acc.some(i => i.branchCode === item.branchCode)) {
                                acc.push(item);
                            }
                            return acc;
                        }, []);
                        branchDropdown.clearValue();
                        branchDropdown.setData(uniqueItems.map(item => {
                            return {value: item.branchCode, text: item.fullBranchName}
                        }));
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        })
    }

    document.querySelectorAll(".dropdown-wrapper:not(.city-list):not(.district-list):not(.branch-list)").forEach((item) => {
        new TcbDropdown(item);
    });
});
