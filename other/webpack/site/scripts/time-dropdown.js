$.fn.changeOptions = function (items, currentTime) {
  this.each(function () {
    if (!$(this).hasClass('time-select')) {
      throw new Error('This element does not have "time-select" class');
    }
    // let selectedDateElement = $(this).find('.selected-time-slot');
    let timeDropdown = $(this).find('.time__dropdown');
    let timeInputSuffix = $(this).find('.selected-time-slot');

    let dropdownElement = $(this).find('.time-slot-list');
    dropdownElement.empty();

    timeDropdown.removeClass('disable');
    if (items.length === 0) {
      timeInputSuffix.text('--:--');
      timeDropdown.addClass('disable');
    }

    items.forEach((item, index) => {
      let isSelected = currentTime === null ? index === 0 : item === currentTime;
      if (isSelected) {
        currentTime = item;
        timeInputSuffix.text(item);
      }
      dropdownElement.append(
        `
            <li class='${
              isSelected ? 'selected' : ''
            }' tabindex='0' role='option' aria-disabled='false' aria-selected='true' data-value='${item}'>
              <span>${item}</span>
            </li>
            `
      );
      $(this)
        .find('li')
        .click(function () {
          let itemValue = $(this).attr('data-value');
          if (currentTime === itemValue) {
            timeDropdown.removeClass('active');
            return;
          }
          timeInputSuffix.text(itemValue);
          currentTime = itemValue;
          $(this).trigger('timeChanged', itemValue);
          timeDropdown.removeClass('active');
        });
    });
  });
};

$.fn.right = function() {
  return $(this).offset().left + $(this).outerWidth();
}

$(document).ready(function () {
  $('.time-select').each(function () {
    let base = $(this);
    let title = $(this).attr('title') ?? 'Thời gian cập nhật';
    let iconMobilePath = $(this).attr('icon-mobile-path') // ?? 'https://d1kndcit1zrj97.cloudfront.net/uploads/line_f78efee1ea.svg?w=16&amp;q=75';
    let iconWebPath = $(this).attr('icon-web-path') // ?? 'https://d1kndcit1zrj97.cloudfront.net/uploads/line_f78efee1ea.svg?w=16&amp;q=75'; 
    let timeIconAlt = $(this).attr('icon-alt');
    $(this).empty();
    $(this).append(
      `
      <div class="header-select__data-input">
        <div class="data-input__prefix">
          <div class="data-input__calendar-icon">
            <picture>
              <source media="(max-width: 360px)"
                      srcset="${iconMobilePath}"/>
              <source media="(max-width: 576px)"
                      srcset="${iconMobilePath}"/>
              <source media="(min-width: 1080px)"
                      srcset="${iconWebPath}"/>
              <source media="(min-width: 1200px)"
                      srcset="${iconWebPath}"/>
              <img alt="${timeIconAlt}"
                  data-nimg="fixed" decoding="async"
                  src="${iconWebPath}">
            </picture>
          </div>
          <p>${title}</p>
        </div>
        <div class="data-input__suffix">
          <p class="selected-time-slot">--:--</p>
          <span class="data-input__arrow-icon">
            <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/viewmore-icon.svg"/>
          </span>
        </div>
      </div>
      <div class="time__dropdown" tabindex="-1">
        <ul class="time-slot-list" role="listbox" tabindex="-1">
        </ul>
      </div>
      `
    );
    let timeDataInput = $(this).find('.header-select__data-input');
    timeDataInput.each(function () {
      $(this).click(function () {
        let dropdown = $(this).next('.time__dropdown');
        if (dropdown.hasClass('disable')) {
          dropdown.removeClass('active');
          return;
        }
        $('body').toggleClass('time-showing');
        timeDataInput.toggleClass('no-pointer');
        updateDropdown(dropdown);
        dropdown.toggleClass('active');
        $(this).toggleClass('no-pointer');
      });
    });

    function updateDropdown(dropdown) {
      let parentOffset = base.offset();
      let windowHeight = $(window).height();
      let maxHeight = Math.min(windowHeight - 96, 11 * 48 + 16);
      let height = Math.min(maxHeight, dropdown.find('li').length * 48 + 16);
      let top = -5;
      if (parentOffset.top + height > windowHeight) {
        top = -5 + (windowHeight - (parentOffset.top + height));
      } else if (parentOffset.top < 0) {
        top = -5 - parentOffset.top + 48;
      }
      dropdown.css({ 'top': top, 'max-height': maxHeight});
    }

  });

  $('body').on('click', function (event) {
    if (
      $(event.target).parents('.time__dropdown').length === 0 &&
      $(event.target).parents('.time-select').length === 0
    ) {
      $('.time__dropdown.active').removeClass('active');
    }
  });
});
