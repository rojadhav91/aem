
$(document).ready(function () {
  var fastAccessContents = $('.quick-access');

  fastAccessContents.each(function () {
    var fastAccessContent = $(this);
    var fastAccessTable = fastAccessContent.find('.quick-access__table-desktop');
    var fastAccessTableHeaderItems = fastAccessTable.find('.table-header__items');
    var headerButton = fastAccessTableHeaderItems.find('.header__button');
    var tableRecordsInner = fastAccessTable.find('.table-records__inner');
    var headerItemMobile = $(this).find('.header-item');
    var currencyPopup = $(this).find('.pop-up');
    var currencyPopupContent = currencyPopup.find('.pop-up-content');
    var currencyPopUpTitle = currencyPopup.find('.title h6');

    headerButton.each(function (i) {
      var itemTemp = $(this);
      if (i == 0) {
        $(this).addClass('header__selected');
        tableRecordsInner.each(function (j) {
          if (j == i) {
            $(this).addClass('record__show');
          }
        });
      }
      itemTemp.click(function () {
        headerButton.removeClass('header__selected');
        $(this).addClass('header__selected');
        headerButton.scrollCenter('.header__selected', 300);
        var headerTileWidth = headerButton.parent().width();
        tableRecordsInner.each(function (j) {
          $(this).removeClass('record__show');
          if (j == i) {
            $(this).addClass('record__show');
          }

          headerButton.css('border-bottom', 'unset');
          $(itemTemp).css('border-bottom', '4px solid #ed1c24');
        });
      });
    });

    if ($(window).width() <= 767) {
      headerItemMobile.click(function (e) {
        e.stopPropagation();
        showCurExPopUp($(this));
        $('body').addClass('overflow-hidden');
      });
    }

    $(window).resize(function () {
      if ($(window).width() <= 767) {
        headerItemMobile.click(function (e) {
          e.stopPropagation();
          showCurExPopUp($(this));
          $('body').addClass('overflow-hidden');
        });
      }
    });

    function showCurExPopUp(element) {
      // remove old content
      var oldContent = currencyPopupContent.find('.table-records__inner');
      oldContent.remove();

      // add new content
      currencyPopup.css('display', 'block');
      currencyPopUpTitle.text(element.find('span').text());
      $(tableRecordsInner[element.index()]).clone().appendTo(currencyPopupContent);
    }

    $(document).click(function (event) {
      if (!$(event.target).is(currencyPopupContent) && !$(event.target).parents().is(currencyPopupContent)) {
        currencyPopup.css('display', 'none');
        $('body').removeClass('overflow-hidden');
      }
    });

    jQuery.fn.scrollCenter = function (elem, speed) {
      var active = $(this).parent().find(elem); // find the active element
      var activeWidth = active.width() / 2; // get active width center

      var pos = active.position().left + activeWidth; //get left position of active li + center position
      var elpos = active.scrollLeft(); // get current scroll position
      var elW = active.width(); //get div width

      pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

      $(this).parent().animate({ scrollLeft: pos }, speed);

      return pos;
    };
  });
});