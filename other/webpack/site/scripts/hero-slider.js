const ANIMATION_TIME = 5000;
const INACTIVE_SLIDE = 'hidden';
const ACTIVATED_SLIDE = 'hero-slider_dot--active';

const heroSliders = [];

window.HeroSlider = class HeroSlider {
  wrapper;
  current;
  slides = [];
  indicators = [];
  timer;
  slideIndex;

  constructor(slider) {
    this.wrapper = slider;
    this.init();
    if (this.slides.length) {
      this.play();
    }
  }

  addIndicators() {
    const indicatorWrapper = $(this.wrapper).find('.hero-slider_indicator');
    this.slides.each((index) => {
      indicatorWrapper.append('<div class="hero-slider_dot"></div>');
    });
    this.indicators = indicatorWrapper.find('.hero-slider_dot');
  }

  init() {
    if (!this.wrapper) {
      return;
    }
    const wrapperElement = $(this.wrapper);
    this.slideIndex = wrapperElement.find('.hero-slider_current-slide');
    this.slides = wrapperElement.find('.hero-slider_slide');
    this.addIndicators();
    this.current = this.slides.length - 1;
    this.updateCurrentIndex();
  }

  play() {
    this.next();
  }

  next() {
    this.slides[this.current].classList.add(INACTIVE_SLIDE);
    this.indicators[this.current].classList.remove(ACTIVATED_SLIDE);
    this.current = (this.current + 1) % this.slides.length;
    this.slides[this.current].classList.remove(INACTIVE_SLIDE);
    this.indicators[this.current].classList.add(ACTIVATED_SLIDE);
    this.updateCurrentIndex();
    this.timer = setTimeout(() => {
      this.next();
    }, ANIMATION_TIME);
  }

  updateCurrentIndex() {
    const index = this.current + 1;
    this.slideIndex.text(index < 10 ? '0' + index : index);
  }
};

const sliderElements = $('.hero-slider');
sliderElements.each((index) => {
  heroSliders.push(new HeroSlider(sliderElements[index]));
});
