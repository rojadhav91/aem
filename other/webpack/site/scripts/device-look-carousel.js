$(document).ready(function () {
  $('.device-look-carousel').each(function () {
    let $deviceLoopStep = $(this);
    let slidesToShowNum = $(this).attr('slides-to-show-num');
    if (!isNaN(slidesToShowNum)) slidesToShowNum = parseInt(slidesToShowNum);
    if ($(window).width() >= 992) {
      configImageSlider(slidesToShowNum);
    }
    resizeStepContentHeight($deviceLoopStep);
    $(window).resize(function () {
      if ($(window).width() >= 992) {
        configImageSlider(slidesToShowNum);
      }
      resizeStepContentHeight($deviceLoopStep);
    });

    let images = $deviceLoopStep.find('.device-look-carousel__item-image-wrapper img');
    resizeStepSameHeight(images);
    $(window).resize(function () {
      resizeStepSameHeight(images);
    });

    function configImageSlider(slidesToShowNum) {

      //Adobe: return if element not found
      if ($('.device-look-carousel__list-item').length === 0) return;
      $deviceLoopStep.find('.device-look-carousel__list-item')
        .not('.slick-initialized')
        .slick({
          slidesToShow: slidesToShowNum || 4,
          slidesToScroll: 1,
          autoplay: false,
          accessibility: true,
          arrows: true,
          infinite: false,
          responsive: [
            {
              breakpoint: 992,
              settings: 'unslick',
            },
          ],
        });
      $deviceLoopStep.find('.device-look-carousel__list-item .slick-next').text('');
      $deviceLoopStep.find('.device-look-carousel__list-item .slick-prev').text('');
      $deviceLoopStep.find('.device-look-carousel__list-item .slick-prev').css('cssText', 'display: none !important');
      $deviceLoopStep.find('.device-look-carousel__list-item .slick-next').css('cssText', 'display: block !important');
      $deviceLoopStep.find('.device-look-carousel__list-item').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        if ($(window).width() > 991) {
          if (currentSlide === 0) {
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-prev').css('cssText', 'display: none !important');
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-next').css('cssText', 'display: block !important');
          } else if (slick.$slides.last().hasClass('slick-active')) {
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-prev').css('cssText', 'display: block !important');
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-next').css('cssText', 'display: none !important');
          } else {
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-prev').css('cssText', 'display: block !important');
            $deviceLoopStep.find('.device-look-carousel__list-item .slick-next').css('cssText', 'display: block !important');
          }
        }
      });
    }

    function resizeStepContentHeight($deviceLookStep) {
      let $stepContentItems = $deviceLookStep.find('.device-look-carousel__item');
      let maxHeight = 0;

      // 1. Find max height of step contents
      $stepContentItems.each(function () {
        let $stepContentItem = $(this);
        if (maxHeight < $stepContentItem.height()) {
          maxHeight = $stepContentItem.height();
        }
      });

      // 2. Assign height of all step contents
      $stepContentItems.each(function () {
        let $stepContentItem = $(this);
        if (maxHeight > $stepContentItem.height()) {
          $stepContentItem.height(maxHeight);
        }
      });
    }

    // Align image, step, text of all items equally
    function resizeStepSameHeight($images) {
      // Find the height of the tallest image
      let maxHeight = 0;
      $images.each(function () {
        let $image = $(this);
        let imageHeight = $image.height();
        if (imageHeight >= maxHeight) {
          maxHeight = imageHeight;
        }
      });
      $images.each(function () {
        let $image = $(this);
        $image.css('padding-bottom', maxHeight - $image.height());
      });
    }
  });

  $('.device-look-carousel').each(function () {
    let itembody = $(this).find('.device-look-carousel__item .device-look-carousel__item-body');

    function findMaxHeight(el) {
      let max = 0;
      el.each(function () {
        let elHeight = $(this).innerHeight();
        if (elHeight > max) {
          max = elHeight;
        }
      });
      return max;
    }

    if ($(window).width() > 991) {
      itembody.each(function () {
        $(this).css('min-height', findMaxHeight(itembody));
      });
    }

    $(window).resize(function () {
      if ($(window).width() > 991) {
        itembody.each(function () {
          $(this).css('min-height', findMaxHeight(itembody));
        });
      } else {
        itembody.each(function () {
          $(this).css('min-height', 'unset');
        });
      }
    });
  });
});
