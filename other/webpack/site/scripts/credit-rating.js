$(document).ready(function () {
    let creditRanking = $('.credit-ranking-component');

    creditRanking.each(function () {
        let showedOptions = $(this).find('.option');
        let creditList = $(this).find('.credits-list');
        let creditReportList = $(this).find('.credit');
        let menuItemList = $(this).find('.menu-item');
        let tabList = $(this).find('.tab');

        creditList.not(creditList.first()).css('display', 'none'); // hidden other list credits/other years
        sortFilterOptions($(this).find('.filter-options'));
        setNumberOfColumns(creditReportList);
        layOutSetUp(creditReportList);

        // select an option in the filter to filter reports
        showedOptions.click(function () {
            //filter tabs by the year user selected
            if ($(this).attr('value') == 'select-all') {
                menuItemList.each(function () {
                    $(this).removeClass('filtered');
                })
                tabList.each(function () {
                    $(this).removeClass('filtered');
                })
            } else {
                let filterYear = $(this).attr('value');
                menuItemList.each(function () {
                    if ($(this).attr('tabyear') === filterYear) {
                        $(this).removeClass('filtered');
                    } else {
                        $(this).addClass('filtered');
                    }
                })
                tabList.each(function () {
                    if ($(this).attr('tabyear') === filterYear) {
                        $(this).removeClass('filtered');
                    } else {
                        $(this).addClass('filtered');
                    }
                })
            }
            creditReportList.each(function () {
                // hide report panels with all tabs filtered
                if ($(this).find('.menu-item:not(.filtered)').length == 0) {
                    $(this).addClass('filtered');
                } else {
                    $(this).removeClass('filtered');
                    //after filtering, auto select 1st tab not filtered
                    let menuItems = $(this).find('.menu-item');
                    let menuItemSelected = $(this).find('.menu-item:not(.filtered):first');
                    let tabItems = $(this).find('.tab');
                    if (menuItemSelected.length > 0) {
                        selectTab(menuItems, menuItemSelected, tabItems, creditReportList);
                    }
                }
            })
            // re-set layout (2 columns/3 columns) after some report has been filtered out
            let filteredCreditReportList = creditReportList.not('.filtered');
            setNumberOfColumns(filteredCreditReportList);
            // re-set height when change option
            layOutSetUp(filteredCreditReportList);
        });

        creditList.each(function () {
            let credits = $(this).find('.credit');

            credits.each(function () {
                let dateMenu = $(this).find('.menu-item');
                let tabItem = $(this).find('.tab');

                tabItem.not(tabItem.first()).addClass('display-none');

                // click tabs
                dateMenu.click(function () {
                    selectTab(dateMenu, $(this), tabItem, credits);
                });

                // Add year attribute to menu and tab items
                dateMenu.each(function () {
                    //process date time and add year attribute to item
                    let tabLabel = $(this).attr('tabLabel');
                    if (tabLabel !== null) {
                        let tabYear = tabLabel.split('/')[2];
                        $(this).attr('tabYear', tabYear);
                    }
                    let lang = $('.credit-ranking-component').attr('lang');
                    let localizeDateString = getLocalizeDateString(tabLabel, lang)
                    $(this).text(localizeDateString);
                    setDataTrackingValue($(this), localizeDateString);
                });
                tabItem.each(function () {
                    //process date time and add year attribute to item
                    let tabLabel = $(this).attr('tabLabel');
                    if (tabLabel !== null) {
                        let tabYear = tabLabel.split('/')[2];
                        $(this).attr('tabYear', tabYear);
                    }
                });
            });
        });

        $(window).resize(function () {
            if ($(window).width() >= 992) {
                layOutSetUp(creditList.find('.credit'));
            } else {
                creditList.find('.tab .content').css('height', 'unset');
            }
        });

        function layOutSetUp(credits) {
            if ($(window).width() >= 992) {
                 // setContentHeight(credits);
            }
        }

        // find max height/per row of list credits
        function setContentHeight(credits) {
            credits.find('.tab .content').css('height', 'unset');
            let creditNum = credits.length;
            let rowNum = Math.ceil(creditNum / 3);
            let temp = 0;

            for (let i = 0; i < rowNum; i++) {
                // find max height in per row
                let maxHeight = 0;
                for (j = temp; j <= temp + 2; j++) {
                    let elHeight = $(credits[j]).find('.tab:not(.display-none) .content').innerHeight();
                    if (elHeight > maxHeight) {
                        maxHeight = elHeight;
                    }
                }

                // set height for element in a row
                for (j = temp; j <= temp + 2; j++) {
                    $(credits[j]).find('.tab:not(.display-none) .content').css('height', maxHeight);
                }

                temp = temp + 3;
            }
        }

        //highlight an item when user click
        function selectTab(dateMenu, dateMenuSelected, tabItem, credits) {
            dateMenu.css('color', '#a2a2a2');
            dateMenu.css('font-weight', 'unset');
            dateMenu.css('border-bottom', 'unset');

            dateMenuSelected.css('border-bottom', '4px solid rgb(237, 27, 36)');
            dateMenuSelected.css('color', '#313131');
            dateMenuSelected.css('font-weight', '700');

            tabItem.addClass('display-none');
            // Because there is a previous arrow, must subtract 1 to tab index
            $(tabItem[dateMenuSelected.index()]).removeClass('display-none');

            layOutSetUp(credits);
        }

        //localize date string
        function getLocalizeDateString(dateString, lang) {
            let monthNames = (lang == 'Vietnamese' ? ['tháng 1', 'tháng 2', 'tháng 3', 'tháng 4', 'tháng 5', 'tháng 6',
                'tháng 7', 'tháng 8', 'tháng 9', 'tháng 10', 'tháng 11', 'tháng 12'
            ] : ['January', 'February', 'March', 'April', 'May', 'June',
                'July', 'August', 'September', 'October', 'November', 'December'
            ]);
            let day = dateString.split('/')[0];
            let month = dateString.split('/')[1];
            let year = dateString.split('/')[2];
            return day + ' ' + monthNames[parseInt(month) - 1] + ' ' + year;
        }

        //sort options in filter by year
        function sortFilterOptions($wrapper) {
            $wrapper.find('.option[value!="select-all"]').sort(function (a, b) {
                return +b.value - +a.value;
            })
                .appendTo($wrapper);
        }

        //switching between 2 columns and 3 columns layouts
        function setNumberOfColumns(creditReportList) {
            if(creditReportList.length > 2) {
                creditReportList.css('flex', '0 0 33%');
                creditReportList.css('max-width', '33%');
            }
            else {
                creditReportList.css('flex', '0 0 50%');
                creditReportList.css('max-width', '50%');
            }
        }

        //scrolling menu
        creditReportList.each(function () {
            let mouseDown = false;
            let startX, scrollLeft;
            let slider = $(this).find('.menu')[0];

            let startDragging = (e) => {
                mouseDown = true;
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            }

            let stopDragging = (e) => {
                mouseDown = false;
            }

            let move = (e) => {
                e.preventDefault();
                if (!mouseDown) { return; }
                let x = e.pageX - slider.offsetLeft;
                let scroll = x - startX;
                slider.scrollLeft = scrollLeft - scroll;
            }

            // Add the event listeners
            slider.addEventListener('mousemove', move, false);
            slider.addEventListener('mousedown', startDragging, false);
            slider.addEventListener('mouseup', stopDragging, false);
            slider.addEventListener('mouseleave', stopDragging, false);
        })

        //AA intergration
        function setDataTrackingValue(cta, ctaName) {
            cta.attr("data-tracking-click-event", "linkClick");
            cta.attr("data-tracking-click-info-value", "{'linkClick' : '" + ctaName + "'}");
            cta.attr(
                "data-tracking-web-interaction-value", "{'webInteractions': {'name': 'Credit Rating','type': 'other'}}"
            );
        }
    });
});