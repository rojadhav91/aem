import moment from 'moment';
require('jquery-ui/ui/widgets/datepicker');

function handleQuery(offset, limit, showMe, tags, selectedDate, cfPath) {
  let body = {
    query: "query getEventDetails($eventTag: StringArrayFilter, $eventDate: Date, $dateOptr: DateOperator, $logOp: LogOp, $limit: Int, $offset: Int) {\n  eventDetailContentFragmentList(\n    filter: {eventTag: $eventTag, _logOp: $logOp, eventDate: {_expressions: [{value: $eventDate, _operator: $dateOptr}]}}\n    offset: $offset\n    limit: $limit\n    sort: \"eventDate DESC, eventTime DESC\"\n  ) {\n    items {\n      _path\n      imageForEvent {\n        ... on ImageRef {\n          _publishUrl\n          _path\n          _authorUrl\n          width\n          height\n          mimeType\n        }\n      }\n      eventDate\n      eventTime\n      eventTag\n      eventHeading\n      eventSubHeading\n      eventDesription {\n        html\n      }\n      ctaButtonLabel\n      ctaTarget\n      ctaButtonLink {\n        ... on PageRef {\n          _path\n          _authorUrl\n          _publishUrl\n        }\n      }\n      ctaExternalLink\n   ctaNoFollow\n      awardLabelText\n    }\n  }\n}\n",
  }
  let variables = {
    offset: offset,
    limit: limit
  };
  if (showMe.length == 0) {
    
  } else if (showMe.length == 2) {
    if (selectedDate) {
      variables.dateOptr = 'AT';
    } else {
      variables.dateOptr = 'NOT_AT';
    }
  } else if (showMe[0] === 'upcoming') {
    if (selectedDate) {
      variables.dateOptr = 'AT_OR_AFTER';
    } else {
      variables.dateOptr = 'AFTER';
    }
  } else if (showMe[0] === 'before') {
    if (selectedDate) {
      variables.dateOptr = 'AT_OR_BEFORE';
    } else {
      variables.dateOptr = 'BEFORE';
    }
  }
  if (selectedDate) {
    variables.eventDate = selectedDate;
  } else if (showMe.length == 1) {
    variables.eventDate = moment().format('YYYY-MM-DD');
  }
  variables.logOp = 'AND';
  if (tags.length > 0) {
    variables.eventTag = {
      _logOp: 'OR',
      _expressions: tags.map(e => ({value: e}))
    }
  }
  variables.cfPath = cfPath;
  body.variables = variables;
  return JSON.stringify(body);
}

$(function() {
  $('.calendar-event .event-filter').each(function() {
    let filterElement = $(this);
    let btnOpenFilter = filterElement.find('.btn-open-filter,.btn-open-filter-sticky');
    let btnCloseFilter = filterElement.find('.news_filter__close-button');
    let eventListElement = filterElement.find('.news-tab-content_container');
    let loadMoreButton = filterElement.find('.load-more__button');
    let showMeElement = filterElement.find('.offer-filter__checkbox input[type="checkbox"]');
    let calendar = filterElement.find('.tcb-date-picker');
    let eventFilter = $('.calendar-event').find('.card-multiple-select');
    let eventTypes = [];
    let eventShowMe = ['upcoming', 'before'];
    let events = [];
    let offset = 0;
    let limit = 6;
    let cfPath = filterElement.parents('.calendar-event').attr('data-cfPath');
    var selectedDate = null;

    // let eventDates = calendarEvents.map(e => moment(new Date(e.startDate)));
    let listEventTypes = filterElement.find('.card-multiple-select').data('options');
    let eventDates = [];
    try {
      listEventTypes = eval(listEventTypes);
      let tmp = listEventTypes;
      listEventTypes = {};
      tmp.forEach(e => {
        listEventTypes[e.id] = e.title;
      });
    } catch {
      listEventTypes = {};
    }

    eventFilter.on('onChanged', function(event, newValues) {
      eventTypes = newValues;
      eventFilter.cardMultipleSelectOnChanged(eventTypes);
      offset = 0;
      getData();
    });

    showMeElement.on('change', function(){
      eventShowMe = showMeElement.filter(':checked').map(function() { return $(this).attr('value'); }).get();
      offset = 0;
      getData();
    });

    calendar.on('ready', function() {
      calendar.datepicker('option', {
        dateFormat: 'yy-mm-dd',
        defaultDate: "0",
        firstDay: 1,
        todayHighlight: false,
        showOtherMonths: false,
        beforeShowDay: renderCalendarCallback,
        onSelect: function(_date) {
          let date = _date;
          if (date === selectedDate) {
            selectedDate = null;
          } else {
            selectedDate = date;
            // analytics : call date start
              selecteCalendar = $(this).html();
              var $dateContent = $(selecteCalendar);
              var $finalDate = $dateContent.find(".ui-state-hover").text();
              selecteCalendar = $finalDate;
              captureFilters();   
              $(".mobile-not-show .card-multiple-select__items").trigger('click'); // to activate dataLayerObject         
            // analytics : call date end
          }
          offset = 0;
          getData();
        }
      });
    });

    btnOpenFilter.on("click",function () {
      filterElement.addClass('open');
      $('body').addClass('modal-showing');
    });

    btnCloseFilter.on("click",function () {
      handleRemoveModal();
    });
    $(window).on("click", function (e) {
      if($(e.target).hasClass('tcb-button') || $(e.target).hasClass('news_filter__close-button')) {
        handleRemoveModal();
      }
    });

    loadMoreButton.on('click', function() {
      getData();
    });

    getData();

    function handleRemoveModal() {
      filterElement.removeClass('open');
      $('body').removeClass('modal-showing');
    }

    function renderCalendarCallback(_date) {
      let date = moment(_date);
      let classes = [];
      if (eventDates.find(e => e.isSame(date, 'date'))) {
        classes.push('isEvent');
      }
      if (date.isBefore(new Date(), 'date')) {
        classes.push('isPast');
      }
      if (date.isSame(selectedDate, 'date')) {
        classes.push('isSelected');
      }
      return [true, classes.join(' ')]
    }

    function getData() {
      
      $.ajax({
        url: '/content/_cq_graphql/techcombank/endpoint.json',
        method: 'post',
        data: handleQuery(offset, limit, eventShowMe, Array.from(eventTypes), selectedDate, cfPath),
        contentType: "application/json; charset=utf-8",
        success: function(_data) {
          let data = _data?.data?.eventDetailContentFragmentList?.items ?? [];
          handleData(data);
        }
      });

      function handleData(data) {
        if (offset == 0) {
          eventListElement.empty();
          events = [];
        }
        events.push(...(data ?? []));
        eventDates = events.map(e => moment(e.eventDate));
        calendar.datepicker('refresh');

        data?.forEach((e) => {
          let description = e.eventDesription?.html;
          let eventType = listEventTypes?.[e.eventTag?.[0] ?? ''];
          let title = e.eventHeading;
          let startDate = moment(new Date(e.eventDate));
          let startTime = moment(e.eventTime, [moment.ISO_8601, 'HH:mm:ss']);

          let dateStr = startDate.format('DD MMM YYYY');
          
          let thumbnail = e.imageForEvent?._path ?? '';
          let mobileThumnail = e.imageForEvent?._path ?? '';

          let actionText = e.ctaButtonLabel;
          let actionUrl = e.ctaButtonLink?._publishUrl;
          //changed by adobe : start
          let webInteractionType = "";
          if (actionUrl) {
            actionUrl = actionUrl.replace(".html", "");
            webInteractionType = 'other';
          } else if(e.ctaExternalLink){
            actionUrl = e.ctaExternalLink;
            webInteractionType = 'exit';
          }
          //changed by adobe : start
          let rel = [];
          let target = "";
          if (e.ctaNoFollow) {
            rel.push('nofollow');
          }
          if (e.ctaTarget) {
            target = 'target="_blank"';
            rel.push('noopener', 'noreferrer');
          }

          eventListElement.append(`
          <div class="news-filter-card" 
               data-tracking-click-event="linkClick"
               data-tracking-click-info-value="{'linkClick' : '${title}'}"
               data-tracking-web-interaction-value="{'webInteractions': {'name': 'Calendar Events','type': '${webInteractionType}'}}">
          <picture class="news-filter-card_cover">
            <source
              media="(max-width: 1024px)"
              srcset="${mobileThumnail}" />
            <source
              media="(min-width: 1025px)"
              srcset="
                ${thumbnail}
              " />
            <img
              class="news-filter-card_cover-image"
              src="${thumbnail}"
              alt="Logo" />
          </picture>
          
          <div class="news-card_info">
            <div class="news-card_type">
              <span class="news-card_type-inner">${eventType}</span>
              <span class="news-card_time">${dateStr} ${startTime.isValid() ? startTime.format('hh:mm a') : ""}</span>
            </div>
            <div class="news-card_title">${title}</div>
            <div class="news-card_description">
              ${description}
            </div>

            <div class="news-card_actions ${actionUrl ? "" : "hidden"}">
              <a class="tcb-button tcb-button--link" href="${actionUrl}" rel="${rel.join(' ')}" ${target}>
                ${actionText}
                <img src="/etc.clientlibs/techcombank/clientlibs/clientlib-site/resources/images/red-arrow-icon.svg"/>
              </a>
            </div>
          </div>
        </div>
          `);
        });
        offset += limit;
        if (data.length < limit) {
          loadMoreButton.css('display', 'none');
        } else {
          loadMoreButton.css('display', '');
        }
      }
    }
  });
});

//analytics - start 
$(document).ready(function () {
  setTimeout(function () {
     $(".card-multiple-select__items .filter__item button").click(function () {
        captureFilters();
     });
     $(".news_filter-group__content .offer-filter__sort-by-wrap .checkbox-item__wrapper, .card-multiple-select__items .filter__item button").click(function () {
        captureFilters();
        $(".mobile-not-show .card-multiple-select__items").trigger('click');
     });
  }, 100);
});

//analytics - function to capture filter realtime values
var sortedEventTypes;
var sortedShowMe;
var selecteCalendar;
var selectefulldate;
var updatedFilters = "";

// capturing Event Type
function captureEventTypes() {
  var updatedFilter = [];
  if ($(window).width() > 767) {
     $('.mobile-not-show .card-multiple-select__items .card-multiple-select__button.filter-selected').each(function () {
        updatedFilter.push($(this).text());
     });
     sortedEventTypes = updatedFilter && updatedFilter.length > 0 ? updatedFilter.join(" | ") : '';
  } else {
     $('.mobile-only .card-multiple-select__items .card-multiple-select__button.filter-selected').each(function () {
        updatedFilter.push($(this).text());
     });
     sortedEventTypes = updatedFilter && updatedFilter.length > 0 ? updatedFilter.join(" | ") : '';
  }
}

// capturing Show Me
function captureShowMe() {
  var updatedType = [];
  $('.offer-filter__sort-by-wrap .checkbox-item__wrapper input[type="checkbox"]:checked').each(function () {
     updatedType.push($(this).next("div").text());
  });
  sortedShowMe = updatedType && updatedType.length > 0 ? updatedType.join(" | ") : "";
}

// capturing Date
function captureDate() {
  var updatedDate = [];
  var monthYear = $(".ui-datepicker-header .ui-datepicker-title").text();
  var selectectDate = selecteCalendar + ' ' + monthYear;
  selectefulldate = selecteCalendar && selecteCalendar.length > 0 ? selectectDate : "";
}

// function -  start to update data attributes as per captured filters for view more button
function captureFilters() {
  captureEventTypes();
  captureShowMe();
  captureDate();

  var eventTypeClick = sortedEventTypes;
  var showMeClick = sortedShowMe;
  var updatedFilters = "";

  if (eventTypeClick && eventTypeClick.trim().length > 0) {
     if (updatedFilters.length > 0) {
        updatedFilters += " | " + eventTypeClick.trim();
     } else {
        updatedFilters += eventTypeClick.trim();
     }
  }

  if (showMeClick && showMeClick.trim().length > 0) {
     if (updatedFilters.length > 0) {
        updatedFilters += " | " + showMeClick.trim();
     } else {
        updatedFilters += showMeClick.trim();
     }
  }

  if (selectefulldate && selectefulldate.trim().length > 0) {
     if (updatedFilters.length > 0) {
        updatedFilters += " | " + selectefulldate.trim();
     } else {
        updatedFilters += selectefulldate.trim();
     }
  }

  //updated dataLayerObject
  var calendarEventsDataValues = $(".calendar-event .mobile-not-show").data().trackingClickInfoValue;
  if(calendarEventsDataValues) {
    var jsonStr = calendarEventsDataValues.replace(/'/g, '"');
    var json = JSON.parse(jsonStr);
    json.articleFilter = updatedFilters;
    var updatedValues = JSON.stringify(json).replace(/"/g, "'");
    $(".calendar-event > .mobile-not-show").attr("data-tracking-click-info-value", updatedValues);
  }
}
