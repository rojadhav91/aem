import moment from 'moment';
import './currency-helper';
import CurrencyHelper from './currency-helper';

// Fixing rate
$(document).ready(function () {
  let fixingRate = $('.fixing-rate');
  let timeSelect = fixingRate.find('.time-select');
  let tableContainer = fixingRate.find('.table-content-container');
  let emptyLabel = fixingRate.find('.exchange-rate__empty-label');
  let tableElements = fixingRate.find('.exchange-rate-table-content');
  let selectedDateElements = fixingRate.find('.calendar__input-field');

  let currentDate = moment().format('DD/MM/yyyy');
  let currentTime = null;
  let currentTimeOptions = [];

  function generateTable(element, items) {
    element.empty();
    items.forEach((item, idx, array) => {
      let lastRow = (idx === array.length - 1);
      element.append(
        `
      <div class="exchange-rate__table-records">
        <div class="table__first-column first-column ${lastRow ? 'last-row' : ''}">
          <p>${item.sourceCurrency}/${item.targetCurrency}</p>
        </div>
        <div class="table-records__data">
          <div class="table-records__data-content ${lastRow ? 'last-row' : ''}">
            <p>${CurrencyHelper.numberWithCommas(item.vndLoan)}</p>
          </div>
          <div class="table-records__data-content ${lastRow ? 'last-row' : ''} last-column">
            <p>${item.inputTime ?? ''}</p>
          </div>
        </div>
      </div>
    `
      );
    });
  }

  selectedDateElements.each((idx, element) => {
    $(element).text(currentDate);
    $(element).on('updateCal', function () {
      if (currentDate === $(element).text()) {
        return;
      }
      currentDate = $(element).text();
      currentTime = null;
      updateData(currentDate, currentTime);
    });
  });

  timeSelect.on('timeChanged', function(event, newValue) {
    currentTime = newValue;
    updateData(currentDate, currentTime);
  });

  updateData(currentDate, null);

  function updateData(date, time) {
    selectedDateElements.each((idx, element) => {
      $(element).text(currentDate);
    });
    let baseUrl = fixingRate.attr('data-url');
    if (baseUrl === undefined) {
      return;
    }
    let url = baseUrl + '/_jcr_content.fixing-rates';
    if (date) {
      url += `.${date.split('/').toReversed().join('-')}`;
      if (time) {
        url += `.${time.replaceAll(':', '-')}`;
      }
    }
    url += '.integration.json';
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      success: function (data) {
        let fixingRate = data?.fixingRate;
        // If not available
        if (fixingRate === null || fixingRate.data.length === 0) {
          emptyLabel.removeClass('hidden');
          tableContainer.addClass('hidden');
          timeSelect.changeOptions(fixingRate.updatedTimes, currentTime);
          return;
        }

        emptyLabel.addClass('hidden');
        tableContainer.removeClass('hidden');

        // generate table
        tableElements.each((idx, element) => {
          generateTable($(element), fixingRate.data);
        });
        timeSelect.changeOptions(fixingRate.updatedTimes, currentTime);
        if (currentTime === null && fixingRate.updatedTimes.length > 0) {
          currentTime = fixingRate.updatedTimes[0];
        }
        upadateDateTimeStamp();
      },
    });
  }

  //analytics funtion to capture selected data & time from exchange rate : start
  function upadateDateTimeStamp(){
    fixingRate.find('.analytics-active-link').click();
  }
  
  fixingRate.find('.analytics-active-link').on('click',function(){
      $(this).datetimeAnalyticUpdate(currentDate, currentTime);
  });

  upadateDateTimeStamp(); // analytics function call on DOM load to update value
});
