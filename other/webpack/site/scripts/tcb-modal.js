window.ModalHandler = class ModalHandler {
  parent;
  modal;
  button;
  container;

  title;
  actionLabel;
  open = false;

  onOpen = () => {};
  onClose = () => {};

  constructor(title, action, buttonElement, containerElement) {
    this.title = title;
    this.actionLabel = action;
    this.button = buttonElement;
    this.container = containerElement;
    this.parent = containerElement.parentElement;

    this.listenEvents();
  }

  listenEvents() {
    $(this.button).on('click', this.showModal.bind(this));
    $(window).on('resize', this.handleWindowResize.bind(this));
  }

  showModal() {
    this.modal = $(`<div class="tcb-modal"></div>`);
    const closeIcon = $(`<span class="material-symbols-outlined btn-close">close</span>`);
    const applyButton = $(`<div class="tcb-button tcb-button--light-black">${this.actionLabel}</div>`);
    const header = $(`<div class="tcb-modal_header">
              <span class="tcb-modal_title">${this.title}</span>
          </div>`);
    const actionBar = $(`<div class="tcb-modal_action-bar"></div>`);
    header.append(closeIcon);
    actionBar.append(applyButton);
    closeIcon.on('click', this.closeModal.bind(this));
    applyButton.on('click', this.execute.bind(this));
    this.modal.append(header);
    this.modal.append(this.container);
    this.modal.append(actionBar);
    $(this.parent).append(this.modal);
    $(this.parent.parentElement).addClass('open');
    $('body').addClass('modal-showing');
    this.open = true;
    this.onOpen();
  }

  execute() {
    this.closeModal(true);
  }

  closeModal(action) {
    // For temporary specific case
    $(this.parent).prepend(this.container);
    this.parent.removeChild(this.modal[0]);
    $('body').removeClass('modal-showing');
    $(this.parent.parentElement).removeClass('open');
    this.open = false;
    this.onClose(action);
  }

  handleWindowResize() {
    if (this.open && window.innerWidth >= 768) {
      this.closeModal();
    }
  }
};

window.TcbModal = class TcbModal {
  content;
  backdrop;
  constructor(content) {
    if (!content.length) {
      return;
    }
    this.content = content;
    this.content[0].parentElement.removeChild(content[0]);
    this.content.removeClass('hidden');
    this.backdrop = $('<div class="tcb-modal_backdrop"></div>');
    this.backdrop.on('click', this.close.bind(this));
    this.content.prepend(this.backdrop);
  }

  show() {
    $(document.body).append(this.content);
  }

  close() {
    document.body.removeChild(this.content[0]);
  }
};
