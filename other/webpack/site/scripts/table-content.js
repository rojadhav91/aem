$(document).ready(function () {
  let tableContent = $(".table-content");
  let isHidden = false;
  const topTableContent = $('.global-header').height() + 40 || 40;
  tableContent.css('top', topTableContent);
  tableContent.each(function () {
    let tableIcon = $(this).find(".table-content__icon");
    let tableEntered = $(this).find(".table-content__entered");
    let tableLabel = $(this).find(".table-content__label");
    let tableEnteredWrapper = $(this).find(".table-content__entered-wrapper");
    let height;
    let isExpanded = true;
    tableLabel.on("click", () => {
      height = tableEnteredWrapper.height() > 0 ? tableEnteredWrapper.height() : height;
      tableIcon.toggleClass("click");
      tableEntered.toggleClass("hide");
      if (!tableEntered.hasClass("hide")) {
        tableEntered.height(height);
        isExpanded = true;
        isHidden = false;
      } else {
        tableEntered.height(height);
        tableEntered.height(0);
        isExpanded = false;
        isHidden = true;
      }
    });
    tableEntered.on('transitionend webkitTransitionEnd oTransitionEnd', () => {
      if (isExpanded) {
        tableEntered.height('auto');
      }
    });
  });
  let toc = $(".table-content .table-content__entered");
  $(window).on('resize', function () {
    if ($(window).width() > 767 && isHidden) {
      toc.removeClass('hide');
      toc.height('auto');
      isHidden = false;
    }
  });

  tableContent.find('a[href^="#"]').on('click', function(e) {
    e.preventDefault();
    const globalHeight = $('.global-header').height() || 0;
    const targetId = $(this).attr('href');
    const targetElement = $(targetId);
  
    if (targetElement.length) {
      const targetPosition = targetElement.offset().top - globalHeight + $('body').scrollTop() - 40;
      $('html, body').animate({
        scrollTop: targetPosition,
        behavior: "smooth"
      });
    }
  });
});
