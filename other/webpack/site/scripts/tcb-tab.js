const TCB_TAB_ACTIVE = 'tcb-tabs_item--active';

window.TcbTab = class TcbTab {
  current = 0;
  tabs = [];
  tabElements = [];
  wrapper;
  tabList;
  scrollable;
  indicator;

  handleSelectTab;

  constructor(wrapperElement, onSelectTab = () => {}) {
    this.handleSelectTab = onSelectTab;
    const element = $(wrapperElement);
    this.current = element.attr('activeIndex') || 0;
    this.wrapper = wrapperElement;
    this.tabs = element.data('tabs').split(';');
    this.initTabs();
    this.checkScrollable();
  }

  initTabs() {
    this.tabList = $('<div class="tcb-tabs_tab-list"></div>')[0];
    this.scrollable = $('<div class="tcb-tabs_scrollable"></div>')[0];
    this.indicator = $('<div class="tcb-tabs_indicator"></div>')[0];
    this.scrollable.append(this.indicator);

    this.tabs.forEach((item, index) => {
      this.tabElements[index] = $(
        `<div class="tcb-tabs_item${
          index === this.current ? ' ' + TCB_TAB_ACTIVE : ''
        }" data-tabIndex="${index}" data-tracking-click-event="linkClick" data-tracking-click-info-value="{'linkClick' : '${item}'}" data-tracking-web-interaction-value="{'webInteractions': {'name': 'Tabs control','type': 'other'}}">${item}</div>`
      )[0];

      $(this.tabElements[index]).on('click', this.handleTabClick.bind(this));

      this.scrollable.append(this.tabElements[index]);
    });
    this.tabList.append(this.scrollable);
    this.wrapper.append(this.tabList);

    const next = $('<div class="tcb-tabs_control tcb-tabs_control--next"></div>')[0];
    $(this.wrapper).append(next);

    const prev = $('<div class="tcb-tabs_control tcb-tabs_control--prev"></div>')[0];
    $(this.wrapper).prepend(prev);

    $(this.tabList).on('scroll', () => {
      this.checkScrollable();
    });
    $(next).on('click', this.handleNext.bind(this));
    $(prev).on('click', this.handlePrev.bind(this));
    $(window).on('resize', this.updateIndicatorSize.bind(this));

    // To wait for element has fully rendered on screen
    setTimeout(() => {
      this.updateIndicatorSize();
      this.handleSelectTab(this.current, this.tabs[this.current]);
    });
  }

  checkScrollable() {
    if (this.tabList.scrollWidth > this.tabList.clientWidth) {
      if (this.tabList.scrollLeft > 0) {
        this.wrapper.classList.add('can-prev');
      } else {
        this.wrapper.classList.remove('can-prev');
      }
      if (this.tabList.scrollLeft < this.tabList.scrollWidth - this.tabList.clientWidth) {
        this.wrapper.classList.add('can-next');
      } else {
        this.wrapper.classList.remove('can-next');
      }
    }
  }

  handleNext() {
    $(this.tabList).animate(
      {
        scrollLeft: this.tabList.scrollLeft + 300,
      },
      300
    );
  }

  handlePrev() {
    $(this.tabList).animate(
      {
        scrollLeft: this.tabList.scrollLeft - 300,
      },
      300
    );
  }

  handleTabClick(event) {
    this.tabElements[this.current].classList.remove(TCB_TAB_ACTIVE);
    this.current = event.target.dataset.tabindex;
    event.target.classList.add(TCB_TAB_ACTIVE);
    this.indicator.style.left = event.target.offsetLeft + 'px';
    this.indicator.style.width = event.target.clientWidth + 'px';
    this.handleSelectTab(this.current, this.tabs[this.current], event.target);
    this.scrollToPartiallyVisibleTab(event);
  }

  scrollToPartiallyVisibleTab(event){
    let targetLeft = event.target.offsetLeft;
    let targetWidth = event.target.clientWidth;
    let scrollViewLeft = this.tabList.scrollLeft;
    let scrollViewWidth = event.target.parentElement.parentElement.offsetWidth;
    if(targetLeft < scrollViewLeft){
      $(this.tabList).animate(
        {
          scrollLeft: targetLeft,
        },
        300
      );
    }
    if((targetLeft + targetWidth) > (scrollViewLeft + scrollViewWidth)){
      $(this.tabList).animate(
        {
          scrollLeft: targetLeft + targetWidth - scrollViewWidth,
        },
        300
      );
    }
  }

  updateIndicatorSize() {
    this.indicator.style.left = this.tabElements[this.current].offsetLeft + 'px';
    this.indicator.style.width = this.tabElements[this.current].clientWidth + 'px';
  }
};
