function loadNetworkPanels(){
  let tabSlider = $('.network-grid-component');
  let slideshow = tabSlider.find('.slideshow .item-list');
  let slideItem = slideshow.find('.item');
  let slidePanel = tabSlider.find('.panel-list .panel');

  tabSlider.each(function () {
    let itemIndex = 1;
    $(slideItem[0]).addClass('active');

    //auto slideshow
    setInterval(function () {
      slideshow.children().removeClass('active');
      $(slideItem[itemIndex]).addClass('active');
      for (i = 0; i < slidePanel.length; i++) {
        $(slidePanel[i]).css('box-shadow', 'unset');
      }

      $(slidePanel[itemIndex]).css('box-shadow', 'rgba(0, 0, 0, 0.1) 0px 4px 12px');

      itemIndex++;

      if (itemIndex === slideItem.length) {
        itemIndex = 0;
      }
    }, 5000);

    // click panel
    slidePanel.click(function () {
      for (i = 0; i < slidePanel.length; i++) {
        $(slidePanel[i]).css('box-shadow', 'unset');
      }

      $(this).css('box-shadow', 'rgba(0, 0, 0, 0.1) 0px 4px 12px');
      slideshow.children().removeClass('active');
      $(slideItem[$(this).index()]).addClass('active');
      itemIndex = $(this).index();
    });
  });
}

function networkGridAnalytics() {
  let tabSlider = $('.network-grid-component');
  tabSlider.each(function () {
    let panels = $(this).find('.panel');
    panels.each(function() {
      let panelTitle = $(this).find('.title').text();
      $(this).attr("data-tracking-click-event", "linkClick");
      $(this).attr("data-tracking-click-info-value", "{'linkClick' : '" + panelTitle + "'}");
      $(this).attr(
          "data-tracking-web-interaction-value", "{'webInteractions': {'name': 'Network Grid','type': 'other'}}"
      );
    })
  });
}

$(document).ready(function () {
  // change between panels in network grid
  loadNetworkPanels();
  window.addEventListener('message', (e) => {
    if (e.data === 'reloadNetworkGrid') {
      loadNetworkPanels();
    }
  }, false);
  // AA implementaion
  networkGridAnalytics();
});