$(function() {
  $('.article-listing').each(function() {
    const perPage = $(this).attr('data-number') ?? 9;
    const numberItems = $(this).find('.article-card').length;
    const viewMore = $(this).find('.view-more');
    const viewMoreBtn = viewMore.find('.btn.btn-outline');
    $(this).find('.article-card').css('display', 'none');
    let currentItems = 0;

    const updateItems = () => {
      currentItems += perPage;
      $(this).find(`.article-card:nth-child(-n+${currentItems})`).css('display', '');
      if (numberItems <= currentItems) {
        viewMore.css('display', 'none');
      } else {
        viewMore.css('display', '');
      }
    }

    updateItems();
    viewMoreBtn.on('click', function() {
      updateItems();
    });
  });

  $('.table-content__tableItem-text').click(function(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    const anchorName = $(this)?.attr('href');

    if (!anchorName) {
      return;
    }

    const headerElement = $('.header_layout');
    const tocElement = $('.articletableofcontent');
    const articleElement = $('.article-text');
    const anchorElement = articleElement?.find(`${anchorName}`)[0];

    if (!(anchorElement && headerElement?.length > 0 && tocElement?.length > 0 && articleElement?.length > 0)) {
      return;
    }

    const offset = anchorElement.offsetTop;
    const heightOfArticle = articleElement.offset().top;
    const heightOfHeader = headerElement.outerHeight();
    const heightOfTOC = tocElement.outerHeight();
    let additionSpace = window.innerWidth > 992 ? 2: 0;

    location.hash = anchorName;

    $('body').animate({scrollTop: offset - (heightOfHeader + heightOfTOC + additionSpace)}, 200);

    if (heightOfArticle > heightOfHeader) {
      $('.table-content__label').click();
    }
  });

  $('body').on('scroll', function() {
    const tableLabel = $('.table-content__label');
    const tableEntered = $('.table-content__entered');
    const tocElement = $('.articletableofcontent');
    const headerElement = $('.header_layout');
    const articleElement = $('.article-text');

    if (!(tocElement?.length > 0 && articleElement?.length > 0 && tableLabel?.length > 0 && tableEntered?.length > 0 && headerElement?.length > 0)) {
      return;
    }

    const heightOfArticle = articleElement?.offset()?.top ?? 0;
    const heightOfHeader = headerElement?.outerHeight() ?? 0;

    if (heightOfArticle < heightOfHeader) {
      tocElement.css('top', `${heightOfHeader}px`);
      if (!tableEntered.hasClass('hide')) {
        tableLabel.click();
      }
    } else {
      tocElement.css('top', '');
    }
  });
});