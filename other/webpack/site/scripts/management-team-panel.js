// Horizontal Tab Bio
$(document).ready(function () {
    $('.management-team-panel').each(function () {
      $('.tab-horizontal-bio__button-see-more').click(function () {
        $(this).parent().find('.tab-horizontal-bio__card.display-none').removeClass('display-none');
        $(this).parent().find('.tab-horizontal-bio__card.display-none').addClass('display-block');
        $(this).hide();
      });
  
      var profileBtn = $(this).find('.tab-horizontal-bio__card-action-btn');
      var bioPopup = $(this).find('.bio-popup');
      var bioPopupContent = bioPopup.find('.bio-popup-content');
      var closePopupBtn = bioPopupContent.find('.close-btn');
      var loadMoreButton = $(this).find('.load-more__button');
      var bioItems = $(this).find('.tab-horizontal-bio__card');
      var bioShowed = 6;

      bioItems.each(function (i) {
        if (i >= 6) {
          $(this).hide();    
        }
      });
  
      profileBtn.click(function () {
        var selectedBio = $(this).closest('.tab-horizontal-bio__card');
        var selectedBioRole = selectedBio.find('.tab-horizontal-bio__card-content-role');
        var selectedBioName = selectedBio.find('.tab-horizontal-bio__card-content-title');
        var selectedBioDetail = selectedBio.find('.tab-horizontal-bio__card-content-description');
        var selectedBioAva = selectedBio.find('img');
  
        bioPopupContent.find('.position').text(selectedBioRole.text());
        bioPopupContent.find('.name').text(selectedBioName.text());
        bioPopupContent.find('.detail-info').text(selectedBioDetail.text());
        bioPopupContent.find('.avatar img').attr('src', selectedBioAva.attr('src'));
  
        bioPopup.addClass('showed');
        $('body').addClass('modal-showing');
      });
  
      closePopupBtn.click(function () {
        bioPopup.removeClass('showed');
        $('body').removeClass('modal-showing');
      });
      $(document).click(function (event) {
        if (bioPopup.hasClass('showed') &&
          !$(event.target).is(profileBtn) &&
          !$(event.target).is(bioPopupContent) &&
          !$(event.target).parents().is(bioPopupContent)
        ) {
          bioPopup.removeClass('showed');
          $('body').removeClass('modal-showing');
        }
      });

      loadMoreButton.click(function () {
        bioShowed += 6;
        bioItems.each(function (i) {
          if ($(this).is(':hidden') && i < bioShowed) {
            $(this).show();
          }
        });
      
        if(bioShowed > bioItems.length -1) {
          loadMoreButton.hide();
        }
      });
    });
  });
  // End of Horizontal Tab Bio