window.GridPagination = class GridPagination {
  wrapper;
  moreButton;
  childClassName;
  items = [];
  showItems = [];

  pageSize = 8;
  currentPage = 1;
  displayMode = 'large';
  // large > 1200 | 991 < medium < 1199 | 768 < low-medium < 990 | small < 768

  get length() {
    return this.pageSize * this.currentPage;
  }

  get mode() {
    return this.displayMode;
  }

  set mode(mode) {
    this.displayMode = mode;
    this.reset();
  }

  constructor(wrapper, children) {
    this.wrapper = wrapper;
    this.childClassName = children;
    this.init();
    this.handleResize();
    this.attachElements();
    this.showMoreButton();
  }

  init() {
    const data = this.wrapper.find(this.childClassName);
    data.each((index, element) => {
      this.items.push(element);
      this.wrapper[0].removeChild(element);
    });
    $(window).on('resize', this.handleResize.bind(this));
  }

  handleResize() {
    const currentMode =
      window.innerWidth >= 1200
        ? 'large'
        : window.innerWidth >= 991
        ? 'medium'
        : window.innerWidth > 768
        ? 'low-medium'
        : 'small';
    if (currentMode !== this.mode) {
      this.mode = currentMode;
    }
  }

  reset() {
    this.wrapper.empty();
    this.currentPage = 1;
    this.pageSize = this.mode === 'small' ? 3 : 8;
    this.attachElements();
    this.showMoreButton();
  }

  more() {
    if (this.length < this.items.length) {
      this.currentPage += 1;
      this.attachElements();
      if (this.length >= this.items.length) {
        this.hideMoreButton();
      }
    }
  }

  attachElements() {
    const showData = this.items.slice((this.currentPage - 1) * this.pageSize, this.currentPage * this.pageSize);
    showData.forEach((element) => {
      this.wrapper.append(element);
    });
  }

  showMoreButton() {
    if (!this.moreButton && this.length < this.items.length) {
      this.moreButton = $(
        '<div class="tcb-grid-card_actions tcb-container"><div class="tcb-grid-card_show-more">Xem thêm</div></div>'
      );
      this.moreButton.on('click', this.more.bind(this));
      this.wrapper[0].parentElement.appendChild(this.moreButton[0]);
    }
  }

  hideMoreButton() {
    this.wrapper[0].parentElement.removeChild(this.moreButton[0]);
    this.moreButton = null;
  }
};
