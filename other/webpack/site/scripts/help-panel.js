//FAQ Tab Vertical Component
$(document).ready(function () {
  $('.help-panel').each(function () {
    $(this)
      .find('.help-panel__tab-control-btn')
      .click(function () {
        if ($(window).width() >= 992) {
          activeTab(this);
        } else {
          showpopup(this);
        }
      });
    if ($(window).width() < 992) {
      $('.help-panel__tab-content').hide();
      $('.help-panel__tab-control-btn').removeClass('tab-active');
      $('.help-panel .help-panel__article-content').css('display', 'none');
    } else {
      activeTab($('.help-panel__tab-control-btn:first-child'));
    }
    $(window).resize(function () {
      $('.help-panel__tab-content').hide();
      $('body').removeClass('show-popup');
      if ($(window).width() < 992) {
        $('.help-panel__tab-control-btn').removeClass('tab-active');
        $('.help-panel .help-panel__article-content').css('display', 'none');
        $('.help-panel .help-panel__tab-content').css('display', 'none');
      } else {
        $('.help-panel__tab-content').css('cssText', 'background-color:white;');
        activeTab($('.help-panel__tab-control-btn:first-child'));
        $('.help-panel .help-panel__tab-content').css('display', 'block');
      }
    });
    $(this).find('.help-panel__tab-content')
      .click(function (e) {
        if($(e.target).hasClass('help-panel__tab-content')) {
          $('.help-panel__article-content').removeClass('open');
          setTimeout(()=> {
            $('.help-panel__tab-content').hide();
            $('body').removeClass('show-popup');
            $('.help-panel__tab-control-btn').removeClass('tab-active');
            $('.help-panel__tab').css('cssText', 'background-color:unset');
          },300);
        }
      });
    $(this)
      .find('.help-panel__modal-close')
      .click(function () {
        $('.help-panel__article-content').removeClass('open');
        setTimeout(()=> {
          $('.help-panel__tab-content').hide();
          $('body').removeClass('show-popup');
          $('.help-panel__tab-control-btn').removeClass('tab-active');
          $('.help-panel__tab').css('cssText', 'background-color:unset');
        },300);
      });
    function activeTab(obj) {
      $('.help-panel .help-panel__tab-control-btn').removeClass('tab-active');
      $(obj).addClass('tab-active');
      var id = $(obj).data('tab-control');
      $('.help-panel__article-content').css('display', 'none');
      $('.help-panel__article-content').each(function () {
        if ($(this).data('tab-content') == id) {
          $(this).css('display', 'block');
        }
      });
    }
    function showpopup(obj) {
      $('.help-panel .help-panel__tab-control-btn').removeClass('tab-active');
      $(obj).addClass('tab-active');
      var id = $(obj).data('tab-control');
      $('.help-panel__article-content').hide();
      $('.help-panel__tab-content').hide();
      $('body').removeClass('show-popup');
      $('.help-panel__article-content').each(function () {
        var item = $(this);
        if (item.data('tab-content') == id) {
          item.show();
          $('body').addClass('show-popup');
          $('.help-panel__tab-content').show();
          
          $('.help-panel__tab-content').css(
            'cssText',
            'background: #00000069; position: fixed;margin: 0 ;width: 100vw; height: 100vh; top: 0; left: 0; z-index: 2; max-width: unset; border-radius: 0px;'
          );
          setTimeout(()=> {
            item.addClass('open');
          },50);
        }
      });
    }
  });
});
// End FAQ Tab Vertical Component