
$(document).ready(function () {
  $('.carousel-images').each(function () {
    $('.carousel-images__carousel-img-container').slick({
      dots: false,
      infinite: true,
      arrows: true,
      speed: 300,
      slidesToShow: 1,
      variableWidth: true,
      prevArrow: "<button type='button' class='slick-prev'>❮</button>",
      nextArrow: "<button type='button' class='slick-next'>❯</button>",
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
          },
        },
      ],
    });
    $(window).on('resize', function () {
      setTimeout(()=> {
        if($('.carousel-images').length > 0 ) {
          $('.carousel-images__carousel-img .slick-prev').text("❮");
          $('.carousel-images__carousel-img .slick-next').text("❯");
        }
      },500);
    });
  });
});