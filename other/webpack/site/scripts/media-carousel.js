// import Swiper bundle with all modules installed
import Swiper from 'swiper/bundle';

// Inspire slider
$(document).ready(function () {  

  let inspireSliderCmp = $('.inspire-slider-component');

  inspireSliderCmp.each(function () {
    let $component = $(this);

    let centerSlide = inspireSliderCmp.find('.swiper-slide').length / 2;
    let sliderSwiper = new Swiper('.inspire-slider-component .inspire-slider-swiper', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      grabCursor: true,
      effect: 'creative',
      slidesPerView: 1,
      centeredSlides: true,
      centerInsufficientSlides: true,
      initialSlide: centerSlide - 1,
      creativeEffect: {
        prev: {
          shadow: true,
          translate: ['-100%', 0, -500],
        },
        next: {
          shadow: true,
          translate: ['100%', 0, -500],
        },
      },
    });
    sliderSwiper.on('init', function() {
      resizeMediaCarousel($component);
     });

    let videoPopup = $(this).find('.slider-popup');
    let videoPopupContent = videoPopup.find('.slider-popup-content');
    let popupClose = videoPopupContent.find('svg');
    let popupIframe = videoPopupContent.find('iframe');
    let videoBtn = $(this).find('.btn-video');
    let videoUrl = videoBtn.attr('video-url');

    videoBtn.click(function () {
      videoPopup.css('display', 'block');
      $('body').addClass('dropdown-overflow-hidden');
      popupIframe.attr('src', videoUrl);
    });

    $(document).click(function (event) {
      let $target = $(event.target);
      if (
          $target.hasClass('close-video')
          || 
          ($target.closest('.btn-video').length == 0 && !$target.hasClass('slider-popup-content') && $target.closest('.slider-popup-content').length == 0)
        ) {
        videoPopup.css('display', 'none');
        $('body').removeClass('dropdown-overflow-hidden');
      }
    });

    popupClose.click(function () {
      videoPopup.css('display', 'none');
      $('body').removeClass('dropdown-overflow-hidden');
    });

    // set position btn is center of active card when windows resize
    $(window).on('resize', function () {
      resizeMediaCarousel($component);
    });
    resizeMediaCarousel($component);

    let $primaryTrackingLst = $('.primary-tracking');
    $primaryTrackingLst.each(function () {
      let $primaryTracking = $(this);
      $primaryTracking.click(function (event) {
        let $secondaryTracking = $primaryTracking.parent().find('.secondary-tracking');
        $secondaryTracking.trigger('click');
      });
    });
  });

  function resizeMediaCarousel($component) {
    $component.find('.swiper-button').addClass('hide');
    setTimeout(function() {
      let $activeCardImage = $component.find('.swiper-slide-active img');
      let $prevBtn = $component.find('.swiper-button-prev');
      let $nextBtn = $component.find('.swiper-button-next');
      let btnTop = ($activeCardImage.height() - $prevBtn.height()) / 2;
      $prevBtn.css('top', btnTop);
      $nextBtn.css('top', btnTop);
      $component.find('.swiper-button').removeClass('hide');
    }, 300); // wait to swiper rendered
  }
});