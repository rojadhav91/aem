/* eslint @typescript-eslint/no-var-requires: "off" */
const moment = require("moment");

const numberWithDots = (value) => {
  return value?.toString()
  .replaceAll('.', '')
  .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

const numberWithCommas = (value) => {
  if (!value) {
    return '';
  }
  return numberWithDots(value)?.replaceAll('.', ',');
};

const toPureNumber = (value) => {
  return parseInt(value.toString().replaceAll(',', '') || 0);
};

const toDisplayValue = (value, selectedTotalPrice) => {
  const formatedValue = numberWithDots(Math.round(value));
  return formatedValue == 'NaN' || formatedValue == 'Infinity' || formatedValue == undefined || !selectedTotalPrice
    ? '0 VND'
    : formatedValue + ' VND';
};

const setNumberValueOnField = (field, value) => {
  const start = field.selectionStart;
  const end = field.selectionEnd;
  field.value = numberWithDots(value);
  field.setSelectionRange(start, end);
};
window.Slider = class Slider {
  wrapper;
  currentPercent;
  inputRange;
  constructor(element) {
    this.wrapper = element;
    this.listenEvent();
  }
  listenEvent() {
    const wrap = $(this.wrapper);
    this.currentPercent = wrap.find(`.percent-value`)[0];
    this.inputRange = wrap.find('input[type="range"]')[0];
    $(this.inputRange).on('input', this.updateValueStableOnView.bind(this));
    $(this.currentPercent).on('input', this.updateValueStableOnView.bind(this));
    this.updateValueStableOnView();
  }
  updateValueStableOnView() {
    const value = this.inputRange.value;
    const percent = $(this.currentPercent);
    percent.text(value + '%');
    percent.css('left', `${value}%`);
    percent.css(
      'transform',
      value < 50
        ? 'translateX(calc(-50% + ' + (12 - (12 * value) / 50) + 'px))'
        : 'translateX(calc(-50% - ' + (12 - (12 * (100 - value)) / 50) + 'px))'
    );
    $(this.inputRange).css('background-size', value + '% 100%');
  }
};
const initSlider = () => {
  const sliders = $('.slide-container');
  sliders.each((index, element) => {
    new Slider(element);
  });
};
window.LoanForm = class LoanForm {
  wrapper;
  totalPrice;
  totalPriceInput;
  range;
  rangeValue;
  selectedPrice;
  selectedPriceInput;
  loanMonth = 0;
  interestRate;
  totalInterest;
  startDate;
  monthlyPayFrom;
  monthlyPayTo;
  totalPay;
  sumOfPay;
  sumOfOrg;
  detailButton;
  modalOpen;
  modalClose;
  tableData = [];
  needUpdateSelected = true;
  principalMoney;
  interest;
  popupTotalLabel;
  panelInfo;
  hasUpdateRange = false;
  rangeValueChanged;
  constructor(element, panelInfo) {
    this.wrapper = element;
    this.panelInfo = panelInfo;
    this.listenEvents();
  }
  listenEvents() {
    const wrap = $(this.wrapper);
    const panelInfo = $(this.panelInfo);
    this.popupTotalLabel = wrap.attr("data-total");

    this.totalPriceInput = wrap.find('[name="totalPrice"]')[0];
    this.detailButton = wrap.find('.loan-calc')[0];
    
    $(this.totalPriceInput).on('input', this.handleTotalPriceChange.bind(this));
    $(this.totalPriceInput).on('keypress', this.handleTotalKeydown.bind(this));

    this.range = wrap.find('.tcb-input-range')[0];
    $(this.range).click(() => {
        this.hasUpdateRange = true;
    });
    $(this.range).on('input', this.handleRangeChange.bind(this));

    this.selectedPriceInput = wrap.find('[name="selectedPrice"]')[0];
    $(this.selectedPriceInput).on('input', this.handleSelectedPriceChange.bind(this));

    this.loanMonth = wrap.find('[name="loanMonth"]')[0];
    $(this.loanMonth).on('input', this.handleChangeLoanMonth.bind(this));

    this.interestRate = wrap.find('[name="interestRate"]')[0];
    $(this.interestRate).on('input', this.handleChangeInterestRate.bind(this));

    this.startDate = wrap.find('[name="startDate"]')[0];
    $(this.startDate).on('input', this.handleChangeStartDate.bind(this));

    this.detailButton = panelInfo.find('.content-button__link.loan-calc')[0];
    $(this.detailButton).on('click', this.openDetailModal.bind(this));
    $(this.detailButton).on('click', this.preventScroll.bind(this.event));

    this.modalOpen = wrap.find('.modal')[0];
    $(this.detailButton).click(function () {
      $(this.modalOpen).addClass('active');
    });
    this.modalClose = wrap.find('.close-modal')[0];

    $(this.modalClose).on('click', this.closeDetailModal.bind(this));

    const outputs = panelInfo.find('.monthly-pay');
    this.monthlyPayFrom = outputs[0];
    this.monthlyPayTo = outputs[1];
    this.totalPay = panelInfo.find('.total-pay')[0];
    
    this.principalMoney = panelInfo.find('.principal-money')[0];
    this.interest = panelInfo.find('.monthly-interest')[0];
    
  }
  preventScroll(event) {
    event.preventDefault();
  }
  closeDetailModal() {
    $('body').removeClass('overflow-hidden');
    this.modalOpen.classList.remove('active');
  }
  openDetailModal() {
    $('body').addClass('overflow-hidden');
    this.handleChangeStartDate();
    this.modalOpen.classList.add('active');
    let trHtml = '';
    for (let i = 0; i < this.tableData?.length; i++) {
      let stt = '<td class="table-cell no"><div>' + this.tableData[i].stt + '</div></td>';
      let limitation = '<td class="table-cell limitation"><div >' + this.tableData[i].limitation + '</div></td>';
      let remaiDebt =
        '<td class="table-cell remain_debt"><div >' +
        (numberWithCommas(Math.round(this.tableData[i].remaiDebt)) || '0') +
        '</div></td>';
      let monthlyPay =
        '<td class="table-cell origin_debt"><div>' +
        (i === 0 ? '' : numberWithCommas(Math.round(this.tableData[i].monthlyPay)) || '0') +
        '</div></td>';
      let remainInterest =
        '<td class="table-cell profit"><div>' +
        (i === 0 ? '' : numberWithCommas(Math.round(this.tableData[i].remainInterest)) || '0') +
        '</div></td>';
      let sumOfPay =
        '<td class="table-cell total_origin_and_profit"><div>' +
        (i === 0 ? '' : numberWithCommas(Math.round(this.tableData[i].sumOfPay)) || '0') +
        '</div></td>';
      let tr =
        "<tr class='table-row'>" + stt + limitation + remaiDebt + monthlyPay + remainInterest + sumOfPay + '</tr>';
      trHtml += tr;
    }
    let totalLoan = `<td class="table-cell footer-cell origin_debt">
        ${this.selectedPrice ? numberWithCommas(Math.round(this.selectedPrice)) : '0'}
    </td>`;
    let totalInterest = `<td class="table-cell footer-cell profit">
        ${this.interestRate.value ? numberWithCommas(Math.round(this.totalInterest)) || '0' : '0'}
    </td>`;
    let sumOfOrg = `<td class="table-cell footer-cell total_origin_and_profit">
        ${this.selectedPrice ? numberWithCommas(Math.round(this.sumOfOrg)) || '0' : '0'}
    </td>`;
    let footerTd = `<tr class="footer-tbl">
            <td class="table-cell footer-cell">${this.popupTotalLabel}</td>
            <td class="table-cell footer-cell" colspan="2"></td>
                ${totalLoan}
                ${totalInterest}
                ${sumOfOrg}
        </tr>`;
    $('.tbl-body').html(trHtml);
    $('.footer-table').html(footerTd);
    $(this.modalOpen).addClass('active');
  }
  handleTotalKeydown(event) {
    const maxNumber = 500000000000;
    const currentInput = this.totalPriceInput.value;
    if (currentInput.length > 14) {
      event.preventDefault();
    }
    if(toPureNumber(currentInput) > maxNumber) {
      this.totalPriceInput.value = numberWithDots(maxNumber);
      this.handleTotalPriceChange();
    }
    var charCode = event.which ? event.which : event.keyCode;
    if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
      return false;
    }
  }
  handleTotalPriceChange() {
    this.totalPrice = toPureNumber(this.totalPriceInput.value);
    this.totalPriceInput.value = numberWithDots(this.totalPrice);
    if (this.totalPrice) {
      this.enableSlider();
    } else if (this.totalPrice == 0) {
      this.enableSlider();
      this.updateLoan();
    } else {
      this.disableSlider();
    }
    this.updateSelectedPrice();
    this.checkPointBeforeCalc();
  }
  handleSelectedPriceChange() {
    if (!this.selectedPriceInput.value) {
      this.selectedPrice = 0;
    } else {
      let value = toPureNumber(this.selectedPriceInput.value);
      if (value > this.totalPrice) {
        value = this.totalPrice;
      }
      this.selectedPrice = isNaN(value) ? '' : isNaN(value) ? '0' : value;
    }
    setNumberValueOnField(this.selectedPriceInput, this.selectedPrice);
    if (this.totalPrice) {
      const percent = (this.selectedPrice / this.totalPrice) * 100;
      // this.range.value = percent;
      this.rangeValue = Math.floor(percent);
      this.range.dispatchEvent(new CustomEvent('setValue', { detail: this.rangeValue }));
      // this.range.dispatchEvent(new Event('input'));
      this.needUpdateSelected = false;
    }
    this.checkPointBeforeCalc();
  }
  handleRangeChange(event) {
    this.rangeValue = event.detail;
    if(this.selectedPriceInput.value == 0 ) {
      this.hasUpdateRange = false;
    }
    if(this.selectedPriceInput.value !== 0 && event.detail == 0 && !this.hasUpdateRange) {
      this.needUpdateSelected = false;
    }
    if (this.needUpdateSelected) {
      this.updateSelectedPrice();
    } else {
      this.needUpdateSelected = true;
    }
    this.checkPointBeforeCalc();
  }
  handleChangeLoanMonth(event) {
    var inputText = $(event.target).val();
    if (!inputText.match(/[0-9]+/g)) {
      this.loanMonth.value = "";
      event.preventDefault();
      return false;
    }

    if (!this.loanMonth.value || this.loanMonth.value < 0) {
      this.loanMonth.value = 0;
    } else if (this.loanMonth.value > 420) {
      this.loanMonth.value = 420;
    } else {
      this.loanMonth.value = parseInt(this.loanMonth.value);
    }
    this.checkPointBeforeCalc();
  }
  handleChangeInterestRate(event) {
    var inputText = $(event.target).val();
    if (!inputText.match(/[0-9]+/g)) {
      this.interestRate.value = "";
      event.preventDefault();
      return false;
    }
    
    if (!this.interestRate.value || this.interestRate.value < 0) {
      this.interestRate.value = 0;
    } else if (this.interestRate.value > 20) {
      this.interestRate.value = 20;
    } else {
      this.interestRate.value = parseInt(this.interestRate.value);
    }
    this.checkPointBeforeCalc();
  }
  handleChangeStartDate() {
    this.checkPointBeforeCalc();
  }
  updateSelectedPrice() {
    const selectedRange = parseInt(this.rangeValue);
    this.selectedPrice = Math.round((selectedRange * this.totalPrice) / 100);
    if (this.selectedPrice > this.totalPrice) {
      this.selectedPrice = this.totalPrice;
    }
    const loan = numberWithDots(this.selectedPrice);
    this.selectedPriceInput.value = loan == 'NaN' ? 0 : loan == 0 ? '0' : loan;
  }
  nextMonth(startDate, monthChange) {
    let arrDate = startDate.split('/');
    let dateDay = arrDate[0];
    let dateMonth = arrDate[1];
    let dateYear = arrDate[2];
    let date = moment(`${dateYear}/${dateMonth}/${dateDay}`).add(monthChange, 'months').format('DD/MM/YYYY').toString();
    return date;
  }
  getTotalProfit(tableData) {
    return tableData.map((oneMonth) => oneMonth.remainInterest);
  }
  checkPointBeforeCalc() {
    this.updateLoan();
  }
  updateLoan() {
    const selectedTotal = this.totalPrice && this.totalPrice > 0;
    if (this.monthlyPayTo) {
      this.tableData = [
        {
          stt: 0,
          limitation: this.startDate.value,
          remaiDebt: this.selectedPrice,
          monthlyPay: '',
          remainInterest: '',
          sumOfPay: '',
        },
      ];

      const monthlyPay = (this.selectedPrice || 0) / (this.loanMonth.value || 0);
      const monthlyPayTo = (this.interestRate.value / (100 * 12)) * monthlyPay + monthlyPay;
      const monthlyPayFrom =
        parseInt(this.loanMonth.value) == '0'
          ? (this.selectedPrice / 12) * (this.interestRate.value / 100)
          : (this.selectedPrice / 12) * (this.interestRate.value / 100) +
            this.selectedPrice / parseInt(this.loanMonth.value);
      for (let i = 1; i <= this.loanMonth.value; i++) {
        let profitValue = (this.selectedPrice - monthlyPay * (i - 1)) * (this.interestRate.value / 12 / 100);
        let dataEachMonth = {
          stt: i,
          limitation: this.nextMonth(this.startDate.value, i),
          remaiDebt: i == this.loanMonth.value ? 0 : this.selectedPrice - monthlyPay * i,
          monthlyPay: monthlyPay || '0',
          remainInterest: profitValue || '0',
          sumOfPay: monthlyPay + profitValue,
        };
        this.tableData.push(dataEachMonth);
        let totalProfit = this.getTotalProfit(this.tableData);
        this.totalInterest = eval(totalProfit.join('+'));
        this.sumOfOrg = this.loanMonth.value == '0' ? 0 : this.totalInterest + this.selectedPrice;
      }
      this.monthlyPayTo.textContent = toDisplayValue(monthlyPayTo, selectedTotal);
      this.monthlyPayFrom.textContent = toDisplayValue(monthlyPayFrom, selectedTotal);
      this.totalPay.textContent = toDisplayValue(this.sumOfOrg, selectedTotal);
    } else {

      if(this.principalMoney && this.interest) {

        const principalMoney = (this.selectedPrice || 0) / (this.loanMonth.value || 0);
        const monthlyPayTo = ((this.selectedPrice || 0) / 12 ) * (this.interestRate.value / 100);
        
        this.tableData = [
          {
            stt: 0,
            limitation: this.startDate.value,
            remaiDebt: this.selectedPrice,
            monthlyPay: '',
            remainInterest: '',
            sumOfPay: '',
          },
        ];
        let sumOfPay = 0;
        for (let i = 1; i <= this.loanMonth.value; i++) {
          let profitValue = (this.selectedPrice - principalMoney * (i - 1)) * (this.interestRate.value / 12 / 100);
          let dataEachMonth = {
            stt: i,
            limitation: this.nextMonth(this.startDate.value, i),
            remaiDebt: i == this.loanMonth.value ? 0 : this.selectedPrice - principalMoney * i,
            monthlyPay: principalMoney || '0',
            remainInterest: profitValue || '0',
            sumOfPay: principalMoney + profitValue,
          };
          sumOfPay+=principalMoney+profitValue;
          this.tableData.push(dataEachMonth);
        }

        
        this.sumOfOrg = sumOfPay;
        let totalProfit = this.getTotalProfit(this.tableData);
        this.totalInterest = eval(totalProfit.join('+'));
        this.principalMoney.textContent = toDisplayValue(principalMoney, selectedTotal);
        this.interest.textContent = toDisplayValue(monthlyPayTo, selectedTotal);
        this.totalPay.textContent = toDisplayValue((principalMoney + monthlyPayTo), selectedTotal);
      } else {
        const sumOfInterest = (this.selectedPrice / 12) * (this.interestRate.value / 100) * this.loanMonth.value || 0;
        const totalPay = sumOfInterest ? numberWithDots(Math.round(this.selectedPrice + sumOfInterest)) : 0;
        const monthlyPayFrom =
          parseInt(this.loanMonth.value) == '0'
            ? (this.selectedPrice / 12) * (this.interestRate.value / 100)
            : (this.selectedPrice / 12) * (this.interestRate.value / 100) +
              this.selectedPrice / parseInt(this.loanMonth.value);
        this.monthlyPayFrom.textContent = toDisplayValue(monthlyPayFrom, selectedTotal);
        this.totalPay.textContent =
          this.interestRate.value == 0
            ? this.loanMonth.value == 0
              ? '0 VND'
              : numberWithDots(this.selectedPrice) + ' VND'
            : (this.totalPrice && this.totalPrice > 0 ? totalPay : '0') + ' VND' || '0 VND';
      }
    }
  }
  enableSlider() {
    this.range.disabled = false;
    this.range.removeAttribute('disabled');
  }
  disableSlider() {
    this.range.disabled = true;
    this.range.setAttribute('disabled');
  }
};
const initForms = () => {
  const tableQuery = $('.insurance-calculation');
  tableQuery.each((idx, elementTable) => {
    const panelInfo = $(elementTable).find('.panel-info');
    const tcbTabs = $(elementTable).find('.tcb-tabs_item');
    
    tcbTabs.click((e) => {
      if($(e.target).hasClass('tcb-tabs_item--active')) {
        panelInfo.each((id, element) => {
          if($(element).attr('data-id') === $(e.target).attr('data-tabindex')) {
            $(element).show();
          } else {
            $(element).hide();
          }
        });
      }
    });

    panelInfo.each((id, element) => {
      new LoanForm(elementTable,element);
    });

    panelInfo[1] ? $(panelInfo[1]).hide() : '';
  });
};
function calcAnalytics() {
    if ($(".insurance-calculation__panel .panel-info").length > 0) {
        $(".panel-inputs").click(function () {
            var panelClick = $(this);
            $(panelClick)
                .parent()
                .find(".panel-info.active-panel .calulator-analytics")
                .trigger("click");
        });
    }
}

$(document).ready(() => {
  initSlider();
  initForms();
  calcAnalytics();
});
