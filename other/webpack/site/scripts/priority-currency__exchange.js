// Currency Exchange Priority component
import 'slick-carousel';
$(document).ready(function () {
  let priorityExchange = $('.currency-converter');
  priorityExchange.each((_,currencyConverter) => {
    let priorityExchangePanel = $(currencyConverter).find('.priority-exchange__panel');
    let panelItemExchangeSection = priorityExchangePanel.find('.panel-item__exchange-section');
    let exchangeSectionTransType = panelItemExchangeSection.find('.exchange-section__transaction-type-selector');
    let exchangeSectionCurSel = panelItemExchangeSection.find('.exchange-section__currency-selector');

    let exchangeSectionTabWrap = exchangeSectionTransType.find('.exchange-section__tab-wrapper');
    let exchangeSectionTabItem = exchangeSectionTabWrap.find('.exchange-section__tabs-item');
    let currencySelectorCurItem = exchangeSectionCurSel.find('.currency-selector__currency-item');

    let exchangeSectionInput = $(currencyConverter).find('.exchange-section__input')[0];
    let exchangeSectionInputField = $(currencyConverter).find('.exchange-section__input input')[0];
    let exchangeSectionNote = $(currencyConverter).find('.exchange-section__note');
    let exchangeSectionError = $(currencyConverter).find('.exchange-section__error');
    let previousValue;
    let outputMoney = $(currencyConverter).find('input.money__output-field')[0];
    let inputMoney = $(currencyConverter).find('input.money__input-field');
    let typeTransaction;
    let matchingItems;
    let scrollConverter = $(currencyConverter).find('.scroll-currency-converter ');
    let currentCurrencySource;

    currencySelectorCurItem.each(function (index) {
      let exchangeSectionTabScroll = $(this).find('.exchange-section__tabs-scroller');
      if (exchangeSectionTabScroll[0].scrollWidth > exchangeSectionTabScroll[0].clientWidth) {
        if (index === 1) {
          exchangeSectionTabScroll.not('.slick-initialized').slick({
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
          });
        } else {
          exchangeSectionTabScroll.not('.slick-initialized').slick({
            infinite: false,
            arrows: true,
            speed: 300,
            slidesToScroll: 1,
            variableWidth: true,
          });
        }
        exchangeSectionTabScroll.on('afterChange', function () {
            const lastItemRight = $(this).find('.slick-slide').last().get(0).getBoundingClientRect().right;
            const divRight = $(this).get(0).getBoundingClientRect().right;
            if(divRight > lastItemRight) {
              scrollConverter.removeClass('can-next');
            } else {
              scrollConverter.addClass('can-next');
            }
        });
      }
    });

    function disableInput(element) {
      element.addClass('not-found');
      currencySelectorCurItem.addClass('not-found');
      exchangeSectionError.css('display','block');
      exchangeSectionNote.css('display','none');
      $(exchangeSectionInputField).attr('disabled','disabled');
      $(exchangeSectionInput).addClass('active');
      previousValue = commasNumber(exchangeSectionInputField.value) || previousValue;
      exchangeSectionInputField.value = "";
    }

    function enableInput(element) {
      element.removeClass('not-found');
      currencySelectorCurItem.removeClass('not-found');
      exchangeSectionError.css('display','none');
      exchangeSectionNote.css('display','block');
      exchangeSectionInputField.removeAttribute('disabled');
      $(exchangeSectionInput).removeClass('active');
      previousValue = exchangeSectionInputField.value ? commasNumber(exchangeSectionInputField.value) : previousValue;
      exchangeSectionInputField.value = numberWithCommas(previousValue) || exchangeSectionInputField.value;
    }

    let url = currencyConverter.getAttribute('data-url');

    $.ajax({
      url:url,
      type:"GET",
      dataType:"json",
      success: function(response) {
        exchangeSectionTabItem.each(function () {
          if($(this).hasClass('tab-active')) {
            typeTransaction = $(this).attr('data-type');
          }
          $(this).click(function () {
            exchangeSectionTabItem.removeClass('tab-active');
            $(this).addClass('tab-active');
            typeTransaction = $(this).attr('data-type');
            matchingItems = getCurrencyWithLabel(currentCurrencySource, response?.exchangeRate.data, typeTransaction);
            if (!matchingItems) {
              disableInput($(this));
            } else {
              enableInput($(this));
            }
            let result =  formatMoney(parseInt(previousValue) * matchingItems?.[typeTransaction]) || "";
            outputMoney.value = result;
          });
        });
        currencySelectorCurItem.each(function() {
          let currencySelector = $(this);
          let exchangeSectionTabScroll = $(this).find('.exchange-section__tabs-scroller.scroller');
          let currencyItemTabItem = exchangeSectionTabScroll.find('.exchange-section__tabs-item');
          currencyItemTabItem.each(function () {
            let tabItem = $(this);
            if(tabItem.hasClass('tab-active')) {
              currentCurrencySource = tabItem.text().trim();
              matchingItems = getCurrencyWithLabel(tabItem.text().trim(), response?.exchangeRate.data, typeTransaction);
            }
            $(this).click(function () {
              currencyItemTabItem.removeClass('tab-active');
              currentCurrencySource = tabItem.text().trim();
              matchingItems = getCurrencyWithLabel(tabItem.text().trim(), response?.exchangeRate.data, typeTransaction);
              $(this).addClass('tab-active');
              if(!matchingItems || $(this).hasClass('not-found')) {
                disableInput(currencySelector);
              } else {
                enableInput(currencySelector);
              }
              
              let result =  formatMoney(parseInt(previousValue) * matchingItems?.[typeTransaction]) || "";
              outputMoney.value = result;
            });
          });
        });
        inputMoney.keyup(function () {
          previousValue = commasNumber(exchangeSectionInputField.value);
          exchangeSectionInputField.value = numberWithCommas(previousValue);
          let result = formatMoney(parseInt(commasNumber(exchangeSectionInputField.value)) * matchingItems?.[typeTransaction]) || "";
          outputMoney.value = result;
        });
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
  function commasNumber(x) {
    return x
      .toString()
      .replaceAll(',', '');
  }
  function numberWithDot(x) {
    if(x) {
      return x
      .toString()
      .replaceAll('.', '')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return "";
  }

  function numberWithCommas(x) {
    return numberWithDot(x).replaceAll('.', ',');
  }
  function formatMoney(number) {
    var formattedNumber = Number(number).toFixed(3);
    var parts = formattedNumber.split(".");
    var integerPart = parts[0];
    var decimalPart = parts[1];
    var integerWithCommas = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if(number) {
      for (var i = decimalPart.length - 1; i >= 0; i--) {
        if(decimalPart[i] == "0") {
          decimalPart = decimalPart.slice(0,-1);
        } else {
          break;
        }
      }
      var result = decimalPart ? integerWithCommas + "." + decimalPart : integerWithCommas;
      return result;
    } else {
      return null;
    }
  }

  function getCurrencyWithLabel(currencyLabel, data, typeTransaction) {
    // The regex for future using `^${currencyLabel}(\\s\\(([\\,,0-9]+)\\))?$`
    let item = data.filter((element) => element.sourceCurrency == currencyLabel && element[typeTransaction]);
    item.sort(function(a, b) { return a < b; });
    if (item.length > 0) {
      return item[0];
    }
    return null;
  }
});
// End Currency Exchange Priority component


 // Currency convertor data layer logic : start
if($(".analytics-transaction-tab .exchange-section__tabs-item").length > 0 ){
 
  $(".analytics-transaction-tab .exchange-section__tabs-item").click(function() {
    let analyticsElement = $(this).parents(".analytics-active-link");
    var transactionLink = $(this).text(); // transaction link
    var currencyLink = analyticsElement.find('.analytics-currency-tab .exchange-section__tabs-item.tab-active').text(); // currency link
    var selectedLink = transactionLink + '|' + currencyLink; // transaction and currency
    var eventTrackingDataValues = analyticsElement.data().trackingClickInfoValue; // data captured

      if(typeof eventTrackingDataValues !== 'undefined'){
        var json = JSON.parse(eventTrackingDataValues.replaceAll('\'', '"'));
        if (typeof json !== 'undefined'){
          json.CalculatorFields = selectedLink;     
          var updatedValue = JSON.stringify(json);
          analyticsElement.attr("data-tracking-click-info-value", updatedValue);
          analyticsElement.data("trackingClickInfoValue", updatedValue);
        }
      } 
  });

  $(".analytics-currency-tab .exchange-section__tabs-item").click(function(){
    let analyticsElement = $(this).parents(".analytics-active-link");
    var currencyLink = $(this).text(); // currency link
    var transactionLink = analyticsElement.find('.analytics-transaction-tab .exchange-section__tabs-item.tab-active').text(); // transaction link    
    var selectedLink = transactionLink + '|' + currencyLink; // transaction and currency
    var eventTrackingDataValues = analyticsElement.data().trackingClickInfoValue; // data captured
    if(typeof eventTrackingDataValues !== 'undefined'){
      var json = JSON.parse(eventTrackingDataValues.replaceAll('\'', '"'));
      if (typeof json !== 'undefined'){
        json.CalculatorFields = selectedLink; 
        var updatedValue = JSON.stringify(json);	   
        analyticsElement.attr("data-tracking-click-info-value", updatedValue);
        analyticsElement.data("trackingClickInfoValue", updatedValue);
      }	
    } 
  });

 }
// Currency convertor data layer logic : end

$(document).ready(function () {
  let $scrollCurrencyConverter = $('.scroll-currency-converter');
  if ($scrollCurrencyConverter.length) {
    $scrollCurrencyConverter.each((index,item) => {
      new ScrollCurrencyConverter(item);
    });
  }
});
