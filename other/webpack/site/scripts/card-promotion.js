const MOBILE_SIZE = 767;
let isOpenFilterDialog = false;

const openDropdownHandler = () => {
  let currentDropdown = $('.dropdown__card-type:visible');
  if (currentDropdown.length > 0) {
    let width = currentDropdown.width();
    let currentTopSelect = currentDropdown.offset().top;
    let currentLeftSelect = currentDropdown.offset().left;
    $('.select-box__container')
      .width(width)
      .css({
        top: currentTopSelect,
        left: currentLeftSelect,
        position: 'absolute',
      })
      .show();
  }
};

const displayElementByScreen = () => {
  let width = $(window).width();
  // let height = $(window).height();
  if (width <= MOBILE_SIZE) {
    $('.offer-filter__container').hide();
    $('.offer-filter__button').show();
    if (isOpenFilterDialog) {
      $('.offer-dialog__container').show();
    }
  } else {
    $('.offer-filter__button').hide();
    $('.offer-filter__container').show();
    if (isOpenFilterDialog) {
      $('.offer-dialog__container').hide();
    }
  }

  // $(".select-box__container").width($(".dropdown__card-type:visible").width());
  openDropdownHandler();
};

$(window).resize(displayElementByScreen);

/**
 * Close dialog filter for mobile screen
 */
$('.btn-apply-filter').click(() => {
  isOpenFilterDialog = false;
  $('.offer-filter__button').show();
  $('.offer-dialog__container').hide();
});

/**
 * Open dialog filter for mobile screen
 */
$('.btn-open-filter').click(() => {
  isOpenFilterDialog = true;
  $('.offer-filter__button').hide();
  $('.offer-dialog__container').show();
});

$('.select-box__item').click(() => {
  $('.select-box__container').hide();
});

// $(".dropdown__card-type").click(openDropdownHandler);

// function selectDropdownItemHandler() {
//   $(".dropdown__card-type label").text(this.children[0].textContent);
// }

// $(".select-box__item").click(selectDropdownItemHandler);

$(function () {
  displayElementByScreen();
});
