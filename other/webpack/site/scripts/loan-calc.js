function updateValueOnView(parentElement, value) {
  const percent = $(parentElement).find('.percent-value');
  percent.text(value + '%');
  percent.css('left', `${value}%`);
  percent.css(
    'transform',
    value < 50
      ? 'translateX(calc(-50% + ' + (12 - (12 * value) / 50) + 'px))'
      : 'translateX(calc(-50% - ' + (12 - (12 * (100 - value)) / 50) + 'px))'
  );
}

function handleInputRangeChange(parentElement, event) {
  updateValueOnView(parentElement, event.target.value);
}

function listenInputRangeChange(sliders) {
  if (!sliders.length) {
    return;
  }

  sliders.each((index, element) => {
    const inputRange = $(element).find('input[type="range"]');
    inputRange.on('input', (event) => handleInputRangeChange(element, event));
    updateValueOnView(element, inputRange[0].value);
  });
}

$(document).ready(function () {
  const sliders = $('.slide-container');

  if (sliders.length) {
    listenInputRangeChange(sliders);

    var slider = document.getElementById('myRange');
    $('#output').html('0%');
    var d = new Date();
    var today = d.getDate();
    $('.calendar__input-field').val(today + '/' + (d.getMonth() + 1) + '/' + d.getFullYear());
    calcLoan();
    function nextMonth(strDate, num) {
      var arrDate = strDate.split('/');
      var d = new Date(arrDate[2], arrDate[1], arrDate[0]);
      var date = new Date(d.setMonth(d.getMonth() + num));
      if (date.getMonth() - num > d.getMonth()) {
        date = new Date(date.getFullYear(), date.getMonth(), 0);
      }
      return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    }

    function calcLoan() {
      let origin_debt = toInt($('#bds_percent').val());
      let month = toInt($('.month__input-field').val());
      let profit = toInt($('#profit').val());
      let payMonth = month == 0 ? 0 : Math.round(origin_debt / month);
      let d = $('.calendar__input-field').val();
      let fullData = [];
      let listData = [];
      let fistData = {
        no: 0,
        limitation: d,
        remain_debt: origin_debt,
        origin_debt: '',
        profit: '',
        total_origin_and_profit: '',
      };

      fullData.push(fistData);

      for (var i = 1; i <= month; i++) {
        var profitValue = Math.round((origin_debt - payMonth * (i - 1)) * (profit / 12 / 100));
        let data = {
          no: i,
          limitation: nextMonth(d, i),
          remain_debt: i == month ? 0 : Math.round(origin_debt - payMonth * i),
          origin_debt: payMonth,
          profit: profitValue,
          total_origin_and_profit: payMonth + profitValue,
        };
        listData.push(data);
        fullData.push(data);
      }
      var total = getTotalPerMonth(listData);
      var totalProfit = getTotalProfit(listData);
      var totalProfitValue = eval(totalProfit.join('+'));
      var totalOrg = totalProfitValue + origin_debt;
      if (listData.length == 0) {
        $('#max-value').html(0 + ' VND');
        $('#min-value').html(0 + ' VND');
        $('#total-value').html(numberWithDot(origin_debt) + ' VND');
        $('.footer-cell.origin_debt').html(numberWithCommas(origin_debt));
        $('.footer-cell.profit').html(0);
        $('.footer-cell.total_origin_and_profit').html(numberWithCommas(origin_debt));
      } else {
        $('#max-value').html(numberWithDot(Math.max(...total)) + ' VND');
        $('#min-value').html(numberWithDot(Math.min(...total)) + ' VND');
        $('#total-value').html(numberWithDot(totalOrg) + ' VND');
        $('.footer-cell.origin_debt').html(numberWithCommas(origin_debt));
        $('.footer-cell.profit').html(numberWithCommas(totalProfitValue));
        $('.footer-cell.total_origin_and_profit').html(numberWithCommas(totalOrg));
      }

      var trHtml = '';
      for (var i = 0; i < fullData.length; i++) {
        let no = '<td class="table-cell no"><div>' + fullData[i].no + '</div></td>';
        let limitation = '<td class="table-cell limitation"><div >' + fullData[i].limitation + '</div></td>';
        let remain_debt =
          '<td class="table-cell remain_debt"><div >' + numberWithCommas(fullData[i].remain_debt) + '</div></td>';
        let origin_debt =
          '<td class="table-cell origin_debt"><div>' + numberWithCommas(fullData[i].origin_debt) + '</div></td>';
        let profit = '<td class="table-cell profit"><div>' + numberWithCommas(fullData[i].profit) + '</div></td>';
        let total_origin_and_profit =
          '<td class="table-cell total_origin_and_profit"><div>' +
          numberWithCommas(fullData[i].total_origin_and_profit) +
          '</div></td>';
        let tr =
          "<tr class='table-row'>" +
          no +
          limitation +
          remain_debt +
          origin_debt +
          profit +
          total_origin_and_profit +
          '</tr>';
        trHtml += tr;
      }

      $('.tbl-body').html(trHtml);
    }

    function getTotalPerMonth(data) {
      return data.map((d) => d.total_origin_and_profit);
    }

    function getTotalProfit(data) {
      return data.map((d) => d.profit);
    }

    function numberWithDot(x) {
      return x
        .toString()
        .replaceAll('.', '')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    function numberWithCommas(x) {
      return numberWithDot(x).replaceAll('.', ',');
    }

    function toInt(x) {
      if (!x) return 0;
      return parseInt(x.replaceAll('.', ''));
    }

    function updateBar(value) {
      $('#output').html(value + '%');
      $('#myRange').css('background-size', value + '% 100%');
      $('.percent-value:first-child').css('left', value + '%');
      var calc = value <= 50 ? (12 * value) / 50 : (12 * (100 - value)) / 50;
      if (value < 50) {
        $('.percent-value').css('transform', 'translateX(calc(-50% + ' + (12 - calc) + 'px))');
      } else {
        $('.percent-value').css('transform', 'translateX(calc(-50% - ' + (12 - calc) + 'px))');
      }
      calcLoan();
    }

    $('.input-field__currency-field>input').keypress(function (e) {
      var charCode = e.which ? e.which : event.keyCode;
      if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
        return false;
      }
    });

    $('input.money__input-field').keyup(function (e) {
      $(this).val(numberWithDot(toInt($(this).val())));
      if (!$(this).val()) {
        $(this).val(0);
      }
      setTimeout(() => {
        calcLoan();
      }, 300);
    });

    $('#bds_value').keyup(function () {
      $('#myRange').removeAttr('disabled');
    });

    $('#bds_percent').keyup(function () {
      var bds_value = toInt($('#bds_value').val());
      var bds_percent = toInt($(this).val());
      console.log('this', this);
      if (bds_percent > bds_value) {
        $(this).val(numberWithDot(bds_value));
      }
      var percent = (bds_percent / bds_value) * 100;
      slider.value = percent;
      updateBar(percent);
    });

    $('.month__input-field').keyup(function (event) {
      if ($(this).val() > 240) {
        $(this).val(240);
      }
      calcLoan();
    });
    $('.percent__input-field').keyup(function (event) {
      calcLoan();
    });

    $('.calendar__input-field').on('updateCal', function () {
      calcLoan();
    });

    slider.oninput = function () {
      updateBar(this.value);
      var bds_value = toInt($('#bds_value').val());
      var loan = numberWithDot(Math.round((this.value * bds_value) / 100));
      $('#bds_percent').val(loan);
    };

    $('.loan-calc').click(function () {
      $('.modal').addClass('active');
    });
    $('.close-modal').click(function () {
      $('.modal').removeClass('active');
    });
  }
});
