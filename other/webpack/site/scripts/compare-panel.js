$(document).ready(function () {
    let loanList = $(".loan-list");
    let selectListDropdown = loanList.find(".select-list__dropdown");
    let selectListSubmit = loanList.find(".select-list__submit");
    let tableBodyData = loanList.find(".table-data");

    tableBodyData.addClass("deactivate");

    selectListDropdown.each(function () {
        $(this).click(function () {
            if ($(this).hasClass("dropdown-open")) {
                selectListDropdown.removeClass("dropdown-open");
            } else {
                selectListDropdown.removeClass("dropdown-open");
                $(this).addClass("dropdown-open");
            }
        });
        let selectListDropdownItems = $(this).find(".select-list__dropdown-items");
        let selectListDropdownItemsList = selectListDropdownItems.find("li");
        let selectListDropdownLabelPrefix = $(this).find(
            ".dropdown-label__prefix-label"
        );
        let selectListDropdownLabelInput = $(this).find(
            ".dropdown-label__place-holder"
        );

        selectListDropdownItemsList.each(function () {
            $(this).click(function () {
                if (!$(this).text().trim()) {
                    selectListDropdownLabelPrefix.removeClass("font-small");
                    setSelectedDropdown(
                        $(this),
                        selectListDropdownLabelInput,
                        selectListDropdown.eq(0),
                        selectListDropdown.eq(1),
                        selectListDropdown.eq(2),
                        true
                    );
                } else {
                    selectListDropdownLabelPrefix.addClass("font-small");
                    setSelectedDropdown(
                        $(this),
                        selectListDropdownLabelInput,
                        selectListDropdown.eq(0),
                        selectListDropdown.eq(1),
                        selectListDropdown.eq(2),
                        false
                    );
                }
            });
        });
    });

    selectListSubmit.click(function () {
        let thirdDropdownValue = selectListDropdown
            .eq(2)
            .find(".dropdown-label__place-holder");
        let secondDropdownValue = selectListDropdown
            .eq(1)
            .find(".dropdown-label__place-holder");
        let firstDropdownValue = selectListDropdown
            .eq(0)
            .find(".dropdown-label__place-holder");
        tableBodyData.addClass("deactivate");
        let thirdTagPath = thirdDropdownValue.attr("data-tag-path");
        if (thirdTagPath) {
            tableBodyData.each(function () {
                let dataItem = $(this);
                let itemTagPath = dataItem.attr("data-tag-path");
                if (
                    itemTagPath &&
                    itemTagPath
                        .replace("techcombank:", "/")
                        .indexOf(
                            thirdTagPath.replace("/content/cq:tags/techcombank", "")
                        ) === 0
                ) {
                    dataItem.removeClass("deactivate");
                }
            });
        } else {
            let secondTagPath = secondDropdownValue.attr("data-tag-path");
            if (secondTagPath) {
                tableBodyData.each(function () {
                    let dataItem = $(this);
                    let itemTagPath = dataItem.attr("data-tag-path");
                    if (
                        itemTagPath &&
                        itemTagPath
                            .replace("techcombank:", "/")
                            .indexOf(
                                secondTagPath.replace("/content/cq:tags/techcombank", "")
                            ) === 0
                    ) {
                        dataItem.removeClass("deactivate");
                    }
                });
            } else {
                let firstTagPath = firstDropdownValue.attr("data-tag-path");
                if (firstTagPath) {
                    tableBodyData.each(function () {
                        let dataItem = $(this);
                        let itemTagPath = dataItem.attr("data-tag-path");
                        if (
                            itemTagPath &&
                            itemTagPath
                                .replace("techcombank:", "/")
                                .indexOf(
                                    firstTagPath.replace("/content/cq:tags/techcombank", "")
                                ) === 0
                        ) {
                            dataItem.removeClass("deactivate");
                        }
                    });
                }
            }
        }
    });

    function setSelectedDropdown(
        $selectedItem,
        $selectedLabel,
        $dropdown1,
        $dropdown2,
        $dropdown3,
        unselected
    ) {
        $selectedLabel.text($selectedItem.text());
        if (unselected) {
            $selectedLabel.removeAttr("data-tag-id");
            $selectedLabel.removeAttr("data-tag-path");
        } else {
            $selectedLabel.attr("data-tag-id", $selectedItem.attr("data-tag-id"));
            $selectedLabel.attr("data-tag-path", $selectedItem.attr("data-tag-path"));
        }
        if ($selectedLabel.hasClass("dropdown-label-customer-type")) {
            $dropdown2.find(".house-type-select-item").addClass("deactivate");
            $dropdown2.find(".dropdown-label__place-holder").text("");
            $dropdown2
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-id");
            $dropdown2
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-path");
            $dropdown3.find(".investor-type-select-item").addClass("deactivate");
            $dropdown3.find(".dropdown-label__place-holder").text("");
            $dropdown3
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-id");
            $dropdown3
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-path");
            $dropdown2
                .find(
                    '.house-type-select-item[data-customer-type="' +
                    $selectedItem.attr("data-tag-id") +
                    '"]'
                )
                .removeClass("deactivate");
        }
        if ($selectedLabel.hasClass("dropdown-label-house-type")) {
            $dropdown3.find(".investor-type-select-item").addClass("deactivate");
            $dropdown3.find(".dropdown-label__place-holder").text("");
            $dropdown3
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-id");
            $dropdown3
                .find(".dropdown-label__place-holder")
                .removeAttr("data-tag-path");
            $dropdown3
                .find(
                    '.investor-type-select-item[data-customer-type="' +
                    $dropdown1
                        .find(".dropdown-label-customer-type")
                        .attr("data-tag-id") +
                    '"][data-house-type="' +
                    $selectedItem.attr("data-tag-id") +
                    '"]'
                )
                .removeClass("deactivate");
        }
    }
});
