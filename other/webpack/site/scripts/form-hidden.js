$(document).ready(function() {
    if ($("input[type='hidden']").length) {
        var url = document.location.href;
        var qs = url.substring(url.indexOf('?') + 1).split('&');
        for(var i = 0, result = {}; i < qs.length; i++){
            qs[i] = qs[i].split('=');
            if ($("input[name='"+ qs[i][0] + "']").length) {
                $("input[name='"+ qs[i][0] + "']").val(qs[i][1]);
            }
        }
    }
});
