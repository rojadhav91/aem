var pdfjsLib = require('pdfjs-dist/legacy/build/pdf.js'); // eslint-disable-line @typescript-eslint/no-var-requires

export const renderYoutube = (serviceFeePopup, title, url) => {
  serviceFeePopup.find('.head span.title').text(title);
  serviceFeePopup.css('display', 'flex');
  $('body').addClass('overflow-hidden');
  serviceFeePopup.find('.close-btn').removeClass('md-ripples');
  serviceFeePopup.find('.popup-content').addClass('video');
  let embedLink = url.replace('/watch?v=', '/embed/');
  const iframeMarkup = `<div class="video-container"><iframe src="${embedLink}" frameborder="0" allow="fullscreen; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe></div>`;
  let videoSection = serviceFeePopup.find('.file-content');
  let loadingText = serviceFeePopup.find('.loading');
  loadingText.css('display', 'none');
  videoSection.empty();
  videoSection.append(iframeMarkup);
}

// read file pdf
export const readPdfFile = (serviceFeePopup, linkDownloadBtn, pdfUrl, docTitle) => {
  let title = docTitle ?? serviceFeePopup.data('default-title');
  serviceFeePopup.find('.head span.title').text(title);
  serviceFeePopup.find('.close-btn').addClass('md-ripples');
  serviceFeePopup.css('display', 'flex');
  linkDownloadBtn.attr('href', pdfUrl);
  $('body').addClass('overflow-hidden'); // prevent overflow
  serviceFeePopup.find('.file-content').empty(); // remove old file
  serviceFeePopup.find('.popup-content').removeClass('video');
  let loadingText = serviceFeePopup.find('.loading');
  // The workerSrc property shall be specified.
  pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/3.11.174/pdf.worker.min.js';

  var currPage = 1;
  var numPages = 0;
  var thePDF = null;
  loadingText.css('display', 'block');
  pdfjsLib.getDocument(pdfUrl).promise.then(function (pdf) {
    //Set PDFJS global object (so we can easily access in our page functions
    thePDF = pdf;

    //How many pages it has
    numPages = pdf.numPages;

    //Start with first page
    pdf.getPage(1).then(handlePages);
    loadingText.css('display', 'none');
  });

  function handlePages(page) {
    //This gives us the page's dimensions at full scale
    var viewport = page.getViewport({ scale: 1 });

    //We'll create a canvas for each page to draw it on
    var canvas = document.createElement('canvas');
    canvas.style.display = 'block';
    var context = canvas.getContext('2d');

    canvas.height = viewport.height;
    canvas.width = viewport.width;

    //Draw it on the canvas
    page.render({ canvasContext: context, viewport: viewport });

    //Add it to the web page
    var PDFSection = serviceFeePopup.find('.file-content');
    PDFSection.append(canvas);

    //Move to next page
    currPage++;
    if (thePDF !== null && currPage <= numPages) {
      thePDF.getPage(currPage).then(handlePages);
    }
  }
};
