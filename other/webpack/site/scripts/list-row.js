$(function() {
  $('.list-row .list-row-content').each(function() {
    let columns = $(this).data('columns');
    let rows = $(this).data('rows');

    let numberItems = columns * rows;
    $(this).find(`.list-row-content_item:nth-child(-n+${numberItems + 1})`).css('display', '');
    $(this).find(`.list-row-content_item:nth-child(n+${numberItems + 1})`).css('display', 'none');
  });
});