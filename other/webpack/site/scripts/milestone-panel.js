// Techcombank history component
$(document).ready(function () {
  var techcombankHistory = $('.milestone');
  var techcombankHistoryBgImage = techcombankHistory.find('.milestone__bg-image');
  var techcombankHistoryBody = techcombankHistoryBgImage.find('.milestone__body');
  var techcombankHistoryImageCard = techcombankHistoryBgImage.find('.milestone__image-card');

  var leftContetnWrapper = techcombankHistoryBody.find('.left-content__wrapper');
  var yearSlider = leftContetnWrapper.find('.year-slider__slick');
  var contentSlider = leftContetnWrapper.find('.content-slider__slick');

  var imageCardRightContent = techcombankHistoryImageCard.find('.image-card__right-content');
  var contentSliderImage = imageCardRightContent.find('.content-slider__image');
  contentSliderImage.eq(0).addClass('image-active');

  //Adobe: return if element not found
  if (yearSlider.length == 0) return;

  yearSlider.not('.slick-initialized').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    vertical: true,
    verticalSwiping: true,
    accessibility: true,
    centerMode: true,
    focusOnSelect: true,
    asNavFor: contentSlider,
    speed: 500,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          vertical: false,
          verticalSwiping: false,
        },
      }
    ],
  });
  contentSlider.not('.slick-initialized').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    fade: true,
    draggable: false,
    autoplay: false,
    arrows: false,
    touchMove: false,
    swipe: false,
    asNavFor: yearSlider,
    adaptiveHeight: true,
    speed: 300,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
  yearSlider.on('beforeChange', function (event, slick, preSlide,currentSlide) {
    contentSliderImage.removeClass('image-active');
    contentSliderImage.each(function (i) {
      if (currentSlide == i) {
        $(this).addClass('image-active');
      }
    });
  });
});
// End Techcombank history component