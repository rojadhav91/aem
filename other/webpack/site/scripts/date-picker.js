require('jquery-ui/ui/widgets/datepicker');
$(function() {
  $('.tcb-date-picker').each(function() {
    let datePicker = $(this);
    datePicker.datepicker({
      inline: true,
      showOtherMonths: true,
    }).trigger('ready');
  });
});