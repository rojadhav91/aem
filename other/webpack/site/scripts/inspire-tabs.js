$(document).ready(function () {
    let inspireTabsContainer = $('.tabs-control-component .inspire-exclusive');
    inspireTabsContainer.each(function () {
        // display content for mobile
        let inspireTabs = $('.inspire-exclusive__container[responsive-type="mobile"]');
        if(inspireTabs.length > 0) {
            inspireTabs.each(function () {
                let hiddenContent = $(this).find('.inspire-exclusive__hidden-content');
                let displayedContent = $(this).find('.inspire-exclusive__tab-menu-mobile');
                let tabsNumber = hiddenContent.find('.tab-menu-item').length;
                // copy authored tab header to displayed tab header
                let hiddenMenuItem = hiddenContent.find('.tab-menu-item h3');
                let displayedMenuItem = displayedContent.find('.tab-menu-mobile-item h3');
                for(let i = 0; i < tabsNumber; i++) {
                    displayedMenuItem.get(i).innerHTML = hiddenMenuItem.get(i).innerHTML;
                }
                // copy authored tab content to displayed tab content
                let hiddenTabItem = hiddenContent.find('.inspire-exclusive__tab-list');
                let displayedTabItem = displayedContent.find('.tab-list-mobile');
                for(let i = 0; i < tabsNumber; i++) {
                    displayedTabItem.get(i).innerHTML = hiddenTabItem.get(i).innerHTML;
                }
            });
        }

        // add slow show effect on scroll
        const observer = new IntersectionObserver(intersections => {
            intersections.forEach(({
              target,
              isIntersecting
            }) => {
                if (isIntersecting) {
                    target.classList.add('animate-slow-show');
                }
            });
        }, {
            threshold: 0
        });
        document.querySelectorAll('.tab-menu-mobile-item').forEach(div => {
            observer.observe(div);
        });

        //fix padding for even tabs
        let evenTabs = $('.inspire-exclusive .tab-menu-mobile-item:nth-child(even) .tab-menu-mobile-content h3');
        if(evenTabs.length > 0) {
            evenTabs.each(function () {
                evneTabHeight = $(this).height();
                $(this).css('padding-bottom', 128 - evneTabHeight);
            })
        }

        //AA integration 
        let tabMenuItems = $(this).find('.tab-menu-item');
        let tabMenuMobileItems = $(this).find('.tab-menu-mobile-item');
        tabMenuItems.each(function (){
            trackingAA($(this));
        })
        tabMenuMobileItems.each(function (){
            trackingAA($(this));
        })

        //refresh slick on change tab to avoid slick breaks
        let currentTabContent = $(this);
        tabMenuItems.click(function() {
            let currentActiveTab = currentTabContent.find('.inspire-exclusive__tab-list.active');
            currentActiveTab.find('.swiper1 .swiper-wrapper').slick('refresh');
        })
        tabMenuMobileItems.click(function() {
            let currentActiveTab = $(this).find('.tab-list-mobile');
            currentActiveTab.find('.swiper1 .swiper-wrapper').slick('refresh');
        })
    });

    $('.inspire-exclusive__container .tab-menu-item').on('click', function () {
        var $tab = $(this);
        setTimeout(function () {
            $tab.closest('.inspire-exclusive__container').find('.inspire-exclusive__tab-list.active .swiper2').trigger('animate-scroll-in-tabs');
        }, 150);
    })

    function trackingAA(tabItem) {
        let tabMenuText = tabItem.find('h3').text().replace(/(\r\n|\n|\r)/gm, "")
        tabItem.attr("data-tracking-click-event", "linkClick");
        tabItem.attr("data-tracking-click-info-value", "{'linkClick' : '" + tabMenuText + "'}");
        tabItem.attr(
            "data-tracking-web-interaction-value", "{'webInteractions': {'name': 'Tabs control','type': 'other'}}"
        );
    }
  });