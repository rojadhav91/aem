$(document).ready(function () {

  function numberWithDot(x) {
    return x
      .toString()
      .replaceAll('.', '')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  function toInt(x) {
    if (!x) return 0;
    return parseInt(x.replaceAll(',', ''));
  }

  function enforceMinMax(el) {
    if (el.value != '') {
      if (parseInt(el.value) < parseInt(el.min)) {
        el.value = el.min;
      }
      if (parseInt(el.value) > parseInt(el.max)) {
        el.value = el.max;
      }
    }
  }

  var sliders = $('.tcb-input--save-calc');
  if (sliders.length > 0) {
    sliders.each(function () {
      var slider = $(this);
      var saveCalculation = slider.closest(".save-calculation");
      const moneyInput = saveCalculation.find("[name='depositMoney']");
      const monthInput = saveCalculation.find("[name='depositMonth']");
      moneyInput.forceNumericOnly();
      monthInput.forceNumericOnly();

      function calcSaveMoney() {
        let sliderValue = slider.find('.tcb-input-range_inline-value')[0];
        let save = toInt(moneyInput.val());
        let percent = toInt(sliderValue.innerText);
        let month = toInt(monthInput.val());
        if (save > 0) {
          let profit = Math.round(save * month * (percent / 100 / 12));
          let total = save + profit;
          saveCalculation.find('#profit-value--save-calc').html(numberWithDot(profit) + ' VND');
          saveCalculation.find('#total-value--save-calc').html(numberWithDot(total) + ' VND');
        } else {
          saveCalculation.find('#profit-value--save-calc').html(0 + ' VND');
          saveCalculation.find('#total-value--save-calc').html(0 + ' VND');
        }
      }
  
      // function updateBar(value) {
      //   $('#output').html(value + '%');
      //   var calc = value < 6 ? (value - 2) * 12 : (value - 2) * 12 - 1;
      //   var calPercent = value == 2 ? 0 : value == 10 ? 100 : calc;
      //   var calPostion = value == 2 ? 0 : value == 10 ? 'calc(100% - 24px)' : calPercent + '%';
      //   $('#myRange').css('background-size', calPercent + '% 100%');
      //   $('.percent-value').css('left', calPostion);
      //   calcSaveMoney();
      // }
  
      
  
      moneyInput.keypress(function (event) {
        if ($(this).val().length > 14) {
          event.preventDefault();
        }
        var charCode = event.which ? event.which : event.keyCode;
        if (String.fromCharCode(charCode).match(/[^0-9]/g)) {
          return false;
        }
      });
  
      moneyInput.keyup(function () {
        const maxNumber = 500000000000;
        const currentInput = toInt($(this).val());
        const updateValue = currentInput > maxNumber? maxNumber : currentInput;
  
        $(this).val(numberWithDot(updateValue));
        
        calcSaveMoney();
      });
  
      monthInput.keyup(function () {
        enforceMinMax(this);
        $(this).val(numberWithDot(toInt($(this).val())));
        calcSaveMoney();
      });
      slider[0].oninput = function () {
        // updateBar(this.value);
        calcSaveMoney();
      };
    });
  }
});
