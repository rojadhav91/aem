// Stylesheets
import "./styles/*.scss";

// Javascript or Typescript
import './scripts/*.js';